//
//  INPrivacyPolicyViewController.m
//  shoplocal
//
//  Created by Rishi on 19/09/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INPrivacyPolicyViewController.h"
#import "CommonCallback.h"
#import "INAppDelegate.h"

@interface INPrivacyPolicyViewController ()

@end

@implementation INPrivacyPolicyViewController
@synthesize privacyWebView;
@synthesize okBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    privacyWebView.delegate = self;
    privacyWebView.scalesPageToFit = TRUE;
    
    okBtn.titleLabel.font = DEFAULT_FONT(16);
    [self loadPrivacyUrl];
}

-(void)loadPrivacyUrl
{
    NSFileManager *fileManager      = [NSFileManager defaultManager];
	NSArray *paths                  = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory    = [paths objectAtIndex:0];
    NSString* filePath              = [documentsDirectory stringByAppendingPathComponent:@"privacy.html"];
    DebugLog(@"filePath = %@" , filePath);
    
    if ([fileManager fileExistsAtPath:filePath])
    {
        DebugLog(@"file exists");
        if(([INUserDefaultOperations getDateDifferenceInHours:[INUserDefaultOperations getPrivacySyncDate]]) >= 24)
        {
            DebugLog(@"file exists but more than 24 hours");
            [self sendRequest:filePath];
        } else {
            DebugLog(@"load from file");
            NSURL *url = [NSURL fileURLWithPath:filePath];
            NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
            [privacyWebView loadRequest:requestObj];
        }
    }else{
        DebugLog(@"file not exists");
        [self sendRequest:filePath];
    }
}

-(void)sendRequest:(NSString *)filePath{
    if ([IN_APP_DELEGATE networkavailable]) {
        NSURL *url = [NSURL URLWithString:PRIVACY_LINK];
        
        NSData *urlData = [NSData dataWithContentsOfURL:url];
        [urlData writeToFile:filePath atomically:YES];
        [INUserDefaultOperations setPrivacySyncDate];
        
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [privacyWebView loadRequest:requestObj];
    }else{
        [INUserDefaultOperations showOfflineAlert];
    }
}

#pragma mark - Web View
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    DebugLog(@"webViewDidStartLoad");
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    DebugLog(@"webViewDidFinishLoad");
    [CommonCallback hideProgressHud];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    DebugLog(@"didFailLoadWithError : %@",error);
    [CommonCallback hideProgressHudWithError];
}

- (IBAction)okBtnPressed:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setPrivacyWebView:nil];
    [self setOkBtn:nil];
    [super viewDidUnload];
}

#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
@end
