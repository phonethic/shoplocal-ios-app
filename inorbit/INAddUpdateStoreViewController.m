//
//  INAddUpdateStoreViewController.m
//  shoplocal
//
//  Created by Rishi on 14/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
//https://www.cocoacontrols.com/controls/ios-filter-control
//https://www.cocoacontrols.com/controls/sdwellsegmentedcontrol
//https://www.cocoacontrols.com/controls/jmtabview
//https://www.cocoacontrols.com/controls/jsscrollabletabbar
//https://www.cocoacontrols.com/controls/mkhorizmenu
//https://www.cocoacontrols.com/controls/slidingtabs


#import "INAddUpdateStoreViewController.h"
#import "constants.h"
#import "CommonCallback.h"
#import "INAppDelegate.h"
#import "ActionSheetPicker.h"
#import "NSDate+TCUtils.h"
#import "INGalleryObj.h"
#import "UIImageView+WebCache.h"
#import "CommonCallback.h"
#import "SIAlertView.h"
#import "INShoplocalImageViewController.h"

#define SELECT_CATEGORY_BUTTON_TEXT @"Select category *"

#define SEARCH_CATEGORY(LOCALITY) [NSString stringWithFormat:@"%@%@%@place_api/place_category?for=%@",LIVE_SERVER,URL_PREFIX,API_VERSION,LOCALITY]
#define STORE_DETAILS(ID) [NSString stringWithFormat:@"%@%@%@place_api/places?place_id=%@",LIVE_SERVER,URL_PREFIX,API_VERSION,ID]
#define ADD_MERCHANT_STORE [NSString stringWithFormat:@"%@%@%@place_api/place",LIVE_SERVER,URL_PREFIX,API_VERSION]
#define MAP_STORE(LAT,LONG) [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/staticmap?center=%f,%f&zoom=16&size=300x170&maptype=roadmap&markers=color:blue|label:S|%f,%f&sensor=true&key=AIzaSyCvFovNWeEIPL4355q8L0Chd1urF8n8c0g",LAT,LONG,LAT,LONG]
#define ADD_STORE_TIME [NSString stringWithFormat:@"%@%@%@place_api/place_time",LIVE_SERVER,URL_PREFIX,API_VERSION]
#define MOBILE_NUMBER(CODE,NUMBER)  [NSString stringWithFormat:@"%@%@",[CODE stringByReplacingOccurrencesOfString:@"+" withString:@""],NUMBER]
#define THUMBNAIL_LINK(IMG_LINK) [NSString stringWithFormat:@"%@%@%@",LIVE_SERVER,URL_PREFIX,IMG_LINK]

#define STORE_GALLERY(ID) [NSString stringWithFormat:@"%@%@%@place_api/place_gallery?place_id=%d",LIVE_SERVER,URL_PREFIX,API_VERSION,ID]
#define ADD_GALLERY [NSString stringWithFormat:@"%@%@place_api/place_gallery",URL_PREFIX,API_VERSION]
#define DELETE_GALLERY(ID) [NSString stringWithFormat:@"%@%@place_api/place_gallery?gallery_id=%@",URL_PREFIX,API_VERSION,ID]
#define DELETE_STORE(ID) [NSString stringWithFormat:@"%@%@place_api/place?place_id=%@",URL_PREFIX,API_VERSION,ID]
#define HIDE_STORE [NSString stringWithFormat:@"%@%@%@place_api/place",LIVE_SERVER,URL_PREFIX,API_VERSION]


#define LANDLINE_PLACEHOLDER @"(Area Code) (Tel Number)"
#define MOBILE_PLACEHODER @"10 Digit Mobile Number"

@interface INAddUpdateStoreViewController ()
@property (nonatomic, retain) MBProgressHUD *progressHUD;
@property(strong) NSDictionary *splashJson;
@end

@implementation INAddUpdateStoreViewController
@synthesize galleryView;
@synthesize gridView = _gridView;
@synthesize galleryArray;
@synthesize storenameTextField;
@synthesize areaTextField;
@synthesize cityTextField;
@synthesize pincodeTextField;
@synthesize storeScrollView;
@synthesize pageControl;
@synthesize descriptionTextView;
@synthesize streetTextView;
@synthesize landmarkTextView;
@synthesize landlineTextField;
@synthesize mobileNoTextField;
@synthesize faxNoTextField;
@synthesize tollfreeTextField;
@synthesize emailTextField;
@synthesize websiteTextField;
@synthesize sunBtn;
@synthesize monBtn;
@synthesize tueBtn;
@synthesize wedBtn;
@synthesize thurBtn;
@synthesize friBtn;
@synthesize satBtn;
@synthesize currentLatitude;
@synthesize currentLongitude;
@synthesize progressHUD;
@synthesize mapImageView;
@synthesize maplbl;
@synthesize placeId;
@synthesize newMedia;
@synthesize iconImageView;
@synthesize selectedDate = _selectedDate;
@synthesize categoryDict;
@synthesize selectedIndex;
@synthesize categoryBtn;
@synthesize toDateBtn;
@synthesize fromDateBtn;
@synthesize fromselectedDate;
@synthesize toselectedDate;
@synthesize locationScrollView;
@synthesize photos = _photos;
@synthesize contactScrollView;
@synthesize gallerymsglbl;
@synthesize mapskipBtn;
@synthesize backView,backview2,backview3,backView4;
@synthesize openlbl;
@synthesize openonlbl;
@synthesize textviewplaceholderLabel;
@synthesize descriptionScrollView;
@synthesize addImageDescriptionView,lbldescriptionViewContent,lbldescriptionViewHeader,descriptionViewSetBtn,descriptionViewSkipBtn,descriptionViewtitleTextView;
@synthesize editedImage;
@synthesize searchdict;
@synthesize locationInfoBtn;
@synthesize lblstoreName,lblstoreDescription;
@synthesize fromDatelbl;
@synthesize toDatelbl;
@synthesize mobileNumberTableView,addMoblieNumberBtn;
@synthesize imgSeperator1,imgSeperator2,imgSeperator3,imgSeperator4,imgSeperator5,imgSeperator6;
@synthesize contactBackScrollView;
@synthesize LandlineNumberTableView,addLandlineNumberBtn;
@synthesize mobileArray;
@synthesize landlineArray;
@synthesize areaArray;
@synthesize place_status;
@synthesize shoplocalImage;
@synthesize conactscreenlbl;
@synthesize txtFbUrl,txtTwitterUrl,imgSeperator7,imgSeperator8;
@synthesize descriptionSkipBtn,contactdetailsSkipBtn,storeLogoSkipBtn;
@synthesize saveMapLocationBtn,saveLocationBtn,saveContactDetailsBtn,descriptionSaveBtn,daysBtn,doneBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [locationManager stopUpdatingLocation];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == pincodeTextField || textField == landlineTextField || textField == mobileNoTextField || textField == faxNoTextField || textField == tollfreeTextField ||  textField.tag == landlineTextField.tag + 1 || textField.tag == landlineTextField.tag + 2 ||  textField.tag == mobileNoTextField.tag + 1 || textField.tag == mobileNoTextField.tag + 2)
    {
        NSCharacterSet *nonNumberSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        
        if ([string rangeOfCharacterFromSet:nonNumberSet].location != NSNotFound)
        {
            return NO;
        } else {
            if(textField == landlineTextField || textField.tag == landlineTextField.tag + 1 || textField.tag == landlineTextField.tag + 2)
            {
                NSUInteger newLength = [textField.text length] + [string length] - range.length;
                return (newLength > 11) ? NO : YES;
                
            } else if(textField == mobileNoTextField || textField.tag == mobileNoTextField.tag + 1 || textField.tag == mobileNoTextField.tag + 2)
            {
                NSUInteger newLength = [textField.text length] + [string length] - range.length;
                return (newLength > 10) ? NO : YES;
            } else {
                return YES;
            }
        }
    } else {
        return YES;
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField.frame.origin.y > locationScrollView.contentOffset.y)
    {
        if(textField == areaTextField || textField == cityTextField || textField == pincodeTextField)
        {
            [locationScrollView setContentOffset:CGPointMake(0,textField.frame.origin.y-100) animated:YES];
            if(textField == areaTextField)
            {
                [self showAreaPicker];
                return NO;
            }
        }
    }
    if (textField.tag == mobileNoTextField.tag+1 || textField.tag == mobileNoTextField.tag+2) {
        [contactScrollView setContentOffset:CGPointMake(0,emailTextField.frame.origin.y-120) animated:YES];
    }
    if(textField.frame.origin.y > contactScrollView.contentOffset.y)
    {
        if(textField == tollfreeTextField || textField == emailTextField || textField == websiteTextField)
        {
            [contactScrollView setContentOffset:CGPointMake(0,textField.frame.origin.y-100) animated:YES];
            
        }
        if(textField == txtFbUrl || textField == txtTwitterUrl )
        {
            [contactScrollView setContentOffset:CGPointMake(0,textField.frame.origin.y) animated:YES];
        }
    }
    [locationScrollView setContentSize: CGSizeMake(locationScrollView.frame.size.width, locationScrollView.frame.size.height + 220)];
    [contactScrollView setContentSize: CGSizeMake(contactScrollView.frame.size.width, contactScrollView.frame.size.height + 220)];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField.frame.origin.y > locationScrollView.contentOffset.y)
    {
        if(textField == areaTextField || textField == cityTextField || textField == pincodeTextField)
        {
            [locationScrollView setContentOffset:CGPointMake(0,textField.frame.origin.y-100) animated:YES];
        }
    }
    if(textField.frame.origin.y > contactScrollView.contentOffset.y)
    {
        if(textField == tollfreeTextField || textField == emailTextField || textField == websiteTextField)
        {
            [contactScrollView setContentOffset:CGPointMake(0,textField.frame.origin.y-100) animated:YES];
            
        }
        if(textField == txtFbUrl || textField == txtTwitterUrl )
        {
            [contactScrollView setContentOffset:CGPointMake(0,textField.frame.origin.y) animated:YES];
        }
    }
    [locationScrollView setContentSize: CGSizeMake(locationScrollView.frame.size.width, locationScrollView.frame.size.height - 170)];
    if(textField != txtFbUrl && textField != txtTwitterUrl ){
        [contactScrollView setContentSize: CGSizeMake(contactScrollView.frame.size.width, contactScrollView.frame.size.height - 170)];
    }
    if (textField == storenameTextField) {
        [storenameTextField resignFirstResponder];
        [self categoryBtnPressed:categoryBtn];
	}  else if (textField == streetTextView) {
        [landmarkTextView becomeFirstResponder];
	} else if (textField == landmarkTextView) {
        [areaTextField becomeFirstResponder];
	} else if (textField == areaTextField) {
        [cityTextField becomeFirstResponder];
	} else if (textField == cityTextField) {
        [pincodeTextField becomeFirstResponder];
	} else if (textField == pincodeTextField) {
        [pincodeTextField resignFirstResponder];
        [self scrollTobottom];
	}else if (textField == landlineTextField) {
        [mobileNoTextField becomeFirstResponder];
	} else if (textField == mobileNoTextField) {
        [faxNoTextField becomeFirstResponder];
	} else if (textField == faxNoTextField) {
        [tollfreeTextField becomeFirstResponder];
	} else if (textField == tollfreeTextField) {
        [emailTextField becomeFirstResponder];
	} else if (textField == emailTextField) {
        [websiteTextField becomeFirstResponder];
	} else if (textField == websiteTextField) {
        [txtFbUrl becomeFirstResponder];
	}else if (textField == txtFbUrl) {
        [txtTwitterUrl becomeFirstResponder];
	} else if (textField == txtTwitterUrl) {
        [txtTwitterUrl resignFirstResponder];
        [self scrollTobottom];
	}
   	return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                             style:UIBarButtonItemStylePlain
                                                            target:nil
                                                            action:nil];
    [[self navigationItem] setBackBarButtonItem:back];
    //[self addHUDView];
//    backView = [CommonCallback setViewPropertiesWithRoundedCorner:backView];
//    backview2 = [CommonCallback setViewPropertiesWithRoundedCorner:backview2];
//    backview3 = [CommonCallback setViewPropertiesWithRoundedCorner:backview3];
//    backView4 = [CommonCallback setViewPropertiesWithRoundedCorner:backView4];
    
    backView.backgroundColor =  [UIColor whiteColor];
    backview2.backgroundColor =  [UIColor whiteColor];
    backview3.backgroundColor =  [UIColor whiteColor];
    backView4.backgroundColor =  [UIColor whiteColor];
    
    [self setUI];
    
    if ([self respondsToSelector:NSSelectorFromString(@"edgesForExtendedLayout")]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    [self loadAreaListFromAreaTable];
    
    isImgFrmShopLocalGallery = 0;
    
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        [button setImage:[UIImage imageNamed:@"back_button_iOS6.png"] forState:UIControlStateNormal];
    }else{
        [button setImage:[UIImage imageNamed:@"back_button_iOS7.png"] forState:UIControlStateNormal];
    }
    [button addTarget:self action:@selector(backbuttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 50, 32)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    for(UIButton *button in _buttonsCollection)
    {
        // Apply your styles
        button.titleLabel.font = DEFAULT_FONT(17);
        
        if ([button isEqual: saveMapLocationBtn] || [button isEqual: saveLocationBtn] ||[button isEqual: saveContactDetailsBtn] ||[button isEqual: descriptionSaveBtn] || [button isEqual: daysBtn] ||[button isEqual: doneBtn]) {
            button.backgroundColor = GREEN_COLOR_FOR_BUTTON;
        }else if ([button isEqual: mapskipBtn] || [button isEqual: contactdetailsSkipBtn] ||[button isEqual: descriptionSkipBtn] || [button isEqual: storeLogoSkipBtn] ) {
            button.backgroundColor = GRAY_COLOR_FOR_BUTTON;
        }
    }

    
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    viewTap.numberOfTapsRequired = 1;
    viewTap.cancelsTouchesInView = NO;
    [self.view  addGestureRecognizer:viewTap];
    
    UITapGestureRecognizer *imageTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(droppibBtnPressed:)];
    imageTap.numberOfTapsRequired = 1;
    imageTap.cancelsTouchesInView = NO;
    [self.mapImageView  addGestureRecognizer:imageTap];
    
    UITapGestureRecognizer *logoImageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chooselogoTapDetected:)];
    logoImageTap.numberOfTapsRequired = 1;
    [self.iconImageView addGestureRecognizer:logoImageTap];
    
    UIView *view = nil;
    CGFloat curXLoc = 0;
    for (view in [storeScrollView subviews])
    {
        if ([view isKindOfClass:[UIView class]])
        {
            view.backgroundColor = [UIColor clearColor];
            CGRect frame = view.frame;
            frame.origin = CGPointMake(curXLoc, 0);
            view.frame = frame;
            
            curXLoc += (storeScrollView.frame.size.width);
        }
    }
    [pageControl setNumberOfPages:6];
    //------------set the content size so it can be scrollable
    //DebugLog(@"_addRecordScrollView.subviews.count %d",storeScrollView.subviews.count);
    [storeScrollView setContentSize:CGSizeMake((6 * storeScrollView.frame.size.width), storeScrollView.frame.size.height)];
    
    storeScrollView.backgroundColor = [UIColor clearColor];
    storeScrollView.clipsToBounds = YES;
    storeScrollView.scrollEnabled = YES;
    storeScrollView.pagingEnabled = YES;
    storeScrollView.showsHorizontalScrollIndicator = NO;
    storeScrollView.showsVerticalScrollIndicator = NO;
    storeScrollView.scrollsToTop = YES;
    storeScrollView.delegate = self;
    storeScrollView.bounces = NO;
    storeScrollView.directionalLockEnabled = YES;
    
//    descriptionTextView.layer.borderColor = [UIColor whiteColor].CGColor;
//    descriptionTextView.layer.borderWidth = 2.0;
//    descriptionTextView.layer.cornerRadius = 8.0;
    
    descriptionTextView.autocapitalizationType   = UITextAutocapitalizationTypeSentences;
    descriptionTextView.autocorrectionType       = UITextAutocorrectionTypeYes;
    
    
    storenameTextField.autocapitalizationType   = UITextAutocapitalizationTypeSentences;
    storenameTextField.autocorrectionType       = UITextAutocorrectionTypeYes;

    streetTextView.autocapitalizationType   = UITextAutocapitalizationTypeSentences;
    streetTextView.autocorrectionType       = UITextAutocorrectionTypeYes;
    
    landmarkTextView.autocapitalizationType   = UITextAutocapitalizationTypeSentences;
    landmarkTextView.autocorrectionType       = UITextAutocorrectionTypeYes;
    
    mobileArray = [[NSMutableArray alloc] init];
    landlineArray = [[NSMutableArray alloc] init];
    
    //ACtionsheetPicker
    self.selectedDate = [NSDate date];
    self.fromselectedDate = [NSDate date];
    self.toselectedDate = [NSDate date];
    self.categoryDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"1",@"Shopping", @"2",@"Food", @"3",@"Entertainment", @"4",@"Services", @"5",@"Kids", nil];

     ///////////////////Multi Select Picker//////////////
    selectionStates = [[NSMutableDictionary alloc] init];
    resultArray = [[NSMutableArray alloc] init];
	for (NSString *key in categoryDict.allKeys)
		[selectionStates setObject:[NSNumber numberWithBool:NO] forKey:key];
	[self addPickerWithDoneButton];
    
    searchdict = [[NSMutableDictionary alloc] init];
    if ([IN_APP_DELEGATE networkavailable]) {
        [self sendStoreCategoryRequest];
    }
    
    gallerymsglbl.textColor = BROWN_COLOR;
    gallerymsglbl.font = DEFAULT_FONT(14);
    maplbl.textColor = BROWN_COLOR;
    maplbl.font = DEFAULT_FONT(14);
    conactscreenlbl.font = DEFAULT_FONT(12);
    conactscreenlbl.textColor = BROWN_COLOR;
    
    fromDateBtn.titleLabel.font = DEFAULT_FONT(14);
    toDateBtn.titleLabel.font = DEFAULT_FONT(14);
    openlbl.font = DEFAULT_BOLD_FONT(14);
    openonlbl.font = DEFAULT_BOLD_FONT(14);
    
    textviewplaceholderLabel.font = DEFAULT_FONT(17);
     ///////////////////Multi Select Picker//////////////
    //Add gridView
    _gridView = [[VCGridView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(gallerymsglbl.frame), self.view.frame.size.width, self.view.frame.size.height - (doneBtn.frame.size.height+CGRectGetMaxY(gallerymsglbl.frame)+32))];
	_gridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	_gridView.delegate = self;
	_gridView.dataSource = self;
	[self.galleryView addSubview:_gridView];
    
    doneBtn.frame = CGRectMake(doneBtn.frame.origin.x, CGRectGetMaxY(_gridView.frame) + 20, doneBtn.frame.size.width, doneBtn.frame.size.height);

    galleryArray = [[NSMutableArray alloc] init];
    
    if(self.placeId == nil || [self.placeId isEqualToString:@""]) {
        //Do shomething
         [galleryArray addObject:@"placeholder"];
        [_gridView reloadData];
        areaTextField.text = [IN_APP_DELEGATE getActivePlaceNameFromAreaTable];
        cityTextField.text = [IN_APP_DELEGATE getCityFromAreaTable];
        pincodeTextField.text = [IN_APP_DELEGATE getPincodeFromAreaTable];
        [saveMapLocationBtn setHidden:YES];
        [fromDateBtn setTitle:@"00:00" forState: UIControlStateNormal];
        [fromDateBtn setTitle:@"00:00" forState: UIControlStateHighlighted];
        [toDateBtn setTitle:@"00:00" forState: UIControlStateNormal];
        [toDateBtn setTitle:@"00:00" forState: UIControlStateHighlighted];
    } else {
        place_status = @"1";
        if ([IN_APP_DELEGATE networkavailable]) {
            [self sendStoreDetailRequest];
            [self sendStoreGalleryRequest];
        } else {
            [INUserDefaultOperations showOfflineAlert];
        }
        [saveMapLocationBtn setHidden:NO];
        mapskipBtn.frame = CGRectMake(mapskipBtn.frame.origin.x, mapskipBtn.frame.origin.y, 125, mapskipBtn.frame.size.height);
    }
    
    contactBackScrollView.clipsToBounds = YES;
    contactBackScrollView.backgroundColor =  [UIColor whiteColor];
//    contactBackScrollView.layer.borderColor = [UIColor whiteColor].CGColor;
//    contactBackScrollView.layer.borderWidth = 2.0;
//    contactBackScrollView.layer.cornerRadius = 8.0;
    
    [contactBackScrollView setContentSize:CGSizeMake(self.contactBackScrollView.frame.size.width,CGRectGetMaxY(txtTwitterUrl.frame)+10)];
    [mobileNumberTableView setFrame:CGRectMake(mobileNumberTableView.frame.origin.x,
                                               mobileNumberTableView.frame.origin.y,
                                               mobileNumberTableView.frame.size.width,
                                               mobileNumberTableView.contentSize.height)];
    [mobileNumberTableView setHidden:YES];
    mobileNumberTableView.scrollEnabled = FALSE;
    
    [LandlineNumberTableView setFrame:CGRectMake(LandlineNumberTableView.frame.origin.x,
                                               LandlineNumberTableView.frame.origin.y,
                                               LandlineNumberTableView.frame.size.width,
                                               LandlineNumberTableView.contentSize.height)];
    [LandlineNumberTableView setHidden:YES];
    LandlineNumberTableView.scrollEnabled = FALSE;
    
    mobileNumberCount = 0; landlineNumberCount = 0;
    
    mobileNumberTableView.backgroundColor   =  [UIColor clearColor];
    LandlineNumberTableView.backgroundColor =  [UIColor clearColor];
    
    mobileNumberTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    LandlineNumberTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    if(self.placeId != nil && ![self.placeId isEqualToString:@""]) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(updateStoreBtnClicked)];
    }
    [self setViewControllerTitle:pageControl.currentPage];
}

-(void)updateStoreBtnClicked {
    SIAlertView *sialertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Choose your option"];
    sialertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
    sialertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
    
    if([place_status isEqualToString:@"1"]) {
        [sialertView addButtonWithTitle:@"Hide Store"
                               type:SIAlertViewButtonTypeDestructive
                            handler:^(SIAlertView *alert) {
                                DebugLog(@"Hide Store Clicked");
                                [self hideStore];
//                                [self sendHideStoreRequest:@"0"];
                            }];
    } else {
        [sialertView addButtonWithTitle:@"Show Store"
                                   type:SIAlertViewButtonTypeDestructive
                                handler:^(SIAlertView *alert) {
                                    DebugLog(@"Hide Store Clicked");
//                                    [self hideStore];
                                    [self sendHideStoreRequest:@"1"];
                                }];
    }
    [sialertView addButtonWithTitle:@"Delete Store"
                               type:SIAlertViewButtonTypeDestructive
                            handler:^(SIAlertView *alert) {
                                DebugLog(@"Delete Store Clicked");
                                [self deleteStore];
//                                [self sendDeleteStoreRequest];
                            }];
    [sialertView addButtonWithTitle:@"Cancel"
                               type:SIAlertViewButtonTypeCancel
                            handler:^(SIAlertView *alert) {
                                DebugLog(@"Skip Clicked");
                            }];
    [sialertView show];
}

-(void)hideStore
{
    SIAlertView *sialertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Do you want to hide this store from listing?"];
    sialertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
    sialertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
    
    [sialertView addButtonWithTitle:@"Yes"
                               type:SIAlertViewButtonTypeDestructive
                            handler:^(SIAlertView *alert) {
                                DebugLog(@"Delete Store Clicked");
                                [self sendHideStoreRequest:@"0"];
                            }];
    [sialertView addButtonWithTitle:@"NO"
                               type:SIAlertViewButtonTypeCancel
                            handler:^(SIAlertView *alert) {
                                DebugLog(@"Skip Clicked");
                            }];
    [sialertView show];
}

-(void)deleteStore
{
    SIAlertView *sialertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Do you want to delete this store ?"];
    sialertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
    sialertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
    
    [sialertView addButtonWithTitle:@"Yes"
                               type:SIAlertViewButtonTypeDestructive
                            handler:^(SIAlertView *alert) {
                                DebugLog(@"Delete Store Clicked");
                                [self sendDeleteStoreRequest];
                            }];
    [sialertView addButtonWithTitle:@"NO"
                               type:SIAlertViewButtonTypeCancel
                            handler:^(SIAlertView *alert) {
                                DebugLog(@"Skip Clicked");
                            }];
    [sialertView show];
}

-(void)showOKAlert:(NSString*)lmessage
{
    if(self.isViewLoaded && self.view.window){
        SIAlertView *sialertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:lmessage];
        sialertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
        sialertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
        
        [sialertView addButtonWithTitle:@"OK"
                                   type:SIAlertViewButtonTypeCancel
                                handler:^(SIAlertView *alert) {
                                    DebugLog(@"Skip Clicked");
                                }];
        [sialertView show];
    }
}

-(void)backbuttonClicked:(id) sender
{
    if(backAlert==nil)
    {
        backAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Are you sure you want to leave this screen ?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
        backAlert.tag = 4;
        [backAlert show];
    } else {
        [backAlert show];
    }
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    [descriptionScrollView setContentSize: CGSizeMake(descriptionScrollView.frame.size.width, descriptionScrollView.frame.size.height + 220)];
    self.descriptionScrollView.scrollEnabled = TRUE;
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if (range.location>0 || text.length!=0) {
        textviewplaceholderLabel.hidden = YES;
    }else{
        textviewplaceholderLabel.hidden = NO;
    }
    return YES;
}

#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma Internal Methods
-(void)setUI{
    storenameTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    storenameTextField.font = DEFAULT_FONT(15);
    categoryBtn.titleLabel.font = DEFAULT_FONT(15);
    streetTextView.font     = DEFAULT_FONT(15);
    landmarkTextView.font   = DEFAULT_FONT(15);
    areaTextField.font      = DEFAULT_FONT(15);
    cityTextField.font      = DEFAULT_FONT(15);
    pincodeTextField.font   = DEFAULT_FONT(15);
    
    landlineTextField.font  = DEFAULT_FONT(15);
    mobileNoTextField.font  = DEFAULT_FONT(15);
    faxNoTextField.font     = DEFAULT_FONT(15);
    tollfreeTextField.font  = DEFAULT_FONT(15);
    emailTextField.font     = DEFAULT_FONT(15);
    websiteTextField.font   = DEFAULT_FONT(15);
    txtFbUrl.font     = DEFAULT_FONT(15);
    txtTwitterUrl.font   = DEFAULT_FONT(15);

    landlineTextField.placeholder = LANDLINE_PLACEHOLDER;
    mobileNoTextField.placeholder = MOBILE_PLACEHODER;
    landlineTextField.tag = 100;
    mobileNoTextField.tag = 200;
    
    descriptionTextView.font  = DEFAULT_FONT(15);
    
    landlineTextField.keyboardType = UIKeyboardTypeNumberPad;
    mobileNoTextField.keyboardType = UIKeyboardTypeNumberPad;

    addImageDescriptionView = [CommonCallback setViewPropertiesWithRoundedCorner:addImageDescriptionView];
    addImageDescriptionView.layer.shadowColor = [UIColor blackColor].CGColor;
    addImageDescriptionView.layer.shadowOffset = CGSizeMake(1, 1);
    addImageDescriptionView.layer.shadowOpacity = 1.0;
    addImageDescriptionView.layer.shadowRadius = 10.0;
    
    lbldescriptionViewHeader.textAlignment = NSTextAlignmentCenter;
    lbldescriptionViewHeader.backgroundColor = [UIColor clearColor];
    lbldescriptionViewHeader.font = DEFAULT_FONT(20);
    lbldescriptionViewHeader.textColor = [UIColor blackColor];
    lbldescriptionViewHeader.adjustsFontSizeToFitWidth = YES;
    lbldescriptionViewHeader.text = ALERT_TITLE;
    
    lbldescriptionViewContent.textAlignment = NSTextAlignmentCenter;
    lbldescriptionViewContent.backgroundColor = [UIColor clearColor];
    lbldescriptionViewContent.font = DEFAULT_FONT(16);
    lbldescriptionViewContent.textColor = [UIColor darkGrayColor];
    lbldescriptionViewContent.numberOfLines = 15;
    lbldescriptionViewContent.text = @"Add a description of the photo";
    
    descriptionViewSetBtn.titleLabel.font = DEFAULT_FONT([UIFont buttonFontSize]);
    descriptionViewSkipBtn.titleLabel.font = DEFAULT_FONT([UIFont buttonFontSize]);
    
    [descriptionViewSetBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [descriptionViewSetBtn setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.8] forState:UIControlStateHighlighted];
    
    [descriptionViewSkipBtn setTitleColor:[UIColor colorWithWhite:0.3 alpha:1] forState:UIControlStateNormal];
    [descriptionViewSkipBtn setTitleColor:[UIColor colorWithWhite:0.3 alpha:0.8] forState:UIControlStateHighlighted];
    
    UIImage *dnormalImage = [UIImage imageNamed:@"SIAlertView.bundle/button-destructive-d"];
    UIImage *dhighlightedImage = [UIImage imageNamed:@"SIAlertView.bundle/button-destructive-d"];
    UIEdgeInsets dinsets = [self getResizedImageInset:dnormalImage];
	dnormalImage = [dnormalImage resizableImageWithCapInsets:dinsets];
	dhighlightedImage = [dhighlightedImage resizableImageWithCapInsets:dinsets];
    
    UIImage *normalImage = [UIImage imageNamed:@"SIAlertView.bundle/button-cancel"];
    UIImage *highlightedImage = [UIImage imageNamed:@"SIAlertView.bundle/button-cancel-d"];
    UIEdgeInsets insets = [self getResizedImageInset:normalImage];
	normalImage = [normalImage resizableImageWithCapInsets:insets];
	highlightedImage = [highlightedImage resizableImageWithCapInsets:insets];
    
    [descriptionViewSetBtn setBackgroundImage:dnormalImage forState:UIControlStateNormal];
	[descriptionViewSetBtn setBackgroundImage:dhighlightedImage forState:UIControlStateHighlighted];
    
	[descriptionViewSkipBtn setBackgroundImage:normalImage forState:UIControlStateNormal];
	[descriptionViewSkipBtn setBackgroundImage:highlightedImage forState:UIControlStateHighlighted];
    
    descriptionViewtitleTextView.textAlignment = NSTextAlignmentLeft;
    descriptionViewtitleTextView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:1];
    descriptionViewtitleTextView.font = DEFAULT_BOLD_FONT(18);
    descriptionViewtitleTextView.textColor = BROWN_COLOR;
    descriptionViewtitleTextView.text = @"";
    descriptionViewtitleTextView.layer.cornerRadius = 5.0;
    descriptionViewtitleTextView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    descriptionViewtitleTextView.layer.borderWidth = 0.5;
    descriptionViewtitleTextView.delegate = self;
    //    descriptionViewtitleTextView.layer.shadowColor = [UIColor blackColor].CGColor;
    //    descriptionViewtitleTextView.layer.shadowOffset = CGSizeMake(1, 1);
    //    descriptionViewtitleTextView.layer.shadowOpacity = 1;
    //    descriptionViewtitleTextView.layer.shadowRadius = 10.0;
    
    [addImageDescriptionView setHidden:YES];
    [self setEditing:FALSE];
    
    locationInfoBtn.layer.cornerRadius = 50.0;
    
    lblstoreName.textAlignment = NSTextAlignmentLeft;
    lblstoreName.backgroundColor = [UIColor clearColor];
    lblstoreName.font = DEFAULT_BOLD_FONT(15);
    lblstoreName.textColor = BROWN_COLOR;
    //lblstoreName.adjustsFontSizeToFitWidth = YES;
    
    lblstoreDescription.textAlignment = NSTextAlignmentLeft;
    lblstoreDescription.backgroundColor = [UIColor clearColor];
    lblstoreDescription.font = DEFAULT_FONT(12);
    lblstoreDescription.textColor = BROWN_COLOR;
    
    
    
}

-(UIEdgeInsets)getResizedImageInset:(UIImage *)image
{
    CGFloat hInset = floorf(image.size.width / 2);
	CGFloat vInset = floorf(image.size.height / 2);
	UIEdgeInsets insets = UIEdgeInsetsMake(vInset, hInset, vInset, hInset);
    return insets;
}


-(void)loadAreaListFromAreaTable
{
    //DebugLog(@"loadAreaListFromAreaTable");
    if (areaArray == nil) {
        areaArray = [[NSMutableArray alloc] init];
    }else{
        [areaArray removeAllObjects];
    }
    sqlite3 *shoplocalDB;
	if (sqlite3_open([[IN_APP_DELEGATE databasePath] UTF8String], &shoplocalDB) == SQLITE_OK) {
        NSString *queryString = @"SELECT SUBLOCALITY FROM AREA ";
        const char *sqlChar = [queryString UTF8String];
        sqlite3_stmt *statement = nil;
        if (sqlite3_prepare_v2(shoplocalDB, sqlChar, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                NSString *sub_locality  = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                [areaArray addObject:sub_locality];
                sub_locality = nil;
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(shoplocalDB);
    }
}

-(void)showAreaPicker
{
    int initialIndex = 0;
    if (areaArray.count > 0) {
        if (![areaTextField.text isEqualToString:@""]) {
            if(NSNotFound != [areaArray indexOfObject:areaTextField.text])
                initialIndex = [areaArray indexOfObject:areaTextField.text];
        }
        [ActionSheetStringPicker showPickerWithTitle:@"Select location" rows:self.areaArray initialSelection:initialIndex target:self successAction:@selector(selectLocation:element:) cancelAction:@selector(actionPickerCancelled:) origin:areaTextField];
    }
}

#pragma mark - Actionsheet Implementation
- (void)selectLocation:(NSNumber *)lselectedIndex element:(id)element {

    NSString *subLocality = [areaArray objectAtIndex:[lselectedIndex intValue]];
    //DebugLog(@"%@",subLocality);
    if ([areaTextField.text isEqualToString:subLocality])
    {
        DebugLog(@"Same as previous");
    }else{
        areaTextField.text = subLocality;
        cityTextField.text = [IN_APP_DELEGATE getCityFromAreaTable:subLocality];
        pincodeTextField.text = [IN_APP_DELEGATE getPincodeFromAreaTable:subLocality];
    }
    [self scrollTobottom];
}

- (void)actionPickerCancelled:(id)sender {
    //DebugLog(@"ActionSheetPicker was cancelled");
    [self scrollTobottom];
}

-(void)addHUDView
{
    hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:hud];
	hud.dimBackground = NO;
    hud.labelText = @"Loading";
	hud.detailsLabelText = @"Please wait ...";
    [hud hide:NO];
}

-(void)removeHUDView
{
    [hud removeFromSuperview];
    hud = nil;
}

//- (void)dealloc {
//    [self removeHUDView];
//}

-(void) addPickerWithDoneButton
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        
        
        DebugLog(@"%f", self.view.frame.size.height);
        
        _customView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width , 256)];
        _customView.backgroundColor = [UIColor whiteColor];
        
        
        UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(50, 6, 200, 30)];
        [lbl setText:@"Select mall"];
        lbl.backgroundColor     = [UIColor clearColor];
        lbl.textAlignment       = NSTextAlignmentCenter;
        lbl.textColor           = [UIColor whiteColor];
        lbl.font                = DEFAULT_FONT(18);
        [_customView addSubview:lbl];
        
        UISegmentedControl *doneButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Done"]];
        doneButton.momentary    = YES;
        doneButton.frame        = CGRectMake(260, 6.0f, 50.0f, 30.0f);
        doneButton.tintColor    = DEFAULT_COLOR;
        doneButton.segmentedControlStyle = UISegmentedControlStyleBar;
        [doneButton addTarget:self action:@selector(doneFilterBtnClicked:) forControlEvents:UIControlEventValueChanged];
        [_customView addSubview:doneButton];
        
        UISegmentedControl *cancelButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Cancel"]];
        cancelButton.momentary    = YES;
        cancelButton.frame        = CGRectMake(10, 6.0f, 50.0f, 30.0f);
        cancelButton.tintColor    = DEFAULT_COLOR;
        cancelButton.segmentedControlStyle = UISegmentedControlStyleBar;
        [cancelButton addTarget:self action:@selector(cancelFilterBtnClicked:) forControlEvents:UIControlEventValueChanged];
        [_customView addSubview:cancelButton];
        
        pickerView = [[ALPickerView alloc] initWithFrame:CGRectMake(0, 40, 320, 400)];
        pickerView.delegate = self;
        [_customView addSubview:pickerView];
        
        [self.view addSubview:_customView];
        
        [_customView setHidden:TRUE];
        
    } else {
        
        
        actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                  delegate:nil
                                         cancelButtonTitle:nil
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:nil];
        
        [actionSheet setActionSheetStyle:UIActionSheetStyleBlackOpaque];
        
        // Init picker and add it to view
        pickerView = [[ALPickerView alloc] initWithFrame:CGRectMake(0, 40, 0, 0)];
        pickerView.delegate = self;
        [actionSheet addSubview:pickerView];
        
        UISegmentedControl *doneButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Done"]];
        doneButton.momentary = YES;
        doneButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
        doneButton.segmentedControlStyle = UISegmentedControlStyleBar;
        //doneButton.tintColor = DEFAULT_COLOR;
        [doneButton addTarget:self action:@selector(doneFilterBtnClicked:) forControlEvents:UIControlEventValueChanged];
        [actionSheet addSubview:doneButton];
        
        UISegmentedControl *cancelButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Cancel"]];
        cancelButton.momentary = YES;
        cancelButton.frame = CGRectMake(10, 7.0f, 60.0f, 30.0f);
        cancelButton.segmentedControlStyle = UISegmentedControlStyleBar;
        cancelButton.tintColor = [UIColor blackColor];
        [cancelButton addTarget:self action:@selector(cancelFilterBtnClicked:) forControlEvents:UIControlEventValueChanged];
        [actionSheet addSubview:cancelButton];
        
    }
    
    //[selectionStates setObject:[NSNumber numberWithBool:YES] forKey:[categoryArray objectAtIndex:1]];
    //[selectionStates setObject:[NSNumber numberWithBool:YES] forKey:[categoryArray objectAtIndex:3]];
    
}

- (void)doneFilterBtnClicked:(id)sender
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        
        [UIView animateWithDuration:0.3
                         animations:^(void){
                             _customView.frame=CGRectMake(0, self.view.frame.size.height, _customView.frame.size.width, _customView.frame.size.height);
                         } completion:^(BOOL finished){
                             [_customView setHidden:TRUE];
                         }];
        
        
    } else {
        [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    }
    [resultArray removeAllObjects];
    for (NSString *row in [selectionStates allKeys]) {
        if ([[selectionStates objectForKey:row] boolValue]) {
            [resultArray addObject:row];
        }
    }
    //DebugLog(@"%@",resultArray);
    NSString *selectedValue = [resultArray componentsJoinedByString:@", "];
    [categoryBtn setTitle:selectedValue forState: UIControlStateNormal];
    [streetTextView becomeFirstResponder];
    
    if([resultArray count] == 0)
    {
        [categoryBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [categoryBtn setTitle:SELECT_CATEGORY_BUTTON_TEXT forState:UIControlStateNormal];
    } else {
        [categoryBtn setTitleColor:BROWN_COLOR forState:UIControlStateNormal];
    }
}

- (void)cancelFilterBtnClicked:(id)sender
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        
        [UIView animateWithDuration:0.3
                         animations:^(void){
                             _customView.frame=CGRectMake(0, self.view.frame.size.height, _customView.frame.size.width, _customView.frame.size.height);
                         } completion:^(BOOL finished){
                             [_customView setHidden:TRUE];
                         }];
        
        
    } else {
        [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    }
}

#pragma mark -
#pragma mark ALPickerView delegate methods

- (NSInteger)numberOfRowsForPickerView:(ALPickerView *)pickerView {
	return [categoryDict.allKeys count];
}

- (NSString *)pickerView:(ALPickerView *)pickerView textForRow:(NSInteger)row {
	return [categoryDict.allKeys objectAtIndex:row];
}

- (BOOL)pickerView:(ALPickerView *)pickerView selectionStateForRow:(NSInteger)row {
	return [[selectionStates objectForKey:[categoryDict.allKeys objectAtIndex:row]] boolValue];
}

- (void)pickerView:(ALPickerView *)pickerView didCheckRow:(NSInteger)row {
	// Check whether all rows are checked or only one
	if (row == -1)
		for (id key in [selectionStates allKeys])
			[selectionStates setObject:[NSNumber numberWithBool:YES] forKey:key];
	else
		[selectionStates setObject:[NSNumber numberWithBool:YES] forKey:[categoryDict.allKeys objectAtIndex:row]];
}

- (void)pickerView:(ALPickerView *)pickerView didUncheckRow:(NSInteger)row {
	// Check whether all rows are unchecked or only one
	if (row == -1)
		for (id key in [selectionStates allKeys])
			[selectionStates setObject:[NSNumber numberWithBool:NO] forKey:key];
	else
		[selectionStates setObject:[NSNumber numberWithBool:NO] forKey:[categoryDict.allKeys objectAtIndex:row]];
}


-(void)getLocationValues
{
    if ([CLLocationManager locationServicesEnabled])
    {
        DebugLog(@"Location services are enabled.");
        [self showProgressHUDWithMessage:@"Please wait ... \n shoplocal is trying to find your location."];
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        [locationManager startUpdatingLocation];
        [self performSelector:@selector(stopUpdatingCoreLocation:) withObject:@"Timed Out" afterDelay:10];
    }
    else
    {
        DebugLog(@"Location services not enabled.");
        UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Location serviecs are disabled on your device, shoplocal needs your location to bring you information from nearby, please enable location services." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [servicesDisabledAlert show];
    }
}

- (void) locationManager:(CLLocationManager *) manager didFailWithError:(NSError *) error
{
    [locationManager stopUpdatingLocation];
    [self hideProgressHUD:YES];
}

- (void)stopUpdatingCoreLocation:(NSString *)state
{
    [self hideProgressHUD:YES];
    [locationManager stopUpdatingLocation];
    [self saveCurrentLocation:[locationManager location]];
}

-(void)saveCurrentLocation:(CLLocation *)lLocation
{
    // If it's not possible to get a location, then return.
	if (!lLocation) {
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                             message:@"Shoplocal is not able to get your current location accurately. Let's check again in some time."
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
        [errorAlert show];
        [self hideProgressHUD:YES];
		return;
	}
    // Configure the new event with information from the location.
    currentLatitude  = lLocation.coordinate.latitude;
    currentLongitude = lLocation.coordinate.longitude;
   // DebugLog(@"%f --- %f",currentLatitude,currentLongitude);
    //DebugLog(@"%@",MAP_STORE(currentLatitude,currentLongitude));
    
    NSString *link = [MAP_STORE(currentLatitude,currentLongitude) stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [mapImageView setImageWithURL:[NSURL URLWithString:link] placeholderImage:[UIImage imageNamed:@"DropAPin.png"] ];
    //maplbl.text = @"Location found. \n\nPlease proceed to next screen.";
    if([self.title isEqualToString:@"Location"] &&  (self.placeId == nil || [self.placeId isEqualToString:@""])) {
        [mapskipBtn setTitle:@"Proceed" forState:UIControlStateNormal];
        [mapskipBtn setTitle:@"Proceed" forState:UIControlStateHighlighted];
        [self performSelector:@selector(mapSkipBtnPressed:) withObject:nil afterDelay:1.0];
        //[self mapSkipBtnPressed:nil];
    }
}

- (void)showProgressHUDWithMessage:(NSString *)message {
    self.progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //self.progressHUD.labelText = message;
    self.progressHUD.detailsLabelText = message;
    self.progressHUD.mode = MBProgressHUDModeIndeterminate;
    [self.view bringSubviewToFront:self.progressHUD];
    [self.progressHUD show:YES];
}

-(void)hideProgressHUD:(BOOL)animated
{
    [self.progressHUD hide:animated];
}

- (void)tapDetected:(UIGestureRecognizer *)sender {
    [self.view endEditing:YES];
    
    
    UIView *view = nil;
    
    if (pageControl.currentPage == 1) {
        CGPoint tapLocation = [sender locationInView:locationScrollView];
        view = [locationScrollView hitTest:tapLocation withEvent:nil];
    }
    if (pageControl.currentPage == 2) {
        CGPoint tapLocation = [sender locationInView:contactScrollView];
        view = [contactScrollView hitTest:tapLocation withEvent:nil];
    }
    if (pageControl.currentPage == 3) {
        CGPoint tapLocation = [sender locationInView:descriptionScrollView];
        view = [descriptionScrollView hitTest:tapLocation withEvent:nil];
    }
    
    if (pageControl.currentPage == 5) {
        if (!addImageDescriptionView.hidden) {
            CGPoint tapLocation = [sender locationInView:self.view];
            UIView *view = [self.view hitTest:tapLocation withEvent:nil];
            if (![view isKindOfClass:[UIButton class]]) {
                [UIView animateWithDuration:0.2 animations:^{
                    addImageDescriptionView.frame = CGRectMake(addImageDescriptionView.frame.origin.x, 58, addImageDescriptionView.frame.size.width, addImageDescriptionView.frame.size.height);
                } completion:^(BOOL finished) {
                }];
            }
        }
        return;
    }
    
    if (view == nil) {
        [self scrollTobottom];
        return;
    }
    if (![view isKindOfClass:[UIButton class]]) {
        [locationScrollView setContentSize: CGSizeMake(locationScrollView.frame.size.width, locationScrollView.frame.size.height - 220)];
        [contactScrollView setContentSize: CGSizeMake(contactScrollView.frame.size.width, contactScrollView.frame.size.height - 220)];
        [descriptionScrollView setContentSize: CGSizeMake(descriptionScrollView.frame.size.width, descriptionScrollView.frame.size.height - 220)];
        [self scrollTobottom];
    }
}

-(void)scrollTobottom
{
    CGPoint bottomOffset = CGPointMake(0, 0);
    [locationScrollView setContentOffset:bottomOffset animated:YES];
    [contactScrollView setContentOffset:bottomOffset animated:YES];
    [descriptionScrollView setContentOffset:bottomOffset animated:YES];
}

- (IBAction)locationInfoBtnPressed:(id)sender {
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Drop a pin if you are at store now.\nThis helps customer to locate you on map."];
    alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
    alertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
    
    [alertView addButtonWithTitle:@"Close"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alert) {
                              DebugLog(@"Close Clicked");
                          }];
    [alertView show];
}

- (IBAction)saveMapLocationBtnPressed:(id)sender {
    [self saveLocationBtnPressed:nil];
}

- (IBAction)descriptionViewSetBtnPressed:(id)sender {
    [addImageDescriptionView setHidden:TRUE];
    [self.view sendSubviewToBack:addImageDescriptionView];
    [self sendAddGalleryImageIntoStoreImageRequest:editedImage];
    addImageDescriptionView.frame = CGRectMake(addImageDescriptionView.frame.origin.x, 58, addImageDescriptionView.frame.size.width, addImageDescriptionView.frame.size.height);
}

- (IBAction)descriptionViewSkipBtnPressed:(id)sender {
    descriptionViewtitleTextView.text = @"";
    [self descriptionViewSetBtnPressed:nil];
}

- (IBAction)pageChange:(id)sender {
    //DebugLog(@"page------->%d",pageControl.currentPage);
    int page = pageControl.currentPage;
    
    
    // update the scroll view to the appropriate page
    CGRect frame = storeScrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    
    [storeScrollView scrollRectToVisible:frame animated:YES];
    [self setViewControllerTitle:pageControl.currentPage];
    //    [UIView animateWithDuration:0.5
    //                          delay:0
    //                        options:UIViewAnimationOptionCurveLinear
    //                     animations:^{ [signupScrollView scrollRectToVisible:frame animated:NO]; }
    //                     completion:NULL];
    //
    //[gridScrollView scrollRectToVisible:frame animated:YES];
    
    // Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
    pageControlUsed = YES;
}


- (IBAction)dayBtnPressed:(UIButton *)sender {
    if([sender isSelected])
        [sender setSelected:FALSE];
    else
        [sender setSelected:TRUE];
}

- (IBAction)droppibBtnPressed:(id)sender {
    if (currentLatitude == 0 || currentLongitude == 0) {
        UIAlertView *droppinalert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Are you at store ?" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Not now", @"I'am at store", nil];
        droppinalert.tag = 1;
        [droppinalert show];
    }else{
        UIAlertView *droppinalert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Do you want to change your location ?" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Yes", @"No", nil];
        droppinalert.tag = 1;
        [droppinalert show];
    }
}

-(void)chooselogoTapDetected:(id)sender{
    UIImage *placeHolderImage = [UIImage imageNamed:@"Add_Logo.png"];
    if (iconImageView.image == nil || [iconImageView.image isEqual:placeHolderImage]) {
        UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Choose your option" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Choose Existing", @"Take Picture", nil];
        servicesDisabledAlert.tag = 2;
        [servicesDisabledAlert show];
    }else{
        UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Choose your option" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Choose Existing", @"Take Picture", @"Remove Existing Logo", nil];
        servicesDisabledAlert.tag = 2;
        [servicesDisabledAlert show];
    }
}

- (IBAction)mapSkipBtnPressed:(id)sender {
    pageControl.currentPage = pageControl.currentPage + 1;
    [self pageChange:nil];
}

- (IBAction)saveLocationBtnPressed:(id)sender {
//    pageControl.currentPage = 2;
//    [self pageChange:nil];
    [self scrollTobottom];
    if([storenameTextField.text isEqualToString:@""] || [areaTextField.text isEqualToString:@""] || [cityTextField.text isEqualToString:@""] ) //|| [pincodeTextField.text isEqualToString:@""]
    {
        [INUserDefaultOperations showAlert:@"Please fill in all fields marked with an asterisk (*)"];
        return;
    } else if([categoryBtn.titleLabel.text isEqualToString:@""] || [categoryBtn.titleLabel.text isEqualToString:SELECT_CATEGORY_BUTTON_TEXT])
    {
        [INUserDefaultOperations showAlert:@"Please select a category."];
        return;
    }
//    else if([pincodeTextField.text length] < 6)
//    {
//        [INUserDefaultOperations showAlert:@"Pincode number must be at least 6 characters in length."];
//    }
    lblstoreName.text = storenameTextField.text;
    if ([IN_APP_DELEGATE networkavailable]) {
        [self sendAddStoreRequest];
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

- (IBAction)saveContactDetailsBtnPressed:(id)sender {
//    pageControl.currentPage = 3;
//    [self pageChange:nil];
    [self scrollTobottom];
    if([storenameTextField.text isEqualToString:@""] || [areaTextField.text isEqualToString:@""] || [cityTextField.text isEqualToString:@""]) //|| [pincodeTextField.text isEqualToString:@""]
    {
        [INUserDefaultOperations showAlert:@"Please fill in all fields marked with an asterisk (*)"];
        return;
    }
    if([landlineTextField.text length] > 0 && [landlineTextField.text length] < 11)
    {
        [INUserDefaultOperations showAlert:@"Landline number must be at least 11 characters in length."];
        return;
    }     else if([landlineTextField.text length] > 0 && [landlineTextField.text length] < 11)
    {
        [INUserDefaultOperations showAlert:@"Landline number must be at least 11 characters in length."];
        return;
    }
    if([mobileNoTextField.text length] > 0 && [mobileNoTextField.text length] < 10)
    {
        [INUserDefaultOperations showAlert:@"Mobile number must be at least 10 characters in length."];
        return;
    }
    if (landlineArray.count > 0) {
        if([[landlineArray objectAtIndex:0] length] > 0 && [[landlineArray objectAtIndex:0] length] < 10)
        {
            [INUserDefaultOperations showAlert:@"Landline number must be at least 11 characters in length."];
            return;
        }
        if (landlineArray.count > 1) {
            if([[landlineArray objectAtIndex:1] length] > 0 && [[landlineArray objectAtIndex:1] length] < 10)
            {
                [INUserDefaultOperations showAlert:@"Landline number must be at least 11 characters in length."];
                return;
            }
        }
    }
    if (mobileArray.count > 0) {
        
        if([[mobileArray objectAtIndex:0] length] > 0 && [[mobileArray objectAtIndex:0] length] < 10)
        {
            [INUserDefaultOperations showAlert:@"Mobile number must be at least 10 characters in length."];
            return;
        }
        if (mobileArray.count > 1) {
            if([[mobileArray objectAtIndex:1] length] > 0 && [[mobileArray objectAtIndex:1] length] < 10)
            {
                [INUserDefaultOperations showAlert:@"Mobile number must be at least 10 characters in length."];
                return;
            }
        }
    }
    
//    int count = 0;
//    if ([mobileNoTextField.text isEqualToString:@""] && [landlineTextField.text isEqualToString:@""])
//    {
//        if ([mobileArray count] > 0 && [[mobileArray objectAtIndex:0] isEqualToString:@""])
//            count++;
//        
//        
//        if ([mobileArray count] > 1 && [[mobileArray objectAtIndex:1] isEqualToString:@""])
//            count++;
//        
//        
//        if ([landlineArray count] > 0 && [[landlineArray objectAtIndex:0] isEqualToString:@""])
//            count++;
//        
//        
//        if ([landlineArray count] > 1 && [[landlineArray objectAtIndex:1] isEqualToString:@""])
//            count++;
//    }
//    
//    if (count > 0) {
//        [INUserDefaultOperations showAlert:@"Please provide atleast one contact number."];
//    }  else {
//        if ([IN_APP_DELEGATE networkavailable]) {
//             [self sendAddContactDetailsRequest];
//        } else {
//            [INUserDefaultOperations showOfflineAlert];
//        }
//    }
    int landlineCount = landlineArray.count;
    for (NSString *str in landlineArray) {
        if (![str isEqualToString:@""]) {
            landlineCount--;
        }
    }
    int mobileCount = mobileArray.count;
    for (NSString *str in mobileArray) {
        if (![str isEqualToString:@""]) {
            mobileCount--;
        }
    }
    
    DebugLog(@"%d landlineCount %d ----%d mobileCount %d",landlineArray.count,landlineCount,mobileArray.count,mobileCount);
    if ([mobileNoTextField.text isEqualToString:@""] && [landlineTextField.text isEqualToString:@""] && landlineArray.count == landlineCount && mobileArray.count == mobileCount) {
            [INUserDefaultOperations showAlert:@"Please provide atleast one contact number."];
    }else if ([mobileNoTextField.text isEqualToString:@""] && [landlineTextField.text isEqualToString:@""] && landlineArray.count == 0 && mobileArray.count == 0) {
        [INUserDefaultOperations showAlert:@"Please provide atleast one contact number."];
    }
    else {
        if ([IN_APP_DELEGATE networkavailable]) {
            [self sendAddContactDetailsRequest];
        } else {
            [INUserDefaultOperations showOfflineAlert];
        }
    }
}

- (IBAction)contactdetailsSkipBtnPressed:(id)sender {
    pageControl.currentPage = pageControl.currentPage + 1;
    [self pageChange:nil];
}


- (IBAction)descriptionSaveBtnPressed:(id)sender {
//    pageControl.currentPage = 4;
//    [self pageChange:nil];
    [self scrollTobottom];
    if([storenameTextField.text isEqualToString:@""] || [areaTextField.text isEqualToString:@""] || [cityTextField.text isEqualToString:@""] )//|| [pincodeTextField.text isEqualToString:@""]
    {
        [INUserDefaultOperations showAlert:@"Please fill in all fields marked with an asterisk (*)"];
    }
//    else if([pincodeTextField.text length] < 6)
//    {
//        [INUserDefaultOperations showAlert:@"Pincode number must be at least 6 characters in length."];
//    }
//    else if([mobileNoTextField.text isEqualToString:@""])
//    {
//        [INUserDefaultOperations showAlert:@"Please fill in all fields marked with an asterisk (*)"];
//    }
//    else if([mobileNoTextField.text length] < 10)
//    {
//        [INUserDefaultOperations showAlert:@"Mobile number must be at least 10 characters in length."];
//    }
    else {
        lblstoreDescription.text = descriptionTextView.text;
        if ([IN_APP_DELEGATE networkavailable]) {
             [self sendAddDescriptionRequest];
        } else {
            [INUserDefaultOperations showOfflineAlert];
        }
    }
}

- (IBAction)descriptionSkipBtnPressed:(id)sender {
    [self scrollTobottom];
    pageControl.currentPage = pageControl.currentPage + 1;
    [self pageChange:nil];
}

- (IBAction)uploadlogoBtnPressed:(id)sender {
    UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Choose your option" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Choose Existing", @"Take Picture", nil];
    servicesDisabledAlert.tag = 2;
    [servicesDisabledAlert show];
}

- (IBAction)daysBtnPressed:(id)sender {
//    pageControl.currentPage = 5;
//    [self pageChange:nil];
    if([storenameTextField.text isEqualToString:@""] || [areaTextField.text isEqualToString:@""] || [cityTextField.text isEqualToString:@""]) //|| [pincodeTextField.text isEqualToString:@""]
    {
        [INUserDefaultOperations showAlert:@"Please fill in all fields marked with an asterisk (*)"];
        return;
    }
    if([landlineTextField.text length] > 0 && [landlineTextField.text length] < 11)
    {
        [INUserDefaultOperations showAlert:@"Landline number must be at least 11 characters in length."];
        return;
    }
    if ( [fromDateBtn.titleLabel.text isEqualToString:@"00:00"] && ![toDateBtn.titleLabel.text isEqualToString:@"00:00"])
    {
        [INUserDefaultOperations showAlert:@"Please select valid open time."];
        return;
    }
    if (![fromDateBtn.titleLabel.text isEqualToString:@"00:00"] && [toDateBtn.titleLabel.text isEqualToString:@"00:00"])
    {
        [INUserDefaultOperations showAlert:@"Please select valid close time."];
        return;
    }
//    else if([faxNoTextField.text length] > 0 && [faxNoTextField.text length] < 11)
//    {
//        [INUserDefaultOperations showAlert:@"Fax number must be at least 11 characters in length."];
//    }
//    else if([tollfreeTextField.text length] > 0 && [tollfreeTextField.text length] < 11)
//    {
//        [INUserDefaultOperations showAlert:@"Toll free number must be at exactly 11 characters in length."];
//    }
    if ([IN_APP_DELEGATE networkavailable]) {
        [self sendAddLogoRequest];
        if((placeId != nil) && ![placeId isEqualToString:@""])
        {
            [self sendPlaceTimeStoreRequest];
        }
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

- (IBAction)storeLogoSkipBtnPressed:(id)sender {
    pageControl.currentPage = pageControl.currentPage + 1;
    [self pageChange:nil];
}

- (IBAction)doneBtnPressed:(id)sender {
//    if(updated == 1) {
//        [INUserDefaultOperations setRefreshState:TRUE];
//        //[[NSNotificationCenter defaultCenter] postNotificationName:IN_MERCHANT_RELOAD_MYPLACES_NOTIFICATION object:nil];
//    }
    if(self.placeId != nil) {
        [[NSNotificationCenter defaultCenter] postNotificationName:IN_MERCHANT_SHOW_OFFER_NOTIFICATION object:nil];
    }
    [self performSelector:@selector(popAddUpdateController) withObject:nil afterDelay:0.0];
}

- (IBAction)categoryBtnPressed:(id)sender {
//    [ActionSheetStringPicker showPickerWithTitle:@"Select Category" rows:self.categoryArray initialSelection:self.selectedIndex target:self successAction:@selector(selectCategory:element:) cancelAction:@selector(actionPickerCancelled:) origin:sender];
 ///////////////////Multi Select Picker//////////////
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        [_customView setHidden:FALSE];
        [UIView animateWithDuration:0.3
                         animations:^(void){
                             _customView.frame=CGRectMake(0, self.view.frame.size.height - 250, _customView.frame.size.width, _customView.frame.size.height);
                         } completion:^(BOOL finished){
                             
                         }];
        
    } else {
        [actionSheet showInView:self.view];
        [actionSheet setBounds:CGRectMake(0, 0, 320, 485)];
    }
///////////////////Multi Select Picker//////////////
}

- (IBAction)setFromDate:(id)sender {
    _fromactionSheetPicker = [[ActionSheetDatePicker alloc] initWithTitle:@"" datePickerMode:UIDatePickerModeTime selectedDate:self.fromselectedDate target:self action:@selector(fromdateSelected:element:) origin:sender];
    //    [self.actionSheetPicker addCustomButtonWithTitle:@"Yesterday" value:[[NSDate date] TC_dateByAddingCalendarUnits:NSDayCalendarUnit amount:-1]];
    //[self.actionSheetPicker addCustomButtonWithTitle:@"Today" value:[NSDate date]];
    //[self.actionSheetPicker addCustomButtonWithTitle:@"Tommorow" value:[[NSDate date] TC_dateByAddingCalendarUnits:NSDayCalendarUnit amount:+1]];
    self.fromactionSheetPicker.hideCancel = NO;
    [self.fromactionSheetPicker showActionSheetPicker];
}

- (IBAction)setToDate:(id)sender {
    _toactionSheetPicker = [[ActionSheetDatePicker alloc] initWithTitle:@"" datePickerMode:UIDatePickerModeTime selectedDate:self.toselectedDate target:self action:@selector(todateSelected:element:) origin:sender];
    //    [self.actionSheetPicker addCustomButtonWithTitle:@"Yesterday" value:[[NSDate date] TC_dateByAddingCalendarUnits:NSDayCalendarUnit amount:-1]];
    //[self.actionSheetPicker addCustomButtonWithTitle:@"Today" value:[NSDate date]];
    //[self.actionSheetPicker addCustomButtonWithTitle:@"Tommorow" value:[[NSDate date] TC_dateByAddingCalendarUnits:NSDayCalendarUnit amount:+1]];
    self.toactionSheetPicker.hideCancel = NO;
    [self.toactionSheetPicker showActionSheetPicker];
}

#pragma mark - Actionsheet Implementation

- (void)selectCategory:(NSNumber *)lselectedIndex element:(id)element {
    self.selectedIndex = [lselectedIndex intValue];
    
    //may have originated from textField or barButtonItem, use an IBOutlet instead of element
    NSString *selectedValue = [self.categoryDict.allKeys objectAtIndex:self.selectedIndex];
    [categoryBtn setTitle:selectedValue forState: UIControlStateNormal];
    [streetTextView becomeFirstResponder];
}

- (void)fromdateSelected:(NSDate *)selectedDate element:(id)element {
    //self.selectedDate = selectedDate;
    
    //may have originated from textField or barButtonItem, use an IBOutlet instead of element
    //self.dateTextField.text = [self.selectedDate description];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSString *fromString = [dateFormatter stringFromDate:selectedDate];
    fromDatelbl.text = fromString;
    //DebugLog(@"MyDate is: %@", fromString);
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"hh:mm a"];
    NSString *fromString1 = [dateFormatter1 stringFromDate:selectedDate];
    [fromDateBtn setTitle:fromString1 forState: UIControlStateNormal];
    [fromDateBtn setTitle:fromString1 forState: UIControlStateHighlighted];

}

- (void)todateSelected:(NSDate *)selectedDate element:(id)element {
    //self.selectedDate = selectedDate;
    
    //may have originated from textField or barButtonItem, use an IBOutlet instead of element
    //self.dateTextField.text = [self.selectedDate description];
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSString *toString = [dateFormatter stringFromDate:selectedDate];
    toDatelbl.text = toString;
    //DebugLog(@"MyDate is: %@", toString);
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"hh:mm a"];
    NSString *toString1 = [dateFormatter1 stringFromDate:selectedDate];
    [toDateBtn setTitle:toString1 forState: UIControlStateNormal];
    [toDateBtn setTitle:toString1 forState: UIControlStateHighlighted];
}

-(NSMutableArray *)getCategoryValues
{
//    switch (self.selectedIndex) {
//        case 0:
//        {
//            return [NSArray arrayWithObjects:@"1",nil];
//        }
//        break;
//        
//        case 1:
//        {
//            return [NSArray arrayWithObjects:@"2",nil];
//        }
//            break;
//        
//        case 2:
//        {
//            return [NSArray arrayWithObjects:@"3",nil];
//        }
//            break;
//            
//        case 3:
//        {
//            return [NSArray arrayWithObjects:@"4",nil];
//        }
//            break;
//            
//        default:
//            return [NSArray arrayWithObjects:@"1",nil];
//            break;
//    }
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (NSString *key in resultArray) {
        [array addObject:[self.categoryDict objectForKey:key]];
    }
    return array;
}

//-(void)setCategoryValues:(NSString *)lselectedIndex
//{
//    self.selectedIndex = [lselectedIndex intValue] - 1;
//    
//    //may have originated from textField or barButtonItem, use an IBOutlet instead of element
//    NSString *selectedValue = [self.categoryDict.allKeys objectAtIndex:self.selectedIndex];
//    [categoryBtn setTitle:selectedValue forState: UIControlStateNormal];
//}

-(void)setCategoryValues:(NSArray *)larray
{
    //DebugLog(@"selected catg array %@",larray);
    //DebugLog(@"selected catg dict %@",self.categoryDict);
    [resultArray removeAllObjects];
    for (NSString *value in larray){
        //DebugLog(@"va-->%@",value);
        if(value != nil) {
            if([[self.categoryDict allKeysForObject:value]  count] > 0)
            {
                NSString *key = [[self.categoryDict allKeysForObject:value] objectAtIndex:0];
                 //DebugLog(@"key-->%@",key);
                 if(key != nil) {
                    [selectionStates setObject:[NSNumber numberWithBool:YES] forKey:key];
                    [resultArray addObject:key];
                 }
            }
        }
    }
    NSString *selectedValue = [resultArray componentsJoinedByString:@", "];
    if(![selectedValue isEqualToString:@""] || ![selectedValue isEqualToString:SELECT_CATEGORY_BUTTON_TEXT])
    {
        [categoryBtn setTitleColor:BROWN_COLOR forState:UIControlStateNormal];
    }
    [categoryBtn setTitle:selectedValue forState: UIControlStateNormal];
    [pickerView reloadAllComponents];
}

-(void)sendStoreCategoryRequest
{
    //[hud show:YES];
    //[CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    //DebugLog(@"%@",SEARCH_CATEGORY([IN_APP_DELEGATE urlEncode:@"shoplocal"]));
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:SEARCH_CATEGORY([IN_APP_DELEGATE urlEncode:@"shoplocal"])]];
    
    //DebugLog(@"search=%@",urlRequest.URL);
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        //[hud hide:YES];
                                                        //[CommonCallback hideProgressHud];
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        //DebugLog(@"%@",self.splashJson);
                                                        if(self.splashJson  != nil) {
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                if (categoryDict == nil) {
                                                                    categoryDict = [[NSMutableDictionary alloc] init];
                                                                }else{
                                                                    [categoryDict removeAllObjects];
                                                                }
                                                                //DebugLog(@"1)categoryDict -%@-",categoryDict);

                                                                NSArray *jsonArray = [self.splashJson  objectForKey:@"data"];
                                                                //DebugLog(@"%@",jsonArray);
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *objdict = [jsonArray  objectAtIndex:index];
                                                                      //DebugLog(@"%@",objdict);
                                                                    [categoryDict setObject:[objdict valueForKey:@"id"] forKey:[objdict valueForKey:@"label"]];
                                                                }
                                                            } else {
                                                                if([self.splashJson  objectForKey:@"message"] != [NSNull null])
                                                                    [INUserDefaultOperations showAlert:[self.splashJson  objectForKey:@"message"]];
                                                            }
                                                        }
                                                        //DebugLog(@"2)categoryDict -%@-",categoryDict);
                                                        [pickerView reloadAllComponents];
                                                        
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        //[hud hide:YES];
                                                        //[CommonCallback hideProgressHud];
                                                        [INUserDefaultOperations showAlert:[NSString stringWithFormat:@"%@",[error localizedDescription]]];
                                                    }];
    
    
    
    [operation start];
}


-(void)sendStoreGalleryRequest
{
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    //DebugLog(@"%@",STORE_GALLERY([self.placeId intValue]));
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:STORE_GALLERY([self.placeId intValue])]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        //DebugLog(@"%@", [self.splashJson description]);
                                                        if(self.splashJson  != nil) {
                                                            [galleryArray removeAllObjects];
                                                            [galleryArray addObject:@"placeholder"];
                                                            
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSArray* jsonArray = [self.splashJson  objectForKey:@"data"];
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *objdict = [jsonArray  objectAtIndex:index];
                                                                    //DebugLog(@"\n\n%@\n\n",objdict);
                                                                    INGalleryObj *tempgalleryObj = [[INGalleryObj alloc] init];
                                                                    tempgalleryObj.galleryID = [objdict objectForKey:@"id"];
                                                                    tempgalleryObj.place_id = [objdict objectForKey:@"place_id"];
                                                                    tempgalleryObj.title = [objdict objectForKey:@"title"];
                                                                    tempgalleryObj.image_url = [objdict objectForKey:@"image_url"];
                                                                    tempgalleryObj.thumb_url = [objdict objectForKey:@"thumb_url"];
                                                                    tempgalleryObj.image_date = [objdict objectForKey:@"image_date"];
                                                                    tempgalleryObj.title = [objdict objectForKey:@"title"];
                                                                    [galleryArray addObject:tempgalleryObj];
                                                                    tempgalleryObj = nil;
                                                                }
                                                            }
                                                            //DebugLog(@"galleryArray %@",galleryArray);
                                                            //if([galleryArray count] > 0){
                                                                [self.gridView reloadData];
                                                            //}
                                                        }
                                                        
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                        [av show];
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                    }];
    
    
    
    [operation start];
}

-(void)sendAddGalleryImageIntoStoreImageRequest:(UIImage *)pickerimage
{
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *postparams = [[NSMutableDictionary alloc] init];
    [postparams setObject:[NSString stringWithFormat:@"%d",[self.placeId intValue]] forKey:@"place_id"];
    [postparams setObject:[INUserDefaultOperations getMerchantAuthId] forKey:@"user_id"];
    [postparams setObject:[INUserDefaultOperations getMerchantAuthCode] forKey:@"auth_id"];
    NSData *dataObj = UIImageJPEGRepresentation(pickerimage,0.75);
    NSString *image = [IN_APP_DELEGATE base64forData:dataObj];
    [postparams setObject:image forKey:@"userfile"];
    [postparams setObject:@"gallery.jpg" forKey:@"filename"];
    [postparams setObject:@"image/jpeg" forKey:@"filetype"];
    
    if ([descriptionViewtitleTextView.text length] > 0) {
        [postparams setObject:descriptionViewtitleTextView.text forKey:@"title"];
    }
    
    //DebugLog(@"----%@------",postparams);
    [httpClient postPath:ADD_GALLERY parameters:postparams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        //DebugLog(@"json afnetworking ->%@",json);
        if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"true"])
        {
            [INEventLogger logEvent:@"Store_ImageAdd"];
            [INUserDefaultOperations showAlert:[json  objectForKey:@"message"]];
            [self sendStoreGalleryRequest];
            editedImage = nil;
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        editedImage = nil;
    }];
    
}

-(void)sendDeleteGalleryImageFromStoreRequest:(NSString *)galleryId
{
    [hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    //DebugLog(@"delete = %@",DELETE_GALLERY(galleryId));
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    [httpClient setDefaultHeader:@"user_id" value:[INUserDefaultOperations getMerchantAuthId]];
    [httpClient setDefaultHeader:@"auth_id" value:[INUserDefaultOperations getMerchantAuthCode]];
    [httpClient deletePath:DELETE_GALLERY(galleryId) parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        //DebugLog(@"json ->%@",json);
        if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"true"])
        {
            [INUserDefaultOperations showAlert:[json  objectForKey:@"message"]];
            [self sendStoreGalleryRequest];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}

-(void)addNewImage
{
    UIAlertView *addImageToGalleryAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Choose your option" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Choose Existing", @"Take Picture", nil];
    addImageToGalleryAlert.tag = 3;
    [addImageToGalleryAlert show];
}

#pragma mark - Image Picker
- (void)useCamera:(int)itag
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker =
        [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        //[imagePicker.navigationBar setTintColor:BROWN_COLOR];
        imagePicker.sourceType =
        UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = [NSArray arrayWithObjects:
                                  (NSString *) kUTTypeImage,
                                  nil];
        imagePicker.allowsEditing = YES;
         imgPickerTag = itag;
        [self presentViewController:imagePicker animated:YES completion:nil];
        newMedia = YES;
    }
}

- (void)useCameraRoll:(int)itag
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    {
        UIImagePickerController *imagePicker =
        [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        //[imagePicker.navigationBar setTintColor:BROWN_COLOR];
        imagePicker.sourceType =
        UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.mediaTypes = [NSArray arrayWithObjects:
                                  (NSString *) kUTTypeImage,
                                  nil];
        imagePicker.allowsEditing = YES;
         imgPickerTag = itag;
        [self presentViewController:imagePicker animated:YES completion:nil];
        newMedia = NO;
    }
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info
                           objectForKey:UIImagePickerControllerMediaType];
//    [self dismissViewControllerAnimated:YES completion:nil];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
//        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
        
        CLImageEditor *editor = [[CLImageEditor alloc] initWithImage:image];
        editor.delegate = self;
        /* @"CLFilterTool",
         @"CLAdjustmentTool",
         @"CLEffectTool",
         @"CLBlurTool",
         @"CLClippingTool",
         @"CLRotateTool",
         @"CLToneCurveTool",*/
        
        CLImageToolInfo *Filtertool = [editor.toolInfo subToolInfoWithToolName:@"CLFilterTool" recursive:NO];
        Filtertool.available = NO;
        CLImageToolInfo *Blurtool = [editor.toolInfo subToolInfoWithToolName:@"CLBlurTool" recursive:NO];
        Blurtool.available = NO;
        CLImageToolInfo *Curvetool = [editor.toolInfo subToolInfoWithToolName:@"CLToneCurveTool" recursive:NO];
        Curvetool.available = NO;
        CLImageToolInfo *ClippingTool = [editor.toolInfo subToolInfoWithToolName:@"CLClippingTool" recursive:NO];
        ClippingTool.available = NO;
        
        CLImageToolInfo *AdjustmentTool = [editor.toolInfo subToolInfoWithToolName:@"CLAdjustmentTool" recursive:NO];
        AdjustmentTool.dockedNumber = 2;
        CLImageToolInfo *EffectTool = [editor.toolInfo subToolInfoWithToolName:@"CLEffectTool" recursive:NO];
        EffectTool.dockedNumber = 3;
//        CLImageToolInfo *ClippingTool = [editor.toolInfo subToolInfoWithToolName:@"CLClippingTool" recursive:NO];
//        ClippingTool.dockedNumber = 1;
        CLImageToolInfo *RotateTool = [editor.toolInfo subToolInfoWithToolName:@"CLRotateTool" recursive:NO];
        RotateTool.dockedNumber = 1;
        [picker pushViewController:editor animated:YES];
    }
}

-(void)image:(UIImage *)image finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Save failed"
                              message: @"Failed to save image"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark- CLImageEditor delegate

- (void)imageEditor:(CLImageEditor *)editor didFinishEdittingWithImage:(UIImage *)leditedImage
{
    [editor dismissViewControllerAnimated:YES completion:nil];
    //DebugLog(@"addStore : saveToalbum %@",leditedImage);
    //DebugLog(@"addStore : imgPickerTag %d",imgPickerTag);
    if (imgPickerTag == 2) {
        iconImageView.image = leditedImage;
        isImgFrmShopLocalGallery = 0;
        //        [self sendAddLogoRequest];
    }else if (imgPickerTag == 3) {
        editedImage = leditedImage;
        descriptionViewtitleTextView.text = @"";
        [self.view bringSubviewToFront:addImageDescriptionView];
        [addImageDescriptionView setHidden:FALSE];
        //add animation code here
        [CommonCallback viewtransitionInCompletion:addImageDescriptionView completion:^{
            [CommonCallback viewtransitionOutCompletion:addImageDescriptionView completion:nil];
        }];
    }
    imgPickerTag = 0;
    if (newMedia)
        UIImageWriteToSavedPhotosAlbum(leditedImage,
                                       self,
                                       @selector(image:finishedSavingWithError:contextInfo:),
                                       nil);
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1) //drop pin
    {
        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        if([title isEqualToString:@"I'am at store"] || [title isEqualToString:@"Yes"])
        {
            //DebugLog(@"I'am at Store was selected.");
            [self getLocationValues];
            [INEventLogger logEvent:@"Store_DropPin"];
        }
    } else if (alertView.tag == 2 || alertView.tag == 3){
        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        if([title isEqualToString:@"Choose Existing"])
        {
            [self useCameraRoll:alertView.tag];
            if (alertView.tag == 2)
            {
                NSDictionary *editstoreParams = [NSDictionary dictionaryWithObjectsAndKeys:@"Gallery",@"Source", nil];
                [INEventLogger logEvent:@"Store_LogoAdd" withParams:editstoreParams];
            }
            if (alertView.tag == 3)
            {
                NSDictionary *editstoreParams = [NSDictionary dictionaryWithObjectsAndKeys:@"Gallery",@"Source", nil];
                [INEventLogger logEvent:@"Store_ImageAdd" withParams:editstoreParams];
            }
        } else if([title isEqualToString:@"Take Picture"])
        {
            [self useCamera:alertView.tag];
            if (alertView.tag == 2)
            {
                NSDictionary *editstoreParams = [NSDictionary dictionaryWithObjectsAndKeys:@"Camera",@"Source", nil];
                [INEventLogger logEvent:@"Store_LogoAdd" withParams:editstoreParams];
            }
            if (alertView.tag == 3)
            {
                NSDictionary *editstoreParams = [NSDictionary dictionaryWithObjectsAndKeys:@"Camera",@"Source", nil];
                [INEventLogger logEvent:@"Store_ImageAdd" withParams:editstoreParams];
            }
        }
        else if([title isEqualToString:@"Choose from Shoplocal"])
        {
            //DebugLog(@"yessssss");
            if (alertView.tag == 2)
            {
            NSDictionary *editstoreParams = [NSDictionary dictionaryWithObjectsAndKeys:@"Shoplocal",@"Source", nil];
                [INEventLogger logEvent:@"Store_LogoAdd" withParams:editstoreParams];
            }
            if (alertView.tag == 3)
            {
                NSDictionary *editstoreParams = [NSDictionary dictionaryWithObjectsAndKeys:@"Shoplocal",@"Source", nil];
                [INEventLogger logEvent:@"Store_ImageAdd" withParams:editstoreParams];
            }

            INShoplocalImageViewController *shoplocalImgController = [[INShoplocalImageViewController alloc] initWithNibName:@"INShoplocalImageViewController" bundle:nil] ;
            shoplocalImgController.title = @"Choose Image";
            shoplocalImgController.delegate = self;
            UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:shoplocalImgController];
            UIButton *cancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
            [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
            cancelButton.titleLabel.font = [UIFont systemFontOfSize:15.0];
            //[cancelButton setImage:[UIImage imageNamed:@"switch.png"] forState:UIControlStateNormal];
            [cancelButton addTarget:self action:@selector(cancelImageController) forControlEvents:UIControlEventTouchUpInside];
            [cancelButton setFrame:CGRectMake(250, 5, 60, 32)];
            [navBar.navigationBar addSubview:cancelButton];
            [self presentViewController:navBar animated:YES completion:nil];
        }
        else if([title isEqualToString:@"Remove Existing Logo"])
        {
            UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Do you want to remove this Logo ?" delegate:self cancelButtonTitle:nil otherButtonTitles:@"NO", @"Yes", nil];
            servicesDisabledAlert.tag = 5;
            [servicesDisabledAlert show];
        }
    }  else if (alertView.tag == 4) {
        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        if([title isEqualToString:@"Ok"])
        {
//            [INUserDefaultOperations setRefreshState:TRUE];
//            if(updated == 1) {
//                //[[NSNotificationCenter defaultCenter] postNotificationName:IN_MERCHANT_RELOAD_MYPLACES_NOTIFICATION object:nil];
//            }
            [self performSelector:@selector(popAddUpdateController) withObject:nil afterDelay:0.0];
        }
    }   else if (alertView.tag == 5) {
        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        if([title isEqualToString:@"Yes"])
        {
            iconImageView.image = [UIImage imageNamed:@"Add_Logo.png"];
            isImgFrmShopLocalGallery = 0;
        }
        
    }
//    else if (alertView.tag == 6) //Hide Store
//    {                                    
//        
//        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
//        if([title isEqualToString:@"Yes"])
//        {
//           [self sendHideStoreRequest:@"1"];
//        }
//        
//    }
//    else if (alertView.tag == 7) //Delete Store
//    {
//        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
//        if([title isEqualToString:@"Yes"])
//        {
//             [self sendDeleteStoreRequest];
//        }
//        
//    }
}
- (void)cancelImageController
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

#pragma ShoplocalImageViewControllerDelegate method
-(void)saveImagePath:(NSString *)image
{
    //DebugLog(@"link-%@",THUMBNAIL_LINK(image));
    if(image != nil && ![image isEqualToString:@""])
    {
        isImgFrmShopLocalGallery = 1;
        [iconImageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(image)] placeholderImage:[UIImage imageNamed:@"Add_Logo.png"]];
        shoplocalImage = THUMBNAIL_LINK(image);
    }
}

#pragma scrollViewDelegates
- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    if (pageControlUsed)
    {
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }
    CGFloat pageWidth = storeScrollView.frame.size.width;
    int page = floor((storeScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if(pageControl.currentPage != page) {
        pageControl.currentPage = page;
        [self setViewControllerTitle:pageControl.currentPage];
        [self.view endEditing:YES];
    }
}

// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
    if (pageControl.currentPage == 4) {
        lblstoreName.text = storenameTextField.text;
        lblstoreDescription.text = descriptionTextView.text;
    }
}

-(void)sendStoreDetailRequest
{
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    //DebugLog(@"%@",STORE_DETAILS(self.placeId));
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:STORE_DETAILS(self.placeId)]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setValue:[INUserDefaultOperations getMerchantAuthId] forHTTPHeaderField:@"user_id"];
    [urlRequest setValue:[INUserDefaultOperations getMerchantAuthCode] forHTTPHeaderField:@"auth_id"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        //DebugLog(@"-%@-", self.splashJson);
                                                        if(self.splashJson  != nil) {
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSArray* jsonArray = [self.splashJson  objectForKey:@"data"];
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *objdict = [jsonArray  objectAtIndex:index];
                                                                    [CommonCallback setTextFieldPropertiesWithBorder:storenameTextField text:[objdict objectForKey:@"name"]];
                                                                    [CommonCallback setTextFieldPropertiesWithBorder:areaTextField text:[objdict objectForKey:@"area"]];
                                                                    [CommonCallback setTextFieldPropertiesWithBorder:cityTextField text:[objdict objectForKey:@"city"]];
                                                                    [CommonCallback setTextFieldPropertiesWithBorder:pincodeTextField text:[objdict objectForKey:@"pincode"]];
                                                                    [CommonCallback setTextFieldPropertiesWithBorder:streetTextView text:[objdict objectForKey:@"street"]];
                                                                    [CommonCallback setTextFieldPropertiesWithBorder:landlineTextField text:[objdict objectForKey:@"tel_no1"]];
                                                                    if([objdict objectForKey:@"mob_no1"] != [NSNull null] && ![[objdict objectForKey:@"mob_no1"] isEqualToString:@""] && ([[objdict objectForKey:@"mob_no1"] length] > 3))
                                                                    {
                                                                        [CommonCallback setTextFieldPropertiesWithBorder:mobileNoTextField text:[[objdict objectForKey:@"mob_no1"] substringFromIndex:[[INUserDefaultOperations getMerchantCountryCode] length]]];
                                                                        
                                                                    }
                                                                    if([objdict objectForKey:@"mob_no2"] != (NSString *)[NSNull null] && ![[objdict objectForKey:@"mob_no2"] isEqualToString:@""])
                                                                    {
                                                                        [mobileArray addObject:[[objdict objectForKey:@"mob_no2"] substringFromIndex:[[INUserDefaultOperations getMerchantCountryCode] length]]];
                                                                    }
                                                                    if([objdict objectForKey:@"mob_no3"] != (NSString *)[NSNull null] && ![[objdict objectForKey:@"mob_no3"] isEqualToString:@""])
                                                                    {
                                                                        [mobileArray addObject:[[objdict objectForKey:@"mob_no3"] substringFromIndex:[[INUserDefaultOperations getMerchantCountryCode] length]]];
                                                                    }
                                                                    
                                                                    if([objdict objectForKey:@"tel_no2"] != (NSString *)[NSNull null] && ![[objdict objectForKey:@"tel_no2"] isEqualToString:@""])
                                                                    {
                                                                        [landlineArray addObject:[objdict objectForKey:@"tel_no2"]];
                                                                    }
                                                                    if([objdict objectForKey:@"tel_no3"] != (NSString *)[NSNull null] && ![[objdict objectForKey:@"tel_no3"] isEqualToString:@""])
                                                                    {
                                                                        [landlineArray addObject:[objdict objectForKey:@"tel_no3"]];
                                                                    }
                                                                    
                                                                    if (mobileArray.count > 0) {
                                                                        [mobileNumberTableView setHidden:FALSE];
                                                                        [self reloadMobileTableView];
                                                                    }
                                                                    if (landlineArray.count > 0) {
                                                                        [LandlineNumberTableView setHidden:FALSE];
                                                                        [self reloadLandlineTableView];
                                                                    }
                                                                    
                                                                    //[CommonCallback setTextFieldPropertiesWithBorder:faxNoTextField text:[objdict objectForKey:@"fax_no1"]];
//                                                                    [CommonCallback setTextFieldPropertiesWithBorder:tollfreeTextField text:[objdict objectForKey:@"toll_free_no1"]];
                                                                    [CommonCallback setTextFieldPropertiesWithBorder:emailTextField text:[objdict objectForKey:@"email"]];
                                                                    [CommonCallback setTextFieldPropertiesWithBorder:websiteTextField text:[objdict objectForKey:@"website"]];
                                                                    [CommonCallback setTextFieldPropertiesWithBorder:txtFbUrl text:[objdict objectForKey:@"facebook_url"]];
                                                                    [CommonCallback setTextFieldPropertiesWithBorder:txtTwitterUrl text:[objdict objectForKey:@"twitter_url"]];
                                                                    
                                                                    [CommonCallback setTextFieldPropertiesWithBorder:streetTextView text:[objdict objectForKey:@"street"]];
                                                                    [CommonCallback setTextFieldPropertiesWithBorder:landmarkTextView text:[objdict objectForKey:@"landmark"]];
                                                                    descriptionTextView.text = [objdict objectForKey:@"description"];
                                                                    if([[objdict objectForKey:@"description"] isEqualToString:@""])
                                                                    {
                                                                        textviewplaceholderLabel.hidden = FALSE;
                                                                    } else {
                                                                        textviewplaceholderLabel.hidden = TRUE;
                                                                    }
                                                                    if([objdict objectForKey:@"latitude"] != [NSNull null] && ![[objdict objectForKey:@"latitude"] isEqualToString:@"0"] && [objdict objectForKey:@"longitude"] != [NSNull null] && ![[objdict objectForKey:@"longitude"] isEqualToString:@"0"])
                                                                    {
                                                                        currentLatitude = [[objdict objectForKey:@"latitude"] doubleValue];
                                                                        currentLongitude = [[objdict objectForKey:@"longitude"] doubleValue];
                                                                        DebugLog(@"%f --- %f",currentLatitude,currentLongitude);
                                                                        NSString *link = [MAP_STORE(currentLatitude,currentLongitude) stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                                                                        [mapImageView setImageWithURL:[NSURL URLWithString:link] placeholderImage:[UIImage imageNamed:@"DropAPin.png"] ];
                                                                        //maplbl.text = @"Location found. \n\nPlease proceed to next screen.";
//                                                                        [mapskipBtn setTitle:@"Proceed" forState:UIControlStateNormal];
//                                                                        [mapskipBtn setTitle:@"Proceed" forState:UIControlStateHighlighted];
                                                                    }
                                                                    if([objdict objectForKey:@"image_url"] != [NSNull null] && ![[objdict objectForKey:@"image_url"] isEqualToString:@""])
                                                                    {
                                                                        [iconImageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK([objdict objectForKey:@"image_url"])] placeholderImage:[UIImage imageNamed:@"Add_Logo.png"]];
                                                                    }
                                                                    if([objdict objectForKey:@"categories"] != [NSNull null])
                                                                    {
                                                                        NSArray *catArray = [objdict objectForKey:@"categories"];
                                                                        //DebugLog(@"%@",catArray);
                                                                        [self setCategoryValues:catArray];
                                                                    }
                                                                    if([objdict objectForKey:@"time"] != [NSNull null])
                                                                    {
                                                                        NSArray *timeArray = [objdict objectForKey:@"time"];
                                                                        //DebugLog(@"%@",timeArray);
                                                                        NSDictionary *timedict = [timeArray  objectAtIndex:0];
                                                                        [self setStoreTimeValues:timedict];
                                                                        for (int index=0; index<[timeArray count]; index++) {
                                                                            NSDictionary *objdict = [timeArray  objectAtIndex:index];
                                                                            [self setStoreDayValues:objdict];
                                                                        }
                                                                    }
                                                                    
                                                                     if([objdict objectForKey:@"place_status"] != [NSNull null])
                                                                     {
                                                                         place_status = [objdict objectForKey:@"place_status"];
                                                                         //DebugLog(@"place status --> %@", place_status);

                                                                     }
                                                                }

                                                            } else {
                                                                if(self.splashJson != nil && [[self.splashJson  objectForKey:@"success"] isEqualToString:@"false"]) {
                                                                    if([self.splashJson  objectForKey:@"code"] != nil &&  [[self.splashJson  objectForKey:@"code"] isEqualToString:MERCHANT_INVALID_USER_AUTH_CODE])
                                                                    {
                                                                        if([INUserDefaultOperations isMerchantLoggedIn]){
                                                                            [INUserDefaultOperations showAlert:MERCHANT_INVALID_USER_AUTH_MESSAGE];
                                                                            [INUserDefaultOperations clearMerchantDetails];
                                                                            [self performSelector:@selector(popAddUpdateController) withObject:nil afterDelay:2.0];
                                                                        }
                                                                    } else {
                                                                       [INUserDefaultOperations showAlert:[self.splashJson  objectForKey:@"message"]];
                                                                    }
                                                                }
                                                            }
                                                            } else {
                                                                UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                             message:@"No records found"
                                                                                                            delegate:nil
                                                                                                   cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                                [av show];
                                                            }
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                        [av show];
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                    }];
    
    
    
    [operation start];
}


-(void)sendAddStoreRequest
{
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    NSMutableArray  *catArray = [self getCategoryValues];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    if((placeId != nil) && ![placeId isEqualToString:@""])
    {
        [httpClient setDefaultHeader:@"X-HTTP-Method-Override" value:@"PUT"];
    }
    //[httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    if((placeId == nil) || [placeId isEqualToString:@""])
    {
        [params setObject:@"" forKey:@"place_id"];
        
    } else {
        [params setObject:placeId forKey:@"place_id"];
    }
    [params setObject:storenameTextField.text forKey:@"name"];
    [params setObject:[IN_APP_DELEGATE getAreaIdFromAreaTable:areaTextField.text] forKey:@"area_id"];
    [params setObject:catArray forKey:@"category"];
    [params setObject:@"0" forKey:@"place_parent"];
    [params setObject:@"" forKey:@"building"];
    if ([descriptionTextView.text length] > 0) {
        [params setObject:descriptionTextView.text forKey:@"description"];
    }else{
        [params setObject:@"" forKey:@"description"];
    }
    if ([streetTextView.text length] > 0) {
        [params setObject:streetTextView.text forKey:@"street"];
    }else{
        [params setObject:@"" forKey:@"street"];
    }
    if ([landmarkTextView.text length] > 0) {
        [params setObject:landmarkTextView.text forKey:@"landmark"];
    }else{
        [params setObject:@"" forKey:@"landmark"];
    }
    if ([areaTextField.text length] > 0) {
        [params setObject:areaTextField.text forKey:@"area"];
    }else{
        [params setObject:@"" forKey:@"area"];
    }
    if ([cityTextField.text length] > 0) {
        [params setObject:cityTextField.text forKey:@"city"];
    }else{
        [params setObject:@"" forKey:@"city"];
    }
    if ([pincodeTextField.text length] > 0) {
        [params setObject:pincodeTextField.text forKey:@"pincode"];
    }else{
        [params setObject:@"" forKey:@"pincode"];
    }
    [params setObject:[NSString stringWithFormat:@"%f",currentLatitude] forKey:@"latitude"];
    [params setObject:[NSString stringWithFormat:@"%f",currentLongitude]forKey:@"longitude"];
    [params setObject:[INUserDefaultOperations getMerchantAuthId] forKey:@"user_id"];
    [params setObject:[INUserDefaultOperations getMerchantAuthCode] forKey:@"auth_id"];
    [httpClient postPath:ADD_MERCHANT_STORE parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        //DebugLog(@"json afnetworking ->%@",json);
        if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"true"])
        {
            if((placeId == nil) || [placeId isEqualToString:@""])
            {
                [INEventLogger logEvent:@"Store_Added"];
            } else {
                [INEventLogger logEvent:@"Store_Modified"];
            }
            NSDictionary* data = [json objectForKey:@"data"];
            placeId = [data objectForKey:@"place_id"];
            //DebugLog(@"place id %@",placeId);
            if (pageControl.currentPage == 0) {
                pageControl.currentPage = 1;
            }else{
                pageControl.currentPage = 2;
            }
            [self pageChange:nil];
            //updated = 1;
            [INUserDefaultOperations setRefreshState:TRUE];
            //[[NSNotificationCenter defaultCenter] postNotificationName:IN_MERCHANT_RELOAD_MYPLACES_NOTIFICATION object:nil];
        } else if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"false"]) {
            if([json  objectForKey:@"code"] != nil &&  [[json  objectForKey:@"code"] isEqualToString:MERCHANT_INVALID_USER_AUTH_CODE])
            {
                if([INUserDefaultOperations isMerchantLoggedIn]){
                    [INUserDefaultOperations showAlert:MERCHANT_INVALID_USER_AUTH_MESSAGE];
                    [INUserDefaultOperations clearMerchantDetails];
                    [self performSelector:@selector(popAddUpdateController) withObject:nil afterDelay:2.0];
                }
            } else {
                //[INUserDefaultOperations showAlert:[json  objectForKey:@"message"]];
                [self showOKAlert:[json  objectForKey:@"message"]];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        [INUserDefaultOperations showAlert:error.localizedDescription];
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}

-(void)sendAddContactDetailsRequest
{
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    //DebugLog(@"place id %@",placeId);
    NSMutableArray  *catArray = [self getCategoryValues];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    if((placeId != nil) && ![placeId isEqualToString:@""])
    {
        [httpClient setDefaultHeader:@"X-HTTP-Method-Override" value:@"PUT"];
    }
    //[httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    if((placeId == nil) || [placeId isEqualToString:@""])
    {
        [params setObject:@"" forKey:@"place_id"];
    } else {
        [params setObject:[NSString stringWithFormat:@"%@",placeId] forKey:@"place_id"];
    }
    [params setObject:storenameTextField.text forKey:@"name"];
    [params setObject:catArray forKey:@"category"];
    [params setObject:@"0" forKey:@"place_parent"];
    [params setObject:@"" forKey:@"building"];
    if ([descriptionTextView.text length] > 0) {
        [params setObject:descriptionTextView.text forKey:@"description"];
    }else{
        [params setObject:@"" forKey:@"description"];
    }
    if ([streetTextView.text length] > 0) {
        [params setObject:streetTextView.text forKey:@"street"];
    }else{
        [params setObject:@"" forKey:@"street"];
    }
    if ([landmarkTextView.text length] > 0) {
        [params setObject:landmarkTextView.text forKey:@"landmark"];
    }else{
        [params setObject:@"" forKey:@"landmark"];
    }
    if ([areaTextField.text length] > 0) {
        [params setObject:areaTextField.text forKey:@"area"];
    }else{
        [params setObject:@"" forKey:@"area"];
    }
    if ([cityTextField.text length] > 0) {
        [params setObject:cityTextField.text forKey:@"city"];
    }else{
        [params setObject:@"" forKey:@"city"];
    }
    if ([pincodeTextField.text length] > 0) {
        [params setObject:pincodeTextField.text forKey:@"pincode"];
    }else{
        [params setObject:@"" forKey:@"pincode"];
    }
    [params setObject:[NSString stringWithFormat:@"%f",currentLatitude] forKey:@"latitude"];
    [params setObject:[NSString stringWithFormat:@"%f",currentLongitude]forKey:@"longitude"];
    ////////////////////////////////////////////////////////////
    if ([landlineTextField.text length] > 0) {
        [params setObject:landlineTextField.text forKey:@"landline"];
    }else{
        [params setObject:@"" forKey:@"landline"];
    }
    
    if([mobileNoTextField.text length] > 0)
    {
        [params setObject:MOBILE_NUMBER([INUserDefaultOperations getMerchantCountryCode],mobileNoTextField.text) forKey:@"mobile"];
    } else {
        [params setObject:@"" forKey:@"mobile"];
    }
    [params setObject:@"" forKey:@"mobile2"];
    [params setObject:@"" forKey:@"mobile3"];
    [params setObject:@"" forKey:@"landline2"];
    [params setObject:@"" forKey:@"landline3"];
    
    if (mobileArray.count > 0) {
        
        if(![[mobileArray objectAtIndex:0] isEqualToString:@""])
        {
            [params setObject:MOBILE_NUMBER([INUserDefaultOperations getMerchantCountryCode],[mobileArray objectAtIndex:0]) forKey:@"mobile2"];
        }
        
        if (mobileArray.count > 1 && ![[mobileArray objectAtIndex:1] isEqualToString:@""]) {
            [params setObject:MOBILE_NUMBER([INUserDefaultOperations getMerchantCountryCode],[mobileArray objectAtIndex:1]) forKey:@"mobile3"];
        }
    }
    if (landlineArray.count > 0) {
        [params setObject:[landlineArray objectAtIndex:0] forKey:@"landline2"];
        if (landlineArray.count > 1) {
            [params setObject:[landlineArray objectAtIndex:1] forKey:@"landline3"];
        }
    }

//    [params setObject:faxNoTextField.text forKey:@"fax"];
//    [params setObject:tollfreeTextField.text forKey:@"tollfree"];
    if ([emailTextField.text length] > 0) {
        [params setObject:emailTextField.text forKey:@"email"];
    }else{
        [params setObject:@"" forKey:@"email"];
    }
    if ([websiteTextField.text length] > 0) {
        [params setObject:websiteTextField.text forKey:@"website"];
    }else{
        [params setObject:@"" forKey:@"website"];
    }
    if ([txtFbUrl.text length] > 0) {
        [params setObject:txtFbUrl.text forKey:@"facebook_url"];
    }else{
        [params setObject:@"" forKey:@"facebook_url"];
    }
    if ([txtTwitterUrl.text length] > 0) {
        [params setObject:txtTwitterUrl.text forKey:@"twitter_url"];
    }else{
        [params setObject:@"" forKey:@"twitter_url"];
    }
    [params setObject:[INUserDefaultOperations getMerchantAuthId] forKey:@"user_id"];
    [params setObject:[INUserDefaultOperations getMerchantAuthCode] forKey:@"auth_id"];
    
    DebugLog(@"%@",params);
    [httpClient postPath:ADD_MERCHANT_STORE parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json afnetworking ->%@",json);
        if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"true"])
        {
            if((placeId == nil) || [placeId isEqualToString:@""])
            {
                [INEventLogger logEvent:@"Store_Added"];
            } else {
                [INEventLogger logEvent:@"Store_Modified"];
            }
            
            NSDictionary* data = [json objectForKey:@"data"];
            placeId = [data objectForKey:@"place_id"];
            //DebugLog(@"place id %@",placeId);
            pageControl.currentPage = 3;
            [self pageChange:nil];
            //updated = 1;
            [INUserDefaultOperations setRefreshState:TRUE];
            //[[NSNotificationCenter defaultCenter] postNotificationName:IN_MERCHANT_RELOAD_MYPLACES_NOTIFICATION object:nil];
        } else {
            if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"false"]) {
                if([json objectForKey:@"code"] != nil &&  [[json  objectForKey:@"code"] isEqualToString:MERCHANT_INVALID_USER_AUTH_CODE])
                {
                    if([INUserDefaultOperations isMerchantLoggedIn]){
                        [INUserDefaultOperations showAlert:MERCHANT_INVALID_USER_AUTH_MESSAGE];
                        [INUserDefaultOperations clearMerchantDetails];
                        [self performSelector:@selector(popAddUpdateController) withObject:nil afterDelay:2.0];
                    }
                } else {
                    [INUserDefaultOperations showAlert:[json  objectForKey:@"message"]];
                }
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        [INUserDefaultOperations showAlert:error.localizedDescription];
    }];
}

-(void)sendAddDescriptionRequest
{
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    //DebugLog(@"place id %@",placeId);
    NSMutableArray  *catArray = [self getCategoryValues];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    if((placeId != nil) && ![placeId isEqualToString:@""])
    {
        [httpClient setDefaultHeader:@"X-HTTP-Method-Override" value:@"PUT"];
    }
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    if((placeId == nil) || [placeId isEqualToString:@""])
    {
        [params setObject:@"" forKey:@"place_id"];
    } else {
        [params setObject:placeId forKey:@"place_id"];
    }
    [params setObject:storenameTextField.text forKey:@"name"];
    [params setObject:catArray forKey:@"category"];
    [params setObject:@"0" forKey:@"place_parent"];
    [params setObject:@"" forKey:@"building"];
    if ([descriptionTextView.text length] > 0) {
        [params setObject:descriptionTextView.text forKey:@"description"];
    }else{
        [params setObject:@"" forKey:@"description"];
    }
    if ([streetTextView.text length] > 0) {
        [params setObject:streetTextView.text forKey:@"street"];
    }else{
        [params setObject:@"" forKey:@"street"];
    }
    if ([landmarkTextView.text length] > 0) {
        [params setObject:landmarkTextView.text forKey:@"landmark"];
    }else{
        [params setObject:@"" forKey:@"landmark"];
    }
    if ([areaTextField.text length] > 0) {
        [params setObject:areaTextField.text forKey:@"area"];
    }else{
        [params setObject:@"" forKey:@"area"];
    }
    if ([cityTextField.text length] > 0) {
        [params setObject:cityTextField.text forKey:@"city"];
    }else{
        [params setObject:@"" forKey:@"city"];
    }
    if ([pincodeTextField.text length] > 0) {
        [params setObject:pincodeTextField.text forKey:@"pincode"];
    }else{
        [params setObject:@"" forKey:@"pincode"];
    }
    [params setObject:[NSString stringWithFormat:@"%f",currentLatitude] forKey:@"latitude"];
    [params setObject:[NSString stringWithFormat:@"%f",currentLongitude]forKey:@"longitude"];
    ////////////////////////////////////////////////////////////
    if ([landlineTextField.text length] > 0) {
        [params setObject:landlineTextField.text forKey:@"landline"];
    }else{
        [params setObject:@"" forKey:@"landline"];
    }
    
    if([mobileNoTextField.text length] > 0)
    {
        [params setObject:MOBILE_NUMBER([INUserDefaultOperations getMerchantCountryCode],mobileNoTextField.text) forKey:@"mobile"];
    } else {
        [params setObject:@"" forKey:@"mobile"];
    }
    [params setObject:@"" forKey:@"mobile2"];
    [params setObject:@"" forKey:@"mobile3"];
    [params setObject:@"" forKey:@"landline2"];
    [params setObject:@"" forKey:@"landline3"];
    
    if (mobileArray.count > 0) {
        
        if(![[mobileArray objectAtIndex:0] isEqualToString:@""])
        {
            [params setObject:MOBILE_NUMBER([INUserDefaultOperations getMerchantCountryCode],[mobileArray objectAtIndex:0]) forKey:@"mobile2"];
        }
        
        if (mobileArray.count > 1 && ![[mobileArray objectAtIndex:1] isEqualToString:@""]) {
            [params setObject:MOBILE_NUMBER([INUserDefaultOperations getMerchantCountryCode],[mobileArray objectAtIndex:1]) forKey:@"mobile3"];
        }
    }
    if (landlineArray.count > 0) {
        [params setObject:[landlineArray objectAtIndex:0] forKey:@"landline2"];
        if (landlineArray.count > 1) {
            [params setObject:[landlineArray objectAtIndex:1] forKey:@"landline3"];
        }
    }

//    [params setObject:faxNoTextField.text forKey:@"fax"];
//    [params setObject:tollfreeTextField.text forKey:@"tollfree"];
    if ([emailTextField.text length] > 0) {
        [params setObject:emailTextField.text forKey:@"email"];
    }else{
        [params setObject:@"" forKey:@"email"];
    }
    if ([websiteTextField.text length] > 0) {
        [params setObject:websiteTextField.text forKey:@"website"];
    }else{
        [params setObject:@"" forKey:@"website"];
    }
    if ([txtFbUrl.text length] > 0) {
        [params setObject:txtFbUrl.text forKey:@"facebook_url"];
    }else{
        [params setObject:@"" forKey:@"facebook_url"];
    }
    if ([txtTwitterUrl.text length] > 0) {
        [params setObject:txtTwitterUrl.text forKey:@"twitter_url"];
    }else{
        [params setObject:@"" forKey:@"twitter_url"];
    }
    
    [params setObject:[INUserDefaultOperations getMerchantAuthId] forKey:@"user_id"];
    [params setObject:[INUserDefaultOperations getMerchantAuthCode] forKey:@"auth_id"];
     //DebugLog(@"%@",params);
    [httpClient postPath:ADD_MERCHANT_STORE parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        //DebugLog(@"json afnetworking ->%@",json);
        if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"true"])
        {
            NSDictionary* data = [json objectForKey:@"data"];
            placeId = [data objectForKey:@"place_id"];
            //DebugLog(@"place id %@",placeId);
            pageControl.currentPage = 4;
            [self pageChange:nil];
            //updated = 1;
            [INUserDefaultOperations setRefreshState:TRUE];
            //[[NSNotificationCenter defaultCenter] postNotificationName:IN_MERCHANT_RELOAD_MYPLACES_NOTIFICATION object:nil];
        } else if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"false"]) {
            if([json  objectForKey:@"code"] != nil &&  [[json  objectForKey:@"code"] isEqualToString:MERCHANT_INVALID_USER_AUTH_CODE])
            {
                if([INUserDefaultOperations isMerchantLoggedIn]){
                    [INUserDefaultOperations showAlert:MERCHANT_INVALID_USER_AUTH_MESSAGE];
                    [INUserDefaultOperations clearMerchantDetails];
                    [self performSelector:@selector(popAddUpdateController) withObject:nil afterDelay:2.0];
                }
            } else {
                //[INUserDefaultOperations showAlert:[json  objectForKey:@"message"]];
                [self showOKAlert:[json  objectForKey:@"message"]];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        [INUserDefaultOperations showAlert:error.localizedDescription];
    }];
}


-(void)sendAddLogoRequest
{
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    NSMutableArray  *catArray = [self getCategoryValues];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    if((placeId != nil) && ![placeId isEqualToString:@""])
    {
        [httpClient setDefaultHeader:@"X-HTTP-Method-Override" value:@"PUT"];
    }
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    if((placeId == nil) || [placeId isEqualToString:@""])
    {
        [params setObject:@"" forKey:@"place_id"];
    } else {
        [params setObject:placeId forKey:@"place_id"];
    }
    [params setObject:storenameTextField.text forKey:@"name"];
    [params setObject:catArray forKey:@"category"];
    [params setObject:@"0" forKey:@"place_parent"];
    [params setObject:@"" forKey:@"building"];
    if ([descriptionTextView.text length] > 0) {
        [params setObject:descriptionTextView.text forKey:@"description"];
    }else{
        [params setObject:@"" forKey:@"description"];
    }
    if ([streetTextView.text length] > 0) {
        [params setObject:streetTextView.text forKey:@"street"];
    }else{
        [params setObject:@"" forKey:@"street"];
    }
    if ([landmarkTextView.text length] > 0) {
        [params setObject:landmarkTextView.text forKey:@"landmark"];
    }else{
        [params setObject:@"" forKey:@"landmark"];
    }
    if ([areaTextField.text length] > 0) {
        [params setObject:areaTextField.text forKey:@"area"];
    }else{
        [params setObject:@"" forKey:@"area"];
    }
    if ([cityTextField.text length] > 0) {
        [params setObject:cityTextField.text forKey:@"city"];
    }else{
        [params setObject:@"" forKey:@"city"];
    }
    if ([pincodeTextField.text length] > 0) {
        [params setObject:pincodeTextField.text forKey:@"pincode"];
    }else{
        [params setObject:@"" forKey:@"pincode"];
    }
    [params setObject:[NSString stringWithFormat:@"%f",currentLatitude] forKey:@"latitude"];
    [params setObject:[NSString stringWithFormat:@"%f",currentLongitude]forKey:@"longitude"];
    ////////////////////////////////////////////////////////////
    if ([landlineTextField.text length] > 0) {
        [params setObject:landlineTextField.text forKey:@"landline"];
    }else{
        [params setObject:@"" forKey:@"landline"];
    }
    
    if([mobileNoTextField.text length] > 0)
    {
        [params setObject:MOBILE_NUMBER([INUserDefaultOperations getMerchantCountryCode],mobileNoTextField.text) forKey:@"mobile"];
    } else {
        [params setObject:@"" forKey:@"mobile"];
    }
    [params setObject:@"" forKey:@"mobile2"];
    [params setObject:@"" forKey:@"mobile3"];
    [params setObject:@"" forKey:@"landline2"];
    [params setObject:@"" forKey:@"landline3"];
    
    if (mobileArray.count > 0) {
        
        if(![[mobileArray objectAtIndex:0] isEqualToString:@""])
        {
            [params setObject:MOBILE_NUMBER([INUserDefaultOperations getMerchantCountryCode],[mobileArray objectAtIndex:0]) forKey:@"mobile2"];
        }
        
        if (mobileArray.count > 1 && ![[mobileArray objectAtIndex:1] isEqualToString:@""]) {
            [params setObject:MOBILE_NUMBER([INUserDefaultOperations getMerchantCountryCode],[mobileArray objectAtIndex:1]) forKey:@"mobile3"];
        }
    }
    if (landlineArray.count > 0) {
        [params setObject:[landlineArray objectAtIndex:0] forKey:@"landline2"];
        if (landlineArray.count > 1) {
            [params setObject:[landlineArray objectAtIndex:1] forKey:@"landline3"];
        }
    }

//    [params setObject:faxNoTextField.text forKey:@"fax"];
//    [params setObject:tollfreeTextField.text forKey:@"tollfree"];
    if ([emailTextField.text length] > 0) {
        [params setObject:emailTextField.text forKey:@"email"];
    }else{
        [params setObject:@"" forKey:@"email"];
    }
    if ([websiteTextField.text length] > 0) {
        [params setObject:websiteTextField.text forKey:@"website"];
    }else{
        [params setObject:@"" forKey:@"website"];
    }
    if ([txtFbUrl.text length] > 0) {
        [params setObject:txtFbUrl.text forKey:@"facebook_url"];
    }else{
        [params setObject:@"" forKey:@"facebook_url"];
    }
    if ([txtTwitterUrl.text length] > 0) {
        [params setObject:txtTwitterUrl.text forKey:@"twitter_url"];
    }else{
        [params setObject:@"" forKey:@"twitter_url"];
    }
    ////////////////////////////////////////////////////////////
    UIImage *placeHolderImage = [UIImage imageNamed:@"Add_Logo.png"];
    if (iconImageView.image == nil || [iconImageView.image isEqual:placeHolderImage]) {
        [params setObject:@"" forKey:@"userfile"];
    }else{
        //DebugLog(@"%d",isImgFrmShopLocalGallery);
        if(isImgFrmShopLocalGallery) {
            [params setObject:shoplocalImage forKey:@"gallery_image"];
        } else {
            NSData *dataObj = UIImageJPEGRepresentation(iconImageView.image,0.75);
            NSString *image = [IN_APP_DELEGATE base64forData:dataObj];
            [params setObject:image forKey:@"userfile"];
            [params setObject:@"broadcast.jpg" forKey:@"filename"];
            [params setObject:@"image/jpeg" forKey:@"filetype"];
        }
    }
    ////////////////////////////////////////////////////////////
    [params setObject:[INUserDefaultOperations getMerchantAuthId] forKey:@"user_id"];
    [params setObject:[INUserDefaultOperations getMerchantAuthCode] forKey:@"auth_id"];
     //DebugLog(@"%@",params);
    [httpClient postPath:ADD_MERCHANT_STORE parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        //DebugLog(@"json afnetworking ->%@",json);
        if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"true"])
        {
            if((placeId == nil) || [placeId isEqualToString:@""])
            {
                [INEventLogger logEvent:@"Store_Added"];
            } else {
                [INEventLogger logEvent:@"Store_Modified"];
            }
            NSDictionary* data = [json objectForKey:@"data"];
            if((placeId == nil) || [placeId isEqualToString:@""])
            {
                placeId = [data objectForKey:@"place_id"];
                [self sendPlaceTimeStoreRequest];
            } else {
                placeId = [data objectForKey:@"place_id"];
            }
            //DebugLog(@"place id %@",placeId);
            //updated = 1;
           
            [INUserDefaultOperations setRefreshState:TRUE];
            //[[NSNotificationCenter defaultCenter] postNotificationName:IN_MERCHANT_RELOAD_MYPLACES_NOTIFICATION object:nil];
        } else if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"false"]) {
            if([json  objectForKey:@"code"] != nil &&  [[json  objectForKey:@"code"] isEqualToString:MERCHANT_INVALID_USER_AUTH_CODE])
            {
                if([INUserDefaultOperations isMerchantLoggedIn]){
                    [INUserDefaultOperations showAlert:MERCHANT_INVALID_USER_AUTH_MESSAGE];
                    [INUserDefaultOperations clearMerchantDetails];
                    [self performSelector:@selector(popAddUpdateController) withObject:nil afterDelay:2.0];
                }
            } else {
                //[INUserDefaultOperations showAlert:[json  objectForKey:@"message"]];
                [self showOKAlert:[json  objectForKey:@"message"]];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"send logo : [HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        [INUserDefaultOperations showAlert:error.localizedDescription];
    }];
}


-(void)sendPlaceTimeStoreRequest
{
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"X-HTTP-Method-Override" value:@"PUT"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:placeId forKey:@"place_id"];
    [self setStoreTime:params];
    [params setObject:[INUserDefaultOperations getMerchantAuthId] forKey:@"user_id"];
    [params setObject:[INUserDefaultOperations getMerchantAuthCode] forKey:@"auth_id"];
    //DebugLog(@"----%@------",params);
    [httpClient postPath:ADD_STORE_TIME parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        //DebugLog(@"json afnetworking ->%@",json);
        if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"true"])
        {
            if((placeId == nil) || [placeId isEqualToString:@""])
            {
                [INEventLogger logEvent:@"Store_Added"];
            } else {
                [INEventLogger logEvent:@"Store_Modified"];
            }
            NSDictionary* data = [json objectForKey:@"data"];
            placeId = [data objectForKey:@"place_id"];
            //DebugLog(@"place id %@",placeId);
            pageControl.currentPage = 5;
            [self pageChange:nil];
            [INUserDefaultOperations setRefreshState:TRUE];
            //[[NSNotificationCenter defaultCenter] postNotificationName:IN_MERCHANT_RELOAD_MYPLACES_NOTIFICATION object:nil];
        } else if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"false"]) {
            if([json  objectForKey:@"code"] != nil &&  [[json  objectForKey:@"code"] isEqualToString:MERCHANT_INVALID_USER_AUTH_CODE])
            {
                if([INUserDefaultOperations isMerchantLoggedIn]){
                    [INUserDefaultOperations showAlert:MERCHANT_INVALID_USER_AUTH_MESSAGE];
                    [INUserDefaultOperations clearMerchantDetails];
                    [self performSelector:@selector(popAddUpdateController) withObject:nil afterDelay:2.0];
                }
            } else {
                //[INUserDefaultOperations showAlert:[json  objectForKey:@"message"]];
                [self showOKAlert:[json  objectForKey:@"message"]];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"add place time :[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];

}

-(void)sendHideStoreRequest:(NSString *)placeStatus
{
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    //DebugLog(@"place id %@",placeId);
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-HTTP-Method-Override" value:@"PUT"];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[NSString stringWithFormat:@"%@",placeId] forKey:@"place_id"];
    [params setObject:placeStatus forKey:@"place_status"];
    [params setObject:[INUserDefaultOperations getMerchantAuthId] forKey:@"user_id"];
    [params setObject:[INUserDefaultOperations getMerchantAuthCode] forKey:@"auth_id"];
    //DebugLog(@"%@",params);
    
    [httpClient postPath:HIDE_STORE parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        //DebugLog(@"json afnetworking ->%@",json);
        if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"true"])
        {
            //updated = 1;
            [INUserDefaultOperations setRefreshState:TRUE];
            if([placeStatus isEqualToString:@"1"])
            {
                place_status = @"1";
            } else {
                 place_status = @"0";
            }
            //[INUserDefaultOperations showAlert:[json  objectForKey:@"message"]];
            [self showOKAlert:[json  objectForKey:@"message"]];
            //[[NSNotificationCenter defaultCenter] postNotificationName:IN_MERCHANT_RELOAD_MYPLACES_NOTIFICATION object:nil];
        } else if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"false"]) {
            if([json  objectForKey:@"code"] != nil &&  [[json  objectForKey:@"code"] isEqualToString:MERCHANT_INVALID_USER_AUTH_CODE])
            {
                if([INUserDefaultOperations isMerchantLoggedIn]){
                    [INUserDefaultOperations showAlert:MERCHANT_INVALID_USER_AUTH_MESSAGE];
                    [INUserDefaultOperations clearMerchantDetails];
                    [self performSelector:@selector(popAddUpdateController) withObject:nil afterDelay:2.0];
                }
            } else {
                //[INUserDefaultOperations showAlert:[json  objectForKey:@"message"]];
                [self showOKAlert:[json  objectForKey:@"message"]];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"sendHideStoreRequest :[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        //[INUserDefaultOperations showAlert:error.localizedDescription];
        [self showOKAlert:error.localizedDescription];
    }];
}


-(void)sendDeleteStoreRequest
{
    [hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    //DebugLog(@"delete store = %@",DELETE_STORE(placeId));
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    [httpClient setDefaultHeader:@"user_id" value:[INUserDefaultOperations getMerchantAuthId]];
    [httpClient setDefaultHeader:@"auth_id" value:[INUserDefaultOperations getMerchantAuthCode]];
    [httpClient deletePath:DELETE_STORE(placeId) parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        //DebugLog(@"json ->%@",json);
        if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"true"])
        {
            //[INUserDefaultOperations showAlert:[json  objectForKey:@"message"]];
            [self showOKAlert:[json  objectForKey:@"message"]];
            [INUserDefaultOperations setRefreshState:TRUE];
//            if(updated == 1) {
//                //[[NSNotificationCenter defaultCenter] postNotificationName:IN_MERCHANT_RELOAD_MYPLACES_NOTIFICATION object:nil];
//            }
            [self performSelector:@selector(popAddUpdateController) withObject:nil afterDelay:2.0];
        } else if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"false"]) {
            if([json  objectForKey:@"code"] != nil &&  [[json  objectForKey:@"code"] isEqualToString:MERCHANT_INVALID_USER_AUTH_CODE])
            {
                if([INUserDefaultOperations isMerchantLoggedIn]){
                    [INUserDefaultOperations showAlert:MERCHANT_INVALID_USER_AUTH_MESSAGE];
                    [INUserDefaultOperations clearMerchantDetails];
                    [self performSelector:@selector(popAddUpdateController) withObject:nil afterDelay:2.0];
                }
            } else {
                //[INUserDefaultOperations showAlert:[json  objectForKey:@"message"]];
                [self showOKAlert:[json  objectForKey:@"message"]];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"sendDeleteStoreRequest : [HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [self showOKAlert:error.localizedDescription];
        [CommonCallback hideProgressHud];
    }];
}

-(void)setStoreTime:(NSMutableDictionary *)lparams
{
    if(fromDateBtn.titleLabel.text == nil || [fromDateBtn.titleLabel.text isEqualToString:@"00:00"] || toDateBtn.titleLabel.text == nil || [toDateBtn.titleLabel.text isEqualToString:@"00:00"] || fromDatelbl.text == nil || [fromDatelbl.text isEqualToString:@""] || toDatelbl.text == nil || [toDatelbl.text isEqualToString:@""] )
    {
        fromDatelbl.text = @"00:00:00";
        toDatelbl.text = @"00:00:00";
    }
    if([sunBtn isSelected])
    {
        [lparams setObject:@"sunday" forKey:@"sunday"];
        [lparams setObject:fromDatelbl.text forKey:@"sun_time1_open"];
        [lparams setObject:toDatelbl.text forKey:@"sun_time1_close"];
        [lparams setObject:@"0" forKey:@"sun_time1_status"];
    } else {
        [lparams setObject:@"sunday" forKey:@"sunday"];
        [lparams setObject:fromDatelbl.text forKey:@"sun_time1_open"];
        [lparams setObject:toDatelbl.text forKey:@"sun_time1_close"];
        [lparams setObject:@"1" forKey:@"sun_time1_status"];
    }
    if([monBtn isSelected])
    {
        [lparams setObject:@"monday" forKey:@"monday"];
        [lparams setObject:fromDatelbl.text forKey:@"mon_time1_open"];
        [lparams setObject:toDatelbl.text forKey:@"mon_time1_close"];
        [lparams setObject:@"0" forKey:@"mon_time1_status"];
    } else {
        [lparams setObject:@"monday" forKey:@"monday"];
        [lparams setObject:fromDatelbl.text forKey:@"mon_time1_open"];
        [lparams setObject:toDatelbl.text forKey:@"mon_time1_close"];
        [lparams setObject:@"1" forKey:@"mon_time1_status"];
    }
    if([tueBtn isSelected])
    {
        [lparams setObject:@"tuesday" forKey:@"tuesday"];
        [lparams setObject:fromDatelbl.text forKey:@"tue_time1_open"];
        [lparams setObject:toDatelbl.text forKey:@"tue_time1_close"];
        [lparams setObject:@"0" forKey:@"tue_time1_status"];
    } else {
        [lparams setObject:@"tuesday" forKey:@"tuesday"];
        [lparams setObject:fromDatelbl.text forKey:@"tue_time1_open"];
        [lparams setObject:toDatelbl.text forKey:@"tue_time1_close"];
        [lparams setObject:@"1" forKey:@"tue_time1_status"];
    }
    if([wedBtn isSelected])
    {
        [lparams setObject:@"wednesday" forKey:@"wednesday"];
        [lparams setObject:fromDatelbl.text forKey:@"wed_time1_open"];
        [lparams setObject:toDatelbl.text forKey:@"wed_time1_close"];
        [lparams setObject:@"0" forKey:@"wed_time1_status"];
    } else {
        [lparams setObject:@"wednesday" forKey:@"wednesday"];
        [lparams setObject:fromDatelbl.text forKey:@"wed_time1_open"];
        [lparams setObject:toDatelbl.text forKey:@"wed_time1_close"];
        [lparams setObject:@"1" forKey:@"wed_time1_status"];
    }
    if([thurBtn isSelected])
    {
        [lparams setObject:@"thursday" forKey:@"thursday"];
        [lparams setObject:fromDatelbl.text forKey:@"thu_time1_open"];
        [lparams setObject:toDatelbl.text forKey:@"thu_time1_close"];
        [lparams setObject:@"0" forKey:@"thu_time1_status"];
    } else {
        [lparams setObject:@"thursday" forKey:@"thursday"];
        [lparams setObject:fromDatelbl.text forKey:@"thu_time1_open"];
        [lparams setObject:toDatelbl.text forKey:@"thu_time1_close"];
        [lparams setObject:@"1" forKey:@"thu_time1_status"];
    }
    if([friBtn isSelected])
    {
        [lparams setObject:@"friday" forKey:@"friday"];
        [lparams setObject:fromDatelbl.text forKey:@"fri_time1_open"];
        [lparams setObject:toDatelbl.text forKey:@"fri_time1_close"];
        [lparams setObject:@"0" forKey:@"fri_time1_status"];
    } else {
        [lparams setObject:@"friday" forKey:@"friday"];
        [lparams setObject:fromDatelbl.text forKey:@"fri_time1_open"];
        [lparams setObject:toDatelbl.text forKey:@"fri_time1_close"];
        [lparams setObject:@"1" forKey:@"fri_time1_status"];
    }
    if([satBtn isSelected])
    {
        [lparams setObject:@"saturday" forKey:@"saturday"];
        [lparams setObject:fromDatelbl.text forKey:@"sat_time1_open"];
        [lparams setObject:toDatelbl.text forKey:@"sat_time1_close"];
        [lparams setObject:@"0" forKey:@"sat_time1_status"];
    } else {
        [lparams setObject:@"saturday" forKey:@"saturday"];
        [lparams setObject:fromDatelbl.text forKey:@"sat_time1_open"];
        [lparams setObject:toDatelbl.text forKey:@"sat_time1_close"];
        [lparams setObject:@"1" forKey:@"sat_time1_status"];
    }
}

-(void)setStoreTimeValues:(NSDictionary *)lparams
{
    NSString *otime = [lparams objectForKey:@"open_time"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setHour:[[otime substringToIndex:2] integerValue]];
    [comps setMinute:[[otime substringFromIndex:3] integerValue]];
    [comps setSecond:[[otime substringToIndex:6] integerValue]];
	self.fromselectedDate = [[NSCalendar currentCalendar] dateFromComponents:comps];
    NSString *fromString = [dateFormatter stringFromDate:self.fromselectedDate];
    //DebugLog(@"from Date is: %@", fromString);
    fromDatelbl.text = fromString;
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    dateFormatter2.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter2 setDateFormat:@"hh:mm a"];
    NSString *fromString2 = [dateFormatter2 stringFromDate:self.fromselectedDate];
    //DebugLog(@"from Date is: %@", fromString2);
    [fromDateBtn setTitle:fromString2 forState: UIControlStateNormal];

    
    NSString *ctime = [lparams objectForKey:@"close_time"];
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    dateFormatter1.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter1 setDateFormat:@"HH:mm:ss"];
    NSDateComponents *comps1 = [[NSDateComponents alloc] init];
    [comps1 setHour:[[ctime substringToIndex:2] integerValue]];
    [comps1 setMinute:[[ctime substringFromIndex:3] integerValue]];
    [comps1 setSecond:[[ctime substringToIndex:6] integerValue]];
	self.toselectedDate = [[NSCalendar currentCalendar] dateFromComponents:comps1];
    NSString *toString = [dateFormatter stringFromDate:self.toselectedDate];
    //DebugLog(@"to Date is: %@", toString);
    toDatelbl.text = toString;
    NSDateFormatter *dateFormatter3 = [[NSDateFormatter alloc] init];
    dateFormatter3.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter3 setDateFormat:@"hh:mm a"];
    NSString *fromString3 = [dateFormatter3 stringFromDate:self.toselectedDate];
    //DebugLog(@"from Date is: %@", fromString3);
    [toDateBtn setTitle:fromString3 forState: UIControlStateNormal];



}
-(void)setStoreDayValues:(NSDictionary *)lparams
{
    NSString *day = [lparams objectForKey:@"day"];
    NSString *close = [lparams objectForKey:@"is_closed"];
    //DebugLog(@"%@   %@",day,close);
//    NSString *otime = [lparams objectForKey:@"open_time"];
//    NSString *ctime = [lparams objectForKey:@"close_time"];
    
    if([day isEqualToString:@"monday"])
    {
        if([close isEqualToString:@"0"])
        {
            [monBtn setSelected:TRUE];
        } else {
            [monBtn setSelected:FALSE];
        }
    } else if([day isEqualToString:@"tuesday"]) {
        if([close isEqualToString:@"0"])
        {
            [tueBtn setSelected:TRUE];
        } else {
            [tueBtn setSelected:FALSE];
        }
    } else if([day isEqualToString:@"wednesday"]) {
        if([close isEqualToString:@"0"])
        {
            [wedBtn setSelected:TRUE];
        } else {
            [wedBtn setSelected:FALSE];
        }
    } else if([day isEqualToString:@"thursday"]) {
        if([close isEqualToString:@"0"])
        {
            [thurBtn setSelected:TRUE];
        } else {
            [thurBtn setSelected:FALSE];
        }
    } else if([day isEqualToString:@"friday"]) {
        if([close isEqualToString:@"0"])
        {
            [friBtn setSelected:TRUE];
        } else {
            [friBtn setSelected:FALSE];
        }
    } else if([day isEqualToString:@"saturday"]) {
        if([close isEqualToString:@"0"])
        {
            [satBtn setSelected:TRUE];
        } else {
            [satBtn setSelected:FALSE];
        }
    } else if([day isEqualToString:@"sunday"]) {
        if([close isEqualToString:@"0"])
        {
            [sunBtn setSelected:TRUE];
        } else {
            [sunBtn setSelected:FALSE];
        }
    }     
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setStorenameTextField:nil];
    [self setAreaTextField:nil];
    [self setCityTextField:nil];
    [self setPincodeTextField:nil];
    [self setStoreScrollView:nil];
    [self setPageControl:nil];
    [self setDescriptionTextView:nil];
    [self setStreetTextView:nil];
    [self setLandmarkTextView:nil];
    [self setLandlineTextField:nil];
    [self setMobileNoTextField:nil];
    [self setFaxNoTextField:nil];
    [self setTollfreeTextField:nil];
    [self setEmailTextField:nil];
    [self setWebsiteTextField:nil];
    [self setSunBtn:nil];
    [self setMonBtn:nil];
    [self setTueBtn:nil];
    [self setWedBtn:nil];
    [self setThurBtn:nil];
    [self setFriBtn:nil];
    [self setSatBtn:nil];
    [self setMapImageView:nil];
    [self setMaplbl:nil];
    [self setIconImageView:nil];
    [self setCategoryBtn:nil];
    [self setToDateBtn:nil];
    [self setFromDateBtn:nil];
    [self setLocationScrollView:nil];
    [self setGalleryView:nil];
    [self setDoneBtn:nil];
    [self setContactScrollView:nil];
    [self setGallerymsglbl:nil];
    [self setMapskipBtn:nil];
    [self setBackView:nil];
    [self setBackview2:nil];
    [self setBackview3:nil];
    [self setOpenlbl:nil];
    [self setOpenonlbl:nil];
    [self setTextviewplaceholderLabel:nil];
    [self setButtonsCollection:nil];
    [self setDescriptionScrollView:nil];
    [self setAddImageDescriptionView:nil];
    [self setLbldescriptionViewHeader:nil];
    [self setLbldescriptionViewContent:nil];
    [self setDescriptionViewSetBtn:nil];
    [self setDescriptionViewSkipBtn:nil];
    [self setDescriptionViewtitleTextView:nil];
    [self setBackView4:nil];
    [self setLocationInfoBtn:nil];
    [self setSaveMapLocationBtn:nil];
    [self setLblstoreName:nil];
    [self setLblstoreDescription:nil];
    [self setFromDatelbl:nil];
    [self setToDatelbl:nil];
    [self setMobileNumberTableView:nil];
    [self setAddMoblieNumberBtn:nil];
    [self setImgSeperator1:nil];
    [self setImgSeperator2:nil];
    [self setImgSeperator3:nil];
    [self setImgSeperator4:nil];
    [self setImgSeperator5:nil];
    [self setContactBackScrollView:nil];
    [self setAddLandlineNumberBtn:nil];
    [self setLandlineNumberTableView:nil];
    [self setImgSeperator6:nil];
    [self setConactscreenlbl:nil];
    [self setTxtFbUrl:nil];
    [self setTxtTwitterUrl:nil];
    [self setImgSeperator7:nil];
    [self setImgSeperator8:nil];
    [self setContactdetailsSkipBtn:nil];
    [self setStoreLogoSkipBtn:nil];
    [self setDescriptionSkipBtn:nil];
    [self setSaveLocationBtn:nil];
    [self setSaveContactDetailsBtn:nil];
    [self setDescriptionSaveBtn:nil];
    [self setDaysBtn:nil];
    [super viewDidUnload];
}


#pragma mark - Private Methods

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
	[super setEditing:editing animated:animated];
	[self.gridView setEditing:editing animated:animated];
	
	if (editing) {
		self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                                                                              target:self
                                                                                              action:@selector(didTapAction:)];
	}else {
		self.navigationItem.leftBarButtonItem = nil;
	}
}

- (void)didTapAction:(id)sender
{
#if DEBUG
	DebugLog(@"Selected item count : %i", [self.gridView.selectedIndexes count]);
#endif
	[[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%i items selected.", [self.gridView.selectedIndexes count]]
                                message:nil
                               delegate:nil
                      cancelButtonTitle:@"Dismiss"
                      otherButtonTitles:nil] show];
    [self.gridView deleteCellAtIndexSet:self.gridView.selectedIndexes animated:YES];
}




#pragma mark - VCGridViewDataSource

- (NSInteger)numberOfCellsInGridView:(VCGridView*)gridView
{
//	return [galleryArray count];
    DebugLog(@"0) galleryArray Count %d",[galleryArray count]);
    double cellsCount = [galleryArray count]/3.0;
    DebugLog(@"1) cellsCount %f",cellsCount);
    if (cellsCount == 0 || cellsCount < 3) {
        return cellsCount = 9;
    }
    int cellsintCount = lroundf(cellsCount) + 1;
    DebugLog(@"2)cellsintCount %d",cellsintCount);
    return cellsintCount * 3;
}

- (VCGridViewCell *)gridView:(VCGridView *)gridView cellAtIndex:(NSInteger)index
{
    UIImageView *imageView;
    UILabel *lblTitle;

	VCGridViewCell *cell = [gridView dequeueReusableCell];
	if (!cell) {
		cell = [[VCGridViewCell alloc] initWithFrame:CGRectZero];
		
		CGRect contentFrame = CGRectInset(cell.bounds, 0, 0);
        
		imageView = [[UIImageView alloc] initWithFrame:contentFrame];
		imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		imageView.contentMode = UIViewContentModeScaleToFill;
        imageView.tag = 111;
		//imageView.image = [UIImage imageNamed:@"cell"];
		[cell.contentView addSubview:imageView];
        
		UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:contentFrame];
		imageView1.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		imageView1.contentMode = UIViewContentModeBottomRight;
		imageView1.image = [UIImage imageNamed:@"check"];
		cell.editingSelectionOverlayView = imageView1;
        
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0,60, 85, 25)];
        lblTitle.text = @"title";
        lblTitle.tag = 112;
        lblTitle.font = DEFAULT_BOLD_FONT(15);
        lblTitle.textAlignment = NSTextAlignmentCenter;
        lblTitle.textColor = [UIColor whiteColor];
        lblTitle.backgroundColor =  BROWN_OFFWHITE_COLOR;
        [cell.contentView addSubview:lblTitle];
        
        cell.contentView.layer.cornerRadius = 8.0;
        cell.contentView.clipsToBounds = YES;
        
        
		cell.highlightedBackgroundView.backgroundColor = BROWN_OFFWHITE_COLOR;
        cell.highlightedBackgroundView.layer.cornerRadius = 8.0;
	}
    imageView = (UIImageView *)[cell viewWithTag:111];
    lblTitle = (UILabel *)[cell viewWithTag:112];
    
    if (index < [galleryArray count]) {
        imageView.backgroundColor = [UIColor clearColor];
        if ([[galleryArray objectAtIndex:index] isKindOfClass:[INGalleryObj class]]) {
            INGalleryObj *tempgalleryObj  = [galleryArray objectAtIndex:index];
            [imageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(tempgalleryObj.thumb_url)] placeholderImage:[UIImage imageNamed:@"list_thumbnail.png"]];
            DebugLog(@"index %d :title %@ link-%@",index,tempgalleryObj.title,THUMBNAIL_LINK(tempgalleryObj.thumb_url));
            
            if (tempgalleryObj.title == nil || [tempgalleryObj.title isEqualToString:@""]) {
                lblTitle.hidden = TRUE;
            }else{
                lblTitle.hidden = FALSE;
                lblTitle.text = [tempgalleryObj.title capitalizedString];
            }
        }else
        {
            DebugLog(@"index %d : add image ",index);
            lblTitle.hidden = TRUE;
            imageView.image = [UIImage imageNamed:@"photo.png"];
        }
    }else{
        DebugLog(@"index %d : empty image ",index);
        lblTitle.hidden = TRUE;
        imageView.image = nil;
        imageView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
    }
	return cell;
}

- (BOOL)gridView:(VCGridView *)gridView canEditCellAtIndex:(NSInteger)index
{
    if (index < [galleryArray count]) {
        if ([[galleryArray objectAtIndex:index] isKindOfClass:[INGalleryObj class]]) {
            return YES;
        }
    }
    return NO;
}

#pragma mark - VCGridViewDelegate

- (void)gridView:(VCGridView*)gridView didSelectCellAtIndex:(NSInteger)index
{
#if DEBUG
	DebugLog(@"Selected %i", index);
#endif
    if (addImageDescriptionView.hidden) {
//        if (index == 0) {
//            if(self.placeId == nil || [self.placeId isEqualToString:@""]) {
//                 [INUserDefaultOperations showAlert:@"Hey, you need to make a store on screen 2 to add images."];
//            } else {
//                [self addNewImage];
//            }
//        }  else {
//            DebugLog(@"push to Browser %i", index);
//            _photos = [[NSMutableArray alloc] init];
//            MWPhoto *photo;
//            INGalleryObj *tempgalleryObj;
//            for(int i = 1; i < [galleryArray count]; i++)
//            {
//                tempgalleryObj = (INGalleryObj *)[galleryArray objectAtIndex:i];
//                //            DebugLog(@"link--- %@",tempgalleryObj.image_url);
//                photo = [[MWPhoto alloc] initWithURL:[NSURL URLWithString:THUMBNAIL_LINK(tempgalleryObj.image_url)]];
//                if (tempgalleryObj.title != nil || ![tempgalleryObj.title isEqualToString:@""])
//                    photo.caption = [NSString stringWithFormat:@"%@",tempgalleryObj.title];
//                [_photos addObject:photo];
//                tempgalleryObj = nil;
//            }
//            MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
//            browser.displayActionButton = YES;
//            browser.wantsFullScreenLayout = YES;
//            [browser setInitialPageIndex:index-1];
//            [self.navigationController pushViewController:browser animated:YES];
//        }
        if (index < [galleryArray count]) {
            if ([[galleryArray objectAtIndex:index] isKindOfClass:[INGalleryObj class]]) {
                DebugLog(@"push to Browser %i", index);
                _photos = [[NSMutableArray alloc] init];
                MWPhoto *photo;
                INGalleryObj *tempgalleryObj;
                for(int i = 1; i < [galleryArray count]; i++)
                {
                    tempgalleryObj = (INGalleryObj *)[galleryArray objectAtIndex:i];
                    DebugLog(@"title--- %@",tempgalleryObj.title);
                    photo = [[MWPhoto alloc] initWithURL:[NSURL URLWithString:THUMBNAIL_LINK(tempgalleryObj.image_url)]];
                    if (tempgalleryObj.title != nil || ![tempgalleryObj.title isEqualToString:@""])
                        photo.caption = [NSString stringWithFormat:@"%@",tempgalleryObj.title];
                    [_photos addObject:photo];
                    tempgalleryObj = nil;
                }
//                MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
//                browser.displayActionButton = YES;
//                browser.wantsFullScreenLayout = YES;
//                [browser setInitialPageIndex:index-1];
//                [self.navigationController pushViewController:browser animated:YES];
                
                // Create browser
                MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
                // Set options
                browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
                browser.wantsFullScreenLayout = YES; // iOS 5 & 6 only: Decide if you want the photo browser full screen, i.e. whether the status bar is affected (defaults to YES)
                
                browser.displayNavArrows = YES; // Whether to display left and right nav arrows on toolbar (defaults to NO)
                browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
                browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
                browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
                browser.enableGrid = NO; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
                browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
                
                // Optionally set the current visible photo before displaying
                [browser setCurrentPhotoIndex:index-1];
                
                // Present
                [self.navigationController pushViewController:browser animated:YES];
            }else
            {
                [self addNewImage];
            }
        }else{
            DebugLog(@"You clicked empty image. :)");
        }
    }
}

- (CGSize)sizeForCellsInGridView:(VCGridView *)gridView
{
//	return CGSizeMake(75.0f, 75.0f);
    return CGSizeMake(85.0f, 85.0f);
}

- (CGFloat)paddingForCellsInGridView:(VCGridView *)gridView
{
//	return 20.0f;
	return 15.0f;
}

#pragma MWPhotoBrowser delegate methods
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser
{
    return [_photos count];
}
- (id<MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index
{
    return [_photos objectAtIndex:index];
}

#pragma UITextView delegate methods
- (void)textViewDidBeginEditing:(UITextView *)textView{
    if (!addImageDescriptionView.hidden && [textView isEqual:descriptionViewtitleTextView]) {
        [UIView animateWithDuration:0.2 animations:^{
            addImageDescriptionView.frame = CGRectMake(addImageDescriptionView.frame.origin.x,self.view.frame.size.height - 516, addImageDescriptionView.frame.size.width, addImageDescriptionView.frame.size.height);
        } completion:^(BOOL finished) {
        }];
    }
}


#pragma UITableView DataSource and Delegate Methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([tableView isEqual:mobileNumberTableView]) {
        return [mobileArray count];// return mobileNumberCount;
    }else if ([tableView isEqual:LandlineNumberTableView]){
        return [landlineArray count];//return landlineNumberCount;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if ([tableView isEqual:mobileNumberTableView]) {
        NSString *CellIdentifier = [NSString stringWithFormat:@"MobileNumberCell%d%d",indexPath.section,indexPath.row];
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        UITextField *mobilenumberTextField;
        UIButton *deleteBtn;
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            mobilenumberTextField = [[UITextField alloc]initWithFrame:CGRectMake(6, 12, 228, 30)];
            mobilenumberTextField.delegate = self;
            mobilenumberTextField.text = @"";
//            mobilenumberTextField.placeholder = [NSString stringWithFormat:@"Mobile Number%d",indexPath.row+2];
            mobilenumberTextField.placeholder = MOBILE_PLACEHODER;
            mobilenumberTextField.tag = mobileNoTextField.tag + (1+indexPath.row);
            mobilenumberTextField.keyboardType = UIKeyboardTypeNumberPad;
            mobilenumberTextField.returnKeyType = UIReturnKeyNext;
            mobilenumberTextField.font = DEFAULT_FONT(15);
            mobilenumberTextField.textColor = BROWN_COLOR;
            [mobilenumberTextField addTarget:self action:@selector(mobileTextChanged:) forControlEvents:UIControlEventEditingDidEnd];
            [cell.contentView addSubview:mobilenumberTextField];
            
            deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [deleteBtn setFrame:CGRectMake(CGRectGetMaxX(mobilenumberTextField.frame)+5, 2, 40, 40)];
            [deleteBtn setTitle:@"-" forState:UIControlStateNormal];
            [deleteBtn setBackgroundImage:[UIImage imageNamed:@"Button1.png"] forState:UIControlStateNormal];
            [deleteBtn setBackgroundColor:[UIColor clearColor]];
            deleteBtn.tag = 3;
            [deleteBtn addTarget:self action:@selector(deleteMobileTextFieldBtnTapped:event:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:deleteBtn];
        }
        mobilenumberTextField = (UITextField *)[cell viewWithTag:mobileNoTextField.tag + (1+indexPath.row)];
        mobilenumberTextField.text = [mobileArray objectAtIndex:indexPath.row];
        return cell;
    }else if ([tableView isEqual:LandlineNumberTableView]){
        NSString *CellIdentifier = [NSString stringWithFormat:@"LandlineNumberCell%d%d",indexPath.section,indexPath.row];
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        UITextField *landlineenumberTextField;
        UIButton *deleteBtn;
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            landlineenumberTextField = [[UITextField alloc]initWithFrame:CGRectMake(6, 12, 228, 30)];
            landlineenumberTextField.delegate = self;
            landlineenumberTextField.text = @"";
//            landlineenumberTextField.placeholder = [NSString stringWithFormat:@"Landline Number%d",indexPath.row+2];
            landlineenumberTextField.placeholder = LANDLINE_PLACEHOLDER;
            landlineenumberTextField.tag = landlineTextField.tag + (1+indexPath.row);
            landlineenumberTextField.keyboardType = UIKeyboardTypeNumberPad;
            landlineenumberTextField.returnKeyType = UIReturnKeyNext;
            landlineenumberTextField.font = DEFAULT_FONT(15);
            landlineenumberTextField.textColor = BROWN_COLOR;
            [landlineenumberTextField addTarget:self action:@selector(landlineTextChanged:) forControlEvents:UIControlEventEditingDidEnd];
            [cell.contentView addSubview:landlineenumberTextField];
            
            deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [deleteBtn setFrame:CGRectMake(CGRectGetMaxX(landlineenumberTextField.frame)+5, 2, 40, 40)];
            [deleteBtn setTitle:@"-" forState:UIControlStateNormal];
            [deleteBtn setBackgroundImage:[UIImage imageNamed:@"Button1.png"] forState:UIControlStateNormal];
            [deleteBtn setBackgroundColor:[UIColor clearColor]];
            deleteBtn.tag = 3;
            [deleteBtn addTarget:self action:@selector(deleteLandlineTextFieldBtnTapped:event:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:deleteBtn];
        }
        landlineenumberTextField = (UITextField *)[cell viewWithTag:landlineTextField.tag + (1+indexPath.row)];
        landlineenumberTextField.text = [landlineArray objectAtIndex:indexPath.row];
        return cell;
    }
    return nil;
}
- (IBAction)mobileTextChanged:(UITextField *)sender {
    
    //    mobilenumberTextField.tag = mobileNoTextField.tag + (1+indexPath.row);
    int index = sender.tag - (mobileNoTextField.tag + 1);
    //DebugLog(@"index %d",index);
    //DebugLog(@"mobileArray %@",mobileArray);
    [mobileArray replaceObjectAtIndex:index withObject:sender.text];
    //DebugLog(@"mobileArray %@",mobileArray);
}

- (IBAction)landlineTextChanged:(UITextField *)sender {
    
    int index = sender.tag - (landlineTextField.tag + 1);
    [landlineArray replaceObjectAtIndex:index withObject:sender.text];
}

- (IBAction)addLandlineNumberBtnPressed:(id)sender {
//    if (landlineNumberCount < 2) {
    if ([landlineArray count] < 2) {
//        landlineNumberCount ++;
        [landlineArray addObject:@""];

        [self.LandlineNumberTableView setHidden:FALSE];
        [LandlineNumberTableView beginUpdates];
//        [LandlineNumberTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:landlineNumberCount-1 inSection:0]] withRowAnimation:UITableViewRowAnimationLeft];
        [LandlineNumberTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:[landlineArray count]-1 inSection:0]] withRowAnimation:UITableViewRowAnimationLeft];
        [LandlineNumberTableView endUpdates];
        [self reloadLandlineTableView];
    }
}
-(void)deleteLandlineTextFieldBtnTapped:(id)sender event:(id)event{
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.LandlineNumberTableView];
    NSIndexPath *indexPath = [self.LandlineNumberTableView indexPathForRowAtPoint: currentTouchPosition];
    if (indexPath != nil)
    {
//        landlineNumberCount --;
        [landlineArray removeObjectAtIndex:indexPath.row];
        [LandlineNumberTableView beginUpdates];
        [LandlineNumberTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        [LandlineNumberTableView endUpdates];
        [self reloadLandlineTableView];
    }
}

- (IBAction)addMoblieNumberBtnPressed:(id)sender {
//    if (mobileNumberCount < 2) {
    if ([mobileArray count] < 2) {

//        mobileNumberCount ++;
        [mobileArray addObject:@""];
        [self.mobileNumberTableView setHidden:FALSE];
        
        [mobileNumberTableView beginUpdates];
//        [mobileNumberTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:mobileNumberCount-1 inSection:0]] withRowAnimation:UITableViewRowAnimationLeft];
        [mobileNumberTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:mobileArray.count-1 inSection:0]] withRowAnimation:UITableViewRowAnimationLeft];
        [mobileNumberTableView endUpdates];
        [self reloadMobileTableView];
    }
}

-(void)deleteMobileTextFieldBtnTapped:(id)sender event:(id)event{
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.mobileNumberTableView];
    NSIndexPath *indexPath = [self.mobileNumberTableView indexPathForRowAtPoint: currentTouchPosition];
    if (indexPath != nil)
    {
//        mobileNumberCount --;
        [mobileArray removeObjectAtIndex:indexPath.row];

        [mobileNumberTableView beginUpdates];
        [mobileNumberTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        [mobileNumberTableView endUpdates];
        [self reloadMobileTableView];
    }
}


-(void)reloadMobileTableView
{
    [mobileNumberTableView reloadData];
    [UIView animateWithDuration:0.5 animations:^{
        if ([mobileArray count] == 2) {
            [addMoblieNumberBtn setHidden: YES];
        }else{
            [addMoblieNumberBtn setHidden: NO];
        }
        
        [mobileNumberTableView setFrame:CGRectMake(mobileNumberTableView.frame.origin.x,
                                             mobileNumberTableView.frame.origin.y,
                                             mobileNumberTableView.frame.size.width,
                                             mobileNumberTableView.contentSize.height)];
        [self adjustBottomLayouts];
    } completion:^(BOOL finished) {
        if ([mobileArray count] == 0) {
            [mobileNumberTableView reloadData];
        }
    }];
}

-(void)reloadLandlineTableView{
    [LandlineNumberTableView reloadData];
    [UIView animateWithDuration:0.5 animations:^{
        if ([landlineArray count] == 2) {
            [addLandlineNumberBtn setHidden: YES];
        }else{
            [addLandlineNumberBtn setHidden: NO];
        }
        [LandlineNumberTableView setFrame:CGRectMake(LandlineNumberTableView.frame.origin.x,
                                                   LandlineNumberTableView.frame.origin.y,
                                                   LandlineNumberTableView.frame.size.width,
                                                   LandlineNumberTableView.contentSize.height)];
        [self adjustBottomLayouts];
    } completion:^(BOOL finished) {
        if ([landlineArray count] == 0) {
            [LandlineNumberTableView reloadData];
        }
    }];
}

-(void)adjustBottomLayouts
{
    [UIView animateWithDuration:0.5 animations:^{
                [landlineTextField setFrame:CGRectMake(landlineTextField.frame.origin.x,landlineTextField.frame.origin.y,landlineTextField.frame.size.width,landlineTextField.frame.size.height)];
        
                [addLandlineNumberBtn setFrame:CGRectMake(addLandlineNumberBtn.frame.origin.x,addLandlineNumberBtn.frame.origin.y,addLandlineNumberBtn.frame.size.width,addLandlineNumberBtn.frame.size.height)];
        
                [LandlineNumberTableView setFrame:CGRectMake(LandlineNumberTableView.frame.origin.x,CGRectGetMaxY(landlineTextField.frame)+5,LandlineNumberTableView.frame.size.width,LandlineNumberTableView.frame.size.height)];
        
                [imgSeperator1 setFrame:CGRectMake(imgSeperator1.frame.origin.x,CGRectGetMaxY(LandlineNumberTableView.frame)+5,imgSeperator1.frame.size.width,imgSeperator1.frame.size.height)];

                [mobileNoTextField setFrame:CGRectMake(mobileNoTextField.frame.origin.x,CGRectGetMaxY(imgSeperator1.frame)+5,mobileNoTextField.frame.size.width,mobileNoTextField.frame.size.height)];
        
                [addMoblieNumberBtn setFrame:CGRectMake(addMoblieNumberBtn.frame.origin.x,CGRectGetMaxY(imgSeperator1.frame)+1,addMoblieNumberBtn.frame.size.width,addMoblieNumberBtn.frame.size.height)];

                [mobileNumberTableView setFrame:CGRectMake(mobileNumberTableView.frame.origin.x,CGRectGetMaxY(mobileNoTextField.frame)+5,mobileNumberTableView.frame.size.width,mobileNumberTableView.frame.size.height)];
        
//                [imgSeperator2 setFrame:CGRectMake(imgSeperator2.frame.origin.x,CGRectGetMaxY(mobileNumberTableView.frame)+5,imgSeperator2.frame.size.width,imgSeperator2.frame.size.height)];
//                        
//                [faxNoTextField setFrame:CGRectMake(faxNoTextField.frame.origin.x,CGRectGetMaxY(imgSeperator2.frame)+5,faxNoTextField.frame.size.width,faxNoTextField.frame.size.height)];
//
//                [imgSeperator3 setFrame:CGRectMake(imgSeperator3.frame.origin.x,CGRectGetMaxY(faxNoTextField.frame)+5,imgSeperator3.frame.size.width,imgSeperator3.frame.size.height)];
//                        
//                [tollfreeTextField setFrame:CGRectMake(tollfreeTextField.frame.origin.x,CGRectGetMaxY(imgSeperator3.frame)+5,tollfreeTextField.frame.size.width,tollfreeTextField.frame.size.height)];
        
                [imgSeperator4 setFrame:CGRectMake(imgSeperator4.frame.origin.x,CGRectGetMaxY(mobileNumberTableView.frame)+5,imgSeperator4.frame.size.width,imgSeperator4.frame.size.height)];

                [emailTextField setFrame:CGRectMake(emailTextField.frame.origin.x,CGRectGetMaxY(imgSeperator4.frame)+5,emailTextField.frame.size.width,emailTextField.frame.size.height)];

                [imgSeperator5 setFrame:CGRectMake(imgSeperator5.frame.origin.x,CGRectGetMaxY(emailTextField.frame)+5,imgSeperator5.frame.size.width,imgSeperator5.frame.size.height)];

                [websiteTextField setFrame:CGRectMake(websiteTextField.frame.origin.x,CGRectGetMaxY(imgSeperator5.frame)+5,websiteTextField.frame.size.width,websiteTextField.frame.size.height)];
        
                [imgSeperator6 setFrame:CGRectMake(imgSeperator6.frame.origin.x,CGRectGetMaxY(websiteTextField.frame)+5,imgSeperator6.frame.size.width,imgSeperator6.frame.size.height)];
        
                [txtFbUrl setFrame:CGRectMake(txtFbUrl.frame.origin.x,CGRectGetMaxY(imgSeperator6.frame)+5,txtFbUrl.frame.size.width,txtFbUrl.frame.size.height)];
                
                [imgSeperator7 setFrame:CGRectMake(imgSeperator7.frame.origin.x,CGRectGetMaxY(txtFbUrl.frame)+5,imgSeperator7.frame.size.width,imgSeperator7.frame.size.height)];
                
                [txtTwitterUrl setFrame:CGRectMake(txtTwitterUrl.frame.origin.x,CGRectGetMaxY(imgSeperator7.frame)+5,txtTwitterUrl.frame.size.width,txtTwitterUrl.frame.size.height)];
                
                [imgSeperator8 setFrame:CGRectMake(imgSeperator8.frame.origin.x,CGRectGetMaxY(txtTwitterUrl.frame)+5,imgSeperator8.frame.size.width,imgSeperator8.frame.size.height)];
        
        
        double tempcontentSizeHeight =  CGRectGetMaxY(txtTwitterUrl.frame)+10;
        //DebugLog(@"tempcontentSizeHeight %f",tempcontentSizeHeight);
        [contactBackScrollView setContentSize:CGSizeMake(self.contactBackScrollView.frame.size.width,tempcontentSizeHeight)];
    } completion:^(BOOL finished) {
    }];
}

-(void) popAddUpdateController
{
    DebugLog(@"popToRootViewControllerAnimated:YES");
    if (backAlert != nil && backAlert.isVisible) {
        [backAlert dismissWithClickedButtonIndex:0 animated:YES];
        backAlert = nil;
    }
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)setViewControllerTitle:(int)lpage
{
    switch (lpage) {
        case 0:
        {
            self.title = @"Location";
        }
            break;
            
        case 1:
        {
            self.title = @"Details";
        }
            break;
            
        case 2:
        {
            self.title = @"Contact";
        }
            break;
            
        case 3:
        {
            self.title = @"Description";
        }
            break;
            
        case 4:
        {
            self.title = @"Operations";
        }
            break;
            
        case 5:
        {
            self.title = @"Gallery";
        }
            break;
            
        default:
            break;
    }
}
@end
