//
//  INKidViewController.h
//  shoplocal
//
//  Created by Kirti Nikam on 10/10/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import "FaceBookShareViewController.h"
#import "INCustomerLoginViewController.h"

@class INAllStoreObj;
@interface INKidViewController : UIViewController <UIActionSheetDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,FacebookProtocolDelegate,INCustomerLoginViewControllerDelegate>
{
    INAllStoreObj *storeObj;
    BOOL isDragging;
    BOOL isLoading;
    BOOL isAdding;
    sqlite3 *shoplocalDB;
    MBProgressHUD *hud;
}
@property (nonatomic, readwrite) int isStoreSearch;
@property (strong, nonatomic) IBOutlet UIImageView *bgImageView;

@property (strong, nonatomic) IBOutlet UITableView *kidTableView;
@property (strong, nonatomic) NSMutableArray *kidsArray;
@property (nonatomic, retain) UIView *refreshHeaderView;
@property (nonatomic, retain) UILabel *refreshLabel;
@property (nonatomic, retain) UIImageView *refreshArrow;
@property (nonatomic, retain) UIActivityIndicatorView *refreshSpinner;
@property (readwrite, nonatomic) NSInteger totalPage;
@property (readwrite, nonatomic) NSInteger totalRecord;
@property (readwrite, nonatomic) NSInteger currentPage;
@property (strong, nonatomic) IBOutlet UIView *countHeaderView;
@property (strong, nonatomic) IBOutlet UILabel *lblpullToRefresh;
@property (strong, nonatomic) IBOutlet UILabel *countlbl;

//*****Pull TO Refresh
@property (nonatomic, copy) NSString *textPull;
@property (nonatomic, copy) NSString *textRelease;
@property (nonatomic, copy) NSString *textLoading;

@property (nonatomic, readwrite) NSInteger currentSelectedRow;

@end
