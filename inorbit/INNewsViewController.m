//
//  INNewsViewController.m
//  shoplocal
//
//  Created by Rishi on 11/11/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INNewsViewController.h"
#import "constants.h"
#import "INAppDelegate.h"
#import "INBroadcastDetailObj.h"
#import "INCustBroadCastDetailsViewController.h"

#define NEWS_BROADCAST(PLACEID,PAGE) [NSString stringWithFormat:@"%@%@%@broadcast_api/broadcasts?place_id=%@&page=%@&count=50",LIVE_SERVER,URL_PREFIX,API_VERSION,PLACEID,PAGE]
#define THUMBNAIL_LINK(IMG_LINK) [NSString stringWithFormat:@"%@%@%@",LIVE_SERVER,URL_PREFIX,IMG_LINK]

@interface INNewsViewController ()
@property(strong) NSDictionary *splashJson;
@end

@implementation INNewsViewController
@synthesize newsArray;
@synthesize newsTableView;
@synthesize countlbl;
@synthesize textPull, textRelease, textLoading, refreshHeaderView, refreshLabel, refreshArrow, refreshSpinner;
@synthesize totalPage;
@synthesize totalRecord;
@synthesize currentPage;
@synthesize countHeaderView,lblpullToRefresh;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self setupStrings];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                             style:UIBarButtonItemStylePlain
                                                            target:nil
                                                            action:nil];
    [[self navigationItem] setBackBarButtonItem:back];
    [self setUI];
    [self setupMenuBarButtonItems];
    //[self addHUDView];
    newsArray = [[NSMutableArray alloc] init];
    [self addPullToRefreshHeader];
    [self loadData];
    
}

#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma set Navigation barbuttons callback
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:MENU_TITLE style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithTitle:MENU_TITLE style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleRightSideMenu)];
    return nil;
}
- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setUI
{
    countHeaderView.backgroundColor =   [UIColor whiteColor];
    countHeaderView.clipsToBounds   = YES;
    
    lblpullToRefresh.font               =   DEFAULT_FONT(15.0);
    lblpullToRefresh.backgroundColor    =   LIGHT_GREEN_COLOR;
    lblpullToRefresh.textColor          =   [UIColor blackColor];
    
    countlbl.font               =   DEFAULT_FONT(15.0);
    countlbl.backgroundColor    =   LIGHT_GREEN_COLOR;
    countlbl.textColor          =   [UIColor blackColor];
}

-(void)sendStoreNewsRequest:(NSString *)urlString
{
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    DebugLog(@"request %@",urlRequest.URL);
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"sendStoreBroadcastRequest %@", self.splashJson);
                                                        if(isLoading)
                                                        {
                                                            [self stopLoading];
                                                        }
                                                        if(self.splashJson  != nil) {
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                //                                                                [emptyBroadcastlbl setHidden:TRUE];
                                                                NSDictionary *maindict = [self.splashJson  objectForKey:@"data"];
                                                                self.totalPage = [[maindict  objectForKey:@"total_page"] integerValue];
                                                                self.totalRecord = [[maindict  objectForKey:@"total_record"] integerValue];
                                                                NSArray* jsonArray = [maindict  objectForKey:@"record"];
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *objdict = [jsonArray  objectAtIndex:index];
                                                                    //DebugLog(@"\n\n%@\n\n",objdict);
                                                                    newsObj = [[INBroadcastDetailObj alloc] init];
                                                                    newsObj.ID = [[objdict objectForKey:@"id"] integerValue];
                                                                    newsObj.place_ID = [[objdict objectForKey:@"place_id"] integerValue];
                                                                    newsObj.type = [objdict objectForKey:@"street"];
                                                                    newsObj.title = [objdict objectForKey:@"title"];
                                                                    newsObj.description = [objdict objectForKey:@"description"];
                                                                    newsObj.url = [objdict objectForKey:@"url"];
                                                                    newsObj.tags = [objdict objectForKey:@"tags"];
                                                                    newsObj.date = [objdict objectForKey:@"date"];
                                                                    newsObj.total_like = [objdict objectForKey:@"total_like"];
                                                                    newsObj.total_share = [objdict objectForKey:@"total_share"];
                                                                    newsObj.user_like = [objdict objectForKey:@"user_like"];
                                                                    newsObj.image_url1 = [objdict objectForKey:@"image_url1"];
                                                                    newsObj.image_url2 = [objdict objectForKey:@"image_url2"];
                                                                    newsObj.image_url3 = [objdict objectForKey:@"image_url3"];

                                                                    newsObj.image_title1 = [objdict objectForKey:@"image_title1"];
                                                                    newsObj.image_title2 = [objdict objectForKey:@"image_title2"];
                                                                    newsObj.image_title3 = [objdict objectForKey:@"image_title3"];
                                                                    
                                                                    [newsArray addObject:newsObj];
                                                                    newsObj = nil;
                                                                }
                                                            }
                                                                [newsTableView reloadData];
                                                                [self setCountLabel];
                                                        }
                                                         isAdding = NO;
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK"
                                                                                           otherButtonTitles:nil];
                                                        [av show];
                                                        isAdding = NO;
                                                        if(isLoading)
                                                        {
                                                            [self stopLoading];
                                                        }
                                                        //[CommonCallback hideProgressHud];
                                                    }];
    
    
    
    [operation start];
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [newsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UILabel *lblDay;
    UILabel *lblMonth;
    UILabel *lblTitle;
    
    UIImageView *thumbImg;
    
    UIImageView *likethumbImg;
    UILabel *lblLikeCount;
    
    UIImageView *sharethumbImg;
    UILabel *lblShareCount;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(10.0,5.0,80.0,60.0)];
        thumbImg.tag = 103;
        thumbImg.contentMode = UIViewContentModeScaleToFill;
        [cell.contentView addSubview:thumbImg];
        
        lblDay = [[UILabel alloc] initWithFrame:CGRectMake(10.0,5.0,80.0,50.0)];
        lblDay.text = @"";
        lblDay.tag = 100;
        lblDay.font = [UIFont systemFontOfSize:35];
        lblDay.textAlignment = NSTextAlignmentCenter;
        lblDay.textColor = [UIColor whiteColor];
        lblDay.backgroundColor =  LIGHT_BLUE;
        [cell.contentView addSubview:lblDay];
        
        
        lblMonth = [[UILabel alloc] initWithFrame:CGRectMake(10.0,55.0,80.0,20.0)];
        lblMonth.text = @"";
        lblMonth.tag = 101;
        lblMonth.font = [UIFont systemFontOfSize:15];
        lblMonth.textAlignment = NSTextAlignmentCenter;
        lblMonth.textColor = [UIColor whiteColor];
        lblMonth.backgroundColor =  DARK_YELLOW_COLOR;
        [cell.contentView addSubview:lblMonth];
        
        
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(100.0,15.0,175.0,50.0)];
        lblTitle.text = @"";
        lblTitle.tag = 102;
        lblTitle.numberOfLines = 3;
        lblTitle.font = DEFAULT_BOLD_FONT(15);
        lblTitle.minimumFontSize = 14;
        lblTitle.adjustsFontSizeToFitWidth = TRUE;
        lblTitle.textAlignment = NSTextAlignmentLeft;
        lblTitle.textColor = BROWN_COLOR;
        lblTitle.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblTitle];
        
        likethumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(276.0,10.0,20.0,20.0)];
        likethumbImg.tag = 104;
        likethumbImg.image = [UIImage imageNamed:@"Shoplocal_Post-Title_Fv.png"];
        likethumbImg.contentMode = UIViewContentModeScaleAspectFill;
        [cell.contentView addSubview:likethumbImg];
        
        lblLikeCount = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(likethumbImg.frame)+2,10.0,20.0,20.0)];
        lblLikeCount.text = @"";
        lblLikeCount.tag = 105;
        lblLikeCount.font = DEFAULT_BOLD_FONT(12);
        lblLikeCount.textAlignment = NSTextAlignmentLeft;
        lblLikeCount.textColor = BROWN_COLOR;
        lblLikeCount.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblLikeCount];
        
        sharethumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(276.0,CGRectGetMaxY(likethumbImg.frame)+2,20.0,20.0)];
        sharethumbImg.tag = 106;
        sharethumbImg.image = [UIImage imageNamed:@"Shoplocal_Post-Title_Share.png"];
        sharethumbImg.contentMode = UIViewContentModeScaleAspectFill;
        [cell.contentView addSubview:sharethumbImg];
        
        lblShareCount = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(sharethumbImg.frame)+2,sharethumbImg.frame.origin.y,20.0,20.0)];
        lblShareCount.text = @"";
        lblShareCount.tag = 107;
        lblShareCount.font = DEFAULT_BOLD_FONT(12);
        lblShareCount.textAlignment = NSTextAlignmentLeft;
        lblShareCount.textColor = BROWN_COLOR;
        lblShareCount.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblShareCount];
        
    }
    if ([indexPath row] < [newsArray count]) {
        lblDay      = (UILabel *)[cell viewWithTag:100];
        lblMonth    = (UILabel *)[cell viewWithTag:101];
        lblTitle    = (UILabel *)[cell viewWithTag:102];
        thumbImg    = (UIImageView *)[cell viewWithTag:103];
        likethumbImg    = (UIImageView *)[cell viewWithTag:104];
        lblLikeCount    = (UILabel *)[cell viewWithTag:105];
        sharethumbImg   = (UIImageView *)[cell viewWithTag:106];
        lblShareCount   = (UILabel *)[cell viewWithTag:107];
        
        INBroadcastDetailObj *lbroadcastObj  = [newsArray objectAtIndex:indexPath.row];
        DebugLog(@"%@",lbroadcastObj.date);
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        NSDate *myDate = [[NSDate alloc] init];
        myDate = [dateFormatter dateFromString:lbroadcastObj.date];
        DebugLog(@"MyDate is: %@", myDate);
        
        NSDateFormatter *dayformatter = [[NSDateFormatter alloc] init];
        [dayformatter setDateFormat:@"dd"];
        NSString *dayFromDate = [dayformatter stringFromDate:myDate];
        DebugLog(@"Myday is: %@", dayFromDate);
        lblDay.text = dayFromDate;
        
        NSDateFormatter *monthformatter = [[NSDateFormatter alloc] init];
        [monthformatter setDateFormat:@"MMM"];
        NSString *monthFromDate = [monthformatter stringFromDate:myDate];
        DebugLog(@"MyMonth is: %@", monthFromDate);
        lblMonth.text = monthFromDate;
        
        lblTitle.text = lbroadcastObj.title;
        
        if(lbroadcastObj.image_url1 != (NSString*)[NSNull null] && ![lbroadcastObj.image_url1 isEqualToString:@""]) {
            lblDay.backgroundColor = [UIColor clearColor];
//            [thumbImg setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(lbroadcastObj.image_url1)] placeholderImage:[UIImage imageNamed:@"table_placeholder.png"]];
            __weak UIImageView *thumbImg_ = thumbImg;
//            [thumbImg setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(lbroadcastObj.image_url1)]
//                     placeholderImage:[UIImage imageNamed:@"list_thumbnail.png"]
//                              success:^(UIImage *image) {
//                                  //DebugLog(@"success");
//                              }
//                              failure:^(NSError *error) {
//                                  //DebugLog(@"write error %@", error);
//                                  thumbImg_.image = [UIImage imageNamed:@"list_thumbnail.png"];
//                              }];
            [thumbImg setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(lbroadcastObj.image_url1)] placeholderImage:[UIImage imageNamed:@"list_thumbnail.png"] completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType) {
                if (error) {
                    thumbImg_.image = [UIImage imageNamed:@"list_thumbnail.png"];
                }
            }];
            DebugLog(@"link-%@",THUMBNAIL_LINK(lbroadcastObj.image_url1));
        } else {
            thumbImg.image = nil;
            lblDay.backgroundColor =  LIGHT_BLUE;
        }
        
        [lblLikeCount setHidden:FALSE];
        [likethumbImg setHidden:FALSE];
        lblLikeCount.text = lbroadcastObj.total_like;
        
        [lblShareCount setHidden:FALSE];
        [sharethumbImg setHidden:FALSE];
        lblShareCount.text = lbroadcastObj.total_share;
    }
    return cell;
}


#pragma mark - Table view delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    INBroadcastDetailObj *tempbroadcastObj  = [newsArray objectAtIndex:indexPath.row];
    INCustBroadCastDetailsViewController *postDetailController = [[INCustBroadCastDetailsViewController alloc] initWithNibName:@"INCustBroadCastDetailsViewController" bundle:nil] ;
    
    DebugLog(@"tempbroadcastObj.storename %@ ",self.title);
    postDetailController.title = self.title;//@"Broadcast Detail";
    postDetailController.storeName = self.title;
    postDetailController.postType = 1;
    postDetailController.postlikeType = tempbroadcastObj.user_like;
    postDetailController.postId = tempbroadcastObj.ID;
    postDetailController.postTitle = tempbroadcastObj.title;
    postDetailController.postDescription = tempbroadcastObj.description;
    if ([tempbroadcastObj.image_url1 isEqual:[NSNull null]] || tempbroadcastObj.image_url1 == nil) {
        postDetailController.postimageUrl = @"";
    }else{
        postDetailController.postimageUrl = tempbroadcastObj.image_url1;
    }
    postDetailController.postimageTitle = tempbroadcastObj.image_title1;
    postDetailController.viewOpenedFrom = FROM_STORE_DETAILS;
    postDetailController.likescountString = tempbroadcastObj.total_like;
    postDetailController.sharecountString = tempbroadcastObj.total_share;
    //postDetailController.telArray = telArray;
    postDetailController.SHOW_BUY_BTN = NO;
    postDetailController.placeId = tempbroadcastObj.place_ID;
    [self.navigationController pushViewController:postDetailController animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)pageRequest:(NSString *)pageNum
{
    if([IN_APP_DELEGATE networkavailable])
    {
        isAdding = YES;
        //[self sendAllStoreRequest:SHOP_STORE_LINK([INUserDefaultOperations storeIdForKey:[INUserDefaultOperations getSelectedStore]],pageNum)];
        
        [self sendStoreNewsRequest:NEWS_BROADCAST([IN_APP_DELEGATE getActiveShoplocalPlaceIdFromAreaTable],pageNum)];

        
    }  else   {
        isAdding = NO;
        if(isLoading)
        {
            [self stopLoading];
        }
        [INUserDefaultOperations showOfflineAlert];
    }
}

-(void)loadData
{
    self.currentPage = 1;
    [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
}

- (void)reloadData:(NSNotification *)notification
{
    DebugLog(@"Reload data");
    [newsArray removeAllObjects];
    self.currentPage = 1;
    [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
}

-(void)setCountLabel
{
    self.countlbl.text = [NSString stringWithFormat:@"%d/%d",[newsArray count],self.totalRecord];
    DebugLog(@"-----%@--------",countlbl.text);
}

#pragma mark PULL TO REFRESH
- (void)setupStrings{
    textPull = @"Pull down to refresh...";
    textRelease = @"Release to refresh...";
    textLoading = @"Loading please wait...";
}

- (void)addPullToRefreshHeader {
    refreshHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0 - REFRESH_HEADER_HEIGHT, 320, REFRESH_HEADER_HEIGHT)];
    refreshHeaderView.backgroundColor = [UIColor clearColor];
    
    refreshLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, REFRESH_HEADER_HEIGHT)];
    refreshLabel.backgroundColor = [UIColor clearColor];
    refreshLabel.font = DEFAULT_BOLD_FONT(15);
    refreshLabel.textAlignment = NSTextAlignmentCenter;
    refreshLabel.textColor = [UIColor blackColor];
    refreshArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:PULLTOREFRESH_BARROW]];
    refreshArrow.frame = CGRectMake(floorf((REFRESH_HEADER_HEIGHT - 2) / 2),
                                    (floorf(REFRESH_HEADER_HEIGHT - 44) / 2),
                                    27, 44);
    
    refreshSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    refreshSpinner.frame = CGRectMake(floorf(floorf(REFRESH_HEADER_HEIGHT - 2) / 2), floorf((REFRESH_HEADER_HEIGHT - 20) / 2), 20, 20);
    refreshSpinner.hidesWhenStopped = YES;
    
    [refreshHeaderView addSubview:refreshLabel];
    [refreshHeaderView addSubview:refreshArrow];
    [refreshHeaderView addSubview:refreshSpinner];
    [newsTableView addSubview:refreshHeaderView];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (isLoading) return;
    isDragging = YES;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (isLoading) {
        // Update the content inset, good for section headers
        if (scrollView.contentOffset.y > 0)
            newsTableView.contentInset = UIEdgeInsetsZero;
        else if (scrollView.contentOffset.y >= -REFRESH_HEADER_HEIGHT)
            newsTableView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (isDragging && scrollView.contentOffset.y < 0) {
        // Update the arrow direction and label
        [UIView animateWithDuration:0.25 animations:^{
            if (scrollView.contentOffset.y < -REFRESH_HEADER_HEIGHT) {
                // User is scrolling above the header
                refreshLabel.text = self.textRelease;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
            } else {
                // User is scrolling somewhere within the header
                refreshLabel.text = self.textPull;
                refreshLabel.textColor = BROWN_COLOR;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
            }
        }];
    } else {
        CGFloat height = scrollView.frame.size.height;
        
        CGFloat contentYoffset = scrollView.contentOffset.y;
        
        CGFloat distanceFromBottom = scrollView.contentSize.height - contentYoffset;
        
        if(distanceFromBottom <= height)
        {
            if(isAdding==TRUE) {
                DebugLog(@"<<<<<<<<<<<<<<<");
                return;
            } else {
                DebugLog(@"end of the table page = %d", self.currentPage);
                if(self.currentPage < self.totalPage)
                {
                    self.currentPage = self.currentPage + 1;
                    [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
                }
            }
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (isLoading) return;
    isDragging = NO;
    if (scrollView.contentOffset.y <= -REFRESH_HEADER_HEIGHT) {
        // Released above the header
        [self startLoading];
    }
}

- (void)startLoading {
    isLoading = YES;
    // Show the header
    [UIView animateWithDuration:0.3 animations:^{
        newsTableView.contentInset = UIEdgeInsetsMake(REFRESH_HEADER_HEIGHT, 0, 0, 0);
        refreshLabel.text = self.textLoading;
        refreshArrow.hidden = YES;
        [refreshSpinner startAnimating];
    }];
    if([IN_APP_DELEGATE networkavailable])
    {
        [newsArray removeAllObjects];
        self.currentPage = 1;
        [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
    }
    else
    {
        [self stopLoading];
    }
    // Refresh action!
    //[self refresh];
}

- (void)stopLoading {
    isLoading = NO;
    // Hide the header
    [UIView animateWithDuration:0.3 animations:^{
        newsTableView.contentInset = UIEdgeInsetsZero;
        [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
    }
                     completion:^(BOOL finished) {
                         [self performSelector:@selector(stopLoadingComplete)];
                     }];
}

- (void)stopLoadingComplete {
    // Reset the header
    refreshLabel.text = self.textPull;
    refreshArrow.hidden = NO;
    [refreshSpinner stopAnimating];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setNewsTableView:nil];
    [self setCountHeaderView:nil];
    [self setLblpullToRefresh:nil];
    [self setCountlbl:nil];
    [super viewDidUnload];
}

@end
