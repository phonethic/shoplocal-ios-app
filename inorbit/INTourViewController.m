//
//  INTourViewController.m
//  shoplocal
//
//  Created by Kirti Nikam on 21/11/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INTourViewController.h"
#import "constants.h"
#import "INAppDelegate.h"
#import "INCustomerLoginViewController.h"

@interface INTourViewController ()

@end

@implementation INTourViewController
@synthesize animationView,lblHeader,lblDescription;
@synthesize loginView;
@synthesize fbLoginBtn,skipBtn,signUpShoplocalBtn;
@synthesize textArray;
@synthesize sellBtn;
@synthesize txtDescription;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[self navigationController] setNavigationBarHidden:YES animated:NO];

    [self setUI];
    loginView.alpha = 0.0;
    animationView.alpha = 1.0;
    
    textArray = [[NSMutableArray alloc] init];
    [textArray addObject:@"Shops"];
    [textArray addObject:@"Local Offers"];
    [textArray addObject:@"Bakeries"];
    [textArray addObject:@"Great Places To Eat"];
    [textArray addObject:@"Coffee Shops"];
    [textArray addObject:@"Pubs"];
    [textArray addObject:@"Karaoke Nights Clubs"];
    [textArray addObject:@"Homemade Cookies"];
    [textArray addObject:@"Organic Food"];
    [textArray addObject:@"Grocery Stores"];
    [textArray addObject:@"Florists"];
    [textArray addObject:@"Chemists"];
    [textArray addObject:@"Hair and Beauty Salons"];
    [textArray addObject:@"Gyms"];
    [textArray addObject:@"Health Spas"];
    [textArray addObject:@"Fitness Trainers"];
    [textArray addObject:@"Music Teachers"];
    [textArray addObject:@"Book Stores"];
    [textArray addObject:@"Personalized Gift Shops"];
    [textArray addObject:@"Interior Decor"];
    [textArray addObject:@"Car Wash"];
    [textArray addObject:@"The best in your neighbourhood"];

    lblHeader.text = @"discover";
    lblDescription.text = [textArray objectAtIndex:0];
    lblDescription.alpha = 0.0;
    
    txtDescription.text = [textArray objectAtIndex:0];
    txtDescription.alpha = 0.0;
    //[self startAnimation:0];
    [self performSelector:@selector(startAnimation:) withObject:[NSNumber numberWithInt:0] afterDelay:0.2];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setAnimationView:nil];
    [self setLogoImageView:nil];
    [self setLblHeader:nil];
    [self setLblDescription:nil];
    [self setLoginView:nil];
    [self setFbLoginBtn:nil];
    [self setSignUpShoplocalBtn:nil];
    [self setSkipBtn:nil];
    [self setShoplocalTextImageView:nil];
    [self setSellBtn:nil];
    [self setTxtDescription:nil];
    [super viewDidUnload];
}

#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


#pragma internal methods
-(void)setUI{
    self.view.backgroundColor = [UIColor colorWithRed:161/255.0 green:195/255.0 blue:101/255.0 alpha:1.0];
    self.animationView.backgroundColor  = [UIColor clearColor];
    self.loginView.backgroundColor      = [UIColor clearColor];
    
    lblHeader.textColor         = [UIColor whiteColor];
    lblHeader.backgroundColor   = [UIColor clearColor];
    lblHeader.font              = DEFAULT_BOLD_FONT(22);

    lblDescription.textColor        = [UIColor whiteColor];
    lblDescription.backgroundColor  = [UIColor clearColor];
    lblDescription.numberOfLines    = 4;
    lblDescription.font             = DEFAULT_FONT(28);
    
    txtDescription.font             = DEFAULT_FONT(30);
    txtDescription.backgroundColor  = [UIColor clearColor];
    txtDescription.textColor        = [UIColor whiteColor];
    
    fbLoginBtn.titleLabel.font          = DEFAULT_FONT(20);
    signUpShoplocalBtn.titleLabel.font  = DEFAULT_FONT(20);
    skipBtn.titleLabel.font             = DEFAULT_FONT(20);
    sellBtn.titleLabel.font             = DEFAULT_FONT(20);
    
    fbLoginBtn.layer.cornerRadius           = 8.0;
    signUpShoplocalBtn.layer.cornerRadius   = 8.0;
    
    [fbLoginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [fbLoginBtn setTitleColor:[UIColor colorWithWhite:0.5 alpha:0.5] forState:UIControlStateHighlighted];
    
    [signUpShoplocalBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [signUpShoplocalBtn setTitleColor:[UIColor colorWithWhite:0.5 alpha:0.5] forState:UIControlStateHighlighted];
    
    [skipBtn setTitleColor:[UIColor colorWithRed:101/255.0 green:123/255.0 blue:64/255.0 alpha:1.0] forState:UIControlStateNormal];
    [skipBtn setTitleColor:[UIColor colorWithWhite:0.5 alpha:0.5] forState:UIControlStateHighlighted];
    
    [sellBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sellBtn setTitleColor:[UIColor colorWithWhite:0.5 alpha:0.5] forState:UIControlStateHighlighted];

    fbLoginBtn.backgroundColor          = [UIColor colorWithRed:64/255.0 green:96/255.0 blue:140/255.0 alpha:1.0];
    signUpShoplocalBtn.backgroundColor  = [UIColor colorWithRed:122/255.0 green:148/255.0 blue:77/255.0 alpha:1.0];
    skipBtn.backgroundColor             = [UIColor clearColor];
    sellBtn.backgroundColor             = [UIColor clearColor];
}

-(void)startAnimation:(NSNumber*)index{
    DebugLog(@"index %d",[index intValue]);
    if ([index intValue] == 0) {
        [UIView animateWithDuration:1.0 delay:0.0 options:UIViewAnimationOptionCurveLinear|UIViewAnimationOptionAllowUserInteraction animations:^{
            txtDescription.text = [textArray objectAtIndex:[index intValue]];
            txtDescription.alpha = 1.0;
        } completion:^(BOOL finished){
            [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveLinear|UIViewAnimationOptionAllowUserInteraction animations:^{
                txtDescription.alpha = 0.0;
            } completion:^(BOOL finished){
                [self startAnimation:[NSNumber numberWithInt:[index intValue]+1]];
            }];
        }];
    }else{
        [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveLinear|UIViewAnimationOptionAllowUserInteraction animations:^{
            txtDescription.alpha = 1.0;
            txtDescription.text = [textArray objectAtIndex:[index intValue]];
        } completion:^(BOOL finished){
            if ([index intValue] == textArray.count - 2) {
                [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveLinear|UIViewAnimationOptionAllowUserInteraction animations:^{
                    txtDescription.alpha = 0.0;
                } completion:^(BOOL finished){
                    [UIView animateWithDuration:1.0 delay:0.0 options:UIViewAnimationOptionCurveEaseIn|UIViewAnimationOptionAllowUserInteraction animations:^{
                        txtDescription.alpha = 1.0;
                        txtDescription.text = [textArray objectAtIndex:[index intValue]+1];
                    } completion:^(BOOL finished){
                        [UIView transitionWithView:animationView duration:0.3 options:UIViewAnimationOptionTransitionNone | UIViewAnimationOptionCurveEaseIn|UIViewAnimationOptionAllowUserInteraction animations:^{
                            animationView.alpha = 0.0;
                        } completion:^(BOOL finished){
                            [UIView transitionWithView:loginView duration:0.8 options:UIViewAnimationOptionTransitionNone |UIViewAnimationOptionCurveEaseIn|UIViewAnimationOptionAllowUserInteraction animations:^{
                                loginView.alpha = 1.0;
                            } completion:^(BOOL finished){
                                [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveLinear|UIViewAnimationOptionAllowUserInteraction animations:^{
                                    txtDescription.alpha = 0.0;
                                } completion:^(BOOL finished){
                                }];
                            }];
                        }];
                    }];
                }];
            }else{
                [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveLinear|UIViewAnimationOptionAllowUserInteraction animations:^{
                    txtDescription.alpha = 0.0;
                } completion:^(BOOL finished){
                    [self startAnimation:[NSNumber numberWithInt:[index intValue]+1]];
                }];
            }
        }];
    }
}



- (IBAction)fbLoginBtnPressed:(id)sender {
    DebugLog(@"fbLoginBtnPressed");
    if ([IN_APP_DELEGATE networkavailable]) {
        [self showLoginModal:LOGIN_TYPE_FB];
        [INEventLogger logEvent:@"Tour_LoginFB"];
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

- (IBAction)signUpShoplocalBtnPressed:(id)sender {
    DebugLog(@"signUpShoplocalBtnPressed");
    if ([IN_APP_DELEGATE networkavailable]) {
        [self showLoginModal:LOGIN_TYPE_SHOPLOCAL];
        [INEventLogger logEvent:@"Tour_SignUp"];
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

- (IBAction)skipBtnPressed:(id)sender {
    DebugLog(@"skipBtnPressed");
    [INEventLogger logEvent:@"Tour_Later"];
    [INUserDefaultOperations setTourFinishedState:1];
    [IN_APP_DELEGATE animationStop];
}

- (IBAction)sellBtnPressed:(id)sender {
    [INEventLogger logEvent:@"Tour_Sell"];
    [IN_APP_DELEGATE switchViewController];
}

-(void)showLoginModal:(int)loginType{
    INCustomerLoginViewController *custLoginController = [[INCustomerLoginViewController alloc] initWithNibName:@"INCustomerLoginViewController" bundle:nil] ;
    custLoginController.delegate = self;
    custLoginController.loginType = loginType;
    UINavigationController *loginnavBar=[[UINavigationController alloc]initWithRootViewController:custLoginController];
    UIButton *moreButton1 =  [UIButton buttonWithType:UIButtonTypeCustom];
    [moreButton1 setTitle:@"Cancel" forState:UIControlStateNormal];
    moreButton1.titleLabel.font = [UIFont systemFontOfSize:17.0];
    //[moreButton1 setBackgroundImage:[UIImage imageNamed:@"Transparent_Button.png"] forState:UIControlStateNormal];
    [moreButton1 addTarget:self action:@selector(dismissLoginView) forControlEvents:UIControlEventTouchUpInside];
    [moreButton1 setFrame:CGRectMake(250, 7, 70, 32)];
    [loginnavBar.navigationBar addSubview:moreButton1];
    [self.navigationController presentViewController:loginnavBar animated:YES completion:nil];
}

-(void)dismissLoginView
{
    DebugLog(@"dismissLoginView %@",[self.navigationController viewControllers]);
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"Dismiss");
        if([INUserDefaultOperations isCustomerLoggedIn])
        {
            [INEventLogger logEvent:@"Tour_Location"];
            DebugLog(@"isCustomerLoggedIn == TRUE");
            [INUserDefaultOperations setTourFinishedState:1];
            [IN_APP_DELEGATE animationStop];
        }
    }];
}
@end
