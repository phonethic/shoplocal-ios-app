//
//  INAllPostsViewController.m
//  shoplocal
//
//  Created by Rishi on 20/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INAllPostsViewController.h"
#import "UIImageView+WebCache.h"
#import "INBroadcastDetailObj.h"
#import "INPostDetailViewController.h"
#import "constants.h"
#import "CommonCallback.h"
#import "MCSwipeTableViewCell.h"
#import "INBroadcastViewController.h"
#import "UIImageView+WebCache.h"
#import "INAppDelegate.h"


#define STORE_BROADCAST(ID) [NSString stringWithFormat:@"%@%@%@broadcast_api/broadcasts?place_id=%d",LIVE_SERVER,URL_PREFIX,API_VERSION,ID]
#define THUMBNAIL_LINK(IMG_LINK) [NSString stringWithFormat:@"%@%@%@",LIVE_SERVER,URL_PREFIX,IMG_LINK]

@interface INAllPostsViewController () <MCSwipeTableViewCellDelegate>
@property(strong) NSDictionary *splashJson;
@end

@implementation INAllPostsViewController
@synthesize broadcastArray;
@synthesize storeId;
@synthesize imageUrl;
@synthesize postsTableView;
@synthesize storeName;
@synthesize textPull, textRelease, textLoading, refreshHeaderView, refreshLabel, refreshArrow, refreshSpinner;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self setupStrings];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //[self addHUDView];
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                             style:UIBarButtonItemStylePlain
                                                            target:nil
                                                            action:nil];
    [[self navigationItem] setBackBarButtonItem:back];
    broadcastArray = [[NSMutableArray alloc] init];
     [self addPullToRefreshHeader];
    [self sendStoreBroadcastRequest];
}

-(void) addAlertView{
    sialertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@""];
    sialertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
    sialertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
    [sialertView addButtonWithTitle:@"OK"
                               type:SIAlertViewButtonTypeCancel
                            handler:^(SIAlertView *alert) {
                            }];
}

-(void)showAddEventsAlert{
    SIAlertView *sialertView1 = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Do you want to reuse this post ?"];
    sialertView1.transitionStyle = SIAlertViewTransitionStyleDropDown;
    sialertView1.backgroundStyle = SIAlertViewBackgroundStyleGradient;
    [sialertView1 addButtonWithTitle:@"Yes"
                                type:SIAlertViewButtonTypeDestructive
                             handler:^(SIAlertView *alert) {
                                 DebugLog(@"Fill dates Clicked");
                             }];
    [sialertView1 addButtonWithTitle:@"NO"
                                type:SIAlertViewButtonTypeCancel
                             handler:^(SIAlertView *alert) {
                                 DebugLog(@"Skip Clicked");
                             }];
    [sialertView1 show];
}


#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void)addHUDView
{
    hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:hud];
	hud.dimBackground = NO;
    hud.labelText = @"Loading";
	hud.detailsLabelText = @"Please wait ...";
    [hud hide:NO];
}

-(void)removeHUDView
{
    [hud removeFromSuperview];
    hud = nil;
}

//- (void)dealloc {
//    [self removeHUDView];
//}

-(void)sendStoreBroadcastRequest
{
    DebugLog(@"%d",self.storeId);
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:STORE_BROADCAST(self.storeId)]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setValue:[INUserDefaultOperations getMerchantAuthId] forHTTPHeaderField:@"user_id"];
    [urlRequest setValue:[INUserDefaultOperations getMerchantAuthCode] forHTTPHeaderField:@"auth_id"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"%@", self.splashJson);
                                                        if(self.splashJson  != nil) {
                                                            [broadcastArray removeAllObjects];
                                                            isAdding = NO;
                                                            if(isLoading)
                                                            {
                                                                [self stopLoading];
                                                            }

                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSDictionary *maindict = [self.splashJson  objectForKey:@"data"];
                                                                //self.totalPage = [[maindict  objectForKey:@"total_page"] integerValue];
                                                                //self.totalRecord = [[maindict  objectForKey:@"total_record"] integerValue];
                                                                NSArray* jsonArray = [maindict  objectForKey:@"record"];
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *objdict = [jsonArray  objectAtIndex:index];
                                                                    //DebugLog(@"\n\n%@\n\n",objdict);
                                                                    broadcastObj = [[INBroadcastDetailObj alloc] init];
                                                                    broadcastObj.ID = [[objdict objectForKey:@"id"] integerValue];
                                                                    broadcastObj.place_ID = [[objdict objectForKey:@"place_id"] integerValue];
                                                                    broadcastObj.type = [objdict objectForKey:@"street"];
                                                                    broadcastObj.title = [objdict objectForKey:@"title"];
                                                                    broadcastObj.description = [objdict objectForKey:@"description"];
                                                                    broadcastObj.url = [objdict objectForKey:@"url"];
                                                                    broadcastObj.tags = [objdict objectForKey:@"tags"];
                                                                    broadcastObj.date = [objdict objectForKey:@"date"];
                                                                    broadcastObj.is_offered       = [objdict objectForKey:@"is_offered"];
                                                                    broadcastObj.offerdate    = [objdict objectForKey:@"offer_date_time"];

                                                                    broadcastObj.image_url1 = [objdict objectForKey:@"image_url1"];
                                                                    broadcastObj.image_url2 = [objdict objectForKey:@"image_url2"];
                                                                    broadcastObj.image_url3 = [objdict objectForKey:@"image_url3"];
                                                                    
                                                                    broadcastObj.thumb_url1 = [objdict objectForKey:@"thumb_url1"];
                                                                    broadcastObj.thumb_url2 = [objdict objectForKey:@"thumb_url2"];
                                                                    broadcastObj.thumb_url3 = [objdict objectForKey:@"thumb_url3"];
                                                                    
//                                                                    NSArray* imageArr = [objdict objectForKey:@"images"];
//                                                                    if([imageArr count]>0) {
//                                                                        broadcastObj.image_url = [imageArr objectAtIndex:1];
//                                                                    }
                                                                    broadcastObj.image_title1 = [objdict objectForKey:@"image_title1"];
                                                                    broadcastObj.image_title2 = [objdict objectForKey:@"image_title2"];
                                                                    broadcastObj.image_title3 = [objdict objectForKey:@"image_title3"];
                                                                    
                                                                     broadcastObj.total_like = [objdict objectForKey:@"total_like"];
                                                                    broadcastObj.total_share = [objdict objectForKey:@"total_share"];
                                                                    broadcastObj.total_view = [objdict objectForKey:@"total_view"];
                                                                    [broadcastArray addObject:broadcastObj];
                                                                    broadcastObj = nil;
                                                                } 
                                                            } else {
                                                                if(self.splashJson != nil && [[self.splashJson  objectForKey:@"success"] isEqualToString:@"false"]) {
                                                                    if([self.splashJson  objectForKey:@"code"] != nil &&  [[self.splashJson  objectForKey:@"code"] isEqualToString:MERCHANT_INVALID_USER_AUTH_CODE])
                                                                    {
                                                                        if([INUserDefaultOperations isMerchantLoggedIn]){
                                                                            [INUserDefaultOperations showAlert:MERCHANT_INVALID_USER_AUTH_MESSAGE];
                                                                            [INUserDefaultOperations clearMerchantDetails];
                                                                            [self performSelector:@selector(popAllPostsController) withObject:nil afterDelay:2.0];
                                                                        }
                                                                    } else {
                                                                         [INUserDefaultOperations showAlert:[self.splashJson  objectForKey:@"message"]];
                                                                    }
                                                                }
                                                            }
//                                                            if([broadcastArray count] > 0)
//                                                            {
                                                                [postsTableView reloadData];
//                                                            }
                                                        }
                                                        
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                        [av show];
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        isAdding = NO;
                                                        if(isLoading)
                                                        {
                                                            [self stopLoading];
                                                        }

                                                    }];
    
    [operation start];
}

#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [broadcastArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UIView *topView;
    
    UILabel *lblDay;
    UILabel *lblMonth;
    UILabel *lblTitle;
    UIImageView *thumbImg;
    
    UILabel *lblLikeCount;
    UIImageView *likethumbImg;
    
    UILabel *lblShareCount;
    UIImageView *sharethumbImg;
    
    UILabel *lblViewsCount;
    UIImageView *viewsthumbImg;
    
    UIImageView *offerthumbImg;
    
    MCSwipeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[MCSwipeTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        
        // Create the top view
        topView = [[UIView alloc] initWithFrame:CGRectMake(5, 5, 310, 96)];
        topView.tag = 111;
        topView.backgroundColor = [UIColor whiteColor];
        topView.clipsToBounds = YES;
        
        thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(5.0,5.0,80.0,60.0)];
        thumbImg.tag = 103;
        thumbImg.contentMode = UIViewContentModeScaleToFill;
        //[cell.contentView addSubview:thumbImg];
        [topView addSubview:thumbImg];
        
        lblDay = [[UILabel alloc] initWithFrame:CGRectMake(5.0,5.0,80.0,60.0)];
        lblDay.text = @"";
        lblDay.tag = 100;
        lblDay.font = [UIFont systemFontOfSize:35];
        lblDay.textAlignment = NSTextAlignmentCenter;
        lblDay.textColor = [UIColor whiteColor];
        lblDay.backgroundColor =  LIGHT_BLUE;
        //[cell.contentView addSubview:lblDay];
        [topView addSubview:lblDay];
        
        lblMonth = [[UILabel alloc] initWithFrame:CGRectMake(5.0,65.0,80.0,25.0)];
        lblMonth.text = @"";
        lblMonth.tag = 101;
        lblMonth.font = [UIFont systemFontOfSize:15];
        lblMonth.textAlignment = NSTextAlignmentCenter;
        lblMonth.textColor = [UIColor whiteColor];
        lblMonth.backgroundColor =  DARK_YELLOW_COLOR;
        //[cell.contentView addSubview:lblMonth];
        [topView addSubview:lblMonth];
        
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame)+5,25.0,170.0,50.0)];
        lblTitle.text = @"";
        lblTitle.tag = 102;
        lblTitle.numberOfLines = 3;
        lblTitle.font = DEFAULT_BOLD_FONT(15);
        lblTitle.minimumScaleFactor = 14;
        lblTitle.adjustsFontSizeToFitWidth = TRUE;
        lblTitle.textAlignment = NSTextAlignmentLeft;
        lblTitle.textColor = BROWN_COLOR;
        lblTitle.backgroundColor =  [UIColor clearColor];
        //[cell.contentView addSubview:lblTitle];
        [topView addSubview:lblTitle];
        
        likethumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lblTitle.frame)+5,20.0,15.0,15.0)];
        likethumbImg.tag = 104;
        likethumbImg.image = [UIImage imageNamed:@"Shoplocal_Post-Title_Fv.png"];
        likethumbImg.contentMode = UIViewContentModeScaleAspectFill;
        //[cell.contentView addSubview:likethumbImg];
        [topView addSubview:likethumbImg];
        
        lblLikeCount = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(likethumbImg.frame),20.0,30.0,15.0)];
        lblLikeCount.text = @"";
        lblLikeCount.tag = 105;
        lblLikeCount.font = DEFAULT_BOLD_FONT(12);
        lblLikeCount.textAlignment = NSTextAlignmentCenter;
        lblLikeCount.textColor = BROWN_COLOR;
        lblLikeCount.backgroundColor =  [UIColor clearColor];
        //[cell.contentView addSubview:lblLikeCount];
        [topView addSubview:lblLikeCount];
        
        sharethumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lblTitle.frame)+5,CGRectGetMaxY(likethumbImg.frame)+4,15.0,15.0)];
        sharethumbImg.tag = 106;
        sharethumbImg.image = [UIImage imageNamed:@"Shoplocal_Post-Title_Share.png"];
        sharethumbImg.contentMode = UIViewContentModeScaleAspectFill;
        //[cell.contentView addSubview:sharethumbImg];
        [topView addSubview:sharethumbImg];
        
        lblShareCount = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(sharethumbImg.frame),sharethumbImg.frame.origin.y,30.0,15.0)];
        lblShareCount.text = @"";
        lblShareCount.tag = 107;
        lblShareCount.font = DEFAULT_BOLD_FONT(12);
        lblShareCount.textAlignment = NSTextAlignmentCenter;
        lblShareCount.textColor = BROWN_COLOR;
        lblShareCount.backgroundColor =  [UIColor clearColor];
        //[cell.contentView addSubview:lblShareCount];
          [topView addSubview:lblShareCount];
        
        viewsthumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lblTitle.frame)+5,CGRectGetMaxY(sharethumbImg.frame)+4,15.0,15.0)];
        viewsthumbImg.tag = 108;
        viewsthumbImg.image = [UIImage imageNamed:@"Shoplocal_Post-Title_View.png"];
        viewsthumbImg.contentMode = UIViewContentModeScaleAspectFill;
        //[cell.contentView addSubview:viewsthumbImg];
          [topView addSubview:viewsthumbImg];
        
        lblViewsCount = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(viewsthumbImg.frame),viewsthumbImg.frame.origin.y,30.0,15.0)];
        lblViewsCount.text = @"";
        lblViewsCount.tag = 109;
        lblViewsCount.font = DEFAULT_BOLD_FONT(12);
        lblViewsCount.textAlignment = NSTextAlignmentCenter;
        lblViewsCount.textColor = BROWN_COLOR;
        lblViewsCount.backgroundColor =  [UIColor clearColor];
        //[cell.contentView addSubview:lblViewsCount];
          [topView addSubview:lblViewsCount];
        
        offerthumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(5,8,61.0,40.0)];
        offerthumbImg.tag = 110;
        offerthumbImg.image = [UIImage imageNamed:@"Offer_Icon1.png"];
        offerthumbImg.contentMode = UIViewContentModeScaleAspectFill;
        [topView addSubview:offerthumbImg];

        
        // Add views to contentView
        [cell.contentView addSubview:topView];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = BROWN_OFFWHITE_COLOR;
        cell.selectedBackgroundView = bgColorView;
        
    }
    [cell setDelegate:self];
    [cell setFirstStateIconName:@"mclist.png"
                     firstColor:[UIColor colorWithRed:243.0 / 255.0 green:128.0 / 255.0 blue:89.0 / 255.0 alpha:0.8]
            secondStateIconName:@"mccheck.png"
                    secondColor:[UIColor colorWithRed:189.0 / 255.0 green:214.0 / 255.0 blue:141.0 / 255.0 alpha:1.0]
                  thirdIconName:@"clock.png"
                     thirdColor:[UIColor colorWithRed:254.0 / 255.0 green:217.0 / 255.0 blue:56.0 / 255.0 alpha:1.0]
                 fourthIconName:@"list.png"
                    fourthColor:[UIColor colorWithRed:206.0 / 255.0 green:149.0 / 255.0 blue:98.0 / 255.0 alpha:1.0]];
    //[cell.contentView setBackgroundColor:[UIColor whiteColor]];
    cell.mode = MCSwipeTableViewCellModeSwitch;
    if ([indexPath row] < [broadcastArray count]) {
        topView = (UIView *)[cell viewWithTag:111];
        lblDay = (UILabel *)[topView viewWithTag:100];
        lblMonth = (UILabel *)[topView viewWithTag:101];
        lblTitle = (UILabel *)[topView viewWithTag:102];
        thumbImg = (UIImageView *)[topView viewWithTag:103];
        likethumbImg = (UIImageView *)[topView viewWithTag:104];
        lblLikeCount = (UILabel *)[topView viewWithTag:105];
        sharethumbImg = (UIImageView *)[topView viewWithTag:106];
        lblShareCount = (UILabel *)[topView viewWithTag:107];
        viewsthumbImg = (UIImageView *)[topView viewWithTag:108];
        lblViewsCount = (UILabel *)[topView viewWithTag:109];
        offerthumbImg   = (UIImageView *)[topView viewWithTag:110];
        
        INBroadcastDetailObj *lbroadcastObj  = [broadcastArray objectAtIndex:indexPath.row];
        //DebugLog(@"%@",lbroadcastObj.date);
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        NSDate *myDate = [[NSDate alloc] init];
        myDate = [dateFormatter dateFromString:lbroadcastObj.date];
        //DebugLog(@"MyDate is: %@", myDate);
        
        NSDateFormatter *dayformatter = [[NSDateFormatter alloc] init];
        [dayformatter setDateFormat:@"dd"];
        NSString *dayFromDate = [dayformatter stringFromDate:myDate];
        //DebugLog(@"Myday is: %@", dayFromDate);
        lblDay.text = dayFromDate;
        
        NSDateFormatter *monthformatter = [[NSDateFormatter alloc] init];
        [monthformatter setDateFormat:@"MMM"];
        NSString *monthFromDate = [monthformatter stringFromDate:myDate];
        //DebugLog(@"MyMonth is: %@", monthFromDate);
        lblMonth.text = monthFromDate;
        
        lblTitle.text = lbroadcastObj.title;
        DebugLog(@"%@%@",lbroadcastObj.title,lbroadcastObj.image_url1);
        if(lbroadcastObj.thumb_url1 != (NSString*)[NSNull null] && ![lbroadcastObj.thumb_url1 isEqualToString:@""]) {
            lblDay.backgroundColor = [UIColor clearColor];

            __weak UIImageView *thumbImg_ = thumbImg;

            [thumbImg setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(lbroadcastObj.thumb_url1)] placeholderImage:[UIImage imageNamed:@"list_thumbnail.png"] completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType) {
                if (error) {
                    thumbImg_.image = [UIImage imageNamed:@"list_thumbnail.png"];
                }
            }];
            DebugLog(@"link-%@",THUMBNAIL_LINK(lbroadcastObj.thumb_url1));
        } else {
            thumbImg.image = nil;
            lblDay.backgroundColor =  LIGHT_BLUE;
        }
        
        if ([lbroadcastObj.is_offered length] > 0 && [lbroadcastObj.is_offered isEqualToString:@"1"]) {
            offerthumbImg.hidden = FALSE;
        }else{
            offerthumbImg.hidden = TRUE;
        }
        
//        if(lbroadcastObj.total_like != nil && ![lbroadcastObj.total_like isEqualToString:@"0"]) {
            [lblLikeCount setHidden:FALSE];
            [likethumbImg setHidden:FALSE];
            lblLikeCount.text = lbroadcastObj.total_like;
//        } else {
//            [lblLikeCount setHidden:TRUE];
//            [likethumbImg setHidden:TRUE];
//        }
//        if(lbroadcastObj.total_share != nil && ![lbroadcastObj.total_share isEqualToString:@"0"]) {
            [lblShareCount setHidden:FALSE];
            [sharethumbImg setHidden:FALSE];
            lblShareCount.text = lbroadcastObj.total_share;
//        } else {
//            [lblShareCount setHidden:TRUE];
//            [sharethumbImg setHidden:TRUE];
//        }
        
//        if(lbroadcastObj.total_view != nil && ![lbroadcastObj.total_view isEqualToString:@"0"]) {
            [lblViewsCount setHidden:FALSE];
            [viewsthumbImg setHidden:FALSE];
            lblViewsCount.text = lbroadcastObj.total_view;
//        } else {
//            [lblViewsCount setHidden:TRUE];
//            [viewsthumbImg setHidden:TRUE];
//        }
    }
    return cell;
}


#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    INBroadcastDetailObj *tempbroadcastObj  = [broadcastArray objectAtIndex:indexPath.row];
    INPostDetailViewController *postDetailController = [[INPostDetailViewController alloc] initWithNibName:@"INPostDetailViewController" bundle:nil] ;
    postDetailController.title = self.storeName;//@"Broadcast Detail";
    postDetailController.postType = 0;
    postDetailController.postId = tempbroadcastObj.ID;
    postDetailController.postTitle = tempbroadcastObj.title;
    postDetailController.postDescription = tempbroadcastObj.description;
    if ([tempbroadcastObj.image_url1 isEqual:[NSNull null]] || tempbroadcastObj.image_url1 == nil) {
        postDetailController.postimageUrl = @"";
    }else{
        postDetailController.postimageUrl = tempbroadcastObj.image_url1;
    }
    postDetailController.postimageTitle = tempbroadcastObj.image_title1;
    postDetailController.storeName = storeName;
    postDetailController.placeId = self.storeId;
    postDetailController.likescountString = tempbroadcastObj.total_like;
    postDetailController.sharecountString = tempbroadcastObj.total_share;
    postDetailController.viewscountString = tempbroadcastObj.total_view;
    postDetailController.postDateTime       = tempbroadcastObj.date;
    postDetailController.postOfferDateTime  = tempbroadcastObj.offerdate;
    postDetailController.postIsOffered      = tempbroadcastObj.is_offered;
    [self.navigationController pushViewController:postDetailController animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

#pragma mark - MCSwipeTableViewCellDelegate

// When the user starts swiping the cell this method is called
- (void)swipeTableViewCellDidStartSwiping:(MCSwipeTableViewCell *)cell {
    DebugLog(@"Did start swiping the cell!");
}

/*
 // When the user is dragging, this method is called and return the dragged percentage from the border
 - (void)swipeTableViewCell:(MCSwipeTableViewCell *)cell didSwipWithPercentage:(CGFloat)percentage {
 DebugLog(@"Did swipe with percentage : %f", percentage);
 }
 */

- (void)swipeTableViewCell:(MCSwipeTableViewCell *)cell didEndSwipingSwipingWithState:(MCSwipeTableViewCellState)state mode:(MCSwipeTableViewCellMode)mode {
    DebugLog(@"Did end swipping with IndexPath : %@ - MCSwipeTableViewCellState : %d - MCSwipeTableViewCellMode : %d", [self.postsTableView indexPathForCell:cell], state, mode);
    if (state == MCSwipeTableViewCellState2 ) {
        SIAlertView *sialertView1 = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Do you want to reuse this post ?"];
        sialertView1.transitionStyle = SIAlertViewTransitionStyleDropDown;
        sialertView1.backgroundStyle = SIAlertViewBackgroundStyleGradient;
        [sialertView1 addButtonWithTitle:@"Yes"
                                    type:SIAlertViewButtonTypeDestructive
                                 handler:^(SIAlertView *alert) {
                                     DebugLog(@"Fill dates Clicked");
                                     [self showReuseController:[self.postsTableView indexPathForCell:cell]];
                                 }];
        [sialertView1 addButtonWithTitle:@"NO"
                                    type:SIAlertViewButtonTypeCancel
                                 handler:^(SIAlertView *alert) {
                                     DebugLog(@"Skip Clicked");
                                 }];
        [sialertView1 show];
    }
}

-(void)showReuseController:(NSIndexPath *)lindexPath
{
    INBroadcastDetailObj *tempbroadcastObj  = [broadcastArray objectAtIndex:lindexPath.row];
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    NSString *placeid = [NSString stringWithFormat:@"%d",tempbroadcastObj.place_ID];
    [tempArray addObject:placeid];
    DebugLog(@"placeid--->%@",placeid);
    
    INBroadcastViewController *broadcastController = [[INBroadcastViewController alloc] initWithNibName:@"INBroadcastViewController" bundle:nil];
    broadcastController.title = @"Talk to shoppers";
    broadcastController.placeidArray = tempArray;
    broadcastController.reusetitle = tempbroadcastObj.title;
    broadcastController.reusedescription = tempbroadcastObj.description;
    broadcastController.reuseimage_url = tempbroadcastObj.image_url1;
    [self.navigationController pushViewController:broadcastController animated:YES];
}

-(void) popAllPostsController
{
    DebugLog(@"popToRootViewControllerAnimated:YES");
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setPostsTableView:nil];
    [super viewDidUnload];
}


#pragma mark PULL TO REFRESH
- (void)setupStrings{
    textPull = @"Pull down to refresh...";
    textRelease = @"Release to refresh...";
    textLoading = @"Loading please wait...";
}

- (void)addPullToRefreshHeader {
    refreshHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0 - REFRESH_HEADER_HEIGHT, 320, REFRESH_HEADER_HEIGHT)];
    refreshHeaderView.backgroundColor = [UIColor clearColor];
    
    refreshLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, REFRESH_HEADER_HEIGHT)];
    refreshLabel.backgroundColor = [UIColor clearColor];
    refreshLabel.font = DEFAULT_BOLD_FONT(15);
    refreshLabel.textAlignment = NSTextAlignmentCenter;
    refreshLabel.textColor = DEFAULT_COLOR;
    refreshArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:PULLTOREFRESH_BARROW]];
    refreshArrow.frame = CGRectMake(floorf((REFRESH_HEADER_HEIGHT - 2) / 2),
                                    (floorf(REFRESH_HEADER_HEIGHT - 44) / 2),
                                    27, 44);
    
    refreshSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    refreshSpinner.frame = CGRectMake(floorf(floorf(REFRESH_HEADER_HEIGHT - 2) / 2), floorf((REFRESH_HEADER_HEIGHT - 20) / 2), 20, 20);
    refreshSpinner.hidesWhenStopped = YES;
    
    [refreshHeaderView addSubview:refreshLabel];
    [refreshHeaderView addSubview:refreshArrow];
    [refreshHeaderView addSubview:refreshSpinner];
    [postsTableView addSubview:refreshHeaderView];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (isLoading) return;
    isDragging = YES;
    lastContentOffset = scrollView.contentOffset.y;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (isLoading) {
        // Update the content inset, good for section headers
        if (scrollView.contentOffset.y > 0)
            postsTableView.contentInset = UIEdgeInsetsZero;
        else if (scrollView.contentOffset.y >= -REFRESH_HEADER_HEIGHT)
            postsTableView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (isDragging && scrollView.contentOffset.y < 0) {
        // Update the arrow direction and label
        [UIView animateWithDuration:0.25 animations:^{
            if (scrollView.contentOffset.y < -REFRESH_HEADER_HEIGHT) {
                // User is scrolling above the header
                refreshLabel.text = self.textRelease;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
            } else {
                // User is scrolling somewhere within the header
                refreshLabel.text = self.textPull;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
            }
        }];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (isLoading) return;
    isDragging = NO;
    if (scrollView.contentOffset.y <= -REFRESH_HEADER_HEIGHT) {
        // Released above the header
        [self startLoading];
    }
}

- (void)startLoading {
    isLoading = YES;
    // Show the header
    [UIView animateWithDuration:0.3 animations:^{
        postsTableView.contentInset = UIEdgeInsetsMake(REFRESH_HEADER_HEIGHT, 0, 0, 0);
        refreshLabel.text = self.textLoading;
        refreshArrow.hidden = YES;
        [refreshSpinner startAnimating];
    }];
    if([IN_APP_DELEGATE networkavailable])
    {
        [self sendStoreBroadcastRequest];
    }
    else
    {
        [self stopLoading];
    }
    // Refresh action!
    //[self refresh];
}

- (void)stopLoading {
    isLoading = NO;
    // Hide the header
    [UIView animateWithDuration:0.3 animations:^{
        postsTableView.contentInset = UIEdgeInsetsZero;
        [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
    }
                     completion:^(BOOL finished) {
                         [self performSelector:@selector(stopLoadingComplete)];
                     }];
}

- (void)stopLoadingComplete {
    // Reset the header
    refreshLabel.text = self.textPull;
    refreshArrow.hidden = NO;
    [refreshSpinner stopAnimating];
}



@end
