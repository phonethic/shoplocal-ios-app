//
//  INAreaObject.h
//  shoplocal
//
//  Created by Rishi on 21/10/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface INAreaObject : NSObject
{
    
}
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *country;
@property (nonatomic, copy) NSString *countrycode;
@property (nonatomic, copy) NSString *areaid;
@property (nonatomic, copy) NSString *iso_code;
@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, copy) NSString *longitude;
@property (nonatomic, copy) NSString *locality;
@property (nonatomic, copy) NSString *pincode;
@property (nonatomic, copy) NSString *published_status;
@property (nonatomic, copy) NSString *shoplocalplaceid;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *sublocality;
@property (nonatomic, readwrite) int is_active;
@property (nonatomic, copy) NSString *distance;

@property (nonatomic, copy) NSString *merchant_count;
@property (nonatomic, copy) NSString *slug;

@end
