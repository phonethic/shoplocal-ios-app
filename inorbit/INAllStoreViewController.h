//
//  INAllStoreViewController.h
//  shoplocal
//
//  Created by Rishi on 07/03/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@class INAllStoreObj;
@interface INAllStoreViewController : UIViewController <UISearchBarDelegate,UITextFieldDelegate>
{
    INAllStoreObj *storeObj;
    BOOL isDragging;
    BOOL isLoading;
    BOOL isAdding;
    sqlite3 *shoplocalDB;
    MBProgressHUD *hud;
    
    NSMutableArray *searchResults;
    
    double previousScrollViewYOffset;
    
    double lastContentOffset;
}
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (readwrite,nonatomic) int comeFrom;

@property (strong, nonatomic) NSMutableArray *storeArray;
@property (strong, nonatomic) NSMutableArray *tempstoreArray;

@property (strong, nonatomic) IBOutlet UITableView *allstoreTableView;
@property (nonatomic, retain) UIView *refreshHeaderView;
@property (nonatomic, retain) UILabel *refreshLabel;
@property (nonatomic, retain) UIImageView *refreshArrow;
@property (nonatomic, retain) UIActivityIndicatorView *refreshSpinner;
@property (strong, nonatomic) IBOutlet UILabel *countlbl;
@property (readwrite, nonatomic) NSInteger totalPage;
@property (readwrite, nonatomic) NSInteger totalRecord;
@property (readwrite, nonatomic) NSInteger currentPage;
@property (strong, nonatomic) IBOutlet UIView *countHeaderView;
@property (strong, nonatomic) IBOutlet UILabel *lblpullToRefresh;
@property (strong, nonatomic) IBOutlet UISearchBar *globalSearchBar;

@property (strong, nonatomic) IBOutlet UIView *searchView;
@property (strong, nonatomic) IBOutlet UITextField *searchTextField;
@property (strong, nonatomic) IBOutlet UIImageView *changeLocationInfoImageView;

//*****Pull TO Refresh
@property (nonatomic, copy) NSString *textPull;
@property (nonatomic, copy) NSString *textRelease;
@property (nonatomic, copy) NSString *textLoading;

@property (strong, nonatomic) NSMutableArray *telArray;
@property (readwrite, nonatomic) NSInteger selectedPlaceId;
@property (copy, nonatomic) NSString* selectedPlaceName;

@property (strong, nonatomic) IBOutlet UIView *callModalViewBackView;
@property (strong, nonatomic) IBOutlet UIView *callModalView;
@property (strong, nonatomic) IBOutlet UILabel *lblcallModalHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblcallModalHeaderSeperator;
@property (strong, nonatomic) IBOutlet UITableView *callModalTableView;
@property (strong, nonatomic) IBOutlet UIButton *btnCancelcallModal;

- (IBAction)btnCancelcallModalPressed:(id)sender;
@end
