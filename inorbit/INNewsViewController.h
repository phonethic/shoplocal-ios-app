//
//  INNewsViewController.h
//  shoplocal
//
//  Created by Rishi on 11/11/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@class INBroadcastDetailObj;
@interface INNewsViewController : UIViewController
{
    INBroadcastDetailObj *newsObj;
    BOOL isDragging;
    BOOL isLoading;
    BOOL isAdding;

}

@property (strong, nonatomic) IBOutlet UIView *countHeaderView;
@property (strong, nonatomic) IBOutlet UILabel *lblpullToRefresh;
@property (strong, nonatomic) IBOutlet UILabel *countlbl;
@property (strong, nonatomic) NSMutableArray *newsArray;
@property (strong, nonatomic) IBOutlet UITableView *newsTableView;

@property (nonatomic, retain) UIView *refreshHeaderView;
@property (nonatomic, retain) UILabel *refreshLabel;
@property (nonatomic, retain) UIImageView *refreshArrow;
@property (nonatomic, retain) UIActivityIndicatorView *refreshSpinner;
@property (readwrite, nonatomic) NSInteger totalPage;
@property (readwrite, nonatomic) NSInteger totalRecord;
@property (readwrite, nonatomic) NSInteger currentPage;
//*****Pull TO Refresh
@property (nonatomic, copy) NSString *textPull;
@property (nonatomic, copy) NSString *textRelease;
@property (nonatomic, copy) NSString *textLoading;
@end
