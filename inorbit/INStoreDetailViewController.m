//
//  INStoreDetailViewController.m
//  shoplocal
//
//  Created by Sagar Mody on 12/01/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//
#import <Social/Social.h>

#import "INStoreDetailViewController.h"
#import "UIImageView+WebCache.h"
#import "INStoreDetailObj.h"
#import "INBroadcastDetailObj.h"
#import "INCustBroadCastDetailsViewController.h"
#import "INGalleryObj.h"
#import "constants.h"
#import "INCustomerLoginViewController.h"
#import "CommonCallback.h"
#import "ActionSheetPicker.h"
#import "INFBWebViewController.h"
#import "INAppDelegate.h"

#import "FacebookOpenGraphAPI.h"
#import "FacebookShareAPI.h"

#define STORE_DETAILS(ID,USERID) [NSString stringWithFormat:@"%@%@%@place_api/places?place_id=%d&customer_id=%@",LIVE_SERVER,URL_PREFIX,API_VERSION,ID,USERID]
#define STORE_GALLERY(ID) [NSString stringWithFormat:@"%@%@%@place_api/place_gallery?place_id=%d",LIVE_SERVER,URL_PREFIX,API_VERSION,ID]
#define STORE_BROADCAST(ID,USERID) [NSString stringWithFormat:@"%@%@%@broadcast_api/broadcasts?place_id=%d&user_id=%@",LIVE_SERVER,URL_PREFIX,API_VERSION,ID,USERID]
#define THUMBNAIL_LINK(IMG_LINK) [NSString stringWithFormat:@"%@%@%@",LIVE_SERVER,URL_PREFIX,IMG_LINK]
#define LIKE_STORE [NSString stringWithFormat:@"%@%@place_api/like",URL_PREFIX,API_VERSION]
#define UNLIKE_STORE(ID) [NSString stringWithFormat:@"%@%@place_api/like?place_id=%d",URL_PREFIX,API_VERSION,ID]

#define SHARE_STORE [NSString stringWithFormat:@"%@%@place_api/share",URL_PREFIX,API_VERSION]


#define GETDIRECTION_LINK_USING_GOOGLEMAPS_IN_BROWSER(CURLAT,CURLNG,DESTLAT,DESTLNG) [NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%lf,%lf&daddr=%@,%@",CURLAT,CURLNG,DESTLAT,DESTLNG]

#define GETDIRECTION_LINK_USING_GOOGLEMAPS_IN_APP(CURLAT,CURLNG,DESTLAT,DESTLNG) [NSString stringWithFormat:@"comgooglemaps://?saddr=%lf,%lf&daddr=%@,%@&zoom=14&views=traffic&mapmode=standard&directionsmode=transit",CURLAT,CURLNG,DESTLAT,DESTLNG]

#define MAP_STORE(LAT,LONG) [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/staticmap?center=%f,%f&zoom=16&size=300x170&maptype=roadmap&markers=color:blue|label:S|%f,%f&sensor=true&key=AIzaSyCvFovNWeEIPL4355q8L0Chd1urF8n8c0g",LAT,LONG,LAT,LONG]

#define MAP_URL(LAT,LON) [NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%f,%f",LAT,LON]

#define CALL_STORE [NSString stringWithFormat:@"%@%@broadcast_api/call",URL_PREFIX,API_VERSION]

@interface INStoreDetailViewController ()
@property(strong) NSDictionary *splashJson;
@end

@implementation INStoreDetailViewController
@synthesize detailScrollView;
@synthesize storeId;
@synthesize storeImageUrl;
@synthesize logoImageView;
@synthesize emailImageView;
@synthesize storeArray;
@synthesize galleryArray;
@synthesize broadcastArray;
@synthesize photos = _photos;
@synthesize descriptionTextView;
@synthesize broadcastTableView;
@synthesize numberslbl;
@synthesize gallerylbl,broadcastlbl;
@synthesize callStoreBtn,likeStore,shareStoreBtn;
@synthesize addressView,lblstoretime,lblemail;
@synthesize telArray;
@synthesize selectedTelNumberToCall;
@synthesize gridView = _gridView;
@synthesize emailBtn;
@synthesize broadcastImageView,galleryImageView;
@synthesize callModalViewBackView,callModalView,lblcallModalHeader,lblcallModalHeaderSeperator,callModalTableView,btnCancelcallModal;
@synthesize fbBtn,twBtn,webBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:TRUE];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:TRUE];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:FALSE];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:FALSE];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                             style:UIBarButtonItemStylePlain
                                                            target:nil
                                                            action:nil];
    [[self navigationItem] setBackBarButtonItem:back];
    
    UIButton *infobtn =  [UIButton buttonWithType:UIButtonTypeInfoLight];
    [infobtn addTarget:self action:@selector(infoBarBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [infobtn setFrame:CGRectMake(285, 5, 32, 32)];
    UIBarButtonItem *infoBarBtn = [[UIBarButtonItem alloc] initWithCustomView:infobtn];
    self.navigationItem.rightBarButtonItem = infoBarBtn;
    
    [self setUI];
    
    storeArray = [[NSMutableArray alloc] init];
    galleryArray = [[NSMutableArray alloc] init];
    broadcastArray = [[NSMutableArray alloc] init];
    
    NSDictionary *storeDetailParams = [NSDictionary dictionaryWithObjectsAndKeys:[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"Area_Name", self.title,@"StoreName", nil];
    [INEventLogger logEvent:@"StoreDetail_View" withParams:storeDetailParams];
    
    logoImageView.image = [UIImage imageNamed:@"list_thumbnail.png"];
    
    if ([IN_APP_DELEGATE networkavailable]) {
        [self sendStoreDetailRequest];
        [self sendStoreGalleryRequest];
        [self sendStoreBroadcastRequest];
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openStoreLogo)];
    tapRecognizer.numberOfTapsRequired = 1;
    [logoImageView addGestureRecognizer:tapRecognizer];
}

- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl {
	DebugLog(@"Selected index %i (via UIControlEventValueChanged)", segmentedControl.selectedSegmentIndex);
}

- (void)uisegmentedControlChangedValue:(UISegmentedControl *)segmentedControl {
	DebugLog(@"Selected index %i", segmentedControl.selectedSegmentIndex);
}

#pragma mark - UIBarButton methods
-(void)infoBarBtnClicked
{
    UIAlertView *infoAlert = [[UIAlertView alloc] initWithTitle:@"Note" message:@"Please swipe right or left to see store details." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [infoAlert show];
}


-(void)openStoreLogo
{
    if(_photos == nil)
    {
        _photos = [[NSMutableArray alloc] init];
    } else {
        [_photos removeAllObjects];
    }
    
    INStoreDetailObj *lstoreObj = [storeArray objectAtIndex:0];
    if(lstoreObj != nil)
    {
        DebugLog(@"addStoreData storeImageUrl %@",lstoreObj.image_url);
        MWPhoto *photo;
        if (lstoreObj.image_url != nil && ![lstoreObj.image_url isEqualToString:@""]) {
            if ([lstoreObj.image_url hasPrefix:@"http"]) {
                photo = [[MWPhoto alloc] initWithURL:[NSURL URLWithString:lstoreObj.image_url]];
                [_photos addObject:photo];
            }else{
                photo = [[MWPhoto alloc] initWithURL:[NSURL URLWithString:THUMBNAIL_LINK(lstoreObj.image_url)]];
                [_photos addObject:photo];
            }
        }
    }
    
    if([_photos count] > 0)
    {
    
        // Create browser
        MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
        // Set options
        browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
        browser.wantsFullScreenLayout = YES; // iOS 5 & 6 only: Decide if you want the photo browser full screen, i.e. whether the status bar is affected (defaults to YES)
        
        browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
        browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
        browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
        browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
        browser.enableGrid = NO; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
        browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
        
        // Optionally set the current visible photo before displaying
        [browser setCurrentPhotoIndex:1];
        
        // Present
        [self.navigationController pushViewController:browser animated:YES];
        
    }
    
}

-(void)sendStoreDetailRequest
{
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    DebugLog(@"url--->%@",STORE_DETAILS(self.storeId,[INUserDefaultOperations getCustomerAuthId]));
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:STORE_DETAILS(self.storeId,[INUserDefaultOperations getCustomerAuthId])]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"-%@-", self.splashJson);
                                                        if(self.splashJson  != nil) {
                                                            [storeArray removeAllObjects];
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSArray* jsonArray = [self.splashJson  objectForKey:@"data"];
                                                                DebugLog(@"\n\n%@\n\n",jsonArray);
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *objdict = [jsonArray  objectAtIndex:index];
                                                                    DebugLog(@"\n\n%@, %d\n\n",objdict,[objdict count]);
                                                                    storeObj = [[INStoreDetailObj alloc] init];
                                                                    storeObj.ID = [[objdict objectForKey:@"id"] integerValue];
                                                                    storeObj.name = [objdict objectForKey:@"name"];
                                                                    storeObj.place_parent = [[objdict objectForKey:@"place_parent"] integerValue];
                                                                    storeObj.description = [objdict objectForKey:@"description"];
                                                                    storeObj.building = [objdict objectForKey:@"building"];
                                                                    storeObj.street = [objdict objectForKey:@"street"];
                                                                    storeObj.landmark = [objdict objectForKey:@"landmark"];
                                                                    storeObj.area = [objdict objectForKey:@"area"];
                                                                    storeObj.pincode = [objdict objectForKey:@"pincode"];
                                                                    storeObj.city = [objdict objectForKey:@"city"];
                                                                    storeObj.state = [objdict objectForKey:@"state"];
                                                                    storeObj.country = [objdict objectForKey:@"country"];
                                                                    storeObj.fb_url = [objdict objectForKey:@"facebook_url"];
                                                                    storeObj.twitter_url = [objdict objectForKey:@"twitter_url"];
                                                                    storeObj.website = [objdict objectForKey:@"website"];
                                                                    
                                                                    if (telArray == nil) {
                                                                        telArray = [[NSMutableArray alloc] init];
                                                                    }else{
                                                                        [telArray removeAllObjects];
                                                                    }
                                                                    if([objdict objectForKey:@"mob_no1"] != (NSString *)[NSNull null] && ![[objdict objectForKey:@"mob_no1"] isEqualToString:@""])
                                                                    {
                                                                        storeObj.mob_no1 = [objdict objectForKey:@"mob_no1"];
                                                                        [telArray addObject:[NSString stringWithFormat:@"+%@",storeObj.mob_no1]];
                                                                    }
                                                                    if([objdict objectForKey:@"mob_no2"] != (NSString *)[NSNull null] && ![[objdict objectForKey:@"mob_no2"] isEqualToString:@""])
                                                                    {
                                                                        storeObj.mob_no2 = [objdict objectForKey:@"mob_no2"];
                                                                        [telArray addObject:[NSString stringWithFormat:@"+%@",storeObj.mob_no2]];
                                                                    }
                                                                    if([objdict objectForKey:@"mob_no3"] != (NSString *)[NSNull null] && ![[objdict objectForKey:@"mob_no3"] isEqualToString:@""])
                                                                    {
                                                                        storeObj.mob_no3 = [objdict objectForKey:@"mob_no3"];
                                                                        [telArray addObject:[NSString stringWithFormat:@"+%@",storeObj.mob_no3]];
                                                                    }
                                                                    if([objdict objectForKey:@"tel_no1"] != (NSString *)[NSNull null] && ![[objdict objectForKey:@"tel_no1"] isEqualToString:@""])
                                                                    {
                                                                        storeObj.tel_no1 = [objdict objectForKey:@"tel_no1"];
                                                                        [telArray addObject:storeObj.tel_no1];
                                                                    }
                                                                    if([objdict objectForKey:@"tel_no2"] != (NSString *)[NSNull null] && ![[objdict objectForKey:@"tel_no2"] isEqualToString:@""])
                                                                    {
                                                                        storeObj.tel_no2 = [objdict objectForKey:@"tel_no2"];
                                                                        [telArray addObject:storeObj.tel_no2];
                                                                    }
                                                                    if([objdict objectForKey:@"tel_no3"] != (NSString *)[NSNull null] && ![[objdict objectForKey:@"tel_no3"] isEqualToString:@""])
                                                                    {
                                                                        storeObj.tel_no3 = [objdict objectForKey:@"tel_no3"];
                                                                        [telArray addObject:storeObj.tel_no3];
                                                                    }
                                                                    
                                                                    storeObj.image_url = [objdict objectForKey:@"image_url"];
                                                                    storeObj.email = [objdict objectForKey:@"email"];
                                                                    storeObj.total_like = [objdict objectForKey:@"total_like"];
                                                                    storeObj.total_share = [objdict objectForKey:@"total_share"];
                                                                    storeObj.user_like = [objdict objectForKey:@"user_like"];
                                                                    
                                                                    storeObj.latitude=[objdict objectForKey:@"latitude"];
                                                                    storeObj.longitude= [objdict objectForKey:@"longitude"];
                                                                    
                                                                    DebugLog(@"%f --- %f",[storeObj.latitude floatValue],[storeObj.longitude floatValue]);
                                                                    //NSString *link = [MAP_STORE([storeObj.latitude floatValue],[storeObj.longitude floatValue]) stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                                                                    //[mapImageView setImageWithURL:[NSURL URLWithString:link] placeholderImage:nil];
                                                                    
                                                                    if([objdict objectForKey:@"time"] != [NSNull null])
                                                                    {
                                                                        NSArray *timeArray = [objdict objectForKey:@"time"];
                                                                        DebugLog(@"%@",timeArray);
                                                                        NSDictionary *timedict = [timeArray  objectAtIndex:0];
                                                                        [self setStoreTimeValues:timedict storeobj:storeObj];
                                                                        
                                                                        NSMutableString *days = [[NSMutableString alloc] initWithString:@""];
                                                                        for(NSDictionary *dict in timeArray){
                                                                            if ([[dict objectForKey:@"is_closed"] intValue] == 1) {
                                                                                if ([days isEqualToString:@""]) {
                                                                                    [days appendString:[NSString stringWithFormat:@"Closed On : %@",[[dict objectForKey:@"day"] capitalizedString]]];
                                                                                }else{
                                                                                    [days appendString:[NSString stringWithFormat:@",%@",[[dict objectForKey:@"day"] capitalizedString]]];
                                                                                }
                                                                            }
                                                                        }
                                                                        DebugLog(@"close days %@",days);
                                                                        storeObj.store_close_days = days;
                                                                    }
                                                                    
                                                                    [storeArray addObject:storeObj];
                                                                    storeObj = nil;
                                                                }
                                                                [self addStoreData];
                                                                [self setStoreLikeType];
                                                            } else {
                                                                UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                             message:@"No records found"
                                                                                                            delegate:nil
                                                                                                   cancelButtonTitle:@"OK"
                                                                                                   otherButtonTitles:nil];
                                                                [av show];
                                                            }
                                                        }
                                                        
                                                        
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        if([self.splashJson  objectForKey:@"message"] != [NSNull null] && ![[self.splashJson objectForKey:@"message"] isEqualToString:@""]){
                                                            NSString *message = [self.splashJson objectForKey:@"message"];
                                                            [INUserDefaultOperations showSIAlertView:message];
                                                        }
                                                        
                                                    }];
    
    
    
    [operation start];
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleShrink];
    [MMProgressHUD showWithTitle:HUD_TITLE
                          status:@""
             confirmationMessage:[NSString stringWithFormat:@"Cancel?"]
                     cancelBlock:^{
                         DebugLog(@"Task was cancelled!");
                         [operation cancel];
                     }];
}

-(void)sendStoreGalleryRequest
{
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:STORE_GALLERY(self.storeId)]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"%@", self.splashJson);
                                                        if(self.splashJson  != nil) {
                                                            [galleryArray removeAllObjects];
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                gallerylbl.hidden = TRUE;
                                                                galleryImageView.hidden = TRUE;
                                                                NSArray* jsonArray = [self.splashJson  objectForKey:@"data"];
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *objdict = [jsonArray  objectAtIndex:index];
                                                                    INGalleryObj *tempgalleryObj = [[INGalleryObj alloc] init];
                                                                    tempgalleryObj.galleryID = [objdict objectForKey:@"id"];
                                                                    tempgalleryObj.place_id = [objdict objectForKey:@"place_id"];
                                                                    tempgalleryObj.title = [objdict objectForKey:@"title"];
                                                                    tempgalleryObj.image_url = [objdict objectForKey:@"image_url"];
                                                                    tempgalleryObj.thumb_url = [objdict objectForKey:@"thumb_url"];
                                                                    tempgalleryObj.image_date = [objdict objectForKey:@"image_date"];
                                                                    [galleryArray addObject:tempgalleryObj];
                                                                    tempgalleryObj = nil;
                                                                }
                                                            } else {
                                                                if([self.splashJson  objectForKey:@"message"] != [NSNull null])
                                                                {
                                                                    //[INUserDefaultOperations showAlert:[self.splashJson  objectForKey:@"message"]];
                                                                    gallerylbl.hidden = FALSE;
                                                                    galleryImageView.hidden = FALSE;
                                                                    gallerylbl.text = [self.splashJson  objectForKey:@"message"];
                                                                }
                                                            }
                                                        }
                                                        //if([galleryArray count] > 0)
                                                        {
                                                            [self.gridView reloadData];
                                                        }
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK"
                                                                                           otherButtonTitles:nil];
                                                        [av show];
                                                        
                                                    }];
    
    
    
    [operation start];
}


-(void)sendStoreBroadcastRequest
{
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:STORE_BROADCAST(self.storeId,[INUserDefaultOperations getCustomerAuthId])]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"sendStoreBroadcastRequest %@", self.splashJson);
                                                        if(self.splashJson  != nil) {
                                                            [broadcastArray removeAllObjects];
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                broadcastlbl.hidden = TRUE;
                                                                broadcastImageView.hidden = TRUE;
                                                                NSDictionary *maindict = [self.splashJson  objectForKey:@"data"];
                                                                //self.totalPage = [[maindict  objectForKey:@"total_page"] integerValue];
                                                                //self.totalRecord = [[maindict  objectForKey:@"total_record"] integerValue];
                                                                NSArray* jsonArray = [maindict  objectForKey:@"record"];
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *objdict = [jsonArray  objectAtIndex:index];
                                                                    //DebugLog(@"\n\n%@\n\n",objdict);
                                                                    broadcastObj = [[INBroadcastDetailObj alloc] init];
                                                                    broadcastObj.ID = [[objdict objectForKey:@"id"] integerValue];
                                                                    broadcastObj.place_ID = [[objdict objectForKey:@"place_id"] integerValue];
                                                                    broadcastObj.type = [objdict objectForKey:@"street"];
                                                                    broadcastObj.title = [objdict objectForKey:@"title"];
                                                                    broadcastObj.description = [objdict objectForKey:@"description"];
                                                                    broadcastObj.url = [objdict objectForKey:@"url"];
                                                                    broadcastObj.tags = [objdict objectForKey:@"tags"];
                                                                    broadcastObj.date = [objdict objectForKey:@"date"];
                                                                    broadcastObj.is_offered       = [objdict objectForKey:@"is_offered"];
                                                                    broadcastObj.offerdate    = [objdict objectForKey:@"offer_date_time"];
                                                                    broadcastObj.total_like = [objdict objectForKey:@"total_like"];
                                                                    broadcastObj.total_share = [objdict objectForKey:@"total_share"];
                                                                    broadcastObj.user_like = [objdict objectForKey:@"user_like"];
                                                                    broadcastObj.image_url1 = [objdict objectForKey:@"image_url1"];
                                                                    broadcastObj.image_url2 = [objdict objectForKey:@"image_url2"];
                                                                    broadcastObj.image_url3 = [objdict objectForKey:@"image_url3"];
                                                                    
                                                                    broadcastObj.thumb_url1 = [objdict objectForKey:@"thumb_url1"];
                                                                    broadcastObj.thumb_url2 = [objdict objectForKey:@"thumb_url2"];
                                                                    broadcastObj.thumb_url3 = [objdict objectForKey:@"thumb_url3"];
                                                                    
                                                                    broadcastObj.image_title1 = [objdict objectForKey:@"image_title1"];
                                                                    broadcastObj.image_title2 = [objdict objectForKey:@"image_title2"];
                                                                    broadcastObj.image_title3 = [objdict objectForKey:@"image_title3"];
                                                                    
                                                                    [broadcastArray addObject:broadcastObj];
                                                                    broadcastObj = nil;
                                                                }
                                                            } else {
                                                                if([self.splashJson  objectForKey:@"message"] != [NSNull null])
                                                                {
                                                                    //[INUserDefaultOperations showAlert:[self.splashJson  objectForKey:@"message"]];
                                                                    broadcastlbl.hidden = FALSE;
                                                                    broadcastImageView.hidden = FALSE;
                                                                    broadcastlbl.text = [self.splashJson  objectForKey:@"message"];
                                                                }
                                                            }
                                                            //if([broadcastArray count] > 0)
                                                            {
                                                                [broadcastTableView reloadData];
                                                            }
                                                            if([broadcastArray count] > 0)
                                                            {
                                                                
                                                            }
                                                        
                                                        }
                                                        
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK"
                                                                                           otherButtonTitles:nil];
                                                        [av show];
                                                        
                                                    }];
    
    
    
    [operation start];
}

-(void)sendStoreLikeRequest
{
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[INUserDefaultOperations getCustomerAuthId] forKey:@"user_id"];
    [params setObject:[INUserDefaultOperations getCustomerAuthCode] forKey:@"auth_id"];
    [params setObject:[NSString stringWithFormat:@"%d",self.storeId] forKey:@"place_id"];
    [httpClient postPath:LIKE_STORE parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            if([json  objectForKey:@"message"] != [NSNull null] && [[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
                if([json  objectForKey:@"message"] != [NSNull null] && ![[json objectForKey:@"message"] isEqualToString:@""]){
                    NSString *message = [json objectForKey:@"message"];
                    [INUserDefaultOperations showSIAlertView:message];
                    [INUserDefaultOperations setCustomerFavouriteState:TRUE];
                    [INUserDefaultOperations setCustomerFavouriteMyShoplocalRefresh:TRUE];
                }
                INStoreDetailObj *lstoreObj = [storeArray objectAtIndex:0];
                lstoreObj.user_like = @"1";
                [storeArray replaceObjectAtIndex:0 withObject:lstoreObj];
                [self setStoreLikeType];
            } else {
                NSString* code = [json objectForKey:@"code"];
                if([code isEqualToString:CUSTOMER_INVALID_USER_AUTH_CODE])
                {
                    if([INUserDefaultOperations isCustomerLoggedIn]){
                        [INUserDefaultOperations clearCustomerDetails];
                        [self showLoginAlert];
                    }
                }else{
                    if([json  objectForKey:@"message"] != [NSNull null] && ![[json objectForKey:@"message"] isEqualToString:@""]){
                        NSString *message = [json objectForKey:@"message"];
                        [INUserDefaultOperations showSIAlertView:message];
                    }
                }
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}

-(void)sendStoreUnLikeRequest
{
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    [httpClient setDefaultHeader:@"user_id" value:[INUserDefaultOperations getCustomerAuthId]];
    [httpClient setDefaultHeader:@"auth_id" value:[INUserDefaultOperations getCustomerAuthCode]];
    [httpClient deletePath:UNLIKE_STORE(self.storeId) parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            if([json  objectForKey:@"message"] != [NSNull null] && [[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
                if([json  objectForKey:@"message"] != [NSNull null] && ![[json objectForKey:@"message"] isEqualToString:@""]){
                    NSString *message = [json objectForKey:@"message"];
                    [INUserDefaultOperations showSIAlertView:message];
                    [INUserDefaultOperations setCustomerFavouriteMyShoplocalRefresh:TRUE];
                }
                INStoreDetailObj *lstoreObj = [storeArray objectAtIndex:0];
                lstoreObj.user_like = @"0";
                [storeArray replaceObjectAtIndex:0 withObject:lstoreObj];
                [self setStoreLikeType];
            } else {
                if([json  objectForKey:@"message"] != [NSNull null] && ![[json objectForKey:@"message"] isEqualToString:@""]){
                    NSString *message = [json objectForKey:@"message"];
                    [INUserDefaultOperations showSIAlertView:message];
                }
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}


#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if ([scrollView isEqual:callModalTableView]) {
        return;
    }
    if ([scrollView isEqual:detailScrollView]) {
        CGFloat pageWidth = scrollView.frame.size.width;
        NSInteger page = scrollView.contentOffset.x / pageWidth;
        [self.segmentedControl1 setSelectedSegmentIndex:page animated:YES];
    }
}

- (IBAction)btnCancelcallModalPressed:(id)sender {
    [callModalViewBackView setHidden:YES];
    [callModalView setHidden:YES];
}

- (IBAction)fbBtnPressed:(id)sender {
    if([IN_APP_DELEGATE networkavailable])
    {
        NSDictionary *storeDetailParams = [NSDictionary dictionaryWithObjectsAndKeys:[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"Area_Name", self.title,@"StoreName", nil];
        [INEventLogger logEvent:@"StoreDetail_Facebook" withParams:storeDetailParams];
        if([storeArray count] > 0)
        {
            INStoreDetailObj *lstoreObj = [storeArray objectAtIndex:0];
            if (lstoreObj.fb_url != nil && lstoreObj.fb_url != (NSString*)[NSNull null] && ![lstoreObj.fb_url isEqualToString:@""]) {
                INFBWebViewController *fbViewController = [[INFBWebViewController alloc] initWithNibName:@"INFBWebViewController" bundle:nil] ;
                fbViewController.title = @"";
                fbViewController.type = 3;
                fbViewController.openURL = lstoreObj.fb_url;
                [self.navigationController pushViewController:fbViewController animated:YES];
            } else {
                [INUserDefaultOperations showSIAlertView:@"Facebook not set."];
            }
        }  else {
            [INUserDefaultOperations showSIAlertView:@"Facebook not set."];
        }
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

- (IBAction)twBtnPressed:(id)sender {
    if([IN_APP_DELEGATE networkavailable])
    {
        NSDictionary *storeDetailParams = [NSDictionary dictionaryWithObjectsAndKeys:[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"Area_Name", self.title,@"StoreName", nil];
        [INEventLogger logEvent:@"StoreDetail_Twitter" withParams:storeDetailParams];
        if([storeArray count] > 0)
        {
            INStoreDetailObj *lstoreObj = [storeArray objectAtIndex:0];
            if (lstoreObj.twitter_url != nil && lstoreObj.twitter_url != (NSString*)[NSNull null] && ![lstoreObj.twitter_url isEqualToString:@""]) {
                INFBWebViewController *fbViewController = [[INFBWebViewController alloc] initWithNibName:@"INFBWebViewController" bundle:nil] ;
                fbViewController.title = @"";
                fbViewController.type = 3;
                fbViewController.openURL = lstoreObj.twitter_url;
                [self.navigationController pushViewController:fbViewController animated:YES];
            } else {
                [INUserDefaultOperations showSIAlertView:@"Twitter not set."];
            }
        } else {
            [INUserDefaultOperations showSIAlertView:@"Twitter not set."];
        }
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

- (IBAction)webBtnPressed:(id)sender {
    if([IN_APP_DELEGATE networkavailable])
    {
        NSDictionary *storeDetailParams = [NSDictionary dictionaryWithObjectsAndKeys:[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"Area_Name", self.title,@"StoreName", nil];
        [INEventLogger logEvent:@"StoreDetail_Website" withParams:storeDetailParams];
        if([storeArray count] > 0)
        {
            INStoreDetailObj *lstoreObj = [storeArray objectAtIndex:0];
            if (lstoreObj.website != nil && lstoreObj.website != (NSString*)[NSNull null] && ![lstoreObj.website isEqualToString:@""]) {
                INFBWebViewController *fbViewController = [[INFBWebViewController alloc] initWithNibName:@"INFBWebViewController" bundle:nil] ;
                fbViewController.title = @"";
                fbViewController.type = 3;
                fbViewController.openURL = lstoreObj.website;
                [self.navigationController pushViewController:fbViewController animated:YES];
            } else {
                [INUserDefaultOperations showSIAlertView:@"Website not set."];
            }
        }  else {
            [INUserDefaultOperations showSIAlertView:@"Website not set."];
        }
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

- (IBAction)callStoreBtnPressed:(id)sender {
    if (telArray != nil && telArray.count > 0) {
//        [ActionSheetStringPicker showPickerWithTitle:@"Select number" rows:self.telArray initialSelection:0 target:self successAction:@selector(selectTel:element:) cancelAction:@selector(actionPickerCancelled:) origin:sender];
//        
//        NSString *areaName = [IN_APP_DELEGATE getActivePlaceNameFromAreaTable];
//        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:@"StoreDetails",@"Source",areaName,@"AreaName", nil];
//        [INEventLogger logEvent:@"StoreCall" withParams:params];

        if ([self.title length] == 0)
        {
            lblcallModalHeader.text =  @"Select number";
        }
        else {
            lblcallModalHeader.text = self.title;
        }

        [callModalViewBackView setHidden:NO];
        [callModalView setHidden:NO];
        [CommonCallback viewtransitionInCompletion:callModalView completion:^{
            [CommonCallback viewtransitionOutCompletion:callModalView completion:nil];
        }];
        
        NSDictionary *callParams = [NSDictionary dictionaryWithObjectsAndKeys:@"StoreDetails",@"Source",[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"AreaName",self.title,@"StoreName", nil];
        [INEventLogger logEvent:@"StoreCall" withParams:callParams];
        
        [self sendStoreCallRequest:@"StoreCall"];
    } else {
         [INUserDefaultOperations showSIAlertView:NO_CONTACT_MESSAGE];
    }
}

-(void)showLoginAlert{
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Please LOGIN to add this Place to MyShoplocal."];
    alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
    alertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
    
    [alertView addButtonWithTitle:@"LOGIN"
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alert) {
                              DebugLog(@"LOGIN Clicked");
                              [self showLoginModal];
                          }];
    [alertView addButtonWithTitle:@"NOT NOW"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alert) {
                              DebugLog(@"NOT NOW Clicked");
                          }];
    [alertView show];
}

- (IBAction)likeStoreBtnPressed:(id)sender {
    if([IN_APP_DELEGATE networkavailable])
    {
        if(![INUserDefaultOperations isCustomerLoggedIn])
        {
            [self showLoginAlert];
        } else {
            if([storeArray count] > 0) {
                INStoreDetailObj *lstoreObj = [storeArray objectAtIndex:0];
                DebugLog(@"%@",lstoreObj.user_like);
                if([lstoreObj.user_like isEqualToString:@"1"])
                {
                    [self sendStoreUnLikeRequest];
                    NSDictionary *storeDetailParams = [NSDictionary dictionaryWithObjectsAndKeys:[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"Area_Name", self.title,@"StoreName", nil];
                    [INEventLogger logEvent:@"StoreDetail_UnFavourite" withParams:storeDetailParams];
                } else {
                    NSDictionary *storeDetailParams = [NSDictionary dictionaryWithObjectsAndKeys:[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"Area_Name", self.title,@"StoreName", nil];
                    [INEventLogger logEvent:@"StoreDetail_Favourite" withParams:storeDetailParams];
                    [self sendStoreLikeRequest];
//                    if ([[INUserDefaultOperations getCustomerLoginFrom] isEqualToString:FACEBOOKSERVER] && [INUserDefaultOperations isOpenGraphShareEnabled]) {
//                        SIAlertView *sialertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Do you want to share this on your Facebook timeline?"];
//                        sialertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
//                        sialertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
//                        [sialertView addButtonWithTitle:@"YES"
//                                                   type:SIAlertViewButtonTypeDestructive
//                                                handler:^(SIAlertView *alert) {
//                                                    DebugLog(@"YES");
//                                                    [self sendStoreLikeRequest];
//
//                                                    INStoreDetailObj *lstoreObj = [storeArray objectAtIndex:0];
//                                                    NSString *deepLinkingUrl    = [FacebookOpenGraphAPI createDeepLinkWithActiveAreaName:[IN_APP_DELEGATE getActiveSlugFromAreaTable] storeName:lstoreObj.name storeId:[NSString stringWithFormat:@"%d",storeId]];
//                                                    
//                                                    if (lstoreObj.image_url != nil && ![lstoreObj.image_url isEqualToString:@""]) {
//                                                        if ([lstoreObj.image_url hasPrefix:@"http"]) {
//                                                            [FacebookOpenGraphAPI likeContentUsingOpenGraphAPIWithTitle:lstoreObj.name description:lstoreObj.description image:lstoreObj.image_url deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_STORE];
//                                                        }else{
//                                                            [FacebookOpenGraphAPI likeContentUsingOpenGraphAPIWithTitle:lstoreObj.name description:lstoreObj.description image:THUMBNAIL_LINK(lstoreObj.image_url) deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_STORE];
//                                                        }
//                                                    }else{
//                                                        [FacebookOpenGraphAPI likeContentUsingOpenGraphAPIWithTitle:lstoreObj.name description:lstoreObj.description image:@"" deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_STORE];
//                                                    }
//                                                }];
//                        [sialertView addButtonWithTitle:@"NOT NOW"
//                                                   type:SIAlertViewButtonTypeCancel
//                                                handler:^(SIAlertView *alert) {
//                                                    DebugLog(@"NO");
//                                                    [self sendStoreLikeRequest];
//                                                }];
//                        [sialertView show];
//                    }else{
//                        [self sendStoreLikeRequest];
//                    }
                }
            }
        }
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

- (IBAction)shareStoreBtnPressed:(id)sender {
    UIActionSheet *shareActionSheet = [[UIActionSheet alloc] initWithTitle:@"Share via" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"SMS",@"Email",@"WhatsApp",@"Facebook",@"Twitter", nil];
    [shareActionSheet showInView:self.view];
}

- (IBAction)getDirectionsBtnPressed:(id)sender {
    if ([INUserDefaultOperations getLatitude] > 0 && [INUserDefaultOperations getLongitude] > 0) {
        NSDictionary *storeDetailParams = [NSDictionary dictionaryWithObjectsAndKeys:[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"Area_Name", self.title,@"StoreName", nil];
        [INEventLogger logEvent:@"StoreDetail_ViewMap" withParams:storeDetailParams];
        
        if([storeArray count] > 0)
        {
            INStoreDetailObj *lstoreObj = [storeArray objectAtIndex:0];

            if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"comgooglemaps://"]]) //is Google maps installed?
            {
                DebugLog(@"Can use comgooglemaps://");
                NSString *googleMapsURLString = GETDIRECTION_LINK_USING_GOOGLEMAPS_IN_APP([INUserDefaultOperations getLatitude], [INUserDefaultOperations getLongitude], lstoreObj.latitude, lstoreObj.longitude);
                DebugLog(@"URL string-----> %@",googleMapsURLString);
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapsURLString]];
            } else {
                DebugLog(@"Can't use comgooglemaps://");
                NSString *googleMapsURLString = GETDIRECTION_LINK_USING_GOOGLEMAPS_IN_BROWSER([INUserDefaultOperations getLatitude], [INUserDefaultOperations getLongitude], lstoreObj.latitude, lstoreObj.longitude);
                DebugLog(@"URL string-----> %@",googleMapsURLString);
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapsURLString]];
            }
        }
    }
}

- (IBAction)emailBtnPressed:(id)sender {
    NSDictionary *storeDetailParams = [NSDictionary dictionaryWithObjectsAndKeys:[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"Area_Name", self.title,@"StoreName", nil];
    [INEventLogger logEvent:@"StoreDetail_Email" withParams:storeDetailParams];
    if ([lblemail.text length] > 0) {
        if ([MFMailComposeViewController canSendMail]) {
            MFMailComposeViewController *emailComposer = [[MFMailComposeViewController alloc] init];
            emailComposer.mailComposeDelegate = self;
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
                emailComposer.navigationBar.tintColor  = [UIColor whiteColor];
            }
            [emailComposer setToRecipients:[NSArray arrayWithObjects:lblemail.text, nil]];
            [emailComposer setSubject:@"Found you on Shoplocal"];
            [emailComposer setMessageBody:@"" isHTML:NO];
            emailComposer.toolbar.tag = 1;
            [self presentViewController:emailComposer animated:YES completion:nil];
        }else
        {
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:ALERT_TITLE
                                      message:@"You may not have set up mail service on your device or You may not connected to internet.\nPlease check and try again."
                                      delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [alertView show];
        }
    }
}

-(void)showLoginModal{
    INCustomerLoginViewController *custLoginController = [[INCustomerLoginViewController alloc] initWithNibName:@"INCustomerLoginViewController" bundle:nil] ;
    custLoginController.delegate = self;
    UINavigationController *loginnavBar=[[UINavigationController alloc]initWithRootViewController:custLoginController];
    UIButton *moreButton1 =  [UIButton buttonWithType:UIButtonTypeCustom];
    [moreButton1 setTitle:@"Cancel" forState:UIControlStateNormal];
    moreButton1.titleLabel.font = [UIFont systemFontOfSize:17.0];
    //[moreButton1 setBackgroundImage:[UIImage imageNamed:@"Transparent_Button.png"] forState:UIControlStateNormal];
    [moreButton1 addTarget:self action:@selector(dismissLoginView) forControlEvents:UIControlEventTouchUpInside];
    [moreButton1 setFrame:CGRectMake(250, 7, 70, 32)];
    [loginnavBar.navigationBar addSubview:moreButton1];
    [loginnavBar.navigationBar setTranslucent:NO];
    [self.navigationController presentViewController:loginnavBar animated:YES completion:nil];
}

-(void)dismissLoginView
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"Dismiss");
        [self sendStoreDetailRequest];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma Internal Methods
-(void)setUI{
//    CGFloat yDelta;
//    
//    if ([[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch] != NSOrderedAscending) {
//        yDelta = 20.0f;
//    } else {
//        yDelta = 0.0f;
//    }
    
    broadcastlbl.hidden = TRUE;
    broadcastImageView.hidden = TRUE;
    gallerylbl.hidden = TRUE;
    galleryImageView.hidden = TRUE;

    numberslbl.font             = DEFAULT_FONT(15);
    lblemail.font               = DEFAULT_FONT(15);
    lblstoretime.font           = DEFAULT_FONT(15);
    descriptionTextView.font    = DEFAULT_FONT(15);
    addressView.font            = DEFAULT_FONT(15);
    broadcastlbl.font           = DEFAULT_FONT(15);
    gallerylbl.font             = DEFAULT_FONT(15);
    
    // Segmented control with scrolling
    self.segmentedControl1 = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"About Store", @"Address", @"Post/offers", @"Photo Gallery"]];
    self.segmentedControl1.textColor = BROWN_COLOR;
    self.segmentedControl1.selectedTextColor = BROWN_COLOR;
    self.segmentedControl1.font = DEFAULT_BOLD_FONT(15);
    self.segmentedControl1.segmentEdgeInset = UIEdgeInsetsMake(0, 10, 0, 10);
    self.segmentedControl1.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    self.segmentedControl1.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    self.segmentedControl1.scrollEnabled = YES;
    [self.segmentedControl1 setSelectionIndicatorHeight:4.0f];
    [self.segmentedControl1 setSelectedSegmentIndex:0];
    self.segmentedControl1.backgroundColor = [UIColor clearColor];
    [self.segmentedControl1 setSelectionIndicatorColor:BROWN_COLOR];
    //[self.segmentedControl1 setSelectedSegmentIndex:HMSegmentedControlNoSegment];
    self.segmentedControl1.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [self.segmentedControl1 setFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    [self.segmentedControl1 addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    
    __weak typeof(self) weakSelf = self;
    [self.segmentedControl1 setIndexChangeBlock:^(NSInteger index) {
        [weakSelf.detailScrollView scrollRectToVisible:CGRectMake(weakSelf.view.frame.size.width * index, 0, weakSelf.view.frame.size.width, 200) animated:YES];
    }];
    
    [self.view addSubview:self.segmentedControl1];
    
//    for(UIView *backView in _viewsCollection)
//    {
//        backView.layer.cornerRadius = 8.0;
//    }
    
    UIView *view = nil;
    CGFloat curXLoc = 10;
    for (view in [detailScrollView subviews])
    {
        if ([view isKindOfClass:[UIView class]])
        {
            CGRect frame = view.frame;
            frame.origin = CGPointMake(curXLoc, 4);
            view.frame = frame;
            
            curXLoc += (detailScrollView.frame.size.width);
        }
    }
    
    [detailScrollView setContentSize:CGSizeMake(self.view.frame.size.width * [self.segmentedControl1.sectionTitles count], self.detailScrollView.frame.size.height)];
    [detailScrollView scrollRectToVisible:CGRectMake(self.view.frame.size.width * self.segmentedControl1.selectedSegmentIndex, 0, self.view.frame.size.width, self.view.frame.size.height -  self.segmentedControl1.frame.size.height) animated:NO];
    detailScrollView.backgroundColor = [UIColor clearColor];
    detailScrollView.clipsToBounds = YES;
    detailScrollView.scrollEnabled = YES;
    detailScrollView.pagingEnabled = YES;
    detailScrollView.showsHorizontalScrollIndicator = NO;
    detailScrollView.showsVerticalScrollIndicator = NO;
    detailScrollView.scrollsToTop = YES;
    detailScrollView.bounces = NO;
    detailScrollView.directionalLockEnabled = YES;
    
    //Add gridView
    _gridView = [[VCGridView alloc] initWithFrame:CGRectMake(-10, 0, self.view.frame.size.width, self.galleryView.frame.size.height)];
	_gridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	_gridView.delegate = self;
	_gridView.dataSource = self;
	[self.galleryView addSubview:_gridView];
    
    
    //callModalView = [CommonCallback setViewPropertiesWithRoundedCorner:callModalView];
    callModalView.backgroundColor =  [UIColor whiteColor];
    callModalView.layer.shadowColor      = [UIColor blackColor].CGColor;
    callModalView.layer.shadowOffset     = CGSizeMake(1, 1);
    callModalView.layer.shadowOpacity    = 1.0;
    callModalView.layer.shadowRadius     = 10.0;
    [callModalView setHidden:YES];
    [callModalViewBackView setHidden:YES];

    lblcallModalHeader.textAlignment     = NSTextAlignmentCenter;
    lblcallModalHeader.backgroundColor   = [UIColor clearColor];
    lblcallModalHeader.font              = DEFAULT_BOLD_FONT(20);
    lblcallModalHeader.textColor         = BROWN_COLOR;
    //    lblcallModalHeader.adjustsFontSizeToFitWidth = YES;
    lblcallModalHeader.minimumScaleFactor = 15;
    lblcallModalHeader.text              = @"Select number";
    lblcallModalHeader.numberOfLines     = 2;
    
    lblcallModalHeaderSeperator.backgroundColor   = DEFAULT_COLOR;
    callModalTableView.backgroundColor = [UIColor whiteColor];

    if ([callModalTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [callModalTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    btnCancelcallModal.backgroundColor = [UIColor clearColor];
}


-(void)addHUDView
{
    hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:hud];
	hud.dimBackground = NO;
    hud.labelText = @"Loading";
	hud.detailsLabelText = @"Please wait ...";
    [hud hide:NO];
}

-(void)addStoreData
{
    INStoreDetailObj *lstoreObj = [storeArray objectAtIndex:0];
    self.title = lstoreObj.name;
    if (lstoreObj.description != (NSString *)[NSNull null] && lstoreObj.description != nil && ![lstoreObj.description isEqualToString:@""]) {
        
        NSString *tempText = [NSString stringWithFormat:@"Total Likes : %@ \n\nTotal Shares : %@ \n\n %@", lstoreObj.total_like, lstoreObj.total_share, lstoreObj.description];
        descriptionTextView.text = tempText;
    } else {
        NSString *tempText = [NSString stringWithFormat:@"Total Likes : %@ \n\nTotal Shares : %@ ", lstoreObj.total_like, lstoreObj.total_share];
        descriptionTextView.text = tempText;
    }
    
    NSMutableString *addreessString = [[NSMutableString alloc] initWithString:@""];
    if (lstoreObj.street != (NSString *)[NSNull null] && lstoreObj.street != nil && ![lstoreObj.street isEqualToString:@""]) {
        [addreessString appendString:lstoreObj.street];
    }
    if (lstoreObj.landmark != (NSString *)[NSNull null] && lstoreObj.landmark != nil && ![lstoreObj.landmark isEqualToString:@""]) {
        if ([addreessString isEqualToString:@""]) {
            [addreessString appendString:lstoreObj.landmark];
        }else{
            [addreessString appendString:[NSString stringWithFormat:@", %@", lstoreObj.landmark]];
        }
    }
    if (lstoreObj.area != (NSString *)[NSNull null] && lstoreObj.area != nil && ![lstoreObj.area isEqualToString:@""]) {
        if ([addreessString isEqualToString:@""]) {
            [addreessString appendString:lstoreObj.area];
        }else{
            [addreessString appendString:[NSString stringWithFormat:@", %@", lstoreObj.area]];
        }
    }
    
    if (lstoreObj.city != (NSString *)[NSNull null] && lstoreObj.city != nil && ![lstoreObj.city isEqualToString:@""]) {
        if ([addreessString isEqualToString:@""]) {
            [addreessString appendString:lstoreObj.city];
        }else{
            [addreessString appendString:[NSString stringWithFormat:@", %@", lstoreObj.city]];
        }
    }
    
    addressView.text = [NSString stringWithFormat:@"%@",addreessString];
    
    if (lstoreObj.open_time != (NSString *)[NSNull null] && lstoreObj.open_time != nil && ![lstoreObj.open_time isEqualToString:@""] && lstoreObj.close_time != (NSString *)[NSNull null] && lstoreObj.close_time != nil && ![lstoreObj.close_time isEqualToString:@""]) {
        lblstoretime.text = [NSString stringWithFormat:@"%@ To %@\n",lstoreObj.open_time, lstoreObj.close_time];
    }else{
        lblstoretime.hidden = TRUE;
    }
    
    //lblstoreClosed.text    = lstoreObj.store_close_days;
    //lblwebsite.text        = lstoreObj.website;
    
    if (lstoreObj.email != (NSString *)[NSNull null] && lstoreObj.email != nil && ![lstoreObj.email isEqualToString:@""]) {
        emailImageView.hidden = FALSE;
        lblemail.text = lstoreObj.email;
    } else {
        emailImageView.hidden = TRUE;
    }

    DebugLog(@"telArray %@",telArray);
    NSMutableString *telstring = [[NSMutableString alloc]init];
    if (telArray != nil && telArray.count > 0) {
        for (int i = 0; i < [telArray count]; i++) {
            [telstring appendString:[NSString stringWithFormat:@"%@\n",[telArray objectAtIndex:i]]];
        }
        numberslbl.text = telstring;
        [callModalTableView reloadData];
        if (callModalTableView.contentSize.height < 240) {
            [callModalTableView setFrame:CGRectMake(callModalTableView.frame.origin.x, callModalTableView.frame.origin.y, callModalTableView.frame.size.width, callModalTableView.contentSize.height)];
        }else{
            [callModalTableView setFrame:CGRectMake(callModalTableView.frame.origin.x, callModalTableView.frame.origin.y, callModalTableView.frame.size.width, 240)];
        }
      [callModalView setFrame:CGRectMake(callModalView.frame.origin.x, callModalView.frame.origin.y, callModalView.frame.size.width, callModalTableView.frame.size.height+85)];

    }else{
        
    }
    telstring = nil;
    
    DebugLog(@"lstoreObj.latitude %@ lstoreObj.longitude %@",lstoreObj.latitude,lstoreObj.longitude);
    
    DebugLog(@"addStoreData storeImageUrl %@",lstoreObj.image_url);
    if (lstoreObj.image_url != (NSString *)[NSNull null] && lstoreObj.image_url != nil && ![lstoreObj.image_url isEqualToString:@""]) {
        if ([lstoreObj.image_url hasPrefix:@"http"]) {
            //storeImageUrl = [NSURL URLWithString:lstoreObj.image_url];
            __weak UIImageView *logoImg_ = logoImageView;
//            [logoImageView setImageWithURL:[NSURL URLWithString:lstoreObj.image_url]
//                          placeholderImage:[UIImage imageNamed:@"list_thumbnail.png"]
//                                   success:^(UIImage *image) {
//                                       //DebugLog(@"success");
//                                   }
//                                   failure:^(NSError *error) {
//                                       //DebugLog(@"write error %@", error);
//                                       logoImg_.image = [UIImage imageNamed:@"list_thumbnail.png"];
//                                   }];
            [logoImageView setImageWithURL:[NSURL URLWithString:lstoreObj.image_url] placeholderImage:[UIImage imageNamed:@"list_thumbnail.png"] completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType) {
                if (error) {
                    logoImg_.image = [UIImage imageNamed:@"list_thumbnail.png"];
                }
            }];
        }else{
            //storeImageUrl = [NSURL URLWithString:THUMBNAIL_LINK(lstoreObj.image_url)];
            __weak UIImageView *logoImg_ = logoImageView;
//            [logoImageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(lstoreObj.image_url)]
//                          placeholderImage:[UIImage imageNamed:@"list_thumbnail.png"]
//                                   success:^(UIImage *image) {
//                                       //DebugLog(@"success");
//                                   }
//                                   failure:^(NSError *error) {
//                                       //DebugLog(@"write error %@", error);
//                                       logoImg_.image = [UIImage imageNamed:@"list_thumbnail.png"];
//                                   }];
            [logoImageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(lstoreObj.image_url)] placeholderImage:[UIImage imageNamed:@"list_thumbnail.png"] completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType) {
                if (error) {
                    logoImg_.image = [UIImage imageNamed:@"list_thumbnail.png"];
                }
            }];
        }
    }
    
    int buttonCount = 3;
    if (lstoreObj.fb_url != (NSString *)[NSNull null] && lstoreObj.fb_url != nil && ![lstoreObj.fb_url isEqualToString:@""]) {
        [fbBtn setHidden:NO];
    }else{
        [fbBtn setHidden:YES];
        buttonCount --;
    }
    if (lstoreObj.twitter_url != (NSString *)[NSNull null] && lstoreObj.twitter_url != nil && ![lstoreObj.twitter_url isEqualToString:@""]) {
        [twBtn setHidden:NO];
    }else{
        [twBtn setHidden:YES];
        buttonCount --;
    }
    if (lstoreObj.website != (NSString *)[NSNull null] && lstoreObj.website != nil && ![lstoreObj.website isEqualToString:@""]) {
        [webBtn setHidden:NO];
    }else{
        [webBtn setHidden:YES];
        buttonCount --;
    }
   
    if (buttonCount < 3) {
        if (buttonCount == 1) {
            CGFloat xaxis = 300 / 2 - (fbBtn.frame.size.width/2);
            if (!fbBtn.isHidden) {
                fbBtn.frame = CGRectMake(xaxis,fbBtn.frame.origin.y, fbBtn.frame.size.width, fbBtn.frame.size.height);
            }
            if (!twBtn.isHidden) {
                twBtn.frame = CGRectMake(xaxis,twBtn.frame.origin.y, twBtn.frame.size.width, twBtn.frame.size.height);
            }
            if (!webBtn.isHidden) {
                webBtn.frame = CGRectMake(xaxis,webBtn.frame.origin.y, webBtn.frame.size.width, webBtn.frame.size.height);
            }
        }
        if (buttonCount == 2) {
            CGFloat xaxis = (300 - (fbBtn.frame.size.width * 2)) / 3;
            if (!fbBtn.isHidden) {
                fbBtn.frame = CGRectMake(xaxis,fbBtn.frame.origin.y, fbBtn.frame.size.width, fbBtn.frame.size.height);
                xaxis = CGRectGetMaxX(fbBtn.frame)+xaxis;
            }
            if (!twBtn.isHidden) {
                twBtn.frame = CGRectMake(xaxis,twBtn.frame.origin.y, twBtn.frame.size.width, twBtn.frame.size.height);
                xaxis = CGRectGetMaxX(twBtn.frame)+xaxis;
            }
            if (!webBtn.isHidden) {
                webBtn.frame = CGRectMake(xaxis,webBtn.frame.origin.y, webBtn.frame.size.width, webBtn.frame.size.height);
            }
        }
    }
}

-(void)setStoreTimeValues:(NSDictionary *)lparams  storeobj:(INStoreDetailObj *)lstoreObj
{
    NSString *otime = [lparams objectForKey:@"open_time"];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setHour:[[otime substringToIndex:2] integerValue]];
    [comps setMinute:[[otime substringFromIndex:3] integerValue]];
    [comps setSecond:[[otime substringToIndex:6] integerValue]];
	NSDate *fromselectedDate = [[NSCalendar currentCalendar] dateFromComponents:comps];
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setDateFormat:@"hh:mm a"];
    NSString *fromString2 = [dateFormatter2 stringFromDate:fromselectedDate];
    DebugLog(@"from Date is: %@", fromString2);
    lstoreObj.open_time = fromString2;
    
    NSString *ctime = [lparams objectForKey:@"close_time"];
    NSDateComponents *comps1 = [[NSDateComponents alloc] init];
    [comps1 setHour:[[ctime substringToIndex:2] integerValue]];
    [comps1 setMinute:[[ctime substringFromIndex:3] integerValue]];
    [comps1 setSecond:[[ctime substringToIndex:6] integerValue]];
	NSDate *toselectedDate = [[NSCalendar currentCalendar] dateFromComponents:comps1];
    NSDateFormatter *dateFormatter3 = [[NSDateFormatter alloc] init];
    [dateFormatter3 setDateFormat:@"hh:mm a"];
    NSString *toString3 = [dateFormatter3 stringFromDate:toselectedDate];
    DebugLog(@"to Date is: %@", toString3);
    lstoreObj.close_time = toString3;
    
}

-(void)setStoreLikeType
{
    if([storeArray count] > 0)
    {
        INStoreDetailObj *lstoreObj = [storeArray objectAtIndex:0];
        DebugLog(@"like-->%@",lstoreObj.user_like);
        if([lstoreObj.user_like isEqualToString:@"1"])
        {
            [likeStore setSelected:TRUE];
            
        } else {
             [likeStore setSelected:FALSE];
        }
    }
}

-(void)removeHUDView
{
    [hud removeFromSuperview];
    hud = nil;
}

#pragma mark - Actionsheet Implementation
- (void)selectTel:(NSNumber *)lselectedIndex element:(id)element {
    //may have originated from textField or barButtonItem, use an IBOutlet instead of element
    selectedTelNumberToCall = [[telArray objectAtIndex:[lselectedIndex intValue]] stringByReplacingOccurrencesOfString:@" " withString:@""];
    DebugLog(@"selectedTelNumberToCall %@",selectedTelNumberToCall);
    
    NSString *deviceType = [UIDevice currentDevice].model;
    if([deviceType isEqualToString:@"iPhone"])
    {
//        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
//                                                           message: [NSString stringWithFormat: @"Do you want to call %@ store number ?",selectedTelNumberToCall]
//                                                          delegate: self
//                                                 cancelButtonTitle: nil
//                                                 otherButtonTitles: @"NO", @"YES", nil
//                              ];
//        [alert show];
//               
        NSString *areaName = [IN_APP_DELEGATE getActivePlaceNameFromAreaTable];
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:@"StoreDetails",@"Source",areaName,@"AreaName", nil];
        [INEventLogger logEvent:@"StoreCallDone" withParams:params];
    }
    else
    {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: @"Calling functionality is not available in this device. "
                                                          delegate: nil
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"OK", nil
                              ];
        [ alert show ];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:@"YES"])
    {
        UIApplication *myApp = [UIApplication sharedApplication];
        DebugLog(@"selectedTelNumberToCall = -%@-",selectedTelNumberToCall);
        [myApp openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",selectedTelNumberToCall]]];
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSString *title = [actionSheet buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"SMS"])
    {
        [self smsShareClicked];
    }
    else if([title isEqualToString:@"Email"])
    {
        [self emailShareClicked];
    }else if([title isEqualToString:@"WhatsApp"])
    {
        [self whatsAppShareClicked];
    }
    else if([title isEqualToString:@"Facebook"])
    {
        [self facebookShareClicked];
    } else if([title isEqualToString:@"Twitter"])
    {
        [self twitterShareClicked];
    }
    
    if(![title isEqualToString:@"Cancel"])
    {
        DebugLog(@"%@",[NSString stringWithFormat:@"%@(%@,%@)",@"StoreDetail_Share", [IN_APP_DELEGATE getActivePlaceNameFromAreaTable],self.title]);
        NSDictionary *storeDetailParams = [NSDictionary dictionaryWithObjectsAndKeys:[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"Area_Name", self.title,@"StoreName", nil];
        [INEventLogger logEvent:@"StoreDetail_Share" withParams:storeDetailParams];
    }
}

-(NSString *)getStoreDetailsMessageBody:(NSString *)shareVia{
    
    //    Template for Sharing Store from Market Place:
    //        ---------------------------------------------
    //        Subject (only for Mail): Store Name
    //        Store Name
    //        Address: <Store Address>, <City>
    //        Contact: +<Store Mobile Nos> <Store Landline nos>
    //        Timing: 10:00am To 11:00pm
    //        Description:
    //        On Google Map:
    //        (Shared via: http://www.shoplocal.co.in )
    
    NSMutableString *message = [[NSMutableString alloc] initWithString:@""];
    INStoreDetailObj *lstoreObj = [storeArray objectAtIndex:0];
    if (lstoreObj.name != nil && ![lstoreObj.name isEqualToString:@""] && ![shareVia isEqualToString:@"facebook"]) {
        [message appendString:[NSString stringWithFormat:@"%@\n",lstoreObj.name]];
    }
    
    if (![shareVia isEqualToString:@"twitter"]) {
        NSMutableString *addreessString = [[NSMutableString alloc] initWithString:@""];
        if (![lstoreObj.street isEqualToString:@""] && lstoreObj.street != nil) {
            [addreessString appendString:lstoreObj.street];
        }
        if (![lstoreObj.landmark isEqualToString:@""] && lstoreObj.landmark != nil) {
            if ([addreessString isEqualToString:@""]) {
                [addreessString appendString:lstoreObj.landmark];
            }else{
                [addreessString appendString:[NSString stringWithFormat:@", %@", lstoreObj.landmark]];
            }
        }
        if (![lstoreObj.area isEqualToString:@""] && lstoreObj.area != nil) {
            if ([addreessString isEqualToString:@""]) {
                [addreessString appendString:lstoreObj.area];
            }else{
                [addreessString appendString:[NSString stringWithFormat:@", %@", lstoreObj.area]];
            }
        }
        if (lstoreObj.city != nil && ![lstoreObj.city isEqualToString:@""]) {
            [addreessString appendString:[NSString stringWithFormat:@", %@",lstoreObj.city]];
        }
        if (![addreessString isEqualToString:@""] && ![shareVia isEqualToString:@"twitter"]) {
            [message appendString:[NSString stringWithFormat:@"Address: %@\n",addreessString]];
        }
    }
    
    NSMutableString *contactString = [[NSMutableString alloc] initWithString:@""];
    if (lstoreObj.mob_no1 != nil && ![lstoreObj.mob_no1 isEqualToString:@""]) {
        [contactString appendString:[NSString stringWithFormat:@"%@",lstoreObj.mob_no1]];
    }
    if (lstoreObj.mob_no2 != nil && ![lstoreObj.mob_no2 isEqualToString:@""]) {
        if ([contactString isEqualToString:@""]) {
            [contactString appendString:lstoreObj.mob_no2];
        }else{
            [contactString appendString:[NSString stringWithFormat:@", %@", lstoreObj.mob_no2]];
        }
    }
    if (lstoreObj.mob_no3 != nil && ![lstoreObj.mob_no3 isEqualToString:@""]) {
        if ([contactString isEqualToString:@""]) {
            [contactString appendString:lstoreObj.mob_no3];
        }else{
            [contactString appendString:[NSString stringWithFormat:@", %@", lstoreObj.mob_no3]];
        }
    }
    if (lstoreObj.tel_no1 != nil && ![lstoreObj.tel_no1 isEqualToString:@""]) {
        if ([contactString isEqualToString:@""]) {
            [contactString appendString:lstoreObj.tel_no1];
        }else{
            [contactString appendString:[NSString stringWithFormat:@", %@", lstoreObj.tel_no1]];
        }
    }
    if (lstoreObj.tel_no2 != nil && ![lstoreObj.tel_no2 isEqualToString:@""]) {
        if ([contactString isEqualToString:@""]) {
            [contactString appendString:lstoreObj.tel_no2];
        }else{
            [contactString appendString:[NSString stringWithFormat:@", %@", lstoreObj.tel_no2]];
        }
    }
    if (lstoreObj.tel_no3 != nil && ![lstoreObj.tel_no3 isEqualToString:@""]) {
        if ([contactString isEqualToString:@""]) {
            [contactString appendString:lstoreObj.tel_no3];
        }else{
            [contactString appendString:[NSString stringWithFormat:@", %@", lstoreObj.tel_no3]];
        }
    }
    if (![contactString isEqualToString:@""]) {
        [message appendString:[NSString stringWithFormat:@"Contact: %@\n",contactString]];
    }
    
    if (![shareVia isEqualToString:@"twitter"]) {
        if (lstoreObj.open_time != nil && ![lstoreObj.open_time isEqualToString:@""]) {
            [message appendString:[NSString stringWithFormat:@"Timings: %@ To %@\n",[lstoreObj.open_time lowercaseString], [lstoreObj.close_time  lowercaseString]]];
        }
        if (lstoreObj.description != nil && ![lstoreObj.description isEqualToString:@""]) {
            [message appendString:[NSString stringWithFormat:@"Description: %@\n",lstoreObj.description]];
        }
        
        if ([lstoreObj.latitude length] > 0 && [lstoreObj.longitude length] > 0 && [lstoreObj.latitude doubleValue] != 0 && [lstoreObj.longitude doubleValue] != 0) {
            [message appendString:[NSString stringWithFormat:@"On Google Map: \n %@\n",MAP_URL([lstoreObj.latitude floatValue],[lstoreObj.longitude floatValue])]];
        }
    }
    [message appendString:@"(Shared via: http://www.shoplocal.co.in)\n"];
    return message;    
}

#pragma share Callbacks
-(void)smsShareClicked{
    if ([MFMessageComposeViewController canSendText]) {
        NSString *message = [self getStoreDetailsMessageBody:@"sms"];
        MFMessageComposeViewController *messageComposer = [[MFMessageComposeViewController alloc] init];
        messageComposer.messageComposeDelegate = self;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            messageComposer.navigationBar.tintColor  = [UIColor whiteColor];
        }
        [messageComposer setBody:message];
        [messageComposer setRecipients:nil];
        [self presentViewController:messageComposer animated:YES completion:nil];
    }else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"You may not have set up sms service on your device.\nPlease check and try again."
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}
#pragma mark MFMessageComposeViewController Delegate Methods
-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
		case MessageComposeResultCancelled:
            DebugLog(@"SMS Cancelled");
			break;
		case MessageComposeResultFailed:
        {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Unknown Error\nSMS failed"
														   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
        }
			break;
		case MessageComposeResultSent:
        {
            DebugLog(@"SMS Sent");
        }
			break;
		default:
			break;
	}
    [self dismissViewControllerAnimated:YES completion:^{
        if (result == MessageComposeResultSent) {
            [self sendStoreShareRequest:@"sms"];
        }
    }];
}

-(void)emailShareClicked{
    if ([MFMailComposeViewController canSendMail]) {
        NSString *message = [self getStoreDetailsMessageBody:@"email"];
        DebugLog(@"message %@",message);
        MFMailComposeViewController *emailComposer = [[MFMailComposeViewController alloc] init];
        emailComposer.mailComposeDelegate = self;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            emailComposer.navigationBar.tintColor  = [UIColor whiteColor];
        }
        [emailComposer setToRecipients:nil];
        INStoreDetailObj *lstoreObj = [storeArray objectAtIndex:0];
        if (lstoreObj.name != nil && ![lstoreObj.name isEqualToString:@""]) {
            [emailComposer setSubject:lstoreObj.name];
        }else{
            [emailComposer setSubject:@""];
        }
        if (lstoreObj.image_url != nil && ![lstoreObj.image_url isEqualToString:@""]) {
            NSString *htmlMessage = [message stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
            DebugLog(@"%@--%@",message,htmlMessage);
            [emailComposer setMessageBody:[NSString stringWithFormat:@"%@ <br><br><img src=%@ alt=%@/>",htmlMessage,THUMBNAIL_LINK(lstoreObj.image_url),lstoreObj.name] isHTML:YES];
        }else{
            [emailComposer setMessageBody:message isHTML:NO];
        }
        [self presentViewController:emailComposer animated:YES completion:nil];
    }else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"You may not have set up mail service on your device or You may not connected to internet.\nPlease check and try again."
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}
#pragma mark Mail Compose Delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    if (result == MFMailComposeResultFailed) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                        message:NSLocalizedString(@"Email failed to send. Please try again.", nil)
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", nil) otherButtonTitles:nil];
		[alert show];
    }
    [self dismissViewControllerAnimated:YES completion:^{
        if (controller.toolbar.tag != 1 && result == MFMailComposeResultSent) {
            [self sendStoreShareRequest:@"email"];
        }
    }];
}

-(void)whatsAppShareClicked{
    NSString *message = [self getStoreDetailsMessageBody:@"whatsapp"];
    NSString *urlString = [NSString stringWithFormat:@"whatsapp://send?text=%@",[message stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURL *whatsappURL = [NSURL URLWithString:urlString];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [[UIApplication sharedApplication] openURL: whatsappURL];
        [self sendStoreShareRequest:@"whatsapp"];
    }
}

-(void)twitterShareClicked{
    if ([IN_APP_DELEGATE networkavailable]) {
        NSString *message = [self getStoreDetailsMessageBody:@"twitter"];
        INStoreDetailObj *lstoreObj = [storeArray objectAtIndex:0];
        NSString *postimageUrl = lstoreObj.image_url;

        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
            
            SLComposeViewController *socialComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
            if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
            {
                if ([message length] > 140) {
                    [socialComposer setInitialText:[message substringToIndex:140]];
                }else{
                    [socialComposer setInitialText:message];
                }
            }
            else{
                [socialComposer setInitialText:message];
            }
            if(postimageUrl != nil && ![postimageUrl isEqualToString:@""])
            {
                UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:THUMBNAIL_LINK(postimageUrl)]]];
                [socialComposer addImage:image];
            }
            [socialComposer setCompletionHandler:^(SLComposeViewControllerResult result){
                switch (result) {
                    case SLComposeViewControllerResultCancelled:
                        DebugLog(@"SLComposeViewControllerResultCancelled");
                        break;
                    case SLComposeViewControllerResultDone:
                        DebugLog(@"SLComposeViewControllerResultDone");
                        [self sendStoreShareRequest:@"twitter"];
                        break;
                    default:
                        DebugLog(@"SLComposeViewControllerResultFailed");
                        break;
                }
                [self dismissViewControllerAnimated:YES completion:NULL];
            }];
            [self presentViewController:socialComposer animated:YES completion:Nil];
        }
        else{
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:ALERT_TITLE
                                      message:@"You may not have set up twitter service on your device.Please check and try again."
                                      delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [alertView show];
        }
    }else{
        [INUserDefaultOperations showOfflineAlert];
    }
}

-(void)facebookShareClicked{
    if ([IN_APP_DELEGATE networkavailable]) {
        [self sendStoreShareRequest:@"facebook"];

        NSMutableString *message = [[NSMutableString alloc] initWithString:[self getStoreDetailsMessageBody:@"facebook"]];
        DebugLog(@"message %@",message);
        INStoreDetailObj *lstoreObj = [storeArray objectAtIndex:0];
        NSString *imageUrl = lstoreObj.image_url;
        NSString *storeName = lstoreObj.name;
        NSString *deepLinkingUrl = [FacebookOpenGraphAPI createDeepLinkWithActiveAreaName:[IN_APP_DELEGATE getActiveSlugFromAreaTable] storeName:lstoreObj.name storeId:[NSString stringWithFormat:@"%d",storeId]];
        
        if (imageUrl != nil && ![imageUrl isEqualToString:@""]) {
            if ([imageUrl hasPrefix:@"http"]) {
                [FacebookShareAPI shareContentWithTitle:storeName description:message image:imageUrl deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_STORE];
            }else{
                [FacebookShareAPI shareContentWithTitle:storeName description:message image:THUMBNAIL_LINK(imageUrl) deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_STORE];
            }
        }else{
            [FacebookShareAPI shareContentWithTitle:storeName description:message image:@"" deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_STORE];
        }
    }else{
        [INUserDefaultOperations showOfflineAlert];
    }
    
//    if ([IN_APP_DELEGATE networkavailable]) {
//        NSString *message = [self getStoreDetailsMessageBody:@"facebook"];
//        INStoreDetailObj *lstoreObj = [storeArray objectAtIndex:0];
//        NSString *postimageUrl = lstoreObj.image_url;
//
//        if (SYSTEM_VERSION_LESS_THAN(@"6.0"))
//        {
//            FaceBookShareViewController *facebookViewComposer = [[FaceBookShareViewController alloc] initWithNibName:@"FaceBookShareViewController" bundle:nil];
//            facebookViewComposer.fbdelegate = self;
//            facebookViewComposer.title = @"FACEBOOK SHARE";
//            facebookViewComposer.FBtitle = ALERT_TITLE;
//            facebookViewComposer.postMessageText = message;
//            if (postimageUrl != nil && ![postimageUrl isEqualToString:@""]) {
//                facebookViewComposer.FBtPic = THUMBNAIL_LINK(postimageUrl);
//            }
//            UINavigationController *fbnavBar=[[UINavigationController alloc]initWithRootViewController:facebookViewComposer];
//            [self.navigationController presentViewController:fbnavBar animated:YES completion:nil];
//        }else{
//            if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
//                
//                SLComposeViewController *socialComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
//                [socialComposer setInitialText:message];
//                if (postimageUrl != nil && ![postimageUrl isEqualToString:@""]) {
//                    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:THUMBNAIL_LINK(postimageUrl)]]];
//                    [socialComposer addImage:image];
//                }
//                [socialComposer setCompletionHandler:^(SLComposeViewControllerResult result){
//                    switch (result) {
//                        case SLComposeViewControllerResultCancelled:
//                            DebugLog(@"SLComposeViewControllerResultCancelled");
//                            break;
//                        case SLComposeViewControllerResultDone:
//                            DebugLog(@"SLComposeViewControllerResultDone");
//                            [self sendStoreShareRequest:@"facebook"];
//                            break;
//                        default:
//                            DebugLog(@"SLComposeViewControllerResultFailed");
//                            break;
//                    }
//                    [self dismissViewControllerAnimated:YES completion:NULL];
//                }];
//                [self presentViewController:socialComposer animated:YES completion:Nil];
//            }
//            else{
//                UIAlertView *alertView = [[UIAlertView alloc]
//                                          initWithTitle:ALERT_TITLE
//                                          message:@"You may not have set up facebook service on your device.Please check and try again."
//                                          delegate:nil
//                                          cancelButtonTitle:@"OK"
//                                          otherButtonTitles:nil];
//                [alertView show];
//            }
//        }
//    }else{
//        [INUserDefaultOperations showOfflineAlert];
//    }
}

-(void)faceBookCompletionCallBack:(int)result
{
    DebugLog(@"Customer : faceBookCompletionCallBack %d",result);
    if (result == FACEBOOK_POST_SUCCESS) {
        [self sendStoreShareRequest:@"facebook"];
    }
}

-(void)sendStoreShareRequest:(NSString *)viaString{

    if ([[INUserDefaultOperations getCustomerLoginFrom] isEqualToString:FACEBOOKSERVER] && [INUserDefaultOperations isOpenGraphShareEnabled] && ![viaString isEqualToString:@"facebook"]) {
        SIAlertView *sialertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Do you want to share this on your Facebook timeline?"];
        sialertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
        sialertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
        [sialertView addButtonWithTitle:@"YES"
                                   type:SIAlertViewButtonTypeDestructive
                                handler:^(SIAlertView *alert) {
                                    DebugLog(@"YES");
                                    INStoreDetailObj *lstoreObj = [storeArray objectAtIndex:0];
                                    NSString *deepLinkingUrl = [FacebookOpenGraphAPI createDeepLinkWithActiveAreaName:[IN_APP_DELEGATE getActiveSlugFromAreaTable] storeName:lstoreObj.name storeId:[NSString stringWithFormat:@"%d",storeId]];
                                    
                                    if (lstoreObj.image_url != nil && ![lstoreObj.image_url isEqualToString:@""]) {
                                        if ([lstoreObj.image_url hasPrefix:@"http"]) {
                                            [FacebookOpenGraphAPI shareContentUsingOpenGraphAPIWithTitle:lstoreObj.name description:lstoreObj.description image:lstoreObj.image_url deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_STORE];
                                        }else{
                                            [FacebookOpenGraphAPI shareContentUsingOpenGraphAPIWithTitle:lstoreObj.name description:lstoreObj.description image:THUMBNAIL_LINK(lstoreObj.image_url) deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_STORE];
                                        }
                                    }else{
                                        [FacebookOpenGraphAPI shareContentUsingOpenGraphAPIWithTitle:lstoreObj.name description:lstoreObj.description image:@"" deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_STORE];
                                    }
                                }];
        [sialertView addButtonWithTitle:@"NOT NOW"
                                   type:SIAlertViewButtonTypeCancel
                                handler:^(SIAlertView *alert) {
                                    DebugLog(@"NO");
                                }];
        [sialertView show];
    }
    
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:[NSString stringWithFormat:@"%d",self.storeId] forKey:@"place_id"];
    [params setObject:viaString forKey:@"via"];
    
    [httpClient postPath:SHARE_STORE parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            //            NSString* message = [json objectForKey:@"message"];
            //            [INUserDefaultOperations showAlert:message];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        [CommonCallback hideProgressHud];
    }];
}

#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:callModalTableView]) {
         return 80;
    }
    return 100;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:callModalTableView]) {
        return [telArray count];
    }
    return [broadcastArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:callModalTableView]) {
        static NSString *CellIdentifier = @"CallCell";
        UIImageView *thumbImg;
        UILabel *lblNumber;
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.backgroundColor = [UIColor whiteColor];
            
            thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(10,15, 50, 50)];
            thumbImg.tag = 1111;
            thumbImg.contentMode = UIViewContentModeScaleToFill;
            thumbImg.image = [UIImage imageNamed:@"call_brown.png"];
            [cell.contentView addSubview:thumbImg];
            
            lblNumber = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame),0, 210, 80)];
            lblNumber.text = @"";
            lblNumber.tag = 2222;
            lblNumber.textColor             = BROWN_COLOR;
            lblNumber.highlightedTextColor  = [UIColor whiteColor];
            lblNumber.font                  = DEFAULT_FONT(23);
            lblNumber.textAlignment         = NSTextAlignmentLeft;
            lblNumber.backgroundColor       = [UIColor clearColor];
            [cell.contentView addSubview:lblNumber];
            
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = BROWN_OFFWHITE_COLOR;
            cell.selectedBackgroundView = bgColorView;
        }
        //cell.textLabel.text = [telArray objectAtIndex:indexPath.row];
        lblNumber        = (UILabel *)[cell.contentView viewWithTag:2222];
        lblNumber.text   = [telArray objectAtIndex:indexPath.row];
        return cell;
    }else{
        static NSString *CellIdentifier = @"Cell";
        UILabel *lblDay;
        UILabel *lblMonth;
        UILabel *lblTitle;
        
        UIImageView *thumbImg;
        
        UIImageView *likethumbImg;
        UILabel *lblLikeCount;
        
        UIImageView *sharethumbImg;
        UILabel *lblShareCount;
        
        UIImageView *offerthumbImg;
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            
            thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(5.0,5.0,90.0,90.0)];
            thumbImg.tag = 103;
            thumbImg.contentMode = UIViewContentModeScaleToFill;
            [cell.contentView addSubview:thumbImg];
            
            lblDay = [[UILabel alloc] initWithFrame:CGRectMake(5.0,5.0,90.0,90.0)];
            lblDay.text = @"";
            lblDay.tag = 100;
            lblDay.font = [UIFont systemFontOfSize:35];
            lblDay.textAlignment = NSTextAlignmentCenter;
            lblDay.textColor = [UIColor whiteColor];
            lblDay.backgroundColor =  LIGHT_BLUE;
            [cell.contentView addSubview:lblDay];
            
            lblMonth = [[UILabel alloc] initWithFrame:CGRectMake(5.0,CGRectGetMaxY(thumbImg.frame)-20,90.0,20.0)];
            lblMonth.text = @"";
            lblMonth.tag = 101;
            lblMonth.font = [UIFont systemFontOfSize:15];
            lblMonth.textAlignment = NSTextAlignmentCenter;
            lblMonth.textColor = [UIColor whiteColor];
            lblMonth.backgroundColor =  DARK_YELLOW_COLOR;
            [cell.contentView addSubview:lblMonth];
            
            lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame)+5,5.0,150.0,80.0)];
            lblTitle.text = @"";
            lblTitle.tag = 102;
            lblTitle.numberOfLines = 3;
            lblTitle.font = DEFAULT_BOLD_FONT(15);
            lblTitle.minimumScaleFactor = 14;
            lblTitle.adjustsFontSizeToFitWidth = TRUE;
            lblTitle.textAlignment = NSTextAlignmentLeft;
            lblTitle.textColor = BROWN_COLOR;
            lblTitle.backgroundColor =  [UIColor clearColor];
            [cell.contentView addSubview:lblTitle];
            
            likethumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lblTitle.frame)+2,10.0,20.0,20.0)];
            likethumbImg.tag = 104;
            likethumbImg.image = [UIImage imageNamed:@"Shoplocal_Post-Title_Fv.png"];
            likethumbImg.contentMode = UIViewContentModeScaleAspectFill;
            [cell.contentView addSubview:likethumbImg];
            
            lblLikeCount = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(likethumbImg.frame)+2,10.0,30.0,20.0)];
            lblLikeCount.text = @"";
            lblLikeCount.tag = 105;
            lblLikeCount.font = DEFAULT_BOLD_FONT(12);
            lblLikeCount.textAlignment = NSTextAlignmentLeft;
            lblLikeCount.textColor = BROWN_COLOR;
            lblLikeCount.backgroundColor =  [UIColor clearColor];
            [cell.contentView addSubview:lblLikeCount];
            
            sharethumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(likethumbImg.frame.origin.x,CGRectGetMaxY(likethumbImg.frame)+2,20.0,20.0)];
            sharethumbImg.tag = 106;
            sharethumbImg.image = [UIImage imageNamed:@"Shoplocal_Post-Title_Share.png"];
            sharethumbImg.contentMode = UIViewContentModeScaleAspectFill;
            [cell.contentView addSubview:sharethumbImg];
            
            lblShareCount = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(sharethumbImg.frame)+2,sharethumbImg.frame.origin.y,30.0,20.0)];
            lblShareCount.text = @"";
            lblShareCount.tag = 107;
            lblShareCount.font = DEFAULT_BOLD_FONT(12);
            lblShareCount.textAlignment = NSTextAlignmentLeft;
            lblShareCount.textColor = BROWN_COLOR;
            lblShareCount.backgroundColor =  [UIColor clearColor];
            [cell.contentView addSubview:lblShareCount];
            
            offerthumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(5,8,61.0,40.0)];
            offerthumbImg.tag = 108;
            offerthumbImg.image = [UIImage imageNamed:@"Offer_Icon1.png"];
            offerthumbImg.contentMode = UIViewContentModeScaleAspectFill;
            [cell.contentView addSubview:offerthumbImg];
            
            UILabel *lblline = [[UILabel alloc] initWithFrame:CGRectMake(0, 98.0, 310.0, 3.0)];
            lblline.text = @"";
            lblline.backgroundColor =  LIGHT_LINE_COLOR;
            [cell.contentView addSubview:lblline];

            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = BROWN_OFFWHITE_COLOR;
            cell.selectedBackgroundView = bgColorView;
        }
        if ([indexPath row] < [broadcastArray count]) {
            lblDay      = (UILabel *)[cell viewWithTag:100];
            lblMonth    = (UILabel *)[cell viewWithTag:101];
            lblTitle    = (UILabel *)[cell viewWithTag:102];
            thumbImg    = (UIImageView *)[cell viewWithTag:103];
            likethumbImg    = (UIImageView *)[cell viewWithTag:104];
            lblLikeCount    = (UILabel *)[cell viewWithTag:105];
            sharethumbImg   = (UIImageView *)[cell viewWithTag:106];
            lblShareCount   = (UILabel *)[cell viewWithTag:107];
            offerthumbImg   = (UIImageView *)[cell viewWithTag:108];
            
            INBroadcastDetailObj *lbroadcastObj  = [broadcastArray objectAtIndex:indexPath.row];
            DebugLog(@"%@",lbroadcastObj.date);
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            DebugLog(@"%@",[NSTimeZone localTimeZone]);
            [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
            NSDate *myDate = [[NSDate alloc] init];
            myDate = [dateFormatter dateFromString:lbroadcastObj.date];
            DebugLog(@"MyDate is: %@", myDate);
            
            NSDateFormatter *dayformatter = [[NSDateFormatter alloc] init];
            [dayformatter setDateFormat:@"dd"];
            NSString *dayFromDate = [dayformatter stringFromDate:myDate];
            DebugLog(@"Myday is: %@", dayFromDate);
            lblDay.text = dayFromDate;
            
            NSDateFormatter *monthformatter = [[NSDateFormatter alloc] init];
            [monthformatter setDateFormat:@"MMM"];
            NSString *monthFromDate = [monthformatter stringFromDate:myDate];
            DebugLog(@"MyMonth is: %@", monthFromDate);
            lblMonth.text = monthFromDate;
            
            
            lblTitle.text = lbroadcastObj.title;
            
            if(lbroadcastObj.thumb_url1 != (NSString*)[NSNull null] && ![lbroadcastObj.thumb_url1 isEqualToString:@""]) {
                lblDay.backgroundColor = [UIColor clearColor];

                __weak UIImageView *thumbImg_ = thumbImg;
                
                [thumbImg setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(lbroadcastObj.thumb_url1)] placeholderImage:[UIImage imageNamed:@"list_thumbnail.png"] completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType) {
                    if (error) {
                        thumbImg_.image = [UIImage imageNamed:@"list_thumbnail.png"];
                    }
                }];
                DebugLog(@"link-%@",THUMBNAIL_LINK(lbroadcastObj.image_url1));
            } else {
                thumbImg.image = nil;
                lblDay.backgroundColor =  LIGHT_BLUE;
            }
            
            if ([lbroadcastObj.is_offered length] > 0 && [lbroadcastObj.is_offered isEqualToString:@"1"]) {
                offerthumbImg.hidden = FALSE;
            }else{
                offerthumbImg.hidden = TRUE;
            }

            
            [lblLikeCount setHidden:FALSE];
            [likethumbImg setHidden:FALSE];
            lblLikeCount.text = lbroadcastObj.total_like;
            
            [lblShareCount setHidden:FALSE];
            [sharethumbImg setHidden:FALSE];
            lblShareCount.text = lbroadcastObj.total_share;
        }
        return cell;
    }
}


#pragma mark - Table view delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:callModalTableView]) {
        [callModalView setHidden:YES];
        [callModalViewBackView setHidden:YES];

        selectedTelNumberToCall = [[telArray objectAtIndex:indexPath.row] stringByReplacingOccurrencesOfString:@" " withString:@""];
        DebugLog(@"selectedTelNumberToCall %@",selectedTelNumberToCall);
        NSString *deviceType = [UIDevice currentDevice].model;
        if([deviceType isEqualToString:@"iPhone"])
        {
            UIApplication *myApp = [UIApplication sharedApplication];
            DebugLog(@"selectedTelNumberToCall = -%@-",selectedTelNumberToCall);
            [myApp openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",selectedTelNumberToCall]]];
            
            
            NSDictionary *callParams = [NSDictionary dictionaryWithObjectsAndKeys:@"StoreDetails",@"Source",[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"AreaName",self.title,@"StoreName",selectedTelNumberToCall,@"Number", nil];
            [INEventLogger logEvent:@"StoreCallDone" withParams:callParams];
            
            [self sendStoreCallRequest:@"StoreCallDone"];
        }
        else
        {
            UIAlertView *alert = [[ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                              message: @"Calling functionality is not available in this device. "
                                                             delegate: nil
                                                    cancelButtonTitle: nil
                                                    otherButtonTitles: @"OK", nil];
            [alert show];
        }
    }else{
        NSDictionary *storeDetailParams = [NSDictionary dictionaryWithObjectsAndKeys:[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"Area_Name", self.title,@"StoreName", nil];
        [INEventLogger logEvent:@"StoreDetail_ViewOffer" withParams:storeDetailParams];
        INBroadcastDetailObj *tempbroadcastObj  = [broadcastArray objectAtIndex:indexPath.row];
        INCustBroadCastDetailsViewController *postDetailController = [[INCustBroadCastDetailsViewController alloc] initWithNibName:@"INCustBroadCastDetailsViewController" bundle:nil] ;
        
        DebugLog(@"tempbroadcastObj.storename %@ ",self.title);
        postDetailController.title = self.title;//@"Broadcast Detail";
        postDetailController.storeName = self.title;
        postDetailController.postType = 1;
        postDetailController.postlikeType = tempbroadcastObj.user_like;
        postDetailController.postId = tempbroadcastObj.ID;
        postDetailController.postTitle = tempbroadcastObj.title;
        postDetailController.postDescription = tempbroadcastObj.description;
        if ([tempbroadcastObj.image_url1 isEqual:[NSNull null]] || tempbroadcastObj.image_url1 == nil) {
            postDetailController.postimageUrl = @"";
        }else{
            postDetailController.postimageUrl = tempbroadcastObj.image_url1;
        }
        postDetailController.postimageTitle = tempbroadcastObj.image_title1;
        postDetailController.viewOpenedFrom = FROM_STORE_DETAILS;
        postDetailController.likescountString = tempbroadcastObj.total_like;
        postDetailController.sharecountString = tempbroadcastObj.total_share;
        postDetailController.telArray = telArray;
        postDetailController.SHOW_BUY_BTN = YES;
        postDetailController.placeId = tempbroadcastObj.place_ID;
        postDetailController.postDateTime   = broadcastObj.date;
        postDetailController.postOfferDateTime  = broadcastObj.offerdate;
        postDetailController.postIsOffered      = broadcastObj.is_offered;
        [self.navigationController pushViewController:postDetailController animated:YES];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - VCGridViewDataSource

- (NSInteger)numberOfCellsInGridView:(VCGridView*)gridView
{
    return [galleryArray count];
}

- (VCGridViewCell *)gridView:(VCGridView *)gridView cellAtIndex:(NSInteger)index
{
    UIImageView *imageView;
    UILabel *lblTitle;
    
	VCGridViewCell *cell = [gridView dequeueReusableCell];
	if (!cell) {
		cell = [[VCGridViewCell alloc] initWithFrame:CGRectZero];
		
		CGRect contentFrame = CGRectInset(cell.bounds, 0, 0);
        //        CGRect contentFrame = CGRectInset(CGRectMake(0, 0, 75, 75), 0, 0);
        
		imageView = [[UIImageView alloc] initWithFrame:contentFrame];
		imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		imageView.contentMode = UIViewContentModeScaleToFill;
        imageView.tag = 222;
		//imageView.image = [UIImage imageNamed:@"cell"];
        // imageView.layer.cornerRadius = 8.0;
		[cell.contentView addSubview:imageView];
        
		UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:contentFrame];
		imageView1.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		imageView1.contentMode = UIViewContentModeBottomRight;
		imageView1.image = [UIImage imageNamed:@"check"];
		cell.editingSelectionOverlayView = imageView1;
        
        
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0,110, 135, 25)];
        lblTitle.text = @"title";
        lblTitle.tag = 223;
        lblTitle.font = DEFAULT_BOLD_FONT(15);
        lblTitle.textAlignment = NSTextAlignmentCenter;
        lblTitle.textColor = [UIColor whiteColor];
        lblTitle.backgroundColor =  BROWN_OFFWHITE_COLOR;
        [cell.contentView addSubview:lblTitle];
        
        cell.contentView.layer.cornerRadius = 8.0;
        cell.contentView.clipsToBounds = YES;
        
        
		cell.highlightedBackgroundView.backgroundColor = BROWN_OFFWHITE_COLOR;
        cell.highlightedBackgroundView.layer.cornerRadius = 8.0;
	}
    imageView = (UIImageView *)[cell viewWithTag:222];
    lblTitle = (UILabel *)[cell viewWithTag:223];
    
    if (index < [galleryArray count]) {
        imageView.backgroundColor = [UIColor clearColor];
        if ([[galleryArray objectAtIndex:index] isKindOfClass:[INGalleryObj class]]) {
            INGalleryObj *tempgalleryObj  = [galleryArray objectAtIndex:index];
            [imageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(tempgalleryObj.thumb_url)] placeholderImage:[UIImage imageNamed:@"list_thumbnail.png"]];
            DebugLog(@"index %d : link-%@",index,THUMBNAIL_LINK(tempgalleryObj.thumb_url));
            
            if (tempgalleryObj.title == nil || [tempgalleryObj.title isEqualToString:@""]) {
                lblTitle.hidden = TRUE;
            }else{
                lblTitle.hidden = FALSE;
                lblTitle.text = [tempgalleryObj.title capitalizedString];
            }
        }
    }
	return cell;
}

- (BOOL)gridView:(VCGridView *)gridView canEditCellAtIndex:(NSInteger)index
{
    return NO;
}

#pragma mark - VCGridViewDelegate

- (void)gridView:(VCGridView*)gridView didSelectCellAtIndex:(NSInteger)index
{
#if DEBUG
	DebugLog(@"Selected %i", index);
#endif
        if (index < [galleryArray count]) {
            if ([[galleryArray objectAtIndex:index] isKindOfClass:[INGalleryObj class]]) {
                DebugLog(@"push to Browser %i", index);
                if(_photos == nil)
                {
                    _photos = [[NSMutableArray alloc] init];
                } else {
                    [_photos removeAllObjects];
                }
                MWPhoto *photo;
                INGalleryObj *tempgalleryObj;
                for(int i = 0; i < [galleryArray count]; i++)
                {
                    tempgalleryObj = (INGalleryObj *)[galleryArray objectAtIndex:i];
                    DebugLog(@"title--- %@",tempgalleryObj.title);
                    photo = [[MWPhoto alloc] initWithURL:[NSURL URLWithString:THUMBNAIL_LINK(tempgalleryObj.image_url)]];
                    if (tempgalleryObj.title != nil || ![tempgalleryObj.title isEqualToString:@""])
                        photo.caption = [NSString stringWithFormat:@"%@",tempgalleryObj.title];
                    [_photos addObject:photo];
                    tempgalleryObj = nil;
                }
                NSDictionary *storeDetailParams = [NSDictionary dictionaryWithObjectsAndKeys:[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"Area_Name", self.title,@"StoreName", nil];
                [INEventLogger logEvent:@"StoreDetail_ViewGallery" withParams:storeDetailParams];
                
//                MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
//                browser.displayActionButton = YES;
//                browser.wantsFullScreenLayout = YES;
//                [browser setInitialPageIndex:index];
//                [self.navigationController pushViewController:browser animated:YES];
                
                // Create browser
                MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
                // Set options
                browser.displayActionButton = YES; // Show action button to allow sharing, copying, etc (defaults to YES)
                browser.wantsFullScreenLayout = YES; // iOS 5 & 6 only: Decide if you want the photo browser full screen, i.e. whether the status bar is affected (defaults to YES)
                
                browser.displayNavArrows = YES; // Whether to display left and right nav arrows on toolbar (defaults to NO)
                browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
                browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
                browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
                browser.enableGrid = NO; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
                browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
                
                // Optionally set the current visible photo before displaying
                [browser setCurrentPhotoIndex:index];
                
                // Present
                [self.navigationController pushViewController:browser animated:YES];
            }
        }
}

- (CGSize)sizeForCellsInGridView:(VCGridView *)gridView
{
	return CGSizeMake(135.0f, 135.0f);
}

- (CGFloat)paddingForCellsInGridView:(VCGridView *)gridView
{
	return 10.0f;
}

#pragma MWPhotoBrowser delegate methods
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser
{
    return [_photos count];
}
- (id<MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index
{
    return [_photos objectAtIndex:index];
}

- (void)viewDidUnload {
    [self setDetailScrollView:nil];
    [self setViewsCollection:nil];
    [self setLogoImageView:nil];
    [self setDescriptionTextView:nil];
    [self setLblstoretime:nil];
    [self setCallStoreBtn:nil];
    [self setLikeStore:nil];
    [self setShareStoreBtn:nil];
    [self setAddressView:nil];
    [self setCallStoreBtn:nil];
    [self setCallStoreBtn:nil];
    [self setLblemail:nil];
    [self setBroadcastTableView:nil];
    [self setGalleryView:nil];
    [self setNumberslbl:nil];
    [self setEmailImageView:nil];
    [self setBroadcastlbl:nil];
    [self setGallerylbl:nil];
    [self setEmailBtn:nil];
    [self setBroadcastImageView:nil];
    [self setGalleryImageView:nil];
    [super viewDidUnload];
}

-(void)sendStoreCallRequest:(NSString *)callType
{
    
    if (![INUserDefaultOperations isCustomerLoggedIn]) {
        return;
    }
    
    NSString *areaID = [IN_APP_DELEGATE getActiveShoplocalPlaceIdFromAreaTable];
    
    DebugLog(@"========================sendStoreCallRequest========================");
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSString *register_from = [NSString stringWithFormat:@"%@ v%@",DEVICE_TYPE,version];
    
    NSMutableDictionary *postparams = [[NSMutableDictionary alloc] init];
    //    {"user_id":"${user_id}","auth_id":"${auth_id}","place_id":"106","via":"welcome ","call_type":"type-a","active_area":"1"}
    [postparams setObject:[INUserDefaultOperations getCustomerAuthId] forKey:@"user_id"];
    [postparams setObject:[INUserDefaultOperations getCustomerAuthCode] forKey:@"auth_id"];
    [postparams setObject:[NSString stringWithFormat:@"%d",storeId] forKey:@"place_id"];
    [postparams setObject:callType forKey:@"call_type"];
    [postparams setObject:areaID forKey:@"active_area"];
    [postparams setObject:register_from forKey:@"via"];
    
    DebugLog(@"sendStoreCallRequest : postParams -%@-",postparams);
    
    [httpClient postPath:CALL_STORE parameters:postparams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"sendStoreCallRequest : json afnetworking ->%@",json);
        if (json!= nil) {
            NSString* code = [json objectForKey:@"code"];
            if([code isEqualToString:CUSTOMER_INVALID_USER_AUTH_CODE])
            {
                if([INUserDefaultOperations isCustomerLoggedIn]){
                    [INUserDefaultOperations clearCustomerDetails];
                }
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"sendStoreCallRequest : [HTTPClient Error]: %@", error.localizedDescription);
    }];
}
@end
