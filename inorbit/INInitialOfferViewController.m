//
//  INInitialOfferViewController.m
//  shoplocal
//
//  Created by Rishi on 04/10/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INInitialOfferViewController.h"
#import "CommonCallback.h"
#import "constants.h"

@interface INInitialOfferViewController ()

@end

@implementation INInitialOfferViewController
@synthesize messageView;
@synthesize messagelbl;
@synthesize postOfferBtn;
@synthesize skipBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //messageView = [CommonCallback setViewPropertiesWithRoundedCorner:messageView];
    messageView.backgroundColor =  [UIColor whiteColor];
    messageView.clipsToBounds = YES;
    
    postOfferBtn.titleLabel.font = DEFAULT_FONT(16);
    skipBtn.titleLabel.font = DEFAULT_FONT(16);
    
    messagelbl.font    = DEFAULT_FONT(14);
    messagelbl.textColor = BROWN_COLOR;
    messagelbl.text = @"Welcome to Shoplocal. You can start talking to customers now. Post an Offer, a special Discount, new stocks or photos of your business. Start talking to thousands of customers in your area.";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setMessagelbl:nil];
    [self setMessageView:nil];
    [self setPostOfferBtn:nil];
    [self setSkipBtn:nil];
    [super viewDidUnload];
}

#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)skipBtnPressed:(id)sender {
    [self.navigationController dismissViewControllerAnimated:TRUE completion:nil];
}

- (IBAction)postOfferBtnPressed:(id)sender {
    [self.navigationController dismissViewControllerAnimated:TRUE completion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:IN_MERCHANT_SHOW_BROADCAST_NOTIFICATION object:nil];
}
@end
