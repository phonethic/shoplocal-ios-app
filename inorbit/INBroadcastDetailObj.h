//
//  INBroadcastDetailObj.h
//  shoplocal
//
//  Created by Sagar Mody on 12/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface INBroadcastDetailObj : NSObject
{}
@property (nonatomic, readwrite) NSInteger ID;
@property (nonatomic, readwrite) NSInteger place_ID;
@property (nonatomic, copy) NSString *areaID;

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *description;
@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *date;
@property (nonatomic, copy) NSString *offerdate;

@property (nonatomic, copy) NSString *is_offered;
@property (nonatomic, copy) NSString *verified;

@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *tags;

@property (nonatomic, copy) NSString *image_title1;
@property (nonatomic, copy) NSString *image_title2;
@property (nonatomic, copy) NSString *image_title3;
@property (nonatomic, copy) NSString *image_url1;
@property (nonatomic, copy) NSString *image_url2;
@property (nonatomic, copy) NSString *image_url3;
@property (nonatomic, copy) NSString *thumb_url1;
@property (nonatomic, copy) NSString *thumb_url2;
@property (nonatomic, copy) NSString *thumb_url3;


@property (nonatomic, copy) NSString *distance;
@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, copy) NSString *longitude;

@property (nonatomic, copy) NSString *mob_no1;
@property (nonatomic, copy) NSString *mob_no2;
@property (nonatomic, copy) NSString *mob_no3;
@property (nonatomic, copy) NSString *tel_no1;
@property (nonatomic, copy) NSString *tel_no2;
@property (nonatomic, copy) NSString *tel_no3;

@property (nonatomic, copy) NSString *user_like;

@property (nonatomic, copy) NSString *total_like;
@property (nonatomic, copy) NSString *total_share;
@property (nonatomic, copy) NSString *total_view;
@end
