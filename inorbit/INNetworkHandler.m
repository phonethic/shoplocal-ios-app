//
//  INNetworkHandler.m
//  inorbit
//
//  Created by Rishi on 28/07/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INNetworkHandler.h"

@interface INNetworkHandler() <NSURLConnectionDelegate>
{
    NSURLConnection *urlConnection;
    NSString *resultString;
    int responsehttpStatus;
    NSMutableURLRequest *request;
}

@end

@implementation INNetworkHandler
@synthesize responseData = _responseData;

#pragma mark- Init Method

-(id)init
{
    self = [super init];
    if(self)
    {
        self.method = @"GET";
        self.timeOut = 30;
    }
    return self;
    
}

-(void)doAsynchronousRequest:(NSString*)url
{

    resultString = nil;
    
    url = [url stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //  NSLog(@"\n url %@",url);
    
    NSURL *initURL=[[NSURL alloc] initWithString:url];
    request = [[NSMutableURLRequest alloc] initWithURL:initURL];
    
    if (_header && ![_header isKindOfClass:[NSNull class]]) {
        
        [_header enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop)
         {
             // if (!stop) {
             NSString *objStr=(NSString*)obj;
             NSString *keyStr=(NSString*)key;
             if (objStr!=nil && keyStr!=nil) {
                 
                 [request addValue:objStr forHTTPHeaderField:keyStr];
             }else{
                 
                 * stop=YES;
             }
         }
         ];
    }
    _method = [_method uppercaseString];
    [request setHTTPBody:_body];
    [request setHTTPMethod:_method];
    request.timeoutInterval = _timeOut;
    
    //   NSLog(@"\n request generated %@\n header %@",theRequest,self.header);
    
    urlConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];

    request=nil;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
    responsehttpStatus = httpResponse.statusCode;
    self.responseHeader = [httpResponse allHeaderFields];
    
    _responseData = [[NSMutableData alloc] init];
    //  NSLog(@"header %@",self.responseHeader);
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // NSLog(@"data %@",data);
    [_responseData appendData:data];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // Do what you need with data
    resultString = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
    
    NSLog(@"\nresponse begain \n status %d\n http header = %@\n\n body = %@\n response end\n",responsehttpStatus,self.responseHeader,resultString);
    if (responsehttpStatus==200 || responsehttpStatus==204)
    {

        [self.delegate responseSuccess:self ResponseData:resultString];
    }
    else
    {

        [self.delegate responseFail:self error:nil];
    }
    
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    //  NSLog(@"\n error code %d  error message = %@", error.code,error);
    
    [self.delegate responseFail:self error:error];
}
-(void)dealloc
{
    urlConnection = nil;
    //NSLog(@"Network view dealloc called");
}

@end
