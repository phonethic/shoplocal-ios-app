//
//  INOffersStreamViewController.h
//  shoplocal
//
//  Created by Rishi on 07/03/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "INPopTableController.h"
#import "FPPopoverController.h"
#import <CoreLocation/CoreLocation.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import "FaceBookShareViewController.h"
#import "INCustomerLoginViewController.h"
#import <StoreKit/StoreKit.h>

@class INFeedObj;
@interface INOffersStreamViewController : UIViewController <INPopTableControllerDelegate,UIScrollViewDelegate,CLLocationManagerDelegate,MFMailComposeViewControllerDelegate,UISearchBarDelegate,UIActionSheetDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,FacebookProtocolDelegate,INCustomerLoginViewControllerDelegate,SKStoreProductViewControllerDelegate>
{
    FPPopoverController *popover;
    BOOL pageControlUsed;
    CLLocationManager *locationManager;
    INFeedObj *feedObj;
    sqlite3 *shoplocalDB;
    BOOL isDragging;
    BOOL isLoading;
    BOOL isAdding;
    
    //    ScrollDirection scrollDirection;
    
    BOOL loadWithAnimation;
    
    double lastContentOffset;

}

@property (strong, nonatomic) IBOutlet UITableView *feedsTableView;
@property (strong, nonatomic) NSMutableArray *feedsArray;
@property (strong, nonatomic) NSMutableArray *tempfeedsArray;
@property (readwrite, nonatomic) NSInteger totalPage;
@property (readwrite, nonatomic) NSInteger totalRecord;
@property (readwrite, nonatomic) NSInteger currentPage;
@property (strong, nonatomic) IBOutlet UIView *countHeaderView;
@property (strong, nonatomic) IBOutlet UILabel *lblpullToRefresh;
@property (strong, nonatomic) IBOutlet UILabel *countlbl;
@property (nonatomic, retain) UIView *refreshHeaderView;
@property (nonatomic, retain) UILabel *refreshLabel;
@property (nonatomic, retain) UIImageView *refreshArrow;
@property (nonatomic, retain) UIActivityIndicatorView *refreshSpinner;

//*****Pull TO Refresh
@property (nonatomic, copy) NSString *textPull;
@property (nonatomic, copy) NSString *textRelease;
@property (nonatomic, copy) NSString *textLoading;

@property (nonatomic, readwrite) NSInteger currentSelectedRow;

@property (strong, nonatomic) NSMutableArray *telArray;
@property (readwrite, nonatomic) NSInteger selectedPostId;
@property (copy, nonatomic) NSString* selectedPlaceName;

@property (strong, nonatomic) IBOutlet UISearchBar *globalSearchBar;

@property (strong, nonatomic) IBOutlet UIView *searchView;
@property (strong, nonatomic) IBOutlet UITextField *searchTextField;


@property (strong, nonatomic) IBOutlet UIView *callModalViewBackView;
@property (strong, nonatomic) IBOutlet UIView *callModalView;
@property (strong, nonatomic) IBOutlet UILabel *lblcallModalHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblcallModalHeaderSeperator;
@property (strong, nonatomic) IBOutlet UITableView *callModalTableView;
@property (strong, nonatomic) IBOutlet UIButton *btnCancelcallModal;
- (IBAction)btnCancelcallModalPressed:(id)sender;

@end
