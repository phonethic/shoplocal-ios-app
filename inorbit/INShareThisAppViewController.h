//
//  INShareThisAppViewController.h
//  shoplocal
//
//  Created by Kirti Nikam on 07/01/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import "FaceBookShareViewController.h"

@interface INShareThisAppViewController : UIViewController <MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate,FacebookProtocolDelegate>
{
    NSMutableArray  *shareArray;
}

@property (strong, nonatomic) IBOutlet UIButton *fbBtn;
@property (strong, nonatomic) IBOutlet UILabel *lblfb;
@property (strong, nonatomic) IBOutlet UIButton *twitterBtn;
@property (strong, nonatomic) IBOutlet UILabel *lblTwitter;
@property (strong, nonatomic) IBOutlet UIButton *emailBtn;
@property (strong, nonatomic) IBOutlet UILabel *lblEmail;
@property (strong, nonatomic) IBOutlet UIButton *whtsAppBtn;
@property (strong, nonatomic) IBOutlet UILabel *lblwhtsApp;
@property (strong, nonatomic) IBOutlet UIButton *smsBtn;
@property (strong, nonatomic) IBOutlet UILabel *lblsms;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *lblCollection;

@property (strong, nonatomic) IBOutlet UITableView *shareTableView;

- (IBAction)fbBtnPressed:(id)sender;
- (IBAction)twitterBtnPressed:(id)sender;
- (IBAction)emailBtnPressed:(id)sender;
- (IBAction)whtsAppBtnPressed:(id)sender;
- (IBAction)smsBtnPressed:(id)sender;
@end
