//
//  INAreaObject.m
//  shoplocal
//
//  Created by Rishi on 21/10/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INAreaObject.h"

@implementation INAreaObject

@synthesize city;
@synthesize country;
@synthesize countrycode;
@synthesize areaid;
@synthesize iso_code;
@synthesize latitude;
@synthesize longitude;
@synthesize locality;
@synthesize pincode;
@synthesize published_status;
@synthesize shoplocalplaceid;
@synthesize state;
@synthesize sublocality;
@synthesize is_active;
@synthesize distance;
@synthesize merchant_count;
@synthesize slug;
@end
