//
//  INShoplocalImageViewController.m
//  shoplocal
//
//  Created by Rishi on 20/11/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INShoplocalImageViewController.h"
#import "constants.h"

#define SHOPLOCAL_GALLERY [NSString stringWithFormat:@"%@%@%@place_api/gallery",LIVE_SERVER,URL_PREFIX,API_VERSION]
#define THUMBNAIL_LINK(IMG_LINK) [NSString stringWithFormat:@"%@%@%@",LIVE_SERVER,URL_PREFIX,IMG_LINK]

@interface INShoplocalImageViewController ()
@property(strong) NSDictionary *splashJson;
@end

@implementation INShoplocalImageViewController
@synthesize galleryArray;
@synthesize gridView = _gridView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    galleryArray = [[NSMutableArray alloc] init];

    //Add gridView
    _gridView = [[VCGridView alloc] initWithFrame:CGRectMake(0, 10, self.view.frame.size.width, self.view.frame.size.height - 20)];
	_gridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	_gridView.delegate = self;
	_gridView.dataSource = self;
	[self.view addSubview:_gridView];
    
    
    [self sendStoreCategoryRequest];
}

-(void)sendStoreCategoryRequest
{

    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:SHOPLOCAL_GALLERY]];
    DebugLog(@"search=%@",urlRequest.URL);
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"%@",self.splashJson);
                                                        if(self.splashJson  != nil) {
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                [galleryArray removeAllObjects];
                                                                NSDictionary *objdict = [self.splashJson  objectForKey:@"data"];
                                                                NSArray *imgArray = [objdict  objectForKey:@"Gallery"];
                                                                for (int index=0; index<[imgArray count]; index++) {
                                                                    NSString *image = [imgArray  objectAtIndex:index];
                                                                    [galleryArray addObject:image];
                                                                    DebugLog(@"%@",image);
                                                                }
                                                                [_gridView reloadData];
                                                            } else {
                                                                if([self.splashJson  objectForKey:@"message"] != [NSNull null])
                                                                    [INUserDefaultOperations showAlert:[self.splashJson  objectForKey:@"message"]];
                                                            }
                                                        }
                                                        
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        [INUserDefaultOperations showAlert:[NSString stringWithFormat:@"%@",[error localizedDescription]]];
                                                    }];
    
    
    
    [operation start];
}

#pragma mark - VCGridViewDataSource

- (NSInteger)numberOfCellsInGridView:(VCGridView*)gridView
{
	return [galleryArray count];
}

- (VCGridViewCell *)gridView:(VCGridView *)gridView cellAtIndex:(NSInteger)index
{
    UIImageView *imageView;
    
	VCGridViewCell *cell = [gridView dequeueReusableCell];
	if (!cell) {
		cell = [[VCGridViewCell alloc] initWithFrame:CGRectZero];
		
		CGRect contentFrame = CGRectInset(cell.bounds, 0, 0);
        
		imageView = [[UIImageView alloc] initWithFrame:contentFrame];
		imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		imageView.contentMode = UIViewContentModeScaleToFill;
        imageView.tag = 111;
		imageView.image = [UIImage imageNamed:@"Add_Logo.png"];
		[cell.contentView addSubview:imageView];

	}
    if (index < [galleryArray count]) {
        imageView = (UIImageView *)[cell viewWithTag:111];
            NSString *image  = [galleryArray objectAtIndex:index];
            DebugLog(@"link-%@",THUMBNAIL_LINK(image));
            [imageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(image)] placeholderImage:[UIImage imageNamed:@"Add_Logo.png"]];
    }
    
	return cell;
}

- (BOOL)gridView:(VCGridView *)gridView canEditCellAtIndex:(NSInteger)index
{
	return YES;
}

#pragma mark - VCGridViewDelegate

- (void)gridView:(VCGridView*)gridView didSelectCellAtIndex:(NSInteger)index
{
    if([self.delegate respondsToSelector:@selector(saveImagePath:)])
    {
        NSString *image  = [galleryArray objectAtIndex:index];
        DebugLog(@"link-%@",THUMBNAIL_LINK(image));
        [self.delegate saveImagePath:image];
    }
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        self.delegate = nil;
    }];
    

}

- (CGSize)sizeForCellsInGridView:(VCGridView *)gridView
{
	return CGSizeMake(75.0f, 75.0f);
}

- (CGFloat)paddingForCellsInGridView:(VCGridView *)gridView
{
	return 20.0f;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
