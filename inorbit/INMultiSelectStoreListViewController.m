//
//  INMultiSelectStoreListViewController.m
//  shoplocal
//
//  Created by Rishi on 03/09/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INMultiSelectStoreListViewController.h"
#import "INBroadcastViewController.h"
#import "INAddUpdateStoreViewController.h"
#import "INStoreDetailObj.h"
#import "constants.h"
#import "CommonCallback.h"
#import "INAppDelegate.h"

#define MERCHANT_STORE_LIST [NSString stringWithFormat:@"%@%@%@place_api/places",LIVE_SERVER,URL_PREFIX,API_VERSION]
#define THUMBNAIL_LINK(IMG_LINK) [NSString stringWithFormat:@"%@%@%@",LIVE_SERVER,URL_PREFIX,IMG_LINK]

@interface INMultiSelectStoreListViewController ()
@property(strong) NSDictionary *splashJson;
@end

@implementation INMultiSelectStoreListViewController
@synthesize storelistTableView;
@synthesize storeArray;
@synthesize storeListType;
@synthesize doneBtn;
@synthesize textPull, textRelease, textLoading, refreshHeaderView, refreshLabel, refreshArrow, refreshSpinner;
@synthesize searchView,searchTextField,searchCancelBtn;
@synthesize searchZoomOutBtn;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self setupStrings];
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    if (isSearchOn) {
        [searchTextField becomeFirstResponder];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //[self addHUDView];
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                             style:UIBarButtonItemStylePlain
                                                            target:nil
                                                            action:nil];
    [[self navigationItem] setBackBarButtonItem:back];
    doneBtn.titleLabel.font = DEFAULT_FONT(16);
    storeArray = [[NSMutableArray alloc] init];
    storeSelectedRows = [[NSMutableArray alloc] init];
    [self addPullToRefreshHeader];
    
    [storelistTableView setBackgroundColor:[UIColor clearColor]];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(searchBarBtnClicked:)];
    
    [self setUI];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)
                                                 name:@"UIKeyboardWillShowNotification"
                                               object:nil];
    keyboardHeight = 216;
    
    [searchView setFrame:CGRectMake(0, -searchView.frame.size.height, searchView.frame.size.width, searchView.frame.size.height)];
    [storelistTableView setFrame:CGRectMake(0,0, storelistTableView.frame.size.width, self.view.frame.size.height - (CGRectGetHeight(doneBtn.frame)+10))];
    [doneBtn setFrame:CGRectMake(doneBtn.frame.origin.x,CGRectGetMaxY(storelistTableView.frame), doneBtn.frame.size.width, doneBtn.frame.size.height)];

    //[self sendMerchantStoreListRequest];
    [self readMyStoresFromDatabase];
    [storelistTableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidUnload {
    [self setStorelistTableView:nil];
    [self setDoneBtn:nil];
    [super viewDidUnload];
}

#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void) keyboardWillShow:(NSNotification *)note {
    if ([self.view window]) //means is visible
    {    //do something
        NSDictionary *userInfo = [note userInfo];
        CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        DebugLog(@"userInfo %@", userInfo);
        
        DebugLog(@"Keyboard Height: %f Width: %f", kbSize.height, kbSize.width);
        
        keyboardHeight = kbSize.height;
        if (isSearchOn) {
            [UIView animateWithDuration:0.3 animations:^{
                storelistTableView.frame = CGRectMake(0, CGRectGetMaxY(searchView.frame), storelistTableView.frame.size.width, self.view.frame.size.height-(CGRectGetMaxY(searchView.frame)+keyboardHeight));
            }];
        }
    }else{
        //return
    }
}

#pragma internal methods
-(void)setUI
{
    searchView.backgroundColor          =       DEFAULT_COLOR;
    searchTextField.font                =       DEFAULT_FONT(20.0);
    searchTextField.backgroundColor     =       [UIColor whiteColor];
    searchTextField.textColor           =       DEFAULT_COLOR;
    searchTextField.layer.cornerRadius  =       8.0;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        searchTextField.tintColor           = DEFAULT_COLOR;
    }
    searchTextField.autocorrectionType  = UITextAutocorrectionTypeYes;
    searchTextField.spellCheckingType   = UITextSpellCheckingTypeYes;
    searchCancelBtn.backgroundColor     =    [UIColor clearColor];
    searchCancelBtn.titleLabel.font     =    DEFAULT_FONT(15.0);
    
    searchZoomOutBtn.backgroundColor     =    [UIColor clearColor];
    searchZoomOutBtn.titleLabel.font     =    DEFAULT_FONT(17.0);
    [searchZoomOutBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    searchZoomOutBtn.hidden = YES;
}


-(void)addHUDView
{
    hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:hud];
	hud.dimBackground = NO;
    hud.labelText = @"Loading";
	hud.detailsLabelText = @"Please wait ...";
    [hud hide:NO];
}

-(void)removeHUDView
{
    [hud removeFromSuperview];
    hud = nil;
}

-(void)sendMerchantStoreListRequest
{
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:MERCHANT_STORE_LIST]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setValue:[INUserDefaultOperations getMerchantAuthId] forHTTPHeaderField:@"user_id"];
    [urlRequest setValue:[INUserDefaultOperations getMerchantAuthCode] forHTTPHeaderField:@"auth_id"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        //[hud hide:YES];
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"%@",self.splashJson);
                                                        if(self.splashJson  != nil) {
                                                            [storeArray removeAllObjects];
                                                            if(isLoading)
                                                            {
                                                                [self stopLoading];
                                                            }
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSArray* jsonArray = [self.splashJson  objectForKey:@"data"];
                                                                DebugLog(@"\n\n%@\n\n",jsonArray);
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *objdict = [jsonArray  objectAtIndex:index];
                                                                    DebugLog(@"\n\n%@, %d\n\n",objdict,[objdict count]);
                                                                    storeObj = [[INStoreDetailObj alloc] init];
                                                                    storeObj.ID = [[objdict objectForKey:@"id"] integerValue];
                                                                    storeObj.name = [[objdict objectForKey:@"name"] stringByReplacingOccurrencesOfString:@"\\" withString:@""];
                                                                    storeObj.place_parent = [[objdict objectForKey:@"place_parent"] integerValue];
                                                                    storeObj.place_status = [objdict objectForKey:@"place_status"];
                                                                    storeObj.published = [objdict objectForKey:@"published"];
                                                                    storeObj.description = [[objdict objectForKey:@"description"] stringByReplacingOccurrencesOfString:@"\\" withString:@""];
                                                                    storeObj.building = [objdict objectForKey:@"building"];
                                                                    storeObj.street = [objdict objectForKey:@"street"];
                                                                    storeObj.landmark = [objdict objectForKey:@"landmark"];
                                                                    storeObj.area = [objdict objectForKey:@"area"];
                                                                    storeObj.pincode = [objdict objectForKey:@"pincode"];
                                                                    storeObj.city = [objdict objectForKey:@"city"];
                                                                    storeObj.state = [objdict objectForKey:@"state"];
                                                                    storeObj.country = [objdict objectForKey:@"country"];
                                                                    storeObj.mob_no1 = [objdict objectForKey:@"mob_no1"];
                                                                    storeObj.mob_no2 = [objdict objectForKey:@"mob_no2"];
                                                                    storeObj.mob_no3 = [objdict objectForKey:@"mob_no3"];
                                                                    storeObj.tel_no1 = [objdict objectForKey:@"tel_no1"];
                                                                    storeObj.tel_no2 = [objdict objectForKey:@"tel_no2"];
                                                                    storeObj.tel_no3 = [objdict objectForKey:@"tel_no3"];
                                                                    storeObj.image_url = [objdict objectForKey:@"image_url"];
                                                                    storeObj.email = [objdict objectForKey:@"email"];
                                                                    storeObj.website = [objdict objectForKey:@"website"];
                                                                    storeObj.total_like = [objdict objectForKey:@"total_like"];
                                                                    storeObj.total_share = [objdict objectForKey:@"total_share"];
                                                                    storeObj.total_view = [objdict objectForKey:@"total_view"];
                                                                    storeObj.fb_url = [objdict objectForKey:@"facebook_url"];
                                                                    storeObj.twitter_url = [objdict objectForKey:@"twitter_url"];
                                                                    storeObj.latitude = [objdict objectForKey:@"latitude"];
                                                                    storeObj.longitude = [objdict objectForKey:@"longitude"];
                                                                    [storeArray addObject:storeObj];
                                                                    storeObj = nil;
                                                                }
                                                                [self deleteMyPlacesTable];
                                                                [self insertAllStoreListTable];
                                                                [storelistTableView reloadData];
                                                                [CommonCallback hideProgressHud];
                                                                isAdding = NO;
                                                                if([storeArray count] == 1) {
                                                                    DebugLog(@"Only 1 Store Added");
                                                                    [self performSelector:@selector(pushSingleStoreBroadcast) withObject:nil afterDelay:2.0];
                                                                } else {
                                                                    DebugLog(@"Multiple Store Added");
                                                                }
                                                            } else {
                                                                DebugLog(@"No records found.");
                                                                [CommonCallback hideProgressHud];
                                                                if(self.splashJson != nil && [[self.splashJson  objectForKey:@"success"] isEqualToString:@"false"]) {
                                                                    
                                                                    if([self.splashJson  objectForKey:@"code"] != nil &&  [[self.splashJson  objectForKey:@"code"] isEqualToString:MERCHANT_INVALID_USER_AUTH_CODE])
                                                                    {
                                                                        if([INUserDefaultOperations isMerchantLoggedIn]){
                                                                            [INUserDefaultOperations showAlert:MERCHANT_INVALID_USER_AUTH_MESSAGE];
                                                                            [INUserDefaultOperations clearMerchantDetails];
                                                                            [self.navigationController popToRootViewControllerAnimated:TRUE];
                                                                        }
                                                                    }else if([self.splashJson  objectForKey:@"code"] != nil &&  [[self.splashJson  objectForKey:@"code"] isEqualToString:MERCHANT_NO_RECORDS_FOUND])
                                                                    {
                                                                        [self performSelector:@selector(pushAddStoreBroadcast) withObject:nil afterDelay:2.0];
                                                                    } else {
                                                                        [INUserDefaultOperations showAlert:[self.splashJson  objectForKey:@"message"]];
                                                                    }
                                                                }
                                                            }
                                                        } else {
                                                            [CommonCallback hideProgressHud];
                                                            isAdding = NO;
                                                            if(isLoading)
                                                            {
                                                                [self stopLoading];
                                                            }
                                                        }
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                        [av show];
                                                        isAdding = NO;
                                                        if(isLoading)
                                                        {
                                                            [self stopLoading];
                                                        }
                                                        
                                                    }];
    
    
    
    [operation start];
}

-(void) readMyStoresFromDatabase {
    
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    // Init the expense Array
    if(storeArray == nil) {
        DebugLog(@"new  expense array created");
        storeArray = [[NSMutableArray alloc] init];
    } else {
        DebugLog(@"old  expense array used");
        [storeArray removeAllObjects];
    }
	// Open the database from the users filessytem
	if(sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"select * from MYPLACES"];
        DebugLog(@"query--->%@",querySQL);
        const char *sqlStatement = [querySQL UTF8String];
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(shoplocalDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				// Read the data from the result row
                //int count = sqlite3_data_count(compiledStatement);
                //DebugLog(@"count=====>>%d",count);
                
				NSInteger lplaceid = sqlite3_column_int(compiledStatement, 1);
				NSInteger lplace_parent = sqlite3_column_int(compiledStatement, 2);
				NSString *lname = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                NSString *ldescription = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
                NSString *lbuilding = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];
                NSString *lstreet = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 6)];
                NSString *llandmark = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 7)];
				NSString *larea = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 8)];
				NSString *lcity = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 9)];
                NSString *lmob_no1 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 10)];
                NSString *lmob_no2 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 11)];
                NSString *lmob_no3 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 12)];
                NSString *ltel_no1 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 13)];
                NSString *ltel_no2 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 14)];
                NSString *ltel_no3 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 15)];
                NSString *limage_url = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 16)];
                NSString *lemail = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 17)];
                NSString *lwebsite = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 18)];
                NSString *ltotal_like = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 19)];
                NSString *ltotal_share = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 20)];
                NSString *ltotal_view = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 21)];
                NSString *lplace_status = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 22)];
                NSString *lpublished = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 23)];
                NSString *lcountry = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 24)];
                NSString *lstate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 25)];
                NSString *lpincode = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 26)];
                NSString *lfb_url = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 27)];
                NSString *ltw_url = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 28)];
                NSString *llat = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 29)];
                NSString *llong = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 30)];
                
                
                
				INStoreDetailObj *tempstoreObj = [[INStoreDetailObj alloc] init];
                tempstoreObj.ID = lplaceid;
                tempstoreObj.place_parent = lplace_parent;
                tempstoreObj.name = lname;
                tempstoreObj.description = ldescription;
                tempstoreObj.building = lbuilding;
                tempstoreObj.street = lstreet;
                tempstoreObj.landmark = llandmark;
                tempstoreObj.area = larea;
                tempstoreObj.city = lcity;
                tempstoreObj.mob_no1 = lmob_no1;
                tempstoreObj.mob_no2 = lmob_no2;
                tempstoreObj.mob_no3 = lmob_no3;
                tempstoreObj.tel_no1 = ltel_no1;
                tempstoreObj.tel_no2 = ltel_no2;
                tempstoreObj.tel_no3 = ltel_no3;
                tempstoreObj.image_url = limage_url;
                tempstoreObj.email = lemail;
                tempstoreObj.website = lwebsite;
                tempstoreObj.total_like = ltotal_like;
                tempstoreObj.total_share = ltotal_share;
                tempstoreObj.total_view = ltotal_view;
                tempstoreObj.place_status = lplace_status;
                tempstoreObj.published = lpublished;
                tempstoreObj.country = lcountry;
                tempstoreObj.state = lstate;
                tempstoreObj.pincode = lpincode;
                tempstoreObj.fb_url = lfb_url;
                tempstoreObj.twitter_url = ltw_url;
                tempstoreObj.latitude = llat;
                tempstoreObj.longitude = llong;
                [storeArray addObject:tempstoreObj];
				tempstoreObj = nil;
            }
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
		
	}
	sqlite3_close(shoplocalDB);
}

#pragma Table Operations
- (void)insertAllStoreListTable
{
    DebugLog(@"%d",[storeArray count]);
    sqlite3_stmt    *statement;
    int lastrowid;
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        for (int index=0; index<[storeArray count] ; index++)
        {
            INStoreDetailObj *tempstoreObj = [storeArray objectAtIndex:index];
            NSString *insertSQL = [NSString stringWithFormat:
                                   @"INSERT or REPLACE INTO MYPLACES (PLACEID, PLACE_PARENT, NAME, DESCRIPTION, BUILDING, STREET, LANDMARK, AREA, CITY, MOB_NO1, MOB_NO2, MOB_NO3, TEL_NO1, TEL_NO2, TEL_NO3, IMG_URL, EMAIL, WEBSITE, TOTAL_LIKE , TOTAL_SHARE , TOTAL_VIEW , PLACE_STATUS , PUBLISHED , COUNTRY , STATE , PINCODE , FB_URL , TWITTER_URL , LATITUDE , LONGITUDE) VALUES (\"%d\",\"%d\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",tempstoreObj.ID,tempstoreObj.place_parent,tempstoreObj.name,tempstoreObj.description,tempstoreObj.building,tempstoreObj.street,tempstoreObj.landmark,tempstoreObj.area,tempstoreObj.city,tempstoreObj.mob_no1,tempstoreObj.mob_no2,tempstoreObj.mob_no3, tempstoreObj.tel_no1, tempstoreObj.tel_no2, tempstoreObj.tel_no3, tempstoreObj.image_url, tempstoreObj.email, tempstoreObj.website, tempstoreObj.total_like, tempstoreObj.total_share, tempstoreObj.total_view, tempstoreObj.place_status, tempstoreObj.published, tempstoreObj.country,tempstoreObj.state, tempstoreObj.pincode, tempstoreObj.fb_url, tempstoreObj.twitter_url, tempstoreObj.latitude, tempstoreObj.longitude];
            DebugLog(@"%@",insertSQL);
            const char *insert_stmt = [insertSQL UTF8String];
            sqlite3_prepare_v2(shoplocalDB, insert_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                lastrowid = sqlite3_last_insert_rowid(shoplocalDB);
                DebugLog(@"inserted id========%d", lastrowid);
            } else {
                DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
            }
            sqlite3_finalize(statement);
        }
    }
    sqlite3_close(shoplocalDB);
}

-(void)deleteMyPlacesTable
{
    sqlite3_stmt    *statement;
    
    DebugLog(@"path---->%@",[IN_APP_DELEGATE databasePath]);
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM MYPLACES"];
        DebugLog(@"delete---->%@",deleteSQL);
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(shoplocalDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"All rows deleted");
        } else {
            DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
}


#pragma mark Table view methods
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 105;
}
// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [storeArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //UILabel *lblTitle;
    UIView *topView;
    UILabel *lblTitle;
    UILabel *lbldescription;
    UIImageView *thumbImg;
    UIImageView *checkMarkImgView;

    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType      = UITableViewCellAccessoryNone;
        cell.selectionStyle     = UITableViewCellSelectionStyleNone;
        cell.backgroundColor    = [UIColor clearColor];

        // Create the top view
        topView = [[UIView alloc] initWithFrame:CGRectMake(0, 5, 320, 100)];
        topView.tag = 111;
        //topView = [CommonCallback setViewPropertiesWithRoundedCorner:topView];
        topView.backgroundColor =  [UIColor whiteColor];
        topView.clipsToBounds = YES;
        //[cell.imageView setImage:[UIImage imageNamed:@"table_placeholder.png"]];
        
        //Initialize Image View with tag 1.(Thumbnail Image)
        thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        thumbImg.tag = 1;
        thumbImg.contentMode = UIViewContentModeScaleToFill;
        [topView addSubview:thumbImg];
        
        
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame) + 5, 2 , topView.frame.size.width - (CGRectGetMaxX(thumbImg.frame) +20), 30)];
        lblTitle.tag = 2;
        lblTitle.numberOfLines = 2;
        lblTitle.shadowOffset  = CGSizeMake(0.0, 2.0);
        lblTitle.font = DEFAULT_BOLD_FONT(15);
        lblTitle.textAlignment = NSTextAlignmentLeft;
        lblTitle.textColor = BROWN_COLOR;
        lblTitle.backgroundColor =  [UIColor clearColor];
        lblTitle.highlightedTextColor = [UIColor whiteColor];
        [topView addSubview:lblTitle];
        
        lbldescription = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame) + 5, CGRectGetMaxY(lblTitle.frame), topView.frame.size.width - (CGRectGetMaxX(thumbImg.frame) +35), 65)];
        lbldescription.tag = 3;
        lbldescription.numberOfLines = 4;
        lbldescription.shadowOffset  = CGSizeMake(0.0, 2.0);
        lbldescription.font = DEFAULT_FONT(12);
        lbldescription.textAlignment = NSTextAlignmentLeft;
        lbldescription.textColor = BROWN_COLOR;
        lbldescription.backgroundColor =  [UIColor clearColor];
        lbldescription.highlightedTextColor = [UIColor whiteColor];
        [topView addSubview:lbldescription];
        
        checkMarkImgView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lbldescription.frame)+2,(CGRectGetHeight(topView.frame)/2)-5, 20, 20)];
        checkMarkImgView.tag = 102;
        checkMarkImgView.contentMode = UIViewContentModeScaleToFill;
        checkMarkImgView.layer.masksToBounds = YES;
        checkMarkImgView.image = [UIImage imageNamed:@"offer_uncheck.png"];
        [topView addSubview:checkMarkImgView];
        
        // Add views to contentView
        [cell.contentView addSubview:topView];
        
//        UIView *bgColorView = [[UIView alloc] init];
//        bgColorView.backgroundColor = BROWN_OFFWHITE_COLOR;
//        cell.selectedBackgroundView = bgColorView;
    }
    
    if ([indexPath row] < [storeArray count]) {
        INStoreDetailObj *placeObj  = [storeArray objectAtIndex:indexPath.row];
//        cell.textLabel.text = placeObj.name;
//        cell.detailTextLabel.text = placeObj.description;
//        cell.detailTextLabel.numberOfLines = 3;
//        cell.textLabel.font = DEFAULT_BOLD_FONT(15);
//        cell.textLabel.textColor = BROWN_COLOR;
//        cell.detailTextLabel.font = DEFAULT_FONT(12);
//        cell.detailTextLabel.textColor = BROWN_COLOR;
        topView = (UIView *)[cell viewWithTag:111];

        thumbImg = (UIImageView *)[topView viewWithTag:1];
        checkMarkImgView = (UIImageView *)[topView viewWithTag:102];

        __weak UIImageView *thumbImg_ = thumbImg;
//        [thumbImg setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(placeObj.image_url)]
//                 placeholderImage:[UIImage imageNamed:@"list_thumbnail.png"]
//                          success:^(UIImage *image) {
//                              //DebugLog(@"success");
//                          }
//                          failure:^(NSError *error) {
//                              //DebugLog(@"write error %@", error);
//                              thumbImg_.image = [UIImage imageNamed:@"list_thumbnail.png"];
//                          }];
        [thumbImg setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(placeObj.image_url)] placeholderImage:[UIImage imageNamed:@"list_thumbnail.png"] completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType) {
            if (error) {
                thumbImg_.image = [UIImage imageNamed:@"list_thumbnail.png"];
            }
        }];
        DebugLog(@"link-%@",THUMBNAIL_LINK(placeObj.image_url));
        
        lblTitle = (UILabel *)[topView viewWithTag:2];
        lblTitle.text = placeObj.name;
        lbldescription = (UILabel *)[topView viewWithTag:3];
        lbldescription.text = placeObj.description;

    }
    if([storeSelectedRows containsObject:indexPath]) {
        //cell.accessoryType = UITableViewCellAccessoryCheckmark;
        [checkMarkImgView setImage:[UIImage imageNamed:@"offer_check.png"]];
    }
    else {
       // cell.accessoryType = UITableViewCellAccessoryNone;
        [checkMarkImgView setImage:[UIImage imageNamed:@"offer_uncheck.png"]];
    }
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UIView *topView = (UIView *)[cell viewWithTag:111];
    UIImageView *checkMarkImgView = (UIImageView *)[topView viewWithTag:102];
    if([storeSelectedRows containsObject:indexPath]) {
        [checkMarkImgView setImage:[UIImage imageNamed:@"offer_uncheck.png"]];
        [storeSelectedRows removeObject:indexPath];
    }
    else {
        [checkMarkImgView setImage:[UIImage imageNamed:@"offer_check.png"]];
        [storeSelectedRows addObject:indexPath];
    }
    
//    if(cell.accessoryType == UITableViewCellAccessoryNone) {
//        cell.accessoryType = UITableViewCellAccessoryCheckmark;
//        [storeSelectedRows addObject:indexPath];
//    }
//    else {
//        cell.accessoryType = UITableViewCellAccessoryNone;
//        [storeSelectedRows removeObject:indexPath];
//    }
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
}


-(void)pushSingleStoreBroadcast
{
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    INStoreDetailObj *placeObj  = [storeArray objectAtIndex:0];
    NSString *placeid = [NSString stringWithFormat:@"%d",placeObj.ID];
    [tempArray addObject:placeid];
    DebugLog(@"placeid--->%@",placeid);

    INBroadcastViewController *broadcastController = [[INBroadcastViewController alloc] initWithNibName:@"INBroadcastViewController" bundle:nil];
    broadcastController.title = @"Talk to shoppers";
    broadcastController.placeidArray = tempArray;
    [self.navigationController pushViewController:broadcastController animated:YES];
    tempArray = nil;
}

-(void)pushAddStoreBroadcast
{
    INAddUpdateStoreViewController *addupdateController = [[INAddUpdateStoreViewController alloc] initWithNibName:@"INAddUpdateStoreViewController" bundle:nil];
    addupdateController.title = @"Add Store";
    [self.navigationController pushViewController:addupdateController animated:YES];
}

- (IBAction)searchZoomOutBtnPressed:(id)sender {
    if (searchView.frame.size.height < 72) {
        [UIView animateWithDuration:0.3 animations:^{
            storelistTableView.transform = CGAffineTransformIdentity;
            searchTextField.alpha = 1.0;
            searchCancelBtn.alpha = 1.0;
        } completion:^(BOOL finished){
            searchZoomOutBtn.hidden = YES;
            searchTextField.hidden = NO;
            searchCancelBtn.hidden = NO;
            [UIView animateWithDuration:0.2
                             animations:^(void){
                                 searchView.frame = CGRectMake(searchView.frame.origin.x, searchView.frame.origin.y, searchView.frame.size.width, searchView.frame.size.height*2);
                                 storelistTableView.frame = CGRectMake(storelistTableView.frame.origin.x, CGRectGetMaxY(searchView.frame), storelistTableView.frame.size.width, self.view.frame.size.height-(CGRectGetMaxY(searchView.frame)));
                             } completion:^(BOOL finished){
                             }];
        }];
    }
}

- (IBAction)doneBtnPressed:(id)sender {
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    for(NSIndexPath *indexPath in storeSelectedRows) {
         INStoreDetailObj *placeObj  = [storeArray objectAtIndex:indexPath.row];
         NSString *placeid = [NSString stringWithFormat:@"%d",placeObj.ID];
        [tempArray addObject:placeid];
         DebugLog(@"placeid--->%@",placeid);
    }
//    if (self.presentingViewController) {
//        //This is a modal viewContoller
//        DebugLog(@"This is a modal viewContoller");
//    } else {
//        //This is a normal ViewController
//        DebugLog(@"This is a normal ViewController");
//    }
     DebugLog(@"placeid--->%@",tempArray);
    if([tempArray count] == 0)
    {
        [INUserDefaultOperations showAlert:@"Please select at least one store for which you want to post a message or offer."];
    } else {
        INBroadcastViewController *broadcastController = [[INBroadcastViewController alloc] initWithNibName:@"INBroadcastViewController" bundle:nil];
        broadcastController.title = @"Talk to shoppers";
        broadcastController.placeidArray = tempArray;
        [self.navigationController pushViewController:broadcastController animated:YES];
        tempArray = nil;
    }

}

#pragma mark PULL TO REFRESH
- (void)setupStrings{
    textPull = @"Pull down to refresh...";
    textRelease = @"Release to refresh...";
    textLoading = @"Loading please wait...";
}

- (void)addPullToRefreshHeader {
    refreshHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0 - REFRESH_HEADER_HEIGHT, 320, REFRESH_HEADER_HEIGHT)];
    refreshHeaderView.backgroundColor = [UIColor clearColor];
    
    refreshLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, REFRESH_HEADER_HEIGHT)];
    refreshLabel.backgroundColor = [UIColor clearColor];
    refreshLabel.font = DEFAULT_BOLD_FONT(15);
    refreshLabel.textAlignment = NSTextAlignmentCenter;
    refreshLabel.textColor = DEFAULT_COLOR;
    refreshArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:PULLTOREFRESH_BARROW]];
    refreshArrow.frame = CGRectMake(floorf((REFRESH_HEADER_HEIGHT - 2) / 2),
                                    (floorf(REFRESH_HEADER_HEIGHT - 44) / 2),
                                    27, 44);
    
    refreshSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    refreshSpinner.frame = CGRectMake(floorf(floorf(REFRESH_HEADER_HEIGHT - 2) / 2), floorf((REFRESH_HEADER_HEIGHT - 20) / 2), 20, 20);
    refreshSpinner.hidesWhenStopped = YES;
    
    [refreshHeaderView addSubview:refreshLabel];
    [refreshHeaderView addSubview:refreshArrow];
    [refreshHeaderView addSubview:refreshSpinner];
    [storelistTableView addSubview:refreshHeaderView];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (isLoading) return;
    isDragging = YES;
    lastContentOffset = scrollView.contentOffset.y;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (isLoading) {
        // Update the content inset, good for section headers
        if (scrollView.contentOffset.y > 0)
            storelistTableView.contentInset = UIEdgeInsetsZero;
        else if (scrollView.contentOffset.y >= -REFRESH_HEADER_HEIGHT)
            storelistTableView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (isDragging && scrollView.contentOffset.y < 0) {
        // Update the arrow direction and label
        [UIView animateWithDuration:0.25 animations:^{
            if (scrollView.contentOffset.y < -REFRESH_HEADER_HEIGHT) {
                // User is scrolling above the header
                refreshLabel.text = self.textRelease;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
            } else {
                // User is scrolling somewhere within the header
                refreshLabel.text = self.textPull;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
            }
        }];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (isLoading) return;
    isDragging = NO;
    if (scrollView.contentOffset.y <= -REFRESH_HEADER_HEIGHT) {
        // Released above the header
        [self startLoading];
    }
    if (isSearchOn) {
        if (lastContentOffset < (int)scrollView.contentOffset.y) {
            DebugLog(@"lastContentOffset <");
            if (searchView.frame.size.height >= 72) {
                searchZoomOutBtn.hidden = NO;
                if ([searchTextField.text length] > 0) {
                    [searchZoomOutBtn setTitle:[NSString stringWithFormat:@"%@",searchTextField.text] forState:UIControlStateNormal];
                }else{
                    [searchZoomOutBtn setTitle:@"Search" forState:UIControlStateNormal];
                }
                [UIView animateWithDuration:0.2
                                 animations:^(void){
                                     searchView.frame = CGRectMake(searchView.frame.origin.x, searchView.frame.origin.y, searchView.frame.size.width, searchView.frame.size.height/2);
                                     storelistTableView.frame = CGRectMake(storelistTableView.frame.origin.x, CGRectGetMaxY(searchView.frame), storelistTableView.frame.size.width, self.view.frame.size.height-(CGRectGetMaxY(searchView.frame)+CGRectGetHeight(doneBtn.frame)+10));
                                     [doneBtn setFrame:CGRectMake(doneBtn.frame.origin.x,CGRectGetMaxY(storelistTableView.frame), doneBtn.frame.size.width, doneBtn.frame.size.height)];
                                     searchTextField.alpha = 0.0;
                                     searchCancelBtn.alpha = 0.0;
                                 } completion:^(BOOL finished){
                                     [UIView animateWithDuration:0.3 animations:^{
                                         searchZoomOutBtn.transform = CGAffineTransformMakeScale(0.8, 0.8);
                                     } completion:^(BOOL finished){
                                         searchTextField.hidden = YES;
                                         searchCancelBtn.hidden = YES;
                                     }];
                                 }];
            }
        }
        else if (lastContentOffset > (int)scrollView.contentOffset.y) {
            DebugLog(@"lastContentOffset >");
            if (searchView.frame.size.height < 72) {
                [UIView animateWithDuration:0.3 animations:^{
                    searchZoomOutBtn.transform = CGAffineTransformIdentity;
                    searchTextField.alpha = 1.0;
                    searchCancelBtn.alpha = 1.0;
                } completion:^(BOOL finished){
                    searchZoomOutBtn.hidden = YES;
                    searchTextField.hidden = NO;
                    searchCancelBtn.hidden = NO;
                    [UIView animateWithDuration:0.2
                                     animations:^(void){
                                         searchView.frame = CGRectMake(searchView.frame.origin.x, searchView.frame.origin.y, searchView.frame.size.width, searchView.frame.size.height*2);
                                         storelistTableView.frame = CGRectMake(storelistTableView.frame.origin.x, CGRectGetMaxY(searchView.frame), storelistTableView.frame.size.width, self.view.frame.size.height-(CGRectGetMaxY(searchView.frame)+CGRectGetHeight(doneBtn.frame)+10));
                                         [doneBtn setFrame:CGRectMake(doneBtn.frame.origin.x,CGRectGetMaxY(storelistTableView.frame), doneBtn.frame.size.width, doneBtn.frame.size.height)];
                                     } completion:^(BOOL finished){
                                     }];
                }];
            }
        }
    }
}

- (void)startLoading {
    isLoading = YES;
    // Show the header
    [UIView animateWithDuration:0.3 animations:^{
        storelistTableView.contentInset = UIEdgeInsetsMake(REFRESH_HEADER_HEIGHT, 0, 0, 0);
        refreshLabel.text = self.textLoading;
        refreshArrow.hidden = YES;
        [refreshSpinner startAnimating];
    }];
    if([IN_APP_DELEGATE networkavailable])
    {
        [storeArray removeAllObjects];
        [self sendMerchantStoreListRequest];
    }
    else
    {
        [self stopLoading];
    }
    // Refresh action!
    //[self refresh];
}

- (void)stopLoading {
    isLoading = NO;
    // Hide the header
    [UIView animateWithDuration:0.3 animations:^{
        storelistTableView.contentInset = UIEdgeInsetsZero;
        [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
    }
                     completion:^(BOOL finished) {
                         [self performSelector:@selector(stopLoadingComplete)];
                     }];
}

- (void)stopLoadingComplete {
    // Reset the header
    refreshLabel.text = self.textPull;
    refreshArrow.hidden = NO;
    [refreshSpinner stopAnimating];
}


- (void)searchBarBtnClicked:(id)sender {
    isSearchOn = YES;
    searchTextField.text = @"";
    self.navigationItem.rightBarButtonItem = nil;
    searchView.hidden = FALSE;
    
    [UIView animateWithDuration:0.3 animations:^{
        [searchView setFrame:CGRectMake(0, 0, searchView.frame.size.width, searchView.frame.size.height)];
        [storelistTableView setFrame:CGRectMake(0,CGRectGetMaxY(searchView.frame), storelistTableView.frame.size.width, self.view.frame.size.height - (searchView.frame.size.height + CGRectGetHeight(doneBtn.frame)+10))];
        [doneBtn setFrame:CGRectMake(doneBtn.frame.origin.x,CGRectGetMaxY(storelistTableView.frame), doneBtn.frame.size.width, doneBtn.frame.size.height)];
    }];
}

- (IBAction)searchCancelBtnPressed:(id)sender {
    sqlite3_close(shoplocalDB);
    isSearchOn = NO;
    searchTextField.text = @"";
    [searchTextField resignFirstResponder];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(searchBarBtnClicked:)];
    [UIView animateWithDuration:0.3 animations:^{
        [searchView setFrame:CGRectMake(0, -searchView.frame.size.height, searchView.frame.size.width, searchView.frame.size.height)];
        [storelistTableView setFrame:CGRectMake(0,0, storelistTableView.frame.size.width, self.view.frame.size.height - (CGRectGetHeight(doneBtn.frame)+10))];
        [doneBtn setFrame:CGRectMake(doneBtn.frame.origin.x,CGRectGetMaxY(storelistTableView.frame), doneBtn.frame.size.width, doneBtn.frame.size.height)];
    } completion:^(BOOL finished){
        searchView.hidden = TRUE;
    }];
    [self readMyStoresFromDatabase];
    [storelistTableView reloadData];
}

- (IBAction)searchTextChanged:(id)sender {
    [self readSearchTextInTable];
    [storelistTableView reloadData];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if ([textField isEqual:searchTextField]) {
        isSearchOn = YES;
        searchTextField.text = @"";
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == searchTextField) {
        DebugLog(@"Got searchButton");
        [self readSearchTextInTable];
        [searchTextField resignFirstResponder];
    }
    return YES;
}

-(void)readSearchTextInTable{
    DebugLog(@"search=%@",searchTextField.text);
    
    if(storeArray == nil) {
        storeArray = [[NSMutableArray alloc] init];
    } else {
        [storeArray removeAllObjects];
    }
    // Open the database from the users filessytem
    if(sqlite3_open([[IN_APP_DELEGATE databasePath] UTF8String], &shoplocalDB) == SQLITE_OK) {
        // Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"select * from MYPLACES where NAME like '%%%@%%' OR DESCRIPTION like '%%%@%%'",searchTextField.text,searchTextField.text];
        DebugLog(@"query--->%@",querySQL);
        const char *sqlStatement = [querySQL UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(shoplocalDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
            // Loop through the results and add them to the feeds array
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                // Read the data from the result row
                //int count = sqlite3_data_count(compiledStatement);
                //DebugLog(@"count=====>>%d",count);
                
                NSInteger lplaceid = sqlite3_column_int(compiledStatement, 1);
                NSInteger lplace_parent = sqlite3_column_int(compiledStatement, 2);
                NSString *lname = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                NSString *ldescription = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
                NSString *lbuilding = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];
                NSString *lstreet = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 6)];
                NSString *llandmark = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 7)];
                NSString *larea = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 8)];
                NSString *lcity = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 9)];
                NSString *lmob_no1 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 10)];
                NSString *lmob_no2 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 11)];
                NSString *lmob_no3 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 12)];
                NSString *ltel_no1 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 13)];
                NSString *ltel_no2 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 14)];
                NSString *ltel_no3 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 15)];
                NSString *limage_url = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 16)];
                NSString *lemail = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 17)];
                NSString *lwebsite = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 18)];
                NSString *ltotal_like = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 19)];
                NSString *ltotal_share = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 20)];
                NSString *ltotal_view = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 21)];
                NSString *lplace_status = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 22)];
                NSString *lpublished = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 23)];
                NSString *lcountry = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 24)];
                NSString *lstate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 25)];
                NSString *lpincode = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 26)];
                NSString *lfb_url = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 27)];
                NSString *ltw_url = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 28)];
                NSString *llat = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 29)];
                NSString *llong = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 30)];
                
                
                DebugLog(@"-%@-%@-",lname,ldescription);
                INStoreDetailObj *tempstoreObj = [[INStoreDetailObj alloc] init];
                tempstoreObj.ID = lplaceid;
                tempstoreObj.place_parent = lplace_parent;
                tempstoreObj.name = lname;
                tempstoreObj.description = ldescription;
                tempstoreObj.building = lbuilding;
                tempstoreObj.street = lstreet;
                tempstoreObj.landmark = llandmark;
                tempstoreObj.area = larea;
                tempstoreObj.city = lcity;
                tempstoreObj.mob_no1 = lmob_no1;
                tempstoreObj.mob_no2 = lmob_no2;
                tempstoreObj.mob_no3 = lmob_no3;
                tempstoreObj.tel_no1 = ltel_no1;
                tempstoreObj.tel_no2 = ltel_no2;
                tempstoreObj.tel_no3 = ltel_no3;
                tempstoreObj.image_url = limage_url;
                tempstoreObj.email = lemail;
                tempstoreObj.website = lwebsite;
                tempstoreObj.total_like = ltotal_like;
                tempstoreObj.total_share = ltotal_share;
                tempstoreObj.total_view = ltotal_view;
                tempstoreObj.place_status = lplace_status;
                tempstoreObj.published = lpublished;
                tempstoreObj.country = lcountry;
                tempstoreObj.state = lstate;
                tempstoreObj.pincode = lpincode;
                tempstoreObj.fb_url = lfb_url;
                tempstoreObj.twitter_url = ltw_url;
                tempstoreObj.latitude = llat;
                tempstoreObj.longitude = llong;
                [storeArray addObject:tempstoreObj];
                tempstoreObj = nil;
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement);
        
    }
    //sqlite3_close(shoplocalDB);
    [storelistTableView reloadData];
}

@end
