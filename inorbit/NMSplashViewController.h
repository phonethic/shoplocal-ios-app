//
//  NMSplashViewController.h
//  NissanMicra
//
//  Created by Rishi on 24/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTSlidingPagesDataSource.h"


@interface NMSplashViewController : UIViewController <TTSlidingPagesDataSource,NSXMLParserDelegate> {
    NSURLConnection *conn;
    NSMutableData *responseAsyncData;
    NSXMLParser *xmlParser;
    TTScrollSlidingPagesController *slider;
    
    
    BOOL TAP_DETECTED;
    int currentPageIndex;
    BOOL stopAnimation;
}
@property (copy,nonatomic) NSString *place_id;
@property (readwrite,nonatomic) int comeFrom;

@property (strong, nonatomic) IBOutlet UIButton *goBtn;
@property (strong, nonatomic) NSMutableArray *imagelinkArray;
@property (strong, nonatomic) NSMutableArray *titlelinkArray;
@property (strong, nonatomic) NSMutableArray *staticImageArray;

@property (strong, nonatomic) IBOutlet UIImageView *imageView1;
@property (strong, nonatomic) IBOutlet UIImageView *imageView2;
@property (nonatomic, strong) NSTimer *autoAnimationTimer;
@property (strong, nonatomic) IBOutlet UIButton *menuBtn;
@property (strong, nonatomic) IBOutlet UIImageView *menuHelpImgView;
@property (strong, nonatomic) IBOutlet UIWebView *webview1;
@property (strong, nonatomic) IBOutlet UIWebView *webview2;



- (IBAction)goBtnPressed:(id)sender;
- (IBAction)menuBtnPressed:(id)sender;


@end
