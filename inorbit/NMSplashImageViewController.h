//
//  NMSplashImageViewController.h
//  NissanMicra
//
//  Created by Rishi on 24/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NMSplashImageViewController : UIViewController
{
     UIWebView *videowebView;
}
@property (copy,nonatomic) NSString *imglink;
@property (copy,nonatomic) NSString *titlelink;
@property (strong, nonatomic) IBOutlet UIImageView *splashimageView;
@property (strong, nonatomic) IBOutlet UIWebView *splashWebView;
@end
