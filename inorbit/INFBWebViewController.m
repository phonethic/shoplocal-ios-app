//
//  INFBWebViewController.m
//  shoplocal
//
//  Created by Rishi on 25/09/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INFBWebViewController.h"
#import "CommonCallback.h"
#import "constants.h"
#import "INAppDelegate.h"

#define FB_LINK @"http://www.facebook.com/shoplocal.co.in"
#define TW_LINK @"https://twitter.com/myshoplocal"

@interface INFBWebViewController ()

@end

@implementation INFBWebViewController
@synthesize fbWebView;
@synthesize type;
@synthesize openURL;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:TRUE];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:TRUE];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:FALSE];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:FALSE];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    [self setupMenuBarButtonItems];
    if ([IN_APP_DELEGATE networkavailable]) {
        NSURLRequest *requestObj;
        if(type==1)
        {
            requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:FB_LINK]];
        } else if(type==2) {
            requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:TW_LINK]];
        } else {
             DebugLog(@"************------------>%@",openURL);
            if(openURL != nil && openURL != (NSString*)[NSNull null] && ![openURL isEqualToString:@""])
            {
                if ([openURL hasPrefix:@"http"]) {
                     DebugLog(@"************------------>%@",openURL);
                    requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:openURL]];
                } else {
                    requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",openURL]]];
                     DebugLog(@"************------------>%@",requestObj.URL);
                }
            }
        }
        //Load the request in the UIWebView.
        fbWebView.delegate = self;
        fbWebView.scalesPageToFit = TRUE;
        [fbWebView loadRequest:requestObj];
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

#pragma set Navigation barbuttons callback
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:MENU_TITLE style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
//    return [[UIBarButtonItem alloc]
//            initWithTitle:MENU_TITLE style:UIBarButtonItemStyleBordered
//            target:self.navigationController.sideMenu
//            action:@selector(toggleRightSideMenu)];
    return nil;
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Web View
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    DebugLog(@"webViewDidStartLoad");
//    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleFade];
    [MMProgressHUD showWithTitle:@"Loading"
                          status:@""
             confirmationMessage:@"Cancel?"
                     cancelBlock:^{
                         DebugLog(@"Task was cancelled!");
                         [fbWebView stopLoading];
                         [INUserDefaultOperations showSIAlertView:@"You have cancelled loading."];
                     }];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    DebugLog(@"webViewDidFinishLoad");
     [CommonCallback hideProgressHud];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    DebugLog(@"didFailLoadWithError : %@",error);
     [CommonCallback hideProgressHudWithError];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setFbWebView:nil];
    [super viewDidUnload];
}
#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
@end
