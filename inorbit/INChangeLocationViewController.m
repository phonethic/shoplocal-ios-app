//
//  INChangeLocationViewController.m
//  shoplocal
//
//  Created by Kirti Nikam on 06/11/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

//In this view, added logic as below:
//Steps: To set user location
//1) If user active area == "" then get user Latitude and Logitude values and call nearest API
    //1.1) Got result, then display Alert as "Set 'Area1' as your local area?" With "Choose From list" and "Yes" button.
    //1.2) If "Yes" then set 'Area1' as his active area
    //1.3) If "Choose From list" -> Show Area List -> User picks 'Area2' -> Done -> Again Show alert as "Set 'Area2' as your preferred local area?" With "No, Find my location" and "Yes" button.
    //1.4) If "Yes" then set 'Area1' as his active area
    //1.5) If "No, Find my location" -> Again follow from Step 1)
//2)else user active area has area then display Alert as "You have choosen 'Area2' as your preferred local area?" With "No, Find my location" and "Yes" button.
    //2.1) If "Yes" then set 'Area1' as his active area
    //2.2) If "No, Find my location" -> Again follow from Step 1)

#import "INChangeLocationViewController.h"
#import "INAppDelegate.h"
#import "ActionSheetPicker.h"
#import "INAreaObject.h"
#import "NMSplashViewController.h"
#import "TTUIScrollViewSlidingPages.h"
#import "TTSlidingPage.h"
#import "TTSlidingPageTitle.h"
#import "CommonCallback.h"
#import "SIAlertView.h"

#define MAP_STORE(LAT,LONG) [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/staticmap?center=%f,%f&zoom=16&size=300x170&maptype=roadmap&markers=color:blue|label:S|%f,%f&sensor=true&key=AIzaSyCvFovNWeEIPL4355q8L0Chd1urF8n8c0g",LAT,LONG,LAT,LONG]

#define UPDATE_CUSTOMER_PROFILE_POST_URL [NSString stringWithFormat:@"%@%@user_api/user",URL_PREFIX,API_VERSION]

#define GET_AREA_LIST(LATITUDE,LONGITUDE) [NSString stringWithFormat:@"%@%@%@place_api/nearest_areas?latitude=%f&longitude=%f",LIVE_SERVER,URL_PREFIX,API_VERSION,LATITUDE,LONGITUDE]

#define ALERT_ON_GOT_AREA_FROM_NEAREST_API(AREA_NAME) [NSString stringWithFormat:@"Set %@ as your local area?",AREA_NAME]
#define ALERT_ON_USER_PICKS_AREA_FROM_LIST(AREA_NAME) [NSString stringWithFormat:@"Set %@ as your preferred local area?",AREA_NAME]
#define ALERT_ON_USER_HAS_ACTIVE_AREA(AREA_NAME) [NSString stringWithFormat:@"Your current location is %@",AREA_NAME]

#define GOT_AREA_FROM_NEAREST_API @"GOT_AREA_FROM_NEAREST_API"
#define USER_PICKS_AREA_FROM_LIST @"USER_PICKS_AREA_FROM_LIST"
#define USER_HAS_ACTIVE_AREA @"USER_HAS_ACTIVE_AREA"

@interface INChangeLocationViewController ()
@property(strong) NSDictionary *splashJson;
@end

@implementation INChangeLocationViewController
@synthesize selectLocationBtn;
@synthesize arrowImageView;
@synthesize areaArray;
@synthesize comeFrom;
@synthesize goBtn;
@synthesize locationMapView;
@synthesize lblLocation;
@synthesize merchantcountlbl;
@synthesize selectedArea;
@synthesize previousArea;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:TRUE];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.locationMapView.layer removeAllAnimations];
    [self.selectLocationBtn.layer removeAllAnimations];
    [self.lblLocation.layer removeAllAnimations];
    [self.arrowImageView.layer removeAllAnimations];
    [self.goBtn.layer removeAllAnimations];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:FALSE];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    DebugLog(@"pre %@ now %@",previousArea,selectedArea);
    if ([previousArea isEqualToString:selectedArea]) {
        DebugLog(@"Same as previous");
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:IN_STORE_DID_CHANGE_NOTIFICATION object:nil];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.view bringSubviewToFront:selectLocationBtn];
    [self.view bringSubviewToFront:lblLocation];
    [self.view bringSubviewToFront:arrowImageView];
    [self.view bringSubviewToFront:goBtn];
    [self.view bringSubviewToFront:locationMapView];
    [self.view setNeedsDisplay];
    
    selectedArea = [IN_APP_DELEGATE getActivePlaceNameFromAreaTable];
    
    DebugLog(@"selectedArea %@",selectedArea);
    
    //Step: 1) If user active area == "" then get user Latitude and Logitude values and call nearest API
    //2)else useractivearea has area then display Alert as "You have choosen 'Area2' as your preferred local area?" With "No, Find my location" and "Yes" button.
    if (selectedArea != nil && ![selectedArea isEqualToString:@""]) {
        previousArea = [selectedArea copy];
        lblLocation.text = selectedArea;
        
        [self loadAreaListFromAreaTable];
//        [IN_APP_DELEGATE sendNetworkNotification];
        
        DebugLog(@"showAlert:USER_HAS_ACTIVE_AREA:  %@",selectedArea);
        [self showAlert:USER_HAS_ACTIVE_AREA];
    } else {
        [self performSelector:@selector(getLocationValues) withObject:nil afterDelay:2.0];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if(comeFrom == HOME)
    {
        [self setupMenuBarButtonItems];
    }
    
//    locationMapView.layer.cornerRadius = locationMapView.frame.size.width / 2.0 ;
//    locationMapView.clipsToBounds = YES;
    
   [self setUI];
    merchantcountlbl.hidden = TRUE;
    
    if ([INUserDefaultOperations getLatitude] != 0.0 && [INUserDefaultOperations getLongitude] != 0.0) {
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([INUserDefaultOperations getLatitude], [INUserDefaultOperations getLongitude] );
        MKCoordinateSpan span;
        span.latitudeDelta = 0.01;
        span.longitudeDelta = 0.01;
        MKCoordinateRegion region;
        region.center = coordinate;
        region.span = span;
        [locationMapView setRegion:region animated:YES];
        
        if ([[locationMapView annotations] count] > 0) {
            [locationMapView removeAnnotations:[locationMapView annotations]];
        }
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        point.coordinate = coordinate;
        point.title = @"You are here";
       // point.subtitle = @"I'm here!!!";
        [locationMapView addAnnotation:point];
    }
    
    if (comeFrom == SPLASH_SCREEN) {
        [[self navigationController] setNavigationBarHidden:YES animated:NO];
    }
    else{
        [[self navigationController] setNavigationBarHidden:NO animated:NO];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleareaRefreshNotification:) name:AREA_UPDATE_NOTIFICATION object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setSelectLocationBtn:nil];
    [self setArrowImageView:nil];
    [self setGoBtn:nil];
    [self setLblLocation:nil];
    [super viewDidUnload];
}

#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma internal methods
-(void)setUI{
    self.view.backgroundColor = [UIColor colorWithRed:161/255.0 green:195/255.0 blue:101/255.0 alpha:1.0];
    
    selectLocationBtn.layer.cornerRadius = 8.0;
    
    [selectLocationBtn setTitle:@"" forState:UIControlStateNormal];
    [selectLocationBtn setTitle:@"" forState:UIControlStateHighlighted];
    
    selectLocationBtn.backgroundColor = [UIColor colorWithRed:122/255.0 green:148/255.0 blue:77/255.0 alpha:1.0];
    
    lblLocation.font                        = DEFAULT_FONT(18);
    lblLocation.minimumScaleFactor          = 14;
    lblLocation.adjustsFontSizeToFitWidth   = YES;
    lblLocation.numberOfLines               = 0;
    lblLocation.textAlignment               = NSTextAlignmentCenter;
    lblLocation.textColor                   = [UIColor whiteColor];
    lblLocation.backgroundColor             = [UIColor clearColor];
    
    goBtn.titleLabel.font           = DEFAULT_FONT(18);
    goBtn.titleLabel.textAlignment  = NSTextAlignmentCenter;
    goBtn.backgroundColor           = [UIColor clearColor];
    
    merchantcountlbl.font                       = DEFAULT_FONT(15);
    merchantcountlbl.minimumScaleFactor         = 14;
    merchantcountlbl.adjustsFontSizeToFitWidth  = YES;
    merchantcountlbl.numberOfLines              = 0;
    merchantcountlbl.textAlignment              = NSTextAlignmentCenter;
    merchantcountlbl.textColor                  = [UIColor whiteColor];
    merchantcountlbl.backgroundColor             = [UIColor clearColor];
}

#pragma set Navigation barbuttons callback
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
           self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:MENU_TITLE style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithTitle:MENU_TITLE style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleRightSideMenu)];
    return nil;
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma NSNotification callback
-(void)handleareaRefreshNotification:(NSNotification *)notification
{
    DebugLog(@"handleareaRefreshNotification");
    [self loadAreaListFromAreaTable];
}

#pragma Location callback
-(void)getLocationValues
{
    locationNotFound = NO;
    isNearAreaRequestAlreadySent = NO;
    if ([CLLocationManager locationServicesEnabled])
    {
        DebugLog(@"Location services are enabled.");
        //        [self showProgressHUDWithMessage:@"Please wait ... \n shoplocal is trying to find your location."];
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        [locationManager startUpdatingLocation];
        [self performSelector:@selector(stopUpdatingCoreLocation:) withObject:@"Timed Out" afterDelay:15];
    }
    else
    {
        DebugLog(@"Location services not enabled.");
        UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Location serviecs are disabled on your device, shoplocal needs your location to bring you information from nearby, please enable location services." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [servicesDisabledAlert show];
    }
}

#pragma mark -
#pragma mark CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations
{
    [CommonCallback showProgressHud:@"Loading" subtitle:@""];
    CLLocation* newLocation = [locations lastObject];
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(newLocation.coordinate, 800, 800);
    [locationMapView setRegion:[locationMapView regionThatFits:region] animated:YES];
    DebugLog(@"newLocation :  accuracy %f currentLatitude %f currentLongitude %f",newLocation.horizontalAccuracy,newLocation.coordinate.latitude,newLocation.coordinate.longitude);
}

- (void) locationManager:(CLLocationManager *) manager didFailWithError:(NSError *) error
{
    DebugLog(@"Error: %@", [error description]);
	switch ([error code])
	{
		case kCLErrorDenied:
			//[errorString appendFormat:@"%@\n", NSLocalizedString(@"LocationDenied", nil)];
			break;
			
			
		case kCLErrorLocationUnknown:
			//[errorString appendFormat:@"%@\n", NSLocalizedString(@"LocationUnknown", nil)];
			break;
			
		case kCLErrorNetwork:
			//[errorString appendFormat:@"%@\n", NSLocalizedString(@"LocationUnknown", nil)];
			break;
            
		case kCLErrorHeadingFailure:
			//[errorString appendFormat:@"%@\n", NSLocalizedString(@"LocationUnknown", nil)];
			break;
			
		default:
			//[errorString appendFormat:@"%@ %d\n", NSLocalizedString(@"GenericLocationError", nil), [error code]];
			break;
	}
    [self stopUpdatingCoreLocation:nil];
    [CommonCallback hideProgressHud];
}

- (void)stopUpdatingCoreLocation:(NSString *)state
{
    //    [self hideProgressHUD:YES];
    [locationManager stopUpdatingLocation];
    [self saveCurrentLocation:[locationManager location]];
    // Add an annotation
    if ([[locationMapView annotations] count] > 0) {
        [locationMapView removeAnnotations:[locationMapView annotations]];
    }
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = [locationManager location].coordinate;
    point.title = @"You are here";
    //point.subtitle = @"I'm here!!!";
    [locationMapView addAnnotation:point];
}

/*
 - (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views {
 MKAnnotationView *aV;
 
 for (aV in views) {
 
 // Don't pin drop if annotation is user location
 if ([aV.annotation isKindOfClass:[MKUserLocation class]]) {
 continue;
 }
 
 // Check if current annotation is inside visible map rect, else go to next one
 MKMapPoint point =  MKMapPointForCoordinate(aV.annotation.coordinate);
 if (!MKMapRectContainsPoint(locationMapView.visibleMapRect, point)) {
 continue;
 }
 
 CGRect endFrame = aV.frame;
 
 // Move annotation out of view
 aV.frame = CGRectMake(aV.frame.origin.x, aV.frame.origin.y - self.view.frame.size.height, aV.frame.size.width, aV.frame.size.height);
 
 // Animate drop
 [UIView animateWithDuration:0.5 delay:0.04*[views indexOfObject:aV] options:UIViewAnimationCurveLinear animations:^{
 
 aV.frame = endFrame;
 
 // Animate squash
 }completion:^(BOOL finished){
 if (finished) {
 [UIView animateWithDuration:0.05 animations:^{
 aV.transform = CGAffineTransformMakeScale(1.0, 0.8);
 
 }completion:^(BOOL finished){
 [UIView animateWithDuration:0.1 animations:^{
 aV.transform = CGAffineTransformIdentity;
 }];
 }];
 }
 }];
 }
 }
 */

-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    }
    MKPinAnnotationView *pinView = nil;
    if (annotation != locationMapView.userLocation) {
        static NSString *defaultID = @"mapIdentifier";
        pinView = (MKPinAnnotationView *)[locationMapView dequeueReusableAnnotationViewWithIdentifier:defaultID];
        if (pinView == nil) {
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultID];
        }
        pinView.canShowCallout = YES;
      //  pinView.animatesDrop = YES;
        UIImage *pinImage = [UIImage imageNamed:@"PinGreen.png"];
        [pinView setImage:pinImage];
        pinView.annotation = annotation;
    }
	return pinView;
}

-(void)saveCurrentLocation:(CLLocation *)lLocation
{
    [self playAnimation];
    if (lLocation) {
        // Configure the new event with information from the location.
        DebugLog(@"%f --- %f",lLocation.coordinate.latitude,lLocation.coordinate.longitude);
        [INUserDefaultOperations setLatitude:lLocation.coordinate.latitude];
        [INUserDefaultOperations setLongitude:lLocation.coordinate.longitude];
        [INEventLogger logLocationValues:lLocation];
        MKCoordinateSpan span;
        span.latitudeDelta = 0.01;
        span.longitudeDelta = 0.01;
        MKCoordinateRegion region;
        region.center = lLocation.coordinate;
        region.span = span;
        [locationMapView setRegion:region animated:YES];
        DebugLog(@"%f --- %f",[INUserDefaultOperations getLatitude],[INUserDefaultOperations getLongitude]);
        [self sendGetAreaListWithDistanceRequest:lLocation.coordinate.latitude longitude:lLocation.coordinate.longitude];
    }else{
        if (locationNotFound) {
            return;
        }
        locationNotFound = YES;
        // If it's not possible to get a location, then return.
        if(self.isViewLoaded && self.view.window){
            UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                 message:@"Sorry we could not get your current location accurately at this point. Please change default location by tapping on location picker. "
                                                                delegate:nil
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
            [errorAlert show];
        }
        //        if (areaArray.count > 0) {
        //            NSString *subLocality = [areaArray objectAtIndex:0];
        //            selectedArea = subLocality;
        //            [IN_APP_DELEGATE updateAreasTableByName:subLocality is_active:1];
        //
        //            lblLocation.text = selectedArea;
        //            NSString *merchantCount = [INUserDefaultOperations getMerchantCountInArea:subLocality];
        //            if ([merchantCount intValue] > 0) {
        //                // lblLocation.text = [NSString stringWithFormat:@"%@ - %@",selectedArea,merchantCount];
        //                merchantcountlbl.text = [NSString stringWithFormat:@"Shoplocal Stores : %@",merchantCount];
        //                merchantcountlbl.hidden = FALSE;
        //            }else{
        //                // lblLocation.text = selectedArea;
        //                merchantcountlbl.hidden = TRUE;
        //            }
        //        }
        [self sendGetAreaListWithDistanceRequest:0 longitude:0];
    }
}

#pragma UIButton action callbacks
- (IBAction)goBtnPressed:(id)sender {
    if ([lblLocation.text isEqualToString:@"Choose your location"])
    {
        NSString *message = @"Hey, please select a location, then press Go.";
        [INUserDefaultOperations showSIAlertView:message];
    } else {
        if (comeFrom == SPLASH_SCREEN) {
            [IN_APP_DELEGATE animationStop];
        }
        else{
            [self dismissChangeLocationView];
        }
    }
}

- (IBAction)selectLocationBtnPressed:(id)sender {
    if(self.isViewLoaded && self.view.window){
        [self loadAreaListFromAreaTable];
        if (areaArray.count > 0) {
            [arrowImageView setHidden:YES];
            int initialIndex = 0;
    //        if (![selectLocationBtn.titleLabel.text isEqualToString:@"Click to select location"]) {
    //            for (int index = 0; index < areaArray.count ; index ++) {
    //                INAreaObject *areaObj = (INAreaObject *)[areaArray objectAtIndex:index];
    //                if ([areaObj.sublocality isEqualToString:selectLocationBtn.titleLabel.text]) {
    //                    initialIndex = index;
    //                    break;
    //                }
    //            }
    //        }
            if (![lblLocation.text isEqualToString:@"Choose your location"]) {
                if ([areaArray containsObject:selectedArea]) {
                    initialIndex = [areaArray indexOfObject:selectedArea];
                }else{
                    initialIndex = 0;
                    lblLocation.text = [areaArray objectAtIndex:0];
                }
            }
            [ActionSheetStringPicker showPickerWithTitle:@"Select location" rows:self.areaArray initialSelection:initialIndex target:self successAction:@selector(selectLocation:element:) cancelAction:@selector(actionPickerCancelled:) origin:sender];
        }
    }
}
#pragma mark - Actionsheet Implementation
- (void)selectLocation:(NSNumber *)lselectedIndex element:(id)element {
    [arrowImageView setHidden:NO];
//    INAreaObject *areaObj = (INAreaObject *)[areaArray objectAtIndex:[lselectedIndex intValue]];
//    if (areaObj.is_active) {
//        DebugLog(@"place already selected");
//    }else{
//        [selectLocationBtn setTitle:areaObj.sublocality forState: UIControlStateNormal];
//        [selectLocationBtn setTitle:areaObj.sublocality forState: UIControlStateHighlighted];
//        [IN_APP_DELEGATE updateAreasTable:areaObj.areaid is_active:1];
//    }
    NSString *subLocality = [areaArray objectAtIndex:[lselectedIndex intValue]];
    if ([lblLocation.text isEqualToString:subLocality])
    {
        DebugLog(@"Same as previous");
    }else{
//        [selectLocationBtn setTitle:subLocality forState: UIControlStateNormal];
//        [selectLocationBtn setTitle:subLocality forState: UIControlStateHighlighted];
        selectedArea = subLocality;
        lblLocation.text = selectedArea;
        DebugLog(@"selectedArea %@",selectedArea);

        NSString *merchantCount = [INUserDefaultOperations getMerchantCountInArea:subLocality];
        if ([merchantCount intValue] > 0) {
            // lblLocation.text = [NSString stringWithFormat:@"%@ - %@",selectedArea,merchantCount];
            merchantcountlbl.text = [NSString stringWithFormat:@"Shoplocal Stores : %@",merchantCount];
            merchantcountlbl.hidden = FALSE;
        }else{
            // lblLocation.text = selectedArea;
            merchantcountlbl.hidden = TRUE;
        }
        
        [IN_APP_DELEGATE updateAreasTableByName:subLocality is_active:1];
        
        NSDictionary *dictionary = [NSDictionary dictionaryWithObject:subLocality forKey:IN_CUSTOMER_CHANGE_LOCATION_KEY];
        [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_CHANGE_LOCATION_NOTIFICATION object:nil userInfo:dictionary];
        
        NSString *areaID = [IN_APP_DELEGATE getActiveShoplocalPlaceIdFromAreaTable];
        NSDictionary *areaParams = [NSDictionary dictionaryWithObjectsAndKeys:[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"Area_Name", areaID,@"Area_ID", nil];
        [INEventLogger logEvent:@"Location_Select" withParams:areaParams];
        
        NSArray *activearea_Array = [[NSArray alloc] initWithObjects:areaID, nil];
        [CommonCallback addTagsInUrbanAriship:activearea_Array name:PUSH_TAG_CUSTOMER_ACTIVE_AREA];
        
        if ([INUserDefaultOperations isCustomerLoggedIn]) {
            DebugLog(@"yes CustomerLoggedIn");
            [self sendUpdateActiveAreaRequest:areaID];
        }
    }
//    float latitude = [[IN_APP_DELEGATE getLatitudeFromAreaTable] floatValue];
//    float longitude = [[IN_APP_DELEGATE getLongitudeFromAreaTable] floatValue];
    
//    NSString *link = [MAP_STORE(latitude,longitude) stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    [mapImageView setImageWithURL:[NSURL URLWithString:link] placeholderImage:nil ];
    
//    if ([self navigationController].navigationBarHidden) {
//        [UIView animateWithDuration:0.3 animations:^{
//            [[self navigationController] setNavigationBarHidden:NO animated:NO];
//        }];
//    }
}

- (void)actionPickerCancelled:(id)sender {
    DebugLog(@"ActionSheetPicker was cancelled");
    [arrowImageView setHidden:NO];
//    if ([lblLocation.text isEqualToString:@"Choose your location"]) {
//        [INUserDefaultOperations setTourFinishedState:0];
//    }
}

#pragma Database callbacks
-(void)loadAreaListFromAreaTable
{
    DebugLog(@"loadAreaListFromAreaTable");
    if (areaArray == nil) {
        areaArray = [[NSMutableArray alloc] init];
    }else{
        [areaArray removeAllObjects];
    }
    sqlite3 *shoplocalDB;
	if (sqlite3_open([[IN_APP_DELEGATE databasePath] UTF8String], &shoplocalDB) == SQLITE_OK) {
        NSString *queryString = @"SELECT SUBLOCALITY FROM AREA where PUBLISHED=1";
        const char *sqlChar = [queryString UTF8String];
        sqlite3_stmt *statement = nil;
        if (sqlite3_prepare_v2(shoplocalDB, sqlChar, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                NSString *sub_locality  = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                [areaArray addObject:sub_locality];
                sub_locality = nil;
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(shoplocalDB);
    }
    DebugLog(@"%d",[areaArray count]);
}


//-(void)loadAreaListFromAreaTable
//{
//    DebugLog(@"loadAreaListFromAreaTable");
//    if (areaArray == nil) {
//        areaArray = [[NSMutableArray alloc] init];
//    }else{
//        [areaArray removeAllObjects];
//    }
//    sqlite3 *shoplocalDB;
//	if (sqlite3_open([[IN_APP_DELEGATE databasePath] UTF8String], &shoplocalDB) == SQLITE_OK) {
//        NSString *queryString = @"SELECT CITY , COUNTRY , COUNTRY_CODE , ID , ISO_CODE , LATITUDE , LONGITUDE , LOCALITY , PINCODE , PLACE_ID , STATE , SUBLOCALITY,IS_ACTIVE FROM AREA";
//        const char *sqlChar = [queryString UTF8String];
//        sqlite3_stmt *statement = nil;
//        if (sqlite3_prepare_v2(shoplocalDB, sqlChar, -1, &statement, NULL) == SQLITE_OK) {
//            while (sqlite3_step(statement) == SQLITE_ROW) {
//                
//				NSString *city          = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
//                NSString *country       = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
//				NSString *country_code  = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
//				NSString *area_id       = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
//				NSString *iso_code      = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 4)];
//				NSString *latitude      = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 5)];
//				NSString *longitude     = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 6)];
//				NSString *locality      = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 7)];
//				NSString *pincode       = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 8)];
//				NSString *place_id      = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 9)];
//				NSString *state         = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 10)];
//                NSString *sub_locality  = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 11)];
//                int is_Active  = sqlite3_column_int(statement, 12);
//                
//                
//				INAreaObject *areaObj = [[INAreaObject alloc] init];
//                areaObj.areaid = area_id;
//                areaObj.sublocality = sub_locality;
//                areaObj.locality = locality;
//                
//                areaObj.latitude = latitude;
//                areaObj.longitude = longitude;
//                
//                areaObj.pincode = pincode;
//                areaObj.city = city;
//                areaObj.state = state;
//                areaObj.country = country;
//                areaObj.countrycode = country_code;
//                
//                areaObj.iso_code = iso_code;
//                areaObj.placeid = place_id;
//                areaObj.is_active = is_Active;
//                [areaArray addObject:areaObj];
//                areaObj = nil;
//            }
//            sqlite3_finalize(statement);
//        }
//        sqlite3_close(shoplocalDB);
//    }
//}

#pragma NEw Change Location Logic callbacks
-(void)showAlert:(NSString *)type
{
     if(self.isViewLoaded && self.view.window){
        SIAlertView *alertView = [[SIAlertView alloc] init];
        alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
        alertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
        alertView.title = ALERT_TITLE;
        
        if ([type isEqualToString:GOT_AREA_FROM_NEAREST_API]){
            alertView.message = ALERT_ON_GOT_AREA_FROM_NEAREST_API(selectedArea);
            [alertView addButtonWithTitle:@"Choose from list"
                                     type:SIAlertViewButtonTypeCancel
                                  handler:^(SIAlertView *alert) {
                                      DebugLog(@"'Choose from list' Clicked");
                                      [self chooseAreaFromList];
                                  }];
            
            [alertView addButtonWithTitle:@"Yes"
                                     type:SIAlertViewButtonTypeDestructive
                                  handler:^(SIAlertView *alert) {
                                      DebugLog(@"'Yes' Clicked");
                                      [self setUserActiveArea];
                                      [self performSelector:@selector(goBtnPressed:) withObject:nil afterDelay:3.0];
                                  }];
        }else{
            [alertView addButtonWithTitle:@"Find me"
                                     type:SIAlertViewButtonTypeCancel
                                  handler:^(SIAlertView *alert) {
                                      DebugLog(@"'Find me' Clicked");
                                      [self getLocationValues];
                                  }];
            
            [alertView addButtonWithTitle:@"Choose from list"
                                     type:SIAlertViewButtonTypeCancel
                                  handler:^(SIAlertView *alert) {
                                      DebugLog(@"'Choose from list' Clicked");
                                      if (onetimeAnimation) {
                                          [self chooseAreaFromList];
                                      }else{
                                          [self playAnimation];
                                          [self performSelector:@selector(chooseAreaFromList) withObject:nil afterDelay:2.9];
                                      }
                                  }];
            
            if ([type isEqualToString:USER_PICKS_AREA_FROM_LIST]){
                alertView.message = ALERT_ON_USER_PICKS_AREA_FROM_LIST(selectedArea);
                [alertView addButtonWithTitle:@"Ok"
                                         type:SIAlertViewButtonTypeDestructive
                                      handler:^(SIAlertView *alert) {
                                          DebugLog(@"'Ok' Clicked");
                                          [self setUserActiveArea];
                                      }];
            }
            else if ([type isEqualToString:USER_HAS_ACTIVE_AREA]){
                    alertView.message = ALERT_ON_USER_HAS_ACTIVE_AREA(selectedArea);
                    [alertView addButtonWithTitle:@"Cancel"
                                         type:SIAlertViewButtonTypeCancel
                                      handler:^(SIAlertView *alert) {
                                          DebugLog(@"'cancel' Clicked");
                                          //Dismiss
                                          [self dismissChangeLocationView];
                                      }];
            }
        }
        [alertView show];
     }
}

-(void)dismissChangeLocationView{
    [self.navigationController dismissViewControllerAnimated:YES completion:^(void){
//        DebugLog(@"pre %@ now %@",previousArea,selectedArea);
//        if ([previousArea isEqualToString:selectedArea]) {
//            DebugLog(@"Same as previous");
//        }else{
//            [[NSNotificationCenter defaultCenter] postNotificationName:IN_STORE_DID_CHANGE_NOTIFICATION object:nil];
//        }
    }];
}

-(void)chooseAreaFromList{
    if(self.isViewLoaded && self.view.window){
        if (areaArray.count > 0) {
            [arrowImageView setHidden:YES];
            int initialIndex = 0;
            if (![lblLocation.text isEqualToString:@"Choose your location"]) {
                if ([areaArray containsObject:selectedArea]) {
                    initialIndex = [areaArray indexOfObject:selectedArea];
                }else{
                    initialIndex = 0;
                    lblLocation.text = [areaArray objectAtIndex:0];
                }
            }
            [ActionSheetStringPicker showPickerWithTitle:@"Select location" rows:self.areaArray initialSelection:initialIndex target:self successAction:@selector(selectArea:element:) cancelAction:@selector(actionPickerCancelled:) origin:selectLocationBtn];
        }
    }
}

#pragma mark - Actionsheet Implementation
- (void)selectArea:(NSNumber *)lselectedIndex element:(id)element {
    [arrowImageView setHidden:NO];
    NSString *subLocality = [areaArray objectAtIndex:[lselectedIndex intValue]];
    if ([lblLocation.text isEqualToString:subLocality])
    {
        DebugLog(@"Same as previous");
    }else{
        selectedArea = subLocality;
    }
    DebugLog(@"showAlert:USER_PICKS_AREA_FROM_LIST:  %@",selectedArea);
    [self showAlert:USER_PICKS_AREA_FROM_LIST];
}


-(void)setUserActiveArea{
        [self playAnimation];
        // set this activeArea in button text
        lblLocation.text = selectedArea;
        
        // Get activeArea merchant count number from database and set it to merchant count label
        NSString *merchantCount = [INUserDefaultOperations getMerchantCountInArea:selectedArea];
        if ([merchantCount intValue] > 0) {
            merchantcountlbl.text = [NSString stringWithFormat:@"Shoplocal Stores : %@",merchantCount];
            merchantcountlbl.hidden = FALSE;
        }else{
            merchantcountlbl.hidden = TRUE;
        }
        
        // Update activeArea in database
        [IN_APP_DELEGATE updateAreasTableByName:selectedArea is_active:1];
        
        // send broadcast to set activeArea in Side Menu, Top nav bar
        NSDictionary *dictionary = [NSDictionary dictionaryWithObject:selectedArea forKey:IN_CUSTOMER_CHANGE_LOCATION_KEY];
        [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_CHANGE_LOCATION_NOTIFICATION object:nil userInfo:dictionary];
        
        // Get activeArea id from database
        NSString *areaID = [IN_APP_DELEGATE getActiveShoplocalPlaceIdFromAreaTable];
        
        // Add this activeArea in Analytics events
        NSDictionary *areaParams = [NSDictionary dictionaryWithObjectsAndKeys:[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"Area_Name", areaID,@"Area_ID", nil];
        [INEventLogger logEvent:@"Location_Select" withParams:areaParams];
        
        // Add this activeArea in push notification urban airship as tag
        NSArray *activearea_Array = [[NSArray alloc] initWithObjects:areaID, nil];
        [CommonCallback addTagsInUrbanAriship:activearea_Array name:PUSH_TAG_CUSTOMER_ACTIVE_AREA];
    

        // If user is Logged in, update his profile with his activeArea
        if ([INUserDefaultOperations isCustomerLoggedIn]) {
            DebugLog(@"yes CustomerLoggedIn");
            [self sendUpdateActiveAreaRequest:areaID];
        }
}


-(void)playAnimation{
    if (!onetimeAnimation) {
        onetimeAnimation = YES;
        isPlayingAnimation = YES;
        lblLocation.hidden = YES;
        merchantcountlbl.hidden = YES;
        arrowImageView.hidden = YES;
        
        selectLocationBtn.userInteractionEnabled = NO;
        [UIView animateWithDuration:1
                animations:^(void){
                    DebugLog(@"animation 1");
                             locationMapView.frame = CGRectMake(locationMapView.frame.origin.x, locationMapView.frame.origin.y, locationMapView.frame.size.width, (self.view.frame.size.height/2)-30);
                }completion:^(BOOL finished){
                   [UIView animateWithDuration:0.7
                           animations:^(void){
                               DebugLog(@"animation 2");
                            locationMapView.frame = CGRectMake(locationMapView.frame.origin.x, locationMapView.frame.origin.y, locationMapView.frame.size.width, self.view.frame.size.height/2);
                            selectLocationBtn.frame= CGRectMake(selectLocationBtn.frame.origin.x, CGRectGetMaxY(locationMapView.frame)+40, selectLocationBtn.frame.size.width, selectLocationBtn.frame.size.height);
                            goBtn.frame= CGRectMake(goBtn.frame.origin.x, CGRectGetMaxY(selectLocationBtn.frame)+CGRectGetHeight(merchantcountlbl.frame)+20, goBtn.frame.size.width, goBtn.frame.size.height);
                            }completion:^(BOOL finished){
                                  [UIView animateWithDuration:0.4
                                           animations:^(void){
                                               DebugLog(@"animation 3");
                                                selectLocationBtn.frame= CGRectMake(selectLocationBtn.frame.origin.x, CGRectGetMaxY(locationMapView.frame)+30, selectLocationBtn.frame.size.width, selectLocationBtn.frame.size.height);
                                                 goBtn.frame= CGRectMake(goBtn.frame.origin.x, CGRectGetMaxY(selectLocationBtn.frame)+CGRectGetHeight(merchantcountlbl.frame)+10, goBtn.frame.size.width, goBtn.frame.size.height);
                                           }completion:^(BOOL finished){
                                               DebugLog(@"animation 4");
                                                   lblLocation.frame= CGRectMake(lblLocation.frame.origin.x, selectLocationBtn.frame.origin.y+2, lblLocation.frame.size.width,lblLocation.frame.size.height);
                                                   arrowImageView.frame= CGRectMake(arrowImageView.frame.origin.x, selectLocationBtn.frame.origin.y+24, arrowImageView.frame.size.width,arrowImageView.frame.size.height);
                                                   merchantcountlbl.frame= CGRectMake(merchantcountlbl.frame.origin.x, CGRectGetMaxY(selectLocationBtn.frame)+5, merchantcountlbl.frame.size.width,merchantcountlbl.frame.size.height);
                                                   [UIView  transitionWithView:lblLocation duration:0.8  options:UIViewAnimationOptionTransitionFlipFromTop
                                                                                        animations:^(void) {
                                                                                            lblLocation.hidden = FALSE;
                                                                                        } completion:nil];
                                                   [UIView  transitionWithView:arrowImageView duration:0.8  options:UIViewAnimationOptionTransitionFlipFromTop
                                                                                        animations:^(void) {
                                                                                            arrowImageView.hidden = FALSE;
                                                                                        } completion:nil];
                                                    [UIView  transitionWithView:merchantcountlbl duration:0.8  options:UIViewAnimationOptionTransitionFlipFromTop
                                                                                        animations:^(void) {
                                                                                            merchantcountlbl.hidden = FALSE;
                                                                                        } completion:^(BOOL finished){
                                                                                    selectLocationBtn.userInteractionEnabled = YES;
                                                                                            isPlayingAnimation = NO;
                                                                                        }];
                                  }];
                   }];
        }];
    }
}

#pragma AFNetworking callbacks
-(void)sendUpdateActiveAreaRequest:(NSString *)activeArea
{
    DebugLog(@"========================sendUpdateActiveAreaRequest========================");
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"X-HTTP-Method-Override" value:@"PUT"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    NSMutableDictionary *postparams = [[NSMutableDictionary alloc] init];
    [postparams setObject:activeArea forKey:@"active_area"];
    [postparams setObject:[INUserDefaultOperations getCustomerAuthId] forKey:@"user_id"];
    [postparams setObject:[INUserDefaultOperations getCustomerAuthCode] forKey:@"auth_id"];
    DebugLog(@"postParams -%@-",postparams);
    
    [httpClient postPath:UPDATE_CUSTOMER_PROFILE_POST_URL parameters:postparams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json afnetworking ->%@",json);
        if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"true"])
        {
//            [INUserDefaultOperations showAlert:@"Your profile has been updated successfully."];
        } else {
            if([json  objectForKey:@"code"] != [NSNull null] && [[json  objectForKey:@"code"] isEqualToString:CUSTOMER_INVALID_USER_AUTH_CODE]) {
                DebugLog(@"Got Invalid Authentication Code.");
//                [INUserDefaultOperations showSIAlertView:CUSTOMER_INVALID_USER_AUTH_MESSAGE];
                [INUserDefaultOperations clearCustomerDetails];
                NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:IN_CUSTOMER_LOGIN_LOGOUT_KEY];
                [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_LOGIN_LOGOUT_NOTIFICATION object:nil userInfo:dictionary];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
    }];
}

-(void)sendGetAreaListWithDistanceRequest:(double)llatitude longitude:(double)llongitude
{
    DebugLog(@"========================sendGetAreaListWithDistanceRequest========================");
 
    if (![IN_APP_DELEGATE networkavailable]) {
        DebugLog(@"No network case");
        [self loadAreaListFromAreaTable];
        [self playAnimation];
        return;
    }
    
    if (isNearAreaRequestAlreadySent) {
        return;
    }
    isNearAreaRequestAlreadySent = YES;
    DebugLog(@"1) sendGetAreaListWithDistanceRequest");

    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:GET_AREA_LIST(llatitude,llongitude)]];
    DebugLog(@"URL %@",urlRequest.URL);

    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"%@",self.splashJson);
                                                        isNearAreaRequestAlreadySent = NO;
                                                        if(self.splashJson  != nil)
                                                        {
                                                            DebugLog(@"--->%@",[self.splashJson  objectForKey:@"success"]);
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSMutableArray *larray;
                                                                if (larray == nil) {
                                                                    larray = [[NSMutableArray alloc] init];
                                                                }else{
                                                                    [larray removeAllObjects];
                                                                }
                                                                if (areaArray == nil) {
                                                                    areaArray = [[NSMutableArray alloc] init];
                                                                }else{
                                                                    [areaArray removeAllObjects];
                                                                }
                                                                NSMutableDictionary *merchantCountDict = [[NSMutableDictionary alloc] init];
                                                                NSDictionary *maindict = [self.splashJson  objectForKey:@"data"];
                                                                NSArray *lareaArray = [maindict objectForKey:@"areas"];
                                                                for (NSDictionary *objdict in lareaArray) {
                                                                    INAreaObject *areaObj = [[INAreaObject alloc] init];
                                                                    areaObj.city = [objdict objectForKey:@"city"];
                                                                    areaObj.country = [objdict objectForKey:@"country"];
                                                                    areaObj.countrycode = [objdict objectForKey:@"country_code"];
                                                                    areaObj.distance = [objdict objectForKey:@"distance"];
                                                                    areaObj.areaid = [objdict objectForKey:@"id"];
                                                                    areaObj.iso_code = [objdict objectForKey:@"iso_code"];
                                                                    areaObj.latitude = [objdict objectForKey:@"latitude"];
                                                                    areaObj.longitude = [objdict objectForKey:@"longitude"];
                                                                    areaObj.locality = [objdict objectForKey:@"locality"];
                                                                    areaObj.pincode = [objdict objectForKey:@"pincode"];
                                                                    areaObj.published_status = [objdict objectForKey:@"published_status"];
                                                                    areaObj.shoplocalplaceid = [objdict objectForKey:@"shoplocal_place_id"];
                                                                    areaObj.state = [objdict objectForKey:@"state"];
                                                                    NSString *subLocality = [objdict objectForKey:@"sublocality"];
                                                                    areaObj.sublocality = subLocality;
                                                                    areaObj.merchant_count = [objdict objectForKey:@"merchant_count"];
                                                                    areaObj.slug = [objdict objectForKey:@"slug"];

                                                                    [larray addObject:areaObj];
                                                                    areaObj = nil;
                                                                    
                                                                    if ([objdict objectForKey:@"merchant_count"] != (NSString *)[NSNull null]) {
                                                                        [merchantCountDict setObject:[objdict objectForKey:@"merchant_count"] forKey:subLocality];
                                                                    }
                                                                    DebugLog(@"published_status %d",[[objdict objectForKey:@"published_status"] intValue]);
                                                                    if ([[objdict objectForKey:@"published_status"] intValue]) {
                                                                        [areaArray addObject:subLocality];
                                                                    }
                                                                }
                                                                
                                                                if (larray.count  > 0) {
                                                                    NSString *tempAreaId = [IN_APP_DELEGATE getActiveAreaIdFromAreaTable];
                                                                    [IN_APP_DELEGATE deleteAreasTable];
                                                                    [IN_APP_DELEGATE insertAreaTable:larray];
                                                                    [larray removeAllObjects];
                                                                    larray = nil;
                                                                    
                                                                    [IN_APP_DELEGATE updateAreasTable:tempAreaId is_active:1];
                                                                    
                                                                    DebugLog(@"MerchantCount %@",merchantCountDict);
                                                                    [INUserDefaultOperations setMerchantCountInArea:merchantCountDict];
                                                                    
                                                                    if ([areaArray count] > 0) {
                                                                        NSString *subLocality = [areaArray objectAtIndex:0];
                                                                        selectedArea = subLocality;
                                                                    }
//                                                                    [IN_APP_DELEGATE updateAreasTableByName:subLocality is_active:1];
//                                                                    
//                                                                    lblLocation.text = selectedArea;
//                                                                    NSString *merchantCount = [INUserDefaultOperations getMerchantCountInArea:subLocality];
//                                                                    if ([merchantCount intValue] > 0) {
//                                                                        // lblLocation.text = [NSString stringWithFormat:@"%@ - %@",selectedArea,merchantCount];
//                                                                        merchantcountlbl.text = [NSString stringWithFormat:@"Shoplocal Stores : %@",merchantCount];
//                                                                        merchantcountlbl.hidden = FALSE;
//                                                                    }else{
//                                                                        // lblLocation.text = selectedArea;
//                                                                        merchantcountlbl.hidden = TRUE;
//                                                                    }
                                                                    DebugLog(@"areaArray:  %@",areaArray);
                                                                    DebugLog(@"showAlert:GOT_AREA_FROM_NEAREST_API:  %@",selectedArea);
                                                                    if (isPlayingAnimation) {
                                                                        [self performSelector:@selector(showAlert:) withObject:GOT_AREA_FROM_NEAREST_API afterDelay:2.9];
                                                                    }else{
                                                                        [self showAlert:GOT_AREA_FROM_NEAREST_API];
                                                                    }
                                                                }else{
                                                                    [self loadAreaListFromAreaTable];
                                                                }
                                                            }
                                                        }
                                                        [CommonCallback hideProgressHud];

                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        isNearAreaRequestAlreadySent = NO;
                                                        [CommonCallback hideProgressHud];
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                        [av show];
                                                    }];
    [operation start];
}
@end
