//
//  INOffersStreamViewController.m
//  shoplocal
//
//  Created by Rishi on 07/03/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//
#import <Social/Social.h>

#import "INOffersStreamViewController.h"
#import "INAllStoreViewController.h"
#import "INSearchViewController.h"
#import "INMerchantLoginViewController.h"
#import "INCustomerLoginViewController.h"
#import "INFavouritesViewController.h"
#import "INCustomerProfileViewController.h"
#import "constants.h"
#import "INCustomerSettingsViewController.h"
#import "INFBWebViewController.h"
#import "INAboutUsViewController.h"
#import "CommonCallback.h"
#import "INFeedObj.h"
#import "INAppDelegate.h"
#import "INCustBroadCastDetailsViewController.h"
#import <time.h>
#import <xlocale.h>
#import "CellWithImageView.h"
#import "CellWithoutImageView.h"
#import "UIImageView+WebCache.h"
#import "FacebookOpenGraphAPI.h"
#import "FacebookShareAPI.h"

#define FEEDS_LINK(ID,PAGE) [NSString stringWithFormat:@"%@%@%@broadcast_api/search?area_id=%@&page=%@&count=50",LIVE_SERVER,URL_PREFIX,API_VERSION,ID,PAGE]
#define THUMBNAIL_LINK(IMG_LINK) [NSString stringWithFormat:@"%@%@%@",LIVE_SERVER,URL_PREFIX,IMG_LINK]

#define OFFERS_SYNC_TIME_INTERVAL 24

#define RATE_INTERVAL 300

#define CALL_STORE [NSString stringWithFormat:@"%@%@broadcast_api/call",URL_PREFIX,API_VERSION]

@interface INOffersStreamViewController ()
@property(strong) NSDictionary *splashJson;
@end

@implementation INOffersStreamViewController
@synthesize feedsArray;
@synthesize tempfeedsArray;
@synthesize totalPage;
@synthesize totalRecord;
@synthesize currentPage;
@synthesize feedsTableView;
@synthesize countlbl;
@synthesize textPull, textRelease, textLoading, refreshHeaderView, refreshLabel, refreshArrow, refreshSpinner;
@synthesize countHeaderView,lblpullToRefresh;
@synthesize globalSearchBar;
@synthesize currentSelectedRow;
@synthesize searchView,searchTextField;
@synthesize selectedPostId;
@synthesize callModalViewBackView,callModalView,lblcallModalHeader,lblcallModalHeaderSeperator,callModalTableView,btnCancelcallModal;
@synthesize telArray;
@synthesize selectedPlaceName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self setupStrings];
    }
    return self;
}

-(void)pageRequest:(NSString *)pageNum
{
    if([IN_APP_DELEGATE networkavailable])
    {
        NSString *activeId = [IN_APP_DELEGATE getActiveAreaIdFromAreaTable];
        isAdding = YES;
        [self sendFeedsRequest:FEEDS_LINK(activeId,pageNum)];
    }  else   {
        isAdding = NO;
        if(isLoading)
        {
            [self stopLoading];
        }
        [INUserDefaultOperations showOfflineAlert];
    }
}

-(void)loadData
{
    [self readFeedFromDatabase];
    if([feedsArray count] > 0)
    {
        [feedsTableView reloadData];
        [self readRecordDatabase];
        [self setCountLabel];
        
        DebugLog(@"diff - >%d",[INUserDefaultOperations getDateDifferenceInHours:[INUserDefaultOperations getOffersSyncDate]]);
        
        if(([INUserDefaultOperations getDateDifferenceInHours:[INUserDefaultOperations getOffersSyncDate]]) > OFFERS_SYNC_TIME_INTERVAL)
        {
            if([IN_APP_DELEGATE networkavailable])
            {
                [feedsArray removeAllObjects];
                //        [shownIndexes removeAllObjects]; /*card animation
                self.currentPage = 1;
                [feedsTableView reloadData];
                [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
            }
        }
    }else{
        DebugLog(@"file not exists");
        self.currentPage = 1;
        [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
    }
}

- (void)reloadData:(NSNotification *)notification
{
    //DebugLog(@"Reload data");
    [self loadData];
}


-(void)setCountLabel
{
    //DebugLog(@"-----%d--------%d------",[storeArray count],self.totalRecord);
    self.countlbl.text = [NSString stringWithFormat:@"%d/%d",[feedsArray count],self.totalRecord];
    //DebugLog(@"-----%@--------",countlbl.text);
}


#pragma mark PULL TO REFRESH
- (void)setupStrings{
    textPull = @"Pull down to refresh...";
    textRelease = @"Release to refresh...";
    textLoading = @"Loading please wait...";
}

- (void)addPullToRefreshHeader {
    refreshHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0 - REFRESH_HEADER_HEIGHT, 320, REFRESH_HEADER_HEIGHT)];
    refreshHeaderView.backgroundColor = [UIColor clearColor];
    
    refreshLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, REFRESH_HEADER_HEIGHT)];
    refreshLabel.backgroundColor = [UIColor clearColor];
    refreshLabel.font = DEFAULT_BOLD_FONT(15);
    refreshLabel.textAlignment = NSTextAlignmentCenter;
    refreshLabel.textColor = DEFAULT_COLOR;
    refreshArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:PULLTOREFRESH_BARROW]];
    refreshArrow.frame = CGRectMake(floorf((REFRESH_HEADER_HEIGHT - 2) / 2),
                                    (floorf(REFRESH_HEADER_HEIGHT - 44) / 2),
                                    27, 44);
    
    refreshSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    refreshSpinner.frame = CGRectMake(floorf(floorf(REFRESH_HEADER_HEIGHT - 2) / 2), floorf((REFRESH_HEADER_HEIGHT - 20) / 2), 20, 20);
    refreshSpinner.hidesWhenStopped = YES;
    
    [refreshHeaderView addSubview:refreshLabel];
    [refreshHeaderView addSubview:refreshArrow];
    [refreshHeaderView addSubview:refreshSpinner];
    [feedsTableView addSubview:refreshHeaderView];
}


- (void)startLoading {
    isLoading = YES;
    // Show the header
    [UIView animateWithDuration:0.3 animations:^{
        feedsTableView.contentInset = UIEdgeInsetsMake(REFRESH_HEADER_HEIGHT, 0, 0, 0);
        refreshLabel.text = self.textLoading;
        refreshArrow.hidden = YES;
        [refreshSpinner startAnimating];
    }];
    if([IN_APP_DELEGATE networkavailable])
    {
        [feedsArray removeAllObjects];
        //        [shownIndexes removeAllObjects]; /*card animation
        self.currentPage = 1;
        [feedsTableView reloadData];
        [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
    }
    else
    {
        [self stopLoading];
    }
    // Refresh action!
    //[self refresh];
}

- (void)stopLoading {
    isLoading = NO;
    // Hide the header
    [UIView animateWithDuration:0.3 animations:^{
        feedsTableView.contentInset = UIEdgeInsetsZero;
        [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
    }
                     completion:^(BOOL finished) {
                         [self performSelector:@selector(stopLoadingComplete)];
                     }];
}

- (void)stopLoadingComplete {
    // Reset the header
    refreshLabel.text = self.textPull;
    refreshArrow.hidden = NO;
    [refreshSpinner stopAnimating];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIImageView *imageView = (UIImageView *)[self.navigationController.navigationBar viewWithTag:1000];
    [imageView setHidden:FALSE];
    UILabel *lbl = (UILabel *)[self.navigationController.navigationBar viewWithTag:1001];
    [lbl setHidden:FALSE];
    UIButton *moreButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1002];
    [moreButton setHidden:FALSE];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:FALSE];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:FALSE];
    
    NSString *selectedArea = [IN_APP_DELEGATE getActivePlaceNameFromAreaTable];
    DebugLog(@"selectedArea %@",selectedArea);
    [locatonButton setTitle:selectedArea forState:UIControlStateNormal];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    UIImageView *imageView = (UIImageView *)[self.navigationController.navigationBar viewWithTag:1000];
    [imageView setHidden:TRUE];
    UILabel *lbl = (UILabel *)[self.navigationController.navigationBar viewWithTag:1001];
    [lbl setHidden:TRUE];
    UIButton *moreButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1002];
    [moreButton setHidden:TRUE];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:TRUE];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                             style:UIBarButtonItemStylePlain
                                                            target:nil
                                                            action:nil];
    [[self navigationItem] setBackBarButtonItem:back];
    [self setupMenuBarButtonItems];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    
    [self.feedsTableView registerNib:[UINib nibWithNibName:@"CellWithImageView" bundle:nil] forCellReuseIdentifier:kWithImageCellIdentifier];
    [self.feedsTableView registerNib:[UINib nibWithNibName:@"CellWithoutImageView" bundle:nil] forCellReuseIdentifier:kWithoutImageCellIdentifier];
    
    loadWithAnimation = YES;
    feedsArray = [[NSMutableArray alloc] init];
    tempfeedsArray = [[NSMutableArray alloc] init];
    [self setUI];
    
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        globalSearchBar.barTintColor = DEFAULT_COLOR;
        [globalSearchBar setTranslucent:NO];
    }
    globalSearchBar.showsCancelButton = YES;
    globalSearchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    globalSearchBar.delegate =  self;
    globalSearchBar.hidden = TRUE;
    [globalSearchBar setFrame:CGRectMake(0, -globalSearchBar.frame.size.height, globalSearchBar.frame.size.width, globalSearchBar.frame.size.height)];
    
    //[self createBarButton];
    
    [self getLocationValues];
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changePageNumber) name:IN_CUSTOMER_LOGIN_LOGOUT_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadData:) name:IN_STORE_DID_CHANGE_NOTIFICATION object:nil];
    
    [self addPullToRefreshHeader];
    [self loadData];
    
    if([INUserDefaultOperations getAppTotalUsageTime] > RATE_INTERVAL)
    {
        if([INUserDefaultOperations getAppRaiterState] == TRUE)
        {
            [self performSelector:@selector(showAppRateAlert) withObject:nil afterDelay:5.0];
        }
    }
}



#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma set Navigation barbuttons callback
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
        {
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            //            UIBarButtonItem *filterBarBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"filter.png"]  style:UIBarButtonItemStylePlain target:self action:@selector(filterBarBtnClicked:)];
            //            UIBarButtonItem *searchBtn =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(searchBarBtnClicked:)];
            //            self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:searchBtn,filterBarBtn, nil];
//            UIBarButtonItem *filterBarBtn = [[UIBarButtonItem alloc] initWithTitle:@"Search" style:UIBarButtonItemStylePlain target:self action:@selector(filterBarBtnClicked:)];
            self.navigationItem.rightBarButtonItem = nil;//filterBarBtn;
            
        }
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
        {
            //            UIBarButtonItem *filterBarBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"filter.png"]  style:UIBarButtonItemStylePlain target:self action:@selector(filterBarBtnClicked:)];
            //            UIBarButtonItem *searchBtn =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(searchBarBtnClicked:)];
            //            self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:searchBtn,filterBarBtn, nil];
//            UIBarButtonItem *filterBarBtn = [[UIBarButtonItem alloc] initWithTitle:@"Search" style:UIBarButtonItemStylePlain target:self action:@selector(filterBarBtnClicked:)];
            self.navigationItem.rightBarButtonItem = nil;//filterBarBtn;
            
        }
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:MENU_TITLE style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    return [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(searchBarBtnClicked:)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)searchBarBtnClicked:(id)sender {
    globalSearchBar.text = @"";
    [globalSearchBar becomeFirstResponder];
    
    self.navigationItem.rightBarButtonItems = nil;
    UIBarButtonItem *filterBarBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"filter.png"]  style:UIBarButtonItemStylePlain target:self action:@selector(filterBarBtnClicked:)];
    self.navigationItem.rightBarButtonItem = filterBarBtn;
    globalSearchBar.hidden = FALSE;
    [UIView animateWithDuration:0.3 animations:^{
        [globalSearchBar setFrame:CGRectMake(0, 0, globalSearchBar.frame.size.width, globalSearchBar.frame.size.height)];
    }];
}

-(void)searchBarCancelBtnClicked {
    globalSearchBar.text = @"";
    [globalSearchBar resignFirstResponder];
    
    self.navigationItem.rightBarButtonItem = nil;
    //    UIBarButtonItem *filterBarBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"filter.png"]  style:UIBarButtonItemStylePlain target:self action:@selector(filterBarBtnClicked:)];
    //    UIBarButtonItem *searchBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(searchBarBtnClicked:)];
    //    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:searchBtn,filterBarBtn, nil];
    UIBarButtonItem *filterBarBtn = [[UIBarButtonItem alloc] initWithTitle:@"Search" style:UIBarButtonItemStylePlain target:self action:@selector(filterBarBtnClicked:)];
    self.navigationItem.rightBarButtonItem = filterBarBtn;
    
    [UIView animateWithDuration:0.3 animations:^{
        [globalSearchBar setFrame:CGRectMake(0, -globalSearchBar.frame.size.height, globalSearchBar.frame.size.width, globalSearchBar.frame.size.height)];
    } completion:^(BOOL finished){
        globalSearchBar.hidden = TRUE;
    }];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    INSearchViewController *searchController = [[INSearchViewController alloc] initWithNibName:@"INSearchViewController" bundle:nil] ;
    searchController.title = @"Search";
    searchController.textToSearch = searchBar.text;
    searchController.isStoreSearch = NO;
    [self.navigationController pushViewController:searchController animated:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar{
    [self searchBarCancelBtnClicked];
}
- (void)filterBarBtnClicked:(id)sender {
    DebugLog(@"filterBarBtnClicked");
    INSearchViewController *searchController = [[INSearchViewController alloc] initWithNibName:@"INSearchViewController" bundle:nil] ;
    searchController.title = @"Search";
    searchController.showCancelBarBtn = YES;
    searchController.isStoreSearch = NO;
    [UIView  transitionWithView:self.navigationController.view duration:0.5  options:UIViewAnimationOptionTransitionCrossDissolve
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         [self.navigationController pushViewController:searchController animated:NO];
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:nil];
}

#pragma internal methods
-(void)setUI
{
    countHeaderView.backgroundColor =   [UIColor clearColor];
    countHeaderView.clipsToBounds   = YES;
    
    lblpullToRefresh.font               =   DEFAULT_FONT(15.0);
    lblpullToRefresh.backgroundColor    =   LIGHT_GREEN_COLOR_WITH_ALPHA(0.5);
    lblpullToRefresh.textColor          =   [UIColor whiteColor];
    
    countlbl.font               =   DEFAULT_FONT(15.0);
    countlbl.backgroundColor    =   LIGHT_GREEN_COLOR_WITH_ALPHA(0.5);
    countlbl.textColor          =   [UIColor whiteColor];
    
    self.feedsTableView.backgroundColor = [UIColor clearColor];//[UIColor colorWithWhite:0.9 alpha:1.0];
    self.feedsTableView.separatorColor = [UIColor clearColor];
    
    searchView.backgroundColor          =       DEFAULT_COLOR;
    searchTextField.font                =       DEFAULT_FONT(20.0);
    searchTextField.backgroundColor     =       [UIColor whiteColor];
    searchTextField.textColor           =       DEFAULT_COLOR;
    searchTextField.layer.cornerRadius  =       8.0;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        searchTextField.tintColor           = DEFAULT_COLOR;
    }
    
    //callModalView = [CommonCallback setViewPropertiesWithRoundedCorner:callModalView];
    callModalView.backgroundColor =  [UIColor whiteColor];
    callModalView.layer.shadowColor      = [UIColor blackColor].CGColor;
    callModalView.layer.shadowOffset     = CGSizeMake(1, 1);
    callModalView.layer.shadowOpacity    = 1.0;
    callModalView.layer.shadowRadius     = 10.0;
    [callModalViewBackView setHidden:YES];
    [callModalView setHidden:YES];
    
    lblcallModalHeader.textAlignment     = NSTextAlignmentCenter;
    lblcallModalHeader.backgroundColor   = [UIColor clearColor];
    lblcallModalHeader.font              = DEFAULT_BOLD_FONT(20);
    lblcallModalHeader.textColor         = BROWN_COLOR;
    //lblcallModalHeader.adjustsFontSizeToFitWidth = YES;
    lblcallModalHeader.minimumScaleFactor = 15;
    lblcallModalHeader.text              = @"Select number";
    lblcallModalHeader.numberOfLines     = 2;
    
    lblcallModalHeaderSeperator.backgroundColor   = DEFAULT_COLOR;
    callModalTableView.backgroundColor = [UIColor whiteColor];
    if ([callModalTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [callModalTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    btnCancelcallModal.backgroundColor = [UIColor clearColor];
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if ([textField isEqual:searchTextField]) {
        INSearchViewController *searchController = [[INSearchViewController alloc] initWithNibName:@"INSearchViewController" bundle:nil] ;
        searchController.title = @"Search";
        searchController.hideBackBarBtn = YES;
        searchController.isStoreSearch = NO;
        [UIView  transitionWithView:self.navigationController.view duration:0.5  options:UIViewAnimationOptionTransitionCrossDissolve
                         animations:^(void) {
                             BOOL oldState = [UIView areAnimationsEnabled];
                             [UIView setAnimationsEnabled:NO];
                             [self.navigationController pushViewController:searchController animated:NO];
                             [UIView setAnimationsEnabled:oldState];
                         }
                         completion:^(BOOL finished){
                             [textField resignFirstResponder];
                         }];
    }
    return YES;
}

-(void)getLocationValues
{
    if ([CLLocationManager locationServicesEnabled])
    {
        DebugLog(@"Location services are enabled.");
        //        [self showProgressHUDWithMessage:@"Please wait ... \n shoplocal is trying to find your location."];
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        [locationManager startUpdatingLocation];
        [self performSelector:@selector(stopUpdatingCoreLocation:) withObject:@"Timed Out" afterDelay:10];
    }
    else
    {
        DebugLog(@"Location services not enabled.");
        UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Location serviecs are disabled on your device, shoplocal needs your location to bring you information from nearby, please enable location services." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [servicesDisabledAlert show];
    }
}

- (void) locationManager:(CLLocationManager *) manager didFailWithError:(NSError *) error
{
    [locationManager stopUpdatingLocation];
    //    [self hideProgressHUD:YES];
}

- (void)stopUpdatingCoreLocation:(NSString *)state
{
    //    [self hideProgressHUD:YES];
    [locationManager stopUpdatingLocation];
    [self saveCurrentLocation:[locationManager location]];
}

-(void)saveCurrentLocation:(CLLocation *)lLocation
{
    // If it's not possible to get a location, then return.
	if (!lLocation) {
        //        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
        //                                                             message:@"Shoplocal is not able to get your current location accurately. Let's check again in some time."
        //                                                            delegate:nil
        //                                                   cancelButtonTitle:@"OK"
        //                                                   otherButtonTitles:nil];
        //        [errorAlert show];
		return;
	}
    // Configure the new event with information from the location.
    DebugLog(@"%f --- %f",lLocation.coordinate.latitude,lLocation.coordinate.longitude);
    [INUserDefaultOperations setLatitude:lLocation.coordinate.latitude];
    [INUserDefaultOperations setLongitude:lLocation.coordinate.longitude];
    [INEventLogger logLocationValues:lLocation];
    DebugLog(@"%f --- %f",[INUserDefaultOperations getLatitude],[INUserDefaultOperations getLongitude]);
}

-(void)createBarButton
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(100.0f, 5.0f, 120.0f, 35.0f);
    button.titleLabel.font = DEFAULT_FONT(16);
    button.titleLabel.lineBreakMode = NSLineBreakByClipping;
    button.tag = 1111;
    [button setBackgroundImage:[UIImage imageNamed:@"menu_back.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchDown];
    if([[INUserDefaultOperations getSelectedStore] isEqualToString:@""])
    {
        [INUserDefaultOperations setSelectedStore:@"Lokhandwala, Andheri West"];
    } else {
        [button setTitle:[INUserDefaultOperations getSelectedStore] forState:UIControlStateNormal];
        [button setTitle:[INUserDefaultOperations getSelectedStore] forState:UIControlStateHighlighted];
    }
    
    [self.navigationController.navigationBar addSubview:button];
}

-(void)setBarButtonText:(NSString *)text
{
    UIButton *button = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [button setTitle:text forState:UIControlStateNormal];
    [button setTitle:text forState:UIControlStateHighlighted];
}

- (void)selectedTableRow:(NSString *)value
{
    // DebugLog(@"SELECTED ROW %@ ",value );
    [popover dismissPopoverAnimated:YES];
    if ([[INUserDefaultOperations getSelectedStore] isEqualToString:value]) {
        //DebugLog(@"SELECTED ROW %@ ",@"Return" );
        return;
    }
    [INUserDefaultOperations setSelectedStore:value];
    [self setBarButtonText:value];
    [[NSNotificationCenter defaultCenter] postNotificationName:IN_STORE_DID_CHANGE_NOTIFICATION object:nil];
}

- (void)buttonClicked:(UIButton *)sender
{
    INPopTableController *controller = [[INPopTableController alloc] initWithStyle:UITableViewStylePlain];
    controller.delegate = self;
    popover = [[FPPopoverController alloc] initWithViewController:controller];
    popover.arrowDirection = FPPopoverArrowDirectionUp;
    popover.contentSize = CGSizeMake(180,200);
    popover.border = YES;
    popover.alpha = 1.0;
    controller.title = @"Choose Location";
    popover.tint = FPPopoverDefaultTint;
    [popover presentPopoverFromView:sender];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)showLoginModal{
    INCustomerLoginViewController *custLoginController = [[INCustomerLoginViewController alloc] initWithNibName:@"INCustomerLoginViewController" bundle:nil] ;
    custLoginController.delegate = self;
    UINavigationController *loginnavBar=[[UINavigationController alloc]initWithRootViewController:custLoginController];
    UIButton *moreButton1 =  [UIButton buttonWithType:UIButtonTypeCustom];
    [moreButton1 setTitle:@"Cancel" forState:UIControlStateNormal];
    moreButton1.titleLabel.font = [UIFont systemFontOfSize:17.0];
    //[moreButton1 setBackgroundImage:[UIImage imageNamed:@"Transparent_Button.png"] forState:UIControlStateNormal];
    [moreButton1 addTarget:self action:@selector(dismissLoginView) forControlEvents:UIControlEventTouchUpInside];
    [moreButton1 setFrame:CGRectMake(250, 7, 70, 32)];
    [loginnavBar.navigationBar addSubview:moreButton1];
    [loginnavBar.navigationBar setTranslucent:NO];
    [self.navigationController presentViewController:loginnavBar animated:YES completion:nil];
}

-(void)dismissLoginView
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"Dismiss");
    }];
}

#pragma scrollViewDelegates
- (void)scrollViewDidScroll:(UIScrollView *)lscrollView
{
    if ([lscrollView isEqual:callModalTableView]) {
        return;
    }
    //DebugLog(@"---->>>>>%@",sender);
    if (isLoading) {
        // Update the content inset, good for section headers
        if (lscrollView.contentOffset.y > 0)
            feedsTableView.contentInset = UIEdgeInsetsZero;
        else if (lscrollView.contentOffset.y >= -REFRESH_HEADER_HEIGHT)
            feedsTableView.contentInset = UIEdgeInsetsMake(-lscrollView.contentOffset.y, 0, 0, 0);
    } else if (isDragging && lscrollView.contentOffset.y < 0) {
        // Update the arrow direction and label
        [UIView animateWithDuration:0.25 animations:^{
            if (lscrollView.contentOffset.y < -REFRESH_HEADER_HEIGHT) {
                // User is scrolling above the header
                refreshLabel.text = self.textRelease;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
            } else {
                // User is scrolling somewhere within the header
                refreshLabel.text = self.textPull;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
            }
        }];
    } else {
        CGFloat height = lscrollView.frame.size.height;
        
        CGFloat contentYoffset = lscrollView.contentOffset.y;
        
        CGFloat distanceFromBottom = lscrollView.contentSize.height - contentYoffset;
        
        if(distanceFromBottom <= height)
        {
            if(isAdding==TRUE) {
                //DebugLog(@"<<<<<<<<<<<<<<<");
                return;
            } else {
                //DebugLog(@"end of the table page = %d", self.currentPage);
                if(self.currentPage < self.totalPage)
                {
                    self.currentPage = self.currentPage + 1;
                    [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
                }
            }
        }
    }
    
}

// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *)lscrollView
{
    if ([lscrollView isEqual:callModalTableView]) {
        return;
    }
    loadWithAnimation = YES;
    if (isLoading) return;
    isDragging = YES;
    
    lastContentOffset = lscrollView.contentOffset.y;
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)lscrollView
{
    if ([lscrollView isEqual:callModalTableView]) {
        return;
    }
    pageControlUsed = NO;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)lscrollView willDecelerate:(BOOL)decelerate {
    if ([lscrollView isEqual:callModalTableView]) {
        return;
    }
    if (isLoading) return;
    isDragging = NO;
    if (lscrollView.contentOffset.y <= -REFRESH_HEADER_HEIGHT) {
        // Released above the header
        [self startLoading];
    }
    if (lastContentOffset < (int)lscrollView.contentOffset.y) {
        DebugLog(@"lastContentOffset <");
        if (searchView.frame.size.height >= 72) {
            [UIView animateWithDuration:0.2
                             animations:^(void){
                                 searchView.frame = CGRectMake(searchView.frame.origin.x, searchView.frame.origin.y, searchView.frame.size.width, 36);
                                 feedsTableView.frame = CGRectMake(feedsTableView.frame.origin.x, CGRectGetMaxY(searchView.frame), feedsTableView.frame.size.width, self.view.frame.size.height-(CGRectGetMaxY(searchView.frame)));
                             } completion:nil];
        }
    }
    else if (lastContentOffset > (int)lscrollView.contentOffset.y) {
        DebugLog(@"lastContentOffset >");
        if (searchView.frame.size.height < 72) {
            [UIView animateWithDuration:0.2
                             animations:^(void){
                                 searchView.frame = CGRectMake(searchView.frame.origin.x, searchView.frame.origin.y, searchView.frame.size.width, 72);
                                 feedsTableView.frame = CGRectMake(feedsTableView.frame.origin.x, CGRectGetMaxY(searchView.frame), feedsTableView.frame.size.width, self.view.frame.size.height-(CGRectGetMaxY(searchView.frame)));
                             } completion:nil];
        }
    }
}

-(void)sendFeedsRequest:(NSString *)urlString
{
    DebugLog(@"url - %@",urlString);
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"--->%@",self.splashJson);
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        
                                                        if(self.splashJson  != nil) {
                                                            [tempfeedsArray removeAllObjects];
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                
                                                                [INUserDefaultOperations setOffersSyncDate];
                                                                NSDictionary *maindict = [self.splashJson  objectForKey:@"data"];
                                                                self.totalPage = [[maindict  objectForKey:@"total_page"] integerValue];
                                                                self.totalRecord = [[maindict  objectForKey:@"total_record"] integerValue];
                                                                if(self.currentPage == 1)
                                                                {
                                                                    [self  deleteFeedsTable];
                                                                    [feedsArray removeAllObjects];
                                                                    //                                                                    [shownIndexes removeAllObjects];  /*card animation
                                                                }
                                                                [self updateTotalPage:self.totalPage];
                                                                [self updateTotalRecords:self.totalRecord];
                                                                [self updateCurrentPage:self.currentPage];
                                                                NSArray* jsonArray = [maindict  objectForKey:@"record"];
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *objdict = [jsonArray  objectAtIndex:index];
                                                                    DebugLog(@"\n\n%@\n\n",objdict);
                                                                    feedObj = [[INFeedObj alloc] init];
                                                                    feedObj.postID = [[objdict objectForKey:@"post_id"] integerValue];
                                                                    feedObj.placeID = [[objdict objectForKey:@"id"] integerValue];
                                                                    feedObj.areaID = [objdict objectForKey:@"area_id"];
                                                                    feedObj.name = [objdict objectForKey:@"name"];
                                                                    feedObj.title = [objdict objectForKey:@"title"];
                                                                    feedObj.description = [objdict objectForKey:@"description"];
                                                                    feedObj.date = [objdict objectForKey:@"date"];
                                                                    feedObj.offerdate = [objdict objectForKey:@"offer_date_time"];
                                                                    feedObj.distance = [objdict objectForKey:@"distance"];
                                                                    feedObj.latitude = [objdict objectForKey:@"latitude"];
                                                                    feedObj.longitude = [objdict objectForKey:@"longitude"];
                                                                    feedObj.tel_no1 = [objdict objectForKey:@"tel_no1"];
                                                                    feedObj.tel_no2 = [objdict objectForKey:@"tel_no2"];
                                                                    feedObj.tel_no3 = [objdict objectForKey:@"tel_no3"];
                                                                    feedObj.mob_no1 = [objdict objectForKey:@"mob_no1"];
                                                                    feedObj.mob_no2 = [objdict objectForKey:@"mob_no2"];
                                                                    feedObj.mob_no3 = [objdict objectForKey:@"mob_no3"];
                                                                    feedObj.image_url1 = [objdict objectForKey:@"image_url1"];
                                                                    feedObj.image_url2 = [objdict objectForKey:@"image_url2"];
                                                                    feedObj.image_url3 = [objdict objectForKey:@"image_url3"];
                                                                    feedObj.thumb_url1 = [objdict objectForKey:@"thumb_url1"];
                                                                    feedObj.thumb_url2 = [objdict objectForKey:@"thumb_url2"];
                                                                    feedObj.thumb_url3 = [objdict objectForKey:@"thumb_url3"];
                                                                    feedObj.total_like = [objdict objectForKey:@"total_like"];
                                                                    feedObj.total_share = [objdict objectForKey:@"total_share"];
                                                                    feedObj.is_offered = [objdict objectForKey:@"is_offered"];
                                                                    feedObj.type = [objdict objectForKey:@"type"];
                                                                    [tempfeedsArray addObject:feedObj];
                                                                    feedObj = nil;
                                                                }
                                                            } else {
                                                                if([self.splashJson  objectForKey:@"message"] != [NSNull null])
                                                                    //                                                                    [INUserDefaultOperations showAlert:[self.splashJson  objectForKey:@"message"]];
                                                                    [INUserDefaultOperations performSelector:@selector(showAlert:) withObject:[self.splashJson  objectForKey:@"message"] afterDelay:1.0];
                                                                
                                                            }
                                                            if([tempfeedsArray count] > 0)
                                                            {
                                                                DebugLog(@"-----%d--------",[tempfeedsArray count]);
                                                                [self insertFeedListTable];
                                                                [self readFeedFromDatabase];
                                                            }
                                                            [feedsTableView reloadData];
                                                            [self setCountLabel];
                                                        }
                                                        isAdding = NO;
                                                        if(isLoading)
                                                        {
                                                            [self stopLoading];
                                                        }
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        [INUserDefaultOperations performSelector:@selector(showAlert:) withObject:[NSString stringWithFormat:@"%@",[error localizedDescription]] afterDelay:2.0];
                                                        
                                                        isAdding = NO;
                                                        if(isLoading)
                                                        {
                                                            [self stopLoading];
                                                        }
                                                        [self readRecordDatabase];
                                                        
                                                    }];
    
    
    
    [operation start];
}


#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:callModalTableView]) {
        return 80;
    }else{
        INFeedObj *tempfeedObj  = [feedsArray objectAtIndex:indexPath.row];
        if(tempfeedObj.image_url1 != (NSString*)[NSNull null] && ![tempfeedObj.image_url1 isEqualToString:@""]) {
            return kWithImageCellHeight;
        }
        else {
            return kWithoutImageCellHeight;
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:callModalTableView]) {
        return [telArray count];
    }else{
        return [feedsArray count];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:callModalTableView]) {
        static NSString *CellIdentifier = @"CallCell";
        UIImageView *thumbImg;
        UILabel *lblNumber;
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.backgroundColor = [UIColor whiteColor];
            
            thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(10,15, 50, 50)];
            thumbImg.tag = 1111;
            thumbImg.contentMode = UIViewContentModeScaleToFill;
            thumbImg.image = [UIImage imageNamed:@"call_brown.png"];
            [cell.contentView addSubview:thumbImg];
            
            lblNumber = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame),0, 210, 80)];
            lblNumber.text = @"";
            lblNumber.tag = 2222;
            lblNumber.textColor             = BROWN_COLOR;
            lblNumber.highlightedTextColor  = [UIColor whiteColor];
            lblNumber.font                  = DEFAULT_FONT(23);
            lblNumber.textAlignment         = NSTextAlignmentLeft;
            lblNumber.backgroundColor       = [UIColor clearColor];
            [cell.contentView addSubview:lblNumber];
            
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = BROWN_OFFWHITE_COLOR;
            cell.selectedBackgroundView = bgColorView;
        }
        lblNumber        = (UILabel *)[cell.contentView viewWithTag:2222];
        DebugLog(@"lblNumber %@",[telArray objectAtIndex:indexPath.row]);
        lblNumber.text   = [telArray objectAtIndex:indexPath.row];
        return cell;
    }else{
        //  DebugLog(@"---%@----",feedsArray);
        if ([feedsArray count] <= 0)
            return nil;
        
        INFeedObj *tempfeedObj  = [feedsArray objectAtIndex:indexPath.row];
        DebugLog(@"---%@----",tempfeedObj.date);
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        NSDate *myDate = [[NSDate alloc] init];
        myDate = [dateFormatter dateFromString:tempfeedObj.date];
        DebugLog(@"MyDate is: %@", myDate);
        
        
        //char	*strptime_l(const char * __restrict, const char * __restrict, struct tm * __restrict, locale_t)
        //    const char *str = [tempfeedObj.date UTF8String];
        //    const char *fmt = [@"%Y-%m-%d %H:%M:%S" UTF8String];
        //    struct tm timeinfo;
        //    memset(&timeinfo, 0, sizeof(timeinfo));
        //    char *ret = strptime_l(str, fmt, &timeinfo, NULL);
        //
        //    NSDate *myDate = nil;
        //
        //    if (ret) {
        //        time_t time = mktime(&timeinfo);
        //
        //        myDate = [NSDate dateWithTimeIntervalSince1970:time];
        //        DebugLog(@"MyDate is: %@", myDate);
        //    }
        
        
        NSDateFormatter *dayformatter = [[NSDateFormatter alloc] init];
        [dayformatter setDateFormat:@"dd"];
        NSString *dayFromDate = [dayformatter stringFromDate:myDate];
        DebugLog(@"Myday is: %@", dayFromDate);
        
        NSDateFormatter *monthformatter = [[NSDateFormatter alloc] init];
        [monthformatter setDateFormat:@"MMM"];
        NSString *monthFromDate = [monthformatter stringFromDate:myDate];
        DebugLog(@"MyMonth is: %@", monthFromDate);
        
        NSDateFormatter *timeformatter = [[NSDateFormatter alloc] init];
        [timeformatter setDateFormat:@"hh:mm a"];
        NSString *timeFromDate = [timeformatter stringFromDate:myDate];
        DebugLog(@"timeFromDate is: %@", timeFromDate);
        
        if(tempfeedObj.image_url1 != (NSString*)[NSNull null] && ![tempfeedObj.image_url1 isEqualToString:@""]) {
            NSString *cellIdentifier = kWithImageCellIdentifier;
            CellWithImageView *cell = (CellWithImageView *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            cell.lblTitle.text          = tempfeedObj.title;
            cell.lblLeftTitle.text      = tempfeedObj.name;
            cell.lblDescription.text    = tempfeedObj.description;
            cell.lblLikeCount.text      = tempfeedObj.total_like;
            cell.lblShareCount.text     = tempfeedObj.total_share;
            cell.lblTimeStamp.text      = [NSString stringWithFormat:@"%@ %@   %@",dayFromDate,monthFromDate,timeFromDate];
            if (tempfeedObj.is_offered != nil && [tempfeedObj.is_offered isEqualToString:@"1"]) {
                [cell.offerImageView setHidden:NO];
            }else{
                [cell.offerImageView setHidden:YES];
            }
            if (tempfeedObj.user_like != nil && [tempfeedObj.user_like isEqualToString:@"1"]) {
                cell.favImageView.image = [UIImage imageNamed:@"filled_like.png"];
            }else{
                cell.favImageView.image = [UIImage imageNamed:@"empty_like.png"];
            }
            
            if(tempfeedObj.image_url1 != nil && ![tempfeedObj.image_url1 isEqualToString:@""])
            {
                __weak UIImageView *thumbImg_ = cell.thumbImageView;
                //            [cell.thumbImageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(tempfeedObj.image_url1)]
                //                                placeholderImage:[UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"]
                //                                         success:^(UIImage *image) {
                //                                             DebugLog(@"success");
                //                                         }
                //                                         failure:^(NSError *error) {
                //                                             DebugLog(@"write error %@", error);
                //                                             thumbImg_.image = [UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"];
                //                                         }];
                
                [cell.thumbImageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(tempfeedObj.image_url1)] placeholderImage:[UIImage imageNamed:@"offer_placeholder.jpg"] completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType) {
                    if (error) {
                        thumbImg_.image = [UIImage imageNamed:@"offer_placeholder.jpg"];
                    }
                }];
            } else {
                cell.thumbImageView.image = [UIImage imageNamed:@"offer_placeholder.jpg"];
            }
            
            UIButton *likeBtn = (UIButton *)[cell viewWithTag:111];
            [likeBtn addTarget:self action:@selector(likeBtnPressed:event:) forControlEvents:UIControlEventTouchUpInside];
            
            UIButton *shareBtn = (UIButton *)[cell viewWithTag:222];
            [shareBtn addTarget:self action:@selector(shareBtnPressed:event:) forControlEvents:UIControlEventTouchUpInside];
            
            UIButton *callBtn = (UIButton *)[cell viewWithTag:333];
            [callBtn addTarget:self action:@selector(callBtnTapped:event:) forControlEvents:UIControlEventTouchUpInside];
            return cell;
            
        }else{
            NSString *cellIdentifier = kWithoutImageCellIdentifier;
            CellWithoutImageView *cell = (CellWithoutImageView *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            cell.lblTitle.text          = tempfeedObj.title;
            cell.lblLeftTitle.text      = tempfeedObj.name;
            cell.lblDescription.text    = tempfeedObj.description;
            cell.lblLikeCount.text      = tempfeedObj.total_like;
            cell.lblShareCount.text     = tempfeedObj.total_share;
            cell.lblTimeStamp.text      = [NSString stringWithFormat:@"%@ %@   %@",dayFromDate,monthFromDate,timeFromDate];
            if (tempfeedObj.is_offered != nil && [tempfeedObj.is_offered isEqualToString:@"1"]) {
                [cell.offerImageView setHidden:NO];
            }else{
                [cell.offerImageView setHidden:YES];
            }
            if (tempfeedObj.user_like != nil && [tempfeedObj.user_like isEqualToString:@"1"]) {
                cell.favImageView.image = [UIImage imageNamed:@"filled_like.png"];
            }else{
                cell.favImageView.image = [UIImage imageNamed:@"empty_like.png"];
            }
            
            UIButton *likeBtn = (UIButton *)[cell viewWithTag:111];
            [likeBtn addTarget:self action:@selector(likeBtnPressed:event:) forControlEvents:UIControlEventTouchUpInside];
            
            UIButton *shareBtn = (UIButton *)[cell viewWithTag:222];
            [shareBtn addTarget:self action:@selector(shareBtnPressed:event:) forControlEvents:UIControlEventTouchUpInside];
            
            UIButton *callBtn = (UIButton *)[cell viewWithTag:333];
            [callBtn addTarget:self action:@selector(callBtnTapped:event:) forControlEvents:UIControlEventTouchUpInside];
            return cell;
        }
    }
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView isEqual:callModalTableView])
        return;
    
    if (indexPath.row > feedsArray.count || feedsArray.count <= 0) {
        return;
    }
    if (!loadWithAnimation) {
        return;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        cell.contentView.alpha = 0.3;
        cell.contentView.transform = CGAffineTransformMakeScale(0.6, 0.6);
        
        [self.feedsTableView bringSubviewToFront:cell.contentView];
        [UIView animateWithDuration:0.65 animations:^{
            cell.contentView.alpha = 1;
            //clear the transform
            cell.contentView.transform = CGAffineTransformIdentity;
        } completion:nil];
    });
}


#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:callModalTableView]) {
        [callModalView setHidden:YES];
        [callModalViewBackView setHidden:YES];
        NSString *selectedTelNumberToCall = [[telArray objectAtIndex:indexPath.row] stringByReplacingOccurrencesOfString:@" " withString:@""];
        DebugLog(@"selectedTelNumberToCall %@",selectedTelNumberToCall);
        NSString *deviceType = [UIDevice currentDevice].model;
        if([deviceType isEqualToString:@"iPhone"])
        {
            UIApplication *myApp = [UIApplication sharedApplication];
            DebugLog(@"selectedTelNumberToCall = -%@-",selectedTelNumberToCall);
            [myApp openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",selectedTelNumberToCall]]];
            
            NSDictionary *callParams = [NSDictionary dictionaryWithObjectsAndKeys:@"OfferStream",@"Source",[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"AreaName",selectedPlaceName,@"StoreName",selectedTelNumberToCall,@"Number", nil];
            [INEventLogger logEvent:@"StoreCallDone" withParams:callParams];
            
            [self sendStoreCallRequest:@"StoreCallDone"];
        }
        else
        {
            UIAlertView *alert = [[ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                              message: @"Calling functionality is not available in this device. "
                                                             delegate: nil
                                                    cancelButtonTitle: nil
                                                    otherButtonTitles: @"OK", nil];
            [alert show];
        }
    }else{
        INFeedObj *lfeedObj  = [feedsArray objectAtIndex:indexPath.row];
        INCustBroadCastDetailsViewController *postDetailController = [[INCustBroadCastDetailsViewController alloc] initWithNibName:@"INCustBroadCastDetailsViewController" bundle:nil] ;
        postDetailController.title = lfeedObj.name;//@"Broadcast Detail";
        postDetailController.storeName = lfeedObj.name;
        postDetailController.postType = 1;
        //    postDetailController.postlikeType = lfeedObj.total_like;
        postDetailController.postId = lfeedObj.postID;
        postDetailController.postTitle = lfeedObj.title;
        postDetailController.postDescription = lfeedObj.description;
        DebugLog(@"lfeedObj.image_url1 %@",lfeedObj.image_url1);
        if ([lfeedObj.image_url1 isEqual:[NSNull null]] || lfeedObj.image_url1 == nil) {
            postDetailController.postimageUrl = @"";
        }else{
            postDetailController.postimageUrl = lfeedObj.image_url1;
        }
        postDetailController.likescountString = lfeedObj.total_like;
        postDetailController.sharecountString = lfeedObj.total_share;
        postDetailController.viewOpenedFrom = FROM_NEWS_FEED;
        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
        if (lfeedObj.tel_no1 != nil && ![lfeedObj.tel_no1 isEqualToString:@""]) {
            [tempArray addObject:lfeedObj.tel_no1];
        }
        if (lfeedObj.tel_no2 != nil && ![lfeedObj.tel_no2 isEqualToString:@""]) {
            [tempArray addObject:lfeedObj.tel_no2];
        }
        if (lfeedObj.tel_no3 != nil && ![lfeedObj.tel_no3 isEqualToString:@""]) {
            [tempArray addObject:lfeedObj.tel_no3];
        }
        if (lfeedObj.mob_no1 != nil && ![lfeedObj.mob_no1 isEqualToString:@""]) {
            [tempArray addObject:[NSString stringWithFormat:@"+%@",lfeedObj.mob_no1]];
        }
        if (lfeedObj.mob_no2 != nil && ![lfeedObj.mob_no2 isEqualToString:@""]) {
            [tempArray addObject:[NSString stringWithFormat:@"+%@",lfeedObj.mob_no2]];
        }
        if (lfeedObj.mob_no3 != nil && ![lfeedObj.mob_no3 isEqualToString:@""]) {
            [tempArray addObject:[NSString stringWithFormat:@"+%@",lfeedObj.mob_no3]];
        }
        DebugLog(@"temparray %@",tempArray);
        postDetailController.telArray = tempArray;
        postDetailController.SHOW_BUY_BTN = YES;
        postDetailController.placeId = lfeedObj.placeID;
        postDetailController.postDateTime   = lfeedObj.date;
        postDetailController.postOfferDateTime  = lfeedObj.offerdate;
        postDetailController.postIsOffered      = lfeedObj.is_offered;
        [self.navigationController pushViewController:postDetailController animated:YES];
        [INEventLogger logEvent:@"OfferStream_Details"];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma Table Operations
- (void)insertFeedListTable
{
    sqlite3_stmt    *statement;
    int lastrowid;
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        for (int index=0; index<[tempfeedsArray count] ; index++)
        {
            INFeedObj *tempfeedObj = [tempfeedsArray objectAtIndex:index];
            NSString *insertSQL = [NSString stringWithFormat:
                                   @"INSERT or REPLACE INTO FEEDS (POSTID, PLACEID , AREAID , TITLE , DESCRIPTION , DATE , OFFERDATE , DISTANCE , MOB_NO1 , MOB_NO2 , MOB_NO3 , TEL_NO1 , TEL_NO2 , TEL_NO3 , IMG_URL1 , IMG_URL2 , IMG_URL3 , THUMB_URL1 , THUMB_URL2 , THUMB_URL3 , IS_OFFERED , TOTAL_LIKE , TOTAL_SHARE , LATITUDE , LONGITUDE , TYPE , NAME) VALUES (\"%d\",\"%d\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",tempfeedObj.postID,tempfeedObj.placeID,tempfeedObj.areaID,[tempfeedObj.title stringByReplacingOccurrencesOfString:@"\"" withString:@"'"],[tempfeedObj.description stringByReplacingOccurrencesOfString:@"\"" withString:@"'"],tempfeedObj.date,tempfeedObj.offerdate,tempfeedObj.distance,tempfeedObj.mob_no1,tempfeedObj.mob_no2,tempfeedObj.mob_no3,tempfeedObj.tel_no1,tempfeedObj.tel_no2,tempfeedObj.tel_no3,tempfeedObj.image_url1,tempfeedObj.image_url2,tempfeedObj.image_url3,tempfeedObj.thumb_url1,tempfeedObj.thumb_url2,tempfeedObj.thumb_url3,tempfeedObj.is_offered,tempfeedObj.total_like,tempfeedObj.total_share,tempfeedObj.latitude,tempfeedObj.longitude,tempfeedObj.type, tempfeedObj.name];
            DebugLog(@"%@",insertSQL);
            const char *insert_stmt = [insertSQL UTF8String];
            sqlite3_prepare_v2(shoplocalDB, insert_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                lastrowid = sqlite3_last_insert_rowid(shoplocalDB);
                //DebugLog(@"inserted id========%d", lastrowid);
            } else {
                DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
            }
            sqlite3_finalize(statement);
        }
    }
    sqlite3_close(shoplocalDB);
}

-(void) readFeedFromDatabase {
    
    NSString *activeId = [IN_APP_DELEGATE getActiveAreaIdFromAreaTable];
    
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    // Init the expense Array
    if(feedsArray == nil) {
        DebugLog(@"new  expense array created");
        feedsArray = [[NSMutableArray alloc] init];
    } else {
        DebugLog(@"old  expense array used");
        [feedsArray removeAllObjects];
        //        [shownIndexes removeAllObjects];  /*card animation
    }
    
    
    //    POSTID INTEGER not null unique, PLACEID INTEGER, AREA TEXT, TITLE TEXT, DESCRIPTION TEXT, DATE TEXT, OFFERDATE TEXT, DISTANCE TEXT, MOB_NO1 TEXT, MOB_NO2 TEXT, MOB_NO3 TEXT, TEL_NO1 TEXT, TEL_NO2 TEXT, TEL_NO3 TEXT, IMG_URL1 TEXT, IMG_URL2 TEXT, IMG_URL3 TEXT, THUMB_URL1 TEXT, THUMB_URL2 TEXT, THUMB_URL3 TEXT, IS_OFFERED TEXT, TOTAL_LIKE TEXT, TOTAL_SHARE TEXT, LATITUDE TEXT, LONGITUDE TEXT, TYPE TEXT
    
	// Open the database from the users filessytem
	if(sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"select * from FEEDS where AREAID like \"%@\" COLLATE NOCASE", activeId];
        DebugLog(@"query--->%@",querySQL);
        const char *sqlStatement = [querySQL UTF8String];
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(shoplocalDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				// Read the data from the result row
                //int count = sqlite3_data_count(compiledStatement);
                //DebugLog(@"count=====>>%d",count);
				NSInteger lpostID = sqlite3_column_int(compiledStatement, 1);
				NSInteger lplaceID = sqlite3_column_int(compiledStatement, 2);
				NSString *lareaID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                NSString *ltitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
                NSString *ldescription = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];
                NSString *ldate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 6)];
                NSString *lofferdate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 7)];
                NSString *ldistance = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 8)];
                NSString *lmob_no1 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 9)];
				NSString *lmob_no2 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 10)];
				NSString *lmob_no3 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 11)];
                NSString *ltel_no1 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 12)];
                NSString *ltel_no2 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 13)];
                NSString *ltel_no3 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 14)];
                NSString *limage_url1 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 15)];
                NSString *limage_url2 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 16)];
                NSString *limage_url3 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 17)];
                NSString *lthumb_url1 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 18)];
                NSString *lthumb_url2 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 19)];
                NSString *lthumb_url3 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 20)];
                NSString *lis_offered = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 21)];
                NSString *ltotal_like = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 22)];
                NSString *ltotal_share = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 23)];
                NSString *llatitude = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 24)];
				NSString *llongitude = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 25)];
                NSString *ltype = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 26)];
                NSString *lname = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 27)];
                
				INFeedObj *tempfeedObj = [[INFeedObj alloc] init];
                tempfeedObj.postID = lpostID;
                tempfeedObj.placeID = lplaceID;
                tempfeedObj.areaID = lareaID;
                tempfeedObj.name = lname;
                tempfeedObj.title = ltitle;
                tempfeedObj.description = ldescription;
                tempfeedObj.date = ldate;
                tempfeedObj.offerdate = lofferdate;
                tempfeedObj.distance = ldistance;
                tempfeedObj.latitude = llatitude;
                tempfeedObj.longitude = llongitude;
                tempfeedObj.tel_no1 = ltel_no1;
                tempfeedObj.tel_no2 = ltel_no2;
                tempfeedObj.tel_no3 = ltel_no3;
                tempfeedObj.mob_no1 = lmob_no1;
                tempfeedObj.mob_no2 = lmob_no2;
                tempfeedObj.mob_no3 = lmob_no3;
                tempfeedObj.image_url1 = limage_url1;
                DebugLog(@"%@",tempfeedObj.image_url1);
                tempfeedObj.image_url2 = limage_url2;
                tempfeedObj.image_url3 = limage_url3;
                tempfeedObj.thumb_url1 = lthumb_url1;
                tempfeedObj.thumb_url2 = lthumb_url2;
                tempfeedObj.thumb_url3 =  lthumb_url3;
                tempfeedObj.total_like = ltotal_like;
                tempfeedObj.total_share = ltotal_share;
                tempfeedObj.is_offered = lis_offered;
                tempfeedObj.type = ltype;
				[feedsArray addObject:tempfeedObj];
				tempfeedObj = nil;
            }
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
		
	}
	sqlite3_close(shoplocalDB);
}

-(void)deleteFeedsTable
{
    NSString *activeId = [IN_APP_DELEGATE getActiveAreaIdFromAreaTable];
    
    sqlite3_stmt    *statement;
    
    DebugLog(@"path---->%@",[IN_APP_DELEGATE databasePath]);
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM FEEDS WHERE AREAID like \"%@\" COLLATE NOCASE", activeId];
        DebugLog(@"delete---->%@",deleteSQL);
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(shoplocalDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"All rows deleted");
        } else {
            DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
}

- (void)updateCurrentPage:(NSInteger)page
{
    NSString *activeId = [IN_APP_DELEGATE getActiveAreaIdFromAreaTable];
    
    sqlite3_stmt    *statement;
    int lastrowid;
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        NSString *updateSQL = [NSString stringWithFormat:@"UPDATE FEEDS_RECORD set CURRENT_PAGE=\"%d\" WHERE STORE like \"%@\" COLLATE NOCASE",page, activeId];
        DebugLog(@"%@",updateSQL);
        
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(shoplocalDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            lastrowid = sqlite3_last_insert_rowid(shoplocalDB);
            // DebugLog(@"inserted id========%d", lastrowid);
        } else {
            DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
}

- (void)updateTotalPage:(NSInteger)pageCount
{
    NSString *activeId = [IN_APP_DELEGATE getActiveAreaIdFromAreaTable];
    
    sqlite3_stmt    *statement;
    int lastrowid;
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        NSString *updateSQL = [NSString stringWithFormat:@"UPDATE FEEDS_RECORD set TOTAL_PAGE=\"%d\" WHERE STORE like \"%@\" COLLATE NOCASE",pageCount, activeId];
        DebugLog(@"%@",updateSQL);
        
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(shoplocalDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            lastrowid = sqlite3_last_insert_rowid(shoplocalDB);
            // DebugLog(@"inserted id========%d", lastrowid);
        } else {
            DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
}

- (void)updateTotalRecords:(NSInteger)recordsCount
{
    NSString *activeId = [IN_APP_DELEGATE getActiveAreaIdFromAreaTable];
    
    sqlite3_stmt    *statement;
    int lastrowid;
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        NSString *updateSQL = [NSString stringWithFormat:@"UPDATE FEEDS_RECORD set TOTAL_RECORD=\"%d\" WHERE STORE like \"%@\" COLLATE NOCASE",recordsCount, activeId];
        DebugLog(@"%@",updateSQL);
        
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(shoplocalDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            lastrowid = sqlite3_last_insert_rowid(shoplocalDB);
            // DebugLog(@"inserted id========%d", lastrowid);
        } else {
            DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
}

-(void)readRecordDatabase
{
    NSString *activeId = [IN_APP_DELEGATE getActiveAreaIdFromAreaTable];
    
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
	// Open the database from the users filessytem
	if(sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"select TOTAL_PAGE,TOTAL_RECORD,CURRENT_PAGE from FEEDS_RECORD WHERE STORE like \"%@\" COLLATE NOCASE", activeId];
        const char *sqlStatement = [querySQL UTF8String];
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(shoplocalDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				// Read the data from the result row
				self.totalPage = sqlite3_column_int(compiledStatement, 0);
				self.totalRecord = sqlite3_column_int(compiledStatement, 1);
                self.currentPage = sqlite3_column_int(compiledStatement, 2);
            }
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
		
	}
	sqlite3_close(shoplocalDB);
    DebugLog(@"total page %d- total record %d- current page %d",self.totalPage,self.totalRecord,self.currentPage);
}




#pragma AFNetworking delegate methods
-(void)sendPostLikeRequest
{
    DebugLog(@"========================sendPostLikeRequest========================");
    INFeedObj *tempfeedObj = (INFeedObj *)[feedsArray objectAtIndex:currentSelectedRow];
    
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[INUserDefaultOperations getCustomerAuthId] forKey:@"user_id"];
    [params setObject:[INUserDefaultOperations getCustomerAuthCode] forKey:@"auth_id"];
    [params setObject:[NSString stringWithFormat:@"%d",tempfeedObj.postID] forKey:@"post_id"];
    [httpClient postPath:LIKE_BROADCAST_OF_STORE parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            if([json  objectForKey:@"message"] != [NSNull null] && [[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
                //                if([json  objectForKey:@"message"] != [NSNull null] && ![[json objectForKey:@"message"] isEqualToString:@""]){
                //                    NSString *message = [json objectForKey:@"message"];
                //                    [INUserDefaultOperations showSIAlertView:message];
                //                }
                tempfeedObj.user_like = @"1";
                tempfeedObj.total_like = [NSString stringWithFormat:@"%d",[tempfeedObj.total_like intValue] + 1];
            } else {
                NSString* code = [json objectForKey:@"code"];
                if([code isEqualToString:CUSTOMER_ALREADY_MADE_POST_FAV])
                {
                    tempfeedObj.user_like = @"1";
                    if ([tempfeedObj.total_like intValue] == 0) {
                        tempfeedObj.total_like = @"1";
                    }
                    //                    if([json  objectForKey:@"message"] != [NSNull null] && ![[json objectForKey:@"message"] isEqualToString:@""]){
                    //                        NSString *message = [json objectForKey:@"message"];
                    //                        [INUserDefaultOperations showSIAlertView:message];
                    //                    }
                }
                else if([code isEqualToString:CUSTOMER_INVALID_USER_AUTH_CODE])
                {
                    if([INUserDefaultOperations isCustomerLoggedIn]){
                        [INUserDefaultOperations clearCustomerDetails];
                        NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:IN_CUSTOMER_LOGIN_LOGOUT_KEY];
                        [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_LOGIN_LOGOUT_NOTIFICATION object:nil userInfo:dictionary];
                        [self showLoginAlert];
                    }
                }else{
                    if([json  objectForKey:@"message"] != [NSNull null] && ![[json objectForKey:@"message"] isEqualToString:@""]){
                        NSString *message = [json objectForKey:@"message"];
                        [INUserDefaultOperations showSIAlertView:message];
                    }
                }
            }
            [feedsArray replaceObjectAtIndex:currentSelectedRow withObject:tempfeedObj];
            loadWithAnimation = NO;
            [feedsTableView reloadData];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}

-(void)sendPostUnLikeRequest
{
    INFeedObj *tempfeedObj = (INFeedObj *)[feedsArray objectAtIndex:currentSelectedRow];
    
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    [httpClient setDefaultHeader:@"user_id" value:[INUserDefaultOperations getCustomerAuthId]];
    [httpClient setDefaultHeader:@"auth_id" value:[INUserDefaultOperations getCustomerAuthCode]];
    [httpClient deletePath:UNLIKE_BROADCAST_OF_STORE(tempfeedObj.postID) parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            if([json  objectForKey:@"message"] != [NSNull null] && [[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
                //                NSString* message = [json objectForKey:@"message"];
                //                [INUserDefaultOperations showAlert:message];
                tempfeedObj.user_like = @"0";
                if ([tempfeedObj.total_like intValue] > 0) {
                    tempfeedObj.total_like = [NSString stringWithFormat:@"%d",[tempfeedObj.total_like intValue] - 1];
                }
                [feedsArray replaceObjectAtIndex:currentSelectedRow withObject:tempfeedObj];
                loadWithAnimation = NO;
                [feedsTableView reloadData];
            } else {
                NSString* code = [json objectForKey:@"code"];
                if([code isEqualToString:CUSTOMER_INVALID_USER_AUTH_CODE])
                {
                    if([INUserDefaultOperations isCustomerLoggedIn]){
                        [INUserDefaultOperations clearCustomerDetails];
                        NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:IN_CUSTOMER_LOGIN_LOGOUT_KEY];
                        [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_LOGIN_LOGOUT_NOTIFICATION object:nil userInfo:dictionary];
                        [self showLoginAlert];
                    }
                }else{
                    if([json  objectForKey:@"message"] != [NSNull null] && ![[json objectForKey:@"message"] isEqualToString:@""]){
                        NSString *message = [json objectForKey:@"message"];
                        [INUserDefaultOperations showSIAlertView:message];
                    }
                }
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}

-(void)sendPostShareRequest:(NSString *)viaString{
    DebugLog(@"shareMsg %@ ",viaString);
    INFeedObj *tempfeedObj = (INFeedObj *)[feedsArray objectAtIndex:currentSelectedRow];
    
    if ([[INUserDefaultOperations getCustomerLoginFrom] isEqualToString:FACEBOOKSERVER] && [INUserDefaultOperations isOpenGraphShareEnabled] && ![viaString isEqualToString:@"facebook"]) {
        SIAlertView *sialertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Do you want to share this on your Facebook timeline?"];
        sialertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
        sialertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
        [sialertView addButtonWithTitle:@"YES"
                                   type:SIAlertViewButtonTypeDestructive
                                handler:^(SIAlertView *alert) {
                                    DebugLog(@"YES");
                                    NSString *deepLinkingUrl = [FacebookOpenGraphAPI createDeepLinkWithActiveAreaName:[IN_APP_DELEGATE getActiveSlugFromAreaTable] storeName:tempfeedObj.name storeId:[NSString stringWithFormat:@"%d",tempfeedObj.placeID]];
                                    NSString *postTitle         = tempfeedObj.title;
                                    NSString *postDescription   = tempfeedObj.description;
                                    NSString *postimageUrl      = tempfeedObj.image_url1;
                                    if (postimageUrl != nil && ![postimageUrl isEqualToString:@""]) {
                                        if ([postimageUrl hasPrefix:@"http"]) {
                                            [FacebookOpenGraphAPI shareContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:postimageUrl deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
                                        }else{
                                            [FacebookOpenGraphAPI shareContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:THUMBNAIL_LINK(postimageUrl) deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
                                        }
                                    }else{
                                        [FacebookOpenGraphAPI shareContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:@"" deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
                                    }
                                }];
        [sialertView addButtonWithTitle:@"NOT NOW"
                                   type:SIAlertViewButtonTypeCancel
                                handler:^(SIAlertView *alert) {
                                    DebugLog(@"NO");
                                }];
        [sialertView show];
    }
    
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    //    if ([INUserDefaultOperations isCustomerLoggedIn]) {
    //        [params setObject:[INUserDefaultOperations getCustomerAuthId] forKey:@"user_id"];
    //        [params setObject:[INUserDefaultOperations getCustomerAuthCode] forKey:@"auth_id"];
    //    }
    [params setObject:[NSString stringWithFormat:@"%d",tempfeedObj.postID] forKey:@"post_id"];
    [params setObject:viaString forKey:@"via"];
    
    [httpClient postPath:SHARE_STORE_POST parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            if([json  objectForKey:@"message"] != [NSNull null] && [[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
                //                NSString* message = [json objectForKey:@"message"];
                //                [INUserDefaultOperations showAlert:message];
                tempfeedObj.total_share = [NSString stringWithFormat:@"%d",[tempfeedObj.total_share intValue] + 1];
                [feedsArray replaceObjectAtIndex:currentSelectedRow withObject:tempfeedObj];
                loadWithAnimation = NO;
                [feedsTableView reloadData];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}

-(void)showLoginAlert{
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Please Login to mark this post as favourite."];
    alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
    alertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
    
    [alertView addButtonWithTitle:@"LOGIN"
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alert) {
                              DebugLog(@"LOGIN Clicked");
                              [self showLoginModal];
                          }];
    [alertView addButtonWithTitle:@"NOT NOW"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alert) {
                              DebugLog(@"NOT NOW Clicked");
                          }];
    [alertView show];
}
- (void)likeBtnPressed:(id)sender event:(id)event {
    DebugLog(@"likeBtnPressed");
    if([IN_APP_DELEGATE networkavailable])
    {
        NSSet *touches = [event allTouches];
        UITouch *touch = [touches anyObject];
        CGPoint currentTouchPosition = [touch locationInView:self.feedsTableView];
        NSIndexPath *indexPath = [self.feedsTableView indexPathForRowAtPoint:currentTouchPosition];
        if (indexPath != nil) {
            if(![INUserDefaultOperations isCustomerLoggedIn])
            {
                [self showLoginAlert];
            } else {
                NSString *areaName = [IN_APP_DELEGATE getActivePlaceNameFromAreaTable];
                NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:@"OfferStream",@"Source",areaName,@"AreaName", nil];
                [INEventLogger logEvent:@"Offer_Like" withParams:params];
                
                currentSelectedRow = indexPath.row;
                INFeedObj *tempfeedObj = (INFeedObj *)[feedsArray objectAtIndex:indexPath.row];
                DebugLog(@"user_like %@ post_id %d",tempfeedObj.user_like,tempfeedObj.postID);
                if ([tempfeedObj.user_like intValue]) {
                    [self sendPostUnLikeRequest];
                }else{
                    [self sendPostLikeRequest];
//                    if ([[INUserDefaultOperations getCustomerLoginFrom] isEqualToString:FACEBOOKSERVER] && [INUserDefaultOperations isOpenGraphShareEnabled]) {
//                        SIAlertView *sialertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Do you want to share this on your Facebook timeline?"];
//                        sialertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
//                        sialertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
//                        [sialertView addButtonWithTitle:@"YES"
//                                                   type:SIAlertViewButtonTypeDestructive
//                                                handler:^(SIAlertView *alert) {
//                                                    DebugLog(@"YES");
//                                                    [self sendPostLikeRequest];
//
//                                                    NSString *deepLinkingUrl = [FacebookOpenGraphAPI createDeepLinkWithActiveAreaName:[IN_APP_DELEGATE getActiveSlugFromAreaTable] storeName:tempfeedObj.name storeId:[NSString stringWithFormat:@"%d",tempfeedObj.placeID]];
//                                                    NSString *postTitle         = tempfeedObj.title;
//                                                    NSString *postDescription   = tempfeedObj.description;
//                                                    NSString *postimageUrl      = tempfeedObj.image_url1;
//                                                    if (postimageUrl != nil && ![postimageUrl isEqualToString:@""]) {
//                                                        if ([postimageUrl hasPrefix:@"http"]) {
//                                                            [FacebookOpenGraphAPI likeContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:postimageUrl deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
//                                                        }else{
//                                                            [FacebookOpenGraphAPI likeContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:THUMBNAIL_LINK(postimageUrl) deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
//                                                        }
//                                                    }else{
//                                                        [FacebookOpenGraphAPI likeContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:@"" deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
//                                                    }
//                                                }];
//                        [sialertView addButtonWithTitle:@"NOT NOW"
//                                                   type:SIAlertViewButtonTypeCancel
//                                                handler:^(SIAlertView *alert) {
//                                                    DebugLog(@"NO");
//                                                    [self sendPostLikeRequest];
//
//                                                }];
//                        [sialertView show];
//                    }else{
//                        [self sendPostLikeRequest];
//                    }
                }
            }
        }
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

- (void)shareBtnPressed:(id)sender event:(id)event {
    DebugLog(@"shareBtnPressed");
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.feedsTableView];
    NSIndexPath *indexPath = [self.feedsTableView indexPathForRowAtPoint:currentTouchPosition];
    if (indexPath != nil) {
        currentSelectedRow = indexPath.row;
        UIActionSheet *shareActionSheet = [[UIActionSheet alloc] initWithTitle:@"Share via" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"SMS",@"Email",@"WhatsApp",@"Facebook",@"Twitter", nil];
        [shareActionSheet showInView:self.view];
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSString *title = [actionSheet buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"SMS"])
    {
        [self smsShareClicked];
    }
    else if([title isEqualToString:@"Email"])
    {
        [self emailShareClicked];
    }else if([title isEqualToString:@"WhatsApp"])
    {
        [self whatsAppShareClicked];
    }
    else if([title isEqualToString:@"Facebook"])
    {
        [self facebookShareClicked];
    } else if([title isEqualToString:@"Twitter"])
    {
        [self twitterShareClicked];
    }
    
    if(![title isEqualToString:@"Cancel"])
    {
        NSString *areaName = [IN_APP_DELEGATE getActivePlaceNameFromAreaTable];
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:@"OfferStream",@"Source",areaName,@"AreaName", nil];
        [INEventLogger logEvent:@"Offer_Share" withParams:params];
    }
}

-(void)smsShareClicked{
    if ([MFMessageComposeViewController canSendText]) {
        NSString *message = [self getPostDetailsMessageBody:@"sms"];
        
        MFMessageComposeViewController *messageComposer = [[MFMessageComposeViewController alloc] init];
        messageComposer.messageComposeDelegate = self;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            messageComposer.navigationBar.tintColor  = [UIColor whiteColor];
        }
        [messageComposer setBody:message];
        [messageComposer setRecipients:nil];
        [self presentViewController:messageComposer animated:YES completion:nil];
    }else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"You may not have set up sms service on your device.\nPlease check and try again."
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}
#pragma mark MFMessageComposeViewController Delegate Methods
-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
		case MessageComposeResultCancelled:
            DebugLog(@"SMS Cancelled");
			break;
		case MessageComposeResultFailed:
        {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Unknown Error\nSMS failed"
														   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
        }
			break;
		case MessageComposeResultSent:
        {
            DebugLog(@"SMS Sent");
        }
			break;
		default:
			break;
	}
    [self dismissViewControllerAnimated:YES completion:^{
        if (result == MessageComposeResultSent) {
            [self sendPostShareRequest:@"sms"];
        }
    }];
}

-(void)emailShareClicked{
    if ([MFMailComposeViewController canSendMail]) {
        INFeedObj *tempfeedObj = (INFeedObj *)[feedsArray objectAtIndex:currentSelectedRow];
        NSString *message = [self getPostDetailsMessageBody:@"email"];
        
        MFMailComposeViewController *emailComposer = [[MFMailComposeViewController alloc] init];
        emailComposer.mailComposeDelegate = self;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            emailComposer.navigationBar.tintColor  = [UIColor whiteColor];
        }
        [emailComposer setToRecipients:nil];
        if (tempfeedObj.name != nil && ![tempfeedObj.name isEqualToString:@""]) {
            [emailComposer setSubject:[NSString stringWithFormat:@"%@",tempfeedObj.name]];
        }else{
            [emailComposer setSubject:@""];
        }
        if (tempfeedObj.image_url1 != nil && ![tempfeedObj.image_url1 isEqualToString:@""]) {
            NSString *htmlMessage = [message stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
            DebugLog(@"%@--%@",message,htmlMessage);
            [emailComposer setMessageBody:[NSString stringWithFormat:@"%@ <br><br><img src=%@ alt=%@/>",htmlMessage,THUMBNAIL_LINK(tempfeedObj.image_url1),tempfeedObj.title] isHTML:YES];
        }else{
            [emailComposer setMessageBody:message isHTML:NO];
        }
        emailComposer.toolbar.tag = 2;
        [self presentViewController:emailComposer animated:YES completion:nil];
    }else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"You may not have set up mail service on your device or You may not connected to internet.\nPlease check and try again."
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}
#pragma mark Mail Compose Delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    if (result == MFMailComposeResultFailed) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                        message:NSLocalizedString(@"Email failed to send. Please try again.", nil)
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", nil) otherButtonTitles:nil];
		[alert show];
    }
    [self dismissViewControllerAnimated:YES completion:^{
        if (controller.toolbar.tag == 2 && result == MFMailComposeResultSent) {
            [self sendPostShareRequest:@"email"];
        }
    }];
}

-(void)whatsAppShareClicked{
    //    NSString *shareMSGBody = [[NSString stringWithFormat:@"Store Name: %@\nOffer: %@\nDescription: %@",storeName,postTitle,postDescription] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *message = [self getPostDetailsMessageBody:@"whatsapp"];
    NSString *urlString = [NSString stringWithFormat:@"whatsapp://send?text=%@",[message stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURL *whatsappURL = [NSURL URLWithString:urlString];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [[UIApplication sharedApplication] openURL: whatsappURL];
        [self sendPostShareRequest:@"whatsapp"];
    }
}

-(void)twitterShareClicked{
    if ([IN_APP_DELEGATE networkavailable]) {
        NSString *message = [self getPostDetailsMessageBody:@"twitter"];
        INFeedObj *tempfeedObj = (INFeedObj *)[feedsArray objectAtIndex:currentSelectedRow];
        NSString *postimageUrl = tempfeedObj.image_url1;
        
        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
            
            SLComposeViewController *socialComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
            if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
            {
                if ([message length] > 140) {
                    [socialComposer setInitialText:[message substringToIndex:140]];
                }else{
                    [socialComposer setInitialText:message];
                }
            }
            else{
                [socialComposer setInitialText:message];
            }
            if(postimageUrl != nil && ![postimageUrl isEqualToString:@""])
            {
                UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:THUMBNAIL_LINK(postimageUrl)]]];
                [socialComposer addImage:image];
            }
            [socialComposer setCompletionHandler:^(SLComposeViewControllerResult result){
                switch (result) {
                    case SLComposeViewControllerResultCancelled:
                        DebugLog(@"SLComposeViewControllerResultCancelled");
                        break;
                    case SLComposeViewControllerResultDone:
                        DebugLog(@"SLComposeViewControllerResultDone");
                        [self sendPostShareRequest:@"twitter"];
                        break;
                    default:
                        DebugLog(@"SLComposeViewControllerResultFailed");
                        break;
                }
                [self dismissViewControllerAnimated:YES completion:NULL];
            }];
            [self presentViewController:socialComposer animated:YES completion:Nil];
        }
        else{
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:ALERT_TITLE
                                      message:@"You may not have set up twitter service on your device.Please check and try again."
                                      delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [alertView show];
        }
    }else{
        [INUserDefaultOperations showOfflineAlert];
    }
}

-(void)facebookShareClicked{
    if ([IN_APP_DELEGATE networkavailable]) {
        [self sendPostShareRequest:@"facebook"];

        NSString *message = [self getPostDetailsMessageBody:@"facebook"];
        DebugLog(@"message %@",message);
        INFeedObj *tempfeedObj = (INFeedObj *)[feedsArray objectAtIndex:currentSelectedRow];
        NSString *postimageUrl = tempfeedObj.image_url1;
        NSString *storeName     = tempfeedObj.name;
        NSInteger placeId       = tempfeedObj.placeID;
        
        NSString *deepLinkingUrl = [FacebookOpenGraphAPI createDeepLinkWithActiveAreaName:[IN_APP_DELEGATE getActiveSlugFromAreaTable] storeName:storeName storeId:[NSString stringWithFormat:@"%d",placeId]];
        
        if (postimageUrl != nil && ![postimageUrl isEqualToString:@""]) {
            if ([postimageUrl hasPrefix:@"http"]) {
                [FacebookShareAPI shareContentWithTitle:[NSString stringWithFormat:@"%@ - Offer\n",storeName] description:message image:postimageUrl deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
            }else{
                [FacebookShareAPI shareContentWithTitle:[NSString stringWithFormat:@"%@ - Offer\n",storeName] description:message image:THUMBNAIL_LINK(postimageUrl) deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
            }
        }else{
            [FacebookShareAPI shareContentWithTitle:[NSString stringWithFormat:@"%@ - Offer\n",storeName] description:message image:@"" deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
        }
    }else{
        [INUserDefaultOperations showOfflineAlert];
    }
//    if ([IN_APP_DELEGATE networkavailable]) {
//        NSString *message = [self getPostDetailsMessageBody:@"facebook"];
//        INFeedObj *tempfeedObj = (INFeedObj *)[feedsArray objectAtIndex:currentSelectedRow];
//        NSString *postimageUrl = tempfeedObj.image_url1;
//        
//        if (SYSTEM_VERSION_LESS_THAN(@"6.0"))
//        {
//            FaceBookShareViewController *facebookViewComposer = [[FaceBookShareViewController alloc] initWithNibName:@"FaceBookShareViewController" bundle:nil];
//            facebookViewComposer.fbdelegate = self;
//            facebookViewComposer.title = @"FACEBOOK SHARE";
//            facebookViewComposer.FBtitle = ALERT_TITLE;
//            facebookViewComposer.postMessageText = message;
//            facebookViewComposer.FBtPic = THUMBNAIL_LINK(postimageUrl);
//            UINavigationController *fbnavBar=[[UINavigationController alloc]initWithRootViewController:facebookViewComposer];
//            [self.navigationController presentViewController:fbnavBar animated:YES completion:nil];
//        }else{
//            if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
//                
//                SLComposeViewController *socialComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
//                [socialComposer setInitialText:message];
//                if(postimageUrl != nil && ![postimageUrl isEqualToString:@""])
//                {
//                    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:THUMBNAIL_LINK(postimageUrl)]]];
//                    [socialComposer addImage:image];
//                }
//                [socialComposer setCompletionHandler:^(SLComposeViewControllerResult result){
//                    switch (result) {
//                        case SLComposeViewControllerResultCancelled:
//                            DebugLog(@"SLComposeViewControllerResultCancelled");
//                            break;
//                        case SLComposeViewControllerResultDone:
//                            DebugLog(@"SLComposeViewControllerResultDone");
//                            [self sendPostShareRequest:@"facebook"];
//                            break;
//                        default:
//                            DebugLog(@"SLComposeViewControllerResultFailed");
//                            break;
//                    }
//                    [self dismissViewControllerAnimated:YES completion:NULL];
//                }];
//                [self presentViewController:socialComposer animated:YES completion:Nil];
//            }
//            else{
//                UIAlertView *alertView = [[UIAlertView alloc]
//                                          initWithTitle:ALERT_TITLE
//                                          message:@"You may not have set up facebook service on your device.Please check and try again."
//                                          delegate:nil
//                                          cancelButtonTitle:@"OK"
//                                          otherButtonTitles:nil];
//                [alertView show];
//            }
//        }
//    }else{
//        [INUserDefaultOperations showOfflineAlert];
//    }
}

-(void)faceBookCompletionCallBack:(int)result
{
    DebugLog(@"Customer : faceBookCompletionCallBack %d",result);
    if (result == FACEBOOK_POST_SUCCESS) {
        [self sendPostShareRequest:@"facebook"];
    }
}

-(NSString *)getPostDetailsMessageBody:(NSString *)shareVia{
    INFeedObj *tempfeedObj = (INFeedObj *)[feedsArray objectAtIndex:currentSelectedRow];
    
    NSMutableString *contactString = [[NSMutableString alloc] initWithString:@""];
    if (tempfeedObj.mob_no1 != nil && ![tempfeedObj.mob_no1 isEqualToString:@""]) {
        [contactString appendString:[NSString stringWithFormat:@"%@",tempfeedObj.mob_no1]];
    }
    if (tempfeedObj.mob_no2 != nil && ![tempfeedObj.mob_no2 isEqualToString:@""]) {
        if ([contactString isEqualToString:@""]) {
            [contactString appendString:tempfeedObj.mob_no2];
        }else{
            [contactString appendString:[NSString stringWithFormat:@", %@", tempfeedObj.mob_no2]];
        }
    }
    if (tempfeedObj.mob_no3 != nil && ![tempfeedObj.mob_no3 isEqualToString:@""]) {
        if ([contactString isEqualToString:@""]) {
            [contactString appendString:tempfeedObj.mob_no3];
        }else{
            [contactString appendString:[NSString stringWithFormat:@", %@", tempfeedObj.mob_no3]];
        }
    }
    if (tempfeedObj.tel_no1 != nil && ![tempfeedObj.tel_no1 isEqualToString:@""]) {
        if ([contactString isEqualToString:@""]) {
            [contactString appendString:tempfeedObj.tel_no1];
        }else{
            [contactString appendString:[NSString stringWithFormat:@", %@", tempfeedObj.tel_no1]];
        }
    }
    if (tempfeedObj.tel_no2 != nil && ![tempfeedObj.tel_no2 isEqualToString:@""]) {
        if ([contactString isEqualToString:@""]) {
            [contactString appendString:tempfeedObj.tel_no2];
        }else{
            [contactString appendString:[NSString stringWithFormat:@", %@", tempfeedObj.tel_no2]];
        }
    }
    if (tempfeedObj.tel_no3 != nil && ![tempfeedObj.tel_no3 isEqualToString:@""]) {
        if ([contactString isEqualToString:@""]) {
            [contactString appendString:tempfeedObj.tel_no3];
        }else{
            [contactString appendString:[NSString stringWithFormat:@", %@", tempfeedObj.tel_no3]];
        }
    }
    
    NSString *message = [IN_APP_DELEGATE getPostDetailsMessageBody:tempfeedObj.name offerTitle:tempfeedObj.title offerDescription:tempfeedObj.description isOffered:tempfeedObj.is_offered offerDateTime:tempfeedObj.offerdate contact:contactString shareVia:shareVia];
    return message;
}

- (void)showAppRateAlert {
    if(([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) && (self.isViewLoaded && self.view.window))
    {
        SIAlertView *sialertView = [[SIAlertView alloc] initWithTitle:@"Rate Shoplocal" andMessage:@"Dear user, \nPlease take a moment to rate this application. Thanks!"];
        sialertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
        sialertView.backgroundStyle = SIAlertViewButtonTypeCancel;
        [sialertView addButtonWithTitle:@"Remind me later"
                                   type:SIAlertViewButtonTypeDefault
                                handler:^(SIAlertView *alert) {
                                    [INUserDefaultOperations setAppTotalUsageTime:0];
                                    [INUserDefaultOperations setAppRaiterState:TRUE];
                                }];
        [sialertView addButtonWithTitle:@"Rate Now"
                                   type:SIAlertViewButtonTypeDestructive
                                handler:^(SIAlertView *alert) {
                                    [self openAppStoreLink];
                                    [INUserDefaultOperations setAppRaiterState:FALSE];
                                }];
        
//        [sialertView addButtonWithTitle:@"Never"
//                                   type:SIAlertViewButtonTypeCancel
//                                handler:^(SIAlertView *alert) {
//                                    [INUserDefaultOperations setAppRaiterState:FALSE];
//                                }];
        [sialertView show];
    }
}

-(void)openAppStoreLink
{
    if(NSClassFromString(@"SKStoreProductViewController")) { // Checks for iOS 6 feature.
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id768745585?at=10l6dK"]];
        } else {
            // Initialize Product View Controller
            SKStoreProductViewController *storeProductViewController = [[SKStoreProductViewController alloc] init];
            
            // Configure View Controller
            [storeProductViewController setDelegate:self];
            [storeProductViewController loadProductWithParameters:@{SKStoreProductParameterITunesItemIdentifier : @"768745585"} completionBlock:^(BOOL result, NSError *error) {
                if (error) {
                    DebugLog(@"Error %@ with User Info %@.", error, [error userInfo]);
                    
                } else {
                    // Present Store Product View Controller
                    [self presentViewController:storeProductViewController animated:YES completion:nil];
                }
            }];
        }
    } else { // Before iOS 6, we can only open the URL
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms://itunes.com/apps/shoplocalindia?at=10l6dK"]];
    }
}

#pragma mark -
#pragma mark Store Product View Controller Delegate Methods
- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidUnload {
    [self setFeedsTableView:nil];
    [self setCountlbl:nil];
    [self setCountHeaderView:nil];
    [self setLblpullToRefresh:nil];
    [self setGlobalSearchBar:nil];
    [super viewDidUnload];
}

- (void) dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(NSIndexPath *)getIndexpathOfEvent:(id)event
{
    NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:feedsTableView];
	NSIndexPath *indexPath = [feedsTableView indexPathForRowAtPoint: currentTouchPosition];
    return indexPath;
}

- (void)callBtnTapped:(id)sender event:(id)event
{
    NSIndexPath *indexPath = [self getIndexpathOfEvent:event];
    if (indexPath != nil)
    {
        INFeedObj *lfeedObj  = [feedsArray objectAtIndex:indexPath.row];
        DebugLog(@"place %@",lfeedObj.name);
        if (lfeedObj.name == nil || [lfeedObj.name length] == 0)
        {
            lblcallModalHeader.text =  @"Select number";
             selectedPlaceName = @"";
        }
        else {
            lblcallModalHeader.text = lfeedObj.name;
            selectedPlaceName = lfeedObj.name;
        }
        
        selectedPostId = lfeedObj.postID;
        if (telArray == nil) {
            telArray = [[NSMutableArray alloc] init];
        }else{
            [telArray removeAllObjects];
        }
        if (lfeedObj.tel_no1 != nil && ![lfeedObj.tel_no1 isEqualToString:@""]) {
            [telArray addObject:lfeedObj.tel_no1];
        }
        if (lfeedObj.tel_no2 != nil && ![lfeedObj.tel_no2 isEqualToString:@""]) {
            [telArray addObject:lfeedObj.tel_no2];
        }
        if (lfeedObj.tel_no3 != nil && ![lfeedObj.tel_no3 isEqualToString:@""]) {
            [telArray addObject:lfeedObj.tel_no3];
        }
        if (lfeedObj.mob_no1 != nil && ![lfeedObj.mob_no1 isEqualToString:@""]) {
            [telArray addObject:[NSString stringWithFormat:@"+%@",lfeedObj.mob_no1]];
        }
        if (lfeedObj.mob_no2 != nil && ![lfeedObj.mob_no2 isEqualToString:@""]) {
            [telArray addObject:[NSString stringWithFormat:@"+%@",lfeedObj.mob_no2]];
        }
        if (lfeedObj.mob_no3 != nil && ![lfeedObj.mob_no3 isEqualToString:@""]) {
            [telArray addObject:[NSString stringWithFormat:@"+%@",lfeedObj.mob_no3]];
        }
        [callModalTableView reloadData];
        DebugLog(@"callModalTableView.contentSize.height %f",callModalTableView.contentSize.height);
        if (callModalTableView.contentSize.height < 240) {
            [callModalTableView setFrame:CGRectMake(callModalTableView.frame.origin.x, callModalTableView.frame.origin.y, callModalTableView.frame.size.width, callModalTableView.contentSize.height)];
        }else{
            [callModalTableView setFrame:CGRectMake(callModalTableView.frame.origin.x, callModalTableView.frame.origin.y, callModalTableView.frame.size.width, 240)];
        }
        [callModalView setFrame:CGRectMake(callModalView.frame.origin.x, callModalView.frame.origin.y, callModalView.frame.size.width, callModalTableView.frame.size.height+85)];
        DebugLog(@"callModalTableView %f",callModalTableView.frame.size.height);
        DebugLog(@"callModalView.height %f",callModalView.frame.size.height);

        DebugLog(@"temparray %@",telArray);
        if (telArray != nil && telArray.count > 0) {
            [callModalViewBackView setHidden:NO];
            [callModalView setHidden:NO];
            [CommonCallback viewtransitionInCompletion:callModalView completion:^{
                [CommonCallback viewtransitionOutCompletion:callModalView completion:nil];
            }];
            
            NSDictionary *callParams = [NSDictionary dictionaryWithObjectsAndKeys:@"OfferStream",@"Source",[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"AreaName",selectedPlaceName,@"StoreName", nil];
            [INEventLogger logEvent:@"StoreCall" withParams:callParams];
            
            [self sendStoreCallRequest:@"StoreCall"];
        } else {
            [INUserDefaultOperations showSIAlertView:NO_CONTACT_MESSAGE];
        }
    }
}

- (IBAction)btnCancelcallModalPressed:(id)sender {
    [callModalView setHidden:YES];
    [callModalViewBackView setHidden:YES];
}

-(void)sendStoreCallRequest:(NSString *)callType
{
    
    if (![INUserDefaultOperations isCustomerLoggedIn]) {
        return;
    }
    
    NSString *areaID = [IN_APP_DELEGATE getActiveShoplocalPlaceIdFromAreaTable];
    
    DebugLog(@"========================sendStoreCallRequest========================");
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSString *register_from = [NSString stringWithFormat:@"%@ v%@",DEVICE_TYPE,version];
    
    NSMutableDictionary *postparams = [[NSMutableDictionary alloc] init];
    //    {"user_id":"${user_id}","auth_id":"${auth_id}","place_id":"106","via":"welcome ","call_type":"type-a","active_area":"1"}
    [postparams setObject:[INUserDefaultOperations getCustomerAuthId] forKey:@"user_id"];
    [postparams setObject:[INUserDefaultOperations getCustomerAuthCode] forKey:@"auth_id"];
    [postparams setObject:[NSString stringWithFormat:@"%d",selectedPostId] forKey:@"post_id"];
    [postparams setObject:callType forKey:@"call_type"];
    [postparams setObject:areaID forKey:@"active_area"];
    [postparams setObject:register_from forKey:@"via"];
    
    DebugLog(@"sendStoreCallRequest : postParams -%@-",postparams);
    
    [httpClient postPath:CALL_STORE parameters:postparams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"sendStoreCallRequest : json afnetworking ->%@",json);
        if (json!= nil) {
            NSString* code = [json objectForKey:@"code"];
            if([code isEqualToString:CUSTOMER_INVALID_USER_AUTH_CODE])
            {
                if([INUserDefaultOperations isCustomerLoggedIn]){
                    [INUserDefaultOperations clearCustomerDetails];
                }
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"sendStoreCallRequest : [HTTPClient Error]: %@", error.localizedDescription);
    }];
}
@end
