//
//  INMerchantReportsViewController.h
//  inorbit
//
//  Created by Kirti Nikam on 05/09/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@class INBroadcastDetailObj;
@interface INMerchantReportsViewController : UIViewController
{
        INBroadcastDetailObj *broadcastObj;
}
@property (nonatomic, readwrite) NSInteger placeId;
@property (nonatomic, copy) NSString *storeName;

@property (strong, nonatomic) NSMutableArray *broadcastArray;
@property (strong, nonatomic) IBOutlet UIView *reportView;
@property (strong, nonatomic) IBOutlet UILabel *lblCustomerCount;
@property (strong, nonatomic) IBOutlet UILabel *lblCustomerCountHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblViewsCount;
@property (strong, nonatomic) IBOutlet UILabel *lblViewsCountHeader;

@property (strong, nonatomic) IBOutlet UILabel *lblLikesCount;
@property (strong, nonatomic) IBOutlet UILabel *lblLikesCountHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblShareCount;
@property (strong, nonatomic) IBOutlet UILabel *lblShareCountHeader;
@property (strong, nonatomic) IBOutlet UIButton *infoBtn;

@property (strong, nonatomic) IBOutlet UIButton *postDetailReportBtn;

- (IBAction)postDetailReportBtnPressed:(id)sender;
- (IBAction)infoBtnPressed:(id)sender;
@end
