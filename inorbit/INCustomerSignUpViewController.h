//
//  INCustomerSignUpViewController.h
//  shoplocal
//
//  Created by Rishi on 20/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SIAlertView.h"

@protocol INCustomerSignUPViewControllerDelegate <NSObject>
- (void)loginUserWithIdAndPassword:(NSString *)lnumber pass:(NSString *)lpass;
@end

@interface INCustomerSignUpViewController : UIViewController <UIScrollViewDelegate>
{
    BOOL pageControlUsed;
    NSURLConnection *connectionRegisterNumber;
    NSURLConnection *connectionisVerifiedNumber;
    NSURLConnection *connectionsetPassword;
    NSURLConnection *connectionsetPasswordWithCode;
    NSURLConnection *connectiongetVerifyCode;
    NSURLConnection *connectionDleteStore;
    NSMutableData *responseAsyncData;
    int status;
    MBProgressHUD *hud;
    SIAlertView *alertView;
    NSDate *registerDate;
}
@property (nonatomic, weak) id<INCustomerSignUPViewControllerDelegate> logindelegate;
@property (readwrite, nonatomic) NSInteger screenType;
@property (strong, nonatomic) IBOutlet UIScrollView *signupScrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UITextField *numberTextField;
@property (strong, nonatomic) IBOutlet UITextField *verrificationTextField;
@property (strong, nonatomic) IBOutlet UILabel *messagelbl;
@property (strong, nonatomic) IBOutlet UITextField *retypepasswordTextField;
@property (strong, nonatomic) IBOutlet UILabel *phoneNumberScreenlbl;
@property (strong, nonatomic) IBOutlet UIButton *nextBtn;
@property (strong, nonatomic) IBOutlet UIButton *resendBtn;
@property (strong, nonatomic) IBOutlet UILabel *registerstatuslbl;
@property (nonatomic, copy) NSString *codeNumber;
@property (nonatomic, copy) NSString *mobileNumber;
@property (nonatomic, copy) NSString *callNumber;
@property (strong, nonatomic) IBOutlet UIView *confirmView;
@property (strong, nonatomic) IBOutlet UILabel *numberconfirmlbl;
@property (strong, nonatomic) IBOutlet UITextField *confirmnumberTextField;
@property (strong, nonatomic) IBOutlet UIButton *confirmProceedBtn;
@property (strong, nonatomic) IBOutlet UIButton *confirmCancelBtn;
@property (strong, nonatomic) IBOutlet UIButton *questionBtn;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttonsCollection;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labelsCollection;
@property (strong,nonatomic) NSMutableArray *countryCodeArray;
@property (strong,nonatomic) NSMutableArray *countryCodePickerArray;
@property (strong, nonatomic) IBOutlet UITextField *codeTextField;
@property (strong, nonatomic) IBOutlet UILabel *codelbl;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView1;
@property (strong, nonatomic) IBOutlet UIView *backView1;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView3;
@property (strong, nonatomic) IBOutlet UIView *backView3;
@property (strong, nonatomic) IBOutlet UILabel *resendsmslbl;
@property (strong, nonatomic) IBOutlet UIButton *privacyBtn;
@property (strong, nonatomic) IBOutlet UIButton *termsBtn;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *signupIndicatior;
@property (strong, nonatomic) IBOutlet UILabel *callscrlbl;
@property (strong, nonatomic) IBOutlet UIButton *proceedafterCallBtn;
@property (strong, nonatomic) IBOutlet UIButton *callBtn;

- (IBAction)pageChange:(id)sender;
- (IBAction)nextBntPressed:(id)sender;
- (IBAction)callBtnPressed:(id)sender;
- (IBAction)proceedBtnPressed:(id)sender;
- (IBAction)resendBtnPressed:(id)sender;
- (IBAction)termsBtnPressed:(id)sender;
- (IBAction)confirmNumberBtnPressed:(id)sender;
- (IBAction)confirmCancelBtnPressed:(id)sender;
- (IBAction)questionBtnPressed:(id)sender;
- (IBAction)privacyBtnPressed:(id)sender;
- (IBAction)editNumberBtnPressed:(id)sender;
- (IBAction)proceedafterCallBtnPressed:(id)sender;
@end
