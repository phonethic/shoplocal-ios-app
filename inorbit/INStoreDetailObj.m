//
//  INStoreDetailObj.m
//  shoplocal
//
//  Created by Sagar Mody on 11/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INStoreDetailObj.h"

@implementation INStoreDetailObj
@synthesize ID;
@synthesize name;
@synthesize place_parent;
@synthesize place_status;
@synthesize published;
@synthesize description;
@synthesize building;
@synthesize street;
@synthesize landmark;
@synthesize area;
@synthesize pincode;
@synthesize city;
@synthesize state;
@synthesize country;
@synthesize latitude;
@synthesize longitude;
@synthesize tel_no1;
@synthesize tel_no2;
@synthesize tel_no3;
@synthesize mob_no1;
@synthesize mob_no2;
@synthesize mob_no3;
@synthesize fax_no1;
@synthesize fax_no2;
@synthesize toll_no1;
@synthesize toll_no2;
@synthesize open_time;
@synthesize close_time;
@synthesize image_url;
@synthesize email;
@synthesize website;
@synthesize total_like;
@synthesize user_like;
@synthesize total_share;
@synthesize total_view;
@synthesize fb_url;
@synthesize twitter_url;
@synthesize store_close_days;
@end
