//
//  ImageFilterViewController.h
//  shoplocal
//
//  Created by Kirti Nikam on 30/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "NLImageCropperView.h"
#import "BJImageCropper.h"

@protocol CropImageFilterViewControllerDelegate <NSObject>
@required
-(void)saveImageToPhotoAlbum:(UIImage *)image;
@end

@interface ImageFilterViewController : UIViewController
{
    int rotationInt;

//    NLImageCropperView* _imageCropper;
    BJImageCropper *imageCropper;
    UIImage *selectedImage, *thumbImage;
    UIImage *minithumbImage;
    
}
@property (nonatomic, strong) BJImageCropper *imageCropper;

@property(nonatomic,assign) id<CropImageFilterViewControllerDelegate> delegate;
@property (nonatomic,strong) UIImage *originalImage;

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIButton *saveBtn;
@property (strong, nonatomic) IBOutlet UIButton *cancelBtn;
@property (strong, nonatomic) IBOutlet UIButton *antiClockWiseRotateBtn;
@property (strong, nonatomic) IBOutlet UIButton *clockWiseRotateBtn;
@property (strong, nonatomic) IBOutlet UIButton *cropBtn;
@property (strong, nonatomic) IBOutlet UIView *toolBarView;
@property (strong, nonatomic) IBOutlet UITableView *filterTableView;
@property (retain, nonatomic) NSMutableArray *arrEffects;
@property (strong, nonatomic) IBOutlet UIImageView *filterImageView;
@property (strong, nonatomic) IBOutlet UIView *filterToolView;

@property (strong, nonatomic) IBOutlet UIButton *saveImageWithFilterBtn;
@property (strong, nonatomic) IBOutlet UIButton *cancelBtn1;

- (IBAction)saveBtnClicked:(id)sender;
- (IBAction)cancelBtnClicked:(id)sender;
- (IBAction)antiClockWiseRotateBtnClicked:(id)sender;
- (IBAction)clockWiseRotateBtnClicked:(id)sender;
- (IBAction)cropBtnClicked:(id)sender;


- (IBAction)saveImageWithFilterBtnPressed:(id)sender;
- (IBAction)cancelBtn1Pressed:(id)sender;
@end
