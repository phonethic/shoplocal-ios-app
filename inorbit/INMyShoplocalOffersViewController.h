//
//  INMyShoplocalOffersViewController.h
//  shoplocal
//
//  Created by Rishi on 07/03/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "INCustomerLoginViewController.h"
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import "FaceBookShareViewController.h"

@class INFeedObj;
@interface INMyShoplocalOffersViewController : UIViewController<INCustomerLoginViewControllerDelegate,UISearchBarDelegate,UIActionSheetDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,FacebookProtocolDelegate>
{
    INFeedObj *feedObj;
    BOOL isDragging;
    BOOL isLoading;
    BOOL isAdding;
    
    double lastContentOffset;
}
@property (strong, nonatomic) IBOutlet UIView *countHeaderView;
@property (strong, nonatomic) IBOutlet UILabel *lblpullToRefresh;
@property (strong, nonatomic) IBOutlet UILabel *countlbl;
@property (strong, nonatomic) IBOutlet UITableView *myoffersTableView;
@property (strong, nonatomic) NSMutableArray *myofferArray;
@property (nonatomic, retain) UIView *refreshHeaderView;
@property (nonatomic, retain) UILabel *refreshLabel;
@property (nonatomic, retain) UIImageView *refreshArrow;
@property (nonatomic, retain) UIActivityIndicatorView *refreshSpinner;
@property (readwrite, nonatomic) NSInteger totalPage;
@property (readwrite, nonatomic) NSInteger totalRecord;
@property (readwrite, nonatomic) NSInteger currentPage;

//*****Pull TO Refresh
@property (nonatomic, copy) NSString *textPull;
@property (nonatomic, copy) NSString *textRelease;
@property (nonatomic, copy) NSString *textLoading;

@property (strong, nonatomic) IBOutlet UIView *loginView;
@property (strong, nonatomic) IBOutlet UIButton *loginBtn;
@property (strong, nonatomic) IBOutlet UILabel *lblLoginText;

@property (strong, nonatomic) IBOutlet UISearchBar *globalSearchBar;

@property (nonatomic, readwrite) NSInteger currentSelectedRow;


@property (strong, nonatomic) IBOutlet UIButton *showStoreViewBtn;
@property (strong, nonatomic) IBOutlet UIButton *showOffersBtn;

@property (strong, nonatomic) IBOutlet UIButton *favStoreBtn;

- (IBAction)favStoreBtnPressed:(id)sender;
- (IBAction)loginBtnPressed:(id)sender;
- (IBAction)showStoreViewBtnPressed:(id)sender;
- (IBAction)showOffersBtnPressed:(id)sender;

@property (strong, nonatomic) NSMutableArray *telArray;
@property (readwrite, nonatomic) NSInteger selectedPostId;
@property (copy, nonatomic) NSString* selectedPlaceName;

@property (strong, nonatomic) IBOutlet UIView *callModalViewBackView;
@property (strong, nonatomic) IBOutlet UIView *callModalView;
@property (strong, nonatomic) IBOutlet UILabel *lblcallModalHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblcallModalHeaderSeperator;
@property (strong, nonatomic) IBOutlet UITableView *callModalTableView;
@property (strong, nonatomic) IBOutlet UIButton *btnCancelcallModal;
- (IBAction)btnCancelcallModalPressed:(id)sender;

@end
