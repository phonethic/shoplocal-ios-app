//
//  FacebookShareAPI.m
//  shoplocal
//
//  Created by Kirti Nikam on 08/05/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import "FacebookShareAPI.h"
#import <FacebookSDK/FacebookSDK.h>
#import "constants.h"

#define LOGO_PICTURE [NSString stringWithFormat:@"%@%@assets/images/icons/shoplocal-logo.png",LIVE_SERVER,URL_PREFIX]
#define THUMBNAIL_LINK(IMG_LINK) [NSString stringWithFormat:@"%@%@%@",LIVE_SERVER,URL_PREFIX,IMG_LINK]

@implementation FacebookShareAPI

+(void)shareContentWithTitle:(NSString *)ltitle description:(NSString *)ldescription image:(NSString *)limage deeplinkingURL:(NSString *)lurl objectType:(NSString *)objectType;
{
    
//link: the url we want to share.
//name: a title.
//caption: a subtitle.
//picture: the url of a thumbnail to associate with the post.
//description: a snippet of text describing the content of the link.
    
    // Check if the Facebook app is installed and we can present the share dialog
    FBLinkShareParams *params   = [[FBLinkShareParams alloc] init];
    if ([limage length] > 0) {
        params.picture              = [NSURL URLWithString:limage];
    }else{
        params.picture              = [NSURL URLWithString:LOGO_PICTURE];
    }
    if ([lurl length] > 0) {
        params.link                 = [NSURL URLWithString:lurl];
    }
    
    if ([ltitle length] > 0) {
        params.name              = ltitle;
    }
    if ([ldescription length] > 0) {
        params.description          = ldescription;
    }
    
    // If the Facebook app is installed and we can present the share dialog
    if ([FBDialogs canPresentShareDialogWithParams:params]) {
        
        // Present share dialog
        [FBDialogs presentShareDialogWithParams:params
                                    clientState:nil
                                        handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                              if(error) {
                                                  // An error occurred, we need to handle the error
                                                  // See: https://developers.facebook.com/docs/ios/errors
                                                  DebugLog(@"Error publishing story: %@", error.description);
                                              } else {
                                                  // Success
                                                  DebugLog(@"result %@", results);
                                                  //[FacebookShareAPI callFacebookOpenGraphAPIWithTitle:ltitle description:ldescription image:limage deeplinkingURL:lurl objectType:objectType];
                                              }
                                        }];
//        [FBDialogs presentShareDialogWithLink:params.link
//                                      handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
//                                          if(error) {
//                                              // An error occurred, we need to handle the error
//                                              // See: https://developers.facebook.com/docs/ios/errors
//                                              DebugLog(@"Error publishing story: %@", error.description);
//                                          } else {
//                                              // Success
//                                              DebugLog(@"result %@", results);
//                                          }
//                                      }];
        
        // If the Facebook app is NOT installed and we can't present the share dialog
    } else {
        // FALLBACK: publish just a link using the Feed dialog
        
        // Put together the dialog parameters
        NSString *imageURL;
        if ([limage length] > 0) {
            imageURL             = limage;
        }else{
            imageURL             = LOGO_PICTURE;
        }
        
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       ltitle, @"name",
//                                       ltitle, @"caption",
                                       ldescription, @"description",
                                       lurl, @"link",
                                       imageURL, @"picture",
                                       nil];
        
        // Show the feed dialog
        [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                               parameters:params
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                      if (error) {
                                                          // An error occurred, we need to handle the error
                                                          // See: https://developers.facebook.com/docs/ios/errors
                                                          DebugLog(@"Error publishing story: %@", error.description);
                                                      } else {
                                                          if (result == FBWebDialogResultDialogNotCompleted) {
                                                              // User canceled.
                                                              DebugLog(@"User cancelled.");
                                                          } else {
                                                              // Handle the publish feed callback
                                                              NSDictionary *urlParams = [FacebookShareAPI parseURLParams:[resultURL query]];
                                                              
                                                              if (![urlParams valueForKey:@"post_id"]) {
                                                                  // User canceled.
                                                                  DebugLog(@"User cancelled.");
                                                                  
                                                              } else {
                                                                  // User clicked the Share button
                                                                  NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
                                                                  DebugLog(@"result %@", result);
                                                                  //[FacebookShareAPI callFacebookOpenGraphAPIWithTitle:ltitle description:ldescription image:limage deeplinkingURL:lurl objectType:objectType];
                                                              }
                                                          }
                                                      }
                                                  }];
    }
}


+(void)shareContentWithTitle:(NSString *)ltitle subTitle:(NSString *)lsubTitle description:(NSString *)ldescription image:(NSString *)limage deeplinkingURL:(NSString *)lurl objectType:(NSString *)objectType;
{
    
    //link: the url we want to share.
    //name: a title.
    //caption: a subtitle.
    //picture: the url of a thumbnail to associate with the post.
    //description: a snippet of text describing the content of the link.
    
    // Check if the Facebook app is installed and we can present the share dialog
    FBLinkShareParams *params   = [[FBLinkShareParams alloc] init];
    if ([limage length] > 0) {
        params.picture              = [NSURL URLWithString:limage];
    }else{
        params.picture              = [NSURL URLWithString:LOGO_PICTURE];
    }
    
    if ([lurl length] > 0) {
        params.link                 = [NSURL URLWithString:lurl];
    }
    
    if ([ltitle length] > 0) {
        params.name                 = ltitle;
        if ([lsubTitle length] > 0) {
            params.caption          = lsubTitle;
        }
    }else{
        params.name                 = lsubTitle;
    }
    
    if ([ldescription length] > 0) {
        params.description          = ldescription;
    }

    // If the Facebook app is installed and we can present the share dialog
    if ([FBDialogs canPresentShareDialogWithParams:params]) {
        
        // Present share dialog
        [FBDialogs presentShareDialogWithParams:params
                                    clientState:nil
                                        handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                            if(error) {
                                                // An error occurred, we need to handle the error
                                                // See: https://developers.facebook.com/docs/ios/errors
                                                DebugLog(@"Error publishing story: %@", error.description);
                                            } else {
                                                // Success
                                                DebugLog(@"result %@", results);
                                                [FacebookShareAPI callFacebookOpenGraphAPIWithTitle:ltitle description:ldescription image:limage deeplinkingURL:lurl objectType:objectType];
                                            }
                                        }];
        // If the Facebook app is NOT installed and we can't present the share dialog
    } else {
        // FALLBACK: publish just a link using the Feed dialog
        
        // Put together the dialog parameters
        NSString *imageURL;
        if ([limage length] > 0) {
            imageURL             = limage;
        }else{
            imageURL             = LOGO_PICTURE;
        }
        
        NSMutableDictionary *params;
        if ([ltitle length] > 0) {
            params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                      ltitle, @"name",
                      lsubTitle, @"caption",
                      ldescription, @"description",
                      lurl, @"link",
                      imageURL, @"picture",
                      nil];
        }else{
            params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                      lsubTitle, @"name",
                      ldescription, @"description",
                      lurl, @"link",
                      imageURL, @"picture",
                      nil];
        }
        // Show the feed dialog
        [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                               parameters:params
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                      if (error) {
                                                          // An error occurred, we need to handle the error
                                                          // See: https://developers.facebook.com/docs/ios/errors
                                                          DebugLog(@"Error publishing story: %@", error.description);
                                                      } else {
                                                          if (result == FBWebDialogResultDialogNotCompleted) {
                                                              // User canceled.
                                                              DebugLog(@"User cancelled.");
                                                          } else {
                                                              // Handle the publish feed callback
                                                              NSDictionary *urlParams = [FacebookShareAPI parseURLParams:[resultURL query]];
                                                              
                                                              if (![urlParams valueForKey:@"post_id"]) {
                                                                  // User canceled.
                                                                  DebugLog(@"User cancelled.");
                                                                  
                                                              } else {
                                                                  // User clicked the Share button
                                                                  NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
                                                                  DebugLog(@"result %@", result);
                                                                  [FacebookShareAPI callFacebookOpenGraphAPIWithTitle:ltitle description:ldescription image:limage deeplinkingURL:lurl objectType:objectType];
                                                              }
                                                          }
                                                      }
                                                  }];
    }
}

// A function for parsing URL parameters returned by the Feed Dialog.
+ (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}

+(void)callFacebookOpenGraphAPIWithTitle:(NSString *)title description:(NSString *)ldescription image:(NSString *)image deeplinkingURL:(NSString *)deepLinkingUrl objectType:(NSString *)objectType;
{
    if ([objectType isEqualToString:OBJECT_TYPE_NONE]) {
        DebugLog(@"Got objectType == OBJECT_TYPE_NONE");
    }else{
        if ([[INUserDefaultOperations getCustomerLoginFrom] isEqualToString:FACEBOOKSERVER]) {
            if (image != nil && ![image isEqualToString:@""]) {
                if ([image hasPrefix:@"http"]) {
                    [FacebookOpenGraphAPI shareContentUsingOpenGraphAPIWithTitle:title description:ldescription image:image deeplinkingURL:deepLinkingUrl objectType:objectType];
                }else{
                    [FacebookOpenGraphAPI shareContentUsingOpenGraphAPIWithTitle:title description:ldescription image:THUMBNAIL_LINK(image) deeplinkingURL:deepLinkingUrl objectType:objectType];
                }
            }else{
                [FacebookOpenGraphAPI shareContentUsingOpenGraphAPIWithTitle:title description:ldescription image:@"" deeplinkingURL:deepLinkingUrl objectType:objectType];
            }
        }
    }
}
@end
