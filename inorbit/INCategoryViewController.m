//
//  INCategoryViewController.m
//  shoplocal
//
//  Created by Kirti Nikam on 09/01/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//
#import <time.h>
#import <xlocale.h>
#import <Social/Social.h>

#import "INCategoryViewController.h"
#import "INAppDelegate.h"
#import "INAllStoreObj.h"
#import "INDetailViewController.h"
#import "CommonCallback.h"
#import "INBroadcastDetailObj.h"
#import "INCustBroadCastDetailsViewController.h"
#import "INStoreDetailViewController.h"
#import "UIImageView+WebCache.h"
#import "CellWithImageView.h"
#import "CellWithoutImageView.h"

#import "FacebookOpenGraphAPI.h"
#import "FacebookShareAPI.h"

#define CATEGORY_STORE_LINK(LAT,LONG,DIST,LOCALITY,PAGE,CATEGORY_ID) [NSString stringWithFormat:@"%@%@%@place_api/search?latitude=%f&longitude=%f&distance=%d&area_id=%@&category_id=%d&page=%@&count=50",LIVE_SERVER,URL_PREFIX,API_VERSION,LAT,LONG,DIST,LOCALITY,CATEGORY_ID,PAGE]
#define THUMBNAIL_LINK(IMG_LINK) [NSString stringWithFormat:@"%@%@%@",LIVE_SERVER,URL_PREFIX,IMG_LINK]

#define CATEGORY_BROADCASTPOST_LINK(LOCALITY,PAGE,CATEGORY_ID) [NSString stringWithFormat:@"%@%@%@broadcast_api/search?area_id=%@&category_id=%d&page=%@&count=50",LIVE_SERVER,URL_PREFIX,API_VERSION,LOCALITY,CATEGORY_ID,PAGE]

#define CATEGORY_STORE_LINK_WITH_OFFERS(LAT,LONG,DIST,LOCALITY,PAGE) [NSString stringWithFormat:@"%@%@%@place_api/search?latitude=%f&longitude=%f&distance=%d&area_id=%@&page=%@&count=50&offer=1",LIVE_SERVER,URL_PREFIX,API_VERSION,LAT,LONG,DIST,LOCALITY,PAGE]

#define CATEGORY_BROADCASTPOST_LINK_WITH_OFFERS(LOCALITY,PAGE) [NSString stringWithFormat:@"%@%@%@broadcast_api/search?area_id=%@&page=%@&count=50&offer=1",LIVE_SERVER,URL_PREFIX,API_VERSION,LOCALITY,PAGE]

#define CALL_STORE [NSString stringWithFormat:@"%@%@broadcast_api/call",URL_PREFIX,API_VERSION]


@interface INCategoryViewController ()
@property(strong) NSDictionary *splashJson;
@end

@implementation INCategoryViewController
@synthesize categoryTableView;
@synthesize catgArray;
@synthesize countlbl;
@synthesize textPull, textRelease, textLoading, refreshHeaderView, refreshLabel, refreshArrow, refreshSpinner;
@synthesize totalPage;
@synthesize totalRecord;
@synthesize currentPage;
@synthesize splashJson;
@synthesize countHeaderView,lblpullToRefresh;
@synthesize isStoreSearch;
@synthesize currentSelectedRow;
@synthesize bgImageView;
@synthesize category_id;
@synthesize callModalViewBackView,callModalView,lblcallModalHeader,lblcallModalHeaderSeperator,callModalTableView,btnCancelcallModal;
@synthesize telArray,selectedPlaceId;
@synthesize selectedPlaceName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self setupStrings];
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:FALSE];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:TRUE];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:TRUE];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:TRUE];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    DebugLog(@"--isStoreSearch %d--category_id %d",isStoreSearch,category_id);
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                             style:UIBarButtonItemStylePlain
                                                            target:nil
                                                            action:nil];
    [[self navigationItem] setBackBarButtonItem:back];
    
    [self setUI];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadData:) name:IN_STORE_DID_CHANGE_NOTIFICATION object:nil];
    
    if (!isStoreSearch) {
        [self.categoryTableView registerNib:[UINib nibWithNibName:@"CellWithImageView" bundle:nil] forCellReuseIdentifier:kWithImageCellIdentifier];
        [self.categoryTableView registerNib:[UINib nibWithNibName:@"CellWithoutImageView" bundle:nil] forCellReuseIdentifier:kWithoutImageCellIdentifier];
    }
    bgImageView.hidden = NO;
    self.categoryTableView.backgroundColor = [UIColor clearColor];
    self.categoryTableView.separatorColor = [UIColor clearColor];
    
    catgArray = [[NSMutableArray alloc] init];
    [self addPullToRefreshHeader];
    [self loadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setBgImageView:nil];
    [self setCategoryTableView:nil];
    [self setCountHeaderView:nil];
    [self setLblpullToRefresh:nil];
    [self setCountlbl:nil];
    [self setCategoryTableView:nil];
    [super viewDidUnload];
}

#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma internal methods
-(void)setUI
{
    countHeaderView.backgroundColor =   [UIColor clearColor];
    countHeaderView.clipsToBounds   = YES;
    
    lblpullToRefresh.font               =   DEFAULT_FONT(15.0);
    lblpullToRefresh.backgroundColor    =   LIGHT_GREEN_COLOR_WITH_ALPHA(0.5);
    lblpullToRefresh.textColor          =   [UIColor whiteColor];
    
    countlbl.font               =   DEFAULT_FONT(15.0);
    countlbl.backgroundColor    =   LIGHT_GREEN_COLOR_WITH_ALPHA(0.5);
    countlbl.textColor          =   [UIColor whiteColor];
    
    
    //callModalView = [CommonCallback setViewPropertiesWithRoundedCorner:callModalView];
    callModalView.backgroundColor =  [UIColor whiteColor];
    callModalView.layer.shadowColor      = [UIColor blackColor].CGColor;
    callModalView.layer.shadowOffset     = CGSizeMake(1, 1);
    callModalView.layer.shadowOpacity    = 1.0;
    callModalView.layer.shadowRadius     = 10.0;
    [callModalViewBackView setHidden:YES];
    [callModalView setHidden:YES];
    
    lblcallModalHeader.textAlignment     = NSTextAlignmentCenter;
    lblcallModalHeader.backgroundColor   = [UIColor clearColor];
    lblcallModalHeader.font              = DEFAULT_BOLD_FONT(20);
    lblcallModalHeader.textColor         = BROWN_COLOR;
    //    lblcallModalHeader.adjustsFontSizeToFitWidth = YES;
    lblcallModalHeader.minimumScaleFactor = 15;
    lblcallModalHeader.text              = @"Select number";
    lblcallModalHeader.numberOfLines     = 2;
    
    lblcallModalHeaderSeperator.backgroundColor   = DEFAULT_COLOR;
    callModalTableView.backgroundColor = [UIColor whiteColor];
    if ([callModalTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [callModalTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    btnCancelcallModal.backgroundColor = [UIColor clearColor];
}


-(void)addHUDView
{
    hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:hud];
	hud.dimBackground = NO;
    hud.labelText = @"Loading";
	hud.detailsLabelText = @"Please wait ...";
    [hud hide:NO];
}

-(void)removeHUDView
{
    [hud removeFromSuperview];
    hud = nil;
}

-(void)sendStoreRequest:(NSString *)urlString
{
    DebugLog(@"url - %@",urlString);
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        //[hud hide:YES];
                                                        DebugLog(@"%@",self.splashJson);
                                                        [CommonCallback hideProgressHud];
                                                      
                                                        if(self.splashJson  != nil) {
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSDictionary *maindict = [self.splashJson  objectForKey:@"data"];
                                                                self.totalPage = [[maindict  objectForKey:@"total_page"] integerValue];
                                                                self.totalRecord = [[maindict  objectForKey:@"total_record"] integerValue];
                                                                NSArray* jsonArray = [maindict  objectForKey:@"record"];
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *objdict = [jsonArray  objectAtIndex:index];
                                                                    //DebugLog(@"\n\n%@\n\n",objdict);
                                                                    storeObj = [[INAllStoreObj alloc] init];
                                                                    storeObj.area = [objdict objectForKey:@"area"];
                                                                    storeObj.building = [objdict objectForKey:@"building"];
                                                                    storeObj.city = [objdict objectForKey:@"city"];
                                                                    storeObj.description = [objdict objectForKey:@"description"];
                                                                    storeObj.distance = [objdict objectForKey:@"distance"];
                                                                    storeObj.email = [objdict objectForKey:@"email"];
                                                                    storeObj.has_offer = [objdict objectForKey:@"has_offer"];
                                                                    storeObj.ID = [[objdict objectForKey:@"id"] integerValue];
                                                                    storeObj.image_url = [objdict objectForKey:@"image_url"];
                                                                    storeObj.landmark = [objdict objectForKey:@"landmark"];
                                                                    storeObj.mob_no1 = [objdict objectForKey:@"mob_no1"];
                                                                    storeObj.name = [objdict objectForKey:@"name"];
                                                                    storeObj.place_parent = [[objdict objectForKey:@"place_parent"] integerValue];
                                                                    storeObj.street = [objdict objectForKey:@"street"];
                                                                    storeObj.title = [objdict objectForKey:@"title"];
                                                                    storeObj.total_like = [objdict objectForKey:@"total_like"];
                                                                    storeObj.website = [objdict objectForKey:@"website"];
                                                                    [catgArray addObject:storeObj];
                                                                    storeObj = nil;
                                                                }
                                                            } else {
                                                                if([self.splashJson  objectForKey:@"message"] != [NSNull null] && ![[self.splashJson objectForKey:@"message"] isEqualToString:@""]){
                                                                    NSString *message = [self.splashJson objectForKey:@"message"];
                                                                    [INUserDefaultOperations showSIAlertView:message];
                                                                }
                                                            }
                                                            //                                                            if([catgArray count] > 0)
                                                            //
                                                             DebugLog(@"-%d-", [catgArray count]);
                                                            [categoryTableView reloadData];
                                                            [self setCountLabel];
                                                            //                                                            }
                                                        }
                                                        isAdding = NO;
                                                        if(isLoading)
                                                        {
                                                            [self stopLoading];
                                                        }
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        DebugLog(@"-%@-%@-%@-%d-",response,error,JSON,response.statusCode);
                                                        DebugLog(@"-%d-%d-",[error code],NSURLErrorCancelled);
                                                        if ([error code] == NSURLErrorCancelled) {
                                                            DebugLog(@"yup operation is cancelled %d",[error code]);
                                                        }else{
                                                            UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                         message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                        delegate:nil
                                                                                               cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                            [av show];
                                                        }
                                                        isAdding = NO;
                                                        if(isLoading)
                                                        {
                                                            [self stopLoading];
                                                        }
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                    }];
    [operation start];
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleShrink];
    [MMProgressHUD showWithTitle:HUD_TITLE
                          status:@""
             confirmationMessage:[NSString stringWithFormat:@"Cancel?"]
                     cancelBlock:^{
                         DebugLog(@"Task was cancelled!");
                         [operation cancel];
                     }];
}

-(void)sendBroadCastRequest:(NSString *)urlString
{
    DebugLog(@"%@", urlString);
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"sendStoreBroadcastRequest %@", self.splashJson);
                                                        if(self.splashJson  != nil) {
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSDictionary *maindict = [self.splashJson  objectForKey:@"data"];
                                                                self.totalPage = [[maindict  objectForKey:@"total_page"] integerValue];
                                                                self.totalRecord = [[maindict  objectForKey:@"total_record"] integerValue];
                                                                NSArray* jsonArray = [maindict  objectForKey:@"record"];
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *objdict = [jsonArray  objectAtIndex:index];
                                                                    //DebugLog(@"\n\n%@\n\n",objdict);
                                                                    INBroadcastDetailObj *broadcastObj = [[INBroadcastDetailObj alloc] init];
                                                                    broadcastObj.ID = [[objdict objectForKey:@"post_id"] integerValue];
                                                                    broadcastObj.place_ID = [[objdict objectForKey:@"id"] integerValue];
                                                                    broadcastObj.areaID = [objdict objectForKey:@"area_id"];
                                                                    broadcastObj.name = [objdict objectForKey:@"name"];
                                                                    broadcastObj.title = [objdict objectForKey:@"title"];
                                                                    broadcastObj.description = [objdict objectForKey:@"description"];
                                                                    broadcastObj.date = [objdict objectForKey:@"date"];
                                                                    broadcastObj.offerdate = [objdict objectForKey:@"offer_date_time"];
                                                                    broadcastObj.distance = [objdict objectForKey:@"distance"];
                                                                    broadcastObj.latitude = [objdict objectForKey:@"latitude"];
                                                                    broadcastObj.longitude = [objdict objectForKey:@"longitude"];
                                                                    broadcastObj.tel_no1 = [objdict objectForKey:@"tel_no1"];
                                                                    broadcastObj.tel_no2 = [objdict objectForKey:@"tel_no2"];
                                                                    broadcastObj.tel_no3 = [objdict objectForKey:@"tel_no3"];
                                                                    broadcastObj.mob_no1 = [objdict objectForKey:@"mob_no1"];
                                                                    broadcastObj.mob_no2 = [objdict objectForKey:@"mob_no2"];
                                                                    broadcastObj.mob_no3 = [objdict objectForKey:@"mob_no3"];
                                                                    broadcastObj.image_url1 = [objdict objectForKey:@"image_url1"];
                                                                    broadcastObj.image_url2 = [objdict objectForKey:@"image_url2"];
                                                                    broadcastObj.image_url3 = [objdict objectForKey:@"image_url3"];
                                                                    broadcastObj.thumb_url1 = [objdict objectForKey:@"thumb_url1"];
                                                                    broadcastObj.thumb_url2 = [objdict objectForKey:@"thumb_url2"];
                                                                    broadcastObj.thumb_url3 = [objdict objectForKey:@"thumb_url3"];
                                                                    broadcastObj.total_like = [objdict objectForKey:@"total_like"];
                                                                    broadcastObj.total_share = [objdict objectForKey:@"total_share"];
                                                                    broadcastObj.is_offered = [objdict objectForKey:@"is_offered"];
                                                                    broadcastObj.type = [objdict objectForKey:@"type"];
                                                                    broadcastObj.verified = [objdict objectForKey:@"verified"];
                                                                    [catgArray addObject:broadcastObj];
                                                                    broadcastObj = nil;
                                                                }
                                                            }else {
                                                                if([self.splashJson  objectForKey:@"message"] != [NSNull null] && ![[self.splashJson objectForKey:@"message"] isEqualToString:@""]){
                                                                    NSString *message = [self.splashJson objectForKey:@"message"];
                                                                    [INUserDefaultOperations showSIAlertView:message];
                                                                }
                                                            }
                                                            DebugLog(@"catgArray %@",catgArray);
                                                            //                                                            if([catgArray count] > 0)
                                                            //                                                            {
                                                            [categoryTableView reloadData];
                                                            [self setCountLabel];
                                                            //                                                            }
                                                        }
                                                        isAdding = NO;
                                                        if(isLoading)
                                                        {
                                                            [self stopLoading];
                                                        }
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK"
                                                                                           otherButtonTitles:nil];
                                                        [av show];
                                                        isAdding = NO;
                                                        if(isLoading)
                                                        {
                                                            [self stopLoading];
                                                        }
                                                    }];
    [operation start];
}



#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:callModalTableView]) {
        return 80;
    }else{
        if (isStoreSearch) {
            return 125;
        }else{
            INBroadcastDetailObj *tempbroadObj  = [catgArray objectAtIndex:indexPath.row];
            if(tempbroadObj.image_url1 != (NSString*)[NSNull null] && ![tempbroadObj.image_url1 isEqualToString:@""]) {
                return kWithImageCellHeight;
            }
            else {
                return kWithoutImageCellHeight;
            }
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:callModalTableView]) {
        return [telArray count];
    }else{
        return [catgArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:callModalTableView]) {
        static NSString *CellIdentifier = @"CallCell";
        UIImageView *thumbImg;
        UILabel *lblNumber;
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.backgroundColor = [UIColor whiteColor];
            
            thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(10,15, 50, 50)];
            thumbImg.tag = 1111;
            thumbImg.contentMode = UIViewContentModeScaleToFill;
            thumbImg.image = [UIImage imageNamed:@"call_brown.png"];
            [cell.contentView addSubview:thumbImg];
            
            lblNumber = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame),0, 210, 80)];
            lblNumber.text = @"";
            lblNumber.tag = 2222;
            lblNumber.textColor             = BROWN_COLOR;
            lblNumber.highlightedTextColor  = [UIColor whiteColor];
            lblNumber.font                  = DEFAULT_FONT(23);
            lblNumber.textAlignment         = NSTextAlignmentLeft;
            lblNumber.backgroundColor       = [UIColor clearColor];
            [cell.contentView addSubview:lblNumber];
            
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = BROWN_OFFWHITE_COLOR;
            cell.selectedBackgroundView = bgColorView;
        }
        lblNumber        = (UILabel *)[cell.contentView viewWithTag:2222];
        DebugLog(@"lblNumber %@",[telArray objectAtIndex:indexPath.row]);
        lblNumber.text   = [telArray objectAtIndex:indexPath.row];
        return cell;
    }else{
        if (isStoreSearch) {
        static NSString *CellIdentifier = @"Cell";
        UIView *backView;
        UILabel *lblOffer;
        
        UIButton *btnCall;
       // UIImageView *thumbImg;
        UILabel *lblTitle;
        UILabel *lblDesc;
        
        UIImageView *offerthumbImg;
        
        UIImageView *likethumbImg;
        UILabel *lblLikeCount;
        
        UIImageView *ldistthumbImg;
        UILabel *lblDist;
        
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.backgroundColor = [UIColor clearColor];

            backView = [[UIView alloc] initWithFrame:CGRectMake(0, 5, 320, 120)];
            backView.tag = 333;
            //backView = [CommonCallback setViewPropertiesWithRoundedCorner:backView];
            backView.backgroundColor =  [UIColor whiteColor];
            backView.clipsToBounds = YES;
            
            lblOffer = [[UILabel alloc] initWithFrame:CGRectMake(0.0,0, 320.0, 30.0)];
            lblOffer.text = @"";
            lblOffer.tag = 111;
            lblOffer.font = DEFAULT_BOLD_FONT(15);
            lblOffer.textAlignment = NSTextAlignmentCenter;
            lblOffer.textColor = [UIColor whiteColor];
            lblOffer.backgroundColor =  LIGHT_GREEN_COLOR;
            lblOffer.hidden = TRUE;
            [backView addSubview:lblOffer];
            
            btnCall = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnCall setFrame:CGRectMake(0, 20, 80, 80)];
            [btnCall setImage:[UIImage imageNamed:@"Call_white.png"] forState:UIControlStateNormal];
            [btnCall setBackgroundColor:[UIColor clearColor]];
            [btnCall setTitle:@"" forState:UIControlStateNormal];
            [btnCall addTarget:self action:@selector(callBtnTapped:event:) forControlEvents:UIControlEventTouchUpInside];
            [backView addSubview:btnCall];
            
            offerthumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,71.0,50.0)];
            offerthumbImg.tag = 107;
            offerthumbImg.image = [UIImage imageNamed:@"Offer_Icon1.png"];
            offerthumbImg.contentMode = UIViewContentModeScaleAspectFill;
            [backView addSubview:offerthumbImg];
            
            lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(btnCall.frame)+5,4,230,40.0)];
            lblTitle.text = @"";
            lblTitle.tag = 101;
            lblTitle.font = DEFAULT_BOLD_FONT(15);
            lblTitle.textAlignment = NSTextAlignmentLeft;
            lblTitle.textColor = BROWN_COLOR;
            lblTitle.highlightedTextColor = [UIColor whiteColor];
            lblTitle.backgroundColor =  [UIColor clearColor];
            lblTitle.numberOfLines = 2;
            [backView addSubview:lblTitle];
            
            lblDesc = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(btnCall.frame)+5, CGRectGetMaxY(lblTitle.frame) - 5,230,60)];
            lblDesc.text = @"";
            lblDesc.tag = 102;
            lblDesc.numberOfLines = 3;
            lblDesc.font = DEFAULT_FONT(12);
            lblDesc.textAlignment = NSTextAlignmentLeft;
            lblDesc.textColor = BROWN_COLOR;
            lblDesc.highlightedTextColor = [UIColor whiteColor];
            lblDesc.backgroundColor =  [UIColor clearColor];
            [backView addSubview:lblDesc];
            
            likethumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth(backView.frame) - 110,CGRectGetMaxY(lblDesc.frame)+2,15.0,15.0)];
            likethumbImg.tag = 103;
            likethumbImg.image = [UIImage imageNamed:@"list_fav_icon.png"];
            likethumbImg.contentMode = UIViewContentModeScaleAspectFill;
            [backView addSubview:likethumbImg];
            
            lblLikeCount = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(likethumbImg.frame),CGRectGetMaxY(lblDesc.frame)+2,30.0,15.0)];
            lblLikeCount.text = @"";
            lblLikeCount.tag = 104;
            lblLikeCount.font = DEFAULT_BOLD_FONT(10);
            lblLikeCount.textAlignment = NSTextAlignmentCenter;
            lblLikeCount.textColor = BROWN_COLOR;
            lblLikeCount.highlightedTextColor = [UIColor whiteColor];
            lblLikeCount.backgroundColor =  [UIColor clearColor];
            [backView addSubview:lblLikeCount];
            
            ldistthumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth(backView.frame)-67,CGRectGetMaxY(lblDesc.frame)+2,15.0,15.0)];
            ldistthumbImg.image = [UIImage imageNamed:@"list_distance_icon.png"];
            ldistthumbImg.tag = 105;
            ldistthumbImg.contentMode = UIViewContentModeScaleAspectFill;
            [backView addSubview:ldistthumbImg];
            
            lblDist = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(ldistthumbImg.frame),CGRectGetMaxY(lblDesc.frame)+2,50.0,15.0)];
            lblDist.text = @"";
            lblDist.tag = 106;
            lblDist.font = DEFAULT_BOLD_FONT(10);
            lblDist.textAlignment = NSTextAlignmentCenter;
            lblDist.textColor = BROWN_COLOR;
            lblDist.highlightedTextColor = [UIColor whiteColor];
            lblDist.backgroundColor =  [UIColor clearColor];
            [backView addSubview:lblDist];
            
            [cell.contentView addSubview:backView];
            
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
            bgColorView.tag = 104;
            cell.selectedBackgroundView = bgColorView;
        }
        if ([indexPath row] < [catgArray count]) {
            INAllStoreObj *placeObj  = [catgArray objectAtIndex:indexPath.row];

            backView        = (UIView *)[cell viewWithTag:333];
            lblOffer        = (UILabel *)[backView viewWithTag:111];
         //   thumbImg        = (UIImageView *)[backView viewWithTag:100];
            lblTitle        = (UILabel *)[backView viewWithTag:101];
            lblDesc         = (UILabel *)[backView viewWithTag:102];
            likethumbImg    = (UIImageView *)[backView viewWithTag:103];
            lblLikeCount    = (UILabel *)[backView viewWithTag:104];
            ldistthumbImg   = (UIImageView *)[backView viewWithTag:105];
            lblDist         = (UILabel *)[backView viewWithTag:106];
            offerthumbImg   = (UIImageView *)[backView viewWithTag:107];
            
            lblOffer.hidden = YES;
            if (placeObj.has_offer != nil && [placeObj.has_offer isEqualToString:@"1"]) {
                offerthumbImg.hidden = FALSE;
            }else{
                offerthumbImg.hidden = YES;
            }
            
            [lblTitle setText:placeObj.name];
            [lblDesc setText: placeObj.description];
            
            [lblDist setHidden:TRUE];
            [ldistthumbImg setHidden:TRUE];
            
            if(placeObj.distance != nil) {
                DebugLog(@"%@",placeObj.distance);
                if ([placeObj.distance floatValue] < 1000) {
                    
                    likethumbImg.frame = CGRectMake(CGRectGetWidth(backView.frame) - 110, CGRectGetMaxY(lblDesc.frame)+2,15.0,15.0);
                    lblLikeCount.frame = CGRectMake(CGRectGetMaxX(likethumbImg.frame),CGRectGetMaxY(lblDesc.frame)+2,30.0,15.0);
                    
                    [lblDist setHidden:FALSE];
                    [ldistthumbImg setHidden:FALSE];
                    
                    if([placeObj.distance floatValue]  > -1 && [placeObj.distance floatValue]  < 1)
                        [lblDist setText: [NSString stringWithFormat:@"%.2fm",([placeObj.distance floatValue] * 100)]];
                    else
                        [lblDist setText: [NSString stringWithFormat:@"%.2fkm",([placeObj.distance floatValue])]];
                }  else {
                    likethumbImg.frame = CGRectMake(CGRectGetWidth(backView.frame) - 67, CGRectGetMaxY(lblDesc.frame)+2,15.0,15.0);
                    lblLikeCount.frame = CGRectMake(CGRectGetMaxX(ldistthumbImg.frame),CGRectGetMaxY(lblDesc.frame)+2,30.0,15.0);
                }
            }
                       
            [lblLikeCount setHidden:FALSE];
            [likethumbImg setHidden:FALSE];
            lblLikeCount.text = placeObj.total_like;
        }
        return cell;
    }
        else
    {
        
        
        //NSLog(@"---%@----",catgArray);
        if ([catgArray count] <= 0)
        {
            NSString *cellIdentifier = kWithImageCellIdentifier;
            CellWithImageView *cell = (CellWithImageView *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            return cell;
        }
         DebugLog(@"-%d-%d-", [catgArray count], indexPath.row);
        
        INBroadcastDetailObj *tempbroadcastObj  = [catgArray objectAtIndex:indexPath.row];
        DebugLog(@"---%@----",tempbroadcastObj.date);
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        NSDate *myDate = [[NSDate alloc] init];
        myDate = [dateFormatter dateFromString:tempbroadcastObj.date];
        DebugLog(@"MyDate is: %@", myDate);
        
        NSDateFormatter *dayformatter = [[NSDateFormatter alloc] init];
        [dayformatter setDateFormat:@"dd"];
        NSString *dayFromDate = [dayformatter stringFromDate:myDate];
        DebugLog(@"Myday is: %@", dayFromDate);
        
        NSDateFormatter *monthformatter = [[NSDateFormatter alloc] init];
        [monthformatter setDateFormat:@"MMM"];
        NSString *monthFromDate = [monthformatter stringFromDate:myDate];
        DebugLog(@"MyMonth is: %@", monthFromDate);
        
        NSDateFormatter *timeformatter = [[NSDateFormatter alloc] init];
        [timeformatter setDateFormat:@"hh:mm a"];
        NSString *timeFromDate = [timeformatter stringFromDate:myDate];
        DebugLog(@"timeFromDate is: %@", timeFromDate);
        
        if(tempbroadcastObj.image_url1 != (NSString*)[NSNull null] && ![tempbroadcastObj.image_url1 isEqualToString:@""])
        {
            NSString *cellIdentifier = kWithImageCellIdentifier;
            CellWithImageView *cell = (CellWithImageView *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            cell.lblTitle.text          = tempbroadcastObj.title;
            cell.lblLeftTitle.text      = tempbroadcastObj.name;
            cell.lblDescription.text    = tempbroadcastObj.description;
            cell.lblLikeCount.text      = tempbroadcastObj.total_like;
            cell.lblShareCount.text     = tempbroadcastObj.total_share;
            cell.lblTimeStamp.text      = [NSString stringWithFormat:@"%@ %@    %@",dayFromDate,monthFromDate,timeFromDate];
            if (tempbroadcastObj.is_offered != nil && [tempbroadcastObj.is_offered isEqualToString:@"1"]) {
                [cell.offerImageView setHidden:NO];
            }else{
                [cell.offerImageView setHidden:YES];
            }
            if (tempbroadcastObj.user_like != nil && [tempbroadcastObj.user_like isEqualToString:@"1"]) {
                cell.favImageView.image = [UIImage imageNamed:@"filled_like.png"];
            }else{
                cell.favImageView.image = [UIImage imageNamed:@"empty_like.png"];
            }
            
            if(tempbroadcastObj.image_url1 != nil && ![tempbroadcastObj.image_url1 isEqualToString:@""])
            {
                __weak UIImageView *thumbImg_ = cell.thumbImageView;
                [cell.thumbImageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(tempbroadcastObj.image_url1)] placeholderImage:[UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"] completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType) {
                    if (error) {
                        thumbImg_.image = [UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"];
                    }
                }];
            } else {
                cell.thumbImageView.image = [UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"];
            }
            
            UIButton *likeBtn = (UIButton *)[cell viewWithTag:111];
            [likeBtn addTarget:self action:@selector(likeBtnPressed:event:) forControlEvents:UIControlEventTouchUpInside];
            
            UIButton *shareBtn = (UIButton *)[cell viewWithTag:222];
            [shareBtn addTarget:self action:@selector(shareBtnPressed:event:) forControlEvents:UIControlEventTouchUpInside];
            
            UIButton *callBtn = (UIButton *)[cell viewWithTag:333];
            [callBtn addTarget:self action:@selector(callBtnTapped:event:) forControlEvents:UIControlEventTouchUpInside];
            return cell;
            
        }else{
            NSString *cellIdentifier = kWithoutImageCellIdentifier;
            CellWithoutImageView *cell = (CellWithoutImageView *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            cell.lblTitle.text          = tempbroadcastObj.title;
            cell.lblLeftTitle.text      = tempbroadcastObj.name;
            cell.lblDescription.text    = tempbroadcastObj.description;
            cell.lblLikeCount.text      = tempbroadcastObj.total_like;
            cell.lblShareCount.text     = tempbroadcastObj.total_share;
            cell.lblTimeStamp.text      = [NSString stringWithFormat:@"%@ %@    %@",dayFromDate,monthFromDate,timeFromDate];
            if (tempbroadcastObj.is_offered != nil && [tempbroadcastObj.is_offered isEqualToString:@"1"]) {
                [cell.offerImageView setHidden:NO];
            }else{
                [cell.offerImageView setHidden:YES];
            }
            if (tempbroadcastObj.user_like != nil && [tempbroadcastObj.user_like isEqualToString:@"1"]) {
                cell.favImageView.image = [UIImage imageNamed:@"filled_like.png"];
            }else{
                cell.favImageView.image = [UIImage imageNamed:@"empty_like.png"];
            }
            
            UIButton *likeBtn = (UIButton *)[cell viewWithTag:111];
            [likeBtn addTarget:self action:@selector(likeBtnPressed:event:) forControlEvents:UIControlEventTouchUpInside];
            
            UIButton *shareBtn = (UIButton *)[cell viewWithTag:222];
            [shareBtn addTarget:self action:@selector(shareBtnPressed:event:) forControlEvents:UIControlEventTouchUpInside];
            
            UIButton *callBtn = (UIButton *)[cell viewWithTag:333];
            [callBtn addTarget:self action:@selector(callBtnTapped:event:) forControlEvents:UIControlEventTouchUpInside];
            return cell;
        }
      }
    }
}


#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:callModalTableView]) {
        [callModalView setHidden:YES];
        [callModalViewBackView setHidden:YES];
        NSString *selectedTelNumberToCall = [[telArray objectAtIndex:indexPath.row] stringByReplacingOccurrencesOfString:@" " withString:@""];
        DebugLog(@"selectedTelNumberToCall %@",selectedTelNumberToCall);
        NSString *deviceType = [UIDevice currentDevice].model;
        if([deviceType isEqualToString:@"iPhone"])
        {
            UIApplication *myApp = [UIApplication sharedApplication];
            DebugLog(@"selectedTelNumberToCall = -%@-",selectedTelNumberToCall);
            [myApp openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",selectedTelNumberToCall]]];
            
            NSDictionary *callParams = [NSDictionary dictionaryWithObjectsAndKeys:@"SearchCategory",@"Source",[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"AreaName",selectedPlaceName,@"StoreName",selectedTelNumberToCall,@"Number", nil];
            [INEventLogger logEvent:@"StoreCallDone" withParams:callParams];
            
            [self sendStoreCallRequest:@"StoreCallDone"];
        }
        else
        {
            UIAlertView *alert = [[ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                              message: @"Calling functionality is not available in this device. "
                                                             delegate: nil
                                                    cancelButtonTitle: nil
                                                    otherButtonTitles: @"OK", nil];
            [alert show];
        }
    }else{
        if (isStoreSearch) {
        INAllStoreObj *placeObj  = [catgArray objectAtIndex:indexPath.row];
        INStoreDetailViewController *detailController = [[INStoreDetailViewController alloc] initWithNibName:@"INStoreDetailViewController" bundle:nil] ;
        detailController.title = placeObj.name;
        detailController.storeId = placeObj.ID;
        DebugLog(@"%@",placeObj.image_url);
        if ([placeObj.image_url hasPrefix:@"http"]) {
            detailController.storeImageUrl = [NSURL URLWithString:placeObj.image_url];
        }else{
            detailController.storeImageUrl = [NSURL URLWithString:THUMBNAIL_LINK(placeObj.image_url)];
        }
        [self.navigationController pushViewController:detailController animated:YES];
    }else{
        INBroadcastDetailObj *broadcastObj  = [catgArray objectAtIndex:indexPath.row];
        INCustBroadCastDetailsViewController *postDetailController = [[INCustBroadCastDetailsViewController alloc] initWithNibName:@"INCustBroadCastDetailsViewController" bundle:nil] ;
        postDetailController.title = broadcastObj.name;//@"Broadcast Detail";
        postDetailController.storeName = broadcastObj.name;
        postDetailController.postType = 1;
        postDetailController.postlikeType = broadcastObj.user_like;
        postDetailController.postId = broadcastObj.ID;
        postDetailController.postTitle = broadcastObj.title;
        postDetailController.postDescription = broadcastObj.description;
        DebugLog(@"lfeedObj.image_url1 %@",broadcastObj.image_url1);
        if ([broadcastObj.image_url1 isEqual:[NSNull null]] || broadcastObj.image_url1 == nil) {
            postDetailController.postimageUrl = @"";
        }else{
            postDetailController.postimageUrl = broadcastObj.image_url1;
        }
        postDetailController.likescountString = broadcastObj.total_like;
        postDetailController.sharecountString = broadcastObj.total_share;
        postDetailController.viewOpenedFrom = FROM_NEWS_FEED;
        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
        if (broadcastObj.tel_no1 != nil && ![broadcastObj.tel_no1 isEqualToString:@""]) {
            [tempArray addObject:broadcastObj.tel_no1];
        }
        if (broadcastObj.tel_no2 != nil && ![broadcastObj.tel_no2 isEqualToString:@""]) {
            [tempArray addObject:broadcastObj.tel_no2];
        }
        if (broadcastObj.tel_no3 != nil && ![broadcastObj.tel_no3 isEqualToString:@""]) {
            [tempArray addObject:broadcastObj.tel_no3];
        }
        if (broadcastObj.mob_no1 != nil && ![broadcastObj.mob_no1 isEqualToString:@""]) {
            [tempArray addObject:broadcastObj.mob_no1];
        }
        if (broadcastObj.mob_no2 != nil && ![broadcastObj.mob_no2 isEqualToString:@""]) {
            [tempArray addObject:broadcastObj.mob_no2];
        }
        if (broadcastObj.mob_no3 != nil && ![broadcastObj.mob_no3 isEqualToString:@""]) {
            [tempArray addObject:broadcastObj.mob_no3];
        }
        DebugLog(@"temparray %@",tempArray);
        postDetailController.telArray = tempArray;
        postDetailController.SHOW_BUY_BTN = YES;
        postDetailController.placeId = broadcastObj.place_ID;
        postDetailController.postDateTime   = broadcastObj.date;
        postDetailController.postOfferDateTime  = broadcastObj.offerdate;
        postDetailController.postIsOffered      = broadcastObj.is_offered;
        [self.navigationController pushViewController:postDetailController animated:YES];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(NSIndexPath *)getIndexpathOfEvent:(id)event
{
    NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:categoryTableView];
	NSIndexPath *indexPath = [categoryTableView indexPathForRowAtPoint: currentTouchPosition];
    return indexPath;
}

- (void)callBtnTapped:(id)sender event:(id)event
{
    NSIndexPath *indexPath = [self getIndexpathOfEvent:event];
    if (indexPath != nil)
    {
        if (telArray == nil) {
            telArray = [[NSMutableArray alloc] init];
        }else{
            [telArray removeAllObjects];
        }
        if (isStoreSearch) {
           INAllStoreObj * placeObj  = (INAllStoreObj *)[catgArray objectAtIndex:indexPath.row];
            DebugLog(@"place %@",placeObj.name);
            if (placeObj.name == nil || [placeObj.name length] == 0)
            {
                lblcallModalHeader.text =  @"Select number";
                selectedPlaceName = @"";
            }
            else {
                lblcallModalHeader.text = placeObj.name;
                selectedPlaceName = placeObj.name;
            }
            
            selectedPlaceId = placeObj.ID;
            
            if (placeObj.tel_no1 != nil && ![placeObj.tel_no1 isEqualToString:@""]) {
                [telArray addObject:placeObj.tel_no1];
            }
            if (placeObj.tel_no2 != nil && ![placeObj.tel_no2 isEqualToString:@""]) {
                [telArray addObject:placeObj.tel_no2];
            }
            if (placeObj.tel_no3 != nil && ![placeObj.tel_no3 isEqualToString:@""]) {
                [telArray addObject:placeObj.tel_no3];
            }
            if (placeObj.mob_no1 != nil && ![placeObj.mob_no1 isEqualToString:@""]) {
                [telArray addObject:[NSString stringWithFormat:@"+%@",placeObj.mob_no1]];
            }
            if (placeObj.mob_no2 != nil && ![placeObj.mob_no2 isEqualToString:@""]) {
                [telArray addObject:[NSString stringWithFormat:@"+%@",placeObj.mob_no2]];
            }
            if (placeObj.mob_no3 != nil && ![placeObj.mob_no3 isEqualToString:@""]) {
                [telArray addObject:[NSString stringWithFormat:@"+%@",placeObj.mob_no3]];
            }
        }else{
           INBroadcastDetailObj * placeObj  = (INBroadcastDetailObj *)[catgArray objectAtIndex:indexPath.row];
            DebugLog(@"place %@",placeObj.name);
            if (placeObj.name == nil || [placeObj.name length] == 0)
            {
                lblcallModalHeader.text =  @"Select number";
                selectedPlaceName = @"";
            }
            else {
                lblcallModalHeader.text = placeObj.name;
                selectedPlaceName = placeObj.name;
            }
            
            selectedPlaceId = placeObj.ID;
            
            if (placeObj.tel_no1 != nil && ![placeObj.tel_no1 isEqualToString:@""]) {
                [telArray addObject:placeObj.tel_no1];
            }
            if (placeObj.tel_no2 != nil && ![placeObj.tel_no2 isEqualToString:@""]) {
                [telArray addObject:placeObj.tel_no2];
            }
            if (placeObj.tel_no3 != nil && ![placeObj.tel_no3 isEqualToString:@""]) {
                [telArray addObject:placeObj.tel_no3];
            }
            if (placeObj.mob_no1 != nil && ![placeObj.mob_no1 isEqualToString:@""]) {
                [telArray addObject:[NSString stringWithFormat:@"+%@",placeObj.mob_no1]];
            }
            if (placeObj.mob_no2 != nil && ![placeObj.mob_no2 isEqualToString:@""]) {
                [telArray addObject:[NSString stringWithFormat:@"+%@",placeObj.mob_no2]];
            }
            if (placeObj.mob_no3 != nil && ![placeObj.mob_no3 isEqualToString:@""]) {
                [telArray addObject:[NSString stringWithFormat:@"+%@",placeObj.mob_no3]];
            }
        }
        
        [callModalTableView reloadData];
        if (callModalTableView.contentSize.height < 240) {
            [callModalTableView setFrame:CGRectMake(callModalTableView.frame.origin.x, callModalTableView.frame.origin.y, callModalTableView.frame.size.width, callModalTableView.contentSize.height)];
        }else{
            [callModalTableView setFrame:CGRectMake(callModalTableView.frame.origin.x, callModalTableView.frame.origin.y, callModalTableView.frame.size.width, 240)];
        }
        [callModalView setFrame:CGRectMake(callModalView.frame.origin.x, callModalView.frame.origin.y, callModalView.frame.size.width, callModalTableView.frame.size.height+85)];

        DebugLog(@"temparray %@",telArray);
        if (telArray != nil && telArray.count > 0) {
            [callModalViewBackView setHidden:NO];
            [callModalView setHidden:NO];
            [CommonCallback viewtransitionInCompletion:callModalView completion:^{
                [CommonCallback viewtransitionOutCompletion:callModalView completion:nil];
            }];
            
            NSDictionary *callParams = [NSDictionary dictionaryWithObjectsAndKeys:@"SearchCategory",@"Source",[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"AreaName",selectedPlaceName,@"StoreName", nil];
            [INEventLogger logEvent:@"StoreCall" withParams:callParams];
            
            [self sendStoreCallRequest:@"StoreCall"];
        } else {
            [INUserDefaultOperations showSIAlertView:NO_CONTACT_MESSAGE];
        }
    }
}

- (IBAction)btnCancelcallModalPressed:(id)sender {
    [callModalView setHidden:YES];
    [callModalViewBackView setHidden:YES];
}

-(void)sendStoreCallRequest:(NSString *)callType
{
    if (![INUserDefaultOperations isCustomerLoggedIn]) {
        return;
    }
    
    NSString *areaID = [IN_APP_DELEGATE getActiveShoplocalPlaceIdFromAreaTable];
    
    DebugLog(@"========================sendStoreCallRequest========================");
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSString *register_from = [NSString stringWithFormat:@"%@ v%@",DEVICE_TYPE,version];
    
    NSMutableDictionary *postparams = [[NSMutableDictionary alloc] init];
    //    {"user_id":"${user_id}","auth_id":"${auth_id}","place_id":"106","via":"welcome ","call_type":"type-a","active_area":"1"}
    [postparams setObject:[INUserDefaultOperations getCustomerAuthId] forKey:@"user_id"];
    [postparams setObject:[INUserDefaultOperations getCustomerAuthCode] forKey:@"auth_id"];
    if (isStoreSearch) {
        [postparams setObject:[NSString stringWithFormat:@"%d",selectedPlaceId] forKey:@"place_id"];
    }else{
        [postparams setObject:[NSString stringWithFormat:@"%d",selectedPlaceId] forKey:@"post_id"];
    }
    [postparams setObject:callType forKey:@"call_type"];
    [postparams setObject:areaID forKey:@"active_area"];
    [postparams setObject:register_from forKey:@"via"];
    
    DebugLog(@"sendStoreCallRequest : postParams -%@-",postparams);
    
    [httpClient postPath:CALL_STORE parameters:postparams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"sendStoreCallRequest : json afnetworking ->%@",json);
        if (json!= nil) {
            NSString* code = [json objectForKey:@"code"];
            if([code isEqualToString:CUSTOMER_INVALID_USER_AUTH_CODE])
            {
                if([INUserDefaultOperations isCustomerLoggedIn]){
                    [INUserDefaultOperations clearCustomerDetails];
                }
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"sendStoreCallRequest : [HTTPClient Error]: %@", error.localizedDescription);
    }];
}

- (UIImage *)scale:(UIImage *)image toSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}

-(void)pageRequest:(NSString *)pageNum
{
    if([IN_APP_DELEGATE networkavailable])
    {
        isAdding = YES;
        DebugLog(@"----------isStoreSearch %d---------",isStoreSearch);
        if (isStoreSearch) {
            if (category_id == 0) {
                [self sendStoreRequest:CATEGORY_STORE_LINK_WITH_OFFERS([INUserDefaultOperations getLatitude],[INUserDefaultOperations getLongitude],[INUserDefaultOperations getCustomerDistance],[IN_APP_DELEGATE getActiveAreaIdFromAreaTable],pageNum)];
            }else{
                [self sendStoreRequest:CATEGORY_STORE_LINK([INUserDefaultOperations getLatitude],[INUserDefaultOperations getLongitude],[INUserDefaultOperations getCustomerDistance],[IN_APP_DELEGATE getActiveAreaIdFromAreaTable],pageNum,category_id)];
            }
        }else{
            if (category_id == 0) {
                [self sendBroadCastRequest:CATEGORY_BROADCASTPOST_LINK_WITH_OFFERS([IN_APP_DELEGATE getActiveAreaIdFromAreaTable],pageNum)];
            }else{
                [self sendBroadCastRequest:CATEGORY_BROADCASTPOST_LINK([IN_APP_DELEGATE getActiveAreaIdFromAreaTable],pageNum,category_id)];
            }
        }
    }  else   {
        isAdding = NO;
        if(isLoading)
        {
            [self stopLoading];
        }
        [INUserDefaultOperations showOfflineAlert];
    }
}

-(void)loadData
{
    self.currentPage = 1;
    [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
}

- (void)reloadData:(NSNotification *)notification
{
    DebugLog(@"Reload data");
    [catgArray removeAllObjects];
    self.currentPage = 1;
    [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
}

-(void)setCountLabel
{
    self.countlbl.text = [NSString stringWithFormat:@"%d/%d",[catgArray count],self.totalRecord];
    DebugLog(@"-----%@--------",countlbl.text);
}

#pragma mark PULL TO REFRESH
- (void)setupStrings{
    textPull = @"Pull down to refresh...";
    textRelease = @"Release to refresh...";
    textLoading = @"Loading please wait...";
}

- (void)addPullToRefreshHeader {
    refreshHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0 - REFRESH_HEADER_HEIGHT, 320, REFRESH_HEADER_HEIGHT)];
    refreshHeaderView.backgroundColor = [UIColor clearColor];
    
    refreshLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, REFRESH_HEADER_HEIGHT)];
    refreshLabel.backgroundColor = [UIColor clearColor];
    refreshLabel.font = DEFAULT_BOLD_FONT(15);
    refreshLabel.textAlignment = NSTextAlignmentCenter;
    refreshLabel.textColor = DEFAULT_COLOR;
    refreshArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:PULLTOREFRESH_BARROW]];
    refreshArrow.frame = CGRectMake(floorf((REFRESH_HEADER_HEIGHT - 2) / 2),
                                    (floorf(REFRESH_HEADER_HEIGHT - 44) / 2),
                                    27, 44);
    
    refreshSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    refreshSpinner.frame = CGRectMake(floorf(floorf(REFRESH_HEADER_HEIGHT - 2) / 2), floorf((REFRESH_HEADER_HEIGHT - 20) / 2), 20, 20);
    refreshSpinner.hidesWhenStopped = YES;
    
    [refreshHeaderView addSubview:refreshLabel];
    [refreshHeaderView addSubview:refreshArrow];
    [refreshHeaderView addSubview:refreshSpinner];
    [categoryTableView addSubview:refreshHeaderView];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if ([scrollView isEqual:callModalTableView]) {
        return;
    }
    if (isLoading) return;
    isDragging = YES;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([scrollView isEqual:callModalTableView]) {
        return;
    }
    if (isLoading) {
        // Update the content inset, good for section headers
        if (scrollView.contentOffset.y > 0)
            categoryTableView.contentInset = UIEdgeInsetsZero;
        else if (scrollView.contentOffset.y >= -REFRESH_HEADER_HEIGHT)
            categoryTableView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (isDragging && scrollView.contentOffset.y < 0) {
        // Update the arrow direction and label
        [UIView animateWithDuration:0.25 animations:^{
            if (scrollView.contentOffset.y < -REFRESH_HEADER_HEIGHT) {
                // User is scrolling above the header
                refreshLabel.text = self.textRelease;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
            } else {
                // User is scrolling somewhere within the header
                refreshLabel.text = self.textPull;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
            }
        }];
    } else {
        CGFloat height = scrollView.frame.size.height;
        
        CGFloat contentYoffset = scrollView.contentOffset.y;
        
        CGFloat distanceFromBottom = scrollView.contentSize.height - contentYoffset;
        
        if(distanceFromBottom <= height)
        {
            if(isAdding==TRUE) {
                DebugLog(@"<<<<<<<<<<<<<<<");
                return;
            } else {
                DebugLog(@"end of the table page = %d", self.currentPage);
                if(self.currentPage < self.totalPage)
                {
                    self.currentPage = self.currentPage + 1;
                    [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
                }
            }
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if ([scrollView isEqual:callModalTableView]) {
        return;
    }
    if (isLoading) return;
    isDragging = NO;
    if (scrollView.contentOffset.y <= -REFRESH_HEADER_HEIGHT) {
        // Released above the header
        [self startLoading];
    }
}

- (void)startLoading {
    isLoading = YES;
    // Show the header
    [UIView animateWithDuration:0.3 animations:^{
        categoryTableView.contentInset = UIEdgeInsetsMake(REFRESH_HEADER_HEIGHT, 0, 0, 0);
        refreshLabel.text = self.textLoading;
        refreshArrow.hidden = YES;
        [refreshSpinner startAnimating];
    }];
    if([IN_APP_DELEGATE networkavailable])
    {
        [catgArray removeAllObjects];
        self.currentPage = 1;
        [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
    }
    else
    {
        [self stopLoading];
    }
    // Refresh action!
    //[self refresh];
}

- (void)stopLoading {
    isLoading = NO;
    // Hide the header
    [UIView animateWithDuration:0.3 animations:^{
        categoryTableView.contentInset = UIEdgeInsetsZero;
        [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
    }
                     completion:^(BOOL finished) {
                         [self performSelector:@selector(stopLoadingComplete)];
                     }];
}

- (void)stopLoadingComplete {
    // Reset the header
    refreshLabel.text = self.textPull;
    refreshArrow.hidden = NO;
    [refreshSpinner stopAnimating];
}

-(void)showLoginAlert{
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Please Login to mark this post as favourite."];
    alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
    alertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
    
    [alertView addButtonWithTitle:@"LOGIN"
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alert) {
                              DebugLog(@"LOGIN Clicked");
                              [self showLoginModal];
                          }];
    [alertView addButtonWithTitle:@"NOT NOW"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alert) {
                              DebugLog(@"NOT NOW Clicked");
                          }];
    [alertView show];
}
- (void)likeBtnPressed:(id)sender event:(id)event {
    DebugLog(@"likeBtnPressed");
    if([IN_APP_DELEGATE networkavailable])
    {
        NSSet *touches = [event allTouches];
        UITouch *touch = [touches anyObject];
        CGPoint currentTouchPosition = [touch locationInView:self.categoryTableView];
        NSIndexPath *indexPath = [self.categoryTableView indexPathForRowAtPoint:currentTouchPosition];
        if (indexPath != nil) {
            if(![INUserDefaultOperations isCustomerLoggedIn])
            {
                [self showLoginAlert];
            } else {
                currentSelectedRow = indexPath.row;
                INBroadcastDetailObj *tempBroadCastObj = (INBroadcastDetailObj *)[catgArray objectAtIndex:indexPath.row];
                DebugLog(@"user_like %@ post_id %d",tempBroadCastObj.user_like,tempBroadCastObj.ID);
                if ([tempBroadCastObj.user_like intValue]) {
                    [self sendPostUnLikeRequest];
                }else{
                    [self sendPostLikeRequest];
//                    if ([[INUserDefaultOperations getCustomerLoginFrom] isEqualToString:FACEBOOKSERVER] && [INUserDefaultOperations isOpenGraphShareEnabled]) {
//                        SIAlertView *sialertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Do you want to share this on your Facebook timeline?"];
//                        sialertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
//                        sialertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
//                        [sialertView addButtonWithTitle:@"YES"
//                                                   type:SIAlertViewButtonTypeDestructive
//                                                handler:^(SIAlertView *alert) {
//                                                    DebugLog(@"YES");
//                                                    [self sendPostLikeRequest];
//                                                    
//                                                    NSString *deepLinkingUrl = [FacebookOpenGraphAPI createDeepLinkWithActiveAreaName:[IN_APP_DELEGATE getActiveSlugFromAreaTable] storeName:tempBroadCastObj.name storeId:[NSString stringWithFormat:@"%d",tempBroadCastObj.place_ID]];
//                                                    NSString *postTitle         = tempBroadCastObj.title;
//                                                    NSString *postDescription   = tempBroadCastObj.description;
//                                                    NSString *postimageUrl      = tempBroadCastObj.image_url1;
//                                                    if (postimageUrl != nil && ![postimageUrl isEqualToString:@""]) {
//                                                        if ([postimageUrl hasPrefix:@"http"]) {
//                                                            [FacebookOpenGraphAPI likeContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:postimageUrl deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
//                                                        }else{
//                                                            [FacebookOpenGraphAPI likeContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:THUMBNAIL_LINK(postimageUrl) deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
//                                                        }
//                                                    }else{
//                                                        [FacebookOpenGraphAPI likeContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:@"" deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
//                                                    }
//
//                                                }];
//                        [sialertView addButtonWithTitle:@"NOT NOW"
//                                                   type:SIAlertViewButtonTypeCancel
//                                                handler:^(SIAlertView *alert) {
//                                                    DebugLog(@"NO");
//                                                    [self sendPostLikeRequest];
//                                                }];
//                        [sialertView show];
//                    }else{
//                        [self sendPostLikeRequest];
//                    }
                }
            }
        }
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

- (void)shareBtnPressed:(id)sender event:(id)event {
    DebugLog(@"shareBtnPressed");
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.categoryTableView];
    NSIndexPath *indexPath = [self.categoryTableView indexPathForRowAtPoint:currentTouchPosition];
    if (indexPath != nil) {
        currentSelectedRow = indexPath.row;
        UIActionSheet *shareActionSheet = [[UIActionSheet alloc] initWithTitle:@"Share via" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"SMS",@"Email",@"WhatsApp",@"Facebook",@"Twitter", nil];
        [shareActionSheet showInView:self.view];
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSString *title = [actionSheet buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"SMS"])
    {
        [self smsShareClicked];
    }
    else if([title isEqualToString:@"Email"])
    {
        [self emailShareClicked];
    }else if([title isEqualToString:@"WhatsApp"])
    {
        [self whatsAppShareClicked];
    }
    else if([title isEqualToString:@"Facebook"])
    {
        [self facebookShareClicked];
    } else if([title isEqualToString:@"Twitter"])
    {
        [self twitterShareClicked];
    }
}
-(void)smsShareClicked{
    if ([MFMessageComposeViewController canSendText]) {
        NSString *message = [self getPostDetailsMessageBody:@"sms"];
        MFMessageComposeViewController *messageComposer = [[MFMessageComposeViewController alloc] init];
        messageComposer.messageComposeDelegate = self;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
             messageComposer.navigationBar.tintColor  = [UIColor whiteColor];
        }
        [messageComposer setBody:message];
        [messageComposer setRecipients:nil];
        [self presentViewController:messageComposer animated:YES completion:nil];
    }else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"You may not have set up sms service on your device.\nPlease check and try again."
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}
#pragma mark MFMessageComposeViewController Delegate Methods
-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
		case MessageComposeResultCancelled:
            DebugLog(@"SMS Cancelled");
			break;
		case MessageComposeResultFailed:
        {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Unknown Error\nSMS failed"
														   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
        }
			break;
		case MessageComposeResultSent:
        {
            DebugLog(@"SMS Sent");
        }
			break;
		default:
			break;
	}
    [self dismissViewControllerAnimated:YES completion:^{
        if (result == MessageComposeResultSent) {
            [self sendPostShareRequest:@"sms"];
        }
    }];
}

-(void)emailShareClicked{
    if ([MFMailComposeViewController canSendMail]) {
        INBroadcastDetailObj *tempBroadCastObj = (INBroadcastDetailObj *)[catgArray objectAtIndex:currentSelectedRow];
        NSString *message = [self getPostDetailsMessageBody:@"email"];
        
        MFMailComposeViewController *emailComposer = [[MFMailComposeViewController alloc] init];
        emailComposer.mailComposeDelegate = self;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            emailComposer.navigationBar.tintColor  = [UIColor whiteColor];
        }
        [emailComposer setToRecipients:nil];
        
        if (tempBroadCastObj.name != nil && ![tempBroadCastObj.name isEqualToString:@""]) {
            [emailComposer setSubject:[NSString stringWithFormat:@"%@",tempBroadCastObj.name]];
        }else{
            [emailComposer setSubject:@""];
        }
        if (tempBroadCastObj.image_url1 != nil && ![tempBroadCastObj.image_url1 isEqualToString:@""]) {
            NSString *htmlMessage = [message stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
            DebugLog(@"%@--%@",message,htmlMessage);
            [emailComposer setMessageBody:[NSString stringWithFormat:@"%@ <br><br><img src=%@ alt=%@/>",htmlMessage,THUMBNAIL_LINK(tempBroadCastObj.image_url1),tempBroadCastObj.title] isHTML:YES];
        }else{
            [emailComposer setMessageBody:message isHTML:NO];
        }
        emailComposer.toolbar.tag = 2;
        [self presentViewController:emailComposer animated:YES completion:nil];
    }else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"You may not have set up mail service on your device or You may not connected to internet.\nPlease check and try again."
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}
#pragma mark Mail Compose Delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    if (result == MFMailComposeResultFailed) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                        message:NSLocalizedString(@"Email failed to send. Please try again.", nil)
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", nil) otherButtonTitles:nil];
		[alert show];
    }
    [self dismissViewControllerAnimated:YES completion:^{
        if (controller.toolbar.tag == 2 && result == MFMailComposeResultSent) {
            [self sendPostShareRequest:@"email"];
        }
    }];
}

-(void)whatsAppShareClicked{
    //    NSString *shareMSGBody = [[NSString stringWithFormat:@"Store Name: %@\nOffer: %@\nDescription: %@",storeName,postTitle,postDescription] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *message = [self getPostDetailsMessageBody:@"whatsapp"];
    NSString *urlString = [NSString stringWithFormat:@"whatsapp://send?text=%@",[message stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURL *whatsappURL = [NSURL URLWithString:urlString];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [[UIApplication sharedApplication] openURL: whatsappURL];
        [self sendPostShareRequest:@"whatsapp"];
    }
}

-(void)twitterShareClicked{
    if ([IN_APP_DELEGATE networkavailable]) {
        NSString *message = [self getPostDetailsMessageBody:@"twitter"];
        INBroadcastDetailObj *tempBroadCastObj = (INBroadcastDetailObj *)[catgArray objectAtIndex:currentSelectedRow];
        NSString *postimageUrl = tempBroadCastObj.image_url1;
        
        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
            
            SLComposeViewController *socialComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
            if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
            {
                if ([message length] > 140) {
                    [socialComposer setInitialText:[message substringToIndex:140]];
                }else{
                    [socialComposer setInitialText:message];
                }
            }
            else{
                [socialComposer setInitialText:message];
            }
            if(postimageUrl != nil && ![postimageUrl isEqualToString:@""])
            {
                UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:THUMBNAIL_LINK(postimageUrl)]]];
                [socialComposer addImage:image];
            }
            [socialComposer setCompletionHandler:^(SLComposeViewControllerResult result){
                switch (result) {
                    case SLComposeViewControllerResultCancelled:
                        DebugLog(@"SLComposeViewControllerResultCancelled");
                        break;
                    case SLComposeViewControllerResultDone:
                        DebugLog(@"SLComposeViewControllerResultDone");
                        [self sendPostShareRequest:@"twitter"];
                        break;
                    default:
                        DebugLog(@"SLComposeViewControllerResultFailed");
                        break;
                }
                [self dismissViewControllerAnimated:YES completion:NULL];
            }];
            [self presentViewController:socialComposer animated:YES completion:Nil];
        }
        else{
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:ALERT_TITLE
                                      message:@"You may not have set up twitter service on your device.Please check and try again."
                                      delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [alertView show];
        }
    }else{
        [INUserDefaultOperations showOfflineAlert];
    }
}

-(void)facebookShareClicked{
    if ([IN_APP_DELEGATE networkavailable]) {
        [self sendPostShareRequest:@"facebook"];
        NSString *message = [self getPostDetailsMessageBody:@"facebook"];
        DebugLog(@"message %@",message);
        INBroadcastDetailObj *tempBroadCastObj = (INBroadcastDetailObj *)[catgArray objectAtIndex:currentSelectedRow];
        NSString *postimageUrl  = tempBroadCastObj.image_url1;
        NSString *storeName     = tempBroadCastObj.name;
        NSInteger placeId       = tempBroadCastObj.place_ID;

        NSString *deepLinkingUrl = [FacebookOpenGraphAPI createDeepLinkWithActiveAreaName:[IN_APP_DELEGATE getActiveSlugFromAreaTable] storeName:storeName storeId:[NSString stringWithFormat:@"%d",placeId]];
        
        if (postimageUrl != nil && ![postimageUrl isEqualToString:@""]) {
            if ([postimageUrl hasPrefix:@"http"]) {
                [FacebookShareAPI shareContentWithTitle:[NSString stringWithFormat:@"%@ - Offer\n",storeName] description:message image:postimageUrl deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
            }else{
                [FacebookShareAPI shareContentWithTitle:[NSString stringWithFormat:@"%@ - Offer\n",storeName] description:message image:THUMBNAIL_LINK(postimageUrl) deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
            }
        }else{
            [FacebookShareAPI shareContentWithTitle:[NSString stringWithFormat:@"%@ - Offer\n",storeName] description:message image:@"" deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
        }
    }else{
        [INUserDefaultOperations showOfflineAlert];
    }
    
//    if ([IN_APP_DELEGATE networkavailable]) {
//        NSString *message = [self getPostDetailsMessageBody:@"facebook"];
//        INBroadcastDetailObj *tempBroadCastObj = (INBroadcastDetailObj *)[catgArray objectAtIndex:currentSelectedRow];
//        NSString *postimageUrl = tempBroadCastObj.image_url1;
//        if (SYSTEM_VERSION_LESS_THAN(@"6.0"))
//        {
//            FaceBookShareViewController *facebookViewComposer = [[FaceBookShareViewController alloc] initWithNibName:@"FaceBookShareViewController" bundle:nil];
//            facebookViewComposer.fbdelegate = self;
//            facebookViewComposer.title = @"FACEBOOK SHARE";
//            facebookViewComposer.FBtitle = ALERT_TITLE;
//            facebookViewComposer.postMessageText =  message;
//            facebookViewComposer.FBtPic = THUMBNAIL_LINK(postimageUrl);
//            UINavigationController *fbnavBar=[[UINavigationController alloc]initWithRootViewController:facebookViewComposer];
//            [self.navigationController presentViewController:fbnavBar animated:YES completion:nil];
//        }else{
//            if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
//                
//                SLComposeViewController *socialComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
//                [socialComposer setInitialText:message];
//                if(postimageUrl != nil && ![postimageUrl isEqualToString:@""])
//                {
//                    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:THUMBNAIL_LINK(postimageUrl)]]];
//                    [socialComposer addImage:image];
//                }
//                [socialComposer setCompletionHandler:^(SLComposeViewControllerResult result){
//                    switch (result) {
//                        case SLComposeViewControllerResultCancelled:
//                            DebugLog(@"SLComposeViewControllerResultCancelled");
//                            break;
//                        case SLComposeViewControllerResultDone:
//                            DebugLog(@"SLComposeViewControllerResultDone");
//                            [self sendPostShareRequest:@"facebook"];
//                            break;
//                        default:
//                            DebugLog(@"SLComposeViewControllerResultFailed");
//                            break;
//                    }
//                    [self dismissViewControllerAnimated:YES completion:NULL];
//                }];
//                [self presentViewController:socialComposer animated:YES completion:Nil];
//            }
//            else{
//                UIAlertView *alertView = [[UIAlertView alloc]
//                                          initWithTitle:ALERT_TITLE
//                                          message:@"You may not have set up facebook service on your device.Please check and try again."
//                                          delegate:nil
//                                          cancelButtonTitle:@"OK"
//                                          otherButtonTitles:nil];
//                [alertView show];
//            }
//        }
//    }else{
//        [INUserDefaultOperations showOfflineAlert];
//    }
}

-(void)faceBookCompletionCallBack:(int)result
{
    DebugLog(@"Customer : faceBookCompletionCallBack %d",result);
    if (result == FACEBOOK_POST_SUCCESS) {
        [self sendPostShareRequest:@"facebook"];
    }
}


#pragma AFNetworking delegate methods
-(void)sendPostLikeRequest
{
    DebugLog(@"========================sendPostLikeRequest========================");
    INBroadcastDetailObj *tempBroadCastObj = (INBroadcastDetailObj *)[catgArray objectAtIndex:currentSelectedRow];
    
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[INUserDefaultOperations getCustomerAuthId] forKey:@"user_id"];
    [params setObject:[INUserDefaultOperations getCustomerAuthCode] forKey:@"auth_id"];
    [params setObject:[NSString stringWithFormat:@"%d",tempBroadCastObj.ID] forKey:@"post_id"];
    [httpClient postPath:LIKE_BROADCAST_OF_STORE parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            if([json  objectForKey:@"message"] != [NSNull null] && [[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
                tempBroadCastObj.user_like = @"1";
                tempBroadCastObj.total_like = [NSString stringWithFormat:@"%d",[tempBroadCastObj.total_like intValue] + 1];
//                if([json  objectForKey:@"message"] != [NSNull null] && ![[json objectForKey:@"message"] isEqualToString:@""]){
//                    NSString *message = [json objectForKey:@"message"];
//                    [INUserDefaultOperations showSIAlertView:message];
//                }
            } else {
                NSString* code = [json objectForKey:@"code"];
                if([code isEqualToString:CUSTOMER_ALREADY_MADE_POST_FAV])
                {
                    tempBroadCastObj.user_like = @"1";
                    if ([tempBroadCastObj.total_like intValue] == 0) {
                        tempBroadCastObj.total_like = @"1";
                    }
//                    if([json  objectForKey:@"message"] != [NSNull null] && ![[json objectForKey:@"message"] isEqualToString:@""]){
//                        NSString *message = [json objectForKey:@"message"];
//                        [INUserDefaultOperations showSIAlertView:message];
//                    }
                }
                else if([code isEqualToString:CUSTOMER_INVALID_USER_AUTH_CODE])
                {
                    if([INUserDefaultOperations isCustomerLoggedIn]){
                        [INUserDefaultOperations clearCustomerDetails];
                        NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:IN_CUSTOMER_LOGIN_LOGOUT_KEY];
                        [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_LOGIN_LOGOUT_NOTIFICATION object:nil userInfo:dictionary];
                        [self showLoginAlert];
                    }
                }else{
                    if([json  objectForKey:@"message"] != [NSNull null] && ![[json objectForKey:@"message"] isEqualToString:@""]){
                        NSString *message = [json objectForKey:@"message"];
                        [INUserDefaultOperations showSIAlertView:message];
                    }
                }
            }
            [catgArray replaceObjectAtIndex:currentSelectedRow withObject:tempBroadCastObj];
            [categoryTableView reloadData];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}

-(void)sendPostUnLikeRequest
{
    INBroadcastDetailObj *tempBroadCastObj = (INBroadcastDetailObj *)[catgArray objectAtIndex:currentSelectedRow];
    
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    [httpClient setDefaultHeader:@"user_id" value:[INUserDefaultOperations getCustomerAuthId]];
    [httpClient setDefaultHeader:@"auth_id" value:[INUserDefaultOperations getCustomerAuthCode]];
    [httpClient deletePath:UNLIKE_BROADCAST_OF_STORE(tempBroadCastObj.ID) parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            if([json  objectForKey:@"message"] != [NSNull null] && [[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
//                NSString* message = [json objectForKey:@"message"];
//                [INUserDefaultOperations showAlert:message];
                tempBroadCastObj.user_like = @"0";
                if ([tempBroadCastObj.total_like intValue] > 0) {
                    tempBroadCastObj.total_like = [NSString stringWithFormat:@"%d",[tempBroadCastObj.total_like intValue] - 1];
                }
                [catgArray replaceObjectAtIndex:currentSelectedRow withObject:tempBroadCastObj];
                [categoryTableView reloadData];
            } else {
                NSString* code = [json objectForKey:@"code"];
                if([code isEqualToString:CUSTOMER_INVALID_USER_AUTH_CODE])
                {
                    if([INUserDefaultOperations isCustomerLoggedIn]){
                        [INUserDefaultOperations clearCustomerDetails];
                        NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:IN_CUSTOMER_LOGIN_LOGOUT_KEY];
                        [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_LOGIN_LOGOUT_NOTIFICATION object:nil userInfo:dictionary];
                        [self showLoginAlert];
                    }
                }else{
                    if([json  objectForKey:@"message"] != [NSNull null] && ![[json objectForKey:@"message"] isEqualToString:@""]){
                        NSString *message = [json objectForKey:@"message"];
                        [INUserDefaultOperations showSIAlertView:message];
                    }
                }
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}

-(void)sendPostShareRequest:(NSString *)viaString{
    DebugLog(@"shareMsg %@ ",viaString);
    
    INBroadcastDetailObj *tempBroadCastObj = (INBroadcastDetailObj *)[catgArray objectAtIndex:currentSelectedRow];

    if ([[INUserDefaultOperations getCustomerLoginFrom] isEqualToString:FACEBOOKSERVER] && [INUserDefaultOperations isOpenGraphShareEnabled] && ![viaString isEqualToString:@"facebook"]) {
        SIAlertView *sialertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Do you want to share this on your Facebook timeline?"];
        sialertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
        sialertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
        [sialertView addButtonWithTitle:@"YES"
                                   type:SIAlertViewButtonTypeDestructive
                                handler:^(SIAlertView *alert) {
                                    DebugLog(@"YES");
                                    NSString *deepLinkingUrl = [FacebookOpenGraphAPI createDeepLinkWithActiveAreaName:[IN_APP_DELEGATE getActiveSlugFromAreaTable] storeName:tempBroadCastObj.name storeId:[NSString stringWithFormat:@"%d",tempBroadCastObj.place_ID]];
                                    NSString *postTitle         = tempBroadCastObj.title;
                                    NSString *postDescription   = tempBroadCastObj.description;
                                    NSString *postimageUrl      = tempBroadCastObj.image_url1;
                                    if (postimageUrl != nil && ![postimageUrl isEqualToString:@""]) {
                                        if ([postimageUrl hasPrefix:@"http"]) {
                                            [FacebookOpenGraphAPI shareContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:postimageUrl deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
                                        }else{
                                            [FacebookOpenGraphAPI shareContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:THUMBNAIL_LINK(postimageUrl) deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
                                        }
                                    }else{
                                        [FacebookOpenGraphAPI shareContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:@"" deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
                                    }
                                }];
        [sialertView addButtonWithTitle:@"NOT NOW"
                                   type:SIAlertViewButtonTypeCancel
                                handler:^(SIAlertView *alert) {
                                    DebugLog(@"NO");
                                }];
        [sialertView show];
    }
    
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    //    if ([INUserDefaultOperations isCustomerLoggedIn]) {
    //        [params setObject:[INUserDefaultOperations getCustomerAuthId] forKey:@"user_id"];
    //        [params setObject:[INUserDefaultOperations getCustomerAuthCode] forKey:@"auth_id"];
    //    }
    [params setObject:[NSString stringWithFormat:@"%d",tempBroadCastObj.ID] forKey:@"post_id"];
    [params setObject:viaString forKey:@"via"];
    
    [httpClient postPath:SHARE_STORE_POST parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            if([json  objectForKey:@"message"] != [NSNull null] && [[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
                //                NSString* message = [json objectForKey:@"message"];
                //                [INUserDefaultOperations showAlert:message];
                tempBroadCastObj.total_share = [NSString stringWithFormat:@"%d",[tempBroadCastObj.total_share intValue] + 1];
                [catgArray replaceObjectAtIndex:currentSelectedRow withObject:tempBroadCastObj];
                [categoryTableView reloadData];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}



-(void)showLoginModal{
    INCustomerLoginViewController *custLoginController = [[INCustomerLoginViewController alloc] initWithNibName:@"INCustomerLoginViewController" bundle:nil] ;
    custLoginController.delegate = self;
    UINavigationController *loginnavBar=[[UINavigationController alloc]initWithRootViewController:custLoginController];
    UIButton *moreButton1 =  [UIButton buttonWithType:UIButtonTypeCustom];
    [moreButton1 setTitle:@"Cancel" forState:UIControlStateNormal];
    moreButton1.titleLabel.font = [UIFont systemFontOfSize:17.0];
    //[moreButton1 setBackgroundImage:[UIImage imageNamed:@"Transparent_Button.png"] forState:UIControlStateNormal];
    [moreButton1 addTarget:self action:@selector(dismissLoginView) forControlEvents:UIControlEventTouchUpInside];
    [moreButton1 setFrame:CGRectMake(250, 7, 70, 32)];
    [loginnavBar.navigationBar addSubview:moreButton1];
    [loginnavBar.navigationBar setTranslucent:NO];
    [self.navigationController presentViewController:loginnavBar animated:YES completion:nil];
}

-(void)dismissLoginView
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"Dismiss");
    }];
}

-(NSString *)getPostDetailsMessageBody:(NSString *)shareVia{
    INBroadcastDetailObj *tempBroadCastObj = (INBroadcastDetailObj *)[catgArray objectAtIndex:currentSelectedRow];

    NSMutableString *contactString = [[NSMutableString alloc] initWithString:@""];
    if (tempBroadCastObj.mob_no1 != nil && ![tempBroadCastObj.mob_no1 isEqualToString:@""]) {
        [contactString appendString:[NSString stringWithFormat:@"%@",tempBroadCastObj.mob_no1]];
    }
    if (tempBroadCastObj.mob_no2 != nil && ![tempBroadCastObj.mob_no2 isEqualToString:@""]) {
        if ([contactString isEqualToString:@""]) {
            [contactString appendString:tempBroadCastObj.mob_no2];
        }else{
            [contactString appendString:[NSString stringWithFormat:@", %@", tempBroadCastObj.mob_no2]];
        }
    }
    if (tempBroadCastObj.mob_no3 != nil && ![tempBroadCastObj.mob_no3 isEqualToString:@""]) {
        if ([contactString isEqualToString:@""]) {
            [contactString appendString:tempBroadCastObj.mob_no3];
        }else{
            [contactString appendString:[NSString stringWithFormat:@", %@", tempBroadCastObj.mob_no3]];
        }
    }
    if (tempBroadCastObj.tel_no1 != nil && ![tempBroadCastObj.tel_no1 isEqualToString:@""]) {
        if ([contactString isEqualToString:@""]) {
            [contactString appendString:tempBroadCastObj.tel_no1];
        }else{
            [contactString appendString:[NSString stringWithFormat:@", %@", tempBroadCastObj.tel_no1]];
        }
    }
    if (tempBroadCastObj.tel_no2 != nil && ![tempBroadCastObj.tel_no2 isEqualToString:@""]) {
        if ([contactString isEqualToString:@""]) {
            [contactString appendString:tempBroadCastObj.tel_no2];
        }else{
            [contactString appendString:[NSString stringWithFormat:@", %@", tempBroadCastObj.tel_no2]];
        }
    }
    if (tempBroadCastObj.tel_no3 != nil && ![tempBroadCastObj.tel_no3 isEqualToString:@""]) {
        if ([contactString isEqualToString:@""]) {
            [contactString appendString:tempBroadCastObj.tel_no3];
        }else{
            [contactString appendString:[NSString stringWithFormat:@", %@", tempBroadCastObj.tel_no3]];
        }
    }
    
    NSString *message = [IN_APP_DELEGATE getPostDetailsMessageBody:tempBroadCastObj.name offerTitle:tempBroadCastObj.title offerDescription:tempBroadCastObj.description isOffered:tempBroadCastObj.is_offered offerDateTime:tempBroadCastObj.offerdate contact:contactString shareVia:shareVia];
    return message;
}

@end
