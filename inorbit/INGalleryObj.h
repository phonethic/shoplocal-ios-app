//
//  INGalleryObj.h
//  shoplocal
//
//  Created by Sagar Mody on 24/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface INGalleryObj : NSObject
{}
@property (nonatomic, copy) NSString *galleryID;
@property (nonatomic, copy) NSString *image_date;
@property (nonatomic, copy) NSString *image_url;
@property (nonatomic, copy) NSString *place_id;
@property (nonatomic, copy) NSString *thumb_url;
@property (nonatomic, copy) NSString *title;

@end
