//
//  INTourViewController.h
//  shoplocal
//
//  Created by Kirti Nikam on 21/11/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "INCustomerLoginViewController.h"

@interface INTourViewController : UIViewController<INCustomerLoginViewControllerDelegate>

@property (strong, nonatomic) NSMutableArray *textArray;

@property (strong, nonatomic) IBOutlet UIView *animationView;
@property (strong, nonatomic) IBOutlet UIImageView *logoImageView;
@property (strong, nonatomic) IBOutlet UILabel *lblHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblDescription;
@property (strong, nonatomic) IBOutlet UITextView *txtDescription;

@property (strong, nonatomic) IBOutlet UIView *loginView;
@property (strong, nonatomic) IBOutlet UIImageView *shoplocalTextImageView;

@property (strong, nonatomic) IBOutlet UIButton *fbLoginBtn;
@property (strong, nonatomic) IBOutlet UIButton *signUpShoplocalBtn;
@property (strong, nonatomic) IBOutlet UIButton *skipBtn;
@property (strong, nonatomic) IBOutlet UIButton *sellBtn;

- (IBAction)fbLoginBtnPressed:(id)sender;
- (IBAction)signUpShoplocalBtnPressed:(id)sender;
- (IBAction)skipBtnPressed:(id)sender;
- (IBAction)sellBtnPressed:(id)sender;
@end
