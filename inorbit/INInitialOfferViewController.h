//
//  INInitialOfferViewController.h
//  shoplocal
//
//  Created by Rishi on 04/10/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface INInitialOfferViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIView *messageView;
@property (strong, nonatomic) IBOutlet UILabel *messagelbl;
@property (strong, nonatomic) IBOutlet UIButton *postOfferBtn;
@property (strong, nonatomic) IBOutlet UIButton *skipBtn;

- (IBAction)skipBtnPressed:(id)sender;
- (IBAction)postOfferBtnPressed:(id)sender;

@end
