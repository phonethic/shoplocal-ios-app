//
//  INAllPostsViewController.h
//  shoplocal
//
//  Created by Rishi on 20/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SIAlertView.h"

@class INBroadcastDetailObj;
@interface INAllPostsViewController : UIViewController
{
    INBroadcastDetailObj *broadcastObj;
    MBProgressHUD *hud;
    SIAlertView *sialertView;
    BOOL isDragging;
    BOOL isLoading;
    BOOL isAdding;
    double lastContentOffset;
}
@property (strong, nonatomic) IBOutlet UITableView *postsTableView;
@property (strong, nonatomic) NSMutableArray *broadcastArray;
@property (nonatomic, readwrite) NSInteger storeId;
@property (nonatomic, copy) NSString *storeName;
@property (nonatomic, copy) NSString *imageUrl;

@property (nonatomic, retain) UIView *refreshHeaderView;
@property (nonatomic, retain) UILabel *refreshLabel;
@property (nonatomic, retain) UIImageView *refreshArrow;
@property (nonatomic, retain) UIActivityIndicatorView *refreshSpinner;

//*****Pull TO Refresh
@property (nonatomic, copy) NSString *textPull;
@property (nonatomic, copy) NSString *textRelease;
@property (nonatomic, copy) NSString *textLoading;
@end
