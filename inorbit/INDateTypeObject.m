//
//  INDateTypeObject.m
//  shoplocal
//
//  Created by Kirti Nikam on 07/10/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INDateTypeObject.h"

@implementation INDateTypeObject
@synthesize date,date_type,count;

#pragma mark NSCoding Protocol
-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.date forKey:@"date"];
    [aCoder encodeObject:self.date_type forKey:@"date_type"];
    [aCoder encodeObject:self.count forKey:@"count"];
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (self != nil) {
        self.date = [aDecoder decodeObjectForKey:@"date"];
        self.date_type = [aDecoder decodeObjectForKey:@"date_type"];
        self.count = [aDecoder decodeObjectForKey:@"count"];
    }
    return self;
}
@end
