//
//  MerchantSideMenuViewController.m
//  shoplocal
//
//  Created by Kirti Nikam on 01/11/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "MerchantSideMenuViewController.h"
#import "INAppDelegate.h"
#import "CommonCallback.h"

#import "INMerchantLoginViewController.h"
#import "INFBWebViewController.h"
#import "INMerchantViewController.h"
#import "INAddUpdateStoreViewController.h"
#import "INStoreListViewController.h"
#import "INBroadcastViewController.h"
#import "INMultiSelectStoreListViewController.h"
#import "INAllPostsViewController.h"
#import "INStoreGalleryViewController.h"
#import "INMerchantReportsViewController.h"

///////////////////////////////////////////////////////////////
#define SECTION0 @"  SHOPLOCAL"
#define REPORTS @"Reports"
#define TALK_NOW @"Talk Now"
#define VIEW_POST @"View Post"
///////////////////////////////////////////////////////////////
#define SECTION1 @"  OTHER STUFF"
#define ADD_STORE @"Add Store"
#define EDIT_STORE @"Edit Store"
#define MANAGE_GALLARY @"Manage Gallery"
///////////////////////////////////////////////////////////////
#define SECTION2 @"  MY SHOPLOCAL"
#define LOGIN @"Login"
#define LOGOUT @"Logout"
#define LOGIN_TO_BUSINESS @"Login to Customer"
///////////////////////////////////////////////////////////////
#define SECTION3 @"  ABOUT SHOPLOCAL"
#define CONTACTUS @"Contact Us"
#define FACEBOOK @"Facebook"
#define TWITTER @"Twitter"
///////////////////////////////////////////////////////////////

@interface MerchantSideMenuViewController ()

@end

@implementation MerchantSideMenuViewController
@synthesize sideMenu;
@synthesize sideMenuDict;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSMutableArray *myShoplocalArray   = [[NSMutableArray alloc] initWithArray:[sideMenuDict objectForKey:SECTION2]];
    if([INUserDefaultOperations isMerchantLoggedIn])
    {
        if(NSNotFound == [myShoplocalArray indexOfObject:LOGOUT]) {
            DebugLog(@"Merchant : logout not found ");
            [myShoplocalArray replaceObjectAtIndex:[myShoplocalArray indexOfObject:LOGIN] withObject:LOGOUT];
        }
    }else{
        if(NSNotFound != [myShoplocalArray indexOfObject:LOGOUT]) {
            [myShoplocalArray replaceObjectAtIndex:[myShoplocalArray indexOfObject:LOGOUT] withObject:LOGIN];
        }
    }
    [sideMenuDict setObject:myShoplocalArray forKey:SECTION2];
    [self.tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:currentRowIndex inSection:currentSectionIndex] animated:NO scrollPosition:UITableViewScrollPositionNone];
}

- (void) viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.backgroundColor  = [UIColor whiteColor];
    //    self.tableView.separatorColor   = [UIColor colorWithPatternImage:[self separatorImage]];
    self.tableView.scrollEnabled    = YES;
    
    sideMenuDict = [[NSMutableDictionary alloc] init];
    [sideMenuDict setObject:[[NSArray alloc]initWithObjects:REPORTS,TALK_NOW,VIEW_POST, nil] forKey:SECTION0];
    [sideMenuDict setObject:[[NSArray alloc]initWithObjects:ADD_STORE,EDIT_STORE,MANAGE_GALLARY, nil] forKey:SECTION1];
    [sideMenuDict setObject:[[NSArray alloc]initWithObjects:LOGIN,LOGIN_TO_BUSINESS, nil] forKey:SECTION2];
    [sideMenuDict setObject:[[NSArray alloc]initWithObjects:CONTACTUS,FACEBOOK,TWITTER, nil] forKey:SECTION3];
    
    //    CGRect searchBarFrame = CGRectMake(0, 0, self.tableView.frame.size.width, 45.0);
    //    self.searchBar = [[UISearchBar alloc] initWithFrame:searchBarFrame];
    //    self.searchBar.delegate = self;
    //
    //    self.tableView.tableHeaderView = self.searchBar;
    
    UINavigationBar *tempNavigationBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [tempNavigationBar setBarStyle:UIBarStyleDefault];
    tempNavigationBar.tintColor = BROWN_COLOR;
    
    UINavigationItem *navItem = [UINavigationItem alloc];
    navItem.title = @"Menu";
    [tempNavigationBar pushNavigationItem:navItem animated:false];
    self.tableView.tableHeaderView = tempNavigationBar;
    
    currentSectionIndex = 0;
    preSectionIndex = 0;
    currentRowIndex = 0;
    preRowIndex = 0;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLoginLogoutNotification:) name:IN_MERCHANT_LOGIN_NOTIFICATION object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
#pragma mark -
#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[sideMenuDict allKeys] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *customHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,50)];
    customHeaderView.clipsToBounds = YES;
    customHeaderView.backgroundColor = [UIColor whiteColor];
    
    UILabel *lblHeader = [[UILabel alloc] initWithFrame:CGRectMake(0 ,2, 320, 46)];
    lblHeader.textColor = DEFAULT_COLOR;
    lblHeader.font = DEFAULT_BOLD_FONT(18.0);
    lblHeader.backgroundColor = [UIColor whiteColor];
    lblHeader.textAlignment = NSTextAlignmentLeft;
    switch (section) {
        case 0:
            lblHeader.text = SECTION0;
            break;
        case 1:
            lblHeader.text = SECTION1;
            break;
        case 2:
            lblHeader.text = SECTION2;
            break;
        case 3:
            lblHeader.text = SECTION3;
            break;
    }
    [customHeaderView addSubview:lblHeader];
    
    UIView *bottomlineView = [[UIView alloc] initWithFrame:CGRectMake(0,CGRectGetMaxY(lblHeader.frame),320,2)];
    bottomlineView.backgroundColor = BROWN_COLOR;
    [customHeaderView addSubview:bottomlineView];
    
    return customHeaderView;
}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    switch (section) {
//        case 0:
//            return SECTION0;
//            break;
//        case 1:
//            return SECTION1;
//            break;
//        case 2:
//            return SECTION2;
//            break;
//        case 3:
//            return SECTION3;
//            break;
//    }
//    return @"";
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return [[sideMenuDict objectForKey:SECTION0] count];
            break;
        case 1:
            return [[sideMenuDict objectForKey:SECTION1] count];
            break;
        case 2:
            return [[sideMenuDict objectForKey:SECTION2] count];
            break;
        case 3:
            return [[sideMenuDict objectForKey:SECTION3] count];
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"merchantSideCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        cell.textLabel.backgroundColor = [UIColor whiteColor];
        
        //        cell.textLabel.shadowColor = [UIColor blackColor];
        //        cell.textLabel.shadowOffset = CGSizeMake(0, -1);
        cell.textLabel.font = DEFAULT_FONT(15);
        cell.textLabel.textColor = BROWN_COLOR;
        cell.textLabel.highlightedTextColor = [UIColor whiteColor];
        
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = BROWN_OFFWHITE_COLOR;
        cell.selectedBackgroundView = bgColorView;
        
    }
    NSString *text = @"";
    switch (indexPath.section) {
        case 0:
            text = [[sideMenuDict objectForKey:SECTION0] objectAtIndex:indexPath.row];
            break;
        case 1:
            text = [[sideMenuDict objectForKey:SECTION1] objectAtIndex:indexPath.row];
            break;
        case 2:
            text = [[sideMenuDict objectForKey:SECTION2] objectAtIndex:indexPath.row];
            break;
        case 3:
            text = [[sideMenuDict objectForKey:SECTION3] objectAtIndex:indexPath.row];
            break;
    }
    cell.textLabel.text = text;
    
    if ([text isEqualToString:REPORTS]) {
        cell.imageView.image = [UIImage imageNamed:@"Carousel_Reports.png"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Carousel_Reports.png"];
    }else if ([text isEqualToString:TALK_NOW]) {
        cell.imageView.image = [UIImage imageNamed:@"Carousel_NewPost.png"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Carousel_NewPost.png"];
    }else if ([text isEqualToString:VIEW_POST]) {
        cell.imageView.image = [UIImage imageNamed:@"Carousel_ViewPost.png"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Carousel_ViewPost.png"];
    }
    
    
    else if ([text isEqualToString:ADD_STORE]) {
        cell.imageView.image = [UIImage imageNamed:@"Grid_Addstore.png"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Grid_Addstore.png"];
    }
    else if ([text isEqualToString:EDIT_STORE]) {
        cell.imageView.image = [UIImage imageNamed:@"Grid_Editstore.png"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Grid_Editstore.png"];
    }
    else if ([text isEqualToString:MANAGE_GALLARY]) {
        cell.imageView.image = [UIImage imageNamed:@"Grid_Gallery.png"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Grid_Gallery.png"];
    }
    
    else if ([text isEqualToString:LOGIN]) {
        cell.imageView.image = [UIImage imageNamed:@"Grid_Login.png"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Grid_Login.png"];
    }
    else if ([text isEqualToString:LOGOUT]) {
        cell.imageView.image = [UIImage imageNamed:@"Grid_Logout.png"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Grid_Logout.png"];
    }
    else if ([text isEqualToString:LOGIN_TO_BUSINESS]) {
        cell.imageView.image = [UIImage imageNamed:@"Grid_Login.png"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Grid_Login.png"];
    }
    
    else if ([text isEqualToString:CONTACTUS]) {
        cell.imageView.image = [UIImage imageNamed:@"Grid_Contactus.png"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Grid_Contactus.png"];
    }
    else if ([text isEqualToString:FACEBOOK]) {
        cell.imageView.image = [UIImage imageNamed:@"Grid_Facebook.png"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Grid_Facebook.png"];
    }
    else if ([text isEqualToString:TWITTER]) {
        cell.imageView.image = [UIImage imageNamed:@"Grid_Twitter.png"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Grid_Twitter.png"];
    }
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(currentSectionIndex == indexPath.section && currentRowIndex == indexPath.row)
        return;
    
    preSectionIndex = currentSectionIndex;
    preRowIndex     = currentRowIndex;
    
    NSString *text = @"";
    switch (indexPath.section) {
        case 0:
            text = [[sideMenuDict objectForKey:SECTION0] objectAtIndex:indexPath.row];
            break;
        case 1:
            text = [[sideMenuDict objectForKey:SECTION1] objectAtIndex:indexPath.row];
            break;
        case 2:
            text = [[sideMenuDict objectForKey:SECTION2] objectAtIndex:indexPath.row];
            break;
        case 3:
            text = [[sideMenuDict objectForKey:SECTION3] objectAtIndex:indexPath.row];
            break;
    }
    if ([text isEqualToString:REPORTS]) {
        
        
    }else if ([text isEqualToString:TALK_NOW]) {
       
    }else if ([text isEqualToString:VIEW_POST]) {
 
    }
    
    
    else if ([text isEqualToString:ADD_STORE]) {
        
    }
    else if ([text isEqualToString:EDIT_STORE]) {
       
    }
    else if ([text isEqualToString:MANAGE_GALLARY]) {
       
    }
    
    else if ([text isEqualToString:LOGIN]) {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        //DebugLog(@"SideMenu: %@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[INMerchantLoginViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        } else {
            obj = nil;
        }
        INMerchantLoginViewController *merLoginController = [[INMerchantLoginViewController alloc] initWithNibName:@"INMerchantLoginViewController" bundle:nil];
        NSArray *controllers = [NSArray arrayWithObject:merLoginController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        controllers = nil;
    }
    else if ([text isEqualToString:LOGOUT]) {
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: @"Are you sure you want to log out?"
                                                          delegate: self
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"NO", @"YES", nil
                              ];
        [alert show];
    }
    else if ([text isEqualToString:LOGIN_TO_BUSINESS]) {
        currentSectionIndex = preSectionIndex;
        currentRowIndex     = preRowIndex;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        [IN_APP_DELEGATE switchViewController];
        return;
    }
    
    else if ([text isEqualToString:CONTACTUS]) {
       
    }
    else if ([text isEqualToString:FACEBOOK]) {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        //DebugLog(@"SideMenu: %@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[INFBWebViewController class]] && currentSectionIndex == indexPath.section && currentRowIndex == indexPath.row)
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        } else {
            obj = nil;
        }
        INFBWebViewController *facebookViewController = [[INFBWebViewController alloc] initWithNibName:@"INFBWebViewController" bundle:nil];
        facebookViewController.title = FACEBOOK;
        facebookViewController.type = 1;
        NSArray *controllers = [NSArray arrayWithObject:facebookViewController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        controllers = nil;
    }
    else if ([text isEqualToString:TWITTER]) {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        //DebugLog(@"SideMenu: %@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[INFBWebViewController class]] && currentSectionIndex == indexPath.section && currentRowIndex == indexPath.row)
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        } else {
            obj = nil;
        }
        INFBWebViewController *twitterViewController = [[INFBWebViewController alloc] initWithNibName:@"INFBWebViewController" bundle:nil];
        twitterViewController.title = TWITTER;
        twitterViewController.type = 2;
        NSArray *controllers = [NSArray arrayWithObject:twitterViewController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        controllers = nil;
    }
    currentSectionIndex = indexPath.section;
    currentRowIndex     = indexPath.row;
}

#pragma mark -
#pragma mark - Seperator
- (UIImage *)separatorImage
{
    UIGraphicsBeginImageContext(CGSizeMake(1, 4));
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIGraphicsPushContext(context);
    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:1].CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, 1, 2));
    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:79/255.0 green:79/255.0 blue:77/255.0 alpha:1].CGColor);
    CGContextFillRect(context, CGRectMake(0, 3, 1, 2));
    UIGraphicsPopContext();
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithCGImage:outputImage.CGImage scale:2.0 orientation:UIImageOrientationUp];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma NSNotificationCenter Callbacks
-(void)handleLoginLogoutNotification:(NSNotification *)notification
{
    NSDictionary *dict = [notification userInfo];
    BOOL isLoggedIn = [[dict objectForKey:IN_MERCHANT_LOGIN_LOGOUT_KEY] boolValue];
    NSMutableArray *myShoplocalArray   = [[NSMutableArray alloc] initWithArray:[sideMenuDict objectForKey:SECTION2]];
    DebugLog(@"before isLoggedIn %@",myShoplocalArray);
    DebugLog(@"Merchant : isLoggedIn %d",isLoggedIn);
    if (isLoggedIn) {
        if(NSNotFound == [myShoplocalArray indexOfObject:LOGOUT]) {
            DebugLog(@"Merchant : logout not found ");
            [myShoplocalArray replaceObjectAtIndex:[myShoplocalArray indexOfObject:LOGIN] withObject:LOGOUT];
        }else{
            DebugLog(@"Merchant : logout  found ");
        }
    }else{
        if(NSNotFound != [myShoplocalArray indexOfObject:LOGOUT]) {
            [myShoplocalArray replaceObjectAtIndex:[myShoplocalArray indexOfObject:LOGOUT] withObject:LOGIN];
        }
    }
    DebugLog(@"after isLoggedIn %@",myShoplocalArray);
    [sideMenuDict setObject:myShoplocalArray forKey:SECTION2];
    
    preSectionIndex = 0;
    preRowIndex     = 0;
    currentSectionIndex = 0;
    currentRowIndex     = 0;
    
    INMerchantViewController *merchantViewController = [[INMerchantViewController alloc] initWithNibName:@"INMerchantViewController" bundle:nil];
    merchantViewController.title = @"";
    NSArray *controllers = [NSArray arrayWithObject:merchantViewController];
    self.sideMenu.navigationController.viewControllers = controllers;
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:currentRowIndex inSection:currentSectionIndex] animated:NO scrollPosition:UITableViewScrollPositionNone];
    [self.tableView reloadData];
}

#pragma UIAlertView Delegates
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"YES"])
    {
        
        [INUserDefaultOperations clearMerchantDetails];
        
        NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:IN_CUSTOMER_LOGIN_LOGOUT_KEY];
        [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_LOGIN_LOGOUT_NOTIFICATION object:nil userInfo:dictionary];
        [self showLoginScreen];
    }
}


-(void)showLoginScreen{
        currentSectionIndex = 2;
        currentRowIndex     = [[sideMenuDict objectForKey:SECTION2] indexOfObject:LOGOUT];
        
        NSMutableArray *myShoplocalArray   = [[NSMutableArray alloc] initWithArray:[sideMenuDict objectForKey:SECTION2]];
        if(NSNotFound != [myShoplocalArray indexOfObject:LOGOUT]) {
            [myShoplocalArray replaceObjectAtIndex:currentRowIndex withObject:LOGIN];
        }
        [sideMenuDict setObject:myShoplocalArray forKey:SECTION2];
        
        INMerchantLoginViewController *merchantLoginViewController = [[INMerchantLoginViewController alloc] initWithNibName:@"INMerchantLoginViewController" bundle:nil];
        NSArray *controllers = [NSArray arrayWithObject:merchantLoginViewController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:currentRowIndex inSection:currentSectionIndex] animated:NO scrollPosition:UITableViewScrollPositionNone];
        [self.tableView reloadData];
}
@end
