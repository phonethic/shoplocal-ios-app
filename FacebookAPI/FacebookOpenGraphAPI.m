//
//  FacebookOpenGraphAPI.m
//  shoplocal
//
//  Created by Kirti Nikam on 07/05/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import "FacebookOpenGraphAPI.h"
#import <FacebookSDK/FacebookSDK.h>
#import "constants.h"

#define ACTION_TYPE_LIKE @"like"
#define ACTION_TYPE_SHARE @"share"

#define FACEBOOK_APP_NAMESPACE @"shop-local"

//#define DEEPLINKING_PREFIX_URL @"http://shoplocal.co.in/marketplace/store"

#define DEEPLINKING_PREFIX_URL [NSString stringWithFormat:@"%@%@marketplace/store",LIVE_SERVER,URL_PREFIX]
#define LOGO_PICTURE [NSString stringWithFormat:@"%@%@assets/images/icons/shoplocal-logo.png",LIVE_SERVER,URL_PREFIX]


@implementation NSString (URLEncoding)
-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding {
	return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                               (CFStringRef)self,
                                                               NULL,
                                                               (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
                                                               CFStringConvertNSStringEncodingToEncoding(encoding)));
}

-(NSString *)urlEncoding:(NSString *)unencodedString{
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef)unencodedString,
                                                              NULL,
                                                              (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}
@end

@implementation FacebookOpenGraphAPI
+(NSString *)createDeepLinkWithActiveAreaName:(NSString *)lactiveAreaName storeName:(NSString *)lstoreName storeId:(NSString *)lstoreId
{
    //@"http://shoplocal.co.in/marketplace/store/lokhandwala-andheri/all/Ambica+Infotech/225";
    
    NSMutableString *deepLinkingUrl = [[NSMutableString alloc] initWithString:@""];
    //lactiveAreaName = @"lokhandwala-andheri";
    if ([lactiveAreaName length] > 0 && [lstoreName length] > 0 && [lstoreId length] > 0) {
        [deepLinkingUrl appendString:DEEPLINKING_PREFIX_URL];
        [deepLinkingUrl appendString:[NSString stringWithFormat:@"/%@",lactiveAreaName]];
        [deepLinkingUrl appendString:@"/all"];
        [deepLinkingUrl appendString:[NSString stringWithFormat:@"/%@",[lstoreName urlEncodeUsingEncoding:NSUTF8StringEncoding]]];
        [deepLinkingUrl appendString:[NSString stringWithFormat:@"/%@",lstoreId]];
    }
    DebugLog(@"deepLinkingUrl %@",deepLinkingUrl);
    return deepLinkingUrl;
}

+(void)shareContentUsingOpenGraphAPIWithTitle:(NSString *)ltitle description:(NSString *)ldescription image:(NSString *)limageUrl deeplinkingURL:(NSString *)url objectType:(NSString *)objectType
{
    // Check for publish permissions
    [FacebookOpenGraphAPI checkPublishPermissionsWithTitle:ltitle description:ldescription image:limageUrl deeplinkingURL:url  objectType:objectType actionType:ACTION_TYPE_SHARE];
}

+(void)likeContentUsingOpenGraphAPIWithTitle:(NSString *)ltitle description:(NSString *)ldescription image:(NSString *)limageUrl deeplinkingURL:(NSString *)url  objectType:(NSString *)objectType
{
    // Check for publish permissions
    [FacebookOpenGraphAPI checkPublishPermissionsWithTitle:ltitle description:ldescription image:limageUrl deeplinkingURL:url  objectType:objectType actionType:ACTION_TYPE_LIKE];
}

+(void)checkPublishPermissionsWithTitle:(NSString *)ltitle description:(NSString *)ldescription image:(NSString *)limageUrl deeplinkingURL:(NSString *)url objectType:(NSString *)objectType actionType:(NSString *)actionType
{
    [FBRequestConnection startWithGraphPath:@"/me/permissions"
      completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
          DebugLog(@"result %@",result);
          if (error) {
              // An error occurred, we need to handle the error
              // See: https://developers.facebook.com/docs/ios/errors
              DebugLog(@"Encountered an error checking permissions: %@", error.description);
          }else{
              NSPredicate *comparePredicate = [NSPredicate predicateWithFormat:@"permission CONTAINS[cd] %@",
                                               @"publish_actions"];
              NSArray *filterArray = [(NSArray *)[result data] filteredArrayUsingPredicate:comparePredicate];
              DebugLog(@"filterArray %@",filterArray);
              
              if ([filterArray count] > 0) {
                  // Permissions present, publish the OG story
                  [FacebookOpenGraphAPI publishOpenGraphStoryWithTitle:ltitle description:ldescription image:limageUrl deeplinkingURL:url objectType:objectType actionType:actionType];
              }else{
                  [FBSession.activeSession requestNewPublishPermissions:[NSArray arrayWithObject:@"publish_actions"]
                                                        defaultAudience:FBSessionDefaultAudienceFriends
                                                      completionHandler:^(FBSession *session, NSError *error) {
                                                          if (error) {
                                                              // An error occurred, we need to handle the error
                                                              // See: https://developers.facebook.com/docs/ios/errors
                                                              DebugLog(@"Encountered an error requesting permissions: %@", error.description);
                                                          }else{
                                                              if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound){
                                                                  // Permission not granted, tell the user we will not share to Facebook
                                                                  DebugLog(@"Permission not granted, we will not share to Facebook.");
                                                              } else {
                                                                  // Permissions present, publish the OG story
                                                                  [FacebookOpenGraphAPI publishOpenGraphStoryWithTitle:ltitle description:ldescription image:limageUrl deeplinkingURL:url objectType:objectType actionType:actionType];
                                                              }
                                                          }
                  }];
              }
          }
    }];
}

+(void)publishOpenGraphStoryWithTitle:(NSString *)ltitle description:(NSString *)ldescription image:(NSString *)limageUrl deeplinkingURL:(NSString *)lurl  objectType:(NSString *)objectType actionType:(NSString *)actionType
{
    DebugLog(@"create OpenGraphObjectId with title -%@- description -%@- image -%@- objecttype -%@- actiontype -%@-",ltitle,ldescription,limageUrl,objectType,actionType);
    DebugLog(@"url : %@",lurl);
    
    if ([objectType isEqualToString:OBJECT_TYPE_NONE]) {
        DebugLog(@"Got objectType == OBJECT_TYPE_NONE");
        return;
    }
    // instantiate a Facebook Open Graph object
    NSMutableDictionary<FBOpenGraphObject> *object = [FBGraphObject openGraphObjectForPost];
    // specify that this Open Graph object will be posted to Facebook
    object.provisionedForPost = YES;
    
    // for og:title
    object[@"title"] = ltitle;
    
    // for og:type, this corresponds to the Namespace you've set for your app and the object type name
    //object[@"type"] = @"shop-local:store";
    object[@"type"] = [NSString stringWithFormat:@"%@:%@",FACEBOOK_APP_NAMESPACE,objectType];

    // for og:description
    if ([ldescription length] > 0) {
        object[@"description"] = ldescription;
    }
    
    // for og:url, we cover how this is used in the "Deep Linking" section below
    if ([lurl length] > 0) {
      //  object[@"url"] = @"http://shoplocal.co.in/marketplace/store/lokhandwala-andheri/services/Ambica+Infotech/225";
        object[@"url"] = lurl;
    }
    
    // for og:image we assign the uri of the image that we just staged
    if ([limageUrl length] > 0) {
        object[@"image"] = @[@{@"url": limageUrl, @"user_generated" : @"false" }];
    }else{
        object[@"image"] = @[@{@"url": LOGO_PICTURE, @"user_generated" : @"false" }];
    }
    
    // Post custom object
    [FBRequestConnection startForPostOpenGraphObject:object
                                   completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {                                       if(error)
                                   {
                                       // An error occurred, we need to handle the error
                                       // See: https://developers.facebook.com/docs/ios/errors
                                       DebugLog(@"Encountered an error posting to Open Graph: %@", error.description);
                                   }else{
                                       // get the object ID for the Open Graph object that is now stored in the Object API
                                       NSString *objectId = [result objectForKey:@"id"];
                                       DebugLog(@"object id: %@", objectId);
                                       if (objectId) {
                                           // create an Open Graph action
                                           if ([actionType isEqualToString:ACTION_TYPE_LIKE]) {
                                               [FacebookOpenGraphAPI sendLikeOpenGraphActionWithOpenGraphObjectId:objectId objectType:objectType];
                                           }else{
                                               [FacebookOpenGraphAPI sendCustomOpenGraphActionWithOpenGraphObjectId:objectId objectType:objectType];
                                           }
                                       }
                                   }
                                   }];
}

+(NSString *)uploadImageToFacebookWithUIImage:(UIImage *)imageToUpload
{
    __block NSString *imageURI = nil;
    [FBRequestConnection startForUploadStagingResourceWithImage:imageToUpload
                                              completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                                  if (error) {
                                                      // An error occurred, we need to handle the error
                                                      // See: https://developers.facebook.com/docs/ios/errors
                                                      DebugLog(@"Error staging an image: %@", error.description);
                                                  }else{
                                                      DebugLog(@"Successfuly staged image with staged URI: %@", [result objectForKey:@"uri"]);
                                                      imageURI = [result objectForKey:@"uri"];
                                                  }
                                              }];
    
    return imageURI;
}

+(void)sendCustomOpenGraphActionWithOpenGraphObjectId:(NSString *)openGraphObjectId objectType:(NSString *)objectType
{
    DebugLog(@"sendCustomOpenGraphActionWithOpenGraphObjectId %@",openGraphObjectId);
    // create an Open Graph action
    id<FBOpenGraphAction> action = (id<FBOpenGraphAction>)[FBGraphObject graphObject];
    [action setObject:openGraphObjectId forKey:objectType];
    [action setObject:@"fb:explicitly_shared" forKey:@"true"];
    
    NSString *graphPathAction = [NSString stringWithFormat:@"/me/%@:share",FACEBOOK_APP_NAMESPACE];
    DebugLog(@"%@",graphPathAction);
    
    // create action referencing user owned object
    [FBRequestConnection startForPostWithGraphPath:graphPathAction graphObject:action completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if (error) {
            // An error occurred, we need to handle the error
            // See: https://developers.facebook.com/docs/ios/errors
            DebugLog(@"Encountered an error posting to Open Graph: %@", error.description);
        }else{
            DebugLog(@"OG story posted, story id: %@", [result objectForKey:@"id"]);
//            [[[UIAlertView alloc] initWithTitle:@"Shoplocal OG Custome story posted"
//                                        message:@"Check your Facebook profile or activity log to see the story."
//                                       delegate:self
//                              cancelButtonTitle:@"OK!"
//                              otherButtonTitles:nil] show];
        }
    }];
}

+(void)sendLikeOpenGraphActionWithOpenGraphObjectId:(NSString *)openGraphObjectId objectType:(NSString *)objectType
{
    DebugLog(@"sendLikeOpenGraphActionWithOpenGraphObjectId %@",openGraphObjectId);
    // create an Open Graph action
    id<FBOpenGraphAction> action = (id<FBOpenGraphAction>)[FBGraphObject graphObject];
    [action setObject:openGraphObjectId forKey:objectType];
 //   [action setObject:@"fb:explicitly_shared" forKey:@"true"];
    
    NSString *graphPathAction = [NSString stringWithFormat:@"/me/%@:favourite",FACEBOOK_APP_NAMESPACE];
    DebugLog(@"%@",graphPathAction);
    
    // create action referencing user owned object
    [FBRequestConnection startForPostWithGraphPath:graphPathAction graphObject:action completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if (error) {
            // An error occurred, we need to handle the error
            // See: https://developers.facebook.com/docs/ios/errors
            DebugLog(@"Encountered an error posting to Open Graph: %@", error.description);
        }else{
            DebugLog(@"OG story posted, story id: %@", [result objectForKey:@"id"]);
            //            [[[UIAlertView alloc] initWithTitle:@"Shoplocal OG Custome story posted"
            //                                        message:@"Check your Facebook profile or activity log to see the story."
            //                                       delegate:self
            //                              cancelButtonTitle:@"OK!"
            //                              otherButtonTitles:nil] show];
        }
    }];
    
//    DebugLog(@"sendLikeOpenGraphActionWithOpenGraphObjectId %@",openGraphObjectId);
//    // create an Open Graph action
//    id<FBOpenGraphAction> action = (id<FBOpenGraphAction>)[FBGraphObject graphObject];
//    [action setObject:openGraphObjectId forKey:@"object"];
//    [action setObject:@"fb:explicitly_shared" forKey:@"true"];
//
//    // create action referencing user owned object
//    [FBRequestConnection startForPostWithGraphPath:@"/me/og.likes" graphObject:action completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//        if (error) {
//            // An error occurred, we need to handle the error
//            // See: https://developers.facebook.com/docs/ios/errors
//            DebugLog(@"Encountered an error posting to Open Graph: %@", error.description);
//        }else{
//            DebugLog(@"OG story posted, story id: %@", [result objectForKey:@"id"]);
////            [[[UIAlertView alloc] initWithTitle:@"Shoplocal OG Like story posted"
////                                        message:@"Check your Facebook profile or activity log to see the story."
////                                       delegate:self
////                              cancelButtonTitle:@"OK!"
////                              otherButtonTitles:nil] show];
//        }
//    }];
}
@end
