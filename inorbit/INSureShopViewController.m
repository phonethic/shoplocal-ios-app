//
//  INSureShopViewController.m
//  shoplocal
//
//  Created by Kirti Nikam on 07/10/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INSureShopViewController.h"
#import "INAppDelegate.h"
#import "CommonCallback.h"
#import "CKCalendarView.h"
#import "INDateTypeObject.h"
#import "SIAlertView.h"

#define GET_SPECIAL_EVENTLIST(PLACE_ID) [NSString stringWithFormat:@"%@%@%@merchant_api/special_date?place_id=%d",LIVE_SERVER,URL_PREFIX,API_VERSION,PLACE_ID]

#define GET_DATE_CATEGORY_LIST [NSString stringWithFormat:@"%@%@%@/user_api/date_categories",LIVE_SERVER,URL_PREFIX,API_VERSION]

#define SPECIAL_BROADCAST_LINK [NSString stringWithFormat:@"%@%@broadcast_api/special",URL_PREFIX,API_VERSION]


@interface INSureShopViewController ()<CKCalendarDelegate>
@property(nonatomic, weak) CKCalendarView *ckCalendarView;
@property(nonatomic, strong) NSDateFormatter *dateFormatter;
@property(strong) NSDictionary *splashJson;
@end

@implementation INSureShopViewController
@synthesize splashJson;
@synthesize ckCalendarView;
@synthesize dateFormatter;
@synthesize datesDict;
@synthesize eventsTableView;
@synthesize dateTypeArray;
@synthesize dataFilePath;
@synthesize placeId,storeName;
@synthesize dateCategoriesDict;
@synthesize selectedSpecialDate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if([INUserDefaultOperations isMerchantLoggedIn])
    {
        [self sendDateCategoriesListRequest];
        datesDict = [[NSMutableDictionary alloc] init];
        dateTypeArray = [[NSMutableArray alloc] init];
        [self initializeCKCalendarView];
                
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
        NSString *docsDir = [dirPaths lastObject];
        
        dataFilePath = [[NSString alloc] initWithString:[docsDir stringByAppendingString:@"dateType.archive"]];
        
        if ([fileManager fileExistsAtPath:dataFilePath]) {
            DebugLog(@"File Exists");
            NSMutableArray *dataArray = [NSKeyedUnarchiver unarchiveObjectWithFile:dataFilePath];
            NSDate *oldDate =  (NSDate *)[dataArray objectAtIndex:0];
            datesDict = [dataArray objectAtIndex:1];
            NSDate *currentDate =  [NSDate date];
            
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSInteger comps = (NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit);
            
            NSDateComponents *date1Components = [calendar components:comps fromDate:oldDate];
            NSDateComponents *date2Components = [calendar components:comps fromDate:currentDate];
            
            oldDate = [calendar dateFromComponents:date1Components];
            currentDate = [calendar dateFromComponents:date2Components];
            DebugLog(@"-----oldDate %@ --currentDate %@--",oldDate,currentDate);

            NSComparisonResult result = [oldDate compare:currentDate];
            if (result == NSOrderedAscending)
            {
                DebugLog(@"oldDate < currentDate"); // result = -1
            }
            else if (result == NSOrderedDescending)
            {
                DebugLog(@"oldDate > currentDate"); // result = 1
            }
            else {
                DebugLog(@"oldDate == currentDate"); // result = 0
            }
            if (datesDict.count > 0 && result == 0) {
                [ckCalendarView reloadData];
                return;
            }
        }
        
        if ([IN_APP_DELEGATE networkavailable]) {
            [self sendGetSpecialEventsListHttpRequest];
        }else{
            UIAlertView *errorView = [[UIAlertView alloc]
                                      initWithTitle:@"No Network Connection"
                                      message:@"Please check your internet connection and try again."
                                      delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [errorView show];
        }
    }else{
        [INUserDefaultOperations showAlert:@"Please login first."];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidUnload {
    [self setEventsTableView:nil];
    [self setInfoBtn:nil];
    [super viewDidUnload];
}
#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
#pragma internal methods
-(void)sendDateCategoriesListRequest
{
    DebugLog(@"========================sendDateCategoriesListRequest========================");
    
    DebugLog(@"user_id %@ auth_id %@",[INUserDefaultOperations getCustomerAuthId],[INUserDefaultOperations getCustomerAuthCode]);
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:GET_DATE_CATEGORY_LIST]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setValue:[INUserDefaultOperations getCustomerAuthId] forHTTPHeaderField:@"user_id"];
    [urlRequest setValue:[INUserDefaultOperations getCustomerAuthCode] forHTTPHeaderField:@"auth_id"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        DebugLog(@"%@",self.splashJson);
                                                        if(self.splashJson  != nil)
                                                        {
                                                            if (dateCategoriesDict == nil) {
                                                                dateCategoriesDict = [[NSMutableDictionary alloc] init];
                                                            }else{
                                                                [dateCategoriesDict removeAllObjects];
                                                            }
                                                            DebugLog(@"--->%@",[self.splashJson  objectForKey:@"success"]);
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSDictionary *maindict = [self.splashJson  objectForKey:@"data"];
                                                                NSArray *dateCatgArray = [maindict objectForKey:@"date_categories"];
                                                                for (int index = 0; index < [dateCatgArray count]; index++) {
                                                                    NSDictionary *dict = [dateCatgArray objectAtIndex:index];
                                                                    NSString *dateId = [dict objectForKey:@"id"];
                                                                    NSString *dateCategory = [dict objectForKey:@"name"];
                                                                    [dateCategoriesDict setObject:dateCategory forKey:dateId];
                                                                }
                                                                
                                                                DebugLog(@"dateCategoriesDict %@",dateCategoriesDict);
                                                            }
                                                            if (datesDict.count > 0) {
                                                                [ckCalendarView setHidden:FALSE];
                                                                [eventsTableView setHidden:FALSE];
                                                                [ckCalendarView reloadData];
                                                            }
                                                        }
                                                        
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                        [av show];
                                                        
                                                    }];
    
    
    
    [operation start];
}

-(void)sendGetSpecialEventsListHttpRequest{
    DebugLog(@"========================sendGetSpecialEventsListHttpRequest========================");
    DebugLog(@"user_id %@ auth_id %@",[INUserDefaultOperations getMerchantAuthId],[INUserDefaultOperations getMerchantAuthCode]);
    [CommonCallback showProgressHud:@"Loading" subtitle:@""];
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:GET_SPECIAL_EVENTLIST(self.placeId)]];
    DebugLog(@"urlRequest  %@",urlRequest.URL);
    
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setValue:[INUserDefaultOperations getMerchantAuthId] forHTTPHeaderField:@"user_id"];
    [urlRequest setValue:[INUserDefaultOperations getMerchantAuthCode] forHTTPHeaderField:@"auth_id"];
    
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        [CommonCallback hideProgressHud];
                                                        DebugLog(@"%@",self.splashJson);
                                                        if(self.splashJson  != nil)
                                                        {
                                                            DebugLog(@"--->%@",[self.splashJson  objectForKey:@"success"]);
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                if (datesDict == nil) {
                                                                    datesDict = [[NSMutableDictionary alloc] init];
                                                                }else{
                                                                    [datesDict removeAllObjects];
                                                                }
                                                                NSArray *dataArray = [self.splashJson objectForKey:@"data"];
                                                                DebugLog(@"data %@",dataArray);
                                                                for (NSDictionary *dict in dataArray) {
                                                                    NSString *dateString = [dict objectForKey:@"date"];
                                                                    NSArray *ldateTypeArray = [dict objectForKey:@"date_type"];
                                                                    
                                                                    //1. Check date_type array count
                                                                    //2. if > 0, then add this to dateDict as key => 'dateString' and datetypearray as  dateTypeObjects array
                                                                    if (ldateTypeArray.count > 0) {
                                                                        NSMutableArray *objArray = [[NSMutableArray alloc] init];
                                                                        
                                                                        for (NSDictionary *dateTypeDict in ldateTypeArray) {
                                                                            dateTypeObj = [[INDateTypeObject alloc] init];
                                                                            dateTypeObj.date = dateString;
                                                                            dateTypeObj.date_type = [dateTypeDict objectForKey:@"type"];
                                                                            dateTypeObj.count = [dateTypeDict objectForKey:@"count"];
                                                                            
                                                                            DebugLog(@"-%@-%@-%@-",dateTypeObj.date,dateTypeObj.date_type,dateTypeObj.count);
                                                                            [objArray addObject:dateTypeObj];
                                                                        }
                                                                        
                                                                        [datesDict setObject:objArray forKey:dateString];
                                                                        objArray = nil;
                                                                    }
                                                                }
                                                                DebugLog(@"datesDict %@",datesDict);
                                                                if (datesDict.count > 0) {
                                                                    [NSKeyedArchiver archiveRootObject:[[NSMutableArray  alloc] initWithObjects:[NSDate date],datesDict, nil] toFile:dataFilePath];
                                                                    [ckCalendarView setHidden:FALSE];
                                                                    [eventsTableView setHidden:FALSE];
                                                                    [ckCalendarView reloadData];
                                                                }
                                                            
                                                            }else{
                                                                if([self.splashJson  objectForKey:@"message"] != [NSNull null])
                                                                    [INUserDefaultOperations showAlert:[self.splashJson  objectForKey:@"message"]];
                                                            }
                                                        }
                                                        
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                        [av show];
                                                        
                                                    }];
    [operation start];
}

-(void)initializeCKCalendarView
{
    CKCalendarView *localCalendarView = [[CKCalendarView alloc] initWithStartDay:startMonday];
    self.ckCalendarView = localCalendarView;

    ckCalendarView.delegate = self;
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    ckCalendarView.onlyShowCurrentMonth = YES;
    ckCalendarView.adaptHeightToNumberOfWeeksInMonth = YES;

    ckCalendarView.frame = CGRectMake(32, 5, 250, 220);
    [self.view addSubview:ckCalendarView];
    
//    [datesDict setObject:@"" forKey:@"2013-10-05"];
//    [datesDict setObject:@"" forKey:@"2013-11-25"];
//    [datesDict setObject:@"" forKey:@"2013-11-14"];
//    [datesDict setObject:@"" forKey:@"2013-10-25"];
//    [datesDict setObject:@"" forKey:@"2013-10-02"];

//    [self.ckCalendarView reloadData];
    
    eventsTableView.frame = CGRectMake(eventsTableView.frame.origin.x,CGRectGetMaxY(ckCalendarView.frame)+20, eventsTableView.frame.size.width,self.view.frame.size.height - (CGRectGetMaxY(ckCalendarView.frame)+20));
    
    [ckCalendarView setHidden:YES];
    [eventsTableView setHidden:YES];
}

- (BOOL)dateIsAvailable:(NSDate *)date {
    for (NSString *avdatestring in [self.datesDict allKeys]) {
        NSDate *avdate = [self.dateFormatter dateFromString:avdatestring];
        if ([avdate isEqualToDate:date]) {
            return YES;
        }
    }
    return NO;
}

#pragma mark -
#pragma mark - CKCalendarDelegate
-(void)calendar:(CKCalendarView *)calendar configureDateItem:(CKDateItem *)dateItem forDate:(NSDate *)date{
    if ([self dateIsAvailable:date]) {
        dateItem.backgroundColor = BROWN_COLOR;
        dateItem.textColor = [UIColor whiteColor];
    }
}

- (BOOL)calendar:(CKCalendarView *)calendar willSelectDate:(NSDate *)date {
    return YES;
}

-(void)calendar:(CKCalendarView *)calendar didSelectDate:(NSDate *)date{
    selectedSpecialDate = [self.dateFormatter stringFromDate:date];
    DebugLog(@"Dated Selected as %@",selectedSpecialDate);
    dateTypeArray = [datesDict objectForKey:selectedSpecialDate];
    [eventsTableView reloadData];
}

//- (BOOL)calendar:(CKCalendarView *)calendar willChangeToMonth:(NSDate *)date {
//    return [date laterDate:self.minimumDate] == date;
//}

#pragma UITableViewDataSource Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [dateTypeArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"dateTypeCell";
    
    UIButton *talkBtn;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
        cell.textLabel.font = DEFAULT_BOLD_FONT(15.0);
        cell.textLabel.textColor = BROWN_COLOR;
        
        talkBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        talkBtn.frame = CGRectMake(240, 5, 70, 35);
        talkBtn.titleLabel.font = DEFAULT_FONT(14.0);
        [talkBtn setTitle:@"Talk Now" forState:UIControlStateNormal];
        [talkBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [talkBtn setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.8] forState:UIControlStateHighlighted];
        [talkBtn setBackgroundImage:[UIImage imageNamed:@"Button1.png"] forState:UIControlStateNormal];
        [talkBtn addTarget:self action:@selector(talkNowBtnPressed:event:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:talkBtn];
    }
    if (dateTypeArray.count > 0) {
        INDateTypeObject *tempObj = (INDateTypeObject *)[dateTypeArray objectAtIndex:indexPath.row];
        NSString *typeName = [self.dateCategoriesDict objectForKey:tempObj.date_type];
        cell.textLabel.text = [NSString stringWithFormat:@"%@ - Count %@",typeName,tempObj.count];
    }
    return cell;
}

-(void)talkNowBtnPressed:(id)sender event:(id)event
{
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self.eventsTableView];
    NSIndexPath *indexPath = [self.eventsTableView indexPathForRowAtPoint:point];
    if (indexPath != nil) {
//        UITableViewCell *cell = [self.eventsTableView cellForRowAtIndexPath:indexPath];
//        INDateTypeObject *tempObj = (INDateTypeObject *)[dateTypeArray objectAtIndex:indexPath.row];
        //do call here
        DebugLog(@"%d",indexPath.row);
        
        //[self sendSpecialBroadcastRequest:selectedSpecialDate];
    }
}

-(void)sendSpecialBroadcastRequest:(NSString *)dateString{

    DebugLog(@"-----------------sendSpecialBroadcastRequest--------------------");
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];

    NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
    [postParams setObject:[NSString stringWithFormat:@"%d",self.placeId] forKey:@"place_id"];
    [postParams setObject:@"specialBroadCast title" forKey:@"title"];
    [postParams setObject:@"specialBroadCast description" forKey:@"description"];
    [postParams setObject:@"" forKey:@"url"];
    
    //set by default
    [postParams setObject:@"1" forKey:@"type"];
    [postParams setObject:@"published" forKey:@"state"];
    [postParams setObject:@"1" forKey:@"is_offered"];
    
    [postParams setObject:dateString forKey:@"special_date"];
    [postParams setObject:@"2013-10-20 12:39:43" forKey:@"offer_date_time"];
    
    [postParams setObject:[INUserDefaultOperations getMerchantAuthId] forKey:@"user_id"];
    [postParams setObject:[INUserDefaultOperations getMerchantAuthCode] forKey:@"auth_id"];
    
    
//    bool result = CGSizeEqualToSize(broadcastImageView.image.size, CGSizeZero);
//    if (result == 0)// not empty {
//        NSData *imageData = UIImageJPEGRepresentation(nil, 1.0);
//        NSString *base64Data = [IN_APP_DELEGATE base64forData:imageData];
//        [postParams setObject:@"specialDateOffer.jpg" forKey:@"filename"];
//        [postParams setObject:@"image/jpeg" forKey:@"filetype"];
//        [postParams setObject:base64Data forKey:@"userfile"];
//    }
    
    DebugLog(@"postParams %@",postParams);

    [httpClient postPath:SPECIAL_BROADCAST_LINK
                parameters:postParams
                success:^(AFHTTPRequestOperation *operation, id responseObject){
                         [CommonCallback hideProgressHud];
                         NSError *error;
                         NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
                         DebugLog(@"json special broadcast ->%@",jsonDict);
                         if(jsonDict != nil && [[jsonDict  objectForKey:@"success"] isEqualToString:@"true"])
                         {
                             [INUserDefaultOperations showAlert:[jsonDict objectForKey:@"message"]];
                             
                         }else {
                             [INUserDefaultOperations showAlert:@"Unable to broadcast your message.Please try again later."];
                         }
                 } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                     DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
                     //[hud hide:YES];
                     [CommonCallback hideProgressHud];
                 }];
}
- (IBAction)infoBtnPressed:(id)sender {
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Sureshop displays important dates like birthdays & anniversaries of local shoppers. you can press 'talk' and send them customised offers. You can see dates upto the next 30 days from today."];
    alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
    alertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
    
    [alertView addButtonWithTitle:@"Close"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alert) {
                              DebugLog(@"Close Clicked");
                          }];
    [alertView show];
}
@end
