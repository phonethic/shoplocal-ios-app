//
//  INTermsViewController.m
//  shoplocal
//
//  Created by Rishi on 12/09/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INTermsViewController.h"
#import "INUserDefaultOperations.h"
#import "INAppDelegate.h"
#import "CommonCallback.h"

@interface INTermsViewController ()

@end

@implementation INTermsViewController
@synthesize termsWebView;
@synthesize acceptBtn;
@synthesize cancelBtn;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    acceptBtn.titleLabel.font = DEFAULT_FONT(16);
    cancelBtn.titleLabel.font = DEFAULT_FONT(16);
    
    termsWebView.delegate = self;
    termsWebView.scalesPageToFit = TRUE;
    [self loadTermsUrl];
}

-(void)loadTermsUrl
{
    NSFileManager *fileManager      = [NSFileManager defaultManager];
	NSArray *paths                  = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory    = [paths objectAtIndex:0];
    NSString* filePath              = [documentsDirectory stringByAppendingPathComponent:@"terms.html"];
    DebugLog(@"filePath = %@" , filePath);
    
    if ([fileManager fileExistsAtPath:filePath])
    {
        DebugLog(@"file exists");
        if(([INUserDefaultOperations getDateDifferenceInHours:[INUserDefaultOperations getTermsSyncDate]]) >= 24)
        {
            DebugLog(@"file exists but more than 24 hours");
            [self sendRequest:filePath];
        } else {
            DebugLog(@"load from file");
            NSURL *url = [NSURL fileURLWithPath:filePath];
            NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
            [termsWebView loadRequest:requestObj];
        }
    }else{
        DebugLog(@"file not exists");
        [self sendRequest:filePath];
    }
}

-(void)sendRequest:(NSString *)filePath{
    if ([IN_APP_DELEGATE networkavailable]) {
        NSURL *url = [NSURL URLWithString:TERMS_AND_CONDITIONS_LINK];
        
        NSData *urlData = [NSData dataWithContentsOfURL:url];
        [urlData writeToFile:filePath atomically:YES];
        [INUserDefaultOperations setTermsSyncDate];
        
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [termsWebView loadRequest:requestObj];
    }else{
        [INUserDefaultOperations showOfflineAlert];
    }
}

#pragma mark - Web View
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    DebugLog(@"webViewDidStartLoad");
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    DebugLog(@"webViewDidFinishLoad");
    [CommonCallback hideProgressHud];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    DebugLog(@"didFailLoadWithError : %@",error);
    [CommonCallback hideProgressHudWithError];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)acceptBtnPressed:(id)sender {
    [INUserDefaultOperations setTermsConditionsValue:@"1"];
//    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
    if ([self.delegate respondsToSelector:@selector(dismissTerms)]) {
        [self.delegate dismissTerms];
    }
}

- (IBAction)cancelBtnpressed:(id)sender {
    [INUserDefaultOperations setTermsConditionsValue:@"0"];
//    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
    if ([self.delegate respondsToSelector:@selector(dismissTerms)]) {
        [self.delegate dismissTerms];
    }
}
- (void)viewDidUnload {
    [self setTermsWebView:nil];
    [self setAcceptBtn:nil];
    [self setCancelBtn:nil];
    [super viewDidUnload];
}

#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
@end
