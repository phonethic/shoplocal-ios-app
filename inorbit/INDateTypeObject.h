//
//  INDateTypeObject.h
//  shoplocal
//
//  Created by Kirti Nikam on 07/10/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface INDateTypeObject : NSObject <NSCoding>
@property (nonatomic, copy) NSString *date;
@property (nonatomic, copy) NSString *date_type;
@property (nonatomic, copy) NSString *count;
@end
