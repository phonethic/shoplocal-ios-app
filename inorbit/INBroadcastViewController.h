//
//  INBroadcastViewController.h
//  shoplocal
//
//  Created by Rishi on 19/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "ActionSheetPicker.h"
#import "CLImageEditor.h"

@class AbstractActionSheetPicker;
@interface INBroadcastViewController : UIViewController <UIScrollViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextViewDelegate,CLImageEditorDelegate>

@property (copy, nonatomic) NSArray *placeidArray;
@property (nonatomic) BOOL newMedia;

@property (nonatomic, strong) AbstractActionSheetPicker *actionSheetPicker;
@property (nonatomic, strong) NSDate *selectedDate;

@property (strong, nonatomic) IBOutlet UIScrollView *broadcastScrollView;
@property (strong, nonatomic) IBOutlet UIView *broadcastView;
@property (strong, nonatomic) IBOutlet UITextField *titleTextField;
@property (strong, nonatomic) IBOutlet UITextView *messageTextView;
@property (strong, nonatomic) IBOutlet UITextField *tagsTextField;
@property (strong, nonatomic) IBOutlet UIImageView *broadcastImageView;
@property (strong, nonatomic) IBOutlet UIImageView *calenderImage;
@property (strong, nonatomic) IBOutlet UIButton *chosseImageBtn;
@property (strong, nonatomic) IBOutlet UIButton *takePictureBtn;
@property (strong, nonatomic) IBOutlet UIButton *imageCancelBtn;
@property (strong, nonatomic) IBOutlet UILabel *validitylbl;
@property (strong, nonatomic) IBOutlet UILabel *isOfferlbl;
@property (strong, nonatomic) IBOutlet UIButton *offerBtn;
@property (strong, nonatomic) IBOutlet UIButton *timeBtn;
@property (strong, nonatomic) IBOutlet UIButton *reviewBtn;


@property (strong, nonatomic) IBOutlet UITextField *imageCaptionTextField;


@property (strong, nonatomic) IBOutlet UIImageView *BlblSeperator1;
@property (strong, nonatomic) IBOutlet UIImageView *BlblSeperator2;
@property (strong, nonatomic) IBOutlet UIImageView *BlblSeperator3;
@property (strong, nonatomic) IBOutlet UIImageView *BlblSeperator4;
@property (strong, nonatomic) IBOutlet UIImageView *BlblSeperator5;




@property (strong, nonatomic) IBOutlet UIScrollView *reviewScrollView;
@property (strong, nonatomic) IBOutlet UIView *reviewView;
@property (strong, nonatomic) IBOutlet UILabel *titilelbl;
@property (strong, nonatomic) IBOutlet UILabel *messagelbl;
@property (strong, nonatomic) IBOutlet UIImageView *messageImageView;
@property (strong, nonatomic) IBOutlet UIButton *editBtn;
@property (strong, nonatomic) IBOutlet UIButton *postBtn;
@property (strong, nonatomic) IBOutlet UILabel *textviewplaceholderLabel;

@property (nonatomic, copy) NSString *reusetitle;
@property (nonatomic, copy) NSString *reusedescription;
@property (nonatomic, copy) NSString *reuseimage_url;
@property (nonatomic, copy) NSString *datein24formatString;

@property (strong, nonatomic) IBOutlet UILabel *lblImageCaption;

@property (strong, nonatomic) IBOutlet UIImageView *RlblSeperator1;
@property (strong, nonatomic) IBOutlet UIImageView *RlblSeperator2;
@property (strong, nonatomic) IBOutlet UIImageView *RlblSeperator3;
@property (strong, nonatomic) IBOutlet UIImageView *RlblSeperator4;
@property (strong, nonatomic) IBOutlet UIImageView *RlblSeperator5;

@property (strong, nonatomic) IBOutlet UIImageView *offerImageView;

@property (strong, nonatomic) IBOutlet UIView *addImageDescriptionView;
@property (strong, nonatomic) IBOutlet UILabel *lbldescriptionViewHeader;
@property (strong, nonatomic) IBOutlet UILabel *lbldescriptionViewContent;
@property (strong, nonatomic) IBOutlet UIButton *descriptionViewSetBtn;
@property (strong, nonatomic) IBOutlet UIButton *descriptionViewSkipBtn;
@property (strong, nonatomic) IBOutlet UITextView *descriptionViewtitleTextView;

- (IBAction)descriptionViewSetBtnPressed:(id)sender;
- (IBAction)descriptionViewSkipBtnPressed:(id)sender;


- (IBAction)chooseImageBtnPressed:(id)sender;
- (IBAction)takePictureBtnPressed:(id)sender;
- (IBAction)isOfferBtnPressed:(id)sender;
- (IBAction)reviewBtnPressed:(id)sender;
- (IBAction)setDateBtnPressed:(id)sender;
- (IBAction)imageCancelBtnPressed:(id)sender;
- (IBAction)editBtnPressed:(id)sender;
- (IBAction)postBtnPressed:(id)sender;
@end
