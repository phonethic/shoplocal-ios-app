//
//  INAppDelegate.h
//  shoplocal
//
//  Created by Rishi on 22/07/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>
#import "constants.h"
#import "NMSplashImageViewController.h"
#import "Crittercism.h"
#import <FacebookSDK/FacebookSDK.h>
#import "MFSideMenu.h"

#define IN_APP_DELEGATE (INAppDelegate *)[[UIApplication sharedApplication] delegate]
extern NSString *const FBSessionStateChangedNotification;

@class INViewController;
@class NMSplashViewController;
@class INMerchantViewController;
@class INAreaObject;
@class SideMenuViewController;

@interface INAppDelegate : UIResponder <UIApplicationDelegate>
{
    Reachability* internetReach;
    Boolean networkavailable;
    sqlite3 *shoplocalDB;
    UINavigationController *clientnavigationController;
    UINavigationController *merchantnavigationController;
    INAreaObject *areaObj;
    
    BOOL isAreaRequestIsRunning;
}
@property (strong, nonatomic) UIWindow *window;
@property(copy,nonatomic) NSString *databasePath;

@property(copy,nonatomic) NSString *activeAreaId;
@property(copy,nonatomic) NSString *activePlaceId;
@property(copy,nonatomic) NSString *storeIdToOpen;

@property(nonatomic) Boolean networkavailable;
@property (strong, nonatomic) NSMutableArray *areaArray;
@property (strong, nonatomic) INViewController *viewController;
@property (strong, nonatomic) INMerchantViewController *merchantviewController;
@property (strong, nonatomic) NMSplashViewController *splashviewController;
@property (strong, nonatomic) SideMenuViewController *custleftSideMenuController;

@property (nonatomic, strong) MFSideMenu *customerSideMenu;
@property (nonatomic, strong) MFSideMenu *merchantSideMenu;


//facebook properties
@property (strong, nonatomic) NSString *loggedInUserID;
@property (strong, nonatomic) FBSession *loggedInSession;
@property(nonatomic,copy) NSDictionary *splashJson;

-(void) writeToTextFile:(NSDictionary *)lcontent name:(NSString *)lname;
-(NSDictionary *) getTextFromFile:(NSString *)lname;
-(BOOL)checkFileExits:(NSString *)lname;
-(void)animationStop;

-(void)initialViewController;
-(void)switchViewController;

- (NSString*)base64forData:(NSData*)theData ;

//facebook callbacks
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI;
- (void) closeSession;
-(NSString*)urlEncode:(NSString*) string;
- (void)handleAuthError:(NSError *)error;

-(NSString *)getPostDetailsMessageBody:(NSString *)storeName offerTitle:(NSString *)postTitle offerDescription:(NSString *)postDescription isOffered:(NSString *)postIsOffered offerDateTime:(NSString *)postOfferDateTime contact:(NSString *)contactString shareVia:(NSString *)shareVia;

//db Callbacks
-(NSString *)getActiveAreaIdFromAreaTable;
-(NSString *)getActiveShoplocalPlaceIdFromAreaTable;
-(NSString *)getActivePlaceNameFromAreaTable;
-(NSString *)getActiveSlugFromAreaTable;
-(NSString *)getLocalityFromAreaTable;
-(NSString *)getCityFromAreaTable;
-(NSString *)getPincodeFromAreaTable;
-(NSString *)getAreaIdFromAreaTable:(NSString *)sublocality;
-(NSString *)getCityFromAreaTable:(NSString *)sublocality;
-(NSString *)getPincodeFromAreaTable:(NSString *)sublocality;
-(NSString *)getLatitudeFromAreaTable;
-(NSString *)getLongitudeFromAreaTable;

-(void)insertAreaTable:(NSArray *)lareaArray;
-(void)deleteAreasTable;
-(void)updateAreasTableFromStateActiveToNonActive;
-(void)updateAreasTable:(NSString *) area_id is_active:(int)activeValue;
-(void)updateAreasTableByName:(NSString *) sublocality is_active:(int)activeValue;

-(void)sendNetworkNotification;
@end
