//
//  INFavouritesViewController.h
//  shoplocal
//
//  Created by Rishi on 29/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "INCustomerLoginViewController.h"

@class INAllStoreObj;
@interface INFavouritesViewController : UIViewController<UISearchBarDelegate,INCustomerLoginViewControllerDelegate>
{
    INAllStoreObj *storeObj;
    BOOL isDragging;
    BOOL isLoading;
    BOOL isAdding;
    sqlite3 *shoplocalDB;
}
@property (strong, nonatomic) IBOutlet UISearchBar *globalSearchBar;
@property (strong, nonatomic) IBOutlet UITableView *favouriteTableView;
@property (strong, nonatomic) NSMutableArray *favArray;
@property (strong, nonatomic) NSMutableArray *tempstoreArray;
@property (strong, nonatomic) NSMutableArray *telArray;

@property (nonatomic, retain) UIView *refreshHeaderView;
@property (nonatomic, retain) UILabel *refreshLabel;
@property (nonatomic, retain) UIImageView *refreshArrow;
@property (nonatomic, retain) UIActivityIndicatorView *refreshSpinner;
@property (strong, nonatomic) IBOutlet UILabel *countlbl;
@property (readwrite, nonatomic) NSInteger totalPage;
@property (readwrite, nonatomic) NSInteger totalRecord;
@property (readwrite, nonatomic) NSInteger currentPage;
@property (readwrite, nonatomic) NSInteger selectedPlaceId;
@property (copy, nonatomic) NSString* selectedPlaceName;

@property (strong, nonatomic) IBOutlet UIView *countHeaderView;
@property (strong, nonatomic) IBOutlet UILabel *lblpullToRefresh;

//*****Pull TO Refresh
@property (nonatomic, copy) NSString *textPull;
@property (nonatomic, copy) NSString *textRelease;
@property (nonatomic, copy) NSString *textLoading;

@property (strong, nonatomic) IBOutlet UIView *loginView;
@property (strong, nonatomic) IBOutlet UIButton *loginBtn;
@property (strong, nonatomic) IBOutlet UILabel *lblLoginText;
@property (strong, nonatomic) IBOutlet UIButton *showStoreViewBtn;
@property (strong, nonatomic) IBOutlet UIButton *showOffersBtn;


@property (strong, nonatomic) IBOutlet UIView *callModalViewBackView;
@property (strong, nonatomic) IBOutlet UIView *callModalView;
@property (strong, nonatomic) IBOutlet UILabel *lblcallModalHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblcallModalHeaderSeperator;
@property (strong, nonatomic) IBOutlet UITableView *callModalTableView;
@property (strong, nonatomic) IBOutlet UIButton *btnCancelcallModal;
- (IBAction)btnCancelcallModalPressed:(id)sender;


- (IBAction)loginBtnPressed:(id)sender;
- (IBAction)showStoreViewBtnPressed:(id)sender;
- (IBAction)showOffersBtnPressed:(id)sender;
@end
