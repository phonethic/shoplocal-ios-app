//
//  INChangeLocationViewController.h
//  shoplocal
//
//  Created by Kirti Nikam on 06/11/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#define AREA_UPDATE_NOTIFICATION @"area_update_notification"

@interface INChangeLocationViewController : UIViewController <CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
    BOOL onetimeAnimation;
    BOOL isPlayingAnimation;
    
    BOOL isNearAreaRequestAlreadySent;
    BOOL locationNotFound;
}
@property (strong, nonatomic) NSString *previousArea;
@property (strong, nonatomic) NSString *selectedArea;
@property (retain, nonatomic) NSMutableArray *areaArray;
@property (readwrite,nonatomic) int comeFrom;
@property (strong, nonatomic) IBOutlet UIButton *selectLocationBtn;
@property (strong, nonatomic) IBOutlet UIImageView *arrowImageView;
@property (strong, nonatomic) IBOutlet UIButton *goBtn;
@property (strong, nonatomic) IBOutlet UILabel *lblLocation;
@property (strong, nonatomic) IBOutlet MKMapView *locationMapView;
//@property (strong, nonatomic) NSMutableArray *staticImageArray;
//@property (strong, nonatomic) NSMutableArray *imagelinkArray;
@property (strong, nonatomic) IBOutlet UILabel *merchantcountlbl;


- (IBAction)selectLocationBtnPressed:(id)sender;
- (IBAction)goBtnPressed:(id)sender;
@end
