//
//  INBroadcastViewController.m
//  shoplocal
//
//  Created by Rishi on 19/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INBroadcastViewController.h"
#import "CommonCallback.h"
#import "INAppDelegate.h"
#import "ActionSheetPicker.h"
#import "NSDate+TCUtils.h"
#import "SIAlertView.h"

#define BROADCAST_LINK [NSString stringWithFormat:@"%@%@broadcast_api/broadcast",URL_PREFIX,API_VERSION]
#define THUMBNAIL_LINK(IMG_LINK) [NSString stringWithFormat:@"%@%@%@",LIVE_SERVER,URL_PREFIX,IMG_LINK]

#define TXT_DESCRIPTION_PLACEHOLDER_TEXT @"Post your offer or just a fun message for your customer."
#define BTN_OFFER_PLACEHOLDER_TEXT @"Please choose end date"
@interface INBroadcastViewController ()

@end

@implementation INBroadcastViewController
@synthesize titleTextField;
@synthesize messageTextView;
@synthesize offerBtn;
@synthesize broadcastImageView;
@synthesize newMedia;
@synthesize placeidArray;
@synthesize tagsTextField;
@synthesize broadcastScrollView;
@synthesize selectedDate = _selectedDate;
@synthesize timeBtn;
@synthesize reviewBtn;
@synthesize calenderImage;
@synthesize chosseImageBtn;
@synthesize takePictureBtn;
@synthesize isOfferlbl;
@synthesize imageCancelBtn;
@synthesize reviewScrollView;
@synthesize validitylbl;
@synthesize titilelbl;
@synthesize messagelbl;
@synthesize messageImageView;
@synthesize editBtn;
@synthesize postBtn;
@synthesize broadcastView,reviewView;
@synthesize BlblSeperator1,BlblSeperator2,BlblSeperator3,BlblSeperator4,BlblSeperator5;
@synthesize reusetitle;
@synthesize reusedescription;
@synthesize reuseimage_url;
@synthesize imageCaptionTextField;
@synthesize lblImageCaption;
@synthesize datein24formatString;
@synthesize RlblSeperator1,RlblSeperator2,RlblSeperator3,RlblSeperator4,RlblSeperator5;
@synthesize addImageDescriptionView,lbldescriptionViewContent,lbldescriptionViewHeader,descriptionViewSetBtn,descriptionViewSkipBtn,descriptionViewtitleTextView;
@synthesize offerImageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void) viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        // back button was pressed.  We know this is true because self is no longer
        // in the navigation stack.
        DebugLog(@"back btn pressed");
    }
    [super viewWillDisappear:animated];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                             style:UIBarButtonItemStylePlain
                                                            target:nil
                                                            action:nil];
    [[self navigationItem] setBackBarButtonItem:back];
    
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    viewTap.numberOfTapsRequired = 1;
    viewTap.cancelsTouchesInView = NO;
    [self.view  addGestureRecognizer:viewTap];
    
//    CGRect contentRect = CGRectZero;
//    for (UIView *view in self.broadcastScrollView.subviews) {
//        contentRect = CGRectUnion(contentRect, view.frame);
//    }
//    self.broadcastScrollView.contentSize = CGSizeMake(self.broadcastScrollView.frame.size.width,contentRect.size.height);
    [self setUI];
    
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        [button setImage:[UIImage imageNamed:@"back_button_iOS6.png"] forState:UIControlStateNormal];
    }else{
        [button setImage:[UIImage imageNamed:@"back_button_iOS7.png"] forState:UIControlStateNormal];
    }
    [button addTarget:self action:@selector(backbuttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 50, 35)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];

    
    broadcastImageView.image   = nil;
    messageImageView.image     = nil;
    broadcastImageView.hidden   = TRUE;
    messageImageView.hidden     = TRUE;
    imageCancelBtn.hidden       = TRUE;
    validitylbl.hidden          = TRUE;
    calenderImage.hidden        = TRUE;
    timeBtn.hidden              = TRUE;
    self.broadcastScrollView.hidden = FALSE;
    self.reviewScrollView.hidden = TRUE;
    
    self.selectedDate = [NSDate date];

    BlblSeperator5.frame = CGRectMake(BlblSeperator5.frame.origin.x,CGRectGetMaxY(isOfferlbl.frame)+5, BlblSeperator5.frame.size.width, BlblSeperator5.frame.size.height);
    reviewBtn.frame = CGRectMake(reviewBtn.frame.origin.x, CGRectGetMaxY(BlblSeperator5.frame)+3, reviewBtn.frame.size.width, reviewBtn.frame.size.height);
    
    self.broadcastScrollView.contentSize = CGSizeMake(self.broadcastScrollView.frame.size.width,CGRectGetMaxY(reviewBtn.frame)+50);
    self.reviewScrollView.contentSize = CGSizeMake(self.reviewScrollView.frame.size.width,CGRectGetMaxY(postBtn.frame)+70);

    if(reusetitle != nil && ![reusetitle isEqualToString:@""])
    {
        titleTextField.text = reusetitle;
        if (reusedescription != nil && ![reusedescription isEqualToString:@""]) {
            messageTextView.text = reusedescription;
            messageTextView.textColor = [UIColor blackColor];
        }else{
            messageTextView.textColor = [UIColor lightGrayColor];
            messageTextView.text = TXT_DESCRIPTION_PLACEHOLDER_TEXT;
        }
        if(reuseimage_url != nil && ![reuseimage_url isEqualToString:@""]){
            DebugLog(@"reuseimage_url %@",reuseimage_url);

           [broadcastImageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(reuseimage_url)] placeholderImage:[UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"]];
            [self reloadScrollViewwithImageView];
           [messageImageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(reuseimage_url)] placeholderImage:[UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"]];
        }
    }
   [self adjustScrollView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
}

- (void)viewDidUnload {
    [self setTitleTextField:nil];
    [self setMessageTextView:nil];
    [self setOfferBtn:nil];
    [self setBroadcastImageView:nil];
    [self setTagsTextField:nil];
    [self setBroadcastScrollView:nil];
    [self setTimeBtn:nil];
    [self setReviewBtn:nil];
    [self setCalenderImage:nil];
    [self setChosseImageBtn:nil];
    [self setTakePictureBtn:nil];
    [self setIsOfferlbl:nil];
    [self setImageCancelBtn:nil];
    [self setReviewScrollView:nil];
    [self setValiditylbl:nil];
    [self setTitilelbl:nil];
    [self setMessagelbl:nil];
    [self setMessageImageView:nil];
    [self setEditBtn:nil];
    [self setPostBtn:nil];
    [self setTextviewplaceholderLabel:nil];
    [self setBroadcastView:nil];
    [self setReviewView:nil];
    [self setBlblSeperator1:nil];
    [self setBlblSeperator2:nil];
    [self setBlblSeperator3:nil];
    [self setBlblSeperator4:nil];
    [self setBlblSeperator5:nil];
    [self setImageCaptionTextField:nil];
    [self setLblImageCaption:nil];
    [self setRlblSeperator1:nil];
    [self setRlblSeperator2:nil];
    [self setRlblSeperator3:nil];
    [self setRlblSeperator4:nil];
    [self setRlblSeperator5:nil];
    [self setAddImageDescriptionView:nil];
    [self setLbldescriptionViewHeader:nil];
    [self setLbldescriptionViewContent:nil];
    [self setDescriptionViewSetBtn:nil];
    [self setDescriptionViewSkipBtn:nil];
    [self setDescriptionViewtitleTextView:nil];
    [self setOfferImageView:nil];
    [super viewDidUnload];
}
#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void)setUI
{
    broadcastScrollView.backgroundColor     = [UIColor clearColor];
    //broadcastScrollView.layer.cornerRadius  = 8.0;
    broadcastScrollView.clipsToBounds       = YES;
    
    reviewScrollView.backgroundColor        = [UIColor clearColor];
    //reviewScrollView.layer.cornerRadius     = 8.0;
    reviewScrollView.clipsToBounds          = YES;
    
    //broadcastView = [CommonCallback setViewPropertiesWithRoundedCorner:broadcastView];
    broadcastView.backgroundColor =  [UIColor whiteColor];
    broadcastView.clipsToBounds = YES;
    
    imageCaptionTextField.font      = DEFAULT_FONT(14);
    isOfferlbl.font                 = DEFAULT_BOLD_FONT(14);
    validitylbl.font                = DEFAULT_BOLD_FONT(13);
    
    titleTextField.font                  = DEFAULT_FONT(18);
    titleTextField.backgroundColor       =  [UIColor whiteColor];
    titleTextField.textColor             =  [UIColor blackColor];
    titleTextField.textAlignment         = NSTextAlignmentLeft;
    titleTextField.autocapitalizationType   = UITextAutocapitalizationTypeSentences;
    titleTextField.autocorrectionType       = UITextAutocorrectionTypeYes;
    
    messageTextView.font                  = DEFAULT_FONT(18);
    messageTextView.backgroundColor       = [UIColor whiteColor];
    messageTextView.textColor             = [UIColor lightGrayColor];
    messageTextView.textAlignment         = NSTextAlignmentLeft;
    messageTextView.delegate              = self;
    messageTextView.text = TXT_DESCRIPTION_PLACEHOLDER_TEXT;
    messageTextView.autocapitalizationType      = UITextAutocapitalizationTypeSentences;
    messageTextView.autocorrectionType          = UITextAutocorrectionTypeYes;
    
    tagsTextField.font                  =   DEFAULT_FONT(18);
    tagsTextField.backgroundColor       = [UIColor whiteColor];
    tagsTextField.textColor             =  [UIColor blackColor];
    tagsTextField.textAlignment         = NSTextAlignmentLeft;
    
    broadcastImageView.backgroundColor = [UIColor clearColor];
    broadcastImageView.layer.masksToBounds = YES;
    broadcastImageView.contentMode = UIViewContentModeScaleAspectFill;
    broadcastImageView.clipsToBounds = YES;
    
    timeBtn.titleLabel.font            = DEFAULT_FONT(15);
    timeBtn.backgroundColor            = [UIColor clearColor];
    [timeBtn setTitleColor:BROWN_COLOR forState:UIControlStateNormal];
    [timeBtn setTitleColor:BROWN_OFFWHITE_COLOR forState:UIControlStateHighlighted];
    [timeBtn setTitle:BTN_OFFER_PLACEHOLDER_TEXT forState:UIControlStateNormal];
    
    isOfferlbl.font                  = DEFAULT_FONT(18);
    isOfferlbl.backgroundColor       = [UIColor clearColor];
    isOfferlbl.textColor             = [UIColor blackColor];
    isOfferlbl.textAlignment         = NSTextAlignmentLeft;
    
    offerBtn.backgroundColor            = [UIColor clearColor];
    offerBtn.titleLabel.font    = DEFAULT_FONT(16);
    [offerBtn setTitleColor:BROWN_COLOR forState:UIControlStateNormal];
    [offerBtn setTitleColor:BROWN_OFFWHITE_COLOR forState:UIControlStateHighlighted];
    
    reviewBtn.titleLabel.font       = DEFAULT_FONT(18);
    chosseImageBtn.titleLabel.font  = DEFAULT_FONT(15);
    takePictureBtn.titleLabel.font  = DEFAULT_FONT(15);
    
    imageCaptionTextField.textColor = BROWN_COLOR;
    isOfferlbl.textColor    = BROWN_COLOR;
    validitylbl.textColor   = BROWN_COLOR;
    
    isOfferlbl.backgroundColor    = [UIColor clearColor];
    validitylbl.backgroundColor   = [UIColor clearColor];
    
    //reviewView = [CommonCallback setViewPropertiesWithRoundedCorner:reviewView];
    reviewView.backgroundColor =  [UIColor whiteColor];
    reviewView.clipsToBounds = YES;
    
    validitylbl.font                  = DEFAULT_FONT(15);
    messagelbl.backgroundColor        = [UIColor clearColor];
    validitylbl.textColor             = [UIColor blackColor];
    validitylbl.textAlignment         = NSTextAlignmentCenter;
    validitylbl.numberOfLines         = 2;
    
    titilelbl.font                  = DEFAULT_FONT(18);
    titilelbl.backgroundColor       = [UIColor clearColor];
    titilelbl.textColor             = [UIColor blackColor];
    titilelbl.textAlignment         = NSTextAlignmentCenter;
    titilelbl.numberOfLines         = 0;
    
    messagelbl.font                  = DEFAULT_FONT(18);
    messagelbl.backgroundColor       = [UIColor clearColor];
    messagelbl.textColor             = [UIColor blackColor];
    messagelbl.textAlignment         = NSTextAlignmentCenter;
    messagelbl.numberOfLines         = 0;
    
    messageImageView.backgroundColor   = [UIColor clearColor];
    messageImageView.layer.masksToBounds = YES;
    messageImageView.contentMode = UIViewContentModeScaleAspectFill;
    messageImageView.clipsToBounds = YES;
    
    postBtn.titleLabel.font = DEFAULT_FONT(18);
    editBtn.titleLabel.font = DEFAULT_FONT(18);
    
    //addImageDescriptionView = [CommonCallback setViewPropertiesWithRoundedCorner:addImageDescriptionView];
    addImageDescriptionView.backgroundColor =  [UIColor whiteColor];
    addImageDescriptionView.layer.shadowColor = [UIColor blackColor].CGColor;
    addImageDescriptionView.layer.shadowOffset = CGSizeMake(1, 1);
    addImageDescriptionView.layer.shadowOpacity = 1.0;
    addImageDescriptionView.layer.shadowRadius = 10.0;
    
    lbldescriptionViewHeader.textAlignment = NSTextAlignmentCenter;
    lbldescriptionViewHeader.backgroundColor = [UIColor clearColor];
    lbldescriptionViewHeader.font = DEFAULT_FONT(20);
    lbldescriptionViewHeader.textColor = [UIColor blackColor];
    lbldescriptionViewHeader.adjustsFontSizeToFitWidth = YES;
    lbldescriptionViewHeader.text = ALERT_TITLE;
    
    lbldescriptionViewContent.textAlignment = NSTextAlignmentCenter;
    lbldescriptionViewContent.backgroundColor = [UIColor clearColor];
    lbldescriptionViewContent.font = DEFAULT_FONT(16);
    lbldescriptionViewContent.textColor = [UIColor darkGrayColor];
    lbldescriptionViewContent.numberOfLines = 15;
    lbldescriptionViewContent.text = @"Say something about the photo";
    
    descriptionViewSetBtn.titleLabel.font = DEFAULT_FONT([UIFont buttonFontSize]);
    descriptionViewSkipBtn.titleLabel.font = DEFAULT_FONT([UIFont buttonFontSize]);
    
    [descriptionViewSetBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [descriptionViewSetBtn setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.8] forState:UIControlStateHighlighted];
    
    [descriptionViewSkipBtn setTitleColor:[UIColor colorWithWhite:0.3 alpha:1] forState:UIControlStateNormal];
    [descriptionViewSkipBtn setTitleColor:[UIColor colorWithWhite:0.3 alpha:0.8] forState:UIControlStateHighlighted];
    
    UIImage *dnormalImage = [UIImage imageNamed:@"SIAlertView.bundle/button-destructive-d"];
    UIImage *dhighlightedImage = [UIImage imageNamed:@"SIAlertView.bundle/button-destructive-d"];
    UIEdgeInsets dinsets = [self getResizedImageInset:dnormalImage];
	dnormalImage = [dnormalImage resizableImageWithCapInsets:dinsets];
	dhighlightedImage = [dhighlightedImage resizableImageWithCapInsets:dinsets];
    
    UIImage *normalImage = [UIImage imageNamed:@"SIAlertView.bundle/button-cancel"];
    UIImage *highlightedImage = [UIImage imageNamed:@"SIAlertView.bundle/button-cancel-d"];
    UIEdgeInsets insets = [self getResizedImageInset:normalImage];
	normalImage = [normalImage resizableImageWithCapInsets:insets];
	highlightedImage = [highlightedImage resizableImageWithCapInsets:insets];
    
    [descriptionViewSetBtn setBackgroundImage:dnormalImage forState:UIControlStateNormal];
	[descriptionViewSetBtn setBackgroundImage:dhighlightedImage forState:UIControlStateHighlighted];
    
	[descriptionViewSkipBtn setBackgroundImage:normalImage forState:UIControlStateNormal];
	[descriptionViewSkipBtn setBackgroundImage:highlightedImage forState:UIControlStateHighlighted];
    
    descriptionViewtitleTextView.textAlignment = NSTextAlignmentLeft;
    descriptionViewtitleTextView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:1];
    descriptionViewtitleTextView.font = DEFAULT_BOLD_FONT(18);
    descriptionViewtitleTextView.textColor = BROWN_COLOR;
    descriptionViewtitleTextView.text = @"";
    descriptionViewtitleTextView.layer.cornerRadius = 5.0;
    descriptionViewtitleTextView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    descriptionViewtitleTextView.layer.borderWidth = 0.5;
    descriptionViewtitleTextView.delegate = self;
    //    descriptionViewtitleTextView.layer.shadowColor = [UIColor blackColor].CGColor;
    //    descriptionViewtitleTextView.layer.shadowOffset = CGSizeMake(1, 1);
    //    descriptionViewtitleTextView.layer.shadowOpacity = 1;
    //    descriptionViewtitleTextView.layer.shadowRadius = 10.0;
    
    [addImageDescriptionView setHidden:YES];
}

-(void)backbuttonClicked:(id) sender
{
    UIAlertView *backAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Are you sure you want to leave this screen?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    backAlert.tag = 1;
    [backAlert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
  if (alertView.tag == 1) {
        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        if([title isEqualToString:@"Ok"])
        {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}

-(UIEdgeInsets)getResizedImageInset:(UIImage *)image
{
    CGFloat hInset = floorf(image.size.width / 2);
	CGFloat vInset = floorf(image.size.height / 2);
	UIEdgeInsets insets = UIEdgeInsetsMake(vInset, hInset, vInset, hInset);
    return insets;
}

- (void)tapDetected:(UIGestureRecognizer *)sender {
    [self.view endEditing:YES];
    if (!broadcastScrollView.isHidden) {
        CGPoint tapLocation = [sender locationInView:broadcastScrollView];
        UIView *view = [broadcastScrollView hitTest:tapLocation withEvent:nil];
        if (![view isKindOfClass:[UIButton class]]) {
            self.broadcastScrollView.contentSize = CGSizeMake(self.broadcastScrollView.frame.size.width,CGRectGetMaxY(reviewBtn.frame)+30);
            [self scrollTobottom];
        }
    }
}

-(void)backButtonPressed{
    DebugLog(@"back btn pressed");
    if ([titleTextField.text length]>0 || [messageTextView.text length] >0 || [tagsTextField.text length] > 0 || [imageCaptionTextField.text length] > 0 || ![timeBtn.titleLabel.text isEqualToString:@"Please choose end date"]) {
        DebugLog(@"got changes");

        SIAlertView *sialertView1 = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Do you want to discard your changes?"];
        sialertView1.transitionStyle = SIAlertViewTransitionStyleDropDown;
        sialertView1.backgroundStyle = SIAlertViewBackgroundStyleGradient;
        [sialertView1 addButtonWithTitle:@"Yes"
                                    type:SIAlertViewButtonTypeDestructive
                                 handler:^(SIAlertView *alert) {
                                     DebugLog(@"Yes Clicked");
                                 }];
        [sialertView1 addButtonWithTitle:@"No"
                                    type:SIAlertViewButtonTypeCancel
                                 handler:^(SIAlertView *alert) {
                                     DebugLog(@"No Clicked");
                                 }];
        [sialertView1 show];
    }
}

#pragma UITextViewDelegate methods
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if ([textView isEqual:messageTextView]){
        messageTextView.textColor = [UIColor blackColor];
        if ([messageTextView.text isEqualToString:TXT_DESCRIPTION_PLACEHOLDER_TEXT]) {
            messageTextView.text = @"";
        }
        if (broadcastImageView.isHidden) {
            [broadcastScrollView setContentSize: CGSizeMake(broadcastScrollView.frame.size.width, broadcastScrollView.frame.size.height + 200)];
        }else{
            [broadcastScrollView setContentSize: CGSizeMake(broadcastScrollView.frame.size.width, broadcastScrollView.frame.size.height + CGRectGetHeight(broadcastImageView.frame) +200)];
        }
        self.broadcastScrollView.scrollEnabled = TRUE;
    }
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    if (!addImageDescriptionView.hidden  && [textView isEqual:descriptionViewtitleTextView]) {
        [UIView animateWithDuration:0.2 animations:^{
            DebugLog(@"self.view.frame.size.height %f",self.view.frame.size.height);
            addImageDescriptionView.frame = CGRectMake(addImageDescriptionView.frame.origin.x,self.view.frame.size.height - 516, addImageDescriptionView.frame.size.width, addImageDescriptionView.frame.size.height);
            // assuming that 300 = addImageDescriptionView height and 216 is keyboard height
        } completion:^(BOOL finished) {
        }];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if ([textView isEqual:messageTextView]){
        if ([[messageTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
            messageTextView.textColor = [UIColor blackColor];
        }else{
            messageTextView.textColor = [UIColor lightGrayColor];
            messageTextView.text = TXT_DESCRIPTION_PLACEHOLDER_TEXT;
        }
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if(textView==messageTextView) {
        NSString *messageTextViewString = [messageTextView.text stringByReplacingCharactersInRange:range withString:text];
        return !([messageTextViewString length] > 300);
    } else {
        return YES;
    }
}

#pragma UITextFieldDelegate methods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField.frame.origin.y > broadcastScrollView.contentOffset.y)
        if(textField == tagsTextField || textField == imageCaptionTextField)
        {
            [broadcastScrollView setContentOffset:CGPointMake(0,textField.frame.origin.y-100) animated:YES];
        }
    if (broadcastImageView.isHidden) {
        [broadcastScrollView setContentSize: CGSizeMake(broadcastScrollView.frame.size.width, broadcastScrollView.frame.size.height + 200)];
    }else{
        [broadcastScrollView setContentSize: CGSizeMake(broadcastScrollView.frame.size.width, broadcastScrollView.frame.size.height + CGRectGetHeight(broadcastImageView.frame) +200)];
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    DebugLog(@"textFieldShouldReturn");
    if (textField == titleTextField) {
        [titleTextField resignFirstResponder];
	}else if (textField == imageCaptionTextField){
        [imageCaptionTextField resignFirstResponder];
        [self scrollTobottom];
    }
    else if (textField == tagsTextField) {
        [tagsTextField resignFirstResponder];
        [self scrollTobottom];
	}
   	return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(textField==titleTextField) {
        NSString *titleTextFieldString = [titleTextField.text stringByReplacingCharactersInRange:range withString:string];
        return !([titleTextFieldString length] > 100);
    } else {
        return YES;
    }
}

-(void)scrollTobottom
{
    [UIView animateWithDuration:0.2 animations:^{
        self.broadcastScrollView.contentSize = CGSizeMake(self.broadcastScrollView.frame.size.width,CGRectGetMaxY(reviewBtn.frame)+30);
        CGPoint bottomOffset = CGPointMake(0, 0);
        [broadcastScrollView setContentOffset:bottomOffset animated:YES];
    }];
}

- (void)useCamera
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker =
        [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        //[imagePicker.navigationBar setTintColor:BROWN_COLOR];
        imagePicker.sourceType =
        UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = [NSArray arrayWithObjects:
                                  (NSString *) kUTTypeImage,
                                  nil];
        imagePicker.allowsEditing = YES;
        [self presentViewController:imagePicker animated:YES completion:nil];
        newMedia = YES;
    }
}

- (void)useCameraRoll
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    {
        UIImagePickerController *imagePicker =
        [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        //[imagePicker.navigationBar setTintColor:BROWN_COLOR];
        imagePicker.sourceType =
        UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.mediaTypes = [NSArray arrayWithObjects:
                                  (NSString *) kUTTypeImage,
                                  nil];
        imagePicker.allowsEditing = YES;
        [self presentViewController:imagePicker animated:YES completion:nil];
        newMedia = NO;
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info
                           objectForKey:UIImagePickerControllerMediaType];
//    [self dismissViewControllerAnimated:YES completion:nil];

    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
//        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
        
        CLImageEditor *editor = [[CLImageEditor alloc] initWithImage:image];
        editor.delegate = self;
        /* @"CLFilterTool",
         @"CLAdjustmentTool",
         @"CLEffectTool",
         @"CLBlurTool",
         @"CLClippingTool",
         @"CLRotateTool",
         @"CLToneCurveTool",*/
        
        CLImageToolInfo *Filtertool = [editor.toolInfo subToolInfoWithToolName:@"CLFilterTool" recursive:NO];
        Filtertool.available = NO;
        CLImageToolInfo *Blurtool = [editor.toolInfo subToolInfoWithToolName:@"CLBlurTool" recursive:NO];
        Blurtool.available = NO;
        CLImageToolInfo *Curvetool = [editor.toolInfo subToolInfoWithToolName:@"CLToneCurveTool" recursive:NO];
        Curvetool.available = NO;
        CLImageToolInfo *ClippingTool = [editor.toolInfo subToolInfoWithToolName:@"CLClippingTool" recursive:NO];
        ClippingTool.available = NO;
        
        CLImageToolInfo *AdjustmentTool = [editor.toolInfo subToolInfoWithToolName:@"CLAdjustmentTool" recursive:NO];
        AdjustmentTool.dockedNumber = 2;
        CLImageToolInfo *EffectTool = [editor.toolInfo subToolInfoWithToolName:@"CLEffectTool" recursive:NO];
        EffectTool.dockedNumber = 3;
//        CLImageToolInfo *ClippingTool = [editor.toolInfo subToolInfoWithToolName:@"CLClippingTool" recursive:NO];
//        ClippingTool.dockedNumber = 1;
        CLImageToolInfo *RotateTool = [editor.toolInfo subToolInfoWithToolName:@"CLRotateTool" recursive:NO];
        RotateTool.dockedNumber = 1;
        [picker pushViewController:editor animated:YES];
    }
}

-(void)image:(UIImage *)image finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Save failed"
                              message: @"Failed to save image"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark- CLImageEditor delegate

- (void)imageEditor:(CLImageEditor *)editor didFinishEdittingWithImage:(UIImage *)editedImage
{
    [editor dismissViewControllerAnimated:YES completion:nil];
    DebugLog(@"gallery : saveToalbum %@",editedImage);
    broadcastImageView.image = editedImage;
    messageImageView.image = editedImage;
    [self reloadScrollViewwithImageView];
    descriptionViewtitleTextView.text = @"";
    [self.view bringSubviewToFront:addImageDescriptionView];
    [addImageDescriptionView setHidden:FALSE];
    //add animation code here
    [CommonCallback viewtransitionInCompletion:addImageDescriptionView completion:^{
        [CommonCallback viewtransitionOutCompletion:addImageDescriptionView completion:nil];
    }];
    
    if (newMedia)
        UIImageWriteToSavedPhotosAlbum(editedImage,
                                       self,
                                       @selector(image:finishedSavingWithError:contextInfo:),
                                       nil);
}

-(void)reloadScrollViewwithImageView{
    [self adjustScrollView];
}

- (IBAction)descriptionViewSetBtnPressed:(id)sender {
    [addImageDescriptionView setHidden:TRUE];
    [self.view sendSubviewToBack:addImageDescriptionView];
    addImageDescriptionView.frame = CGRectMake(addImageDescriptionView.frame.origin.x, 58, addImageDescriptionView.frame.size.width, addImageDescriptionView.frame.size.height);
}

- (IBAction)descriptionViewSkipBtnPressed:(id)sender {
    descriptionViewtitleTextView.text = @"";
    [self descriptionViewSetBtnPressed:nil];
}

- (IBAction)chooseImageBtnPressed:(id)sender {
    [self scrollTobottom];
    [self useCameraRoll];    
    NSDictionary *editstoreParams = [NSDictionary dictionaryWithObjectsAndKeys:@"Gallery",@"Source", nil];
    [INEventLogger logEvent:@"NewPost_ImageAdded" withParams:editstoreParams];
}

- (IBAction)takePictureBtnPressed:(id)sender {
    [self scrollTobottom];
    [self useCamera];
    NSDictionary *editstoreParams = [NSDictionary dictionaryWithObjectsAndKeys:@"Camera",@"Source", nil];
    [INEventLogger logEvent:@"NewPost_ImageAdded" withParams:editstoreParams];
}

- (IBAction)isOfferBtnPressed:(id)sender {
    if([sender isSelected])
    {
        [sender setSelected:FALSE];
        [UIView animateWithDuration:0.5 animations:^{
            BlblSeperator5.frame = CGRectMake(BlblSeperator5.frame.origin.x,CGRectGetMaxY(isOfferlbl.frame)+5, BlblSeperator5.frame.size.width, BlblSeperator5.frame.size.height);
            reviewBtn.frame = CGRectMake(reviewBtn.frame.origin.x, CGRectGetMaxY(BlblSeperator5.frame)+3, reviewBtn.frame.size.width, reviewBtn.frame.size.height);
            self.broadcastScrollView.contentSize = CGSizeMake(self.broadcastScrollView.frame.size.width,CGRectGetMaxY(reviewBtn.frame)+30);
            calenderImage.hidden = TRUE;
            timeBtn.hidden = TRUE;
        } completion:^(BOOL finished) {
        }];
        validitylbl.hidden = TRUE;
    } else {
        [sender setSelected:TRUE];
        [UIView animateWithDuration:0.5 animations:^{
            BlblSeperator5.frame = CGRectMake(BlblSeperator5.frame.origin.x,CGRectGetMaxY(timeBtn.frame)+2, BlblSeperator5.frame.size.width, BlblSeperator5.frame.size.height);
            reviewBtn.frame = CGRectMake(reviewBtn.frame.origin.x, CGRectGetMaxY(BlblSeperator5.frame)+3, reviewBtn.frame.size.width, reviewBtn.frame.size.height);
            self.broadcastScrollView.contentSize = CGSizeMake(self.broadcastScrollView.frame.size.width,CGRectGetMaxY(reviewBtn.frame)+30);
            calenderImage.hidden = FALSE;
            timeBtn.hidden = FALSE;
        } completion:^(BOOL finished) {
        }];
        validitylbl.hidden = FALSE;
    }
}

- (IBAction)reviewBtnPressed:(id)sender {
    if([titleTextField.text isEqualToString:@""]){
        [INUserDefaultOperations showAlert:@"Write a headline for your message."];
        return;
    }
    if ([[messageTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0  || [messageTextView.text isEqualToString:TXT_DESCRIPTION_PLACEHOLDER_TEXT]) {
        [INUserDefaultOperations showAlert:@"Please enter description."];
        return;
    }
    if([offerBtn isSelected] && ([timeBtn.titleLabel.text isEqualToString:@""] || [timeBtn.titleLabel.text isEqualToString:BTN_OFFER_PLACEHOLDER_TEXT])){
        [INUserDefaultOperations showAlert:@"Please select a date till which the offer is valid."];
        return;
    }
    [titilelbl setText:titleTextField.text];
    [messagelbl setText:messageTextView.text];
    [messageImageView setImage:broadcastImageView.image];
    lblImageCaption.text = descriptionViewtitleTextView.text;

    //Adjust Review View Layout
    double nextYaxis = CGRectGetMaxY(validitylbl.frame)+5;
    
    CGSize titilelblSize = [titilelbl.text sizeWithFont:DEFAULT_FONT(18.0) constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
    
    CGSize messagelblSize = [messagelbl.text sizeWithFont:DEFAULT_FONT(18.0) constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
    
    [titilelbl setFrame:CGRectMake(titilelbl.frame.origin.x,
                                   titilelbl.frame.origin.y,
                                   titilelbl.frame.size.width,
                                   titilelblSize.height + 30)];
    
    [RlblSeperator1 setFrame:CGRectMake(RlblSeperator1.frame.origin.x,
                                        CGRectGetMaxY(titilelbl.frame),
                                        RlblSeperator1.frame.size.width,
                                        RlblSeperator1.frame.size.height)];
    
    [messagelbl setFrame:CGRectMake(messagelbl.frame.origin.x,
                                    CGRectGetMaxY(RlblSeperator1.frame),
                                    messagelbl.frame.size.width,
                                    messagelblSize.height + 30)];
    
    [RlblSeperator2 setFrame:CGRectMake(RlblSeperator2.frame.origin.x,
                                        CGRectGetMaxY(messagelbl.frame),
                                        RlblSeperator2.frame.size.width,
                                        RlblSeperator2.frame.size.height)];
    
    nextYaxis = CGRectGetMaxY(RlblSeperator2.frame)+5;
    
    RlblSeperator2.hidden   = FALSE;
    RlblSeperator3.hidden   = FALSE;
    messageImageView.frame = CGRectMake(messageImageView.frame.origin.x,CGRectGetMaxY(RlblSeperator2.frame)-4, messageImageView.frame.size.width, messageImageView.frame.size.height);
    
    RlblSeperator4.hidden = TRUE;
    lblImageCaption.hidden = TRUE;
    offerImageView.hidden = TRUE;

    if (broadcastImageView.isHidden  && validitylbl.isHidden) {
        RlblSeperator2.hidden   = TRUE;
        RlblSeperator3.hidden   = TRUE;
    }else if (broadcastImageView.isHidden && !validitylbl.isHidden){
        validitylbl.frame = CGRectMake(validitylbl.frame.origin.x,nextYaxis+1, validitylbl.frame.size.width, validitylbl.frame.size.height);
        nextYaxis = CGRectGetMaxY(validitylbl.frame)+5;
        messageImageView.hidden = TRUE;
        RlblSeperator3.hidden   = TRUE;
    }else if (!broadcastImageView.isHidden && validitylbl.isHidden){
        nextYaxis = CGRectGetMaxY(messageImageView.frame)-4;
        RlblSeperator3.hidden   = TRUE;
        messageImageView.hidden = FALSE;
    }else{
        offerImageView.frame = CGRectMake(offerImageView.frame.origin.x,messageImageView.frame.origin.y, offerImageView.frame.size.width, offerImageView.frame.size.height);
        RlblSeperator3.frame = CGRectMake(RlblSeperator3.frame.origin.x,CGRectGetMaxY(messageImageView.frame)-4, RlblSeperator3.frame.size.width, RlblSeperator3.frame.size.height);
        validitylbl.frame = CGRectMake(validitylbl.frame.origin.x,CGRectGetMaxY(RlblSeperator3.frame)+1, validitylbl.frame.size.width, validitylbl.frame.size.height);
        nextYaxis = CGRectGetMaxY(validitylbl.frame)+5;
        messageImageView.hidden = FALSE;
        offerImageView.hidden = FALSE;
    }
    
    RlblSeperator5.frame = CGRectMake(RlblSeperator5.frame.origin.x,nextYaxis+1, RlblSeperator5.frame.size.width, RlblSeperator5.frame.size.height);
    editBtn.frame = CGRectMake(editBtn.frame.origin.x, CGRectGetMaxY(RlblSeperator5.frame)+5, editBtn.frame.size.width, editBtn.frame.size.height);
    postBtn.frame = CGRectMake(postBtn.frame.origin.x, CGRectGetMaxY(RlblSeperator5.frame)+5, postBtn.frame.size.width, postBtn.frame.size.height);
    
    if (broadcastImageView.isHidden) {
        self.reviewScrollView.frame = CGRectMake(reviewScrollView.frame.origin.x,reviewScrollView.frame.origin.x, reviewScrollView.frame.size.width, CGRectGetMaxY(postBtn.frame)+20);
    }else{
        self.reviewScrollView.frame = CGRectMake(reviewScrollView.frame.origin.x,reviewScrollView.frame.origin.x, reviewScrollView.frame.size.width, self.view.frame.size.height-24);
    }
    self.reviewScrollView.contentSize = CGSizeMake(self.reviewScrollView.frame.size.width,CGRectGetMaxY(postBtn.frame)+20);
    [UIView  transitionWithView:self.broadcastScrollView duration:0.8  options:UIViewAnimationOptionTransitionFlipFromLeft
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         self.broadcastScrollView.hidden = TRUE;
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:nil];
    [UIView  transitionWithView:self.reviewScrollView duration:0.8  options:UIViewAnimationOptionTransitionFlipFromLeft
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         self.reviewScrollView.hidden = FALSE;
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:^(BOOL finished){
                         [self scrollTobottom];
                     }];
}

- (IBAction)editBtnPressed:(id)sender {
    [UIView  transitionWithView:self.broadcastScrollView duration:0.8  options:UIViewAnimationOptionTransitionFlipFromRight
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         self.broadcastScrollView.hidden = FALSE;
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:nil];
    [UIView  transitionWithView:self.reviewScrollView duration:0.8  options:UIViewAnimationOptionTransitionFlipFromRight
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         self.reviewScrollView.hidden = TRUE;
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:nil];
}

- (IBAction)postBtnPressed:(id)sender {
    if([titleTextField.text isEqualToString:@""]){
        [INUserDefaultOperations showAlert:@"Write a headline for your message."];
        return;
    }
    if ([[messageTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0  || [messageTextView.text isEqualToString:TXT_DESCRIPTION_PLACEHOLDER_TEXT]) {
        [INUserDefaultOperations showAlert:@"Please enter description."];
        return;
    }
    if([offerBtn isSelected] && ([timeBtn.titleLabel.text isEqualToString:@""] || [timeBtn.titleLabel.text isEqualToString:BTN_OFFER_PLACEHOLDER_TEXT])){
        [INUserDefaultOperations showAlert:@"Please select a date till which the offer is valid."];
        return;
    }
    
    if ([IN_APP_DELEGATE networkavailable]) {
        if (placeidArray.count == 0 ) {
            DebugLog(@"NewPost : Don't have place_id -> %@ or",placeidArray);
        }else{
            [self sendStoreBroadcastRequest];
        }
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

- (IBAction)setDateBtnPressed:(id)sender {
    DebugLog(@"setDateBtnPressed");
    _actionSheetPicker = [[ActionSheetDatePicker alloc] initWithTitle:@"" datePickerMode:UIDatePickerModeDateAndTime selectedDate:self.selectedDate minimumDate:self.selectedDate target:self action:@selector(dateWasSelected:element:) origin:sender];
    //    [self.actionSheetPicker addCustomButtonWithTitle:@"Yesterday" value:[[NSDate date] TC_dateByAddingCalendarUnits:NSDayCalendarUnit amount:-1]];
    [self.actionSheetPicker addCustomButtonWithTitle:@"Today" value:[NSDate date]];
    [self.actionSheetPicker addCustomButtonWithTitle:@"Tommorow" value:[[NSDate date] TC_dateByAddingCalendarUnits:NSDayCalendarUnit amount:+1]];
    self.actionSheetPicker.hideCancel = YES;
    [self.actionSheetPicker showActionSheetPicker];
}

- (void)dateWasSelected:(NSDate *)selectedDate element:(id)element {
    self.selectedDate = selectedDate;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    datein24formatString = [dateFormatter stringFromDate:selectedDate];
    DebugLog(@"dateString->%@",datein24formatString);
    
    DebugLog(@"is 12 hour clock %d", [CommonCallback use24HourClock]);
    
    if([CommonCallback use24HourClock])
    {
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"yyyy-MM-dd hh:mm:ss a"];
        NSString *dateString1 = [dateFormatter1 stringFromDate:selectedDate];
        DebugLog(@"dateString1->%@",dateString1);
        [validitylbl setText:[NSString stringWithFormat:@"Offer Valid Till : %@",dateString1]];
        [timeBtn setTitle:dateString1 forState: UIControlStateNormal];
        [timeBtn setTitle:dateString1 forState: UIControlStateHighlighted];
        
    } else {
        [validitylbl setText:[NSString stringWithFormat:@"Offer Valid Till : %@",datein24formatString]];
        [timeBtn setTitle:datein24formatString forState: UIControlStateNormal];
        [timeBtn setTitle:datein24formatString forState: UIControlStateHighlighted];
    }
}

- (IBAction)imageCancelBtnPressed:(id)sender {
    broadcastImageView.image = nil;
    messageImageView.image = nil;
    [self adjustScrollView];
}

-(void)adjustScrollView{
    [UIView animateWithDuration:0.5 animations:^{
        bool result = CGSizeEqualToSize(broadcastImageView.image.size, CGSizeZero);
        DebugLog(@"%d",result);
        if(result==0) //Not empty
        {
            broadcastImageView.frame = CGRectMake(broadcastImageView.frame.origin.x,CGRectGetMaxY(messageTextView.frame), broadcastImageView.frame.size.width, broadcastImageView.frame.size.height);
            
            imageCancelBtn.frame = CGRectMake(CGRectGetWidth(broadcastImageView.frame)-CGRectGetWidth(imageCancelBtn.frame),broadcastImageView.frame.origin.y, imageCancelBtn.frame.size.width, imageCancelBtn.frame.size.height);
            
            broadcastImageView.hidden   = FALSE;
            imageCancelBtn.hidden       = FALSE;
            messageImageView.hidden     = FALSE;
            
            chosseImageBtn.frame = CGRectMake(chosseImageBtn.frame.origin.x,CGRectGetMaxY(broadcastImageView.frame)+2, chosseImageBtn.frame.size.width, chosseImageBtn.frame.size.height);
            takePictureBtn.frame = CGRectMake(takePictureBtn.frame.origin.x, CGRectGetMaxY(broadcastImageView.frame)+2, takePictureBtn.frame.size.width, takePictureBtn.frame.size.height);
            
        }else{
            broadcastImageView.hidden   = TRUE;
            messageImageView.hidden     = TRUE;
            imageCancelBtn.hidden       = TRUE;
            broadcastImageView.image    = nil;
            messageImageView.image      = nil;
            
            chosseImageBtn.frame = CGRectMake(chosseImageBtn.frame.origin.x,CGRectGetMaxY(BlblSeperator2.frame)+2, chosseImageBtn.frame.size.width, chosseImageBtn.frame.size.height);
            takePictureBtn.frame = CGRectMake(takePictureBtn.frame.origin.x, CGRectGetMaxY(BlblSeperator2.frame)+2, takePictureBtn.frame.size.width, takePictureBtn.frame.size.height);
        }
        
        BlblSeperator3.frame = CGRectMake(BlblSeperator3.frame.origin.x,CGRectGetMaxY(takePictureBtn.frame)+2, BlblSeperator3.frame.size.width, BlblSeperator3.frame.size.height);
        
        tagsTextField.frame = CGRectMake(tagsTextField.frame.origin.x, CGRectGetMaxY(BlblSeperator3.frame), tagsTextField.frame.size.width, tagsTextField.frame.size.height);
        
        BlblSeperator4.frame = CGRectMake(BlblSeperator4.frame.origin.x,CGRectGetMaxY(tagsTextField.frame), BlblSeperator4.frame.size.width, BlblSeperator4.frame.size.height);
        
        double yaxis = CGRectGetMaxY(BlblSeperator4.frame)+2;
        
        isOfferlbl.frame = CGRectMake(isOfferlbl.frame.origin.x, yaxis, isOfferlbl.frame.size.width, isOfferlbl.frame.size.height);
        offerBtn.frame = CGRectMake(offerBtn.frame.origin.x, yaxis, offerBtn.frame.size.width, offerBtn.frame.size.height);
        calenderImage.frame = CGRectMake(calenderImage.frame.origin.x, CGRectGetMaxY(isOfferlbl.frame)+10, calenderImage.frame.size.width, calenderImage.frame.size.height);
        timeBtn.frame = CGRectMake(timeBtn.frame.origin.x, CGRectGetMaxY(isOfferlbl.frame)+10, timeBtn.frame.size.width, timeBtn.frame.size.height);
        
        if (offerBtn.isSelected) {
            BlblSeperator5.frame = CGRectMake(BlblSeperator5.frame.origin.x,CGRectGetMaxY(timeBtn.frame), BlblSeperator5.frame.size.width, BlblSeperator5.frame.size.height);
        }else{
            BlblSeperator5.frame = CGRectMake(BlblSeperator5.frame.origin.x,CGRectGetMaxY(isOfferlbl.frame)+5, BlblSeperator5.frame.size.width, BlblSeperator5.frame.size.height);
        }
        
        reviewBtn.frame = CGRectMake(reviewBtn.frame.origin.x, CGRectGetMaxY(BlblSeperator5.frame)+3, reviewBtn.frame.size.width, reviewBtn.frame.size.height);
        
        self.broadcastScrollView.contentSize = CGSizeMake(self.broadcastScrollView.frame.size.width,CGRectGetMaxY(reviewBtn.frame)+30);
    }];
}

-(void)sendStoreBroadcastRequest
{
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    DebugLog(@"%@",placeidArray);
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *postparams = [[NSMutableDictionary alloc] init];
    if ([titleTextField.text length] > 0) {
        [postparams setObject:titleTextField.text forKey:@"title"];
    }else{
        [postparams setObject:@"" forKey:@"title"];
    }
    [postparams setObject:placeidArray forKey:@"place_id"];
    if ([messageTextView.text length] > 0) {
        [postparams setObject:messageTextView.text forKey:@"description"];
    }else{
        [postparams setObject:@"" forKey:@"description"];
    }
    [postparams setObject:@"" forKey:@"url"];
    [postparams setObject:@"published" forKey:@"state"];
    if ([tagsTextField.text length] > 0) {
        [postparams setObject:tagsTextField.text forKey:@"tags"];
    }else{
        [postparams setObject:@"" forKey:@"tags"];
    }
    [postparams setObject:@"html" forKey:@"format"];
    if([offerBtn isSelected])
    {
        [postparams setObject:@"1" forKey:@"is_offered"];
        [postparams setObject:datein24formatString forKey:@"offer_date_time"];
    } else {
        [postparams setObject:@"0" forKey:@"is_offered"];
        [postparams setObject:@"" forKey:@"offer_date_time"];
    }
    [postparams setObject:[INUserDefaultOperations getMerchantAuthId] forKey:@"user_id"];
    [postparams setObject:[INUserDefaultOperations getMerchantAuthCode] forKey:@"auth_id"];
    DebugLog(@"----%@------",postparams);
    ////////////////////////////////////////////////////////////
    NSMutableDictionary *fileparams = [[NSMutableDictionary alloc] init];
    bool result = CGSizeEqualToSize(broadcastImageView.image.size, CGSizeZero);
    DebugLog(@"%d",result);
    if(result==0) //Not empty
    {
        [postparams setObject:@"photo" forKey:@"type"];
        NSData *dataObj = UIImageJPEGRepresentation(broadcastImageView.image, 0.75);
        NSString *image = [IN_APP_DELEGATE base64forData:dataObj];
        [fileparams setObject:image forKey:@"userfile"];
        [fileparams setObject:@"logo.jpg" forKey:@"filename"];
        [fileparams setObject:@"image/jpeg" forKey:@"filetype"];
        
        //        if (![imageCaptionTextField.text isEqualToString:@""]) {
        //            [fileparams setObject:imageCaptionTextField.text forKey:@"title"];
        //        }
        if (![descriptionViewtitleTextView.text isEqualToString:@""]) {
            [fileparams setObject:descriptionViewtitleTextView.text forKey:@"title"];
        }
        
        NSMutableArray *fileArray = [[NSMutableArray alloc] initWithCapacity:1];
        [fileArray addObject:fileparams];
        DebugLog(@"%@",fileArray);
        [postparams setObject:fileArray forKey:@"file"];
    }
    ////////////////////////////////////////////////////////////
    [httpClient postPath:BROADCAST_LINK parameters:postparams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json afnetworking ->%@",json);
        if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"true"])
        {
            [INEventLogger logEvent:@"NewPost_Posted"];
            [INUserDefaultOperations showAlert:[json  objectForKey:@"message"]];
            [self.navigationController popToRootViewControllerAnimated:TRUE];
            
            //Merchant Posted an offer so record the date to decide when next to show him local notification
            [INUserDefaultOperations setTalkNowDate];
            [INUserDefaultOperations setTalkNowNotification];
            
        } else {
            if ([json  objectForKey:@"code"] != [NSNull null] && ![[json  objectForKey:@"code"] isEqualToString:@""])
            {
                if([[json objectForKey:@"success"] isEqualToString:@"false"]  && [[json  objectForKey:@"code"] isEqualToString:MERCHANT_INVALID_USER_AUTH_CODE])
                {
                    if([INUserDefaultOperations isMerchantLoggedIn]){
                        [INUserDefaultOperations showAlert:MERCHANT_INVALID_USER_AUTH_MESSAGE];
                        [INUserDefaultOperations clearMerchantDetails];
                        [self.navigationController popToRootViewControllerAnimated:TRUE];
                    }
                } else {
                    [INUserDefaultOperations showAlert:[json  objectForKey:@"message"]];
                }
                [INEventLogger logEvent:@"NewPost_PostFailed"];
            } else {
                [INUserDefaultOperations showAlert:@"Unable to broadcast your message.Please try again later."];
            }
        }
        //{"status":"set","message":[{"post_id":665,"place_id":"777"}]}
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        [CommonCallback hideProgressHud];
    }];
}
@end
