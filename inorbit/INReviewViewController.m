//
//  INReviewViewController.m
//  inorbit
//
//  Created by Rishi on 20/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INReviewViewController.h"

@interface INReviewViewController ()

@end

@implementation INReviewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)editBtnPressed:(id)sender {
}

- (IBAction)postBtnPressed:(id)sender {
}
- (void)viewDidUnload {
    [self setTitlelbl:nil];
    [self setDatelbl:nil];
    [self setDescriptionlbl:nil];
    [super viewDidUnload];
}
@end
