//
//  INAllStoreObj.m
//  shoplocal
//
//  Created by Rishi on 05/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INAllStoreObj.h"

@implementation INAllStoreObj
@synthesize ID;
@synthesize place_parent;
@synthesize area;
@synthesize areaID;
@synthesize building;
@synthesize city;
@synthesize description;
@synthesize distance;
@synthesize email;
@synthesize has_offer;
@synthesize image_url;
@synthesize landmark;
@synthesize mob_no1;
@synthesize mob_no2;
@synthesize mob_no3;
@synthesize tel_no1;
@synthesize tel_no2;
@synthesize tel_no3;
@synthesize name;
@synthesize street;
@synthesize title;
@synthesize total_like;
@synthesize website;
@synthesize verified;
@end
