//
//  INAllStoreObj.h
//  shoplocal
//
//  Created by Rishi on 05/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface INAllStoreObj : NSObject
{}
@property (nonatomic, readwrite) NSInteger ID;
@property (nonatomic, readwrite) NSInteger place_parent;
@property (nonatomic, copy) NSString *area;
@property (nonatomic, copy) NSString *areaID;
@property (nonatomic, copy) NSString *building;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *description;
@property (nonatomic, copy) NSString *distance;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *has_offer;
@property (nonatomic, copy) NSString *image_url;
@property (nonatomic, copy) NSString *landmark;
@property (nonatomic, copy) NSString *mob_no1;
@property (nonatomic, copy) NSString *mob_no2;
@property (nonatomic, copy) NSString *mob_no3;
@property (nonatomic, copy) NSString *tel_no1;
@property (nonatomic, copy) NSString *tel_no2;
@property (nonatomic, copy) NSString *tel_no3;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *street;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *total_like;
@property (nonatomic, copy) NSString *website;
@property (nonatomic, copy) NSString *verified;
@end
