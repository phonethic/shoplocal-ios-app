//
//  INMerchantLoginViewController.m
//  shoplocal
//
//  Created by Rishi on 12/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INMerchantLoginViewController.h"
#import "INMerchantSignUPViewController.h"
#import "INMultiSelectStoreListViewController.h"
#import "INAppDelegate.h"
#import "CommonCallback.h"
#import "INCountryCodeObj.h"
#import "ActionSheetPicker.h"
#import "INStoreDetailObj.h"
#import "INPrivacyPolicyViewController.h"
#import "UIView+Genie.h"
#import "UAPush.h"

#define REGISTER_MERCHANT [NSString stringWithFormat:@"%@%@%@merchant_api/new_merchant",LIVE_SERVER,URL_PREFIX,API_VERSION]
#define LOGIN_MERCHANT [NSString stringWithFormat:@"%@%@merchant_api/validate_credentials",URL_PREFIX,API_VERSION]
#define MOBILE_NUMBER(CODE,NUMBER)  [NSString stringWithFormat:@"%@%@",[CODE stringByReplacingOccurrencesOfString:@"+" withString:@""],NUMBER]
#define MERCHANT_STORE_LIST [NSString stringWithFormat:@"%@%@%@place_api/places",LIVE_SERVER,URL_PREFIX,API_VERSION]

@interface INMerchantLoginViewController ()
@property(strong) NSDictionary *splashJson;
@property (nonatomic) BOOL viewIsIn;
@end

@implementation INMerchantLoginViewController
@synthesize usernameText;
@synthesize passwordtext;
@synthesize loginBtn;
@synthesize signupBtn;
@synthesize forgotBtn;
@synthesize loginView,lblHeader,lblfirsttimeuser,signUpView;
@synthesize countryCodeArray;
@synthesize countryCodePickerArray;
@synthesize codeText;
@synthesize storeArray;
@synthesize rowcount;
@synthesize infoView,infoPrivacyBtn,infoOKBtn,lblinfoHeader,lblinfoContent;
@synthesize infoBtn;
@synthesize loginQuesBtn;
@synthesize txtViewinfoContent;
@synthesize merchantImageView;
@synthesize merchantnameText;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Merchant Login";
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    DebugLog(@"viewDidAppear");
//    DebugLog(@"isOpenedTermsViewWhileSignUp %d",isOpenedTermsViewWhileSignUp);
//    DebugLog(@"isRetryingToAcceptTerms %d",isRetryingToAcceptTerms);
//    if (isOpenedTermsViewWhileSignUp) {
//        isOpenedTermsViewWhileSignUp = FALSE;
//        if([usernameText.text length] ==  10 && [[INUserDefaultOperations getMerchantTermsConditionsValue] isEqualToString:@"1"])
//        {
//            [self signupUser];
//        }else{
//            [self showTermsRetryAlert];
//        }
//    }
//    if (isRetryingToAcceptTerms) {
//        isRetryingToAcceptTerms = FALSE;
//        if([usernameText.text length] ==  10 && [[INUserDefaultOperations getMerchantTermsConditionsValue] isEqualToString:@"1"])
//        {
//            [self signupUser];
//        }else if([usernameText.text length] ==  10 && [[INUserDefaultOperations getMerchantTermsConditionsValue] isEqualToString:@"0"]){
//            [self showTermsRetryAlert];
//        }
//    }
//    if (loginView.alpha == 0) {
//        [self performSelector:@selector(performAnimation) withObject:nil afterDelay:0.5];
//    }
}
-(void)performAnimation{
    [UIView animateWithDuration:0.5 animations:^{
        merchantImageView.alpha = 1.0;
    } completion:^(BOOL finished){
        [UIView animateWithDuration:0.5 animations:^{
            loginView.alpha = 1.0;
            CABasicAnimation* scaleUp = [CABasicAnimation animationWithKeyPath:@"transform"];
            scaleUp.duration = 0.2;
            scaleUp.fromValue = [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1)];
            scaleUp.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.2, 1.2, 1)];
            [self.loginView.layer addAnimation:scaleUp forKey:nil];
        }];
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    merchantImageView.alpha = 0.0;
//    loginView.alpha = 0.0;

    isOpenedTermsViewWhileSignUp = FALSE;
    isRetryingToAcceptTerms = FALSE;
    countryCodeArray = [[NSMutableArray alloc] init];
    countryCodePickerArray = [[NSMutableArray alloc] init];
    storeArray = [[NSMutableArray alloc] init];
    [self setUI];
    
//    infoBtn.layer.transform = CATransform3DMakeScale(0.80, 0.80, 1);
//    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform"];
//	animation.toValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
//	animation.autoreverses = YES;
//	animation.duration = 0.40;
//	animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//	animation.repeatCount = HUGE_VALF;
//	[infoBtn.layer addAnimation:animation forKey:@"pulseAnimation"];
    
//    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(switchController)];
//    self.navigationItem.rightBarButtonItem = anotherButton;
//     [loginBtn setBackgroundImage:[UIImage imageNamed:@"Transparent_Button.png"] forState:UIControlStateNormal];
//     [signupBtn setBackgroundImage:[UIImage imageNamed:@"Transparent_Button.png"] forState:UIControlStateNormal];
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    viewTap.numberOfTapsRequired = 1;
    viewTap.cancelsTouchesInView = NO;
    [self.view  addGestureRecognizer:viewTap];
    
    if(![[INUserDefaultOperations getMerchantId] isEqualToString:@""])
    {
        usernameText.text = [INUserDefaultOperations getMerchantId];
    }
    
    NSError* error;
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"country_code" ofType:@"txt"];
    if (filePath) {
        NSString *data = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:[data dataUsingEncoding:NSUTF8StringEncoding]  options:kNilOptions error:&error];
        
        for(NSDictionary *obj in json) {
            INCountryCodeObj *codeObj = [[INCountryCodeObj alloc] init];
            codeObj.countryName = [obj objectForKey:@"name"];
            codeObj.countrycode = [obj objectForKey:@"code"];
            codeObj.countrydialcode = [obj objectForKey:@"dial_code"];
            [countryCodeArray addObject:codeObj];
            [countryCodePickerArray addObject:[NSString stringWithFormat:@"%@ (%@)",[obj objectForKey:@"name"],[obj objectForKey:@"dial_code"]]];
            codeObj = nil;
        }
    }
    
    if([[INUserDefaultOperations getMerchantCountryCode] isEqualToString:@""])
    {
//        NSLocale *locale = [NSLocale currentLocale];
//        NSString *countryCode = [locale objectForKey: NSLocaleCountryCode];
//        DebugLog(@"countryCode:%@", countryCode);
//        NSString *country = [locale displayNameForKey: NSLocaleCountryCode value: countryCode];
//        DebugLog(@"country:%@", country);
//        
//        for (int i=0; i < [countryCodeArray count] ; i++) {
//            INCountryCodeObj *codeObj = [countryCodeArray objectAtIndex:i];
//            DebugLog(@"%@",codeObj.countrycode);
//            if([codeObj.countrycode isEqualToString:countryCode])
//            {
//                [codeText setText:codeObj.countrydialcode];
//                break;
//            }
//        }
        if([codeText.text isEqualToString:@""])
        {
            [codeText setText:@"+91"];
        }
    } else {
        codeText.text = [NSString stringWithFormat:@"+%@",[INUserDefaultOperations getMerchantCountryCode]];
    }
    
    if([codeText.text isEqualToString:@""])
    {
        [codeText setText:@"+91"];
    }

    
    self.viewIsIn = TRUE;
    
    loginView.hidden = TRUE;
    
    [self.view insertSubview:loginView belowSubview:merchantImageView];
    
    [self performSelector:@selector(animateMerchant) withObject:nil afterDelay:0.5];

    //[self addHUDView];
    
}

-(void)animateMerchant
{
    [self genieToRect:merchantImageView.frame edge:BCRectEdgeTop];
}

- (void) genieToRect: (CGRect)rect edge: (BCRectEdge) edge
{
    
    CGRect endRect = CGRectInset(rect, 20.0, 20.0);
    
    if (self.viewIsIn) {
        loginView.hidden = FALSE;
        [self.loginView genieOutTransitionWithDuration:1.0 startRect:endRect startEdge:edge completion:^{
            self.loginView.userInteractionEnabled = YES;
        }];
    } else {
        self.loginView.userInteractionEnabled = NO;
        [self.loginView genieInTransitionWithDuration:1.0 destinationRect:endRect destinationEdge:edge completion:
         ^{
             loginView.hidden = FALSE;
         }];
    }
    
    self.viewIsIn = ! self.viewIsIn;
}


#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
-(void)setUI
{
//    
//    NSArray *fonts = [UIFont familyNames];
//    
//    for(NSString *string in fonts){
//        DebugLog(@"%@", string);
//    }
    loginView = [CommonCallback setViewPropertiesWithRoundedCorner:loginView];
    signUpView = [CommonCallback setViewPropertiesWithRoundedCorner:signUpView];
    loginView.backgroundColor = [UIColor clearColor];
    loginView.clipsToBounds = YES;
    
    lblHeader.font          = DEFAULT_FONT(18.0);
    lblfirsttimeuser.font   = DEFAULT_FONT(13.0);
    
    merchantnameText.font = DEFAULT_FONT(15);
    usernameText.font = DEFAULT_FONT(18);
    passwordtext.font = DEFAULT_FONT(18);
    
    codeText.font = DEFAULT_FONT(18);
    codeText.textColor = BROWN_COLOR;
    
    forgotBtn.titleLabel.font    = DEFAULT_FONT(12.0);
    
    merchantnameText.backgroundColor = [UIColor whiteColor];
    usernameText.backgroundColor = [UIColor whiteColor];
    passwordtext.backgroundColor = [UIColor whiteColor];
    
    loginBtn.titleLabel.font = DEFAULT_FONT(18);
    
    [forgotBtn setTitleColor:BROWN_COLOR forState:UIControlStateNormal];
    [forgotBtn setTitleColor:BROWN_OFFWHITE_COLOR forState:UIControlStateHighlighted];
    
    lblHeader.textColor          = BROWN_COLOR;
    lblfirsttimeuser.textColor   = BROWN_COLOR;
    
    merchantnameText.textColor = BROWN_COLOR;
    usernameText.textColor = BROWN_COLOR;
    passwordtext.textColor = BROWN_COLOR;
    
    loginQuesBtn.titleLabel.font = DEFAULT_FONT(14.0);
    [loginQuesBtn setTitleColor:BROWN_COLOR forState:UIControlStateNormal];
    [loginQuesBtn setTitleColor:BROWN_OFFWHITE_COLOR forState:UIControlStateHighlighted];
    
    infoView = [CommonCallback setViewPropertiesWithRoundedCorner:infoView];
    infoView.layer.shadowColor      = [UIColor blackColor].CGColor;
    infoView.layer.shadowOffset     = CGSizeMake(1, 1);
    infoView.layer.shadowOpacity    = 1.0;
    infoView.layer.shadowRadius     = 10.0;
    
    lblinfoHeader.textAlignment     = NSTextAlignmentCenter;
    lblinfoHeader.backgroundColor   = [UIColor clearColor];
    lblinfoHeader.font              = DEFAULT_FONT(20);
    lblinfoHeader.textColor         = [UIColor blackColor];
    lblinfoHeader.adjustsFontSizeToFitWidth = YES;
    lblinfoHeader.text              = ALERT_TITLE;
    
    lblinfoContent.textAlignment    = NSTextAlignmentCenter;
    lblinfoContent.backgroundColor  = [UIColor clearColor];
    lblinfoContent.font             = DEFAULT_FONT(14.8);
    lblinfoContent.textColor        = [UIColor darkGrayColor];
    lblinfoContent.numberOfLines    = 15;
//    lblinfoContent.text             = @"We use your mobile number to create a unique identity for you. This allows us to register you as a unique user and prevents duplication. In case you misplace your password or change your mobile phone, this will let you set a new password or get a New personalized Shoplocal version on your phone. We never share your personal data with anybody. Please see our Privacy Policy for details.";
    
    txtViewinfoContent.textAlignment    = NSTextAlignmentCenter;
    txtViewinfoContent.backgroundColor  = [UIColor clearColor];
    txtViewinfoContent.font             = DEFAULT_FONT(15);
    txtViewinfoContent.textColor        = [UIColor darkGrayColor];
    txtViewinfoContent.text             = @"We use your mobile number to create a unique identity for you. This allows us to register you as a unique user and prevents duplication. In case you misplace your password or change your mobile phone, this will let you set a new password or get a New personalized Shoplocal version on your phone. We never share your personal data with anybody. Please see our Privacy Policy for details.\nAs a logged in Shoplocal Merchant, You can register your stores and also post personalized offers in your area. \nPlease see our Privacy Policy for details.";
    
    infoPrivacyBtn.titleLabel.font  = DEFAULT_FONT([UIFont buttonFontSize]);
    infoOKBtn.titleLabel.font   = DEFAULT_FONT([UIFont buttonFontSize]);
    
    infoPrivacyBtn.backgroundColor   = [UIColor clearColor];
    infoOKBtn.backgroundColor   = [UIColor clearColor];
    
    [infoPrivacyBtn setTitleColor:[UIColor colorWithWhite:0.3 alpha:1] forState:UIControlStateNormal];
    [infoPrivacyBtn setTitleColor:[UIColor colorWithWhite:0.3 alpha:0.8] forState:UIControlStateHighlighted];
    
    [infoOKBtn setTitleColor:[UIColor colorWithWhite:0.3 alpha:1] forState:UIControlStateNormal];
    [infoOKBtn setTitleColor:[UIColor colorWithWhite:0.3 alpha:0.8] forState:UIControlStateHighlighted];
    
    UIImage *normalImage        = [UIImage imageNamed:@"SIAlertView.bundle/button-cancel"];
    UIImage *highlightedImage   = [UIImage imageNamed:@"SIAlertView.bundle/button-cancel-d"];
    CGFloat hInset              = floorf(normalImage.size.width / 2);
	CGFloat vInset              = floorf(normalImage.size.height / 2);
	UIEdgeInsets insets         = UIEdgeInsetsMake(vInset, hInset, vInset, hInset);
	normalImage                 = [normalImage resizableImageWithCapInsets:insets];
	highlightedImage            = [highlightedImage resizableImageWithCapInsets:insets];
	[infoOKBtn setBackgroundImage:normalImage forState:UIControlStateNormal];
	[infoOKBtn setBackgroundImage:highlightedImage forState:UIControlStateHighlighted];
    
    [infoView setHidden:YES];
}

-(void)addHUDView
{
    hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:hud];
	hud.dimBackground = NO;
    hud.labelText = @"Loading";
	hud.detailsLabelText = @"Please wait ...";
    [hud hide:NO];
}

-(void)removeHUDView
{
    [hud removeFromSuperview];
    hud = nil;
}

- (void)tapDetected:(UIGestureRecognizer *)sender {
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setUsernameText:nil];
    [self setPasswordtext:nil];
    [self setLoginBtn:nil];
    [self setSignupBtn:nil];
    [self setForgotBtn:nil];
    [self setLoginView:nil];
    [self setLblHeader:nil];
    [self setLblfirsttimeuser:nil];
    [self setSignUpView:nil];
    [self setCodeText:nil];
    [self setTermsBtn:nil];
    [self setPrivacyBtn:nil];
    [self setInfoBtn:nil];
    [self setInfoView:nil];
    [self setLblinfoHeader:nil];
    [self setLblinfoContent:nil];
    [self setInfoOKBtn:nil];
    [self setInfoPrivacyBtn:nil];
    [self setLoginQuesBtn:nil];
    [self setTxtViewinfoContent:nil];
    [self setMerchantImageView:nil];
    [self setMerchantnameText:nil];
    [super viewDidUnload];
}

- (IBAction)infoOKBtnPressed:(id)sender {
    [infoView setHidden:YES];
}

- (IBAction)infoPrivacyBtnPressed:(id)sender {
    [self privacyBtnPressed:nil];
    [infoView setHidden:YES];
}

- (IBAction)forgotBtnPressed:(id)sender {
    [self forgotPassword];
}

- (IBAction)loginBtnPressed:(id)sender {
    if([usernameText.text isEqualToString:@""] || [passwordtext.text isEqualToString:@""])
    {
        [INUserDefaultOperations showAlert:@"Please fill all the fields."];
    }
    else if([usernameText.text length] < 10) 
    {
        [INUserDefaultOperations showAlert:@"Mobile number must be at least 10 characters in length."];
    }
    else if([passwordtext.text length] < 6)
    {
        [INUserDefaultOperations showAlert:@"Password field must be at least 6 characters in length."];
    } else {
        if ([IN_APP_DELEGATE networkavailable]) {
            [self sendMerchantLoginRequest:1];   //Show Add Store/New Post Offer after Login
        } else {
            [INUserDefaultOperations showOfflineAlert];
        }
    }
}

- (IBAction)signupBtnPressed:(id)sender {
    DebugLog(@"signupBtnPressed ");
    if([usernameText.text isEqualToString:@""])
    {
        [INUserDefaultOperations showAlert:@"Please enter valid mobile number."];
    }
    else if([usernameText.text length] < 10)
    {
        [INUserDefaultOperations showAlert:@"Mobile number must be at least 10 characters in length."];
    } else if([usernameText.text length] > 10)
    {
        [INUserDefaultOperations showAlert:@"Mobile number must be 10 characters in length."];
    }else if([[INUserDefaultOperations getMerchantTermsConditionsValue] isEqualToString:@"0"])
    {
        [self termsBtnPressed:nil];
        isOpenedTermsViewWhileSignUp = YES;
    }   
    else {
        if ([IN_APP_DELEGATE networkavailable]) {
//            [self signupUser];
            [INEventLogger logEvent:@"MSignUp_Proceed"];
            [self sendNumberRegisterRequest];
        } else {
            [INUserDefaultOperations showOfflineAlert];
        }
    }
}
-(void)showTermsRetryAlert{
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"You must read and accept the Terms & conditions to register your business on shoplocal."];
    alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
    alertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
    
    [alertView addButtonWithTitle:@"Try again"
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alert) {
                              DebugLog(@"Retry Clicked");
                              isRetryingToAcceptTerms = YES;
                              [self termsBtnPressed:nil];
                          }];
    [alertView addButtonWithTitle:@"Do not Register"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alert) {
                              DebugLog(@"Do not Register Clicked");
                          }];
    [alertView show];
}

-(void)dismissTerms{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"termsBtn dismiss completion ");
        DebugLog(@"viewDidAppear");
        DebugLog(@"isOpenedTermsViewWhileSignUp %d",isOpenedTermsViewWhileSignUp);
        DebugLog(@"isRetryingToAcceptTerms %d",isRetryingToAcceptTerms);
        DebugLog(@"getTermsConditionsValue %@",[INUserDefaultOperations getTermsConditionsValue]);
        [INUserDefaultOperations setMerchantTermsConditionsValue:[INUserDefaultOperations getTermsConditionsValue]];

        if (isOpenedTermsViewWhileSignUp) {
            isOpenedTermsViewWhileSignUp = FALSE;
            if([usernameText.text length] ==  10 && [[INUserDefaultOperations getMerchantTermsConditionsValue] isEqualToString:@"1"])
            {
            //    [self signupUser];
                [self sendNumberRegisterRequest];
            }else{
                [self showTermsRetryAlert];
            }
        }
        if (isRetryingToAcceptTerms) {
            isRetryingToAcceptTerms = FALSE;
            if([usernameText.text length] ==  10 && [[INUserDefaultOperations getMerchantTermsConditionsValue] isEqualToString:@"1"])
            {
               // [self signupUser];
                [self sendNumberRegisterRequest];
            }else if([usernameText.text length] ==  10 && [[INUserDefaultOperations getMerchantTermsConditionsValue] isEqualToString:@"0"]){
                [self showTermsRetryAlert];
            }
        }
    }];
}

- (IBAction)termsBtnPressed:(id)sender {
    if ([IN_APP_DELEGATE networkavailable]) {
        INTermsViewController *termsController = [[INTermsViewController alloc] initWithNibName:@"INTermsViewController" bundle:nil] ;
        termsController.title = @"";
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:termsController];
        navController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        navController.modalPresentationStyle = UIModalPresentationFormSheet;
        navController.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
        //    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
        //                                                                                   target:self
        //                                                                                   action:@selector(didDismissPresentedViewController)];
        //    loginController.navigationItem.rightBarButtonItem = doneBarButton;
        termsController.navigationItem.title = @"Terms & Conditions";
        termsController.delegate = self;
        [self.navigationController presentViewController:navController animated:YES completion:^{
            DebugLog(@"termsBtn completion ");
        }];
        isOpenedTermsViewWhileSignUp = NO;
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

- (IBAction)privacyBtnPressed:(id)sender {
    if ([IN_APP_DELEGATE networkavailable]) {
        INPrivacyPolicyViewController *privacyController = [[INPrivacyPolicyViewController alloc] initWithNibName:@"INPrivacyPolicyViewController" bundle:nil] ;
        privacyController.title = @"Privacy Policy";
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:privacyController];
        navController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        navController.modalPresentationStyle = UIModalPresentationFormSheet;
        navController.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
        //    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
        //                                                                                   target:self
        //                                                                                   action:@selector(didDismissPresentedViewController)];
        //    loginController.navigationItem.rightBarButtonItem = doneBarButton;
        privacyController.navigationItem.title = @"Privacy Policy";
        //termsController.delegate = self;
        [self.navigationController presentViewController:navController animated:YES completion:NULL];
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

- (IBAction)infoBtnPressed:(id)sender {
    [infoBtn.layer removeAllAnimations];
    [infoView setHidden:FALSE];
    //add animation code here
    [CommonCallback viewtransitionInCompletion:infoView completion:^{
        [CommonCallback viewtransitionOutCompletion:infoView completion:nil];
    }];
}

- (IBAction)loginQuesBtnPressed:(id)sender {
    [infoView setHidden:FALSE];
    //add animation code here
    [CommonCallback viewtransitionInCompletion:infoView completion:^{
        [CommonCallback viewtransitionOutCompletion:infoView completion:nil];
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == usernameText) {
        [passwordtext becomeFirstResponder];
	} else if (textField == passwordtext) {
        [passwordtext resignFirstResponder];
	}
   	return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(textField==usernameText) {
        NSString *usernameString = [usernameText.text stringByReplacingCharactersInRange:range withString:string];
        return !([usernameString length] > 10);
    } else if(textField==merchantnameText) {
        NSString *usernameString = [merchantnameText.text stringByReplacingCharactersInRange:range withString:string];
        return !([usernameString length] > 50);
    } else {
        return YES;
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField==codeText)
    {
        [self setCountryCodeBtnPressed];
        return NO;
    }
    return YES;
}

-(void)setCountryCodeBtnPressed
{
    [ActionSheetStringPicker showPickerWithTitle:@"Select Country" rows:countryCodePickerArray initialSelection:0 target:self successAction:@selector(selectCode:element:) cancelAction:@selector(actionPickerCancelled:) origin:self.codeText];
}

#pragma mark - Actionsheet Implementation
- (void)selectCode:(NSNumber *)lselectedIndex element:(id)element {
    NSInteger selectedIndex = [lselectedIndex integerValue];
    INCountryCodeObj *codeObj = [countryCodeArray objectAtIndex:selectedIndex];
    DebugLog(@"object-->%@  %@ %@",codeObj.countryName,codeObj.countrycode,codeObj.countrydialcode);
    codeText.text = codeObj.countrydialcode;
    DebugLog(@"value-->%@",[countryCodePickerArray objectAtIndex:selectedIndex]);
}


- (void)loginUserWithIdAndPassword:(NSString *)lnumber pass:(NSString *)lpass showAddStore:(int)lval
{
    usernameText.text = lnumber;
    passwordtext.text = lpass;
    if(signupControllernavBar != nil)
    {
        signupController.logindelegate = nil;
        forgotpassController.logindelegate = nil;
        signupController = nil;
        forgotpassController = nil;
        [signupControllernavBar dismissViewControllerAnimated:YES completion:^{
            DebugLog(@"Dismiss signup %d", lval);
            [self sendMerchantLoginRequest:lval];  // 1 means show Add Store/New Post Offer after Login
                                                   // 0 means hide Add Store/New Post Offer after Login
        }];
    }
    
}

-(void)signupUser
{
    DebugLog(@"signupUser");
    signupController = [[INMerchantSignUPViewController alloc] initWithNibName:@"INMerchantSignUPViewController" bundle:nil] ;
    signupController.title = @"Sign Up";
    signupController.screenType = 0;
    signupController.codeNumber = codeText.text;
    signupController.mobileNumber = usernameText.text;
    signupController.logindelegate = self;
    signupControllernavBar = [[UINavigationController alloc]initWithRootViewController:signupController];
    UIButton *moreButton1 =  [UIButton buttonWithType:UIButtonTypeCustom];
    [moreButton1 setTitle:@"Cancel" forState:UIControlStateNormal];
    moreButton1.titleLabel.font = [UIFont systemFontOfSize:15.0];
    //[moreButton1 setBackgroundImage:[UIImage imageNamed:@"Transparent_Button.png"] forState:UIControlStateNormal];
    [moreButton1 addTarget:self action:@selector(dismissSignup) forControlEvents:UIControlEventTouchUpInside];
    [moreButton1 setFrame:CGRectMake(250, 5, 60, 32)];
    [signupControllernavBar.navigationBar addSubview:moreButton1];
    [signupControllernavBar.navigationBar setTranslucent:NO];
    [self.navigationController presentViewController:signupControllernavBar animated:YES completion:nil];
}

-(void)forgotPassword
{
    forgotpassController = [[INMerchantSignUPViewController alloc] initWithNibName:@"INMerchantSignUPViewController" bundle:nil] ;
    forgotpassController.title = @"Forgot Password";
    forgotpassController.screenType = 1;
    forgotpassController.logindelegate = self;
    signupControllernavBar = [[UINavigationController alloc]initWithRootViewController:forgotpassController];
    UIButton *moreButton1 =  [UIButton buttonWithType:UIButtonTypeCustom];
    [moreButton1 setTitle:@"Cancel" forState:UIControlStateNormal];
    moreButton1.titleLabel.font = [UIFont systemFontOfSize:15.0];
    //[moreButton1 setBackgroundImage:[UIImage imageNamed:@"Transparent_Button.png"] forState:UIControlStateNormal];
    [moreButton1 addTarget:self action:@selector(dismissSignup) forControlEvents:UIControlEventTouchUpInside];
    [moreButton1 setFrame:CGRectMake(250, 5, 60, 32)];
    [signupControllernavBar.navigationBar addSubview:moreButton1];
    [signupControllernavBar.navigationBar setTranslucent:NO];
    [self.navigationController presentViewController:signupControllernavBar animated:YES completion:nil];
}

-(void)dismissSignup
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"Dismiss");
        [INUserDefaultOperations setMerchantTermsConditionsValue:@"0"];
    }];
}

-(void)sendNumberRegisterRequest
{
    DebugLog(@"number--->%@",MOBILE_NUMBER(codeText.text, usernameText.text));
    DebugLog(@"link--->%@",REGISTER_MERCHANT);
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:REGISTER_MERCHANT] cachePolicy:NO timeoutInterval:30.0];
    NSMutableDictionary *postReq = [[NSMutableDictionary alloc] init];
    [postReq setObject:MOBILE_NUMBER(codeText.text, usernameText.text) forKey:@"mobile"];
    [postReq setObject:DEVICE_TYPE forKey:@"register_from"];
    if ([merchantnameText.text length] > 0) {
        [postReq setObject:merchantnameText.text forKey:@"name"];
    }else{
        [postReq setObject:@"" forKey:@"name"];
    }
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postReq
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (!jsonData) {
        DebugLog(@"Got an error: %@", error);
    }
    [urlRequest setHTTPBody: jsonData];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setHTTPMethod:@"POST"];
    connectionRegisterNumber = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)activateUser:(NSString *)message
{
    DebugLog(@"activateUser");
    INMerchantVerifyViewController *verifyViewController = [[INMerchantVerifyViewController alloc] initWithNibName:@"INMerchantVerifyViewController" bundle:nil] ;
    verifyViewController.title = @"Shoplocal Registration";
    verifyViewController.codeNumber = codeText.text;
    verifyViewController.mobileNumber = usernameText.text;
    verifyViewController.message = message;
    verifyViewController.logindelegate = self;
    UINavigationController *verifyViewControllernavBar = [[UINavigationController alloc]initWithRootViewController:verifyViewController];
    verifyViewControllernavBar.navigationBar.translucent = NO;
    UIButton *cancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    cancelButton.titleLabel.font = [UIFont systemFontOfSize:17.0];
    //[cancelButton setBackgroundImage:[UIImage imageNamed:@"Transparent_Button.png"] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(dismissActivationView) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setFrame:CGRectMake(250, 7, 70, 32)];
    [verifyViewControllernavBar.navigationBar addSubview:cancelButton];
    [verifyViewControllernavBar.navigationBar setTranslucent:NO];
    [self.navigationController presentViewController:verifyViewControllernavBar animated:YES completion:nil];
}

-(void)dismissActivationView
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"dismissActivationView");
        [INUserDefaultOperations setMerchantTermsConditionsValue:@"0"];
    }];
}

- (void)loginUserWithActivationCode:(NSString *)lnumber pass:(NSString *)lpass showAddStore:(int)lval
{
    usernameText.text = lnumber;
    passwordtext.text = lpass;
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"dismissActivationView");
        DebugLog(@"Dismiss %d", lval);
        [self sendMerchantLoginRequest:lval];  // 1 means show Add Store/New Post Offer after Login
        // 0 means hide Add Store/New Post Offer after Login
    }];
}

-(void)sendMerchantLoginRequest:(int)lval
{
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:MOBILE_NUMBER(codeText.text, usernameText.text) forKey:@"mobile"];
    [params setObject:passwordtext.text forKey:@"password"];
    [httpClient postPath:LOGIN_MERCHANT parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        //[CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            if([[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
                NSDictionary* data = [json objectForKey:@"data"];
                NSString * resultid = [data objectForKey:@"id"];
                NSString * resultcode = [data objectForKey:@"auth_code"];
                NSString* message = [json objectForKey:@"message"];
                DebugLog(@"%@ %@ %@",resultid,resultcode,message);
                [INUserDefaultOperations setMerchantDetails:usernameText.text pass:passwordtext.text];
                [INUserDefaultOperations setMerchantAuthDetails:resultid pass:resultcode];
                [INUserDefaultOperations setMerchantCountryCode:[codeText.text stringByReplacingOccurrencesOfString:@"+" withString:@""]];
                [INUserDefaultOperations setMerchantValidateLoginDate:[NSDate date]];

                [INEventLogger setUser:[NSString stringWithFormat:@"%@%@", @"M", resultid]];
                [self sendMerchantStoreListRequest:lval];
//                [self.navigationController dismissViewControllerAnimated:YES completion:^{
//                    DebugLog(@"Dismiss");
//                    if(lval==1) {
//                        [[NSNotificationCenter defaultCenter] postNotificationName:IN_MERCHANT_LOGIN_NOTIFICATION object:nil];
//                    }
//                }];
                NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:IN_MERCHANT_LOGIN_LOGOUT_KEY];
                [[NSNotificationCenter defaultCenter] postNotificationName:IN_MERCHANT_LOGIN_LOGOUT_NOTIFICATION object:nil userInfo:dictionary];
                [INEventLogger logEvent:@"MSignUp_ShoplocalLoginSuccess"];
                
                //When merchant logged in then add him to UAPush
                [CommonCallback addTagsInUrbanAriship:[[NSArray alloc] initWithObjects:PUSH_TAG_MERCHANT, nil] name:PUSH_TAG_MERCHANT];
//                [[UAPush shared] addTagToCurrentDevice:@"merchant"];
//                [[UAPush shared] updateRegistration];
                
                DebugLog(@"tags %@",[[UAPush shared] tags]);
            } else {
                NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:IN_MERCHANT_LOGIN_LOGOUT_KEY];
                [[NSNotificationCenter defaultCenter] postNotificationName:IN_MERCHANT_LOGIN_LOGOUT_NOTIFICATION object:nil userInfo:dictionary];

                NSString* message = [json objectForKey:@"message"];
                [INUserDefaultOperations showAlert:message];
                [INEventLogger logEvent:@"MSignUp_ShoplocalLoginFailed"];
                
                //When merchant logouts in then remove him from UAPush
//                [[UAPush shared] removeTagFromCurrentDevice:@"merchant"];
//                [[UAPush shared] updateRegistration];
                [CommonCallback removeTagsFromUrbanAriship:PUSH_TAG_MERCHANT];
                [CommonCallback hideProgressHud];
            }
        } else {
            [CommonCallback hideProgressHud];
            [INUserDefaultOperations showAlert:@"No response from server. Please try again later."];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}


-(void)sendMerchantStoreListRequest:(int)lval
{
    //[hud show:YES];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:MERCHANT_STORE_LIST]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setValue:[INUserDefaultOperations getMerchantAuthId] forHTTPHeaderField:@"user_id"];
    [urlRequest setValue:[INUserDefaultOperations getMerchantAuthCode] forHTTPHeaderField:@"auth_id"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        //[hud hide:YES];
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"%@",self.splashJson);
                                                        if(self.splashJson  != nil) {
                                                            [storeArray removeAllObjects];
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                [INUserDefaultOperations setEditStoreState:FALSE];
                                                                NSArray* jsonArray = [self.splashJson  objectForKey:@"data"];
                                                               // DebugLog(@"\n\n%@\n\n",jsonArray);
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *objdict = [jsonArray  objectAtIndex:index];
                                                                    //DebugLog(@"\n\n%@, %d\n\n",objdict,[objdict count]);
                                                                    storeObj = [[INStoreDetailObj alloc] init];
                                                                    storeObj.ID = [[objdict objectForKey:@"id"] integerValue];
                                                                    storeObj.name = [objdict objectForKey:@"name"];
                                                                    storeObj.place_parent = [[objdict objectForKey:@"place_parent"] integerValue];
                                                                    storeObj.place_status = [objdict objectForKey:@"place_status"];
                                                                    storeObj.published = [objdict objectForKey:@"published"];
                                                                    storeObj.description = [objdict objectForKey:@"description"];
                                                                    storeObj.building = [objdict objectForKey:@"building"];
                                                                    storeObj.street = [objdict objectForKey:@"street"];
                                                                    storeObj.landmark = [objdict objectForKey:@"landmark"];
                                                                    storeObj.area = [objdict objectForKey:@"area"];
                                                                    storeObj.pincode = [objdict objectForKey:@"pincode"];
                                                                    storeObj.city = [objdict objectForKey:@"city"];
                                                                    storeObj.state = [objdict objectForKey:@"state"];
                                                                    storeObj.country = [objdict objectForKey:@"country"];
                                                                    storeObj.mob_no1 = [objdict objectForKey:@"mob_no1"];
                                                                    storeObj.mob_no2 = [objdict objectForKey:@"mob_no2"];
                                                                    storeObj.mob_no3 = [objdict objectForKey:@"mob_no3"];
                                                                    storeObj.tel_no1 = [objdict objectForKey:@"tel_no1"];
                                                                    storeObj.tel_no2 = [objdict objectForKey:@"tel_no2"];
                                                                    storeObj.tel_no3 = [objdict objectForKey:@"tel_no3"];
                                                                    storeObj.image_url = [objdict objectForKey:@"image_url"];
                                                                    storeObj.email = [objdict objectForKey:@"email"];
                                                                    storeObj.website = [objdict objectForKey:@"website"];
                                                                    storeObj.total_like = [objdict objectForKey:@"total_like"];
                                                                    storeObj.total_share = [objdict objectForKey:@"total_share"];
                                                                    storeObj.total_view = [objdict objectForKey:@"total_view"];
                                                                    storeObj.fb_url = [objdict objectForKey:@"facebook_url"];
                                                                    storeObj.twitter_url = [objdict objectForKey:@"twitter_url"];
                                                                    storeObj.latitude = [objdict objectForKey:@"latitude"];
                                                                    storeObj.longitude = [objdict objectForKey:@"longitude"];
                                                                    
                                                                    if((storeObj.latitude == 0) || ([storeObj.description length] == 0) || ([storeObj.image_url length] == 0))
                                                                    {
                                                                        [INUserDefaultOperations setEditStoreState:TRUE];
                                                                    }
                                                                    [storeArray addObject:storeObj];
                                                                    storeObj = nil;
                                                                }                                                                
                                                            }
                                                            [self deleteMyPlacesTable];
                                                            [self insertAllStoreListTable];
                                                            [self getCountMyPlacesTable];
                                                            [INUserDefaultOperations setRefreshState:FALSE];
                                                        }
                                                        [CommonCallback hideProgressHud];
                                                        [self.navigationController dismissViewControllerAnimated:YES completion:^{
                                                            DebugLog(@"Dismiss");
                                                            if(rowcount == 0) {
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:IN_MERCHANT_LOGIN_NOTIFICATION object:nil];
                                                            }
                                                        }];
                                                        
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                        [av show];
                                                        
                                                    }];
    
    
    
    [operation start];
}

#pragma Table Operations
- (void)insertAllStoreListTable
{
    NSMutableArray *idtagsArray = [[NSMutableArray alloc] init];

    sqlite3_stmt    *statement;
    int lastrowid;
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        for (int index=0; index<[storeArray count] ; index++)
        {
            INStoreDetailObj *tempstoreObj = [storeArray objectAtIndex:index];
            NSString *insertSQL = [NSString stringWithFormat:
                                   @"INSERT INTO MYPLACES (PLACEID, PLACE_PARENT, NAME, DESCRIPTION, BUILDING, STREET, LANDMARK, AREA, CITY, MOB_NO1, MOB_NO2, MOB_NO3, TEL_NO1, TEL_NO2, TEL_NO3, IMG_URL, EMAIL, WEBSITE, TOTAL_LIKE , TOTAL_SHARE , TOTAL_VIEW, PLACE_STATUS , PUBLISHED , COUNTRY , STATE , PINCODE , FB_URL , TWITTER_URL, LATITUDE , LONGITUDE) VALUES (\"%d\",\"%d\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",tempstoreObj.ID,tempstoreObj.place_parent,tempstoreObj.name,[tempstoreObj.description stringByReplacingOccurrencesOfString:@"\"" withString:@"'"],tempstoreObj.building,tempstoreObj.street,tempstoreObj.landmark,tempstoreObj.area,tempstoreObj.city,tempstoreObj.mob_no1,tempstoreObj.mob_no2,tempstoreObj.mob_no3, tempstoreObj.tel_no1, tempstoreObj.tel_no2, tempstoreObj.tel_no3, tempstoreObj.image_url, tempstoreObj.email, tempstoreObj.website, tempstoreObj.total_like, tempstoreObj.total_share, tempstoreObj.total_view, tempstoreObj.place_status, tempstoreObj.published, tempstoreObj.country,tempstoreObj.state, tempstoreObj.pincode, tempstoreObj.fb_url, tempstoreObj.twitter_url, tempstoreObj.latitude, tempstoreObj.longitude];
            DebugLog(@"%@",insertSQL);
            const char *insert_stmt = [insertSQL UTF8String];
            sqlite3_prepare_v2(shoplocalDB, insert_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                lastrowid = sqlite3_last_insert_rowid(shoplocalDB);
                 DebugLog(@"inserted id========%d", lastrowid);
                [idtagsArray addObject:[NSString stringWithFormat:@"%d",tempstoreObj.ID]];
            } else {
                DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
            }
            sqlite3_finalize(statement);
        }
    }
    sqlite3_close(shoplocalDB);
    if (idtagsArray.count > 0) {
        [CommonCallback addTagsInUrbanAriship:idtagsArray name:PUSH_TAG_MERCHANT_MYPLACES];
    }
}

-(void)deleteMyPlacesTable
{
    sqlite3_stmt    *statement;
    
    DebugLog(@"path---->%@",[IN_APP_DELEGATE databasePath]);
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM MYPLACES"];
        DebugLog(@"delete---->%@",deleteSQL);
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(shoplocalDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"All rows deleted");
        } else {
            DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
}

-(void)getCountMyPlacesTable
{
    sqlite3_stmt    *statement;
    
    DebugLog(@"path---->%@",[IN_APP_DELEGATE databasePath]);
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        NSString *countSQL = [NSString stringWithFormat:@"SELECT count(*) FROM MYPLACES"];
        DebugLog(@"select---->%@",countSQL);
        const char *count_stmt = [countSQL UTF8String];
        if (sqlite3_prepare_v2(shoplocalDB, count_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                rowcount =  sqlite3_column_int(statement, 0);
                DebugLog(@"%d", rowcount);
            } else {
                DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
            }
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
}

//- (void)dealloc {
//    [self removeHUDView];
//}

//-(void)sendMerchantLoginRequest
//{
//    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:LOGIN_MERCHANT] cachePolicy:NO timeoutInterval:30.0];
//    NSMutableDictionary *postReq = [[NSMutableDictionary alloc] init];
//    [postReq setObject:MOBILE_NUMBER(usernameText.text) forKey:@"mobile"];
//    [postReq setObject:passwordtext.text forKey:@"password"];
//    NSError *error;
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postReq
//                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
//                                                         error:&error];
//    if (!jsonData) {
//        DebugLog(@"Got an error: %@", error);
//    }
//    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
//    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
//    [urlRequest setHTTPMethod:@"POST"];
//    [urlRequest setHTTPBody: jsonData];
//    connectionRegister = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
//}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    DebugLog(@"Error: %@", [error localizedDescription]);
    //[hud hide:YES];
    [CommonCallback hideProgressHud];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError* error;
    //  [hud hide:YES];
    [CommonCallback hideProgressHud];
    if (connection==connectionRegisterNumber)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            DebugLog(@"json: %@", json);
            if(json != nil) {
                if([[json  objectForKey:@"success"] isEqualToString:@"true"]) {
                    if([[json  objectForKey:@"code"] isEqualToString:@"-119"])
                    {
                        [INEventLogger logEvent:@"MSignUp_New"];
                    } else {
                        [INEventLogger logEvent:@"MSignUp_Return"];
                    }
                    [INUserDefaultOperations setMerchantTermsConditionsValue:@"0"];
                    [INUserDefaultOperations setMerchantCountryCode:[codeText.text stringByReplacingOccurrencesOfString:@"+" withString:@""]];
                    [INUserDefaultOperations setMerchantDetails:usernameText.text pass:nil];
                    [INUserDefaultOperations clearMerchantDetails];
                    [INUserDefaultOperations setMerchantLoginState:[json  objectForKey:@"message"] visitedPage:2];
                    if ([IN_APP_DELEGATE networkavailable]) {
                        [self activateUser:[json  objectForKey:@"message"]];
                    } else {
                        [INUserDefaultOperations showOfflineAlert];
                    }
                } else {
                    [INUserDefaultOperations showAlert:[json  objectForKey:@"message"]];
                }
            } else {
                [INUserDefaultOperations showAlert:@"No response from server. Please try again."];
                //[signupIndicatior stopAnimating];
                //[phoneNumberScreenforgotlbl setHidden:TRUE];
            }
        }
        responseAsyncData = nil;
    }
}
@end
