//
//  INCustomerSettingsViewController.m
//  shoplocal
//
//  Created by Kirti Nikam on 23/09/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INCustomerSettingsViewController.h"
#import "constants.h"
#import "ActionSheetPicker.h"
#import "MFSideMenu.h"
#import <FacebookSDK/FacebookSDK.h>
#import "INAboutUsViewController.h"
#import "SIAlertView.h"
#import "INSocialViewController.h"

@interface INCustomerSettingsViewController ()

@end

@implementation INCustomerSettingsViewController
@synthesize settingsTableView;
@synthesize selectedIndex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:TRUE];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:TRUE];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:FALSE];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:TRUE];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    [self setupMenuBarButtonItems];

    [self setUI];
    
    double custDistance = [INUserDefaultOperations getCustomerDistance];
    DebugLog(@"custDistance %f",custDistance);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setSettingsTableView:nil];
    [super viewDidUnload];
}

#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma set Navigation barbuttons callback
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:MENU_TITLE style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
//    return [[UIBarButtonItem alloc]
//            initWithTitle:MENU_TITLE style:UIBarButtonItemStyleBordered
//            target:self.navigationController.sideMenu
//            action:@selector(toggleRightSideMenu)];
    return nil;
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma internal methods
-(void)setUI{
    
    settingsTableView.backgroundColor = [UIColor clearColor];
    
}


#pragma UITableView Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([INUserDefaultOperations isCustomerLoggedIn])
    {
        if ([[INUserDefaultOperations getCustomerLoginFrom] isEqualToString:FACEBOOKSERVER])
        {
            return 6;
        }else{
            return 5;
        }
    }
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    if (section == 0) {
//        return 40;
//    } else {
//        return 10;
//    }
//}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(0,0,300,30)];
//    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
//    headerLabel.backgroundColor = [UIColor clearColor];
//    headerLabel.frame = CGRectMake(15, 5, 300, 30);
//    headerLabel.font = DEFAULT_BOLD_FONT(16.0f);
//    switch (section) {
//        case 0:
//            headerLabel.text = @"Contact Us";
//            break;
//    }
//    headerLabel.textColor = [UIColor whiteColor];
//    [customView addSubview:headerLabel];
//    return customView;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"settingsCell";
    UIView *topView;
    UILabel *lblTitle;
    UIImageView *thumbImg;
    UIImageView *checkMarkImgView;

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backgroundColor = [UIColor clearColor];
        cell.layer.shadowOffset = CGSizeMake(-15, 20);
        cell.layer.shadowColor = [UIColor darkGrayColor].CGColor;
        cell.layer.shadowRadius = 5;
        
        cell.layer.shadowPath = [UIBezierPath bezierPathWithRect:cell.bounds].CGPath;
        
        // Create the top view
        topView = [[UIView alloc] initWithFrame:CGRectMake(10, 5, 300, 75)];
        topView.tag = 111;
        topView.backgroundColor = [UIColor whiteColor];
        //topView = [CommonCallback setViewPropertiesWithRoundedCorner:topView];
        topView.clipsToBounds = YES;
        
        
        thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(5, 7, 60, 60)];
        thumbImg.tag = 1;
        thumbImg.contentMode = UIViewContentModeScaleAspectFit;
        //[cell.contentView addSubview:thumbImg];
        [topView addSubview:thumbImg];
        
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(80.0,25.0,210.0,25.0)];
        lblTitle.text = @"";
        lblTitle.tag = 2;
        lblTitle.font = DEFAULT_FONT(17);
        lblTitle.textAlignment = NSTextAlignmentLeft;
        lblTitle.textColor = DEFAULT_COLOR;
        lblTitle.backgroundColor =  [UIColor clearColor];
        lblTitle.minimumScaleFactor = 0.5f;
        lblTitle.adjustsFontSizeToFitWidth = YES;
        lblTitle.numberOfLines = 2;
        //[cell.contentView addSubview:lblTitle];
        [topView addSubview:lblTitle];
        
        //        UILabel *lblline = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 73.0, 300.0, 2.0)];
        //        lblline.text = @"";
        //        lblline.backgroundColor =  LIGHT_ORANGE_COLOR;
        //        [topView addSubview:lblline];
        
        // Add views to contentView
        [cell.contentView addSubview:topView];
        
        checkMarkImgView = [[UIImageView alloc] initWithFrame:CGRectMake(272,(CGRectGetHeight(topView.frame)/2)-7, 20, 20)];
        checkMarkImgView.tag = 102;
        checkMarkImgView.contentMode = UIViewContentModeScaleToFill;
        checkMarkImgView.layer.masksToBounds = YES;
        checkMarkImgView.image = [UIImage imageNamed:@"offer_uncheck.png"];
        [topView addSubview:checkMarkImgView];

        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = BROWN_OFFWHITE_COLOR;
        cell.selectedBackgroundView = bgColorView;
    }
    
    topView = (UIView *)[cell viewWithTag:111];
    lblTitle = (UILabel *)[topView viewWithTag:2];
    lblTitle.frame = CGRectMake(80.0,25.0,210.0,25.0);

    checkMarkImgView = (UIImageView *)[topView viewWithTag:102];
    [checkMarkImgView setHidden:YES];
    switch (indexPath.row) {
        case 0:
        {
//                cell.textLabel.text = SHOPLOCAL_CONTACT;
//                cell.detailTextLabel.text = @"";
//                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                
                [lblTitle setText:SHOPLOCAL_CONTACT];
                
                UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
                [thumbImgview setImage:[UIImage imageNamed:@"Call.png"]];
        }
            break;
            
        case 1:
        {
//                cell.textLabel.text = @"contact@shoplocal.co.in";
//                cell.detailTextLabel.text = @"";
//                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            [lblTitle setText:@"contact@shoplocal.co.in"];
            
            UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
            [thumbImgview setImage:[UIImage imageNamed:@"EMail.png"]];
        }
            break;
            
        case 2:
        {
//                cell.textLabel.text = @"Social Connect";
//                cell.textLabel.textColor = BROWN_COLOR;
//                cell.textLabel.font = DEFAULT_BOLD_FONT(15);
//                cell.detailTextLabel.textColor = BROWN_COLOR;
//                cell.detailTextLabel.font = DEFAULT_FONT(15);
//                cell.detailTextLabel.text = @"";
//                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                
                [lblTitle setText:@"Social Connect"];
                
                UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
                [thumbImgview setImage:[UIImage imageNamed:@"Social.png"]];
        }
            break;
            
        case 3:
        {
//                cell.textLabel.text = @"About Us";
//                cell.textLabel.textColor = BROWN_COLOR;
//                cell.textLabel.font = DEFAULT_BOLD_FONT(15);
//                cell.detailTextLabel.textColor = BROWN_COLOR;
//                cell.detailTextLabel.font = DEFAULT_FONT(15);
//                cell.detailTextLabel.text = @"";
//                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                
                [lblTitle setText:@"About Us"];
                
                UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
                [thumbImgview setImage:[UIImage imageNamed:@"AboutUs.png"]];
        }
            break;
        case 4:
        {
            if ([[INUserDefaultOperations getCustomerLoginFrom] isEqualToString:FACEBOOKSERVER])
            {
                    lblTitle.frame = CGRectMake(80.0,0,190,75);
                    [lblTitle setText:@"Share on Facebook timeline"];
                    
                    UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
                    [thumbImgview setImage:[UIImage imageNamed:@"Facebook_circle.png"]];
                
                    [checkMarkImgView setHidden:NO];
                    if([INUserDefaultOperations isOpenGraphShareEnabled]) {
                        [checkMarkImgView setImage:[UIImage imageNamed:@"offer_check.png"]];
                    }
                    else {
                        [checkMarkImgView setImage:[UIImage imageNamed:@"offer_uncheck.png"]];
                    }
            }else{
                if ([[INUserDefaultOperations getCustomerLoginFrom] isEqualToString:FACEBOOKSERVER]) {
                    DebugLog(@"[INUserDefaultOperations getCustomerId] %@",[INUserDefaultOperations getCustomerId]);
                    //cell.textLabel.text = @"Logout from facebook account";
                    UILabel *lblTitle = (UILabel *)[topView viewWithTag:2];
                    [lblTitle setText:@"Logout from Facebook"];
                } else {
                    DebugLog(@"[INUserDefaultOperations getCustomerCountryCode] %@ [INUserDefaultOperations getCustomerId] %@",[INUserDefaultOperations getCustomerCountryCode],[INUserDefaultOperations getCustomerId]);
                    //cell.textLabel.text = [NSString stringWithFormat:@"Unlink Account                %@%@", [INUserDefaultOperations getCustomerCountryCode],[INUserDefaultOperations getCustomerId]];
                    UILabel *lblTitle = (UILabel *)[topView viewWithTag:2];
                    [lblTitle setText:[NSString stringWithFormat:@"Logout (%@%@)", [INUserDefaultOperations getCustomerCountryCode],[INUserDefaultOperations getCustomerId]]];
                }
                UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
                [thumbImgview setImage:[UIImage imageNamed:@"Logout.png"]];
            }
        }
            break;
        case 5:
        {
//            cell.textLabel.font = DEFAULT_FONT(15);
//            cell.textLabel.textColor = BROWN_COLOR;
//            cell.detailTextLabel.text = @"";

            if ([[INUserDefaultOperations getCustomerLoginFrom] isEqualToString:FACEBOOKSERVER]) {
                DebugLog(@"[INUserDefaultOperations getCustomerId] %@",[INUserDefaultOperations getCustomerId]);
                //cell.textLabel.text = @"Logout from facebook account";
                UILabel *lblTitle = (UILabel *)[topView viewWithTag:2];
                [lblTitle setText:@"Logout from Facebook"];
            } else {
                DebugLog(@"[INUserDefaultOperations getCustomerCountryCode] %@ [INUserDefaultOperations getCustomerId] %@",[INUserDefaultOperations getCustomerCountryCode],[INUserDefaultOperations getCustomerId]);
                //cell.textLabel.text = [NSString stringWithFormat:@"Unlink Account                %@%@", [INUserDefaultOperations getCustomerCountryCode],[INUserDefaultOperations getCustomerId]];
                UILabel *lblTitle = (UILabel *)[topView viewWithTag:2];
                [lblTitle setText:[NSString stringWithFormat:@"Logout (%@%@)", [INUserDefaultOperations getCustomerCountryCode],[INUserDefaultOperations getCustomerId]]];
            }
            UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
            [thumbImgview setImage:[UIImage imageNamed:@"Logout.png"]];
        }
        break;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
     switch (indexPath.row) {
         case 0:
         {
             //SHOPLOCAL_CONTACT;
             NSString *deviceType = [UIDevice currentDevice].model;
             if([deviceType isEqualToString:@"iPhone"])
             {
                 NSDictionary *callParams = [NSDictionary dictionaryWithObjectsAndKeys:@"Number",@"Source", nil];
                 [INEventLogger logEvent:@"Tab_ContactUs" withParams:callParams];
                 UIAlertView *alert = [[ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                                   message: [NSString stringWithFormat: @"Do you want to call %@ number ?",SHOPLOCAL_CONTACT]
                                                                  delegate: self
                                                         cancelButtonTitle: nil
                                                         otherButtonTitles: @"NO", @"YES", nil
                                       ];
                 [alert show];
             }else{
                 UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                                    message: @"Calling functionality is not available in this device. "
                                                                   delegate: nil
                                                          cancelButtonTitle: nil
                                                          otherButtonTitles: @"OK", nil
                                       ];
                 [ alert show ];
             }
         }
             break;
             
         case 1:
         {
             //@"contact@shoplocal.co.in";
             if ([MFMailComposeViewController canSendMail]) {
                 NSDictionary *emailParams = [NSDictionary dictionaryWithObjectsAndKeys:@"Email",@"Source", nil];
                 [INEventLogger logEvent:@"Tab_ContactUs" withParams:emailParams];
                 MFMailComposeViewController *emailComposer = [[MFMailComposeViewController alloc] init];
                 if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
                     emailComposer.navigationBar.tintColor  = [UIColor whiteColor];
                 }
                 emailComposer.mailComposeDelegate = self;
                 [emailComposer setToRecipients:[NSArray arrayWithObjects:@"contact@shoplocal.co.in", nil]];
                 [emailComposer setSubject:@""];
                 [emailComposer setMessageBody:@"" isHTML:NO];
                 [self presentViewController:emailComposer animated:YES completion:nil];
             }else
             {
                 UIAlertView *alertView = [[UIAlertView alloc]
                                           initWithTitle:ALERT_TITLE
                                           message:@"You may not have set up mail service on your device or You may not connected to internet.\nPlease check and try again."
                                           delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
                 [alertView show];
             }
         }
             break;
             
         case 2:
         {
             [INEventLogger logEvent:@"Tab_SocialConnect"];
                 INSocialViewController *socialViewController = [[INSocialViewController alloc] initWithNibName:@"INSocialViewController" bundle:nil];
                 socialViewController.title = @"Shoplocal";
                 [self.navigationController pushViewController:socialViewController animated:YES];
         }
             break;
             
         case 3:
         {
              [INEventLogger logEvent:@"Tab_AboutShoplocal"];
                 INAboutUsViewController *aboutusViewController = [[INAboutUsViewController alloc] initWithNibName:@"INAboutUsViewController" bundle:nil];
                 aboutusViewController.title = @"About Shoplocal";
                 [self.navigationController pushViewController:aboutusViewController animated:YES];
         }
             break;
         case 4:
         {
                 if ([[INUserDefaultOperations getCustomerLoginFrom] isEqualToString:FACEBOOKSERVER])
                 {
                     BOOL value = [INUserDefaultOperations isOpenGraphShareEnabled];
                     [INUserDefaultOperations setIsOpenGraphShareEnabled:!value];
                     UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
                     UIImageView *checkMarkImgView = (UIImageView *)[cell viewWithTag:102];
                     if([INUserDefaultOperations isOpenGraphShareEnabled]) {
                         [checkMarkImgView setImage:[UIImage imageNamed:@"offer_check.png"]];
                     }
                     else {
                         [checkMarkImgView setImage:[UIImage imageNamed:@"offer_uncheck.png"]];
                     }
                 }else{
                     [self showLogoutAlert];
                 }
            }
             break;
         case 5:
         {
//                 UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
//                                                                    message: @"Are you sure you want to log out?"
//                                                                   delegate: self
//                                                          cancelButtonTitle: nil
//                                                          otherButtonTitles: @"NO", @"YES", nil
//                                       ];
//                 [alert show];
             [self showLogoutAlert];
         }
         break;
     }
    [self.settingsTableView deselectRowAtIndexPath:indexPath animated:YES];
}
-(void)showLogoutAlert
{
    SIAlertView *sialertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Are you sure you want to log out?"];
    sialertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
    sialertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
    [sialertView addButtonWithTitle:@"YES"
                               type:SIAlertViewButtonTypeDestructive
                            handler:^(SIAlertView *alert) {
                                DebugLog(@"YES");
                                if ([[INUserDefaultOperations getCustomerLoginFrom] isEqualToString:FACEBOOKSERVER]) {
                                    [FBSession.activeSession closeAndClearTokenInformation];
                                    [INUserDefaultOperations setIsOpenGraphShareEnabled:NO];
                                }
                                [INUserDefaultOperations clearCustomerDetails];
                                NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:IN_CUSTOMER_LOGIN_LOGOUT_KEY];
                                [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_LOGIN_LOGOUT_NOTIFICATION object:nil userInfo:dictionary];
                                [self.settingsTableView reloadData];
                            }];
    [sialertView addButtonWithTitle:@"NO"
                               type:SIAlertViewButtonTypeCancel
                            handler:^(SIAlertView *alert) {
                                DebugLog(@"NO");
                            }];
    [sialertView show];
}

#pragma UIAlertView Delegates

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"YES"])
    {
        UIApplication *myApp = [UIApplication sharedApplication];
        [myApp openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",SHOPLOCAL_CONTACT]]];
    }
}

#pragma mark Mail Compose Delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    if (result == MFMailComposeResultFailed) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                        message:NSLocalizedString(@"Email failed to send. Please try again.", nil)
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", nil) otherButtonTitles:nil];
		[alert show];
    }else if (result == MFMailComposeResultSent)
    {
        DebugLog(@"MFMailComposeResultSent");
    }
	[self dismissViewControllerAnimated:YES completion:nil];
}
@end
