//
//  INFeedObj.m
//  shoplocal
//
//  Created by Rishi on 24/10/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INFeedObj.h"

@implementation INFeedObj
@synthesize postID;
@synthesize placeID;
@synthesize areaID;
@synthesize name;
@synthesize title;
@synthesize description;
@synthesize date;
@synthesize offerdate;
@synthesize distance;
@synthesize latitude;
@synthesize longitude;
@synthesize tel_no1;
@synthesize tel_no2;
@synthesize tel_no3;
@synthesize mob_no1;
@synthesize mob_no2;
@synthesize mob_no3;
@synthesize image_url1;
@synthesize image_url2;
@synthesize image_url3;
@synthesize thumb_url1;
@synthesize thumb_url2;
@synthesize thumb_url3;
@synthesize total_like;
@synthesize total_share;
@synthesize is_offered;
@synthesize type;
@synthesize user_like;
@end
