//
//  INContactUsViewController.h
//  shoplocal
//
//  Created by Kirti Nikam on 11/11/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface INContactUsViewController : UIViewController <MFMailComposeViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UIView *backView;
@property (strong, nonatomic) IBOutlet UILabel *lblMessage;

@property (strong, nonatomic) IBOutlet UIButton *sendMailBtn;
- (IBAction)sendMailBtnPressed:(id)sender;
@end
