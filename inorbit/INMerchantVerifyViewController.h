//
//  INMerchantVerifyViewController.h
//  shoplocal
//
//  Created by Kirti Nikam on 23/12/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol INMerchantVerifyViewControllerDelegate <NSObject>
- (void)loginUserWithActivationCode:(NSString *)lnumber pass:(NSString *)lpass showAddStore:(int)lval;
@end

@interface INMerchantVerifyViewController : UIViewController{
    NSURLConnection *connectionRegisterNumber;
    NSURLConnection *connectionsetActivationCode;
    NSMutableData *responseAsyncData;
    int status;
}
@property (nonatomic, weak) id<INMerchantVerifyViewControllerDelegate> logindelegate;
@property (nonatomic, copy) NSString *codeNumber;
@property (nonatomic, copy) NSString *mobileNumber;
@property (nonatomic, copy) NSString *message;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *backView;
@property (strong, nonatomic) IBOutlet UILabel *lblMessage;
@property (strong, nonatomic) IBOutlet UITextField *txtActivationCode;
@property (strong, nonatomic) IBOutlet UIImageView *imgSeparator;
@property (strong, nonatomic) IBOutlet UIButton *btnNext;
@property (strong, nonatomic) IBOutlet UILabel *lblQuest;
@property (strong, nonatomic) IBOutlet UIButton *btnResend;
@property (strong, nonatomic) IBOutlet UILabel *lblUnderLine;
@property (strong, nonatomic) IBOutlet UIButton *btnEdit;

- (IBAction)btnNextPressed:(id)sender;
- (IBAction)btnResendPressed:(id)sender;
- (IBAction)btnEditPressed:(id)sender;
@end
