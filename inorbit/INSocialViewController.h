//
//  INSocialViewController.h
//  inorbit
//
//  Created by Rishi on 24/12/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface INSocialViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITableView *socialTableView;
@end
