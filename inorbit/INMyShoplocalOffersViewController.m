//
//  INMyShoplocalOffersViewController.m
//  shoplocal
//
//  Created by Rishi on 07/03/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <time.h>
#import <xlocale.h>
#import <Social/Social.h>

#import "INMyShoplocalOffersViewController.h"
#import "INAppDelegate.h"
#import "CommonCallback.h"
#import "constants.h"
#import "INFeedObj.h"
#import "INCustBroadCastDetailsViewController.h"
#import "INSearchViewController.h"
#import "UIImageView+WebCache.h"
#import "CellWithImageView.h"
#import "CellWithoutImageView.h"

#import "SideMenuViewController.h"
#import "INCustomerProfileViewController.h"
#import "INFavouritesViewController.h"

#import "FacebookOpenGraphAPI.h"
#import "FacebookShareAPI.h"

#define FAV_STORE_LINK(PAGE) [NSString stringWithFormat:@"%@%@%@user_api/favourite_places?page=%@&count=50",LIVE_SERVER,URL_PREFIX,API_VERSION,PAGE]

#define MYOFFERS_LINK(PAGE) [NSString stringWithFormat:@"%@%@%@user_api/favourite_posts?page=%@&count=50",LIVE_SERVER,URL_PREFIX,API_VERSION,PAGE]
#define THUMBNAIL_LINK(IMG_LINK) [NSString stringWithFormat:@"%@%@%@",LIVE_SERVER,URL_PREFIX,IMG_LINK]

#define CALL_STORE [NSString stringWithFormat:@"%@%@broadcast_api/call",URL_PREFIX,API_VERSION]

@interface INMyShoplocalOffersViewController ()
@property(strong) NSDictionary *splashJson;
@end

@implementation INMyShoplocalOffersViewController
@synthesize countHeaderView;
@synthesize lblpullToRefresh;
@synthesize countlbl;
@synthesize myoffersTableView;
@synthesize myofferArray;
@synthesize totalPage;
@synthesize totalRecord;
@synthesize currentPage;
@synthesize textPull, textRelease, textLoading, refreshHeaderView, refreshLabel, refreshArrow, refreshSpinner;
@synthesize loginView,loginBtn,lblLoginText;
@synthesize globalSearchBar;
@synthesize currentSelectedRow;
@synthesize showOffersBtn,showStoreViewBtn;
@synthesize favStoreBtn;
@synthesize callModalViewBackView,callModalView,lblcallModalHeader,lblcallModalHeaderSeperator,callModalTableView,btnCancelcallModal;
@synthesize telArray,selectedPostId;
@synthesize selectedPlaceName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self setupStrings];
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [storeButton setHidden:TRUE];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [storeButton setHidden:TRUE];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                             style:UIBarButtonItemStylePlain
                                                            target:nil
                                                            action:nil];
    [[self navigationItem] setBackBarButtonItem:back];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentMyProfileView) name:IN_CUSTOMER_SHOW_PROFILE_LOCAL_NOTIFICATION object:nil];

    
    [self setupMenuBarButtonItems];
    [self setUI];
    myofferArray = [[NSMutableArray alloc] init];
    [self addPullToRefreshHeader];
    
//    myoffersTableView.tableHeaderView = favStoreBtn;

    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        globalSearchBar.barTintColor = DEFAULT_COLOR;
        [globalSearchBar setTranslucent:NO];
    }
    globalSearchBar.showsCancelButton = YES;
    globalSearchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    globalSearchBar.delegate =  self;
    globalSearchBar.hidden = TRUE;
    [globalSearchBar setFrame:CGRectMake(0, -globalSearchBar.frame.size.height, globalSearchBar.frame.size.width, globalSearchBar.frame.size.height)];
//    [countHeaderView setFrame:CGRectMake(0, 0, countHeaderView.frame.size.width, countHeaderView.frame.size.height)];
//    [myoffersTableView setFrame:CGRectMake(0, CGRectGetMaxY(countHeaderView.frame)+1, myoffersTableView.frame.size.width, self.view.frame.size.height-countHeaderView.frame.size.height)];
    
//    [myoffersTableView setFrame:CGRectMake(0,0, myoffersTableView.frame.size.width, self.view.frame.size.height-favStoreBtn.frame.size.height)];
    
    self.myoffersTableView.backgroundColor = [UIColor clearColor];//[UIColor colorWithWhite:0.9 alpha:1.0];
    self.myoffersTableView.separatorColor = [UIColor clearColor];
    [self.myoffersTableView registerNib:[UINib nibWithNibName:@"CellWithImageView" bundle:nil] forCellReuseIdentifier:kWithImageCellIdentifier];
    [self.myoffersTableView registerNib:[UINib nibWithNibName:@"CellWithoutImageView" bundle:nil] forCellReuseIdentifier:kWithoutImageCellIdentifier];
    
    
    [loginView setHidden:TRUE];
    showStoreViewBtn.hidden = TRUE;
    showOffersBtn.hidden    = TRUE;
    [favStoreBtn setHidden:TRUE];
    [myoffersTableView setHidden:TRUE];
    [countHeaderView setHidden:TRUE];
    if(![INUserDefaultOperations isCustomerLoggedIn])
    {
        [loginView setHidden:FALSE];
    } else {
        [self loadData];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setCountHeaderView:nil];
    [self setLblpullToRefresh:nil];
    [self setCountlbl:nil];
    [self setMyoffersTableView:nil];
    [self setLoginView:nil];
    [self setLoginBtn:nil];
    [self setLblLoginText:nil];
    [self setGlobalSearchBar:nil];
    [self setShowStoreViewBtn:nil];
    [self setShowOffersBtn:nil];
    [super viewDidUnload];
}
#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma set Navigation barbuttons callback
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:MENU_TITLE style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    if ([INUserDefaultOperations isCustomerLoggedIn]) {
        return [[UIBarButtonItem alloc] initWithTitle:@"MyProfile" style:UIBarButtonItemStyleBordered
                                               target:self
                                               action:@selector(presentMyProfileView)];

    }else{
        return nil;
    }
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)presentMyProfileView {
    [INEventLogger logEvent:@"Tab_CustomerProfile"];
    INCustomerProfileViewController *customerProfileViewController = [[INCustomerProfileViewController alloc] initWithNibName:@"INCustomerProfileViewController" bundle:nil] ;
    customerProfileViewController.title = @"My Profile";
    [self.navigationController pushViewController:customerProfileViewController animated:YES];
}

- (void)searchBarBtnClicked:(id)sender {
    globalSearchBar.text = @"";
    [globalSearchBar becomeFirstResponder];
    self.navigationItem.rightBarButtonItem = nil;
    //    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(searchBarCancelBtnClicked)];
    
    globalSearchBar.hidden = FALSE;
    [UIView animateWithDuration:0.3 animations:^{
        [globalSearchBar setFrame:CGRectMake(0, 0, globalSearchBar.frame.size.width, globalSearchBar.frame.size.height)];
        [countHeaderView setFrame:CGRectMake(0, CGRectGetMaxY(globalSearchBar.frame), countHeaderView.frame.size.width, countHeaderView.frame.size.height)];
        [myoffersTableView setFrame:CGRectMake(0, CGRectGetMaxY(countHeaderView.frame)+1, myoffersTableView.frame.size.width, self.view.frame.size.height-(countHeaderView.frame.size.height+globalSearchBar.frame.size.height))];
    }];
}

-(void)searchBarCancelBtnClicked {
    globalSearchBar.text = @"";
    [globalSearchBar resignFirstResponder];
    self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
    [UIView animateWithDuration:0.3 animations:^{
        [globalSearchBar setFrame:CGRectMake(0, -globalSearchBar.frame.size.height, globalSearchBar.frame.size.width, globalSearchBar.frame.size.height)];
        [countHeaderView setFrame:CGRectMake(0, 0, countHeaderView.frame.size.width, countHeaderView.frame.size.height)];
        [myoffersTableView setFrame:CGRectMake(0, CGRectGetMaxY(countHeaderView.frame)+1, myoffersTableView.frame.size.width, self.view.frame.size.height-countHeaderView.frame.size.height)];
    } completion:^(BOOL finished){
        globalSearchBar.hidden = TRUE;
    }];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    INSearchViewController *searchController = [[INSearchViewController alloc] initWithNibName:@"INSearchViewController" bundle:nil] ;
    searchController.title = @"Search";
    searchController.textToSearch = searchBar.text;
    searchController.isStoreSearch = NO;
    [self.navigationController pushViewController:searchController animated:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar{
    [self searchBarCancelBtnClicked];
}


-(void)setUI
{
    //loginView = [CommonCallback setViewPropertiesWithRoundedCorner:loginView];
    loginView.backgroundColor       =   [UIColor whiteColor];
    loginView.clipsToBounds   = YES;

    countHeaderView.backgroundColor =   [UIColor clearColor];
    countHeaderView.clipsToBounds   = YES;
    
    lblpullToRefresh.font               =   DEFAULT_FONT(15.0);
    lblpullToRefresh.backgroundColor    =   LIGHT_GREEN_COLOR_WITH_ALPHA(0.5);
    lblpullToRefresh.textColor          =   [UIColor whiteColor];
    
    countlbl.font               =   DEFAULT_FONT(15.0);
    countlbl.backgroundColor    =   LIGHT_GREEN_COLOR_WITH_ALPHA(0.5);
    countlbl.textColor          =   [UIColor whiteColor];
    
    lblLoginText.textAlignment    =     NSTextAlignmentCenter;
    lblLoginText.backgroundColor  =     [UIColor clearColor];
    lblLoginText.font             =     DEFAULT_FONT(18);
    lblLoginText.textColor        =     DEFAULT_COLOR;
    lblLoginText.numberOfLines    =     15;
    lblLoginText.text             =     @"You can add stores to Myshoplocal. It brings the latest offers from your favourite stores in the neighbourhood at your fingertips";
    
    loginBtn.titleLabel.font            =   DEFAULT_BOLD_FONT(18);
    loginBtn.titleLabel.textAlignment   =   NSTextAlignmentCenter;
    loginBtn.backgroundColor            =   [UIColor clearColor];
    [loginBtn setTitle:@"Login" forState:UIControlStateNormal];
    [loginBtn setTitle:@"Login" forState:UIControlStateHighlighted];
    [loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    showStoreViewBtn.titleLabel.font            =   DEFAULT_BOLD_FONT(18);
    showStoreViewBtn.titleLabel.textAlignment   =   NSTextAlignmentCenter;
    showStoreViewBtn.titleLabel.numberOfLines   =   3;
    showStoreViewBtn.backgroundColor            =   BUTTON_COLOR;
    [showStoreViewBtn setTitle:@"Start adding business to your Shoplocal Bag" forState:UIControlStateNormal];
    [showStoreViewBtn setTitle:@"Start adding business to your Shoplocal Bag" forState:UIControlStateHighlighted];
    [showStoreViewBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    showOffersBtn.titleLabel.font           =   DEFAULT_BOLD_FONT(18);
    showOffersBtn.titleLabel.textAlignment  =   NSTextAlignmentCenter;
    showOffersBtn.titleLabel.numberOfLines  =   3;
    showOffersBtn.backgroundColor           =   BUTTON_COLOR;
    [showOffersBtn setTitle:@"See offers" forState:UIControlStateNormal];
    [showOffersBtn setTitle:@"See offers" forState:UIControlStateHighlighted];
    [showOffersBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    favStoreBtn.titleLabel.font           =   DEFAULT_BOLD_FONT(20.0);
    favStoreBtn.titleLabel.textAlignment  =   NSTextAlignmentCenter;
    favStoreBtn.backgroundColor           =   BUTTON_COLOR;
    [favStoreBtn setTitle:@"My Favourite Business" forState:UIControlStateNormal];
    [favStoreBtn setTitle:@"My Favourite Business" forState:UIControlStateHighlighted];
    [favStoreBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    //    loginBtn.titleLabel.font     =   DEFAULT_BOLD_FONT(16.0);
    //    loginBtn.backgroundColor     =   [UIColor whiteColor];
    //    loginBtn.layer.borderColor   = BUTTON_BORDER_COLOR.CGColor;
    //    loginBtn.layer.borderWidth   = 2.0;
    //    loginBtn.layer.cornerRadius  = 5.0;
    //    [loginBtn setTitle:@"Login" forState:UIControlStateNormal];
    //    [loginBtn setTitle:@"Login" forState:UIControlStateHighlighted];
    //    [loginBtn setTitleColor:BUTTON_COLOR forState:UIControlStateNormal];
    //    [loginBtn setTitleColor:BUTTON_OFFWHITE_COLOR forState:UIControlStateHighlighted];
    
    //callModalView = [CommonCallback setViewPropertiesWithRoundedCorner:callModalView];
    callModalView.backgroundColor =  [UIColor whiteColor];
    callModalView.layer.shadowColor      = [UIColor blackColor].CGColor;
    callModalView.layer.shadowOffset     = CGSizeMake(1, 1);
    callModalView.layer.shadowOpacity    = 1.0;
    callModalView.layer.shadowRadius     = 10.0;
    [callModalViewBackView setHidden:YES];
    [callModalView setHidden:YES];
    
    lblcallModalHeader.textAlignment     = NSTextAlignmentCenter;
    lblcallModalHeader.backgroundColor   = [UIColor clearColor];
    lblcallModalHeader.font              = DEFAULT_BOLD_FONT(20);
    lblcallModalHeader.textColor         = BROWN_COLOR;
    //    lblcallModalHeader.adjustsFontSizeToFitWidth = YES;
    lblcallModalHeader.minimumScaleFactor = 15;
    lblcallModalHeader.text              = @"Select number";
    lblcallModalHeader.numberOfLines     = 2;
    
    lblcallModalHeaderSeperator.backgroundColor   = DEFAULT_COLOR;
    callModalTableView.backgroundColor = [UIColor whiteColor];
    if ([callModalTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [callModalTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    btnCancelcallModal.backgroundColor = [UIColor clearColor];
    
}

-(void)showLoginModal{
    INCustomerLoginViewController *custLoginController = [[INCustomerLoginViewController alloc] initWithNibName:@"INCustomerLoginViewController" bundle:nil] ;
    custLoginController.delegate = self;
    UINavigationController *loginnavBar=[[UINavigationController alloc]initWithRootViewController:custLoginController];
    UIButton *moreButton1 =  [UIButton buttonWithType:UIButtonTypeCustom];
    [moreButton1 setTitle:@"Cancel" forState:UIControlStateNormal];
    moreButton1.titleLabel.font = [UIFont systemFontOfSize:17.0];
    //[moreButton1 setBackgroundImage:[UIImage imageNamed:@"Transparent_Button.png"] forState:UIControlStateNormal];
    [moreButton1 addTarget:self action:@selector(dismissLoginView) forControlEvents:UIControlEventTouchUpInside];
    [moreButton1 setFrame:CGRectMake(250, 7, 70, 32)];
    [loginnavBar.navigationBar addSubview:moreButton1];
    [loginnavBar.navigationBar setTranslucent:NO];
    [self.navigationController presentViewController:loginnavBar animated:YES completion:nil];
}

-(void)dismissLoginView
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"Dismiss");
        if([INUserDefaultOperations isCustomerLoggedIn])
        {
            [loginView setHidden:TRUE];
            [self loadData];
            [self setupMenuBarButtonItems];
            [INEventLogger logEvent:@"MyShoplocal_LoginSuccess"];
        }
    }];
}

- (IBAction)favStoreBtnPressed:(id)sender {
    [INEventLogger logEvent:@"Tab_FavouriteBusiness"];
    INFavouritesViewController *favBusinessViewController = [[INFavouritesViewController alloc] initWithNibName:@"INFavouritesViewController" bundle:nil] ;
    favBusinessViewController.title = @"My Favourite Business";
    [self.navigationController pushViewController:favBusinessViewController animated:YES];
}

- (IBAction)loginBtnPressed:(id)sender {
    [INEventLogger logEvent:@"MyShoplocal_Login"];
    [self showLoginModal];
}

- (IBAction)showStoreViewBtnPressed:(id)sender {
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:ALL_BUSINESS forKey:IN_CUSTOMER_CHANGE_TAB_MANUALLY_KEY];
    [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_CHANGE_TAB_MANUALLY_NOTIFICATION object:nil userInfo:dictionary];
}

- (IBAction)showOffersBtnPressed:(id)sender {
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:DISCOVER forKey:IN_CUSTOMER_CHANGE_TAB_MANUALLY_KEY];
    [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_CHANGE_TAB_MANUALLY_NOTIFICATION object:nil userInfo:dictionary];
}


-(void)sendFavouritesRequest:(NSString *)urlString
{
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setValue:[INUserDefaultOperations getCustomerAuthId] forHTTPHeaderField:@"user_id"];
    [urlRequest setValue:[INUserDefaultOperations getCustomerAuthCode] forHTTPHeaderField:@"auth_id"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        if(self.splashJson  != nil) {
                                                            DebugLog(@"--->%@",[self.splashJson  objectForKey:@"success"]);
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSDictionary *maindict = [self.splashJson  objectForKey:@"data"];
                                                                if ([[maindict  objectForKey:@"total_record"] integerValue] > 0) {
                                                                    [favStoreBtn setHidden:FALSE];
                                                                }

                                                            } else {
                                                                if([self.splashJson  objectForKey:@"code"] != [NSNull null] && [[self.splashJson  objectForKey:@"code"] isEqualToString:CUSTOMER_INVALID_USER_AUTH_CODE]) {
                                                                    DebugLog(@"Got Invalid Authentication Code.");
                                                                    [favStoreBtn setHidden:TRUE];
                                                                } else if([self.splashJson  objectForKey:@"code"] != [NSNull null] && ([[self.splashJson  objectForKey:@"code"] isEqualToString:CUSTOMER_NO_RECORDS_FOUND] || [[self.splashJson  objectForKey:@"code"] isEqualToString:CUSTOMER_NO_FAVSTORE_FOUND])) {
                                                                    [favStoreBtn setHidden:TRUE];
                                                                    DebugLog(@"No Favourite records.....");
                                                                }
                                                            }
                                                        }
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                    }];
    [operation start];
}


-(void)sendMyShoplocalOfferRequest:(NSString *)urlString
{
    DebugLog(@"url - %@",urlString);
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setValue:[INUserDefaultOperations getCustomerAuthId] forHTTPHeaderField:@"user_id"];
    [urlRequest setValue:[INUserDefaultOperations getCustomerAuthCode] forHTTPHeaderField:@"auth_id"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"--->%@",self.splashJson);
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        
                                                        if(self.splashJson  != nil) {
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSDictionary *maindict = [self.splashJson  objectForKey:@"data"];
                                                                self.totalPage = [[maindict  objectForKey:@"total_page"] integerValue];
                                                                self.totalRecord = [[maindict  objectForKey:@"total_record"] integerValue];
                                                                if(self.currentPage == 1)
                                                                {
                                                                    [myofferArray removeAllObjects];
                                                                }
                                                                NSArray* jsonArray = [maindict  objectForKey:@"record"];
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *objdict = [jsonArray  objectAtIndex:index];
                                                                    DebugLog(@"\n\n%@\n\n",objdict);
                                                                    feedObj = [[INFeedObj alloc] init];
                                                                    feedObj.postID = [[objdict objectForKey:@"post_id"] integerValue];
                                                                    feedObj.placeID = [[objdict objectForKey:@"id"] integerValue];
                                                                    feedObj.areaID = [objdict objectForKey:@"area_id"];
                                                                    feedObj.name = [[objdict objectForKey:@"name"] stringByReplacingOccurrencesOfString:@"\\" withString:@""];
                                                                    feedObj.title = [[objdict objectForKey:@"title"] stringByReplacingOccurrencesOfString:@"\\" withString:@""];
                                                                    feedObj.description = [[objdict objectForKey:@"description"] stringByReplacingOccurrencesOfString:@"\\" withString:@""];
                                                                    feedObj.date = [objdict objectForKey:@"date"];
                                                                    feedObj.offerdate = [objdict objectForKey:@"offer_date_time"];
                                                                    feedObj.distance = [objdict objectForKey:@"distance"];
                                                                    feedObj.latitude = [objdict objectForKey:@"latitude"];
                                                                    feedObj.longitude = [objdict objectForKey:@"longitude"];
                                                                    feedObj.tel_no1 = [objdict objectForKey:@"tel_no1"];
                                                                    feedObj.tel_no2 = [objdict objectForKey:@"tel_no2"];
                                                                    feedObj.tel_no3 = [objdict objectForKey:@"tel_no3"];
                                                                    feedObj.mob_no1 = [objdict objectForKey:@"mob_no1"];
                                                                    feedObj.mob_no2 = [objdict objectForKey:@"mob_no2"];
                                                                    feedObj.mob_no3 = [objdict objectForKey:@"mob_no3"];
                                                                    feedObj.image_url1 = [objdict objectForKey:@"image_url1"];
                                                                    feedObj.image_url2 = [objdict objectForKey:@"image_url2"];
                                                                    feedObj.image_url3 = [objdict objectForKey:@"image_url3"];
                                                                    feedObj.thumb_url1 = [objdict objectForKey:@"thumb_url1"];
                                                                    feedObj.thumb_url2 = [objdict objectForKey:@"thumb_url2"];
                                                                    feedObj.thumb_url3 = [objdict objectForKey:@"thumb_url3"];
                                                                    feedObj.total_like = [objdict objectForKey:@"total_like"];
                                                                    feedObj.total_share = [objdict objectForKey:@"total_share"];
                                                                    feedObj.is_offered = [objdict objectForKey:@"is_offered"];
                                                                    feedObj.type = [objdict objectForKey:@"type"];
                                                                    [myofferArray addObject:feedObj];
                                                                    feedObj = nil;
                                                                }
                                                                [loginView setHidden:TRUE];
                                                                showStoreViewBtn.hidden = TRUE;
                                                                showOffersBtn.hidden    = TRUE;
                                                                [favStoreBtn setHidden:FALSE];
                                                                [myoffersTableView setHidden:FALSE];
                                                                [countHeaderView setHidden:FALSE];
                                                                [self sendFavouritesRequest:FAV_STORE_LINK(@"1")];
                                                            } else {
                                                                if([self.splashJson  objectForKey:@"code"] != [NSNull null] && [[self.splashJson  objectForKey:@"code"] isEqualToString:CUSTOMER_INVALID_USER_AUTH_CODE]) {
                                                                    DebugLog(@"Got Invalid Authentication Code.");
                                                                    [INUserDefaultOperations showSIAlertView:CUSTOMER_INVALID_USER_AUTH_MESSAGE];
                                                                    [loginView setHidden:FALSE];
                                                                    showStoreViewBtn.hidden = TRUE;
                                                                    showOffersBtn.hidden    = TRUE;
                                                                    [favStoreBtn setHidden:TRUE];
                                                                    [myoffersTableView setHidden:TRUE];
                                                                    [countHeaderView setHidden:TRUE];
                                                                    [INUserDefaultOperations clearCustomerDetails];
                                                                    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:IN_CUSTOMER_LOGIN_LOGOUT_KEY];
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_LOGIN_LOGOUT_NOTIFICATION object:nil userInfo:dictionary];
                                                                    [self setupMenuBarButtonItems];
                                                                    
                                                                } else if([self.splashJson  objectForKey:@"code"] != [NSNull null] && ([[self.splashJson  objectForKey:@"code"] isEqualToString:CUSTOMER_NO_RECORDS_FOUND] || [[self.splashJson  objectForKey:@"code"] isEqualToString:CUSTOMER_NO_FAVSTORE_FOUND_FORPOST])) {
                                                                    if(self.currentPage == 1)
                                                                    {
                                                                        [loginView setHidden:TRUE];
                                                                        showStoreViewBtn.hidden = FALSE;
                                                                        showOffersBtn.hidden    = FALSE;
                                                                        //[favStoreBtn setHidden:TRUE];
                                                                        [myoffersTableView setHidden:TRUE];
                                                                        [countHeaderView setHidden:TRUE];
                                                                    }
                                                                    [self sendFavouritesRequest:FAV_STORE_LINK(@"1")];
                                                                }
                                                                else if([self.splashJson  objectForKey:@"message"] != [NSNull null]){
                                                                    [INUserDefaultOperations performSelector:@selector(showAlert:) withObject:[self.splashJson  objectForKey:@"message"] afterDelay:2.0];
                                                                }
                                                            }
                                                            //                                                            if([myofferArray count] > 0)
                                                            //                                                            {
                                                            [myoffersTableView reloadData];
                                                            [self setCountLabel];
                                                            //                                                            }
                                                        }
                                                        if(isLoading)
                                                        {
                                                            [self stopLoading];
                                                        }
                                                        isAdding = NO;
                                                        
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        //[hud hide:YES];
                                                        DebugLog(@"-%@-%@-%@-%d-",response,error,JSON,response.statusCode);
                                                        DebugLog(@"-%d-%d-",[error code],NSURLErrorCancelled);
                                                        if ([error code] == NSURLErrorCancelled) {
                                                            DebugLog(@"yup operation is cancelled %d",[error code]);
                                                        }else{
                                                            [INUserDefaultOperations performSelector:@selector(showAlert:) withObject:[NSString stringWithFormat:@"%@",[error localizedDescription]] afterDelay:2.0];
                                                        }
                                                        isAdding = NO;
                                                        if(isLoading)
                                                        {
                                                            [self stopLoading];
                                                        }
                                                        [CommonCallback hideProgressHud];
                                                    }];
    [operation start];
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleShrink];
    [MMProgressHUD showWithTitle:HUD_TITLE
                          status:@""
             confirmationMessage:[NSString stringWithFormat:@"Cancel?"]
                     cancelBlock:^{
                         DebugLog(@"Task was cancelled!");
                         [operation cancel];
                     }];
}

-(void)pageRequest:(NSString *)pageNum
{
    if([IN_APP_DELEGATE networkavailable])
    {
        isAdding = YES;
        [self sendMyShoplocalOfferRequest:MYOFFERS_LINK(pageNum)];
    }  else   {
        isAdding = NO;
        if(isLoading)
        {
            [self stopLoading];
        }
        [INUserDefaultOperations showOfflineAlert];
    }
}

#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:callModalTableView]) {
        return 80;
    }else{
        INFeedObj *tempfeedObj  = [myofferArray objectAtIndex:indexPath.row];
        if(tempfeedObj.image_url1 != (NSString*)[NSNull null] && ![tempfeedObj.image_url1 isEqualToString:@""]) {
            return kWithImageCellHeight;
        }
        else {
            return kWithoutImageCellHeight;
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:callModalTableView]) {
        return [telArray count];
    }else{
        return [myofferArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:callModalTableView]) {
        static NSString *CellIdentifier = @"CallCell";
        UIImageView *thumbImg;
        UILabel *lblNumber;
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.backgroundColor = [UIColor whiteColor];
            
            thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(10,15, 50, 50)];
            thumbImg.tag = 1111;
            thumbImg.contentMode = UIViewContentModeScaleToFill;
            thumbImg.image = [UIImage imageNamed:@"call_brown.png"];
            [cell.contentView addSubview:thumbImg];
            
            lblNumber = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame),0, 210, 80)];
            lblNumber.text = @"";
            lblNumber.tag = 2222;
            lblNumber.textColor             = BROWN_COLOR;
            lblNumber.highlightedTextColor  = [UIColor whiteColor];
            lblNumber.font                  = DEFAULT_FONT(23);
            lblNumber.textAlignment         = NSTextAlignmentLeft;
            lblNumber.backgroundColor       = [UIColor clearColor];
            [cell.contentView addSubview:lblNumber];
            
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = BROWN_OFFWHITE_COLOR;
            cell.selectedBackgroundView = bgColorView;
        }
        lblNumber        = (UILabel *)[cell.contentView viewWithTag:2222];
        DebugLog(@"lblNumber %@",[telArray objectAtIndex:indexPath.row]);
        lblNumber.text   = [telArray objectAtIndex:indexPath.row];
        return cell;
    }else{
    
        DebugLog(@"---%@----",myofferArray);
        if ([myofferArray count] <= 0)
            return nil;
        
        INFeedObj *tempfeedObj  = [myofferArray objectAtIndex:indexPath.row];
        DebugLog(@"---%@----",tempfeedObj.date);
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        NSDate *myDate = [[NSDate alloc] init];
        myDate = [dateFormatter dateFromString:tempfeedObj.date];
        DebugLog(@"MyDate is: %@", myDate);
        
        NSDateFormatter *dayformatter = [[NSDateFormatter alloc] init];
        [dayformatter setDateFormat:@"dd"];
        NSString *dayFromDate = [dayformatter stringFromDate:myDate];
        DebugLog(@"Myday is: %@", dayFromDate);
        
        NSDateFormatter *monthformatter = [[NSDateFormatter alloc] init];
        [monthformatter setDateFormat:@"MMM"];
        NSString *monthFromDate = [monthformatter stringFromDate:myDate];
        DebugLog(@"MyMonth is: %@", monthFromDate);
        
        NSDateFormatter *timeformatter = [[NSDateFormatter alloc] init];
        [timeformatter setDateFormat:@"hh:mm a"];
        NSString *timeFromDate = [timeformatter stringFromDate:myDate];
        DebugLog(@"timeFromDate is: %@", timeFromDate);
        
        if(tempfeedObj.image_url1 != (NSString*)[NSNull null] && ![tempfeedObj.image_url1 isEqualToString:@""]) {
            NSString *cellIdentifier = kWithImageCellIdentifier;
            CellWithImageView *cell = (CellWithImageView *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            cell.lblTitle.text          = tempfeedObj.title;
            cell.lblLeftTitle.text      = tempfeedObj.name;
            cell.lblDescription.text    = tempfeedObj.description;
            cell.lblLikeCount.text      = tempfeedObj.total_like;
            cell.lblShareCount.text     = tempfeedObj.total_share;
            cell.lblTimeStamp.text      = [NSString stringWithFormat:@"%@ %@    %@",dayFromDate,monthFromDate,timeFromDate];
            if (tempfeedObj.is_offered != nil && [tempfeedObj.is_offered isEqualToString:@"1"]) {
                [cell.offerImageView setHidden:NO];
            }else{
                [cell.offerImageView setHidden:YES];
            }
            if (tempfeedObj.user_like != nil && [tempfeedObj.user_like isEqualToString:@"1"]) {
                cell.favImageView.image = [UIImage imageNamed:@"filled_like.png"];
            }else{
                cell.favImageView.image = [UIImage imageNamed:@"empty_like.png"];
            }
            //        [cell.thumbImageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(tempfeedObj.image_url1)] placeholderImage:[UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"]];
            
            if(tempfeedObj.image_url1 != nil && ![tempfeedObj.image_url1 isEqualToString:@""])
            {
                __weak UIImageView *thumbImg_ = cell.thumbImageView;
                //            [cell.thumbImageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(tempfeedObj.image_url1)]
                //                                placeholderImage:[UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"]
                //                                         success:^(UIImage *image) {
                //                                             DebugLog(@"success");
                //                                         }
                //                                         failure:^(NSError *error) {
                //                                             DebugLog(@"write error %@", error);
                //                                             thumbImg_.image = [UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"];
                //                                         }];
                [cell.thumbImageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(tempfeedObj.image_url1)] placeholderImage:[UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"] completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType) {
                    if (error) {
                        thumbImg_.image = [UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"];
                    }
                }];
            } else {
                cell.thumbImageView.image = [UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"];
            }
            
            UIButton *likeBtn = (UIButton *)[cell viewWithTag:111];
            [likeBtn addTarget:self action:@selector(likeBtnPressed:event:) forControlEvents:UIControlEventTouchUpInside];
            
            UIButton *shareBtn = (UIButton *)[cell viewWithTag:222];
            [shareBtn addTarget:self action:@selector(shareBtnPressed:event:) forControlEvents:UIControlEventTouchUpInside];
            
            UIButton *callBtn = (UIButton *)[cell viewWithTag:333];
            [callBtn addTarget:self action:@selector(callBtnTapped:event:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
            
        }else{
            NSString *cellIdentifier = kWithoutImageCellIdentifier;
            CellWithoutImageView *cell = (CellWithoutImageView *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            cell.lblTitle.text          = tempfeedObj.title;
            cell.lblLeftTitle.text      = tempfeedObj.name;
            cell.lblDescription.text    = tempfeedObj.description;
            cell.lblLikeCount.text      = tempfeedObj.total_like;
            cell.lblShareCount.text     = tempfeedObj.total_share;
            cell.lblTimeStamp.text      = [NSString stringWithFormat:@"%@ %@    %@",dayFromDate,monthFromDate,timeFromDate];
            if (tempfeedObj.is_offered != nil && [tempfeedObj.is_offered isEqualToString:@"1"]) {
                [cell.offerImageView setHidden:NO];
            }else{
                [cell.offerImageView setHidden:YES];
            }
            if (tempfeedObj.user_like != nil && [tempfeedObj.user_like isEqualToString:@"1"]) {
                cell.favImageView.image = [UIImage imageNamed:@"filled_like.png"];
            }else{
                cell.favImageView.image = [UIImage imageNamed:@"empty_like.png"];
            }
            
            UIButton *likeBtn = (UIButton *)[cell viewWithTag:111];
            [likeBtn addTarget:self action:@selector(likeBtnPressed:event:) forControlEvents:UIControlEventTouchUpInside];
            
            UIButton *shareBtn = (UIButton *)[cell viewWithTag:222];
            [shareBtn addTarget:self action:@selector(shareBtnPressed:event:) forControlEvents:UIControlEventTouchUpInside];
            
            UIButton *callBtn = (UIButton *)[cell viewWithTag:333];
            [callBtn addTarget:self action:@selector(callBtnTapped:event:) forControlEvents:UIControlEventTouchUpInside];
            return cell;
        }
    }
}

//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//
//
//
//    //    UILabel *lblDist;
//    //    UIImageView *ldistthumbImg;
//
//    UILabel *lblDay;
//    UILabel *lblMonth;
//
//    UILabel *lblStoreName;
//    UILabel *lblOfferTitle;
//    UILabel *lblDesc;
//    UIImageView *thumbImg;
//
//    UIImageView *offerthumbImg;
//
//    UILabel *lblLikeCount;
//    UIImageView *likethumbImg;
//
//    UILabel *lblShareCount;
//    UIImageView *sharethumbImg;
//
//    //    UILabel *lblViewsCount;
//    //    UIImageView *viewsthumbImg;
//
//    //    UILabel *lblOffer;
//
//    static NSString *CellIdentifier = @"feedCell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//
//    if(cell == nil)
//    {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
//        cell.accessoryType = UITableViewCellAccessoryNone;
//        //[cell.imageView setImage:[UIImage imageNamed:@"table_placeholder.png"]];
//
//        //        lblOffer = [[UILabel alloc] initWithFrame:CGRectMake(1.0,0, 318.0, 30.0)];
//        //        lblOffer.text = @"";
//        //        lblOffer.tag = 111;
//        //        lblOffer.font = DEFAULT_BOLD_FONT(15);
//        //        lblOffer.textAlignment = UITextAlignmentCenter;
//        //        lblOffer.textColor = [UIColor whiteColor];
//        //        lblOffer.backgroundColor =  LIGHT_GREEN_COLOR;
//        //        lblOffer.hidden = TRUE;
//        //        [cell.contentView addSubview:lblOffer];
//
//        //Initialize Image View with tag 1.(Thumbnail Image)
//        thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(10,5,80.0,70.0)];
//        thumbImg.tag = 100;
//        thumbImg.contentMode = UIViewContentModeScaleToFill;
//        [cell.contentView addSubview:thumbImg];
//
//        lblDay = [[UILabel alloc] initWithFrame:CGRectMake(10.0,5.0,80.0,70.0)];
//        lblDay.text = @"";
//        lblDay.tag = 101;
//        lblDay.font = [UIFont systemFontOfSize:35];
//        lblDay.textAlignment = UITextAlignmentCenter;
//        lblDay.textColor = [UIColor whiteColor];
//        lblDay.backgroundColor =  LIGHT_BLUE;
//        [cell.contentView addSubview:lblDay];
//
//        lblMonth = [[UILabel alloc] initWithFrame:CGRectMake(10.0,CGRectGetMaxY(thumbImg.frame),80.0,20.0)];
//        lblMonth.text = @"";
//        lblMonth.tag = 102;
//        lblMonth.font = [UIFont systemFontOfSize:15];
//        lblMonth.textAlignment = UITextAlignmentCenter;
//        lblMonth.textColor = [UIColor whiteColor];
//        lblMonth.backgroundColor =  DARK_YELLOW_COLOR;
//        [cell.contentView addSubview:lblMonth];
//
//        lblStoreName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame)+5,5.0,175.0,20.0)];
//        lblStoreName.text = @"";
//        lblStoreName.tag = 103;
//        lblStoreName.numberOfLines = 3;
//        lblStoreName.font = DEFAULT_BOLD_FONT(15);
//        lblStoreName.minimumFontSize = 14;
//        lblStoreName.adjustsFontSizeToFitWidth = TRUE;
//        lblStoreName.textAlignment = UITextAlignmentLeft;
//        lblStoreName.textColor = BROWN_COLOR;
//        lblStoreName.backgroundColor =  [UIColor clearColor];
//        [cell.contentView addSubview:lblStoreName];
//
//        lblOfferTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame)+5,CGRectGetMaxY(lblStoreName.frame),175.0,20.0)];
//        lblOfferTitle.text = @"";
//        lblOfferTitle.tag = 104;
//        lblOfferTitle.numberOfLines = 3;
//        lblOfferTitle.font = DEFAULT_FONT(13);
//        lblOfferTitle.minimumFontSize = 12;
//        lblOfferTitle.adjustsFontSizeToFitWidth = TRUE;
//        lblOfferTitle.textAlignment = UITextAlignmentLeft;
//        lblOfferTitle.textColor = BROWN_COLOR;
//        lblOfferTitle.backgroundColor =  [UIColor clearColor];
//        [cell.contentView addSubview:lblOfferTitle];
//
//        lblDesc = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame)+5,CGRectGetMaxY(lblOfferTitle.frame),175.0,50.0)];
//        lblDesc.text = @"";
//        lblDesc.tag = 105;
//        lblDesc.numberOfLines = 4;
//        lblDesc.font = DEFAULT_FONT(12);
//        lblDesc.textAlignment = UITextAlignmentLeft;
//        lblDesc.textColor = BROWN_COLOR;
//        lblDesc.backgroundColor =  [UIColor clearColor];
//        [cell.contentView addSubview:lblDesc];
//
//        offerthumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lblOfferTitle.frame)+5,5.0,40.0,40.0)];
//        offerthumbImg.tag = 106;
//        offerthumbImg.image = [UIImage imageNamed:@"ic_offer_icon.png"];
//        offerthumbImg.contentMode = UIViewContentModeScaleAspectFill;
//        [cell.contentView addSubview:offerthumbImg];
//
//        likethumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lblStoreName.frame)+5,CGRectGetMaxY(offerthumbImg.frame)+2,20.0,20.0)];
//        likethumbImg.tag = 107;
//        likethumbImg.image = [UIImage imageNamed:@"Shoplocal_Post-Title_Fv.png"];
//        likethumbImg.contentMode = UIViewContentModeScaleAspectFill;
//        [cell.contentView addSubview:likethumbImg];
//
//        lblLikeCount = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(likethumbImg.frame)+2,likethumbImg.frame.origin.y,20.0,20.0)];
//        lblLikeCount.text = @"";
//        lblLikeCount.tag = 108;
//        lblLikeCount.font = DEFAULT_BOLD_FONT(12);
//        lblLikeCount.textAlignment = UITextAlignmentLeft;
//        lblLikeCount.textColor = BROWN_COLOR;
//        lblLikeCount.backgroundColor =  [UIColor clearColor];
//        [cell.contentView addSubview:lblLikeCount];
//
//        sharethumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lblStoreName.frame)+5,CGRectGetMaxY(likethumbImg.frame)+2,20.0,20.0)];
//        sharethumbImg.tag = 109;
//        sharethumbImg.image = [UIImage imageNamed:@"Shoplocal_Post-Title_Share.png"];
//        sharethumbImg.contentMode = UIViewContentModeScaleAspectFill;
//        [cell.contentView addSubview:sharethumbImg];
//
//        lblShareCount = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(sharethumbImg.frame)+2,sharethumbImg.frame.origin.y,20.0,20.0)];
//        lblShareCount.text = @"";
//        lblShareCount.tag = 110;
//        lblShareCount.font = DEFAULT_BOLD_FONT(12);
//        lblShareCount.textAlignment = UITextAlignmentLeft;
//        lblShareCount.textColor = BROWN_COLOR;
//        lblShareCount.backgroundColor =  [UIColor clearColor];
//        [cell.contentView addSubview:lblShareCount];
//
//        //        viewsthumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lblTitle.frame)+5,CGRectGetMaxY(sharethumbImg.frame)+4,15.0,15.0)];
//        //        viewsthumbImg.tag = 109;
//        //        viewsthumbImg.image = [UIImage imageNamed:@"Shoplocal_Post-Title_View.png"];
//        //        viewsthumbImg.contentMode = UIViewContentModeScaleAspectFill;
//        //        [cell.contentView addSubview:viewsthumbImg];
//        //
//        //        lblViewsCount = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(viewsthumbImg.frame),viewsthumbImg.frame.origin.y,30.0,15.0)];
//        //        lblViewsCount.text = @"";
//        //        lblViewsCount.tag = 110;
//        //        lblViewsCount.font = DEFAULT_BOLD_FONT(12);
//        //        lblViewsCount.textAlignment = UITextAlignmentCenter;
//        //        lblViewsCount.textColor = BROWN_COLOR;
//        //        lblViewsCount.backgroundColor =  [UIColor clearColor];
//        //        [cell.contentView addSubview:lblViewsCount];
//
//
//        //        ldistthumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(272.0,75.0,15.0,15.0)];
//        //        ldistthumbImg.image = [UIImage imageNamed:@"list_distance_icon.png"];
//        //        ldistthumbImg.contentMode = UIViewContentModeScaleAspectFill;
//        //        [cell.contentView addSubview:ldistthumbImg];
//
//        //        lblDist = [[UILabel alloc] initWithFrame:CGRectMake(285.0,75.0,35.0,15.0)];
//        //        lblDist.text = @"";
//        //        lblDist.tag = 106;
//        //        lblDist.font = DEFAULT_BOLD_FONT(10);
//        //        lblDist.textAlignment = UITextAlignmentCenter;
//        //        lblDist.textColor = BROWN_COLOR;
//        //        lblDist.backgroundColor =  [UIColor clearColor];
//        //        [cell.contentView addSubview:lblDist];
//
//    }
//    if ([indexPath row] < [myofferArray count]) {
//        INFeedObj *tempfeedObj  = [myofferArray objectAtIndex:indexPath.row];
//        thumbImg        = (UIImageView *)[cell viewWithTag:100];
//        lblDay          = (UILabel *)[cell viewWithTag:101];
//        lblMonth        = (UILabel *)[cell viewWithTag:102];
//        lblStoreName    = (UILabel *)[cell viewWithTag:103];
//        lblOfferTitle   = (UILabel *)[cell viewWithTag:104];
//        lblDesc         = (UILabel *)[cell viewWithTag:105];
//        offerthumbImg   = (UIImageView *)[cell viewWithTag:106];
//        likethumbImg    = (UIImageView *)[cell viewWithTag:107];
//        lblLikeCount    = (UILabel *)[cell viewWithTag:108];
//        sharethumbImg   = (UIImageView *)[cell viewWithTag:109];
//        lblShareCount   = (UILabel *)[cell viewWithTag:110];
//        //        viewsthumbImg = (UIImageView *)[cell viewWithTag:109];
//        //        lblViewsCount = (UILabel *)[cell viewWithTag:110];
//        //        lblOffer        = (UILabel *)[cell viewWithTag:111];
//
//        /*if (tempfeedObj.is_offered != nil && [tempfeedObj.is_offered isEqualToString:@"1"]) {
//         //            thumbImg.frame =  CGRectMake(10, 30, 100, 100);
//         lblOffer.hidden = FALSE;
//         [lblOffer setText:[NSString stringWithFormat:@"Offer: %@",tempfeedObj.title]];
//
//         thumbImg.frame =  CGRectMake(10.0,CGRectGetMaxY(lblOffer.frame), thumbImg.frame.size.width, thumbImg.frame.size.height);
//         lblDay.frame =  CGRectMake(10.0,CGRectGetMaxY(lblOffer.frame)+5, lblDay.frame.size.width, lblDay.frame.size.height);
//         lblMonth.frame =  CGRectMake(10.0,CGRectGetMaxY(lblOffer.frame)+65, lblMonth.frame.size.width, lblMonth.frame.size.height);
//
//         lblTitle.frame =  CGRectMake(CGRectGetMaxX(thumbImg.frame)+5,CGRectGetMaxY(lblOffer.frame)+5, lblTitle.frame.size.width, lblTitle.frame.size.height);
//         lblDesc.frame =  CGRectMake(CGRectGetMaxX(thumbImg.frame)+5,CGRectGetMaxY(lblOffer.frame)+25, lblDesc.frame.size.width, lblDesc.frame.size.height);
//
//         likethumbImg.frame =  CGRectMake(CGRectGetMaxX(lblTitle.frame)+5,CGRectGetMaxY(lblOffer.frame)+20, likethumbImg.frame.size.width, likethumbImg.frame.size.height);
//         lblLikeCount.frame =  CGRectMake(CGRectGetMaxX(likethumbImg.frame),CGRectGetMaxY(lblOffer.frame)+20, lblLikeCount.frame.size.width, lblLikeCount.frame.size.height);
//
//         } else {
//         lblOffer.hidden = TRUE;
//         [lblOffer setText:@""];
//         thumbImg.frame =  CGRectMake(10.0, 0, thumbImg.frame.size.width, thumbImg.frame.size.height);
//         lblDay.frame =  CGRectMake(10.0, 5, lblDay.frame.size.width, lblDay.frame.size.height);
//         lblMonth.frame =  CGRectMake(10.0, 65, lblMonth.frame.size.width, lblMonth.frame.size.height);
//
//         lblTitle.frame =  CGRectMake(CGRectGetMaxX(thumbImg.frame)+5, 5, lblTitle.frame.size.width, lblTitle.frame.size.height);
//         lblDesc.frame =  CGRectMake(CGRectGetMaxX(thumbImg.frame)+5, 25, lblDesc.frame.size.width, lblDesc.frame.size.height);
//
//         likethumbImg.frame =  CGRectMake(CGRectGetMaxX(lblTitle.frame)+5, 20, likethumbImg.frame.size.width, likethumbImg.frame.size.height);
//         lblLikeCount.frame =  CGRectMake(CGRectGetMaxX(likethumbImg.frame), 20, lblLikeCount.frame.size.width, lblLikeCount.frame.size.height);
//         }
//         sharethumbImg.frame =  CGRectMake(CGRectGetMaxX(lblTitle.frame)+5,CGRectGetMaxY(likethumbImg.frame)+4, sharethumbImg.frame.size.width, sharethumbImg.frame.size.height);
//         lblShareCount.frame =  CGRectMake(CGRectGetMaxX(sharethumbImg.frame),sharethumbImg.frame.origin.y,lblShareCount.frame.size.width, lblShareCount.frame.size.height);
//         */
//
//        if (tempfeedObj.is_offered != nil && [tempfeedObj.is_offered isEqualToString:@"1"]) {
//            [offerthumbImg setHidden:NO];
//        }else{
//            [offerthumbImg setHidden:YES];
//        }
//
//        if(tempfeedObj.image_url1 != (NSString*)[NSNull null] && ![tempfeedObj.image_url1 isEqualToString:@""]) {
//            lblDay.backgroundColor = [UIColor clearColor];
//            [thumbImg setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(tempfeedObj.image_url1)] placeholderImage:[UIImage imageNamed:@"list_thumbnail.png"]];
//            DebugLog(@"link-%@",THUMBNAIL_LINK(tempfeedObj.image_url1));
//        } else {
//            thumbImg.image = nil;
//            lblDay.backgroundColor =  LIGHT_BLUE;
//        }
//
//        //DebugLog(@"%@",lbroadcastObj.date);
//        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//        [dateFormatter setDateFormat:@"yyyy-MM-dd  hh:mm:ss"];
//        NSDate *myDate = [[NSDate alloc] init];
//        myDate = [dateFormatter dateFromString:tempfeedObj.date];
//        //DebugLog(@"MyDate is: %@", myDate);
//
//        NSDateFormatter *dayformatter = [[NSDateFormatter alloc] init];
//        [dayformatter setDateFormat:@"dd"];
//        NSString *dayFromDate = [dayformatter stringFromDate:myDate];
//        //DebugLog(@"Myday is: %@", dayFromDate);
//        lblDay.text = dayFromDate;
//
//        NSDateFormatter *monthformatter = [[NSDateFormatter alloc] init];
//        [monthformatter setDateFormat:@"MMM"];
//        NSString *monthFromDate = [monthformatter stringFromDate:myDate];
//        //DebugLog(@"MyMonth is: %@", monthFromDate);
//        lblMonth.text = monthFromDate;
//
//        [lblStoreName setText:tempfeedObj.name];
//        [lblOfferTitle setText:tempfeedObj.title];
//        [lblDesc setText: tempfeedObj.description];
//
//        //        DebugLog(@"%@",tempfeedObj.distance);
//        //        if(tempfeedObj.distance != nil) {
//        //            UILabel *lblDist = (UILabel *)[cell viewWithTag:106];
//        //            DebugLog(@"%@",tempfeedObj.distance);
//        //            if([tempfeedObj.distance floatValue]  > -1 && [tempfeedObj.distance floatValue]  < 1)
//        //                [lblDist setText: [NSString stringWithFormat:@"%.2fm",([tempfeedObj.distance floatValue] * 100)]];
//        //            else
//        //                [lblDist setText: [NSString stringWithFormat:@"%.2fkm",([tempfeedObj.distance floatValue])]];
//        //        } else {
//        //            [lblDist setText: @"0.00m"];
//        //        }
//
//        [lblLikeCount setHidden:FALSE];
//        [likethumbImg setHidden:FALSE];
//        lblLikeCount.text = tempfeedObj.total_like;
//        [lblShareCount setHidden:FALSE];
//        [sharethumbImg setHidden:FALSE];
//        lblShareCount.text = tempfeedObj.total_share;
//
//        //        [lblViewsCount setHidden:FALSE];
//        //        [viewsthumbImg setHidden:FALSE];
//        //        lblViewsCount.text = tempfeedObj.total_view;
//    }
//    return cell;
//}


#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:callModalTableView]) {
        [callModalView setHidden:YES];
        [callModalViewBackView setHidden:YES];
        NSString *selectedTelNumberToCall = [[telArray objectAtIndex:indexPath.row] stringByReplacingOccurrencesOfString:@" " withString:@""];
        DebugLog(@"selectedTelNumberToCall %@",selectedTelNumberToCall);
        NSString *deviceType = [UIDevice currentDevice].model;
        if([deviceType isEqualToString:@"iPhone"])
        {
            UIApplication *myApp = [UIApplication sharedApplication];
            DebugLog(@"selectedTelNumberToCall = -%@-",selectedTelNumberToCall);
            [myApp openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",selectedTelNumberToCall]]];
            
            NSDictionary *callParams = [NSDictionary dictionaryWithObjectsAndKeys:@"MyShoplocal",@"Source",[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"AreaName",selectedPlaceName,@"StoreName",selectedTelNumberToCall,@"Number", nil];
            [INEventLogger logEvent:@"StoreCallDone" withParams:callParams];
            
            [self sendStoreCallRequest:@"StoreCallDone"];
        }
        else
        {
            UIAlertView *alert = [[ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                              message: @"Calling functionality is not available in this device. "
                                                             delegate: nil
                                                    cancelButtonTitle: nil
                                                    otherButtonTitles: @"OK", nil];
            [alert show];
        }
    }else{
        INFeedObj *lfeedObj  = [myofferArray objectAtIndex:indexPath.row];
        INCustBroadCastDetailsViewController *postDetailController = [[INCustBroadCastDetailsViewController alloc] initWithNibName:@"INCustBroadCastDetailsViewController" bundle:nil] ;
        postDetailController.title = lfeedObj.name;//@"Broadcast Detail";
        postDetailController.storeName = lfeedObj.name;
        postDetailController.postType = 1;
        //    postDetailController.postlikeType = lfeedObj.total_like;
        postDetailController.postId = lfeedObj.postID;
        postDetailController.postTitle = lfeedObj.title;
        postDetailController.postDescription = lfeedObj.description;
        if ([lfeedObj.image_url1 isEqual:[NSNull null]] ||  lfeedObj.image_url1 == nil) {
            postDetailController.postimageUrl = @"";
        }else{
            postDetailController.postimageUrl = lfeedObj.image_url1;
        }
        postDetailController.likescountString = lfeedObj.total_like;
        postDetailController.sharecountString = lfeedObj.total_share;
        postDetailController.viewOpenedFrom = FROM_NEWS_FEED;
        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
        if (lfeedObj.tel_no1 != nil && ![lfeedObj.tel_no1 isEqualToString:@""]) {
            [tempArray addObject:lfeedObj.tel_no1];
        }
        if (lfeedObj.tel_no2 != nil && ![lfeedObj.tel_no2 isEqualToString:@""]) {
            [tempArray addObject:lfeedObj.tel_no2];
        }
        if (lfeedObj.tel_no3 != nil && ![lfeedObj.tel_no3 isEqualToString:@""]) {
            [tempArray addObject:lfeedObj.tel_no3];
        }
        if (lfeedObj.mob_no1 != nil && ![lfeedObj.mob_no1 isEqualToString:@""]) {
            [tempArray addObject:lfeedObj.mob_no1];
        }
        if (lfeedObj.mob_no2 != nil && ![lfeedObj.mob_no2 isEqualToString:@""]) {
            [tempArray addObject:lfeedObj.mob_no2];
        }
        if (lfeedObj.mob_no3 != nil && ![lfeedObj.mob_no3 isEqualToString:@""]) {
            [tempArray addObject:lfeedObj.mob_no3];
        }
        DebugLog(@"temparray %@",tempArray);
        postDetailController.telArray = tempArray;
        postDetailController.SHOW_BUY_BTN = YES;
        postDetailController.placeId = lfeedObj.placeID;
        postDetailController.postDateTime   = lfeedObj.date;
        postDetailController.postOfferDateTime  = lfeedObj.offerdate;
        postDetailController.postIsOffered      = lfeedObj.is_offered;
        [self.navigationController pushViewController:postDetailController animated:YES];
        [INEventLogger logEvent:@"MyShoplocal_Details"];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)loadData
{
    self.currentPage = 1;
    [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
    [INEventLogger logEvent:@"MyShoplocal_Fetch"];
}

- (void)reloadData:(NSNotification *)notification
{
    DebugLog(@"Reload data");
    [myofferArray removeAllObjects];
    self.totalRecord = 0;
    self.currentPage = 1;
    [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
}

-(void)setCountLabel
{
    self.countlbl.text = [NSString stringWithFormat:@"%d/%d",[myofferArray count],self.totalRecord];
    DebugLog(@"-----%@--------",countlbl.text);
}

#pragma mark PULL TO REFRESH
- (void)setupStrings{
    textPull = @"Pull down to refresh...";
    textRelease = @"Release to refresh...";
    textLoading = @"Loading please wait...";
}

- (void)addPullToRefreshHeader {
    refreshHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0 - REFRESH_HEADER_HEIGHT, 320, REFRESH_HEADER_HEIGHT)];
    refreshHeaderView.backgroundColor = [UIColor clearColor];
    
    refreshLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, REFRESH_HEADER_HEIGHT)];
    refreshLabel.backgroundColor = [UIColor clearColor];
    refreshLabel.font = DEFAULT_BOLD_FONT(15);
    refreshLabel.textAlignment = NSTextAlignmentCenter;
    refreshLabel.textColor = DEFAULT_COLOR;
    refreshArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:PULLTOREFRESH_BARROW]];
    refreshArrow.frame = CGRectMake(floorf((REFRESH_HEADER_HEIGHT - 2) / 2),
                                    (floorf(REFRESH_HEADER_HEIGHT - 44) / 2),
                                    27, 44);
    
    refreshSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    refreshSpinner.frame = CGRectMake(floorf(floorf(REFRESH_HEADER_HEIGHT - 2) / 2), floorf((REFRESH_HEADER_HEIGHT - 20) / 2), 20, 20);
    refreshSpinner.hidesWhenStopped = YES;
    
    [refreshHeaderView addSubview:refreshLabel];
    [refreshHeaderView addSubview:refreshArrow];
    [refreshHeaderView addSubview:refreshSpinner];
    [myoffersTableView addSubview:refreshHeaderView];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if ([scrollView isEqual:callModalTableView]) {
        return;
    }
    if (isLoading) return;
    isDragging = YES;
    
    lastContentOffset = scrollView.contentOffset.y;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([scrollView isEqual:callModalTableView]) {
        return;
    }
    if (isLoading) {
        // Update the content inset, good for section headers
        if (scrollView.contentOffset.y > 0)
            myoffersTableView.contentInset = UIEdgeInsetsZero;
        else if (scrollView.contentOffset.y >= -REFRESH_HEADER_HEIGHT)
            myoffersTableView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (isDragging && scrollView.contentOffset.y < 0) {
        // Update the arrow direction and label
        [UIView animateWithDuration:0.25 animations:^{
            if (scrollView.contentOffset.y < -REFRESH_HEADER_HEIGHT) {
                // User is scrolling above the header
                refreshLabel.text = self.textRelease;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
            } else {
                // User is scrolling somewhere within the header
                refreshLabel.text = self.textPull;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
            }
        }];
    } else {
        CGFloat height = scrollView.frame.size.height;
        
        CGFloat contentYoffset = scrollView.contentOffset.y;
        
        CGFloat distanceFromBottom = scrollView.contentSize.height - contentYoffset;
        
        if(distanceFromBottom <= height)
        {
            if(isAdding==TRUE) {
                DebugLog(@"<<<<<<<<<<<<<<<");
                return;
            } else {
                DebugLog(@"end of the table page = %d", self.currentPage);
                if(self.currentPage < self.totalPage)
                {
                    self.currentPage = self.currentPage + 1;
                    [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
                }
            }
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if ([scrollView isEqual:callModalTableView]) {
        return;
    }
    if (isLoading) return;
    isDragging = NO;
    if (scrollView.contentOffset.y <= -REFRESH_HEADER_HEIGHT) {
        // Released above the header
        [self startLoading];
    }
    
    if (lastContentOffset < (int)scrollView.contentOffset.y) {
        DebugLog(@"lastContentOffset <");
        if (favStoreBtn.frame.size.height >= 70) {
            [UIView animateWithDuration:0.2
                             animations:^(void){
                                 favStoreBtn.frame = CGRectMake(favStoreBtn.frame.origin.x, favStoreBtn.frame.origin.y, favStoreBtn.frame.size.width, 35);
                                 myoffersTableView.frame = CGRectMake(myoffersTableView.frame.origin.x, CGRectGetMaxY(favStoreBtn.frame), myoffersTableView.frame.size.width, self.view.frame.size.height-(CGRectGetMaxY(favStoreBtn.frame)));
                             } completion:nil];
        }
    }
    else if (lastContentOffset > (int)scrollView.contentOffset.y) {
        DebugLog(@"lastContentOffset >");
        if (favStoreBtn.frame.size.height < 70) {
            [UIView animateWithDuration:0.2
                             animations:^(void){
                                 favStoreBtn.frame = CGRectMake(favStoreBtn.frame.origin.x, favStoreBtn.frame.origin.y, favStoreBtn.frame.size.width, 70);
                                 myoffersTableView.frame = CGRectMake(myoffersTableView.frame.origin.x, CGRectGetMaxY(favStoreBtn.frame), myoffersTableView.frame.size.width, self.view.frame.size.height-(CGRectGetMaxY(favStoreBtn.frame)));
                             } completion:nil];
        }
    }
}

- (void)startLoading {
    isLoading = YES;
    // Show the header
    [UIView animateWithDuration:0.3 animations:^{
        myoffersTableView.contentInset = UIEdgeInsetsMake(REFRESH_HEADER_HEIGHT, 0, 0, 0);
        refreshLabel.text = self.textLoading;
        refreshArrow.hidden = YES;
        [refreshSpinner startAnimating];
    }];
    if([IN_APP_DELEGATE networkavailable])
    {
        //[myofferArray removeAllObjects];
        self.totalRecord = 0;
        self.currentPage = 1;
        [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
        [INEventLogger logEvent:@"MyShoplocal_Fetch"];
    }
    else
    {
        [self stopLoading];
    }
    // Refresh action!
    //[self refresh];
}

- (void)stopLoading {
    isLoading = NO;
    // Hide the header
    [UIView animateWithDuration:0.3 animations:^{
        myoffersTableView.contentInset = UIEdgeInsetsZero;
        [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
    }
                     completion:^(BOOL finished) {
                         [self performSelector:@selector(stopLoadingComplete)];
                     }];
}

- (void)stopLoadingComplete {
    // Reset the header
    refreshLabel.text = self.textPull;
    refreshArrow.hidden = NO;
    [refreshSpinner stopAnimating];
}

-(void)showLoginAlert{
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Please Login to mark this post as favourite."];
    alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
    alertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
    
    [alertView addButtonWithTitle:@"LOGIN"
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alert) {
                              DebugLog(@"LOGIN Clicked");
                              [self showLoginModal];
                          }];
    [alertView addButtonWithTitle:@"NOT NOW"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alert) {
                              DebugLog(@"NOT NOW Clicked");
                          }];
    [alertView show];
}
- (void)likeBtnPressed:(id)sender event:(id)event {
    DebugLog(@"likeBtnPressed");
    if([IN_APP_DELEGATE networkavailable])
    {
        NSSet *touches = [event allTouches];
        UITouch *touch = [touches anyObject];
        CGPoint currentTouchPosition = [touch locationInView:self.myoffersTableView];
        NSIndexPath *indexPath = [self.myoffersTableView indexPathForRowAtPoint:currentTouchPosition];
        if (indexPath != nil) {
            if(![INUserDefaultOperations isCustomerLoggedIn])
            {
                [self showLoginAlert];
            } else {
                [INEventLogger logEvent:@"MyShoplocal_Liked"];
                currentSelectedRow = indexPath.row;
                INFeedObj *tempfeedObj = (INFeedObj *)[myofferArray objectAtIndex:indexPath.row];
                DebugLog(@"user_like %@ post_id %d",tempfeedObj.user_like,tempfeedObj.postID);
                if ([tempfeedObj.user_like intValue]) {
                    [self sendPostUnLikeRequest];
                }else{
                     [self sendPostLikeRequest];
//                    if ([[INUserDefaultOperations getCustomerLoginFrom] isEqualToString:FACEBOOKSERVER] && [INUserDefaultOperations isOpenGraphShareEnabled]) {
//                        SIAlertView *sialertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Do you want to share this on your Facebook timeline?"];
//                        sialertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
//                        sialertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
//                        [sialertView addButtonWithTitle:@"YES"
//                                                   type:SIAlertViewButtonTypeDestructive
//                                                handler:^(SIAlertView *alert) {
//                                                    DebugLog(@"YES");
//                                                    [self sendPostLikeRequest];
//
//                                                    NSString *deepLinkingUrl = [FacebookOpenGraphAPI createDeepLinkWithActiveAreaName:[IN_APP_DELEGATE getActiveSlugFromAreaTable] storeName:tempfeedObj.name storeId:[NSString stringWithFormat:@"%d",tempfeedObj.placeID]];
//                                                    NSString *postTitle         = tempfeedObj.title;
//                                                    NSString *postDescription   = tempfeedObj.description;
//                                                    NSString *postimageUrl      = tempfeedObj.image_url1;
//                                                    if (postimageUrl != nil && ![postimageUrl isEqualToString:@""]) {
//                                                        if ([postimageUrl hasPrefix:@"http"]) {
//                                                            [FacebookOpenGraphAPI likeContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:postimageUrl deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
//                                                        }else{
//                                                            [FacebookOpenGraphAPI likeContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:THUMBNAIL_LINK(postimageUrl) deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
//                                                        }
//                                                    }else{
//                                                        [FacebookOpenGraphAPI likeContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:@"" deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
//                                                    }
//                                                }];
//                        [sialertView addButtonWithTitle:@"NOT NOW"
//                                                   type:SIAlertViewButtonTypeCancel
//                                                handler:^(SIAlertView *alert) {
//                                                    DebugLog(@"NO");
//                                                    [self sendPostLikeRequest];
//                                                }];
//                        [sialertView show];
//                    }else{
//                        [self sendPostLikeRequest];
//                    }
                }
            }
        }
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

- (void)shareBtnPressed:(id)sender event:(id)event {
    DebugLog(@"shareBtnPressed");
    [INEventLogger logEvent:@"MyShoplocal_Shared"];
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.myoffersTableView];
    NSIndexPath *indexPath = [self.myoffersTableView indexPathForRowAtPoint:currentTouchPosition];
    if (indexPath != nil) {
        currentSelectedRow = indexPath.row;
        UIActionSheet *shareActionSheet = [[UIActionSheet alloc] initWithTitle:@"Share via" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"SMS",@"Email",@"WhatsApp",@"Facebook",@"Twitter", nil];
        [shareActionSheet showInView:self.view];
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSString *title = [actionSheet buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"SMS"])
    {
        [self smsShareClicked];
    }
    else if([title isEqualToString:@"Email"])
    {
        [self emailShareClicked];
    }else if([title isEqualToString:@"WhatsApp"])
    {
        [self whatsAppShareClicked];
    }
    else if([title isEqualToString:@"Facebook"])
    {
        [self facebookShareClicked];
    } else if([title isEqualToString:@"Twitter"])
    {
        [self twitterShareClicked];
    }
}
-(void)smsShareClicked{
    if ([MFMessageComposeViewController canSendText]) {
        NSString *message = [self getPostDetailsMessageBody:@"sms"];
        
        MFMessageComposeViewController *messageComposer = [[MFMessageComposeViewController alloc] init];
        messageComposer.messageComposeDelegate = self;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            messageComposer.navigationBar.tintColor  = [UIColor whiteColor];
        }
        [messageComposer setBody:message];
        [messageComposer setRecipients:nil];
        [self presentViewController:messageComposer animated:YES completion:nil];
    }else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"You may not have set up sms service on your device.\nPlease check and try again."
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}
#pragma mark MFMessageComposeViewController Delegate Methods
-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
		case MessageComposeResultCancelled:
            DebugLog(@"SMS Cancelled");
			break;
		case MessageComposeResultFailed:
        {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Unknown Error\nSMS failed"
														   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
        }
			break;
		case MessageComposeResultSent:
        {
            DebugLog(@"SMS Sent");
        }
			break;
		default:
			break;
	}
    [self dismissViewControllerAnimated:YES completion:^{
        if (result == MessageComposeResultSent) {
            [self sendPostShareRequest:@"sms"];
        }
    }];
}

-(void)emailShareClicked{
    if ([MFMailComposeViewController canSendMail]) {
        INFeedObj *tempfeedObj = (INFeedObj *)[myofferArray objectAtIndex:currentSelectedRow];
        NSString *message = [self getPostDetailsMessageBody:@"email"];
        
        MFMailComposeViewController *emailComposer = [[MFMailComposeViewController alloc] init];
        emailComposer.mailComposeDelegate = self;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            emailComposer.navigationBar.tintColor  = [UIColor whiteColor];
        }
        [emailComposer setToRecipients:nil];
        if (tempfeedObj.name != nil && ![tempfeedObj.name isEqualToString:@""]) {
            [emailComposer setSubject:[NSString stringWithFormat:@"%@",tempfeedObj.name]];
        }else{
            [emailComposer setSubject:@""];
        }
        if (tempfeedObj.image_url1 != nil && ![tempfeedObj.image_url1 isEqualToString:@""]) {
            NSString *htmlMessage = [message stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
            DebugLog(@"%@--%@",message,htmlMessage);
            [emailComposer setMessageBody:[NSString stringWithFormat:@"%@ <br><br><img src=%@ alt=%@/>",htmlMessage,THUMBNAIL_LINK(tempfeedObj.image_url1),tempfeedObj.title] isHTML:YES];
        }else{
            [emailComposer setMessageBody:message isHTML:NO];
        }
        [self presentViewController:emailComposer animated:YES completion:nil];
    }else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"You may not have set up mail service on your device or You may not connected to internet.\nPlease check and try again."
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}
#pragma mark Mail Compose Delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    if (result == MFMailComposeResultFailed) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                        message:NSLocalizedString(@"Email failed to send. Please try again.", nil)
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", nil) otherButtonTitles:nil];
		[alert show];
    }
	[self dismissViewControllerAnimated:YES completion:^{
        if (result == MFMailComposeResultSent) {
            [self sendPostShareRequest:@"email"];
        }
    }];
}

-(void)whatsAppShareClicked{
    //    NSString *shareMSGBody = [[NSString stringWithFormat:@"Store Name: %@\nOffer: %@\nDescription: %@",storeName,postTitle,postDescription] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *message = [self getPostDetailsMessageBody:@"whatsapp"];
    NSString *urlString = [NSString stringWithFormat:@"whatsapp://send?text=%@",[message stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURL *whatsappURL = [NSURL URLWithString:urlString];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [[UIApplication sharedApplication] openURL: whatsappURL];
        [self sendPostShareRequest:@"whatsapp"];
    }
}

-(void)twitterShareClicked{
    if ([IN_APP_DELEGATE networkavailable]) {
        NSString *message = [self getPostDetailsMessageBody:@"twitter"];
        INFeedObj *tempfeedObj = (INFeedObj *)[myofferArray objectAtIndex:currentSelectedRow];
        NSString *postimageUrl = tempfeedObj.image_url1;
        DebugLog(@"Length %d",[message length]);

        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
            
            SLComposeViewController *socialComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
            if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
            {
                if ([message length] > 140) {
                    [socialComposer setInitialText:[message substringToIndex:140]];
                }else{
                    [socialComposer setInitialText:message];
                }
            }
            else{
                [socialComposer setInitialText:message];
            }
            DebugLog(@"length %d",[message length]);
            if(postimageUrl != nil && ![postimageUrl isEqualToString:@""])
            {
                UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:THUMBNAIL_LINK(postimageUrl)]]];
                [socialComposer addImage:image];
            }
            [socialComposer setCompletionHandler:^(SLComposeViewControllerResult result){
                switch (result) {
                    case SLComposeViewControllerResultCancelled:
                        DebugLog(@"SLComposeViewControllerResultCancelled");
                        break;
                    case SLComposeViewControllerResultDone:
                        DebugLog(@"SLComposeViewControllerResultDone");
                        [self sendPostShareRequest:@"twitter"];
                        break;
                    default:
                        DebugLog(@"SLComposeViewControllerResultFailed");
                        break;
                }
                [self dismissViewControllerAnimated:YES completion:NULL];
            }];
            [self presentViewController:socialComposer animated:YES completion:Nil];
        }
        else{
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:ALERT_TITLE
                                      message:@"You may not have set up twitter service on your device.Please check and try again."
                                      delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [alertView show];
        }
    }else{
        [INUserDefaultOperations showOfflineAlert];
    }
}

-(void)facebookShareClicked{
    if ([IN_APP_DELEGATE networkavailable]) {
        [self sendPostShareRequest:@"facebook"];

        NSString *message = [self getPostDetailsMessageBody:@"facebook"];
        DebugLog(@"message %@",message);
        INFeedObj *tempfeedObj = (INFeedObj *)[myofferArray objectAtIndex:currentSelectedRow];
        NSString *postimageUrl = tempfeedObj.image_url1;
        NSString *storeName     = tempfeedObj.name;
        NSInteger placeId       = tempfeedObj.placeID;
        
        NSString *deepLinkingUrl = [FacebookOpenGraphAPI createDeepLinkWithActiveAreaName:[IN_APP_DELEGATE getActiveSlugFromAreaTable] storeName:storeName storeId:[NSString stringWithFormat:@"%d",placeId]];
        
        if (postimageUrl != nil && ![postimageUrl isEqualToString:@""]) {
            if ([postimageUrl hasPrefix:@"http"]) {
                [FacebookShareAPI shareContentWithTitle:[NSString stringWithFormat:@"%@ - Offer\n",storeName] description:message image:postimageUrl deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
            }else{
                [FacebookShareAPI shareContentWithTitle:[NSString stringWithFormat:@"%@ - Offer\n",storeName] description:message image:THUMBNAIL_LINK(postimageUrl) deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
            }
        }else{
            [FacebookShareAPI shareContentWithTitle:[NSString stringWithFormat:@"%@ - Offer\n",storeName] description:message image:@"" deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
        }
    }else{
        [INUserDefaultOperations showOfflineAlert];
    }
    
//    if ([IN_APP_DELEGATE networkavailable]) {
//        NSString *message = [self getPostDetailsMessageBody:@"facebook"];
//        INFeedObj *tempfeedObj = (INFeedObj *)[myofferArray objectAtIndex:currentSelectedRow];
//        NSString *postimageUrl = tempfeedObj.image_url1;
//
//        if (SYSTEM_VERSION_LESS_THAN(@"6.0"))
//        {
//            FaceBookShareViewController *facebookViewComposer = [[FaceBookShareViewController alloc] initWithNibName:@"FaceBookShareViewController" bundle:nil];
//            facebookViewComposer.fbdelegate = self;
//            facebookViewComposer.title = @"FACEBOOK SHARE";
//            facebookViewComposer.FBtitle = ALERT_TITLE;
//            facebookViewComposer.postMessageText = message;
//            facebookViewComposer.FBtPic = THUMBNAIL_LINK(postimageUrl);
//            UINavigationController *fbnavBar=[[UINavigationController alloc]initWithRootViewController:facebookViewComposer];
//            [self.navigationController presentViewController:fbnavBar animated:YES completion:nil];
//        }else{
//            if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
//                
//                SLComposeViewController *socialComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
//                [socialComposer setInitialText:message];
//                if(postimageUrl != nil && ![postimageUrl isEqualToString:@""])
//                {
//                    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:THUMBNAIL_LINK(postimageUrl)]]];
//                    [socialComposer addImage:image];
//                }
//                [socialComposer setCompletionHandler:^(SLComposeViewControllerResult result){
//                    switch (result) {
//                        case SLComposeViewControllerResultCancelled:
//                            DebugLog(@"SLComposeViewControllerResultCancelled");
//                            break;
//                        case SLComposeViewControllerResultDone:
//                            DebugLog(@"SLComposeViewControllerResultDone");
//                            [self sendPostShareRequest:@"facebook"];
//                            break;
//                        default:
//                            DebugLog(@"SLComposeViewControllerResultFailed");
//                            break;
//                    }
//                    [self dismissViewControllerAnimated:YES completion:NULL];
//                }];
//                [self presentViewController:socialComposer animated:YES completion:Nil];
//            }
//            else{
//                UIAlertView *alertView = [[UIAlertView alloc]
//                                          initWithTitle:ALERT_TITLE
//                                          message:@"You may not have set up facebook service on your device.Please check and try again."
//                                          delegate:nil
//                                          cancelButtonTitle:@"OK"
//                                          otherButtonTitles:nil];
//                [alertView show];
//            }
//        }
//    }else{
//        [INUserDefaultOperations showOfflineAlert];
//    }
}

-(void)faceBookCompletionCallBack:(int)result
{
    DebugLog(@"Customer : faceBookCompletionCallBack %d",result);
    if (result == FACEBOOK_POST_SUCCESS) {
        [self sendPostShareRequest:@"facebook"];
    }
}


#pragma AFNetworking delegate methods
-(void)sendPostLikeRequest
{
    DebugLog(@"========================sendPostLikeRequest========================");
    INFeedObj *tempfeedObj = (INFeedObj *)[myofferArray objectAtIndex:currentSelectedRow];
    
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[INUserDefaultOperations getCustomerAuthId] forKey:@"user_id"];
    [params setObject:[INUserDefaultOperations getCustomerAuthCode] forKey:@"auth_id"];
    [params setObject:[NSString stringWithFormat:@"%d",tempfeedObj.postID] forKey:@"post_id"];
    [httpClient postPath:LIKE_BROADCAST_OF_STORE parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            if([json  objectForKey:@"message"] != [NSNull null] && [[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
                tempfeedObj.user_like = @"1";
                tempfeedObj.total_like = [NSString stringWithFormat:@"%d",[tempfeedObj.total_like intValue] + 1];
                //                if([json  objectForKey:@"message"] != [NSNull null] && ![[json objectForKey:@"message"] isEqualToString:@""]){
                //                    NSString *message = [json objectForKey:@"message"];
                //                    [INUserDefaultOperations showSIAlertView:message];
                //                }
            } else {
                NSString* code = [json objectForKey:@"code"];
                if([code isEqualToString:CUSTOMER_ALREADY_MADE_POST_FAV])
                {
                    tempfeedObj.user_like = @"1";
                    if ([tempfeedObj.total_like intValue] == 0) {
                        tempfeedObj.total_like = @"1";
                    }
                    //                    if([json  objectForKey:@"message"] != [NSNull null] && ![[json objectForKey:@"message"] isEqualToString:@""]){
                    //                        NSString *message = [json objectForKey:@"message"];
                    //                        [INUserDefaultOperations showSIAlertView:message];
                    //                    }
                }
                else if([code isEqualToString:CUSTOMER_INVALID_USER_AUTH_CODE])
                {
                    [INUserDefaultOperations showSIAlertView:CUSTOMER_INVALID_USER_AUTH_MESSAGE];
                    if([INUserDefaultOperations isCustomerLoggedIn]){
                        [INUserDefaultOperations clearCustomerDetails];
                        NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:IN_CUSTOMER_LOGIN_LOGOUT_KEY];
                        [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_LOGIN_LOGOUT_NOTIFICATION object:nil userInfo:dictionary];
                        [loginView setHidden:FALSE];
                        showStoreViewBtn.hidden = TRUE;
                        showOffersBtn.hidden    = TRUE;
                        [favStoreBtn setHidden:TRUE];
                        [myoffersTableView setHidden:TRUE];
                        [countHeaderView setHidden:TRUE];
                        [self setupMenuBarButtonItems];
                    }
                }else{
                    if([json  objectForKey:@"message"] != [NSNull null] && ![[json objectForKey:@"message"] isEqualToString:@""]){
                        NSString *message = [json objectForKey:@"message"];
                        [INUserDefaultOperations showSIAlertView:message];
                    }
                }
            }
            [myofferArray replaceObjectAtIndex:currentSelectedRow withObject:tempfeedObj];
            [myoffersTableView reloadData];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}

-(void)sendPostUnLikeRequest
{
    INFeedObj *tempfeedObj = (INFeedObj *)[myofferArray objectAtIndex:currentSelectedRow];
    
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    [httpClient setDefaultHeader:@"user_id" value:[INUserDefaultOperations getCustomerAuthId]];
    [httpClient setDefaultHeader:@"auth_id" value:[INUserDefaultOperations getCustomerAuthCode]];
    [httpClient deletePath:UNLIKE_BROADCAST_OF_STORE(tempfeedObj.postID) parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            if([json  objectForKey:@"message"] != [NSNull null] && [[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
                //                NSString* message = [json objectForKey:@"message"];
                //                [INUserDefaultOperations showAlert:message];
                tempfeedObj.user_like = @"0";
                if ([tempfeedObj.total_like intValue] > 0) {
                    tempfeedObj.total_like = [NSString stringWithFormat:@"%d",[tempfeedObj.total_like intValue] - 1];
                }
                [myofferArray replaceObjectAtIndex:currentSelectedRow withObject:tempfeedObj];
                [myoffersTableView reloadData];
            } else {
                NSString* code = [json objectForKey:@"code"];
                if([code isEqualToString:CUSTOMER_INVALID_USER_AUTH_CODE])
                {
                    [INUserDefaultOperations showSIAlertView:CUSTOMER_INVALID_USER_AUTH_MESSAGE];
                    if([INUserDefaultOperations isCustomerLoggedIn]){
                        [INUserDefaultOperations clearCustomerDetails];
                        NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:IN_CUSTOMER_LOGIN_LOGOUT_KEY];
                        [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_LOGIN_LOGOUT_NOTIFICATION object:nil userInfo:dictionary];
                        [loginView setHidden:FALSE];
                        showStoreViewBtn.hidden = TRUE;
                        showOffersBtn.hidden    = TRUE;
                        [favStoreBtn setHidden:TRUE];
                        [myoffersTableView setHidden:TRUE];
                        [countHeaderView setHidden:TRUE];
                        [self setupMenuBarButtonItems];
                    }
                }else{
                    if([json  objectForKey:@"message"] != [NSNull null] && ![[json objectForKey:@"message"] isEqualToString:@""]){
                        NSString *message = [json objectForKey:@"message"];
                        [INUserDefaultOperations showSIAlertView:message];
                    }
                }
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}

-(void)sendPostShareRequest:(NSString *)viaString{
    DebugLog(@"shareMsg %@ ",viaString);
    INFeedObj *tempfeedObj = (INFeedObj *)[myofferArray objectAtIndex:currentSelectedRow];
    
    if ([[INUserDefaultOperations getCustomerLoginFrom] isEqualToString:FACEBOOKSERVER] && [INUserDefaultOperations isOpenGraphShareEnabled] && ![viaString isEqualToString:@"facebook"]) {
        SIAlertView *sialertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Do you want to share this on your Facebook timeline?"];
        sialertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
        sialertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
        [sialertView addButtonWithTitle:@"YES"
                                   type:SIAlertViewButtonTypeDestructive
                                handler:^(SIAlertView *alert) {
                                    DebugLog(@"YES");
                                    NSString *deepLinkingUrl = [FacebookOpenGraphAPI createDeepLinkWithActiveAreaName:[IN_APP_DELEGATE getActiveSlugFromAreaTable] storeName:tempfeedObj.name storeId:[NSString stringWithFormat:@"%d",tempfeedObj.placeID]];
                                    NSString *postTitle         = tempfeedObj.title;
                                    NSString *postDescription   = tempfeedObj.description;
                                    NSString *postimageUrl      = tempfeedObj.image_url1;
                                    if (postimageUrl != nil && ![postimageUrl isEqualToString:@""]) {
                                        if ([postimageUrl hasPrefix:@"http"]) {
                                            [FacebookOpenGraphAPI shareContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:postimageUrl deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
                                        }else{
                                            [FacebookOpenGraphAPI shareContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:THUMBNAIL_LINK(postimageUrl) deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
                                        }
                                    }else{
                                        [FacebookOpenGraphAPI shareContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:@"" deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
                                    }
                                }];
        [sialertView addButtonWithTitle:@"NOT NOW"
                                   type:SIAlertViewButtonTypeCancel
                                handler:^(SIAlertView *alert) {
                                    DebugLog(@"NO");
                                }];
        [sialertView show];
    }
    
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    //    if ([INUserDefaultOperations isCustomerLoggedIn]) {
    //        [params setObject:[INUserDefaultOperations getCustomerAuthId] forKey:@"user_id"];
    //        [params setObject:[INUserDefaultOperations getCustomerAuthCode] forKey:@"auth_id"];
    //    }
    [params setObject:[NSString stringWithFormat:@"%d",tempfeedObj.postID] forKey:@"post_id"];
    [params setObject:viaString forKey:@"via"];
    
    [httpClient postPath:SHARE_STORE_POST parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            if([json  objectForKey:@"message"] != [NSNull null] && [[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
                //                NSString* message = [json objectForKey:@"message"];
                //                [INUserDefaultOperations showAlert:message];
                tempfeedObj.total_share = [NSString stringWithFormat:@"%d",[tempfeedObj.total_share intValue] + 1];
                [myofferArray replaceObjectAtIndex:currentSelectedRow withObject:tempfeedObj];
                [myoffersTableView reloadData];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}

-(NSString *)getPostDetailsMessageBody:(NSString *)shareVia{
    INFeedObj *tempfeedObj = (INFeedObj *)[myofferArray objectAtIndex:currentSelectedRow];
    
    NSMutableString *contactString = [[NSMutableString alloc] initWithString:@""];
    if (tempfeedObj.mob_no1 != nil && ![tempfeedObj.mob_no1 isEqualToString:@""]) {
        [contactString appendString:[NSString stringWithFormat:@"%@",tempfeedObj.mob_no1]];
    }
    if (tempfeedObj.mob_no2 != nil && ![tempfeedObj.mob_no2 isEqualToString:@""]) {
        if ([contactString isEqualToString:@""]) {
            [contactString appendString:tempfeedObj.mob_no2];
        }else{
            [contactString appendString:[NSString stringWithFormat:@", %@", tempfeedObj.mob_no2]];
        }
    }
    if (tempfeedObj.mob_no3 != nil && ![tempfeedObj.mob_no3 isEqualToString:@""]) {
        if ([contactString isEqualToString:@""]) {
            [contactString appendString:tempfeedObj.mob_no3];
        }else{
            [contactString appendString:[NSString stringWithFormat:@", %@", tempfeedObj.mob_no3]];
        }
    }
    if (tempfeedObj.tel_no1 != nil && ![tempfeedObj.tel_no1 isEqualToString:@""]) {
        if ([contactString isEqualToString:@""]) {
            [contactString appendString:tempfeedObj.tel_no1];
        }else{
            [contactString appendString:[NSString stringWithFormat:@", %@", tempfeedObj.tel_no1]];
        }
    }
    if (tempfeedObj.tel_no2 != nil && ![tempfeedObj.tel_no2 isEqualToString:@""]) {
        if ([contactString isEqualToString:@""]) {
            [contactString appendString:tempfeedObj.tel_no2];
        }else{
            [contactString appendString:[NSString stringWithFormat:@", %@", tempfeedObj.tel_no2]];
        }
    }
    if (tempfeedObj.tel_no3 != nil && ![tempfeedObj.tel_no3 isEqualToString:@""]) {
        if ([contactString isEqualToString:@""]) {
            [contactString appendString:tempfeedObj.tel_no3];
        }else{
            [contactString appendString:[NSString stringWithFormat:@", %@", tempfeedObj.tel_no3]];
        }
    }
    
    NSString *message = [IN_APP_DELEGATE getPostDetailsMessageBody:tempfeedObj.name offerTitle:tempfeedObj.title offerDescription:tempfeedObj.description isOffered:tempfeedObj.is_offered offerDateTime:tempfeedObj.offerdate contact:contactString shareVia:shareVia];
    return message;
}

-(NSIndexPath *)getIndexpathOfEvent:(id)event
{
    NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:myoffersTableView];
	NSIndexPath *indexPath = [myoffersTableView indexPathForRowAtPoint: currentTouchPosition];
    return indexPath;
}

- (void)callBtnTapped:(id)sender event:(id)event
{
    NSIndexPath *indexPath = [self getIndexpathOfEvent:event];
    if (indexPath != nil)
    {
        INFeedObj *lfeedObj  = [myofferArray objectAtIndex:indexPath.row];
        DebugLog(@"place %@",lfeedObj.name);
        if (lfeedObj.name == nil || [lfeedObj.name length] == 0)
        {
            lblcallModalHeader.text =  @"Select number";
             selectedPlaceName = @"";
        }
        else {
            lblcallModalHeader.text = lfeedObj.name;
            selectedPlaceName = lfeedObj.name;
        }
        
        selectedPostId = lfeedObj.postID;
        if (telArray == nil) {
            telArray = [[NSMutableArray alloc] init];
        }else{
            [telArray removeAllObjects];
        }
        if (lfeedObj.tel_no1 != nil && ![lfeedObj.tel_no1 isEqualToString:@""]) {
            [telArray addObject:lfeedObj.tel_no1];
        }
        if (lfeedObj.tel_no2 != nil && ![lfeedObj.tel_no2 isEqualToString:@""]) {
            [telArray addObject:lfeedObj.tel_no2];
        }
        if (lfeedObj.tel_no3 != nil && ![lfeedObj.tel_no3 isEqualToString:@""]) {
            [telArray addObject:lfeedObj.tel_no3];
        }
        if (lfeedObj.mob_no1 != nil && ![lfeedObj.mob_no1 isEqualToString:@""]) {
            [telArray addObject:[NSString stringWithFormat:@"+%@",lfeedObj.mob_no1]];
        }
        if (lfeedObj.mob_no2 != nil && ![lfeedObj.mob_no2 isEqualToString:@""]) {
            [telArray addObject:[NSString stringWithFormat:@"+%@",lfeedObj.mob_no2]];
        }
        if (lfeedObj.mob_no3 != nil && ![lfeedObj.mob_no3 isEqualToString:@""]) {
            [telArray addObject:[NSString stringWithFormat:@"+%@",lfeedObj.mob_no3]];
        }
        [callModalTableView reloadData];
        if (callModalTableView.contentSize.height < 240) {
            [callModalTableView setFrame:CGRectMake(callModalTableView.frame.origin.x, callModalTableView.frame.origin.y, callModalTableView.frame.size.width, callModalTableView.contentSize.height)];
        }else{
            [callModalTableView setFrame:CGRectMake(callModalTableView.frame.origin.x, callModalTableView.frame.origin.y, callModalTableView.frame.size.width, 240)];
        }
        [callModalView setFrame:CGRectMake(callModalView.frame.origin.x, callModalView.frame.origin.y, callModalView.frame.size.width, callModalTableView.frame.size.height+85)];

        DebugLog(@"temparray %@",telArray);
        if (telArray != nil && telArray.count > 0) {
            [callModalViewBackView setHidden:NO];
            [callModalView setHidden:NO];
            [CommonCallback viewtransitionInCompletion:callModalView completion:^{
                [CommonCallback viewtransitionOutCompletion:callModalView completion:nil];
            }];
            
            NSDictionary *callParams = [NSDictionary dictionaryWithObjectsAndKeys:@"MyShoplocal",@"Source",[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"AreaName",selectedPlaceName,@"StoreName", nil];
            [INEventLogger logEvent:@"StoreCall" withParams:callParams];
            
            [self sendStoreCallRequest:@"StoreCall"];
        } else {
            [INUserDefaultOperations showSIAlertView:NO_CONTACT_MESSAGE];
        }
    }
}

- (IBAction)btnCancelcallModalPressed:(id)sender {
    [callModalView setHidden:YES];
    [callModalViewBackView setHidden:YES];
}

-(void)sendStoreCallRequest:(NSString *)callType
{
    
    if (![INUserDefaultOperations isCustomerLoggedIn]) {
        return;
    }
    
    NSString *areaID = [IN_APP_DELEGATE getActiveShoplocalPlaceIdFromAreaTable];
    
    DebugLog(@"========================sendStoreCallRequest========================");
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSString *register_from = [NSString stringWithFormat:@"%@ v%@",DEVICE_TYPE,version];
    
    NSMutableDictionary *postparams = [[NSMutableDictionary alloc] init];
    //    {"user_id":"${user_id}","auth_id":"${auth_id}","place_id":"106","via":"welcome ","call_type":"type-a","active_area":"1"}
    [postparams setObject:[INUserDefaultOperations getCustomerAuthId] forKey:@"user_id"];
    [postparams setObject:[INUserDefaultOperations getCustomerAuthCode] forKey:@"auth_id"];
    [postparams setObject:[NSString stringWithFormat:@"%d",selectedPostId] forKey:@"post_id"];
    [postparams setObject:callType forKey:@"call_type"];
    [postparams setObject:areaID forKey:@"active_area"];
    [postparams setObject:register_from forKey:@"via"];
    
    DebugLog(@"sendStoreCallRequest : postParams -%@-",postparams);
    
    [httpClient postPath:CALL_STORE parameters:postparams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"sendStoreCallRequest : json afnetworking ->%@",json);
        if (json!= nil) {
            NSString* code = [json objectForKey:@"code"];
            if([code isEqualToString:CUSTOMER_INVALID_USER_AUTH_CODE])
            {
                if([INUserDefaultOperations isCustomerLoggedIn]){
                    [INUserDefaultOperations clearCustomerDetails];
                }
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"sendStoreCallRequest : [HTTPClient Error]: %@", error.localizedDescription);
    }];
}
@end
