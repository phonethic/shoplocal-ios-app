//
//  INDetailViewController.h
//  shoplocal
//
//  Created by Sagar Mody on 11/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "FXImageView.h"
#import "MWPhotoBrowser.h"
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import "FaceBookShareViewController.h"
#import "INCustomerLoginViewController.h"

@class INStoreDetailObj;
@class INBroadcastDetailObj;
@interface INDetailViewController : UIViewController <iCarouselDataSource, iCarouselDelegate, MWPhotoBrowserDelegate,UIActionSheetDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,FacebookProtocolDelegate,INCustomerLoginViewControllerDelegate>
{
    INStoreDetailObj *storeObj;
    INBroadcastDetailObj *broadcastObj;
    MBProgressHUD *hud;
}

@property (strong, nonatomic) NSMutableArray *storeArray;
@property (strong, nonatomic) NSMutableArray *galleryArray;
@property (strong, nonatomic) NSMutableArray *broadcastArray;
@property (nonatomic, readwrite) NSInteger storeId;
@property (nonatomic, copy) NSString *storeImageUrl;

@property (strong, nonatomic) IBOutlet UILabel *emptyBroadcastlbl;
@property (strong, nonatomic) NSMutableArray *photos;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet iCarousel *carousel;

@property (strong, nonatomic) IBOutlet UITextView *descriptionTextView;

@property (strong, nonatomic) IBOutlet UIView *actionView;
@property (strong, nonatomic) IBOutlet UIButton *callStoreBtn;
@property (strong, nonatomic) IBOutlet UIButton *likeStore;
@property (strong, nonatomic) IBOutlet UIButton *shareStoreBtn;
@property (strong, nonatomic) IBOutlet UILabel *lblCallStore;
@property (strong, nonatomic) IBOutlet UILabel *lblAddfav;
@property (strong, nonatomic) IBOutlet UILabel *lblShareStore;

@property (strong, nonatomic) IBOutlet UIButton *addressBtn;
@property (strong, nonatomic) IBOutlet UIScrollView *addressView;

@property (strong, nonatomic) IBOutlet UILabel *address;
@property (strong, nonatomic) IBOutlet UILabel *citylbl;
@property (strong, nonatomic) IBOutlet UILabel *lblstoretime;
@property (strong, nonatomic) IBOutlet UILabel *lblstoreClosed;
@property (strong, nonatomic) IBOutlet UILabel *lblemail;
@property (strong, nonatomic) IBOutlet UILabel *lblwebsite;
@property (strong, nonatomic) IBOutlet UILabel *lblfacebook;
@property (strong, nonatomic) IBOutlet UILabel *lbltwitter;



@property (strong, nonatomic) IBOutlet UIButton *broadcastBtn;
@property (strong, nonatomic) IBOutlet UITableView *broadcastTableView;

@property (strong, nonatomic) IBOutlet UIImageView *imgSeperator1;
@property (strong, nonatomic) IBOutlet UIImageView *imgSeperator2;
@property (strong, nonatomic) IBOutlet UIImageView *imgSeperator3;
@property (strong, nonatomic) IBOutlet UIImageView *imgSeperator4;

@property (strong, nonatomic) IBOutlet UIImageView *addressDropDownImageView;
@property (strong, nonatomic) IBOutlet UIImageView *broadcastDropDownImageView;
@property (strong, nonatomic) IBOutlet UIButton *getDirectionBtn;
@property (strong, nonatomic) IBOutlet UIImageView *mapImageView;

@property (copy, nonatomic) NSMutableArray *telArray;
@property (nonatomic, copy) NSString *selectedTelNumberToCall;

- (IBAction)addressBtnPressed:(id)sender;
- (IBAction)broadcastBtnPressed:(id)sender;

- (IBAction)callStoreBtnPressed:(id)sender;
- (IBAction)likeStoreBtnPressed:(id)sender;
- (IBAction)shareStoreBtnPressed:(id)sender;

- (IBAction)getDirectionBtnPressed:(id)sender;

@end
