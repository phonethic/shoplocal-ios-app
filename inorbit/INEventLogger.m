//
//  INEventLogger.m
//  shoplocal
//
//  Created by Rishi on 13/12/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INEventLogger.h"
#import "Flurry.h"

@implementation INEventLogger


+(void)logEvent:(NSString*) eventName
{
    [Flurry logEvent:eventName];
    [[LocalyticsSession shared] tagEvent:eventName];
}

+(void)logEvent:(NSString*) eventName  withParams:(NSDictionary*) eventParams
{
    [Flurry logEvent:eventName withParameters:eventParams];
    [[LocalyticsSession shared] tagEvent:eventName attributes:eventParams];
}

+(void)setUser:(NSString*) eventName
{
    [Flurry setUserID:eventName];
}

+(void)setGender:(NSString*) gender
{
    [Flurry setGender:gender];
}

+(void)setAge:(int) age
{
    [Flurry setAge:age];
}

+(void)logPageViews:(id)navigationController
{
    [Flurry logAllPageViews:navigationController];
}

+(void)logLocationValues:(CLLocation *)location
{
    [Flurry setLatitude:location.coordinate.latitude
              longitude:location.coordinate.longitude
     horizontalAccuracy:location.horizontalAccuracy
       verticalAccuracy:location.verticalAccuracy];
}

+ (void)localyticsSessionWillResignActive
{
    [[LocalyticsSession shared] close];
    [[LocalyticsSession shared] upload];
}

+ (void)localyticsSessionDidEnterBackground
{
    [[LocalyticsSession shared] close];
    [[LocalyticsSession shared] upload];
}

+ (void)localyticsSessionWillEnterForeground
{
    [[LocalyticsSession shared] resume];
    [[LocalyticsSession shared] upload];
}

+ (void)localyticsSessionDidBecomeActive:(NSString *)appId
{
    [[LocalyticsSession shared] LocalyticsSession:appId];
    //[[LocalyticsSession shared] setLoggingEnabled:YES]; // Used while testing
    [[LocalyticsSession shared] resume];
    [[LocalyticsSession shared] upload];
}

+ (void)localyticsSessionWillTerminate
{
    [[LocalyticsSession shared] close];
    [[LocalyticsSession shared] upload];
}

@end
