//
//  INStoreDetailObj.h
//  shoplocal
//
//  Created by Sagar Mody on 11/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface INStoreDetailObj : NSObject
{}
@property (nonatomic, readwrite) NSInteger ID;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, readwrite) NSInteger place_parent;
@property (nonatomic, copy) NSString *place_status;
@property (nonatomic, copy) NSString *published;
@property (nonatomic, copy) NSString *description;
@property (nonatomic, copy) NSString *building;
@property (nonatomic, copy) NSString *street;
@property (nonatomic, copy) NSString *landmark;
@property (nonatomic, copy) NSString *area;
@property (nonatomic, copy) NSString *pincode;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *country;
@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, copy) NSString *longitude;
@property (nonatomic, copy) NSString *tel_no1;
@property (nonatomic, copy) NSString *tel_no2;
@property (nonatomic, copy) NSString *tel_no3;
@property (nonatomic, copy) NSString *mob_no1;
@property (nonatomic, copy) NSString *mob_no2;
@property (nonatomic, copy) NSString *mob_no3;
@property (nonatomic, copy) NSString *fax_no1;
@property (nonatomic, copy) NSString *fax_no2;
@property (nonatomic, copy) NSString *toll_no1;
@property (nonatomic, copy) NSString *toll_no2;
@property (nonatomic, copy) NSString *open_time;
@property (nonatomic, copy) NSString *close_time;
@property (nonatomic, copy) NSString *image_url;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *website;
@property (nonatomic, copy) NSString *total_like;
@property (nonatomic, copy) NSString *total_share;
@property (nonatomic, copy) NSString *total_view;
@property (nonatomic, copy) NSString *user_like;
@property (nonatomic, copy) NSString *fb_url;
@property (nonatomic, copy) NSString *twitter_url;
@property (nonatomic, copy) NSString *store_close_days;
@end
