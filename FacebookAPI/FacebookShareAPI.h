//
//  FacebookShareAPI.h
//  shoplocal
//
//  Created by Kirti Nikam on 08/05/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FacebookOpenGraphAPI.h"

@interface FacebookShareAPI : NSObject
+(void)shareContentWithTitle:(NSString *)title description:(NSString *)ldescription image:(NSString *)image deeplinkingURL:(NSString *)url objectType:(NSString *)objectType;
+(void)shareContentWithTitle:(NSString *)ltitle subTitle:(NSString *)lsubTitle description:(NSString *)ldescription image:(NSString *)limage deeplinkingURL:(NSString *)lurl objectType:(NSString *)objectType;
+ (NSDictionary*)parseURLParams:(NSString *)query;
@end
