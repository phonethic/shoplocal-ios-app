//
//  INMerchantReportsViewController.m
//  inorbit
//
//  Created by Kirti Nikam on 05/09/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INMerchantReportsViewController.h"
#import "INAppDelegate.h"
#import "CommonCallback.h"
#import "INBroadcastDetailObj.h"
#import "INAllPostsViewController.h"

#define GET_CUSTOMER_COUNT(ID) [NSString stringWithFormat:@"%@%@%@user_api/count?place_id=%d",LIVE_SERVER,URL_PREFIX,API_VERSION,ID]
#define STORE_DETAILS(ID) [NSString stringWithFormat:@"%@%@%@place_api/places?place_id=%d",LIVE_SERVER,URL_PREFIX,API_VERSION,ID]
#define STORE_BROADCAST(ID) [NSString stringWithFormat:@"%@%@%@broadcast_api/broadcasts?place_id=%d",LIVE_SERVER,URL_PREFIX,API_VERSION,ID]
#define THUMBNAIL_LINK(IMG_LINK) [NSString stringWithFormat:@"%@%@%@",LIVE_SERVER,URL_PREFIX,IMG_LINK]

@interface INMerchantReportsViewController ()
@property(strong) NSDictionary *splashJson;
@end

@implementation INMerchantReportsViewController
@synthesize placeId;
@synthesize lblCustomerCount,lblViewsCount,lblLikesCount,lblShareCount;
@synthesize lblCustomerCountHeader,lblLikesCountHeader,lblShareCountHeader,lblViewsCountHeader;
@synthesize postDetailReportBtn;
@synthesize reportView;

@synthesize splashJson;
@synthesize broadcastArray;
@synthesize storeName;
@synthesize infoBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                             style:UIBarButtonItemStylePlain
                                                            target:nil
                                                            action:nil];
    [[self navigationItem] setBackBarButtonItem:back];
    
    lblCustomerCount.text = @"0";
    lblViewsCount.text = @"0";
    lblLikesCount.text = @"0";
    lblShareCount.text = @"0";
    
    [self setUI];
    
    lblCustomerCount.userInteractionEnabled = YES;
    lblViewsCount.userInteractionEnabled = YES;
    lblLikesCount.userInteractionEnabled = YES;
    lblShareCount.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *customerCountviewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    customerCountviewTap.numberOfTapsRequired = 1;
    customerCountviewTap.cancelsTouchesInView = NO;
    [lblCustomerCount  addGestureRecognizer:customerCountviewTap];
    
    UITapGestureRecognizer *ViewsCountviewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    ViewsCountviewTap.numberOfTapsRequired = 1;
    ViewsCountviewTap.cancelsTouchesInView = NO;
    [lblViewsCount  addGestureRecognizer:ViewsCountviewTap];
    
    UITapGestureRecognizer *LikesCountviewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    LikesCountviewTap.numberOfTapsRequired = 1;
    LikesCountviewTap.cancelsTouchesInView = NO;
    [lblLikesCount  addGestureRecognizer:LikesCountviewTap];
    
    UITapGestureRecognizer *ShareCountviewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    ShareCountviewTap.numberOfTapsRequired = 1;
    ShareCountviewTap.cancelsTouchesInView = NO;
    [lblShareCount  addGestureRecognizer:ShareCountviewTap];
    
    broadcastArray = [[NSMutableArray alloc] init];

    
    if ([IN_APP_DELEGATE networkavailable]) {
        [self sendGetCustomersCountRequest];
        [self sendGetViewsAndLikesCountRequest];
    }else{
         [INUserDefaultOperations showOfflineAlert];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setLblCustomerCount:nil];
    [self setLblViewsCount:nil];
    [self setLblLikesCount:nil];
    [self setReportView:nil];
    [self setLblCustomerCountHeader:nil];
    [self setLblViewsCountHeader:nil];
    [self setLblLikesCountHeader:nil];
    [self setLblShareCount:nil];
    [self setLblShareCountHeader:nil];
    [self setPostDetailReportBtn:nil];
    [self setInfoBtn:nil];
    [super viewDidUnload];
}
#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma Gesture Methods
- (void)tapDetected:(UIGestureRecognizer *)sender {
    if (sender.view==lblCustomerCount) {
        [self customerCountBtnPressed:nil];
    } else if (sender.view==lblViewsCount) {
        [self viewsCountBtnPressed:nil];
    } else if (sender.view==lblLikesCount) {
        [self likesCountBtnPressed:nil];
    } else if (sender.view==lblShareCount) {
        [self shareCountBtnPressed:nil];
    }
}

#pragma internal methods
-(void)setUI
{
    //reportView = [CommonCallback setViewPropertiesWithRoundedCorner:reportView];
    reportView.backgroundColor =  [UIColor whiteColor];
    reportView.clipsToBounds = YES;
    
    lblCustomerCount.font       =   DEFAULT_BOLD_FONT(40.0);
    lblLikesCount.font          =   DEFAULT_BOLD_FONT(40.0);
    lblViewsCount.font          =   DEFAULT_BOLD_FONT(40.0);
    lblShareCount.font          =   DEFAULT_BOLD_FONT(40.0);
    lblCustomerCountHeader.font =   DEFAULT_FONT(14.0);
    lblLikesCountHeader.font    =   DEFAULT_FONT(14.0);
    lblViewsCountHeader.font    =   DEFAULT_FONT(14.0);
    lblShareCountHeader.font    =   DEFAULT_FONT(14.0);
    
    lblCustomerCount.adjustsFontSizeToFitWidth = YES;
    lblLikesCount.adjustsFontSizeToFitWidth = YES;
    lblViewsCount.adjustsFontSizeToFitWidth = YES;
    lblShareCount.adjustsFontSizeToFitWidth = YES;
    
    lblCustomerCount.backgroundColor    =   [UIColor clearColor];
    lblLikesCount.backgroundColor       =   [UIColor clearColor];
    lblViewsCount.backgroundColor       =   [UIColor clearColor];
    lblShareCount.backgroundColor       =   [UIColor clearColor];
    lblCustomerCountHeader.backgroundColor  =   [UIColor clearColor];
    lblLikesCountHeader.backgroundColor     =   [UIColor clearColor];
    lblViewsCountHeader.backgroundColor     =   [UIColor clearColor];
    lblShareCountHeader.backgroundColor     =   [UIColor clearColor];
    
    lblCustomerCount.textColor  =   BROWN_COLOR;
    lblLikesCount.textColor     =   BROWN_COLOR;
    lblViewsCount.textColor     =   BROWN_COLOR;
    lblShareCount.textColor     =   BROWN_COLOR;
    lblCustomerCountHeader.textColor    =   BROWN_COLOR;
    lblLikesCountHeader.textColor       =   BROWN_COLOR;
    lblViewsCountHeader.textColor       =   BROWN_COLOR;
    lblShareCountHeader.textColor       =   BROWN_COLOR;
    
    postDetailReportBtn.titleLabel.font   =   DEFAULT_BOLD_FONT(16.0);
    postDetailReportBtn.backgroundColor   =   [UIColor clearColor];
    [postDetailReportBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [postDetailReportBtn setTitleColor:[UIColor colorWithWhite:0.5 alpha:0.5] forState:UIControlStateHighlighted];
}


#pragma AFNetworking callbacks
-(void)sendGetCustomersCountRequest{
    DebugLog(@"========================sendGetCustomersCountRequest========================");
    DebugLog(@"user_id %@ auth_id %@",[INUserDefaultOperations getMerchantAuthId],[INUserDefaultOperations getMerchantAuthCode]);
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:GET_CUSTOMER_COUNT(self.placeId)]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setValue:[INUserDefaultOperations getMerchantAuthId] forHTTPHeaderField:@"merchant_id"];
    [urlRequest setValue:[INUserDefaultOperations getMerchantAuthCode] forHTTPHeaderField:@"auth_id"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"GetCustomersCount json : %@",splashJson);
                                                        if (self.splashJson != nil) {
                                                            if ([[self.splashJson objectForKey:@"success"] boolValue] == 1) {
                                                                NSDictionary *maindict = [self.splashJson  objectForKey:@"data"];
                                                                NSString *countString = [maindict objectForKey:@"customer_count"];
                                                                lblCustomerCount.text = countString;
                                                            } else if(self.splashJson != nil && [[self.splashJson  objectForKey:@"success"] isEqualToString:@"false"]) {
                                                                if([self.splashJson  objectForKey:@"code"] != nil &&  [[self.splashJson  objectForKey:@"code"] isEqualToString:MERCHANT_INVALID_USER_AUTH_CODE])
                                                                {
                                                                    if([INUserDefaultOperations isMerchantLoggedIn]){
                                                                        [INUserDefaultOperations showAlert:MERCHANT_INVALID_USER_AUTH_MESSAGE];
                                                                        [INUserDefaultOperations clearMerchantDetails];
                                                                        [self performSelector:@selector(popReportsController) withObject:nil afterDelay:2.0];
                                                                    }

                                                                } else {
                                                                    [INUserDefaultOperations showAlert:[self.splashJson  objectForKey:@"message"]];
                                                                }
                                                            }
                                                        }
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                        [av show];
                                                    }];
    [operation start];
}

-(void)sendGetViewsAndLikesCountRequest
{
    DebugLog(@"========================sendGetViewsAndLikesCountRequest========================");
    DebugLog(@"%@",STORE_DETAILS(self.placeId));
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:STORE_DETAILS(self.placeId)]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setValue:[INUserDefaultOperations getMerchantAuthId] forHTTPHeaderField:@"user_id"];
    [urlRequest setValue:[INUserDefaultOperations getMerchantAuthCode] forHTTPHeaderField:@"auth_id"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"GetViewsAndLikes -%@-", self.splashJson);
                                                        if(self.splashJson  != nil) {
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSArray* jsonArray = [self.splashJson  objectForKey:@"data"];
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *maindict = [jsonArray  objectAtIndex:index];
                                                                    NSString *viewscountString = [maindict objectForKey:@"total_view"];
                                                                    NSString *likescountString = [maindict objectForKey:@"total_like"];
                                                                    NSString *sharecountString = [maindict objectForKey:@"total_share"];

                                                                    lblViewsCount.text = viewscountString;
                                                                    lblLikesCount.text = likescountString;
                                                                    lblShareCount.text = sharecountString;

                                                                }
                                                            } else if(self.splashJson != nil && [[self.splashJson  objectForKey:@"success"] isEqualToString:@"false"]) {
                                                                if([self.splashJson  objectForKey:@"code"] != nil &&  [[self.splashJson  objectForKey:@"code"] isEqualToString:MERCHANT_INVALID_USER_AUTH_CODE])
                                                                {
                                                                    if([INUserDefaultOperations isMerchantLoggedIn]){
                                                                        [INUserDefaultOperations showAlert:MERCHANT_INVALID_USER_AUTH_MESSAGE];
                                                                        [INUserDefaultOperations clearMerchantDetails];
                                                                        [self performSelector:@selector(popReportsController) withObject:nil afterDelay:2.0];                                                                }
                                                                    
                                                                } else {
                                                                    [INUserDefaultOperations showAlert:[self.splashJson  objectForKey:@"message"]];
                                                                }
                                                            }
                                                        }
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                        [av show];
                                                    }];
    [operation start];
}

- (IBAction)postDetailReportBtnPressed:(id)sender {
    DebugLog(@"placeid--->%d",self.placeId);
    INAllPostsViewController *allpostController = [[INAllPostsViewController alloc] initWithNibName:@"INAllPostsViewController" bundle:nil];
    allpostController.title = @"Store Broadcasts";
    allpostController.storeId = placeId;
    allpostController.storeName = storeName;
    [self.navigationController pushViewController:allpostController animated:YES];
}

- (IBAction)infoBtnPressed:(id)sender {
    NSString *message = @"Reach : Total number of shoplocal users in your area.\n\nVisits : Number of times your store details were seen.\n\nFavourites : People who have marked your store as a favourite and follow your updates.\n\nShares : Number of times your store was shared by people on social media.\n";
    
    SIAlertView *sialertView1 = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:message];
    sialertView1.transitionStyle = SIAlertViewTransitionStyleDropDown;
    sialertView1.backgroundStyle = SIAlertViewBackgroundStyleGradient;
    [sialertView1 addButtonWithTitle:@"OK"
                                type:SIAlertViewButtonTypeCancel
                             handler:^(SIAlertView *alert) {
                                 DebugLog(@"OK Clicked");
                             }];
    [sialertView1 show];
}


- (void)customerCountBtnPressed:(id)sender {
    NSString *message = @"Reach : Total number of shoplocal users in your area.";
    
    SIAlertView *sialertView1 = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:message];
    sialertView1.transitionStyle = SIAlertViewTransitionStyleDropDown;
    sialertView1.backgroundStyle = SIAlertViewBackgroundStyleGradient;
    [sialertView1 addButtonWithTitle:@"OK"
                                type:SIAlertViewButtonTypeCancel
                             handler:^(SIAlertView *alert) {
                                 DebugLog(@"OK Clicked");
                             }];
    [sialertView1 show];
}

- (void)viewsCountBtnPressed:(id)sender {
    NSString *message = @"Visits : Number of times your store details were seen.";
    
    SIAlertView *sialertView1 = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:message];
    sialertView1.transitionStyle = SIAlertViewTransitionStyleDropDown;
    sialertView1.backgroundStyle = SIAlertViewBackgroundStyleGradient;
    [sialertView1 addButtonWithTitle:@"OK"
                                type:SIAlertViewButtonTypeCancel
                             handler:^(SIAlertView *alert) {
                                 DebugLog(@"OK Clicked");
                             }];
    [sialertView1 show];
}

- (void)likesCountBtnPressed:(id)sender {
    NSString *message = @"Favourites : People who have marked your store as a favourite and follow your updates.";
    
    SIAlertView *sialertView1 = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:message];
    sialertView1.transitionStyle = SIAlertViewTransitionStyleDropDown;
    sialertView1.backgroundStyle = SIAlertViewBackgroundStyleGradient;
    [sialertView1 addButtonWithTitle:@"OK"
                                type:SIAlertViewButtonTypeCancel
                             handler:^(SIAlertView *alert) {
                                 DebugLog(@"OK Clicked");
                             }];
    [sialertView1 show];
}

- (void)shareCountBtnPressed:(id)sender {
    NSString *message = @"Shares : Number of times your store was shared by people on social media.";
    
    SIAlertView *sialertView1 = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:message];
    sialertView1.transitionStyle = SIAlertViewTransitionStyleDropDown;
    sialertView1.backgroundStyle = SIAlertViewBackgroundStyleGradient;
    [sialertView1 addButtonWithTitle:@"OK"
                                type:SIAlertViewButtonTypeCancel
                             handler:^(SIAlertView *alert) {
                                 DebugLog(@"OK Clicked");
                             }];
    [sialertView1 show];
}

-(void) popReportsController
{
    DebugLog(@"popToRootViewControllerAnimated:YES");
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
