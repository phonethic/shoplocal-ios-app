//
//  INShareThisAppViewController.m
//  shoplocal
//
//  Created by Kirti Nikam on 07/01/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//
#import <Social/Social.h>

#import "INShareThisAppViewController.h"
#import "INAppDelegate.h"
#import "CommonCallback.h"
#import "FacebookShareAPI.h"

//#define OLD_SHARE_MESSAGE_BODY_TEXT @"Hea, I have found a cool app Shoplocal - http://shoplocal.co.in/download - Why don't you try and experience it yourself."
#define SHOLOCAL_APP_DOWNLOAD_URL @"http://shoplocal.co.in/download"
#define SOCIAL_SHARE_MESSAGE_BODY_TEXT @"I'm using Shoplocal to find local business near me. Give it a spin? (http://shoplocal.co.in/download)"
#define EMAIL_SHARE_MESSAGE_BODY_TEXT @"Hi, I want you to try Shoplocal Mobile app. Just click on the link and install the mobile app to find local business near you. (http://shoplocal.co.in/download)"
#define TEXT_SHARE_MESSAGE_BODY_TEXT @"Hey, i want you to try Shoplocal, the best way to find local business near you. (http://shoplocal.co.in/download)"


#define WHATSAPP @"WhatsApp"
#define FACEBOOK @"Facebook"
#define TWITTER  @"Twitter"
#define EMAIL    @"Email"
#define SMS      @"SMS"


@interface INShareThisAppViewController ()

@end

@implementation INShareThisAppViewController
@synthesize fbBtn,twitterBtn,emailBtn,smsBtn,whtsAppBtn;
@synthesize lblCollection,lblEmail,lblfb,lblsms,lblTwitter,lblwhtsApp;
@synthesize shareTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:TRUE];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:TRUE];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    [self setupMenuBarButtonItems];
    
    [self setUI];
    
    shareArray = [[NSMutableArray alloc] init];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"whatsapp://"]]) {
        [shareArray addObject:WHATSAPP];
    }
    
    [shareArray addObject:FACEBOOK];
    [shareArray addObject:TWITTER];
    [shareArray addObject:EMAIL];
    
    if ([MFMessageComposeViewController canSendText]) {
         [shareArray addObject:SMS];
    } 
    
//    if (![MFMessageComposeViewController canSendText]) {
//        [smsBtn setHidden:YES];
//        [lblsms setHidden:YES];
//    }
//    
//    if (![[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"whatsapp://"]]) {
//        [whtsAppBtn setHidden:YES];
//        [lblwhtsApp setHidden:YES];
//    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setFbBtn:nil];
    [self setLblfb:nil];
    [self setTwitterBtn:nil];
    [self setLblTwitter:nil];
    [self setEmailBtn:nil];
    [self setLblEmail:nil];
    [self setWhtsAppBtn:nil];
    [self setLblwhtsApp:nil];
    [self setSmsBtn:nil];
    [self setLblsms:nil];
    [self setLblCollection:nil];
    [super viewDidUnload];
}

#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma set Navigation barbuttons callback
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:MENU_TITLE style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithTitle:MENU_TITLE style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleRightSideMenu)];
    return nil;
}
- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)setUI{

    for(UILabel *lbl in lblCollection)
    {
        lbl.font            = DEFAULT_FONT(18.0);
        lbl.textColor       = [UIColor whiteColor];
        lbl.numberOfLines   = 2;
        lbl.textAlignment   = NSTextAlignmentCenter;
    }
}

- (IBAction)fbBtnPressed:(id)sender {
    if ([IN_APP_DELEGATE networkavailable]) {
        NSString *message = SOCIAL_SHARE_MESSAGE_BODY_TEXT;
        DebugLog(@"message %@",message);
        [FacebookShareAPI shareContentWithTitle:ALERT_TITLE description:message image:@"" deeplinkingURL:SHOLOCAL_APP_DOWNLOAD_URL objectType:OBJECT_TYPE_NONE];
    }else{
        [INUserDefaultOperations showOfflineAlert];
    }
//    if ([IN_APP_DELEGATE networkavailable]) {
//        if (SYSTEM_VERSION_LESS_THAN(@"6.0"))
//        {
//            FaceBookShareViewController *fbShareComposer = [[FaceBookShareViewController alloc] initWithNibName:@"FaceBookShareViewController" bundle:nil];
//            fbShareComposer.fbdelegate = self;
//            fbShareComposer.title = @"FACEBOOK SHARE";
//            fbShareComposer.FBtitle = ALERT_TITLE;
//            //    fbShareComposer.FBtLink = SHOPLOCAL_APPSTORE_URL;
//            fbShareComposer.postMessageText = SOCIAL_SHARE_MESSAGE_BODY_TEXT;
//            UINavigationController *fbnavBar=[[UINavigationController alloc]initWithRootViewController:fbShareComposer];
//            [self.navigationController presentViewController:fbnavBar animated:YES completion:nil];
//        }else{
//            if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
//                
//                SLComposeViewController *socialComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
//                [socialComposer setInitialText:SOCIAL_SHARE_MESSAGE_BODY_TEXT];
//    //                [socialComposer addURL:[NSURL URLWithString:@"http://inorbitapp.in/download/"]];
//                [socialComposer addImage:[UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"]];
//                [socialComposer setCompletionHandler:^(SLComposeViewControllerResult result){
//                    switch (result) {
//                        case SLComposeViewControllerResultCancelled:
//                            DebugLog(@"SLComposeViewControllerResultCancelled");
//                            break;
//                        case SLComposeViewControllerResultDone:
//                            DebugLog(@"SLComposeViewControllerResultDone");
//                            break;
//                        default:
//                            DebugLog(@"SLComposeViewControllerResultFailed");
//                            break;
//                    }
//                    [self dismissViewControllerAnimated:YES completion:NULL];
//                }];
//                [self presentViewController:socialComposer animated:YES completion:Nil];
//            }
//            else{
//                UIAlertView *alertView = [[UIAlertView alloc]
//                                          initWithTitle:ALERT_TITLE
//                                          message:@"You may not have set up facebook service on your device.Please check and try again."
//                                          delegate:nil
//                                          cancelButtonTitle:@"OK"
//                                          otherButtonTitles:nil];
//                [alertView show];
//            }
//        }
//    }else{
//        [INUserDefaultOperations showOfflineAlert];
//    }
}

-(void)faceBookCompletionCallBack:(int)result
{
    DebugLog(@"Customer : faceBookCompletionCallBack %d",result);
    if (result == FACEBOOK_POST_SUCCESS) {
        DebugLog(@"Customer : FACEBOOK_POST_SUCCESS");
    }
}

- (IBAction)twitterBtnPressed:(id)sender {
    if ([IN_APP_DELEGATE networkavailable]) {
        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
            
            SLComposeViewController *socialComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
            [socialComposer setInitialText:SOCIAL_SHARE_MESSAGE_BODY_TEXT];
            [socialComposer setCompletionHandler:^(SLComposeViewControllerResult result){
                switch (result) {
                    case SLComposeViewControllerResultCancelled:
                        DebugLog(@"SLComposeViewControllerResultCancelled");
                        break;
                    case SLComposeViewControllerResultDone:
                        DebugLog(@"SLComposeViewControllerResultDone");
                        break;
                    default:
                        DebugLog(@"SLComposeViewControllerResultFailed");
                        break;
                }
                [self dismissViewControllerAnimated:YES completion:NULL];
            }];
            [self presentViewController:socialComposer animated:YES completion:Nil];
        }
        else{
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:ALERT_TITLE
                                      message:@"You may not have set up twitter service on your device.Please check and try again."
                                      delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [alertView show];
        }
    }else{
        [INUserDefaultOperations showOfflineAlert];
    }
    
}

- (IBAction)emailBtnPressed:(id)sender {
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
        mailComposer.mailComposeDelegate = self;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            mailComposer.navigationBar.tintColor  = [UIColor whiteColor];
        }
        [mailComposer setToRecipients:nil];
        [mailComposer setMessageBody:EMAIL_SHARE_MESSAGE_BODY_TEXT isHTML:NO];
        [self presentViewController:mailComposer animated:YES completion:nil];
    }else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"You may not have set up mail service on your device or You may not connected to internet.\nPlease check and try again."
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}
#pragma mark Mail Compose Delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    if (result == MFMailComposeResultFailed) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                        message:NSLocalizedString(@"Email failed to send. Please try again.", nil)
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", nil) otherButtonTitles:nil];
		[alert show];
    }else if (result == MFMailComposeResultSent)
    {
        DebugLog(@"Mail Sent");
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)smsBtnPressed:(id)sender {
    if ([MFMessageComposeViewController canSendText]) {
        MFMessageComposeViewController *messageComposer = [[MFMessageComposeViewController alloc] init];
        messageComposer.messageComposeDelegate = self;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            messageComposer.navigationBar.tintColor  = [UIColor whiteColor];
        }
        [messageComposer setRecipients:nil];
        [messageComposer setBody:TEXT_SHARE_MESSAGE_BODY_TEXT];
        [self presentViewController:messageComposer animated:YES completion:nil];
    }else{
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"You may not have set up sms service on your device.\nPlease check and try again."
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

#pragma mark MFMessageComposeViewController Delegate Methods
-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
		case MessageComposeResultCancelled:
            DebugLog(@"SMS Cancelled");
			break;
		case MessageComposeResultFailed:
        {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Unknown Error\nSMS failed"
														   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
        }
			break;
		case MessageComposeResultSent:
        {
            DebugLog(@"SMS Sent");
        }
			break;
		default:
			break;
	}
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)whtsAppBtnPressed:(id)sender {
    NSString *urlString = [NSString stringWithFormat:@"whatsapp://send?text=%@",[TEXT_SHARE_MESSAGE_BODY_TEXT stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURL *whatsappURL = [NSURL URLWithString:urlString];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [[UIApplication sharedApplication] openURL: whatsappURL];
    }
}


#pragma UITableView Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [shareArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"settingsCell";
    UIView *topView;
    UILabel *lblTitle;
    UIImageView *thumbImg;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backgroundColor = [UIColor clearColor];
        cell.layer.shadowOffset = CGSizeMake(-15, 20);
        cell.layer.shadowColor = [UIColor darkGrayColor].CGColor;
        cell.layer.shadowRadius = 5;
        
        cell.layer.shadowPath = [UIBezierPath bezierPathWithRect:cell.bounds].CGPath;
        
        // Create the top view
        topView = [[UIView alloc] initWithFrame:CGRectMake(10, 5, 300, 75)];
        topView.tag = 111;
        topView.backgroundColor = [UIColor whiteColor];
        //topView = [CommonCallback setViewPropertiesWithRoundedCorner:topView];
        topView.clipsToBounds = YES;
        
        
        thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(5, 7, 60, 60)];
        thumbImg.tag = 1;
        thumbImg.contentMode = UIViewContentModeScaleAspectFit;
        //[cell.contentView addSubview:thumbImg];
        [topView addSubview:thumbImg];
        
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(80.0,25.0,210.0,25.0)];
        lblTitle.text = @"";
        lblTitle.tag = 2;
        lblTitle.font = DEFAULT_FONT(20);
        lblTitle.textAlignment = NSTextAlignmentLeft;
        lblTitle.textColor = DEFAULT_COLOR;
        lblTitle.backgroundColor =  [UIColor clearColor];
        lblTitle.minimumScaleFactor = 0.5f;
        lblTitle.adjustsFontSizeToFitWidth = YES;
        //[cell.contentView addSubview:lblTitle];
        [topView addSubview:lblTitle];
        
        //        UILabel *lblline = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 73.0, 300.0, 2.0)];
        //        lblline.text = @"";
        //        lblline.backgroundColor =  LIGHT_ORANGE_COLOR;
        //        [topView addSubview:lblline];
        
        // Add views to contentView
        [cell.contentView addSubview:topView];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = BROWN_OFFWHITE_COLOR;
        cell.selectedBackgroundView = bgColorView;
        
    }
    
    topView = (UIView *)[cell viewWithTag:111];
    
    if([[shareArray objectAtIndex:indexPath.row] isEqualToString:WHATSAPP])
    {
        UILabel *lblTitle = (UILabel *)[topView viewWithTag:2];
        [lblTitle setText:[shareArray objectAtIndex:indexPath.row]];
        
        UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
        [thumbImgview setImage:[UIImage imageNamed:@"Whatsapp_circle"]];
    } else if ([[shareArray objectAtIndex:indexPath.row] isEqualToString:FACEBOOK]) {
        UILabel *lblTitle = (UILabel *)[topView viewWithTag:2];
        [lblTitle setText:[shareArray objectAtIndex:indexPath.row]];
        
        UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
        [thumbImgview setImage:[UIImage imageNamed:@"Facebook_circle.png"]];
    } else if ([[shareArray objectAtIndex:indexPath.row] isEqualToString:TWITTER]) {
        UILabel *lblTitle = (UILabel *)[topView viewWithTag:2];
        [lblTitle setText:[shareArray objectAtIndex:indexPath.row]];
        
        UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
        [thumbImgview setImage:[UIImage imageNamed:@"Twitter_circle.png"]];
    } else if ([[shareArray objectAtIndex:indexPath.row] isEqualToString:EMAIL]) {
        UILabel *lblTitle = (UILabel *)[topView viewWithTag:2];
        [lblTitle setText:[shareArray objectAtIndex:indexPath.row]];
        
        UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
        [thumbImgview setImage:[UIImage imageNamed:@"EMail.png"]];
    } else if ([[shareArray objectAtIndex:indexPath.row] isEqualToString:SMS]) {
        UILabel *lblTitle = (UILabel *)[topView viewWithTag:2];
        [lblTitle setText:[shareArray objectAtIndex:indexPath.row]];
        
        UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
        [thumbImgview setImage:[UIImage imageNamed:@"SMS.png"]];
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if([[shareArray objectAtIndex:indexPath.row] isEqualToString:WHATSAPP])
    {
        [self whtsAppBtnPressed:nil];
        NSDictionary *socialParams = [NSDictionary dictionaryWithObjectsAndKeys:@"Website",@"Source", nil];
        [INEventLogger logEvent:@"ShareThisApp" withParams:socialParams];
    } else if ([[shareArray objectAtIndex:indexPath.row] isEqualToString:FACEBOOK]) {
        [self fbBtnPressed:nil];
        NSDictionary *socialParams = [NSDictionary dictionaryWithObjectsAndKeys:@"Facebook",@"Source", nil];
        [INEventLogger logEvent:@"ShareThisApp" withParams:socialParams];
    } else if ([[shareArray objectAtIndex:indexPath.row] isEqualToString:TWITTER]) {
        [self twitterBtnPressed:nil];
        NSDictionary *socialParams = [NSDictionary dictionaryWithObjectsAndKeys:@"Twitter",@"Source", nil];
        [INEventLogger logEvent:@"ShareThisApp" withParams:socialParams];
    } else if ([[shareArray objectAtIndex:indexPath.row] isEqualToString:EMAIL]) {
        [self emailBtnPressed:nil];
        NSDictionary *socialParams = [NSDictionary dictionaryWithObjectsAndKeys:@"Email",@"Source", nil];
        [INEventLogger logEvent:@"ShareThisApp" withParams:socialParams];
    } else if ([[shareArray objectAtIndex:indexPath.row] isEqualToString:SMS]) {
        [self smsBtnPressed:nil];
        NSDictionary *socialParams = [NSDictionary dictionaryWithObjectsAndKeys:@"SMS",@"Source", nil];
        [INEventLogger logEvent:@"ShareThisApp" withParams:socialParams];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
