//
//  INContactUsViewController.m
//  shoplocal
//
//  Created by Kirti Nikam on 11/11/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INContactUsViewController.h"
#import "constants.h"
#import "INAppDelegate.h"
#import "CommonCallback.h"

@interface INContactUsViewController ()

@end

@implementation INContactUsViewController
@synthesize lblMessage;
@synthesize sendMailBtn;
@synthesize backView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:TRUE];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:FALSE];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    [self setupMenuBarButtonItems];
    [self setUI];
}


#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma set Navigation barbuttons callback
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:MENU_TITLE style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
//    return [[UIBarButtonItem alloc]
//            initWithTitle:MENU_TITLE style:UIBarButtonItemStyleBordered
//            target:self.navigationController.sideMenu
//            action:@selector(toggleRightSideMenu)];
    return nil;
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setSendMailBtn:nil];
    [self setLblMessage:nil];
    [self setBackView:nil];
    [super viewDidUnload];
}

#pragma internal method
-(void)setUI{
    backView = [CommonCallback setViewPropertiesWithRoundedCorner:backView];
    
    lblMessage.textAlignment    = NSTextAlignmentCenter;
    lblMessage.backgroundColor  = [UIColor clearColor];
    lblMessage.font             = DEFAULT_FONT(18);
    lblMessage.textColor        = DEFAULT_COLOR;
    lblMessage.numberOfLines    = 15;
    lblMessage.text             = @"We love hearing from our customers. Write us a mail and tell us about a new store you want to include in shoplocal or anything at all.";
    
    sendMailBtn.titleLabel.font     =   DEFAULT_BOLD_FONT(18);
    sendMailBtn.backgroundColor     =   [UIColor clearColor];
    [sendMailBtn setTitle:@"Send feedback" forState:UIControlStateNormal];
    [sendMailBtn setTitle:@"Send feedback" forState:UIControlStateHighlighted];
    [sendMailBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
//    sendMailBtn.titleLabel.font     =   DEFAULT_BOLD_FONT(16.0);
//    sendMailBtn.backgroundColor     =   [UIColor whiteColor];
//    sendMailBtn.layer.borderColor   =   BUTTON_BORDER_COLOR.CGColor;
//    sendMailBtn.layer.borderWidth   = 2.0;
//    sendMailBtn.layer.cornerRadius  = 5.0;
//    [sendMailBtn setTitle:@"Send feedback" forState:UIControlStateNormal];
//    [sendMailBtn setTitle:@"Send feedback" forState:UIControlStateHighlighted];
//    [sendMailBtn setTitleColor:BUTTON_COLOR forState:UIControlStateNormal];
//    [sendMailBtn setTitleColor:BUTTON_OFFWHITE_COLOR forState:UIControlStateHighlighted];
}


- (IBAction)sendMailBtnPressed:(id)sender {
        if ([MFMailComposeViewController canSendMail]) {
            MFMailComposeViewController *emailComposer = [[MFMailComposeViewController alloc] init];
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
                emailComposer.navigationBar.tintColor  = [UIColor whiteColor];
            }
            emailComposer.mailComposeDelegate = self;
            [emailComposer setToRecipients:[NSArray arrayWithObjects:@"contact@shoplocal.co.in", nil]];
            [emailComposer setSubject:@""];
            [emailComposer setMessageBody:@"" isHTML:NO];
            [self presentViewController:emailComposer animated:YES completion:nil];
        }else
        {
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:ALERT_TITLE
                                      message:@"You may not have set up mail service on your device or You may not connected to internet.\nPlease check and try again."
                                      delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [alertView show];
        }
}

#pragma mark Mail Compose Delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    if (result == MFMailComposeResultFailed) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                        message:NSLocalizedString(@"Email failed to send. Please try again.", nil)
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", nil) otherButtonTitles:nil];
		[alert show];
    }else if (result == MFMailComposeResultSent)
    {
        DebugLog(@"MFMailComposeResultSent");
    }
	[self dismissViewControllerAnimated:YES completion:nil];
}
@end
