//
//  INTermsViewController.h
//  shoplocal
//
//  Created by Rishi on 12/09/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol termsDelegate <NSObject>
-(void)dismissTerms;
@end

@interface INTermsViewController : UIViewController <UIWebViewDelegate>
@property (weak, nonatomic) id<termsDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIWebView *termsWebView;
@property (strong, nonatomic) IBOutlet UIButton *acceptBtn;
@property (strong, nonatomic) IBOutlet UIButton *cancelBtn;

- (IBAction)acceptBtnPressed:(id)sender;
- (IBAction)cancelBtnpressed:(id)sender;

@end
