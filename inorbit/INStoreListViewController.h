//
//  INStoreListViewController.h
//  shoplocal
//
//  Created by Rishi on 19/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@class INStoreDetailObj;
@interface INStoreListViewController : UIViewController{
    INStoreDetailObj *storeObj;
    MBProgressHUD *hud;
    BOOL isDragging;
    BOOL isLoading;
    BOOL isAdding;
    sqlite3 *shoplocalDB;
    
    double keyboardHeight;
    BOOL isSearchOn;
    
     double lastContentOffset;
}

@property (strong, nonatomic) IBOutlet UITableView *storelistTableView;
@property (strong, nonatomic) NSMutableArray *storeArray;
@property (readwrite, nonatomic) int storeListType;

@property (nonatomic, retain) UIView *refreshHeaderView;
@property (nonatomic, retain) UILabel *refreshLabel;
@property (nonatomic, retain) UIImageView *refreshArrow;
@property (nonatomic, retain) UIActivityIndicatorView *refreshSpinner;

//*****Pull TO Refresh
@property (nonatomic, copy) NSString *textPull;
@property (nonatomic, copy) NSString *textRelease;
@property (nonatomic, copy) NSString *textLoading;

@property (strong, nonatomic) IBOutlet UIView *searchView;
@property (strong, nonatomic) IBOutlet UITextField *searchTextField;
@property (strong, nonatomic) IBOutlet UIButton *searchCancelBtn;
- (IBAction)searchCancelBtnPressed:(id)sender;
- (IBAction)searchTextChanged:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *searchZoomOutBtn;
- (IBAction)searchZoomOutBtnPressed:(id)sender;

@end
