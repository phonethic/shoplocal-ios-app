//
//  INPrivacyPolicyViewController.h
//  shoplocal
//
//  Created by Rishi on 19/09/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface INPrivacyPolicyViewController : UIViewController  <UIWebViewDelegate>
@property (strong, nonatomic) IBOutlet UIWebView *privacyWebView;
@property (strong, nonatomic) IBOutlet UIButton *okBtn;
- (IBAction)okBtnPressed:(id)sender;

@end
