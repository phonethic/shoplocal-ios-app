//
//  INEventLogger.h
//  shoplocal
//
//  Created by Rishi on 13/12/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocalyticsSession.h"

@interface INEventLogger : NSObject


+(void)logEvent:(NSString*) eventName;
+(void)logEvent:(NSString*) eventName  withParams:(NSDictionary*) eventParams;
+(void)setUser:(NSString*) eventName;
+(void)setGender:(NSString*) gender;
+(void)setAge:(int) age;
+(void)logPageViews:(id)navigationController;
+(void)logLocationValues:(CLLocation *)location;
+ (void)localyticsSessionWillResignActive;
+ (void)localyticsSessionDidEnterBackground;
+ (void)localyticsSessionWillEnterForeground;
+ (void)localyticsSessionDidBecomeActive:(NSString *)appId;
+ (void)localyticsSessionWillTerminate;

@end
