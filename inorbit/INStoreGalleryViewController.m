//
//  INStoreGalleryViewController.m
//  shoplocal
//
//  Created by Rishi on 23/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INStoreGalleryViewController.h"
#import "INAppDelegate.h"
#import "constants.h"
#import "INGalleryObj.h"
#import "UIImageView+WebCache.h"
#import "CommonCallback.h"
#import "SIAlertView.h"

#define STORE_GALLERY(ID) [NSString stringWithFormat:@"%@%@%@place_api/place_gallery?place_id=%d",LIVE_SERVER,URL_PREFIX,API_VERSION,ID]
#define ADD_GALLERY [NSString stringWithFormat:@"%@%@place_api/place_gallery",URL_PREFIX,API_VERSION]
#define THUMBNAIL_LINK(IMG_LINK) [NSString stringWithFormat:@"%@%@%@",LIVE_SERVER,URL_PREFIX,IMG_LINK]
#define DELETE_GALLERY(PLACEID,GALLERYID) [NSString stringWithFormat:@"%@%@place_api/place_gallery?place_id=%@&gallery_id=%@",URL_PREFIX,API_VERSION,PLACEID,GALLERYID]


@interface INStoreGalleryViewController ()
@property(strong) NSDictionary *splashJson;
@end

@implementation INStoreGalleryViewController
@synthesize galleryArray;
@synthesize storeId;
@synthesize newMedia;
@synthesize gridView = _gridView;
@synthesize photos = _photos;
@synthesize addImageDescriptionView,lbldescriptionViewContent,lbldescriptionViewHeader,descriptionViewSetBtn,descriptionViewSkipBtn,descriptionViewtitleTextView;
@synthesize editedImage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                             style:UIBarButtonItemStylePlain
                                                            target:nil
                                                            action:nil];
    [[self navigationItem] setBackBarButtonItem:back];
//    UIBarButtonItem *addbutton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
//                                                                               target:self action:@selector(addNewImage)];
//    [[self navigationItem] setRightBarButtonItem:addbutton];
    [self setUI];
    
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    viewTap.numberOfTapsRequired = 1;
    viewTap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:viewTap];
    
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

    //Add gridView
    _gridView = [[VCGridView alloc] initWithFrame:self.view.bounds];
	_gridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	_gridView.delegate = self;
	_gridView.dataSource = self;
	[self.view addSubview:_gridView];
    //self.navigationItem.rightBarButtonItem = self.editButtonItem;
    galleryArray = [[NSMutableArray alloc] init];
    if ([IN_APP_DELEGATE networkavailable]) {
        [self sendStoreGalleryRequest];
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

#pragma Gesture Methods
- (void)tapDetected:(UIGestureRecognizer *)sender {
    if (!addImageDescriptionView.hidden) {
        [self.view endEditing:YES];
        CGPoint tapLocation = [sender locationInView:self.view];
        UIView *view = [self.view hitTest:tapLocation withEvent:nil];
        if (![view isKindOfClass:[UIButton class]]) {
            [UIView animateWithDuration:0.2 animations:^{
                addImageDescriptionView.frame = CGRectMake(addImageDescriptionView.frame.origin.x, 58, addImageDescriptionView.frame.size.width, addImageDescriptionView.frame.size.height);
            } completion:^(BOOL finished) {
            }];
        }
    }
}

#pragma Internal Methods
-(void)setUI{
    addImageDescriptionView = [CommonCallback setViewPropertiesWithRoundedCorner:addImageDescriptionView];
    addImageDescriptionView.layer.shadowColor = [UIColor blackColor].CGColor;
    addImageDescriptionView.layer.shadowOffset = CGSizeMake(1, 1);
    addImageDescriptionView.layer.shadowOpacity = 1.0;
    addImageDescriptionView.layer.shadowRadius = 10.0;
    
    lbldescriptionViewHeader.textAlignment = NSTextAlignmentCenter;
    lbldescriptionViewHeader.backgroundColor = [UIColor clearColor];
    lbldescriptionViewHeader.font = DEFAULT_FONT(20);
    lbldescriptionViewHeader.textColor = [UIColor blackColor];
    lbldescriptionViewHeader.adjustsFontSizeToFitWidth = YES;
    lbldescriptionViewHeader.text = ALERT_TITLE;
    
    lbldescriptionViewContent.textAlignment = NSTextAlignmentCenter;
    lbldescriptionViewContent.backgroundColor = [UIColor clearColor];
    lbldescriptionViewContent.font = DEFAULT_FONT(16);
    lbldescriptionViewContent.textColor = [UIColor darkGrayColor];
    lbldescriptionViewContent.numberOfLines = 15;
    lbldescriptionViewContent.text = @"Add a description of the photo";
    
    descriptionViewSetBtn.titleLabel.font = DEFAULT_FONT([UIFont buttonFontSize]);
    descriptionViewSkipBtn.titleLabel.font = DEFAULT_FONT([UIFont buttonFontSize]);
    
    [descriptionViewSetBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [descriptionViewSetBtn setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.8] forState:UIControlStateHighlighted];
    
    [descriptionViewSkipBtn setTitleColor:[UIColor colorWithWhite:0.3 alpha:1] forState:UIControlStateNormal];
    [descriptionViewSkipBtn setTitleColor:[UIColor colorWithWhite:0.3 alpha:0.8] forState:UIControlStateHighlighted];
    
    UIImage *dnormalImage = [UIImage imageNamed:@"SIAlertView.bundle/button-destructive-d"];
    UIImage *dhighlightedImage = [UIImage imageNamed:@"SIAlertView.bundle/button-destructive-d"];
    UIEdgeInsets dinsets = [self getResizedImageInset:dnormalImage];
	dnormalImage = [dnormalImage resizableImageWithCapInsets:dinsets];
	dhighlightedImage = [dhighlightedImage resizableImageWithCapInsets:dinsets];
    
    UIImage *normalImage = [UIImage imageNamed:@"SIAlertView.bundle/button-cancel"];
    UIImage *highlightedImage = [UIImage imageNamed:@"SIAlertView.bundle/button-cancel-d"];
    UIEdgeInsets insets = [self getResizedImageInset:normalImage];
	normalImage = [normalImage resizableImageWithCapInsets:insets];
	highlightedImage = [highlightedImage resizableImageWithCapInsets:insets];
    
    [descriptionViewSetBtn setBackgroundImage:dnormalImage forState:UIControlStateNormal];
	[descriptionViewSetBtn setBackgroundImage:dhighlightedImage forState:UIControlStateHighlighted];
    
	[descriptionViewSkipBtn setBackgroundImage:normalImage forState:UIControlStateNormal];
	[descriptionViewSkipBtn setBackgroundImage:highlightedImage forState:UIControlStateHighlighted];
    
    descriptionViewtitleTextView.textAlignment = NSTextAlignmentLeft;
    descriptionViewtitleTextView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:1];
    descriptionViewtitleTextView.font = DEFAULT_BOLD_FONT(18);
    descriptionViewtitleTextView.textColor = BROWN_COLOR;
    descriptionViewtitleTextView.text = @"";
    descriptionViewtitleTextView.layer.cornerRadius = 5.0;
    descriptionViewtitleTextView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    descriptionViewtitleTextView.layer.borderWidth = 0.5;
    descriptionViewtitleTextView.delegate = self;
//    descriptionViewtitleTextView.layer.shadowColor = [UIColor blackColor].CGColor;
//    descriptionViewtitleTextView.layer.shadowOffset = CGSizeMake(1, 1);
//    descriptionViewtitleTextView.layer.shadowOpacity = 1;
//    descriptionViewtitleTextView.layer.shadowRadius = 10.0;
    
    [addImageDescriptionView setHidden:YES];
    [self setEditing:FALSE];
}
-(UIEdgeInsets)getResizedImageInset:(UIImage *)image
{
    CGFloat hInset = floorf(image.size.width / 2);
	CGFloat vInset = floorf(image.size.height / 2);
	UIEdgeInsets insets = UIEdgeInsetsMake(vInset, hInset, vInset, hInset);
    return insets;
}

-(void)addNewImage
{
    UIAlertView *addImageToGalleryAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Choose your option" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Choose Existing", @"Take Picture", nil];
    [addImageToGalleryAlert show];
}

-(void)sendStoreGalleryRequest
{
    DebugLog(@"%@",STORE_GALLERY(self.storeId));
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:STORE_GALLERY(self.storeId)]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setValue:[INUserDefaultOperations getMerchantAuthId] forHTTPHeaderField:@"user_id"];
    [urlRequest setValue:[INUserDefaultOperations getMerchantAuthCode] forHTTPHeaderField:@"auth_id"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        [CommonCallback hideProgressHud];
                                                        DebugLog(@"%@", [self.splashJson description]);
                                                        if(self.splashJson  != nil) {
                                                            [galleryArray removeAllObjects];
                                                            [galleryArray addObject:@"placeholder"];

                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSArray* jsonArray = [self.splashJson  objectForKey:@"data"];
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *objdict = [jsonArray  objectAtIndex:index];
                                                                    //DebugLog(@"\n\n%@\n\n",objdict);
                                                                    INGalleryObj *tempgalleryObj = [[INGalleryObj alloc] init];
                                                                    tempgalleryObj.galleryID = [objdict objectForKey:@"id"];
                                                                    tempgalleryObj.place_id = [objdict objectForKey:@"place_id"];
                                                                    tempgalleryObj.title = [objdict objectForKey:@"title"];
                                                                    tempgalleryObj.image_url = [objdict objectForKey:@"image_url"];
                                                                    tempgalleryObj.thumb_url = [objdict objectForKey:@"thumb_url"];
                                                                    tempgalleryObj.image_date = [objdict objectForKey:@"image_date"];
                                                                    tempgalleryObj.title = [objdict objectForKey:@"title"];
                                                                    [galleryArray addObject:tempgalleryObj];
                                                                    tempgalleryObj = nil;
                                                                }
                                                            } else if(self.splashJson != nil && [[self.splashJson  objectForKey:@"success"] isEqualToString:@"false"]) {
                                                                if([self.splashJson  objectForKey:@"code"] != nil &&  [[self.splashJson  objectForKey:@"code"] isEqualToString:MERCHANT_INVALID_USER_AUTH_CODE])
                                                                {
                                                                    if([INUserDefaultOperations isMerchantLoggedIn]){
                                                                        [INUserDefaultOperations showAlert:MERCHANT_INVALID_USER_AUTH_MESSAGE];
                                                                        [INUserDefaultOperations clearMerchantDetails];
                                                                        [self performSelector:@selector(popStoreGalleryController) withObject:nil afterDelay:2.0];
                                                                    }
                                                                } else {
                                                                     [INUserDefaultOperations showAlert:[self.splashJson  objectForKey:@"message"]];
                                                                }
                                                            }
                                                            DebugLog(@"galleryArray %@",galleryArray);
//                                                            if([galleryArray count] > 0){
                                                                [self.gridView reloadData];
//                                                            }
                                                            
                                                            if([galleryArray count] > 1)
                                                            {
                                                                self.navigationItem.rightBarButtonItem = self.editButtonItem;
                                                            } else {
                                                                self.navigationItem.rightBarButtonItem = nil;
                                                            }
                                                        }

                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        [CommonCallback hideProgressHud];
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                        [av show];
                                                        
                                                    }];
    
    
    
    [operation start];
}

-(void)sendAddGalleryImageIntoStoreImageRequest:(UIImage *)pickerimage
{
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *postparams = [[NSMutableDictionary alloc] init];
    [postparams setObject:[NSString stringWithFormat:@"%d",self.storeId] forKey:@"place_id"];
    [postparams setObject:[INUserDefaultOperations getMerchantAuthId] forKey:@"user_id"];
    [postparams setObject:[INUserDefaultOperations getMerchantAuthCode] forKey:@"auth_id"];
    NSData *dataObj = UIImageJPEGRepresentation(pickerimage, 0.75);
    NSString *image = [IN_APP_DELEGATE base64forData:dataObj];
    [postparams setObject:image forKey:@"userfile"];
    [postparams setObject:@"gallery.jpg" forKey:@"filename"];
    [postparams setObject:@"image/jpeg" forKey:@"filetype"];
    
    if ([descriptionViewtitleTextView.text length] > 0) {
        [postparams setObject:descriptionViewtitleTextView.text forKey:@"title"];
    }
    
    DebugLog(@"----%@------",postparams);
    [httpClient postPath:ADD_GALLERY parameters:postparams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json afnetworking ->%@",json);
        if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"true"])
        {
            //data =   {"place_id" = 415;}
            [INUserDefaultOperations showAlert:[json  objectForKey:@"message"]];
            [self sendStoreGalleryRequest];
            editedImage = nil;
        }else{
            if([json objectForKey:@"code"] != nil &&  [[json objectForKey:@"code"] isEqualToString:MERCHANT_INVALID_USER_AUTH_CODE])
            {
                if([INUserDefaultOperations isMerchantLoggedIn]){
                    [INUserDefaultOperations showAlert:MERCHANT_INVALID_USER_AUTH_MESSAGE];
                    [INUserDefaultOperations clearMerchantDetails];
                    [self performSelector:@selector(popStoreGalleryController) withObject:nil afterDelay:2.0];
                }
            } else {
                [INUserDefaultOperations showAlert:[json objectForKey:@"message"]];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        [CommonCallback hideProgressHud];
        editedImage = nil;
    }];
    
}

-(void)sendDeleteGalleryImageFromStoreRequest:(NSString *)placeId gallery:(NSString *)galleryId
{
     DebugLog(@"delete = %@",DELETE_GALLERY(placeId, galleryId));
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    [httpClient setDefaultHeader:@"user_id" value:[INUserDefaultOperations getMerchantAuthId]];
    [httpClient setDefaultHeader:@"auth_id" value:[INUserDefaultOperations getMerchantAuthCode]];
    [httpClient deletePath:DELETE_GALLERY(placeId,galleryId) parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"true"])
        {
            [self sendStoreGalleryRequest];
        }else{
            if([json objectForKey:@"code"] != nil &&  [[json objectForKey:@"code"] isEqualToString:MERCHANT_INVALID_USER_AUTH_CODE])
            {
                if([INUserDefaultOperations isMerchantLoggedIn]){
                    [INUserDefaultOperations showAlert:MERCHANT_INVALID_USER_AUTH_MESSAGE];
                    [INUserDefaultOperations clearMerchantDetails];
                    [self performSelector:@selector(popStoreGalleryController) withObject:nil afterDelay:2.0];
                }
            } else {
                [INUserDefaultOperations showAlert:[json objectForKey:@"message"]];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        [CommonCallback hideProgressHud];
    }];
}

#pragma mark - alertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"Choose Existing"])
    {
        [self useCameraRoll];
        NSDictionary *storeGalleryParams = [NSDictionary dictionaryWithObjectsAndKeys:@"Source",@"Gallery", nil];
        [INEventLogger logEvent:@"StoreGallery_ImageAdd" withParams:storeGalleryParams];
        DebugLog(@"%@",storeGalleryParams);
    } else if([title isEqualToString:@"Take Picture"])
    {
        [self useCamera];
        NSDictionary *storeGalleryParams = [NSDictionary dictionaryWithObjectsAndKeys:@"Source",@"Camera", nil];
        DebugLog(@"%@",storeGalleryParams);
        [INEventLogger logEvent:@"StoreGallery_ImageAdd" withParams:storeGalleryParams];
    }
}

#pragma mark - Image Picker
- (void)useCamera
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker =
        [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        //[imagePicker.navigationBar setTintColor:BROWN_COLOR];
        imagePicker.sourceType =
        UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = [NSArray arrayWithObjects:
                                  (NSString *) kUTTypeImage,
                                  nil];
        imagePicker.allowsEditing = YES;
        [self presentViewController:imagePicker animated:YES completion:nil];
        newMedia = YES;
    }
}

- (void)useCameraRoll
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    {
        UIImagePickerController *imagePicker =
        [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        //[imagePicker.navigationBar setTintColor:BROWN_COLOR];
        imagePicker.sourceType =
        UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.mediaTypes = [NSArray arrayWithObjects:
                                  (NSString *) kUTTypeImage,
                                  nil];
        imagePicker.allowsEditing = YES;
        [self presentViewController:imagePicker animated:YES completion:nil];
        newMedia = NO;
    }
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info
                           objectForKey:UIImagePickerControllerMediaType];
//    [self dismissViewControllerAnimated:YES completion:nil];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
//        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
        
        /*DebugLog(@"image size -%f-%f-",image.size.width,image.size.height);
        
        NSData *imageData1 = [[NSData alloc] initWithData:UIImageJPEGRepresentation((image),0.6)];
        int imageSize1 = (int)imageData1.length / 1024;
        DebugLog(@"JPEGRepresentation 0.6 -->> SIZE OF IMAGE in KiloBytes: %i ", imageSize1);
        
        NSData *imageData2 = [[NSData alloc] initWithData:UIImageJPEGRepresentation((image),0.7)];
        int imageSize2 = (int)imageData2.length / 1024;
        DebugLog(@"JPEGRepresentation 0.7 -->> SIZE OF IMAGE in KiloBytes: %i ", imageSize2);
        
        NSData *imageData3 = [[NSData alloc] initWithData:UIImageJPEGRepresentation((image),0.8)];
        int imageSize3 = (int)imageData3.length / 1024;
        DebugLog(@"JPEGRepresentation 0.8 -->> SIZE OF IMAGE in KiloBytes: %i ", imageSize3);
        
        NSData *imageData4 = [[NSData alloc] initWithData:UIImageJPEGRepresentation((image),0.9)];
        int imageSize4 = (int)imageData4.length / 1024;
        DebugLog(@"JPEGRepresentation 0.9 -->> SIZE OF IMAGE in KiloBytes: %i ", imageSize4);
        
        NSData *imageData5 = [[NSData alloc] initWithData:UIImageJPEGRepresentation((image),1)];
        int imageSize5 = (int)imageData5.length / 1024;
        DebugLog(@"JPEGRepresentation 1 -->> SIZE OF IMAGE in KiloBytes: %i ", imageSize5);*/
        
        CLImageEditor *editor = [[CLImageEditor alloc] initWithImage:image];
        editor.delegate = self;
        /* @"CLFilterTool",
         @"CLAdjustmentTool",
         @"CLEffectTool",
         @"CLBlurTool",
         @"CLClippingTool",
         @"CLRotateTool",
         @"CLToneCurveTool",*/
        
        CLImageToolInfo *Filtertool = [editor.toolInfo subToolInfoWithToolName:@"CLFilterTool" recursive:NO];
        Filtertool.available = NO;
        CLImageToolInfo *Blurtool = [editor.toolInfo subToolInfoWithToolName:@"CLBlurTool" recursive:NO];
        Blurtool.available = NO;
        CLImageToolInfo *Curvetool = [editor.toolInfo subToolInfoWithToolName:@"CLToneCurveTool" recursive:NO];
        Curvetool.available = NO;
        CLImageToolInfo *ClippingTool = [editor.toolInfo subToolInfoWithToolName:@"CLClippingTool" recursive:NO];
        ClippingTool.available = NO;
        
        CLImageToolInfo *AdjustmentTool = [editor.toolInfo subToolInfoWithToolName:@"CLAdjustmentTool" recursive:NO];
        AdjustmentTool.dockedNumber = 2;
        CLImageToolInfo *EffectTool = [editor.toolInfo subToolInfoWithToolName:@"CLEffectTool" recursive:NO];
        EffectTool.dockedNumber = 3;
//        CLImageToolInfo *ClippingTool = [editor.toolInfo subToolInfoWithToolName:@"CLClippingTool" recursive:NO];
//        ClippingTool.dockedNumber = 1;
        CLImageToolInfo *RotateTool = [editor.toolInfo subToolInfoWithToolName:@"CLRotateTool" recursive:NO];
        RotateTool.dockedNumber = 1;
        [picker pushViewController:editor animated:YES];
        
    }
    else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie])
    {
		// Code here to support video if enabled
	}
}

-(void)image:(UIImage *)image finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Save failed"
                              message: @"Failed to save image"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark- CLImageEditor delegate
- (void)imageEditor:(CLImageEditor *)editor didFinishEdittingWithImage:(UIImage *)leditedImage
{
    [editor dismissViewControllerAnimated:YES completion:nil];
    DebugLog(@"gallery : saveToalbum %@",leditedImage);
    
    DebugLog(@"After filter, image size -%f-%f-",leditedImage.size.width,leditedImage.size.height);
    
    editedImage = leditedImage;
    descriptionViewtitleTextView.text = @"";
    [self.view bringSubviewToFront:addImageDescriptionView];
    [addImageDescriptionView setHidden:FALSE];
    //add animation code here
    [CommonCallback viewtransitionInCompletion:addImageDescriptionView completion:^{
        [CommonCallback viewtransitionOutCompletion:addImageDescriptionView completion:nil];
    }];
    
    if (newMedia)
        UIImageWriteToSavedPhotosAlbum(leditedImage,
                                       self,
                                       @selector(image:finishedSavingWithError:contextInfo:),
                                       nil);
    /*
    NSData *imageData1 = [[NSData alloc] initWithData:UIImageJPEGRepresentation((leditedImage),0.6)];
    int imageSize1 = (int)imageData1.length / 1024;
    DebugLog(@"JPEGRepresentation 0.6 -->> SIZE OF IMAGE in KiloBytes: %i ", imageSize1);
    
    NSData *imageData2 = [[NSData alloc] initWithData:UIImageJPEGRepresentation((leditedImage),0.7)];
    int imageSize2 = (int)imageData2.length / 1024;
    DebugLog(@"JPEGRepresentation 0.7 -->> SIZE OF IMAGE in KiloBytes: %i ", imageSize2);
    
    NSData *imageData3 = [[NSData alloc] initWithData:UIImageJPEGRepresentation((leditedImage),0.8)];
    int imageSize3 = (int)imageData3.length / 1024;
    DebugLog(@"JPEGRepresentation 0.8 -->> SIZE OF IMAGE in KiloBytes: %i ", imageSize3);
    
    NSData *imageData4 = [[NSData alloc] initWithData:UIImageJPEGRepresentation((leditedImage),0.9)];
    int imageSize4 = (int)imageData4.length / 1024;
    DebugLog(@"JPEGRepresentation 0.9 -->> SIZE OF IMAGE in KiloBytes: %i ", imageSize4);
    
    NSData *imageData5 = [[NSData alloc] initWithData:UIImageJPEGRepresentation((leditedImage),1)];
    int imageSize5 = (int)imageData5.length / 1024;
    DebugLog(@"JPEGRepresentation 1 -->> SIZE OF IMAGE in KiloBytes: %i ", imageSize5);
    
    UIImage *image1 = [UIImage imageWithData:imageData1];
    UIImage *image2 = [UIImage imageWithData:imageData2];
    UIImage *image3 = [UIImage imageWithData:imageData3];
    UIImage *image4 = [UIImage imageWithData:imageData4];
    UIImage *image5 = [UIImage imageWithData:imageData5];
    
    UIImageWriteToSavedPhotosAlbum(image1,
                                   self,
                                   @selector(image:finishedSavingWithError:contextInfo:),
                                   nil);
    
    UIImageWriteToSavedPhotosAlbum(image2,
                                   self,
                                   @selector(image:finishedSavingWithError:contextInfo:),
                                   nil);

    UIImageWriteToSavedPhotosAlbum(image3,
                                   self,
                                   @selector(image:finishedSavingWithError:contextInfo:),
                                   nil);
    
    UIImageWriteToSavedPhotosAlbum(image4,
                                   self,
                                   @selector(image:finishedSavingWithError:contextInfo:),
                                   nil);
    
    UIImageWriteToSavedPhotosAlbum(image5,
                                   self,
                                   @selector(image:finishedSavingWithError:contextInfo:),
                                   nil);
    */
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setAddImageDescriptionView:nil];
    [self setLbldescriptionViewHeader:nil];
    [self setLbldescriptionViewContent:nil];
    [self setDescriptionViewSetBtn:nil];
    [self setDescriptionViewSkipBtn:nil];
    [self setDescriptionViewtitleTextView:nil];
    [super viewDidUnload];
}

#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - Private Methods

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
	[super setEditing:editing animated:animated];
	[self.gridView setEditing:editing animated:animated];
	
	if (editing) {
		self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash
																							   target:self
																							   action:@selector(didTapAction:)];
	}else {
		self.navigationItem.leftBarButtonItem = nil;
	}
}

- (void)didTapAction:(id)sender
{
#if DEBUG
	DebugLog(@"Selected item count : %i %d", [self.gridView.selectedIndexes count],(int)[self.gridView.selectedIndexes firstIndex]);
#endif
//	[[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%i items selected.", [self.gridView.selectedIndexes count]]
//								 message:nil
//								delegate:nil
//					   cancelButtonTitle:@"Dismiss"
//					   otherButtonTitles:nil] show];
//    [self.gridView deleteCellAtIndexSet:self.gridView.selectedIndexes animated:YES];
//    DebugLog(@"%d",[self.gridView.selectedIndexes count]);
    //show alert
    NSString *msg = @"Do you want to delete selected image ?";
    if ([self.gridView.selectedIndexes count] > 0) {
        msg = @"Do you want to delete selected images ?";
    
        SIAlertView *sialertView1 = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:msg];
        sialertView1.transitionStyle = SIAlertViewTransitionStyleDropDown;
        sialertView1.backgroundStyle = SIAlertViewBackgroundStyleGradient;
        [sialertView1 addButtonWithTitle:@"Delete"
                                    type:SIAlertViewButtonTypeDestructive
                                 handler:^(SIAlertView *alert) {
                                     DebugLog(@"Delete Clicked");
                                     __block NSString *placeId = @"";
                                     __block NSString *galleryIds = @"";
                                     [self.gridView.selectedIndexes enumerateIndexesUsingBlock:^(NSUInteger index, BOOL *stop){
                                         DebugLog(@"index %d",index);
                                         if (index == 0) {
                                             return;
                                         }
                                         DebugLog(@" %@",[galleryArray objectAtIndex:index]);
                                         INGalleryObj *tempgalleryObj = (INGalleryObj *)[galleryArray objectAtIndex:index];
                                         if([galleryIds isEqualToString:@""])
                                         {
                                             galleryIds = [galleryIds stringByAppendingFormat:@"%@", tempgalleryObj.galleryID];
                                         } else {
                                             galleryIds = [galleryIds stringByAppendingFormat:@",%@", tempgalleryObj.galleryID];
                                         }
                                         placeId = tempgalleryObj.place_id;
                                     }];
                                     DebugLog(@"%@  -- %@",placeId,galleryIds);
                                     if(![galleryIds isEqualToString:@""])
                                     {
                                         [self sendDeleteGalleryImageFromStoreRequest:placeId gallery:galleryIds];
                                     }
                                     [self setEditing:FALSE animated:TRUE];
                                 }];
        [sialertView1 addButtonWithTitle:@"Cancel"
                                    type:SIAlertViewButtonTypeCancel
                                 handler:^(SIAlertView *alert) {
                                     DebugLog(@"Cancel Clicked");
                                 }];
        [sialertView1 show];
    } else {
        [INUserDefaultOperations showAlert:@"Please select atleast one image to delete."];
    }
}


#pragma mark - VCGridViewDataSource

- (NSInteger)numberOfCellsInGridView:(VCGridView*)gridView
{
//	return [galleryArray count];
    DebugLog(@"0) galleryArray Count %d",[galleryArray count]);
    double cellsCount = [galleryArray count]/3.0;
    DebugLog(@"1) cellsCount %f",cellsCount);
    if (cellsCount == 0 || cellsCount < 3) {
       return cellsCount = 9;
    }
    int cellsintCount = lroundf(cellsCount) + 1;
    DebugLog(@"2)cellsintCount %d",cellsintCount);
    return cellsintCount * 3;
}

- (VCGridViewCell *)gridView:(VCGridView *)gridView cellAtIndex:(NSInteger)index
{
    UIImageView *imageView;
    UILabel *lblTitle;
    
	VCGridViewCell *cell = [gridView dequeueReusableCell];
	if (!cell) {
		cell = [[VCGridViewCell alloc] initWithFrame:CGRectZero];
		
		CGRect contentFrame = CGRectInset(cell.bounds, 0, 0);
//        CGRect contentFrame = CGRectInset(CGRectMake(0, 0, 75, 75), 0, 0);

		imageView = [[UIImageView alloc] initWithFrame:contentFrame];
		imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		imageView.contentMode = UIViewContentModeScaleToFill;
        imageView.tag = 222;
		//imageView.image = [UIImage imageNamed:@"cell"];
       // imageView.layer.cornerRadius = 8.0;
		[cell.contentView addSubview:imageView];
        
		UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:contentFrame];
		imageView1.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		imageView1.contentMode = UIViewContentModeBottomRight;
		imageView1.image = [UIImage imageNamed:@"check"];
		cell.editingSelectionOverlayView = imageView1;
        
        
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0,60, 85, 25)];
        lblTitle.text = @"title";
        lblTitle.tag = 223;
        lblTitle.font = DEFAULT_BOLD_FONT(15);
        lblTitle.textAlignment = NSTextAlignmentCenter;
        lblTitle.textColor = [UIColor whiteColor];
        lblTitle.backgroundColor =  BROWN_OFFWHITE_COLOR;
        [cell.contentView addSubview:lblTitle];
        
        cell.contentView.layer.cornerRadius = 8.0;
        cell.contentView.clipsToBounds = YES;
        
 
		cell.highlightedBackgroundView.backgroundColor = BROWN_OFFWHITE_COLOR;
        cell.highlightedBackgroundView.layer.cornerRadius = 8.0;
	}
//    if (index < [galleryArray count]) {
//        imageView = (UIImageView *)[cell viewWithTag:111];
//        if ([[galleryArray objectAtIndex:index] isKindOfClass:[INGalleryObj class]]) {
//            INGalleryObj *tempgalleryObj  = [galleryArray objectAtIndex:index];
//            //DebugLog(@"link-%@",THUMBNAIL_LINK(tempgalleryObj.thumb_url));
//            [imageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(tempgalleryObj.thumb_url)] placeholderImage:[UIImage imageNamed:@"bar_icon.png"]];
//            if (tempgalleryObj.title == nil || [tempgalleryObj.title isEqualToString:@""]) {
//                lblTitle.hidden = TRUE;
//            }else{
//                lblTitle.hidden = FALSE;
//                lblTitle = (UILabel *)[cell viewWithTag:223];
//                lblTitle.text = [tempgalleryObj.title capitalizedString];
//            }
//        }else
//        {
//            lblTitle.hidden = TRUE;
//            imageView.image = [UIImage imageNamed:@"photo.png"];
//        }
//    }
    imageView = (UIImageView *)[cell viewWithTag:222];
    lblTitle = (UILabel *)[cell viewWithTag:223];

    if (index < [galleryArray count]) {
        imageView.backgroundColor = [UIColor clearColor];
        if ([[galleryArray objectAtIndex:index] isKindOfClass:[INGalleryObj class]]) {
            INGalleryObj *tempgalleryObj  = [galleryArray objectAtIndex:index];
            [imageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(tempgalleryObj.thumb_url)] placeholderImage:[UIImage imageNamed:@"list_thumbnail.png"]];
            DebugLog(@"index %d : link-%@",index,THUMBNAIL_LINK(tempgalleryObj.thumb_url));
            
            if (tempgalleryObj.title == nil || [tempgalleryObj.title isEqualToString:@""]) {
                lblTitle.hidden = TRUE;
            }else{
                lblTitle.hidden = FALSE;
                lblTitle.text = [tempgalleryObj.title capitalizedString];
            }
        }else
        {
            DebugLog(@"index %d : add image ",index);
            lblTitle.hidden = TRUE;
            imageView.image = [UIImage imageNamed:@"photo.png"];
        }
    }else{
        DebugLog(@"index %d : empty image ",index);
        lblTitle.hidden = TRUE;
        imageView.image = nil;
        imageView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
    }
	return cell;
}

- (BOOL)gridView:(VCGridView *)gridView canEditCellAtIndex:(NSInteger)index
{
//    if (index == 0) {
//        return NO;
//    }
//	return YES;
    if (index < [galleryArray count]) {
        if ([[galleryArray objectAtIndex:index] isKindOfClass:[INGalleryObj class]]) {
            return YES;
        }
    }
    return NO;
}

#pragma mark - VCGridViewDelegate

- (void)gridView:(VCGridView*)gridView didSelectCellAtIndex:(NSInteger)index
{
#if DEBUG
	DebugLog(@"Selected %i", index);
#endif
    if (addImageDescriptionView.hidden) {
//        if (index == 0) {
//            [self addNewImage];
//        }
//        else
//        {
//            DebugLog(@"push to Browser %i", index);
//            _photos = [[NSMutableArray alloc] init];
//            MWPhoto *photo;
//            INGalleryObj *tempgalleryObj;
//            for(int i = 1; i < [galleryArray count]; i++)
//            {
//                tempgalleryObj = (INGalleryObj *)[galleryArray objectAtIndex:i];
//                DebugLog(@"title--- %@",tempgalleryObj.title);
//                photo = [[MWPhoto alloc] initWithURL:[NSURL URLWithString:THUMBNAIL_LINK(tempgalleryObj.image_url)]];
//                if (tempgalleryObj.title != nil || ![tempgalleryObj.title isEqualToString:@""])
//                    photo.caption = [NSString stringWithFormat:@"%@",tempgalleryObj.title];
//                [_photos addObject:photo];
//                tempgalleryObj = nil;
//            }
//            MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
//            browser.displayActionButton = YES;
//            browser.wantsFullScreenLayout = YES;
//            [browser setInitialPageIndex:index-1];
//            [self.navigationController pushViewController:browser animated:YES];
//        }
        
        if (index < [galleryArray count]) {
            if ([[galleryArray objectAtIndex:index] isKindOfClass:[INGalleryObj class]]) {
                DebugLog(@"push to Browser %i", index);
                _photos = [[NSMutableArray alloc] init];
                MWPhoto *photo;
                INGalleryObj *tempgalleryObj;
                for(int i = 1; i < [galleryArray count]; i++)
                {
                    tempgalleryObj = (INGalleryObj *)[galleryArray objectAtIndex:i];
                    DebugLog(@"title--- %@",tempgalleryObj.title);
                    photo = [[MWPhoto alloc] initWithURL:[NSURL URLWithString:THUMBNAIL_LINK(tempgalleryObj.image_url)]];
                    if (tempgalleryObj.title != nil || ![tempgalleryObj.title isEqualToString:@""])
                        photo.caption = [NSString stringWithFormat:@"%@",tempgalleryObj.title];
                    [_photos addObject:photo];
                    tempgalleryObj = nil;
                }
//                MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
//                browser.displayActionButton = YES;
//                browser.wantsFullScreenLayout = YES;
//                [browser setInitialPageIndex:index-1];
//                [self.navigationController pushViewController:browser animated:YES];
                
                // Create browser
                MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
                // Set options
                browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
                browser.wantsFullScreenLayout = YES; // iOS 5 & 6 only: Decide if you want the photo browser full screen, i.e. whether the status bar is affected (defaults to YES)
                
                browser.displayNavArrows = YES; // Whether to display left and right nav arrows on toolbar (defaults to NO)
                browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
                browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
                browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
                browser.enableGrid = NO; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
                browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
                
                // Optionally set the current visible photo before displaying
                [browser setCurrentPhotoIndex:index-1];
                
                // Present
                [self.navigationController pushViewController:browser animated:YES];
            }else
            {
                [self addNewImage];
            }
        }else{
              DebugLog(@"You clicked empty image. :)");
        }
    }
}

- (CGSize)sizeForCellsInGridView:(VCGridView *)gridView
{
	return CGSizeMake(85.0f, 85.0f);
}

- (CGFloat)paddingForCellsInGridView:(VCGridView *)gridView
{
	return 15.0f;
}

#pragma MWPhotoBrowser delegate methods
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser
{
    return [_photos count];
}
- (id<MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index
{
    return [_photos objectAtIndex:index];
}

- (IBAction)descriptionViewSetBtnPressed:(id)sender {
    [addImageDescriptionView setHidden:TRUE];
    [self.view sendSubviewToBack:addImageDescriptionView];
    [self sendAddGalleryImageIntoStoreImageRequest:editedImage];
    addImageDescriptionView.frame = CGRectMake(addImageDescriptionView.frame.origin.x, 58, addImageDescriptionView.frame.size.width, addImageDescriptionView.frame.size.height);
}

- (IBAction)descriptionViewSkipBtnPressed:(id)sender {
    descriptionViewtitleTextView.text = @"";
    [self descriptionViewSetBtnPressed:nil];
}

#pragma UITextView delegate methods
- (void)textViewDidBeginEditing:(UITextView *)textView{
    if (!addImageDescriptionView.hidden  && [textView isEqual:descriptionViewtitleTextView]) {
        [UIView animateWithDuration:0.2 animations:^{
            DebugLog(@"self.view.frame.size.height %f",self.view.frame.size.height);
            addImageDescriptionView.frame = CGRectMake(addImageDescriptionView.frame.origin.x,self.view.frame.size.height - 516, addImageDescriptionView.frame.size.width, addImageDescriptionView.frame.size.height);
            
            // assuming that 300 = addImageDescriptionView height and 216 is keyboard height
        } completion:^(BOOL finished) {
        }];
    }
}

-(void) popStoreGalleryController
{
    DebugLog(@"popToRootViewControllerAnimated:YES");
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
