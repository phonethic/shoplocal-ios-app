//
//  INSureShopViewController.h
//  shoplocal
//
//  Created by Kirti Nikam on 07/10/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@class INDateTypeObject;
@interface INSureShopViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    INDateTypeObject *dateTypeObj;
}
@property (strong,nonatomic) NSMutableDictionary *dateCategoriesDict;

@property (nonatomic, readwrite) NSInteger placeId;
@property (nonatomic, copy) NSString *storeName;

@property (nonatomic, copy) NSString *dataFilePath;
@property (nonatomic, retain) NSMutableDictionary *datesDict;
@property (nonatomic, retain) NSMutableArray *dateTypeArray;

@property (strong, nonatomic) IBOutlet UITableView *eventsTableView;

@property (strong, nonatomic) IBOutlet UIButton *infoBtn;
@property (nonatomic,strong) NSString *selectedSpecialDate;
- (IBAction)infoBtnPressed:(id)sender;

@end
