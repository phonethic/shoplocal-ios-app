//
//  INCustomerSettingsViewController.h
//  shoplocal
//
//  Created by Kirti Nikam on 23/09/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface INCustomerSettingsViewController : UIViewController <MFMailComposeViewControllerDelegate,UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, readwrite) NSInteger selectedIndex;


@property (strong, nonatomic) IBOutlet UITableView *settingsTableView;


@end
