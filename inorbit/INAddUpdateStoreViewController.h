//
//  INAddUpdateStoreViewController.h
//  shoplocal
//
//  Created by Rishi on 14/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "ActionSheetPicker.h"
#import "VCGridView.h"
#import "MWPhotoBrowser.h"
#import "ALPickerView.h"
#import "INShoplocalImageViewController.h"
#import "CLImageEditor.h"

@class AbstractActionSheetPicker;
@interface INAddUpdateStoreViewController : UIViewController <UIScrollViewDelegate, CLLocationManagerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate,VCGridViewDelegate, VCGridViewDataSource,MWPhotoBrowserDelegate,ALPickerViewDelegate,UITextViewDelegate,ShoplocalImageViewControllerDelegate,CLImageEditorDelegate>
{
    BOOL pageControlUsed;
    CLLocationManager *locationManager;
    VCGridView *_gridView;
	BOOL _isInputViewVisible;
    int imgPickerTag;
     MBProgressHUD *hud;
    UIAlertView *backAlert;
    
    ///////////////////Multi Select Picker//////////////
    NSArray *entries;
	NSMutableDictionary *selectionStates;
	NSMutableArray *resultArray;
	ALPickerView *pickerView;
    UIActionSheet *actionSheet;
    int mobileNumberCount;
    int landlineNumberCount;
    int isImgFrmShopLocalGallery;
}

///////////////////Multi Select Picker//////////////
@property (strong, nonatomic) IBOutlet UIView *galleryView;
///////////////////////////////////////////////////
@property (strong, nonatomic) NSMutableDictionary *searchdict;
@property (nonatomic, retain) VCGridView *gridView;
@property (strong, nonatomic) NSMutableArray *galleryArray;
@property (strong, nonatomic) NSMutableArray *photos;
@property (strong, nonatomic) IBOutlet UITextField *storenameTextField;
@property (strong, nonatomic) IBOutlet UITextField *areaTextField;
@property (strong, nonatomic) IBOutlet UITextField *cityTextField;
@property (strong, nonatomic) IBOutlet UITextField *pincodeTextField;
@property (strong, nonatomic) IBOutlet UIScrollView *storeScrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (strong, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (strong, nonatomic) IBOutlet UITextField *streetTextView;
@property (strong, nonatomic) IBOutlet UITextField *landmarkTextView;
@property (strong, nonatomic) IBOutlet UITextField *landlineTextField;
@property (strong, nonatomic) IBOutlet UITextField *mobileNoTextField;
@property (strong, nonatomic) IBOutlet UITextField *faxNoTextField;
@property (strong, nonatomic) IBOutlet UITextField *tollfreeTextField;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *websiteTextField;
@property (strong, nonatomic) IBOutlet UIButton *sunBtn;
@property (strong, nonatomic) IBOutlet UIButton *monBtn;
@property (strong, nonatomic) IBOutlet UIButton *tueBtn;
@property (strong, nonatomic) IBOutlet UIButton *wedBtn;
@property (strong, nonatomic) IBOutlet UIButton *thurBtn;
@property (strong, nonatomic) IBOutlet UIButton *friBtn;
@property (strong, nonatomic) IBOutlet UIButton *satBtn;
@property (strong, nonatomic) IBOutlet UIButton *categoryBtn;
@property (readwrite,nonatomic) double currentLatitude;
@property (readwrite,nonatomic) double currentLongitude;
@property (nonatomic, strong) NSString *placeId;
@property (strong, nonatomic) IBOutlet UIImageView *mapImageView;
@property (strong, nonatomic) IBOutlet UILabel *maplbl;
@property (nonatomic) BOOL newMedia;
@property (strong, nonatomic) IBOutlet UIImageView *iconImageView;
@property (nonatomic, strong) AbstractActionSheetPicker *fromactionSheetPicker;
@property (nonatomic, strong) AbstractActionSheetPicker *toactionSheetPicker;
@property (nonatomic, strong) NSDate *selectedDate;
@property (nonatomic, strong) NSMutableDictionary *categoryDict;
@property (nonatomic, assign) NSInteger selectedIndex;
@property (strong, nonatomic) IBOutlet UIButton *toDateBtn;
@property (strong, nonatomic) IBOutlet UIButton *fromDateBtn;
@property (nonatomic, strong) NSDate *fromselectedDate;
@property (nonatomic, strong) NSDate *toselectedDate;
@property (strong, nonatomic) IBOutlet UILabel *fromDatelbl;
@property (strong, nonatomic) IBOutlet UILabel *toDatelbl;
@property (strong, nonatomic) IBOutlet UIScrollView *locationScrollView;
@property (strong, nonatomic) IBOutlet UIScrollView *contactScrollView;
@property (strong, nonatomic) IBOutlet UIScrollView *descriptionScrollView;
@property (strong, nonatomic) IBOutlet UILabel *gallerymsglbl;
@property (strong, nonatomic) IBOutlet UIView *backView;
@property (strong, nonatomic) IBOutlet UIView *backview2;
@property (strong, nonatomic) IBOutlet UIView *backview3;
@property (strong, nonatomic) IBOutlet UIView *backView4;
@property (strong, nonatomic) IBOutlet UILabel *openlbl;
@property (strong, nonatomic) IBOutlet UILabel *openonlbl;
@property (strong, nonatomic) IBOutlet UILabel *textviewplaceholderLabel;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttonsCollection;
@property (nonatomic,strong) UIImage *editedImage;


//  addImageDescriptionView : to add image description
@property (strong, nonatomic) IBOutlet UIView *addImageDescriptionView;
@property (strong, nonatomic) IBOutlet UILabel *lbldescriptionViewHeader;
@property (strong, nonatomic) IBOutlet UILabel *lbldescriptionViewContent;
@property (strong, nonatomic) IBOutlet UIButton *descriptionViewSetBtn;
@property (strong, nonatomic) IBOutlet UIButton *descriptionViewSkipBtn;
@property (strong, nonatomic) IBOutlet UITextView *descriptionViewtitleTextView;
// end of addImageDescriptionView outlets

@property (strong, nonatomic) IBOutlet UIButton *locationInfoBtn;
@property (strong, nonatomic) IBOutlet UIImageView *imgSeperator1;
@property (strong, nonatomic) IBOutlet UIImageView *imgSeperator2;
@property (strong, nonatomic) IBOutlet UIImageView *imgSeperator3;
@property (strong, nonatomic) IBOutlet UIImageView *imgSeperator4;
@property (strong, nonatomic) IBOutlet UIImageView *imgSeperator5;
@property (strong, nonatomic) IBOutlet UIImageView *imgSeperator6;
@property (strong, nonatomic) IBOutlet UIScrollView *contactBackScrollView;
@property (strong, nonatomic) IBOutlet UIButton *addLandlineNumberBtn;
@property (strong, nonatomic) IBOutlet UITableView *LandlineNumberTableView;
@property (strong, nonatomic) IBOutlet UIButton *addMoblieNumberBtn;
@property (strong, nonatomic) IBOutlet UITableView *mobileNumberTableView;
@property (strong, nonatomic) IBOutlet UILabel *lblstoreName;
@property (strong, nonatomic) IBOutlet UILabel *lblstoreDescription;
@property (retain, nonatomic) NSMutableArray *mobileArray;
@property (retain, nonatomic) NSMutableArray *landlineArray;
@property (retain, nonatomic) NSMutableArray *areaArray;
@property (nonatomic, copy) NSString *place_status;
@property (nonatomic, copy) NSString *shoplocalImage;
@property (strong, nonatomic) IBOutlet UILabel *conactscreenlbl;

@property (strong, nonatomic) IBOutlet UIImageView *imgSeperator7;
@property (strong, nonatomic) IBOutlet UIImageView *imgSeperator8;
@property (strong, nonatomic) IBOutlet UITextField *txtFbUrl;
@property (strong, nonatomic) IBOutlet UITextField *txtTwitterUrl;

@property (strong, nonatomic) IBOutlet UIButton *mapskipBtn;
@property (strong, nonatomic) IBOutlet UIButton *contactdetailsSkipBtn;
@property (strong, nonatomic) IBOutlet UIButton *descriptionSkipBtn;
@property (strong, nonatomic) IBOutlet UIButton *storeLogoSkipBtn;

@property (strong, nonatomic) IBOutlet UIButton *saveMapLocationBtn;
@property (strong, nonatomic) IBOutlet UIButton *saveLocationBtn;
@property (strong, nonatomic) IBOutlet UIButton *saveContactDetailsBtn;
@property (strong, nonatomic) IBOutlet UIButton *descriptionSaveBtn;
@property (strong, nonatomic) IBOutlet UIButton *daysBtn;
@property (strong, nonatomic) IBOutlet UIButton *doneBtn;
@property (strong,nonatomic) UIView *customView;

- (IBAction)addLandlineNumberBtnPressed:(id)sender;
- (IBAction)addMoblieNumberBtnPressed:(id)sender;
- (IBAction)locationInfoBtnPressed:(id)sender;
- (IBAction)saveMapLocationBtnPressed:(id)sender;
- (IBAction)descriptionViewSetBtnPressed:(id)sender;
- (IBAction)descriptionViewSkipBtnPressed:(id)sender;
- (IBAction)pageChange:(id)sender;
- (IBAction)droppibBtnPressed:(id)sender;
- (IBAction)mapSkipBtnPressed:(id)sender;
- (IBAction)saveLocationBtnPressed:(id)sender;
- (IBAction)saveContactDetailsBtnPressed:(id)sender;
- (IBAction)descriptionSaveBtnPressed:(id)sender;
- (IBAction)descriptionSkipBtnPressed:(id)sender;
- (IBAction)uploadlogoBtnPressed:(id)sender;
- (IBAction)dayBtnPressed:(UIButton *)sender;
- (IBAction)daysBtnPressed:(id)sender;
- (IBAction)doneBtnPressed:(id)sender;
- (IBAction)categoryBtnPressed:(id)sender;
- (IBAction)setFromDate:(id)sender;
- (IBAction)setToDate:(id)sender;
- (IBAction)storeLogoSkipBtnPressed:(id)sender;
- (IBAction)contactdetailsSkipBtnPressed:(id)sender;
@end
