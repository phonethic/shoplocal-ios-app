//
//  MerchantSideMenuViewController.h
//  shoplocal
//
//  Created by Kirti Nikam on 01/11/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MFSideMenu.h"

@interface MerchantSideMenuViewController : UITableViewController{
    int currentSectionIndex;
    int preSectionIndex;
    int currentRowIndex;
    int preRowIndex;
}
@property (nonatomic, retain) NSMutableDictionary *sideMenuDict;
@property (nonatomic, assign) MFSideMenu *sideMenu;
@end
