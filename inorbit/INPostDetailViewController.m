//
//  INPostDetailViewController.m
//  shoplocal
//
//  Created by Rishi on 20/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import <Social/Social.h>

#import "INPostDetailViewController.h"
#import "constants.h"
#import "FaceBookShareViewController.h"
#import "INGalleryObj.h"
#import "CommonCallback.h"
#import "INAppDelegate.h"
#import "INAppDelegate.h"
#import "FacebookShareAPI.h"
#import "FacebookOpenGraphAPI.h"

#define LIKE_STORE [NSString stringWithFormat:@"%@%@broadcast_api/like",URL_PREFIX,API_VERSION]
#define UNLIKE_STORE(ID) [NSString stringWithFormat:@"%@%@broadcast_api/like?post_id=%d",URL_PREFIX,API_VERSION,ID]

#define SHARE_STORE [NSString stringWithFormat:@"%@%@broadcast_api/share",URL_PREFIX,API_VERSION]

#define THUMBNAIL_LINK(IMG_LINK) [NSString stringWithFormat:@"%@%@%@",LIVE_SERVER,URL_PREFIX,IMG_LINK]

#define BROADCAST_DETAIL(ID) [NSString stringWithFormat:@"%@%@%@broadcast_api/broadcasts?post_id=%d",LIVE_SERVER,URL_PREFIX,API_VERSION,ID]

@interface INPostDetailViewController ()
@property(strong) NSDictionary *splashJson;
@end

@implementation INPostDetailViewController
@synthesize titlelbl;
@synthesize descriptionlbl;
@synthesize postimageView;
@synthesize postTitle;
@synthesize postDescription;
@synthesize postimageUrl,postimageTitle;
@synthesize postId;
@synthesize likepostBtn;
@synthesize postType;
@synthesize postlikeType;
@synthesize storeName;
@synthesize galleryArray;
@synthesize photos = _photos;
@synthesize sharepostBtn,viewpostBtn;
@synthesize postView;
@synthesize splashJson;
@synthesize sharepostBtn1;
@synthesize likescountString,sharecountString,viewscountString;
@synthesize scrollView;
@synthesize likepostBtnImageView,sharepostBtnImageView,viewpostBtnImageView;
@synthesize himgSeperator1,himgSeperator2,himgSeperator3;
@synthesize vimgSeperator1,vimgSeperator2;
@synthesize sharepostBtn1ImageView;
@synthesize postIsOffered,postOfferDateTime;
@synthesize lblPostedOn,lblOfferDate;
@synthesize himgSeperator4;
@synthesize postDateTime;
@synthesize placeId;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma view lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setUI];
    galleryArray = [[NSMutableArray alloc] init];
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    viewTap.numberOfTapsRequired = 1;
    viewTap.cancelsTouchesInView = NO;
    [postimageView  addGestureRecognizer:viewTap];
    
    //[self addHUDView];
    
    titlelbl.text = self.postTitle;
    if (self.postDescription != nil && ![self.postDescription isEqualToString:@""]) {
        descriptionlbl.text = self.postDescription;
    }else{
        descriptionlbl.text = DESCRIPTION_PLACEHOLDER_TEXT;
    }
    DebugLog(@"imageurl %@",postimageUrl);
    if(postimageUrl != (NSString *)[NSNull null] && ![postimageUrl isEqualToString:@""])
        [postimageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(postimageUrl)] placeholderImage:[UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"]];
    
    [likepostBtn setTitle:likescountString forState:UIControlStateNormal];
    [likepostBtn setTitle:likescountString forState:UIControlStateHighlighted];

    [sharepostBtn setTitle:sharecountString forState:UIControlStateNormal];
    [sharepostBtn setTitle:sharecountString forState:UIControlStateHighlighted];

    [viewpostBtn setTitle:viewscountString forState:UIControlStateNormal];
    [viewpostBtn setTitle:viewscountString forState:UIControlStateHighlighted];
    
    [self adjustViewsAccordingToViewComeFrom];

    if ([IN_APP_DELEGATE networkavailable]) {
        [self sendGetBroadcastDetailsRequest];
    }
//    else{
//        UIAlertView *errorView = [[UIAlertView alloc]
//                                  initWithTitle:@"No Network Connection"
//                                  message:@"Please check your internet connection and try again."
//                                  delegate:nil
//                                  cancelButtonTitle:@"OK"
//                                  otherButtonTitles:nil];
//        [errorView show];
//    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)dealloc {
//    [self removeHUDView];
//}

- (void)viewDidUnload {
    [self setTitlelbl:nil];
    [self setDescriptionlbl:nil];
    [self setPostimageView:nil];
    [self setLikepostBtn:nil];
    [self setPostView:nil];
    [self setSharepostBtn:nil];
    [self setViewpostBtn:nil];
    [self setSharepostBtn1:nil];
    [self setScrollView:nil];
    [self setLikepostBtnImageView:nil];
    [self setSharepostBtnImageView:nil];
    [self setViewpostBtnImageView:nil];
    [self setVimgSeperator1:nil];
    [self setVimgSeperator2:nil];
    [self setHimgSeperator1:nil];
    [self setHimgSeperator2:nil];
    [self setHimgSeperator3:nil];
    [self setSharepostBtn1ImageView:nil];
    [self setHimgSeperator3:nil];
    [super viewDidUnload];
}

#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma internal methods
//-(void)adjustViewsAccordingToViewComeFrom{
//    
//    CGSize titleSize = [postTitle sizeWithFont:titlelbl.font constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
//    
//    CGSize descriptionSize = [postDescription sizeWithFont:descriptionlbl.font constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
//    
//    [titlelbl setFrame:CGRectMake(titlelbl.frame.origin.x,
//                                  titlelbl.frame.origin.y,
//                                  titlelbl.frame.size.width,
//                                  titleSize.height + 30)];
//    
//    [himgSeperator1 setFrame:CGRectMake(himgSeperator1.frame.origin.x,
//                                       CGRectGetMaxY(titlelbl.frame),
//                                       himgSeperator1.frame.size.width,
//                                       himgSeperator1.frame.size.height)];
//    
//    [descriptionlbl setFrame:CGRectMake(descriptionlbl.frame.origin.x,
//                                        CGRectGetMaxY(himgSeperator1.frame),
//                                        descriptionlbl.frame.size.width,
//                                        descriptionSize.height + 40)];
//    
//    [himgSeperator2 setFrame:CGRectMake(himgSeperator2.frame.origin.x,
//                                       CGRectGetMaxY(descriptionlbl.frame),
//                                       himgSeperator2.frame.size.width,
//                                       himgSeperator2.frame.size.height)];
//    
//    [postimageView setFrame:CGRectMake(postimageView.frame.origin.x,
//                                       CGRectGetMaxY(descriptionlbl.frame),
//                                       postimageView.frame.size.width,
//                                       postimageView.frame.size.height)];
//    
//    [himgSeperator3 setFrame:CGRectMake(himgSeperator3.frame.origin.x,
//                                        CGRectGetMaxY(postimageView.frame)-2,
//                                        himgSeperator3.frame.size.width,
//                                        himgSeperator3.frame.size.height)];
//    
//    double yaxis = CGRectGetMaxY(himgSeperator2.frame);
//    if(postimageUrl == nil || [postimageUrl isEqualToString:@""])
//    {
//        [postimageView setHidden:YES];
//        yaxis = CGRectGetMaxY(himgSeperator2.frame);
//
//    }else{
//        [postimageView setHidden:NO];
//        yaxis = CGRectGetMaxY(himgSeperator3.frame);
//    }
//    
//    [vimgSeperator1 setFrame:CGRectMake(vimgSeperator1.frame.origin.x,
//                                        yaxis-4,
//                                        vimgSeperator1.frame.size.width,
//                                        vimgSeperator1.frame.size.height)];
//    
//    [vimgSeperator2 setFrame:CGRectMake(vimgSeperator2.frame.origin.x,
//                                        yaxis-4,
//                                        vimgSeperator2.frame.size.width,
//                                        vimgSeperator2.frame.size.height)];
//    
//    [likepostBtn setFrame:CGRectMake(likepostBtn.frame.origin.x,
//                                     yaxis+10,
//                                     likepostBtn.frame.size.width,
//                                     likepostBtn.frame.size.height)];
//    
//    [sharepostBtn setFrame:CGRectMake(sharepostBtn.frame.origin.x,
//                                      yaxis+10,
//                                      sharepostBtn.frame.size.width,
//                                      sharepostBtn.frame.size.height)];
//    
//    [viewpostBtn setFrame:CGRectMake(viewpostBtn.frame.origin.x,
//                                     yaxis+10,
//                                     viewpostBtn.frame.size.width,
//                                     viewpostBtn.frame.size.height)];
//    
//    [likepostBtnImageView setFrame:CGRectMake(likepostBtnImageView.frame.origin.x,
//                                              likepostBtn.frame.origin.y+8,
//                                              likepostBtnImageView.frame.size.width,
//                                              likepostBtnImageView.frame.size.height)];
//    
//    [sharepostBtnImageView setFrame:CGRectMake(sharepostBtnImageView.frame.origin.x,
//                                               sharepostBtn.frame.origin.y+8,
//                                               sharepostBtnImageView.frame.size.width,
//                                               sharepostBtnImageView.frame.size.height)];
//    
//    [viewpostBtnImageView setFrame:CGRectMake(viewpostBtnImageView.frame.origin.x,
//                                              viewpostBtn.frame.origin.y+8,
//                                              viewpostBtnImageView.frame.size.width,
//                                              viewpostBtnImageView.frame.size.height)];
//    
//    [postView setFrame:CGRectMake(postView.frame.origin.x,
//                                  postView.frame.origin.y,
//                                  postView.frame.size.width,
//                                  CGRectGetMaxY(sharepostBtn.frame))];
//    
//    [sharepostBtn1 setFrame:CGRectMake(sharepostBtn1.frame.origin.x,
//                                       CGRectGetMaxY(postView.frame)+15,
//                                       sharepostBtn1.frame.size.width,
//                                       sharepostBtn1.frame.size.height)];
//    [sharepostBtn1ImageView setFrame:CGRectMake(sharepostBtn1ImageView.frame.origin.x,
//                                                sharepostBtn1.frame.origin.y+5,
//                                                sharepostBtn1ImageView.frame.size.width,
//                                                sharepostBtn1ImageView.frame.size.height)];
//    
//    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, CGRectGetMaxY(sharepostBtn1.frame)+20);
//}

-(void)adjustViewsAccordingToViewComeFrom{
    
    CGSize titleSize = [postTitle sizeWithFont:titlelbl.font constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
    
    CGSize descriptionSize = [postDescription sizeWithFont:descriptionlbl.font constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
    
    [titlelbl setFrame:CGRectMake(titlelbl.frame.origin.x,
                                  titlelbl.frame.origin.y,
                                  titlelbl.frame.size.width,
                                  titleSize.height + 30)];
    
    [himgSeperator1 setFrame:CGRectMake(himgSeperator1.frame.origin.x,
                                        CGRectGetMaxY(titlelbl.frame),
                                        himgSeperator1.frame.size.width,
                                        himgSeperator1.frame.size.height)];
    
    double nextYaxis = CGRectGetMaxY(himgSeperator1.frame);
    if(postDescription == nil || [[postDescription stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0)
    {
        descriptionlbl.hidden = YES;
        himgSeperator2.hidden = YES;
    }else{
        descriptionlbl.hidden = NO;
        himgSeperator2.hidden = NO;
        descriptionlbl.text = postDescription;
        [descriptionlbl setFrame:CGRectMake(descriptionlbl.frame.origin.x,
                                            CGRectGetMaxY(himgSeperator1.frame),
                                            descriptionlbl.frame.size.width,
                                            descriptionSize.height + 40)];
        [himgSeperator2 setFrame:CGRectMake(himgSeperator2.frame.origin.x,
                                           CGRectGetMaxY(descriptionlbl.frame),
                                           himgSeperator2.frame.size.width,
                                           himgSeperator2.frame.size.height)];
        nextYaxis = CGRectGetMaxY(descriptionlbl.frame);
    }
    
    if(postimageUrl == nil || [postimageUrl isEqualToString:@""])
    {
        postimageView.hidden = YES;
    }else{
        if (postDescription == nil || [[postDescription stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0) {
            himgSeperator1.hidden = YES;
            nextYaxis = CGRectGetMaxY(titlelbl.frame);
        }
        postimageView.hidden = NO;
        [postimageView setFrame:CGRectMake(postimageView.frame.origin.x,
                                           nextYaxis+postView.frame.origin.y,
                                           postimageView.frame.size.width,
                                           postimageView.frame.size.height)];
        nextYaxis = CGRectGetMaxY(postimageView.frame)  - postView.frame.origin.y;
    }
    
    //Adjust lblOfferDate
    [himgSeperator3 setHidden:YES];
    [lblOfferDate setHidden:YES];
    if (postIsOffered && postOfferDateTime != nil && ![postOfferDateTime isEqualToString:@""]) {
        NSString *postOfferFormatString = [self formatOfferDateTime:postOfferDateTime];
        if ([postOfferFormatString length] > 0)
        {
            [himgSeperator3 setHidden:NO];
            [lblOfferDate setHidden:NO];
            lblOfferDate.text =  [NSString stringWithFormat:@"Offer Valid Till: %@",postOfferFormatString];
            CGSize postOfferDateTimeSize = [postOfferDateTime sizeWithFont:lblOfferDate.font constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
            
            [lblOfferDate setFrame:CGRectMake(lblOfferDate.frame.origin.x,
                                              nextYaxis,
                                              lblOfferDate.frame.size.width,
                                              postOfferDateTimeSize.height + 30)];
            
            [himgSeperator3 setFrame:CGRectMake(himgSeperator3.frame.origin.x,
                                               CGRectGetMaxY(lblOfferDate.frame),
                                               himgSeperator3.frame.size.width,
                                               himgSeperator3.frame.size.height)];
            nextYaxis  = CGRectGetMaxY(himgSeperator3.frame);
        }
    }
    
    //Adjust lblPostedOn
    if (postDateTime != nil && ![postDateTime isEqualToString:@""]) {
        NSString *postOfferFormatString = [self formatOfferDateTime:postDateTime];
        if ([postOfferFormatString length] > 0)
        {
            lblPostedOn.text =  [NSString stringWithFormat:@" %@",postOfferFormatString];
            CGSize postDateSize = [postDateTime sizeWithFont:lblPostedOn.font constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
            [lblPostedOn setFrame:CGRectMake(lblPostedOn.frame.origin.x,
                                             nextYaxis+3,
                                             lblPostedOn.frame.size.width,
                                             postDateSize.height+10)];
            nextYaxis  = CGRectGetMaxY(lblPostedOn.frame);
        }
    }
    
    [himgSeperator4 setFrame:CGRectMake(himgSeperator4.frame.origin.x,
                                        nextYaxis,
                                        himgSeperator4.frame.size.width,
                                        himgSeperator4.frame.size.height)];

    nextYaxis = CGRectGetMaxY(himgSeperator4.frame);
    
    [vimgSeperator1 setFrame:CGRectMake(vimgSeperator1.frame.origin.x,
                                        nextYaxis-4,
                                        vimgSeperator1.frame.size.width,
                                        vimgSeperator1.frame.size.height)];
    
    [vimgSeperator2 setFrame:CGRectMake(vimgSeperator2.frame.origin.x,
                                        nextYaxis-4,
                                        vimgSeperator2.frame.size.width,
                                        vimgSeperator2.frame.size.height)];
    
    [likepostBtn setFrame:CGRectMake(likepostBtn.frame.origin.x,
                                     nextYaxis+10,
                                     likepostBtn.frame.size.width,
                                     likepostBtn.frame.size.height)];
    
    [sharepostBtn setFrame:CGRectMake(sharepostBtn.frame.origin.x,
                                      nextYaxis+10,
                                      sharepostBtn.frame.size.width,
                                      sharepostBtn.frame.size.height)];
    
    [viewpostBtn setFrame:CGRectMake(viewpostBtn.frame.origin.x,
                                     nextYaxis+10,
                                     viewpostBtn.frame.size.width,
                                     viewpostBtn.frame.size.height)];
    
    [likepostBtnImageView setFrame:CGRectMake(likepostBtnImageView.frame.origin.x,
                                              likepostBtn.frame.origin.y+5,
                                              likepostBtnImageView.frame.size.width,
                                              likepostBtnImageView.frame.size.height)];
    
    [sharepostBtnImageView setFrame:CGRectMake(sharepostBtnImageView.frame.origin.x,
                                               sharepostBtn.frame.origin.y+5,
                                               sharepostBtnImageView.frame.size.width,
                                               sharepostBtnImageView.frame.size.height)];
    
    [viewpostBtnImageView setFrame:CGRectMake(viewpostBtnImageView.frame.origin.x,
                                              viewpostBtn.frame.origin.y+8,
                                              viewpostBtnImageView.frame.size.width,
                                              viewpostBtnImageView.frame.size.height)];
    
    [postView setFrame:CGRectMake(postView.frame.origin.x,
                                  postView.frame.origin.y,
                                  postView.frame.size.width,
                                  CGRectGetMaxY(sharepostBtn.frame))];
    
    [sharepostBtn1 setFrame:CGRectMake(sharepostBtn1.frame.origin.x,
                                       CGRectGetMaxY(postView.frame)+15,
                                       sharepostBtn1.frame.size.width,
                                       sharepostBtn1.frame.size.height)];
    [sharepostBtn1ImageView setFrame:CGRectMake(sharepostBtn1ImageView.frame.origin.x,
                                                sharepostBtn1.frame.origin.y+2,
                                                sharepostBtn1ImageView.frame.size.width,
                                                sharepostBtn1ImageView.frame.size.height)];
    
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, CGRectGetMaxY(sharepostBtn1.frame)+20);
}

-(NSString *)formatOfferDateTime:(NSString *)offerdate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSDate *myDate = [[NSDate alloc] init];
    myDate = [dateFormatter dateFromString:offerdate];
    DebugLog(@"MyDate is: %@", myDate);
    
    NSDateFormatter *dayformatter = [[NSDateFormatter alloc] init];
    [dayformatter setDateFormat:@"dd MMM YYYY hh:mm a"];
    NSString *dayFromDate = [dayformatter stringFromDate:myDate];
    DebugLog(@"Myday is: %@", dayFromDate);
    
    return dayFromDate;
}

- (void)tapDetected:(UIGestureRecognizer *)sender {
    DebugLog(@"-------------------------");
    if(postimageUrl != nil && ![postimageUrl isEqualToString:@""])
    {
        _photos = [[NSMutableArray alloc] init];
        MWPhoto *photo;
        photo = [[MWPhoto alloc] initWithURL:[NSURL URLWithString:THUMBNAIL_LINK(postimageUrl)]];
        if (postimageTitle != nil && ![postimageTitle isEqualToString:@""]) {
            photo.caption = postimageTitle;
        }
        [_photos addObject:photo];
        
//        MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
//        browser.displayActionButton = YES;
//        browser.wantsFullScreenLayout = YES;
//        [browser setInitialPageIndex:0];
//        [self.navigationController pushViewController:browser animated:YES];
        
        // Create browser
        MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
        // Set options
        browser.displayActionButton = YES; // Show action button to allow sharing, copying, etc (defaults to YES)
        browser.wantsFullScreenLayout = YES; // iOS 5 & 6 only: Decide if you want the photo browser full screen, i.e. whether the status bar is affected (defaults to YES)
        
        browser.displayNavArrows = YES; // Whether to display left and right nav arrows on toolbar (defaults to NO)
        browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
        browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
        browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
        browser.enableGrid = NO; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
        browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
        
        // Optionally set the current visible photo before displaying
        [browser setCurrentPhotoIndex:0];
        
        // Present
        [self.navigationController pushViewController:browser animated:YES];
    }
}

-(void)setUI
{
    //postView = [CommonCallback setViewPropertiesWithRoundedCorner:postView];
    
    postView.backgroundColor =  [UIColor whiteColor];
    postView.clipsToBounds = YES;
    postimageView.backgroundColor   = [UIColor clearColor];
    
    titlelbl.font                   =   DEFAULT_BOLD_FONT(18.0);
    descriptionlbl.font             =   DEFAULT_FONT(16.0);
    likepostBtn.titleLabel.font     =   DEFAULT_FONT(16.0);
    sharepostBtn.titleLabel.font    =   DEFAULT_FONT(16.0);
    viewpostBtn.titleLabel.font     =   DEFAULT_FONT(16.0);

    titlelbl.backgroundColor            =   [UIColor clearColor];
    descriptionlbl.backgroundColor      =   [UIColor clearColor];
    likepostBtn.backgroundColor         =   [UIColor clearColor];
    sharepostBtn.backgroundColor        =   [UIColor clearColor];
    viewpostBtn.backgroundColor         =   [UIColor clearColor];
    postimageView.backgroundColor       =   [UIColor clearColor];

    titlelbl.textColor          =  BROWN_COLOR;
    descriptionlbl.textColor    =  BROWN_COLOR;
    
    titlelbl.numberOfLines = 0;
    descriptionlbl.numberOfLines = 0;
    
    
    lblPostedOn.font                   =   DEFAULT_FONT(13.0);
    lblPostedOn.textColor              =   BROWN_COLOR;
    lblPostedOn.numberOfLines          =   0;
    lblPostedOn.textAlignment          =   NSTextAlignmentLeft;
    lblPostedOn.backgroundColor        =   [UIColor clearColor];
    
    lblOfferDate.font                   =   DEFAULT_BOLD_FONT(15.0);
    lblOfferDate.textColor              =   BROWN_COLOR;
    lblOfferDate.numberOfLines          =   0;
    lblOfferDate.textAlignment          =   NSTextAlignmentLeft;
    lblOfferDate.backgroundColor        =   [UIColor clearColor];
    
    [likepostBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [likepostBtn setTitleColor:[UIColor colorWithWhite:0.5 alpha:0.5] forState:UIControlStateHighlighted];

    [sharepostBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [sharepostBtn setTitleColor:[UIColor colorWithWhite:0.5 alpha:0.5] forState:UIControlStateHighlighted];
    
    [viewpostBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    
    [likepostBtn setTitle:@"0" forState:UIControlStateNormal];
    [sharepostBtn setTitle:@"0" forState:UIControlStateNormal];
    [viewpostBtn setTitle:@"0" forState:UIControlStateNormal];
    
    likepostBtn.userInteractionEnabled  = FALSE;
    sharepostBtn.userInteractionEnabled = FALSE;
    viewpostBtn.userInteractionEnabled  = FALSE;

//    sharepostBtn1.titleLabel.font   =   DEFAULT_BOLD_FONT(18.0);
//    sharepostBtn1.backgroundColor   =   [UIColor whiteColor];
////    sharepostBtn1.layer.borderColor = BUTTON_BORDER_COLOR.CGColor;
////    sharepostBtn1.layer.borderWidth = 2.0;
////    sharepostBtn1.layer.cornerRadius= 5.0;
//    [sharepostBtn1 setTitleColor:BUTTON_COLOR forState:UIControlStateNormal];
//    [sharepostBtn1 setTitleColor:BUTTON_OFFWHITE_COLOR forState:UIControlStateHighlighted];
    
    sharepostBtn1.titleLabel.font     =   DEFAULT_BOLD_FONT(18.0);
    sharepostBtn1.backgroundColor     =   BUTTON_COLOR;
    [sharepostBtn1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sharepostBtn1 setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.5] forState:UIControlStateHighlighted];
}


-(void)addHUDView
{
    hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:hud];
	hud.dimBackground = NO;
    hud.labelText = @"Loading";
	hud.detailsLabelText = @"Please wait ...";
    [hud hide:NO];
}

-(void)removeHUDView
{
    [hud removeFromSuperview];
    hud = nil;
}

- (IBAction)likepostBtnPressed:(id)sender {
    if(![INUserDefaultOperations isCustomerLoggedIn])
    {
        [INUserDefaultOperations showAlert:@"Please login first to mark a store as favourite."];
        return;
    } else {
        if([likepostBtn isSelected])
        {
            [self sendPostUnLikeRequest];
        } else {
            [self sendPostLikeRequest];
        }
    }
}

- (IBAction)sharepostBtnPressed:(id)sender {
    UIActionSheet *shareActionSheet = [[UIActionSheet alloc] initWithTitle:@"Share via" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"SMS",@"Email",@"WhatsApp",@"Facebook",@"Twitter", nil];
    [shareActionSheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSString *title = [actionSheet buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"SMS"])
    {
        [self smsShareClicked];
    }
    else if([title isEqualToString:@"Email"])
    {
        [self emailShareClicked];
    }else if([title isEqualToString:@"WhatsApp"])
    {
        [self whatsAppShareClicked];
    }
    else if([title isEqualToString:@"Facebook"])
    {
        [self facebookShareClicked];
    } else if([title isEqualToString:@"Twitter"])
    {
        [self twitterShareClicked];
    }
}
-(void)smsShareClicked{
    if ([MFMessageComposeViewController canSendText]) {
        NSString *message = [self getPostDetailsMessageBody:@"sms"];

        MFMessageComposeViewController *messageComposer = [[MFMessageComposeViewController alloc] init];
        messageComposer.messageComposeDelegate = self;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            messageComposer.navigationBar.tintColor  = [UIColor whiteColor];
        }
        [messageComposer setBody:message];
        [messageComposer setRecipients:nil];
        [self presentViewController:messageComposer animated:YES completion:nil];
    }else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"You may not have set up sms service on your device.\nPlease check and try again."
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}
#pragma mark MFMessageComposeViewController Delegate Methods
-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
		case MessageComposeResultCancelled:
            DebugLog(@"SMS Cancelled");
			break;
		case MessageComposeResultFailed:
        {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Unknown Error\nSMS failed"
														   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
        }
			break;
		case MessageComposeResultSent:
        {
            DebugLog(@"SMS Sent");
        }
			break;
		default:
			break;
	}
    [self dismissViewControllerAnimated:YES completion:^{
        if (result == MessageComposeResultSent) {
            [self sendPostShareRequest:@"sms"];
        }
    }];
}

-(void)emailShareClicked{
    if ([MFMailComposeViewController canSendMail]) {
        NSString *message = [self getPostDetailsMessageBody:@"email"];

        MFMailComposeViewController *emailComposer = [[MFMailComposeViewController alloc] init];
        emailComposer.mailComposeDelegate = self;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            emailComposer.navigationBar.tintColor  = [UIColor whiteColor];
        }
        [emailComposer setToRecipients:nil];
        if (storeName != nil && ![storeName isEqualToString:@""]) {
            [emailComposer setSubject:[NSString stringWithFormat:@"%@",storeName]];
        }else{
            [emailComposer setSubject:@""];
        }
        if (postimageUrl != nil && ![postimageUrl isEqualToString:@""]) {
            NSString *htmlMessage = [message stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
            DebugLog(@"%@--%@",message,htmlMessage);
            [emailComposer setMessageBody:[NSString stringWithFormat:@"%@ <br><br><img src=%@ alt=%@/>",htmlMessage,THUMBNAIL_LINK(postimageUrl),postTitle] isHTML:YES];
        }else{
            [emailComposer setMessageBody:message isHTML:NO];
        }
        [self presentViewController:emailComposer animated:YES completion:nil];
    }else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"You may not have set up mail service on your device or You may not connected to internet.\nPlease check and try again."
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}
#pragma mark Mail Compose Delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    if (result == MFMailComposeResultFailed) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                        message:NSLocalizedString(@"Email failed to send. Please try again.", nil)
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", nil) otherButtonTitles:nil];
		[alert show];
    }
    [self dismissViewControllerAnimated:YES completion:^{
        if (result == MFMailComposeResultSent) {
            [self sendPostShareRequest:@"email"];
        }
    }];
}

-(void)whatsAppShareClicked{
    //    NSString *shareMSGBody = [[NSString stringWithFormat:@"Store Name: %@\nOffer: %@\nDescription: %@",storeName,postTitle,postDescription] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *message = [self getPostDetailsMessageBody:@"whatsapp"];

    NSString *urlString = [NSString stringWithFormat:@"whatsapp://send?text=%@",[message stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURL *whatsappURL = [NSURL URLWithString:urlString];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [[UIApplication sharedApplication] openURL: whatsappURL];
        [self sendPostShareRequest:@"whatsapp"];
    }
}

-(void)twitterShareClicked{
    if ([IN_APP_DELEGATE networkavailable]) {
        NSString *message = [self getPostDetailsMessageBody:@"twitter"];

        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
            SLComposeViewController *socialComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
            if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
            {
                if ([message length] > 140) {
                    [socialComposer setInitialText:[message substringToIndex:140]];
                }else{
                    [socialComposer setInitialText:message];
                }
            }
            else{
                [socialComposer setInitialText:message];
            }
            if(postimageUrl != nil && ![postimageUrl isEqualToString:@""])
            {
                UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:THUMBNAIL_LINK(postimageUrl)]]];
                [socialComposer addImage:image];
            }
            [socialComposer setCompletionHandler:^(SLComposeViewControllerResult result){
                switch (result) {
                    case SLComposeViewControllerResultCancelled:
                        DebugLog(@"SLComposeViewControllerResultCancelled");
                        break;
                    case SLComposeViewControllerResultDone:
                        DebugLog(@"SLComposeViewControllerResultDone");
                        [self sendPostShareRequest:@"twitter"];
                        break;
                    default:
                        DebugLog(@"SLComposeViewControllerResultFailed");
                        break;
                }
                [self dismissViewControllerAnimated:YES completion:NULL];
            }];
            [self presentViewController:socialComposer animated:YES completion:Nil];
        }
        else{
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:ALERT_TITLE
                                      message:@"You may not have set up twitter service on your device.Please check and try again."
                                      delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [alertView show];
        }
    }else{
        [INUserDefaultOperations showOfflineAlert];
    }
}

-(void)facebookShareClicked{
    if ([IN_APP_DELEGATE networkavailable]) {
        [self sendPostShareRequest:@"facebook"];

        NSString *message = [self getPostDetailsMessageBody:@"facebook"];
        DebugLog(@"message %@",message);
        NSString *deepLinkingUrl = [FacebookOpenGraphAPI createDeepLinkWithActiveAreaName:[IN_APP_DELEGATE getActiveSlugFromAreaTable] storeName:storeName storeId:[NSString stringWithFormat:@"%d",self.placeId]];
        
        if (postimageUrl != nil && ![postimageUrl isEqualToString:@""]) {
            if ([postimageUrl hasPrefix:@"http"]) {
                [FacebookShareAPI shareContentWithTitle:[NSString stringWithFormat:@"%@ - Offer\n",storeName] description:message image:postimageUrl deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
            }else{
                [FacebookShareAPI shareContentWithTitle:[NSString stringWithFormat:@"%@ - Offer\n",storeName] description:message image:THUMBNAIL_LINK(postimageUrl) deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
            }
        }else{
            [FacebookShareAPI shareContentWithTitle:[NSString stringWithFormat:@"%@ - Offer\n",storeName] description:message image:@"" deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
        }
    }else{
        [INUserDefaultOperations showOfflineAlert];
    }
    
//    if ([IN_APP_DELEGATE networkavailable]) {
//        NSString *message = [self getPostDetailsMessageBody:@"facebook"];
//        
//        if (SYSTEM_VERSION_LESS_THAN(@"6.0"))
//        {
//            FaceBookShareViewController *facebookViewComposer = [[FaceBookShareViewController alloc] initWithNibName:@"FaceBookShareViewController" bundle:nil];
//            facebookViewComposer.fbdelegate = self;
//            facebookViewComposer.title = @"FACEBOOK SHARE";
//            facebookViewComposer.FBtitle = ALERT_TITLE;
//            facebookViewComposer.postMessageText = message;
//            facebookViewComposer.FBtPic = THUMBNAIL_LINK(postimageUrl);
//            UINavigationController *fbnavBar=[[UINavigationController alloc]initWithRootViewController:facebookViewComposer];
//            [self.navigationController presentViewController:fbnavBar animated:YES completion:nil];
//
//        }else{
//            if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
//                SLComposeViewController *socialComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
//                [socialComposer setInitialText:message];
//                if(postimageUrl != nil && ![postimageUrl isEqualToString:@""])
//                {
//                    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:THUMBNAIL_LINK(postimageUrl)]]];
//                    [socialComposer addImage:image];
//                }
//                [socialComposer setCompletionHandler:^(SLComposeViewControllerResult result){
//                    switch (result) {
//                        case SLComposeViewControllerResultCancelled:
//                            DebugLog(@"SLComposeViewControllerResultCancelled");
//                            break;
//                        case SLComposeViewControllerResultDone:
//                            DebugLog(@"SLComposeViewControllerResultDone");
//                            [self sendPostShareRequest:@"facebook"];
//                            break;
//                        default:
//                            DebugLog(@"SLComposeViewControllerResultFailed");
//                            break;
//                    }
//                    [self dismissViewControllerAnimated:YES completion:NULL];
//                }];
//                [self presentViewController:socialComposer animated:YES completion:Nil];
//            }
//            else{
//                UIAlertView *alertView = [[UIAlertView alloc]
//                                          initWithTitle:ALERT_TITLE
//                                          message:@"You may not have set up facebook service on your device.Please check and try again."
//                                          delegate:nil
//                                          cancelButtonTitle:@"OK"
//                                          otherButtonTitles:nil];
//                [alertView show];
//            }
//        }
//    }else{
//        [INUserDefaultOperations showOfflineAlert];
//    }
}

-(void)faceBookCompletionCallBack:(int)result
{
    DebugLog(@"Merchant : faceBookCompletionCallBack %d",result);
    if (result == FACEBOOK_POST_SUCCESS) {
        [self sendPostShareRequest:@"facebook"];
    }
}

#pragma AFNetworking delegate methods
-(void)sendPostLikeRequest
{
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[INUserDefaultOperations getCustomerAuthId] forKey:@"user_id"];
    [params setObject:[INUserDefaultOperations getCustomerAuthCode] forKey:@"auth_id"];
    [params setObject:[NSString stringWithFormat:@"%d",self.postId] forKey:@"post_id"];
    [httpClient postPath:LIKE_STORE parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            if([json  objectForKey:@"message"] != [NSNull null] && [[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
                NSString* message = [json objectForKey:@"message"];
                [INUserDefaultOperations showAlert:message];
                [likepostBtn setSelected:TRUE];
            } else {
                NSString* message = [json objectForKey:@"message"];
                if([message hasPrefix:@"You have already made this post as a favourite."])
                {
                    [likepostBtn setSelected:TRUE];
                }
                [INUserDefaultOperations showAlert:message];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}

-(void)sendPostUnLikeRequest
{
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    [httpClient setDefaultHeader:@"user_id" value:[INUserDefaultOperations getCustomerAuthId]];
    [httpClient setDefaultHeader:@"auth_id" value:[INUserDefaultOperations getCustomerAuthCode]];
    [httpClient deletePath:UNLIKE_STORE(self.postId) parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            if([json  objectForKey:@"message"] != [NSNull null] && [[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
                NSString* message = [json objectForKey:@"message"];
                [INUserDefaultOperations showAlert:message];
                [likepostBtn setSelected:FALSE];
            } else {
                NSString* message = [json objectForKey:@"message"];
                [INUserDefaultOperations showAlert:message];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}

-(void)sendPostShareRequest:(NSString *)viaString{
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
//    if([INUserDefaultOperations isMerchantLoggedIn]){
//        [params setObject:[INUserDefaultOperations getMerchantAuthId] forKey:@"user_id"];
//        [params setObject:[INUserDefaultOperations getMerchantAuthCode] forKey:@"auth_id"];
//    }
    [params setObject:[NSString stringWithFormat:@"%d",self.postId] forKey:@"post_id"];
    [params setObject:viaString forKey:@"via"];
    
    [httpClient postPath:SHARE_STORE parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            NSString* message = [json objectForKey:@"message"];
            [INUserDefaultOperations showAlert:message];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}

-(void)sendGetBroadcastDetailsRequest
{
    DebugLog(@"========================sendGetPlaceDetailsRequest========================");
    DebugLog(@"%@",BROADCAST_DETAIL(self.postId));
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:BROADCAST_DETAIL(self.postId)]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"Response -%@-", self.splashJson);
                                                        if(self.splashJson  != nil) {
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSDictionary* maindict = [self.splashJson  objectForKey:@"data"];
                                                                
                                                                postTitle = [maindict objectForKey:@"title"];
                                                                postDescription = [maindict objectForKey:@"description"];
                                                                
                                                                postimageUrl =  [maindict objectForKey:@"image_url1"];
                                                                postimageTitle = [maindict objectForKey:@"image_title1"];
                                                                
                                                                postlikeType    = [maindict objectForKey:@"user_like"];
                                                                postIsOffered       = [maindict objectForKey:@"is_offered"];
                                                                postOfferDateTime   = [maindict objectForKey:@"offer_date_time"];
                                                                postDateTime  = [maindict objectForKey:@"date"];
                                                                
                                                                DebugLog(@"imageurl %@",postimageUrl);
                                                                if(postimageUrl != nil && ![postimageUrl isEqualToString:@""])
                                                                    [postimageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(postimageUrl)] placeholderImage:[UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"]];
                                                                
                                                                likescountString = [maindict objectForKey:@"total_like"];
                                                                sharecountString = [maindict objectForKey:@"total_share"];
                                                                viewscountString = [maindict objectForKey:@"total_view"];
                                                                
                                                                [likepostBtn setTitle:likescountString forState:UIControlStateNormal];
                                                                [sharepostBtn setTitle:sharecountString forState:UIControlStateNormal];
                                                                [viewpostBtn setTitle:viewscountString forState:UIControlStateNormal];
                                                                
                                                                [self adjustViewsAccordingToViewComeFrom];
                                                            }
                                                        }
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                        [av show];
                                                    }];
    [operation start];
}

#pragma MWPhotoBrowser delegate methods
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser
{
    return [_photos count];
}
- (id<MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index
{
    return [_photos objectAtIndex:index];
}
-(NSString *)getPostDetailsMessageBody:(NSString *)shareVia{
     NSString *message = [IN_APP_DELEGATE getPostDetailsMessageBody:storeName offerTitle:postTitle offerDescription:postDescription isOffered:postIsOffered offerDateTime:postOfferDateTime contact:@"" shareVia:shareVia];
    return message;
}
@end
