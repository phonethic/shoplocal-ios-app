//
//  INCountryCodeObj.h
//  shoplocal
//
//  Created by Rishi on 26/09/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface INCountryCodeObj : NSObject
{
}
@property (nonatomic, copy) NSString *countryName;
@property (nonatomic, copy) NSString *countrydialcode;
@property (nonatomic, copy) NSString *countrycode;
@end
