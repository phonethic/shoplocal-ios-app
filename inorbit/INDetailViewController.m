//
//  INDetailViewController.m
//  shoplocal
//
//  Created by Sagar Mody on 11/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//


//https://www.cocoacontrols.com/controls/ios-filter-control

#import "INDetailViewController.h"
#import "UIImageView+WebCache.h"
#import "INStoreDetailObj.h"
#import "INBroadcastDetailObj.h"
#import "INCustBroadCastDetailsViewController.h"
#import "INGalleryObj.h"
#import "constants.h"
#import "INCustomerLoginViewController.h"
#import "CommonCallback.h"
#import <Twitter/Twitter.h>
#import "ActionSheetPicker.h"
#import "INAppDelegate.h"

#define STORE_DETAILS(ID,USERID) [NSString stringWithFormat:@"%@%@%@place_api/places?place_id=%d&customer_id=%@",LIVE_SERVER,URL_PREFIX,API_VERSION,ID,USERID]
#define STORE_GALLERY(ID) [NSString stringWithFormat:@"%@%@%@place_api/place_gallery?place_id=%d",LIVE_SERVER,URL_PREFIX,API_VERSION,ID]
#define STORE_BROADCAST(ID,USERID) [NSString stringWithFormat:@"%@%@%@broadcast_api/broadcasts?place_id=%d&user_id=%@",LIVE_SERVER,URL_PREFIX,API_VERSION,ID,USERID]
#define THUMBNAIL_LINK(IMG_LINK) [NSString stringWithFormat:@"%@%@%@",LIVE_SERVER,URL_PREFIX,IMG_LINK]
#define LIKE_STORE [NSString stringWithFormat:@"%@%@place_api/like",URL_PREFIX,API_VERSION]
#define UNLIKE_STORE(ID) [NSString stringWithFormat:@"%@%@place_api/like?place_id=%d",URL_PREFIX,API_VERSION,ID]

#define SHARE_STORE [NSString stringWithFormat:@"%@%@place_api/share",URL_PREFIX,API_VERSION]


#define GETDIRECTION_LINK_USING_GOOGLEMAPS_IN_BROWSER(CURLAT,CURLNG,DESTLAT,DESTLNG) [NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%lf,%lf&daddr=%@,%@",CURLAT,CURLNG,DESTLAT,DESTLNG]

#define GETDIRECTION_LINK_USING_GOOGLEMAPS_IN_APP(CURLAT,CURLNG,DESTLAT,DESTLNG) [NSString stringWithFormat:@"comgooglemaps://?saddr=%lf,%lf&daddr=%@,%@&zoom=14&views=traffic&mapmode=standard&directionsmode=transit",CURLAT,CURLNG,DESTLAT,DESTLNG]

#define MAP_STORE(LAT,LONG) [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/staticmap?center=%f,%f&zoom=16&size=300x170&maptype=roadmap&markers=color:blue|label:S|%f,%f&sensor=true&key=AIzaSyCvFovNWeEIPL4355q8L0Chd1urF8n8c0g",LAT,LONG,LAT,LONG]

#define MAP_URL(LAT,LON) [NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%f,%f",LAT,LON]

@interface INDetailViewController ()
@property(strong) NSDictionary *splashJson;
@end

@implementation INDetailViewController
@synthesize carousel;
@synthesize storeId;
@synthesize storeImageUrl;
@synthesize storeArray;
@synthesize galleryArray;
@synthesize broadcastArray;

@synthesize emptyBroadcastlbl;
@synthesize photos = _photos;

@synthesize scrollView;
@synthesize descriptionTextView;
@synthesize actionView,callStoreBtn,likeStore,shareStoreBtn,lblCallStore,lblAddfav,lblShareStore;
@synthesize addressBtn,addressView,address,citylbl,lblstoretime,lblstoreClosed,lblemail,lblwebsite,lblfacebook,lbltwitter;

@synthesize broadcastBtn,broadcastTableView;
@synthesize imgSeperator1,imgSeperator2,imgSeperator3,imgSeperator4;
@synthesize addressDropDownImageView,broadcastDropDownImageView;
@synthesize getDirectionBtn;
@synthesize mapImageView;
@synthesize telArray;
@synthesize selectedTelNumberToCall;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:TRUE];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:FALSE];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                             style:UIBarButtonItemStylePlain
                                                            target:nil
                                                            action:nil];
    [[self navigationItem] setBackBarButtonItem:back];
    
    UITapGestureRecognizer *textViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(textViewTapTapDetected:)];
    textViewTap.numberOfTapsRequired = 1;
    [self.descriptionTextView addGestureRecognizer:textViewTap];
    
    [self setUI];

    storeArray = [[NSMutableArray alloc] init];
    galleryArray = [[NSMutableArray alloc] init];
    broadcastArray = [[NSMutableArray alloc] init];
    carousel.type = iCarouselTypeLinear;
    //[self addHUDView];
    
     NSDictionary *storeDetailParams = [NSDictionary dictionaryWithObjectsAndKeys:[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"Area_Name", self.title,@"StoreName", nil];
    [INEventLogger logEvent:@"StoreDetail_View" withParams:storeDetailParams];
    
    addressView.contentSize = CGSizeMake(self.addressView.frame.size.width, CGRectGetMaxY(getDirectionBtn.frame)+50);
    DebugLog(@"storeimageurl %@",storeImageUrl);
    [addressView setHidden:YES];
    [broadcastTableView setHidden:YES];

    
    if ([IN_APP_DELEGATE networkavailable]) {
        [self sendStoreDetailRequest];
        [self sendStoreGalleryRequest];
        [self sendStoreBroadcastRequest];
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma Internal Methods
-(void)setUI{
    
    self.scrollView.backgroundColor = [UIColor whiteColor];
    
    descriptionTextView.textColor = BROWN_COLOR;
    descriptionTextView.font = DEFAULT_FONT(15);
    descriptionTextView.backgroundColor = [UIColor clearColor];
    descriptionTextView.scrollEnabled = NO;
    descriptionTextView.text = DESCRIPTION_PLACEHOLDER_TEXT;
    actionView.backgroundColor = [UIColor clearColor];
    
    lblCallStore.font = DEFAULT_FONT(12);
    lblAddfav.font = DEFAULT_FONT(12);
    lblShareStore.font = DEFAULT_FONT(12);
   
    lblCallStore.textColor    = BROWN_COLOR;
    lblAddfav.textColor       = BROWN_COLOR;
    lblShareStore.textColor   = BROWN_COLOR;
    
    addressBtn.titleLabel.font = DEFAULT_BOLD_FONT(18);
    broadcastBtn.titleLabel.font = DEFAULT_BOLD_FONT(18);
    
    addressBtn.backgroundColor      = [UIColor whiteColor];
    broadcastBtn.backgroundColor    = [UIColor whiteColor];

    [addressBtn setTitleColor:BROWN_COLOR forState:UIControlStateNormal];
    [addressBtn setTitleColor:BROWN_OFFWHITE_COLOR forState:UIControlStateHighlighted];
    
    [broadcastBtn setTitleColor:BROWN_COLOR forState:UIControlStateNormal];
    [broadcastBtn setTitleColor:BROWN_OFFWHITE_COLOR forState:UIControlStateHighlighted];
    
    addressView.backgroundColor = [UIColor clearColor];
    address.font            = DEFAULT_FONT(13);
    citylbl.font            = DEFAULT_FONT(13);
    lblstoretime.font       = DEFAULT_FONT(13);
    lblstoreClosed.font     = DEFAULT_FONT(13);
    lblemail.font           = DEFAULT_FONT(13);
    lblwebsite.font         = DEFAULT_FONT(13);
    lblfacebook.font        = DEFAULT_FONT(13);
    lbltwitter.font         = DEFAULT_FONT(13);

    address.textColor           = BROWN_COLOR;
    citylbl.textColor           = BROWN_COLOR;
    lblstoretime.textColor      = BROWN_COLOR;
    lblstoreClosed.textColor    = BROWN_COLOR;
    lblemail.textColor          = BROWN_COLOR;
    lblwebsite.textColor        = BROWN_COLOR;
    lblfacebook.textColor       = BROWN_COLOR;
    lbltwitter.textColor        = BROWN_COLOR;
    
    address.backgroundColor         = [UIColor clearColor];
    citylbl.backgroundColor         = [UIColor clearColor];
    lblstoretime.backgroundColor    = [UIColor clearColor];
    lblstoreClosed.backgroundColor  = [UIColor clearColor];
    lblemail.backgroundColor        = [UIColor clearColor];
    lblwebsite.backgroundColor      = [UIColor clearColor];
    lblfacebook.backgroundColor     = [UIColor clearColor];
    lbltwitter.backgroundColor      = [UIColor clearColor];
    mapImageView.backgroundColor    = [UIColor clearColor];
    
    broadcastTableView.backgroundColor = [UIColor clearColor];
    emptyBroadcastlbl.font      = DEFAULT_FONT(20);
    emptyBroadcastlbl.textColor = BROWN_COLOR;
    [emptyBroadcastlbl setHidden:YES];
    
    getDirectionBtn.titleLabel.font = DEFAULT_FONT(18);
    getDirectionBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [getDirectionBtn setHidden:YES];
    [getDirectionBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [getDirectionBtn setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.8] forState:UIControlStateHighlighted];
    
    [broadcastTableView setHidden:TRUE];
    [emptyBroadcastlbl setHidden:TRUE];
}


-(void)addHUDView
{
    hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:hud];
	hud.dimBackground = NO;
    hud.labelText = @"Loading";
	hud.detailsLabelText = @"Please wait ...";
    [hud hide:NO];
}

-(void)removeHUDView
{
    [hud removeFromSuperview];
    hud = nil;
}

//- (void)dealloc {
//    [self removeHUDView];
//}

-(void)sendStoreDetailRequest
{
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    DebugLog(@"url--->%@",STORE_DETAILS(self.storeId,[INUserDefaultOperations getCustomerAuthId]));
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:STORE_DETAILS(self.storeId,[INUserDefaultOperations getCustomerAuthId])]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"-%@-", self.splashJson);
                                                        if(self.splashJson  != nil) {
                                                            [storeArray removeAllObjects];
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSArray* jsonArray = [self.splashJson  objectForKey:@"data"];
                                                                DebugLog(@"\n\n%@\n\n",jsonArray);
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *objdict = [jsonArray  objectAtIndex:index];
                                                                    DebugLog(@"\n\n%@, %d\n\n",objdict,[objdict count]);
                                                                    storeObj = [[INStoreDetailObj alloc] init];
                                                                    storeObj.ID = [[objdict objectForKey:@"id"] integerValue];
                                                                    storeObj.name = [objdict objectForKey:@"name"];
                                                                    storeObj.place_parent = [[objdict objectForKey:@"place_parent"] integerValue];
                                                                    storeObj.description = [objdict objectForKey:@"description"];
                                                                    storeObj.building = [objdict objectForKey:@"building"];
                                                                    storeObj.street = [objdict objectForKey:@"street"];
                                                                    storeObj.landmark = [objdict objectForKey:@"landmark"];
                                                                    storeObj.area = [objdict objectForKey:@"area"];
                                                                    storeObj.pincode = [objdict objectForKey:@"pincode"];
                                                                    storeObj.city = [objdict objectForKey:@"city"];
                                                                    storeObj.state = [objdict objectForKey:@"state"];
                                                                    storeObj.country = [objdict objectForKey:@"country"];
                                                                    storeObj.fb_url = [objdict objectForKey:@"facebook_url"];
                                                                    storeObj.twitter_url = [objdict objectForKey:@"twitter_url"];

                                                                    if (telArray == nil) {
                                                                        telArray = [[NSMutableArray alloc] init];
                                                                    }else{
                                                                        [telArray removeAllObjects];
                                                                    }
                                                                    if([objdict objectForKey:@"mob_no1"] != (NSString *)[NSNull null] && ![[objdict objectForKey:@"mob_no1"] isEqualToString:@""])
                                                                    {
                                                                        storeObj.mob_no1 = [objdict objectForKey:@"mob_no1"];
                                                                        [telArray addObject:[NSString stringWithFormat:@"+%@",storeObj.mob_no1]];
                                                                    }
                                                                    if([objdict objectForKey:@"mob_no2"] != (NSString *)[NSNull null] && ![[objdict objectForKey:@"mob_no2"] isEqualToString:@""])
                                                                    {
                                                                        storeObj.mob_no2 = [objdict objectForKey:@"mob_no2"];
                                                                        [telArray addObject:[NSString stringWithFormat:@"+%@",storeObj.mob_no2]];
                                                                    }
                                                                    if([objdict objectForKey:@"mob_no3"] != (NSString *)[NSNull null] && ![[objdict objectForKey:@"mob_no3"] isEqualToString:@""])
                                                                    {
                                                                        storeObj.mob_no3 = [objdict objectForKey:@"mob_no3"];
                                                                        [telArray addObject:[NSString stringWithFormat:@"+%@",storeObj.mob_no3]];
                                                                    }
                                                                    if([objdict objectForKey:@"tel_no1"] != (NSString *)[NSNull null] && ![[objdict objectForKey:@"tel_no1"] isEqualToString:@""])
                                                                    {
                                                                        storeObj.tel_no1 = [objdict objectForKey:@"tel_no1"];
                                                                        [telArray addObject:storeObj.tel_no1];
                                                                    }
                                                                    if([objdict objectForKey:@"tel_no2"] != (NSString *)[NSNull null] && ![[objdict objectForKey:@"tel_no2"] isEqualToString:@""])
                                                                    {
                                                                        storeObj.tel_no2 = [objdict objectForKey:@"tel_no2"];
                                                                        [telArray addObject:storeObj.tel_no2];
                                                                    }
                                                                    if([objdict objectForKey:@"tel_no3"] != (NSString *)[NSNull null] && ![[objdict objectForKey:@"tel_no3"] isEqualToString:@""])
                                                                    {
                                                                        storeObj.tel_no3 = [objdict objectForKey:@"tel_no3"];
                                                                        [telArray addObject:storeObj.tel_no3];
                                                                    }
                                                                    
                                                                    storeObj.image_url = [objdict objectForKey:@"image_url"];
                                                                    storeObj.email = [objdict objectForKey:@"email"];
                                                                    storeObj.total_like = [objdict objectForKey:@"total_like"];
                                                                    storeObj.total_share = [objdict objectForKey:@"total_share"];
                                                                    storeObj.user_like = [objdict objectForKey:@"user_like"];
                                                                    
                                                                    storeObj.latitude=[objdict objectForKey:@"latitude"];
                                                                    storeObj.longitude= [objdict objectForKey:@"longitude"];
                                                                    
                                                                    DebugLog(@"%f --- %f",[storeObj.latitude floatValue],[storeObj.longitude floatValue]);
                                                                    NSString *link = [MAP_STORE([storeObj.latitude floatValue],[storeObj.longitude floatValue]) stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                                                                    [mapImageView setImageWithURL:[NSURL URLWithString:link] placeholderImage:nil];
                                                                    
                                                                    if([objdict objectForKey:@"time"] != [NSNull null])
                                                                    {
                                                                        NSArray *timeArray = [objdict objectForKey:@"time"];
                                                                        DebugLog(@"%@",timeArray);
                                                                        NSDictionary *timedict = [timeArray  objectAtIndex:0];
                                                                        [self setStoreTimeValues:timedict storeobj:storeObj];

                                                                        NSMutableString *days = [[NSMutableString alloc] initWithString:@""];
                                                                        for(NSDictionary *dict in timeArray){
                                                                            if ([[dict objectForKey:@"is_closed"] intValue] == 1) {
                                                                                    if ([days isEqualToString:@""]) {
                                                                                        [days appendString:[NSString stringWithFormat:@"Closed On : %@",[[dict objectForKey:@"day"] capitalizedString]]];
                                                                                    }else{
                                                                                        [days appendString:[NSString stringWithFormat:@",%@",[[dict objectForKey:@"day"] capitalizedString]]];
                                                                                    }
                                                                                }
                                                                        }
                                                                        DebugLog(@"close days %@",days);
                                                                        storeObj.store_close_days = days;
                                                                    }

                                                                    [storeArray addObject:storeObj];
                                                                    storeObj = nil;
                                                                }
                                                                [self addStoreData];
                                                                [self setStoreLikeType];
                                                            } else {
                                                                UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                    message:@"No records found"
                                                                                    delegate:nil
                                                                                    cancelButtonTitle:@"OK"
                                                                                    otherButtonTitles:nil];
                                                                [av show];
                                                            }
                                                        }
                                                        
                                                        
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        if([self.splashJson  objectForKey:@"message"] != [NSNull null])
                                                            [INUserDefaultOperations showAlert:[self.splashJson  objectForKey:@"message"]];
                                
                                                    }];
    
    
    
    [operation start];
}

-(void)sendStoreGalleryRequest
{
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:STORE_GALLERY(self.storeId)]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"%@", self.splashJson);
                                                        if(self.splashJson  != nil) {
                                                            [galleryArray removeAllObjects];
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSArray* jsonArray = [self.splashJson  objectForKey:@"data"];
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *objdict = [jsonArray  objectAtIndex:index];
                                                                    INGalleryObj *tempgalleryObj = [[INGalleryObj alloc] init];
                                                                    tempgalleryObj.galleryID = [objdict objectForKey:@"id"];
                                                                    tempgalleryObj.place_id = [objdict objectForKey:@"place_id"];
                                                                    tempgalleryObj.title = [objdict objectForKey:@"title"];
                                                                    tempgalleryObj.image_url = [objdict objectForKey:@"image_url"];
                                                                    tempgalleryObj.thumb_url = [objdict objectForKey:@"thumb_url"];
                                                                    tempgalleryObj.image_date = [objdict objectForKey:@"image_date"];
                                                                    [galleryArray addObject:tempgalleryObj];
                                                                    tempgalleryObj = nil;
                                                                }
                                                            } else {
                                                                //if([self.splashJson  objectForKey:@"message"] != [NSNull null])
                                                                    //[INUserDefaultOperations showAlert:[self.splashJson  objectForKey:@"message"]];
                                                            }
                                                        }
                                                        if([galleryArray count] > 0)
                                                        {
                                                            [carousel reloadData];
                                                            [carousel scrollToItemAtIndex:1 animated:NO];

                                                        }
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                            message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                            delegate:nil
                                                                            cancelButtonTitle:@"OK"
                                                                            otherButtonTitles:nil];
                                                        [av show];
                                                        
                                                    }];
    
    
    
    [operation start];
}


-(void)sendStoreBroadcastRequest
{
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:STORE_BROADCAST(self.storeId,[INUserDefaultOperations getCustomerAuthId])]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"sendStoreBroadcastRequest %@", self.splashJson);
                                                        if(self.splashJson  != nil) {
                                                            [broadcastArray removeAllObjects];
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
//                                                                [emptyBroadcastlbl setHidden:TRUE];
                                                                NSDictionary *maindict = [self.splashJson  objectForKey:@"data"];
                                                                //self.totalPage = [[maindict  objectForKey:@"total_page"] integerValue];
                                                                //self.totalRecord = [[maindict  objectForKey:@"total_record"] integerValue];
                                                                NSArray* jsonArray = [maindict  objectForKey:@"record"];
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *objdict = [jsonArray  objectAtIndex:index];
                                                                    //DebugLog(@"\n\n%@\n\n",objdict);
                                                                    broadcastObj = [[INBroadcastDetailObj alloc] init];
                                                                    broadcastObj.ID = [[objdict objectForKey:@"id"] integerValue];
                                                                    broadcastObj.place_ID = [[objdict objectForKey:@"place_id"] integerValue];
                                                                    broadcastObj.type = [objdict objectForKey:@"street"];
                                                                    broadcastObj.title = [objdict objectForKey:@"title"];
                                                                    broadcastObj.description = [objdict objectForKey:@"description"];
                                                                    broadcastObj.url = [objdict objectForKey:@"url"];
                                                                    broadcastObj.tags = [objdict objectForKey:@"tags"];
                                                                    broadcastObj.date = [objdict objectForKey:@"date"];
                                                                    broadcastObj.total_like = [objdict objectForKey:@"total_like"];
                                                                    broadcastObj.total_share = [objdict objectForKey:@"total_share"];
                                                                    broadcastObj.user_like = [objdict objectForKey:@"user_like"];
                                                                    broadcastObj.image_url1 = [objdict objectForKey:@"image_url1"];
                                                                    broadcastObj.image_url2 = [objdict objectForKey:@"image_url2"];
                                                                    broadcastObj.image_url3 = [objdict objectForKey:@"image_url3"];
//                                                                    NSArray* imageArr = [objdict objectForKey:@"images"];
//                                                                    if([imageArr count]>0) {
//                                                                        broadcastObj.image_url = [imageArr objectAtIndex:0];
//                                                                    }
                                                                    
                                                                    broadcastObj.image_title1 = [objdict objectForKey:@"image_title1"];
                                                                    broadcastObj.image_title2 = [objdict objectForKey:@"image_title2"];
                                                                    broadcastObj.image_title3 = [objdict objectForKey:@"image_title3"];
                                                                    
                                                                    [broadcastArray addObject:broadcastObj];
                                                                    broadcastObj = nil;
                                                                }
                                                            } 
                                                            if([broadcastArray count] > 0)
                                                            {
                                                                [broadcastTableView reloadData];
                                                                [emptyBroadcastlbl setHidden:TRUE];
                                                            }else{
                                                                [broadcastTableView setHidden:TRUE];
                                                            }
                                                        }

                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                             message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                            delegate:nil
                                                                            cancelButtonTitle:@"OK"
                                                                            otherButtonTitles:nil];
                                                        [av show];
                                                        
                                                    }];
    
    
    
    [operation start];
}

-(void)sendStoreLikeRequest
{
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[INUserDefaultOperations getCustomerAuthId] forKey:@"user_id"];
    [params setObject:[INUserDefaultOperations getCustomerAuthCode] forKey:@"auth_id"];
    [params setObject:[NSString stringWithFormat:@"%d",self.storeId] forKey:@"place_id"];
    [httpClient postPath:LIKE_STORE parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
         //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            if([json  objectForKey:@"message"] != [NSNull null] && [[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
                NSString* message = [json objectForKey:@"message"];
                [INUserDefaultOperations showAlert:message];
                INStoreDetailObj *lstoreObj = [storeArray objectAtIndex:0];
                lstoreObj.user_like = @"1";
                [storeArray replaceObjectAtIndex:0 withObject:lstoreObj];
                [self setStoreLikeType];
            } else {
                NSString* message = [json objectForKey:@"message"];
                [INUserDefaultOperations showAlert:message];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
         //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}

-(void)sendStoreUnLikeRequest
{
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    [httpClient setDefaultHeader:@"user_id" value:[INUserDefaultOperations getCustomerAuthId]];
    [httpClient setDefaultHeader:@"auth_id" value:[INUserDefaultOperations getCustomerAuthCode]];
    [httpClient deletePath:UNLIKE_STORE(self.storeId) parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
         //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            if([json  objectForKey:@"message"] != [NSNull null] && [[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
                NSString* message = [json objectForKey:@"message"];
                [INUserDefaultOperations showAlert:message];
                INStoreDetailObj *lstoreObj = [storeArray objectAtIndex:0];
                lstoreObj.user_like = @"0";
                [storeArray replaceObjectAtIndex:0 withObject:lstoreObj];
                [self setStoreLikeType];
            } else {
                NSString* message = [json objectForKey:@"message"];
                [INUserDefaultOperations showAlert:message];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
         //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}

-(void)addStoreData
{
    INStoreDetailObj *lstoreObj = [storeArray objectAtIndex:0];
    if (lstoreObj.description != nil && ![lstoreObj.description isEqualToString:@""]) {
        descriptionTextView.text = lstoreObj.description;
    }
    
    NSMutableString *addreessString = [[NSMutableString alloc] initWithString:@""];
    if (![lstoreObj.street isEqualToString:@""] && lstoreObj.street != nil) {
        [addreessString appendString:lstoreObj.street];
    }
    if (![lstoreObj.landmark isEqualToString:@""] && lstoreObj.landmark != nil) {
        if ([addreessString isEqualToString:@""]) {
            [addreessString appendString:lstoreObj.landmark];
        }else{
            [addreessString appendString:[NSString stringWithFormat:@", %@", lstoreObj.landmark]];
        }
    }
    if (![lstoreObj.area isEqualToString:@""] && lstoreObj.area != nil) {
        if ([addreessString isEqualToString:@""]) {
            [addreessString appendString:lstoreObj.area];
        }else{
            [addreessString appendString:[NSString stringWithFormat:@", %@", lstoreObj.area]];
        }
    }

    address.text = addreessString;
    citylbl.text = lstoreObj.city;
    lblstoretime.text = [NSString stringWithFormat:@"Timings: %@ To %@\n",lstoreObj.open_time, lstoreObj.close_time];
    lblstoreClosed.text    = lstoreObj.store_close_days;
    lblemail.text          = lstoreObj.email;
    lblwebsite.text        = lstoreObj.website;
    
    if (![lstoreObj.fb_url isEqualToString:@""] && lstoreObj.fb_url != nil) {
        lblfacebook.text       = [NSString stringWithFormat:@"Facebook : %@",lstoreObj.fb_url];
    }else{
        [lblfacebook setHidden:YES];
    }
    if (![lstoreObj.twitter_url isEqualToString:@""] && lstoreObj.twitter_url != nil) {
        lbltwitter.text        = [NSString stringWithFormat:@"Twitter        : %@",lstoreObj.twitter_url];
    }
    else{
        [lbltwitter setHidden:YES];
    }
    DebugLog(@"telArray %@",telArray);
    
    DebugLog(@"lstoreObj.latitude %@ lstoreObj.longitude %@",lstoreObj.latitude,lstoreObj.longitude);

    if ([lstoreObj.latitude doubleValue] != 0 && [lstoreObj.longitude doubleValue] != 0) {
        [mapImageView setHidden:NO];
        [getDirectionBtn setHidden:NO];
    }else{
        [mapImageView setHidden:YES];
        [getDirectionBtn setHidden:YES];
    }
    
    if (lstoreObj.image_url != nil && ![lstoreObj.image_url isEqualToString:@""]) {
        if ([lstoreObj.image_url hasPrefix:@"http"]) {
            storeImageUrl = [NSURL URLWithString:lstoreObj.image_url];
        }else{
            storeImageUrl = [NSURL URLWithString:THUMBNAIL_LINK(lstoreObj.image_url)];
        }
        DebugLog(@"addStoreData storeImageUrl %@",storeImageUrl);
        [carousel reloadData];
     }
}

-(void)setStoreTimeValues:(NSDictionary *)lparams  storeobj:(INStoreDetailObj *)lstoreObj
{
    NSString *otime = [lparams objectForKey:@"open_time"];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setHour:[[otime substringToIndex:2] integerValue]];
    [comps setMinute:[[otime substringFromIndex:3] integerValue]];
    [comps setSecond:[[otime substringToIndex:6] integerValue]];
	NSDate *fromselectedDate = [[NSCalendar currentCalendar] dateFromComponents:comps];
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setDateFormat:@"hh:mm a"];
    NSString *fromString2 = [dateFormatter2 stringFromDate:fromselectedDate];
    DebugLog(@"from Date is: %@", fromString2);
    lstoreObj.open_time = fromString2;
    
    NSString *ctime = [lparams objectForKey:@"close_time"];
    NSDateComponents *comps1 = [[NSDateComponents alloc] init];
    [comps1 setHour:[[ctime substringToIndex:2] integerValue]];
    [comps1 setMinute:[[ctime substringFromIndex:3] integerValue]];
    [comps1 setSecond:[[ctime substringToIndex:6] integerValue]];
	NSDate *toselectedDate = [[NSCalendar currentCalendar] dateFromComponents:comps1];
    NSDateFormatter *dateFormatter3 = [[NSDateFormatter alloc] init];
    [dateFormatter3 setDateFormat:@"hh:mm a"];
    NSString *toString3 = [dateFormatter3 stringFromDate:toselectedDate];
    DebugLog(@"to Date is: %@", toString3);
    lstoreObj.close_time = toString3;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setCarousel:nil];
    [self setAddress:nil];
    [self setCitylbl:nil];
    [self setBroadcastTableView:nil];
    [self setLikeStore:nil];
    [self setEmptyBroadcastlbl:nil];
    [self setDescriptionTextView:nil];
    [self setShareStoreBtn:nil];
    [self setAddressBtn:nil];
    [self setBroadcastBtn:nil];
    [self setLblCallStore:nil];
    [self setLblAddfav:nil];
    [self setLblShareStore:nil];
    [self setScrollView:nil];
    [self setAddressView:nil];
    [self setCallStoreBtn:nil];
    [self setActionView:nil];
    [self setImgSeperator1:nil];
    [self setImgSeperator2:nil];
    [self setImgSeperator3:nil];
    [self setImgSeperator4:nil];
    [self setAddressDropDownImageView:nil];
    [self setBroadcastDropDownImageView:nil];
    [self setGetDirectionBtn:nil];
    [self setMapImageView:nil];
    [self setLblstoretime:nil];
    [self setLblstoreClosed:nil];
    [self setLblemail:nil];
    [self setLblwebsite:nil];
    [self setLblfacebook:nil];
    [self setLbltwitter:nil];
    [self setAddressView:nil];
    [super viewDidUnload];
}


#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [broadcastArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UILabel *lblDay;
    UILabel *lblMonth;
    UILabel *lblTitle;
    
    UIImageView *thumbImg;
    
    UIImageView *likethumbImg;
    UILabel *lblLikeCount;

    UIImageView *sharethumbImg;
    UILabel *lblShareCount;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(10.0,5.0,80.0,60.0)];
        thumbImg.tag = 103;
        thumbImg.contentMode = UIViewContentModeScaleToFill;
        [cell.contentView addSubview:thumbImg];
        
        lblDay = [[UILabel alloc] initWithFrame:CGRectMake(10.0,5.0,80.0,50.0)];
        lblDay.text = @"";
        lblDay.tag = 100;
        lblDay.font = [UIFont systemFontOfSize:35];
        lblDay.textAlignment = UITextAlignmentCenter;
        lblDay.textColor = [UIColor whiteColor];
        lblDay.backgroundColor =  LIGHT_BLUE;
        [cell.contentView addSubview:lblDay];
        
        
        lblMonth = [[UILabel alloc] initWithFrame:CGRectMake(10.0,55.0,80.0,20.0)];
        lblMonth.text = @"";
        lblMonth.tag = 101;
        lblMonth.font = [UIFont systemFontOfSize:15];
        lblMonth.textAlignment = UITextAlignmentCenter;
        lblMonth.textColor = [UIColor whiteColor];
        lblMonth.backgroundColor =  DARK_YELLOW_COLOR;
        [cell.contentView addSubview:lblMonth];
        
        
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(100.0,15.0,175.0,50.0)];
        lblTitle.text = @"";
        lblTitle.tag = 102;
        lblTitle.numberOfLines = 3;
        lblTitle.font = DEFAULT_BOLD_FONT(15);
        lblTitle.minimumFontSize = 14;
        lblTitle.adjustsFontSizeToFitWidth = TRUE;
        lblTitle.textAlignment = UITextAlignmentLeft;
        lblTitle.textColor = BROWN_COLOR;
        lblTitle.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblTitle];
        
        likethumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(276.0,10.0,20.0,20.0)];
        likethumbImg.tag = 104;
        likethumbImg.image = [UIImage imageNamed:@"Shoplocal_Post-Title_Fv.png"];
        likethumbImg.contentMode = UIViewContentModeScaleAspectFill;
        [cell.contentView addSubview:likethumbImg];
        
        lblLikeCount = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(likethumbImg.frame)+2,10.0,20.0,20.0)];
        lblLikeCount.text = @"";
        lblLikeCount.tag = 105;
        lblLikeCount.font = DEFAULT_BOLD_FONT(12);
        lblLikeCount.textAlignment = UITextAlignmentLeft;
        lblLikeCount.textColor = BROWN_COLOR;
        lblLikeCount.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblLikeCount];
        
        sharethumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(276.0,CGRectGetMaxY(likethumbImg.frame)+2,20.0,20.0)];
        sharethumbImg.tag = 106;
        sharethumbImg.image = [UIImage imageNamed:@"Shoplocal_Post-Title_Share.png"];
        sharethumbImg.contentMode = UIViewContentModeScaleAspectFill;
        [cell.contentView addSubview:sharethumbImg];
        
        lblShareCount = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(sharethumbImg.frame)+2,sharethumbImg.frame.origin.y,20.0,20.0)];
        lblShareCount.text = @"";
        lblShareCount.tag = 107;
        lblShareCount.font = DEFAULT_BOLD_FONT(12);
        lblShareCount.textAlignment = UITextAlignmentLeft;
        lblShareCount.textColor = BROWN_COLOR;
        lblShareCount.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblShareCount];

    }
    if ([indexPath row] < [broadcastArray count]) {
        lblDay      = (UILabel *)[cell viewWithTag:100];
        lblMonth    = (UILabel *)[cell viewWithTag:101];
        lblTitle    = (UILabel *)[cell viewWithTag:102];
        thumbImg    = (UIImageView *)[cell viewWithTag:103];
        likethumbImg    = (UIImageView *)[cell viewWithTag:104];
        lblLikeCount    = (UILabel *)[cell viewWithTag:105];
        sharethumbImg   = (UIImageView *)[cell viewWithTag:106];
        lblShareCount   = (UILabel *)[cell viewWithTag:107];
        
        INBroadcastDetailObj *lbroadcastObj  = [broadcastArray objectAtIndex:indexPath.row];
        DebugLog(@"%@",lbroadcastObj.date);
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        DebugLog(@"%@",[NSTimeZone localTimeZone]);
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        NSDate *myDate = [[NSDate alloc] init];
        myDate = [dateFormatter dateFromString:lbroadcastObj.date];
        DebugLog(@"MyDate is: %@", myDate);

        NSDateFormatter *dayformatter = [[NSDateFormatter alloc] init];
        [dayformatter setDateFormat:@"dd"];
        NSString *dayFromDate = [dayformatter stringFromDate:myDate];
        DebugLog(@"Myday is: %@", dayFromDate);
        lblDay.text = dayFromDate;
        
        NSDateFormatter *monthformatter = [[NSDateFormatter alloc] init];
        [monthformatter setDateFormat:@"MMM"];
        NSString *monthFromDate = [monthformatter stringFromDate:myDate];
        DebugLog(@"MyMonth is: %@", monthFromDate);
        lblMonth.text = monthFromDate;
        

        lblTitle.text = lbroadcastObj.title;
        
        if(lbroadcastObj.image_url1 != (NSString*)[NSNull null] && ![lbroadcastObj.image_url1 isEqualToString:@""]) {
            lblDay.backgroundColor = [UIColor clearColor];
//            [thumbImg setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(lbroadcastObj.image_url1)] placeholderImage:[UIImage imageNamed:@"table_placeholder.png"]];
            __weak UIImageView *thumbImg_ = thumbImg;
            [thumbImg setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(lbroadcastObj.image_url1)]
                     placeholderImage:[UIImage imageNamed:@"list_thumbnail.png"]
                              success:^(UIImage *image) {
                                  //DebugLog(@"success");
                              }
                              failure:^(NSError *error) {
                                  //DebugLog(@"write error %@", error);
                                  thumbImg_.image = [UIImage imageNamed:@"list_thumbnail.png"];
                              }];
            DebugLog(@"link-%@",THUMBNAIL_LINK(lbroadcastObj.image_url1));
        } else {
            thumbImg.image = nil;
            lblDay.backgroundColor =  LIGHT_BLUE;
        }
        
        [lblLikeCount setHidden:FALSE];
        [likethumbImg setHidden:FALSE];
        lblLikeCount.text = lbroadcastObj.total_like;
       
        [lblShareCount setHidden:FALSE];
        [sharethumbImg setHidden:FALSE];
        lblShareCount.text = lbroadcastObj.total_share;
    }
    return cell;
}


#pragma mark - Table view delegate  
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *storeDetailParams = [NSDictionary dictionaryWithObjectsAndKeys:[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"Area_Name", self.title,@"StoreName", nil];
    [INEventLogger logEvent:@"StoreDetail_ViewOffer" withParams:storeDetailParams];
    INBroadcastDetailObj *tempbroadcastObj  = [broadcastArray objectAtIndex:indexPath.row];
    INCustBroadCastDetailsViewController *postDetailController = [[INCustBroadCastDetailsViewController alloc] initWithNibName:@"INCustBroadCastDetailsViewController" bundle:nil] ;
    
    DebugLog(@"tempbroadcastObj.storename %@ ",self.title);
    postDetailController.title = self.title;//@"Broadcast Detail";
    postDetailController.storeName = self.title;
    postDetailController.postType = 1;
    postDetailController.postlikeType = tempbroadcastObj.user_like;
    postDetailController.postId = tempbroadcastObj.ID;
    postDetailController.postTitle = tempbroadcastObj.title;
    postDetailController.postDescription = tempbroadcastObj.description;
    if ([tempbroadcastObj.image_url1 isEqual:[NSNull null]] || tempbroadcastObj.image_url1 == nil) {
        postDetailController.postimageUrl = @"";
    }else{
        postDetailController.postimageUrl = tempbroadcastObj.image_url1;
    }
    postDetailController.postimageTitle = tempbroadcastObj.image_title1;
    postDetailController.viewOpenedFrom = FROM_STORE_DETAILS;
    postDetailController.likescountString = tempbroadcastObj.total_like;
    postDetailController.sharecountString = tempbroadcastObj.total_share;
    postDetailController.telArray = telArray;
    postDetailController.SHOW_BUY_BTN = YES;
    postDetailController.placeId = tempbroadcastObj.place_ID;
    [self.navigationController pushViewController:postDetailController animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    if (galleryArray.count == 0) {
        return 1;
    }
    return [galleryArray count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        FXImageView *imageView = [[FXImageView alloc] initWithFrame:CGRectMake(0, 0, 100.0f, 100.0f)];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        ((UIImageView *)view).image = [UIImage imageNamed:@"carousal_lock.png"];
        imageView.asynchronous = YES;
        view = imageView;
    }
    
    if(galleryArray.count == 0)
    {
        DebugLog(@"imageUrl carousal %@",storeImageUrl);
        UIImageView *thumbImgview = ((UIImageView *)view);
        [thumbImgview setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",storeImageUrl]]
                     placeholderImage:[UIImage imageNamed:@"list_thumbnail.png"]
                              success:^(UIImage *image) {
                                  //DebugLog(@"success");
                              }
                              failure:^(NSError *error) {
                                  //DebugLog(@"write error %@", error);
                              }];
    } else {
        INGalleryObj *tempgalleryObj = (INGalleryObj *)[galleryArray objectAtIndex:index];
        UIImageView *thumbImgview = ((UIImageView *)view);
        [thumbImgview setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(tempgalleryObj.image_url)]
                     placeholderImage:[UIImage imageNamed:@"list_thumbnail.png"]
                              success:^(UIImage *image) {
                                  //DebugLog(@"success");
                              }
                              failure:^(NSError *error) {
                                  //DebugLog(@"write error %@", error);
                              }];
    }
    return view;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    DebugLog(@"didselectitem %d %d",index , [galleryArray count]);
    if([galleryArray count] == 0)
        return;
    
    _photos = [[NSMutableArray alloc] init];
    MWPhoto *photo;
    INGalleryObj *tempgalleryObj;
    for(int i = 0; i < [galleryArray count] ; i++)
    {
        tempgalleryObj = (INGalleryObj *)[galleryArray objectAtIndex:i];
        DebugLog(@"link--- %@",tempgalleryObj.image_url);
        photo = [[MWPhoto alloc] initWithURL:[NSURL URLWithString:THUMBNAIL_LINK(tempgalleryObj.image_url)]];
        if (tempgalleryObj.title != nil || ![tempgalleryObj.title isEqualToString:@""])
            photo.caption = [NSString stringWithFormat:@"%@",tempgalleryObj.title];
        [_photos addObject:photo];
        tempgalleryObj = nil;
    }
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton = YES;
    browser.wantsFullScreenLayout = YES;
    [browser setInitialPageIndex:index];
    [self.navigationController pushViewController:browser animated:YES];
}

- (CGFloat)carousel:(iCarousel *)_carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return 1.05;
        }
        default:
        {
            return value;
        }
    }
}


- (IBAction)shareStoreBtnPressed:(id)sender {
    UIActionSheet *shareActionSheet = [[UIActionSheet alloc] initWithTitle:@"Share via" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"SMS",@"Email",@"WhatsApp",@"Facebook",@"Twitter", nil];
    [shareActionSheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSString *title = [actionSheet buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"SMS"])
    {
        [self smsShareClicked];
    }
    else if([title isEqualToString:@"Email"])
    {
        [self emailShareClicked];
    }else if([title isEqualToString:@"WhatsApp"])
    {
        [self whatsAppShareClicked];
    }
    else if([title isEqualToString:@"Facebook"])
    {
        [self facebookShareClicked];
    } else if([title isEqualToString:@"Twitter"])
    {
        [self twitterShareClicked];
    }
    
    if(![title isEqualToString:@"Cancel"])
    {
        DebugLog(@"%@",[NSString stringWithFormat:@"%@(%@,%@)",@"StoreDetail_Share", [IN_APP_DELEGATE getActivePlaceNameFromAreaTable],self.title]);
        NSDictionary *storeDetailParams = [NSDictionary dictionaryWithObjectsAndKeys:[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"Area_Name", self.title,@"StoreName", nil];
        [INEventLogger logEvent:@"StoreDetail_Share" withParams:storeDetailParams];
    }
}

#pragma Gesture Methods
- (void)textViewTapTapDetected:(UIGestureRecognizer *)sender {
    if ([descriptionTextView.text isEqualToString:DESCRIPTION_PLACEHOLDER_TEXT]) {
        return;
    }
    [UIView animateWithDuration:0.4
                     animations:^{
                         if(descriptionTextView.isScrollEnabled) {
                             [descriptionTextView setFrame:CGRectMake(descriptionTextView.frame.origin.x,
                                                             100,
                                                             descriptionTextView.frame.size.width,
                                                             128)];
                             descriptionTextView.scrollEnabled = NO;
                             [imgSeperator2 setFrame:CGRectMake(imgSeperator2.frame.origin.x,
                                                                CGRectGetMaxY(descriptionTextView.frame),
                                                                imgSeperator2.frame.size.width,
                                                                imgSeperator2.frame.size.height)];
                             
                             [scrollView setFrame:CGRectMake(scrollView.frame.origin.x,
                                                             scrollView.frame.origin.y,
                                                             scrollView.frame.size.width,
                                                             416)];
                             //push down
                             [carousel setFrame:CGRectMake(carousel.frame.origin.x,
                                                           0,
                                                           carousel.frame.size.width,
                                                           carousel.frame.size.height)];
                             
                         }else{
                             [descriptionTextView setFrame:CGRectMake(descriptionTextView.frame.origin.x,
                                                                      0,
                                                                      descriptionTextView.frame.size.width,
                                                                      self.view.frame.size.height)];
                             descriptionTextView.scrollEnabled = YES;
                             
                             [imgSeperator2 setFrame:CGRectMake(imgSeperator2.frame.origin.x,
                                                                CGRectGetMaxY(self.view.frame),
                                                                imgSeperator2.frame.size.width,
                                                                imgSeperator2.frame.size.height)];
                             
                             [scrollView setFrame:CGRectMake(scrollView.frame.origin.x,
                                                             scrollView.frame.origin.y,
                                                             scrollView.frame.size.width,
                                                             self.view.frame.size.height)];
                             //push upwards
                             [carousel setFrame:CGRectMake(carousel.frame.origin.x,
                                                           (descriptionTextView.frame.origin.y-CGRectGetMaxY(scrollView.frame)),
                                                           carousel.frame.size.width,
                                                           carousel.frame.size.height)];
                         }
                         [imgSeperator1 setFrame:CGRectMake(imgSeperator1.frame.origin.x,
                                                            CGRectGetMaxY(carousel.frame),
                                                            imgSeperator1.frame.size.width,
                                                            imgSeperator1.frame.size.height)];
                        
                         [actionView setFrame:CGRectMake(actionView.frame.origin.x,
                                                         CGRectGetMaxY(imgSeperator2.frame),
                                                         actionView.frame.size.width,
                                                         actionView.frame.size.height)];
                         
                         [imgSeperator3 setFrame:CGRectMake(imgSeperator3.frame.origin.x,
                                                            CGRectGetMaxY(actionView.frame),
                                                            imgSeperator3.frame.size.width,
                                                            imgSeperator3.frame.size.height)];
                         
                         [addressBtn setFrame:CGRectMake(addressBtn.frame.origin.x,
                                                         CGRectGetMaxY(imgSeperator3.frame),
                                                         addressBtn.frame.size.width,
                                                         addressBtn.frame.size.height)];
                         
                         [addressDropDownImageView setFrame:CGRectMake(addressDropDownImageView.frame.origin.x,
                                                                       addressBtn.frame.origin.y+13,
                                                                       addressDropDownImageView.frame.size.width,
                                                                       addressDropDownImageView.frame.size.height)];
                         
                         [imgSeperator4 setFrame:CGRectMake(imgSeperator4.frame.origin.x,
                                                            CGRectGetMaxY(addressBtn.frame),
                                                            imgSeperator4.frame.size.width,
                                                            imgSeperator4.frame.size.height)];
                         
                         [broadcastBtn setFrame:CGRectMake(broadcastBtn.frame.origin.x,
                                                         CGRectGetMaxY(imgSeperator4.frame),
                                                         broadcastBtn.frame.size.width,
                                                         broadcastBtn.frame.size.height)];
                         
                         [broadcastDropDownImageView setFrame:CGRectMake(broadcastDropDownImageView.frame.origin.x,
                                                                       broadcastBtn.frame.origin.y+13,
                                                                       broadcastDropDownImageView.frame.size.width,
                                                                       broadcastDropDownImageView.frame.size.height)];
                         
                     } completion:^(BOOL finished){
                     }];
}

- (IBAction)addressBtnPressed:(id)sender {
    [broadcastTableView setHidden:YES];
    [UIView animateWithDuration:0.4
                     animations:^{
                         if(addressBtn.isSelected) {
                             [addressBtn setFrame:CGRectMake(addressBtn.frame.origin.x,
                                                             309,
                                                             addressBtn.frame.size.width,
                                                             addressBtn.frame.size.height)];
                             addressBtn.selected = NO;
                             addressView.hidden = YES;
                             
                             [imgSeperator4 setFrame:CGRectMake(imgSeperator4.frame.origin.x,
                                                                CGRectGetMaxY(addressBtn.frame),
                                                                imgSeperator4.frame.size.width,
                                                                imgSeperator4.frame.size.height)];
                             
                             [scrollView setFrame:CGRectMake(scrollView.frame.origin.x,
                                                               scrollView.frame.origin.y,
                                                               scrollView.frame.size.width,
                                                               416)];
                             
                             
                             //push upwards
                             [carousel setFrame:CGRectMake(carousel.frame.origin.x,
                                                             0,
                                                             carousel.frame.size.width,
                                                             carousel.frame.size.height)];
                             
                         }else{
                             [addressBtn setFrame:CGRectMake(addressBtn.frame.origin.x,
                                                                0,
                                                                addressBtn.frame.size.width,
                                                                addressBtn.frame.size.height)];
                             addressBtn.selected = YES;
                             addressView.hidden = NO;
                             
                             [imgSeperator4 setFrame:CGRectMake(imgSeperator4.frame.origin.x,
                                                                CGRectGetMaxY(self.view.frame),
                                                                imgSeperator4.frame.size.width,
                                                                imgSeperator4.frame.size.height)];
                             
                             [scrollView setFrame:CGRectMake(scrollView.frame.origin.x,
                                                             scrollView.frame.origin.y,
                                                             scrollView.frame.size.width,
                                                             self.view.frame.size.height+CGRectGetHeight(imgSeperator4.frame)+CGRectGetHeight(broadcastBtn.frame))];
                             //push upwards
                             [carousel setFrame:CGRectMake(carousel.frame.origin.x,
                                                           (addressBtn.frame.origin.y-CGRectGetMaxY(scrollView.frame)),
                                                           carousel.frame.size.width,
                                                           carousel.frame.size.height)];
                         }
                         [addressDropDownImageView setFrame:CGRectMake(addressDropDownImageView.frame.origin.x,
                                                                       addressBtn.frame.origin.y+13,
                                                                       addressDropDownImageView.frame.size.width,
                                                                       addressDropDownImageView.frame.size.height)];
                         
                         [broadcastBtn setFrame:CGRectMake(broadcastBtn.frame.origin.x,
                                                           CGRectGetMaxY(imgSeperator4.frame),
                                                           broadcastBtn.frame.size.width,
                                                           broadcastBtn.frame.size.height)];
                         
                         [broadcastDropDownImageView setFrame:CGRectMake(broadcastDropDownImageView.frame.origin.x,
                                                                         broadcastBtn.frame.origin.y+13,
                                                                         broadcastDropDownImageView.frame.size.width,
                                                                         broadcastDropDownImageView.frame.size.height)];
                         
                         [addressView setFrame:CGRectMake(addressView.frame.origin.x,
                                                          CGRectGetMaxY(addressBtn.frame),
                                                          addressView.frame.size.width,
                                                          CGRectGetHeight(self.view.frame) - CGRectGetHeight(addressBtn.frame))];
                         
                         [imgSeperator1 setFrame:CGRectMake(imgSeperator1.frame.origin.x,
                                                            CGRectGetMaxY(carousel.frame),
                                                            imgSeperator1.frame.size.width,
                                                            imgSeperator1.frame.size.height)];
                         
                         [descriptionTextView setFrame:CGRectMake(descriptionTextView.frame.origin.x,
                                                                  imgSeperator1.frame.origin.y+1,
                                                                  descriptionTextView.frame.size.width,
                                                                  descriptionTextView.frame.size.height)];
                         
                         [imgSeperator2 setFrame:CGRectMake(imgSeperator2.frame.origin.x,
                                                            CGRectGetMaxY(descriptionTextView.frame),
                                                            imgSeperator2.frame.size.width,
                                                            imgSeperator2.frame.size.height)];
                         
                         [actionView setFrame:CGRectMake(actionView.frame.origin.x,
                                                         CGRectGetMaxY(imgSeperator2.frame),
                                                         actionView.frame.size.width,
                                                         actionView.frame.size.height)];
                         
                         [imgSeperator3 setFrame:CGRectMake(imgSeperator3.frame.origin.x,
                                                            CGRectGetMaxY(actionView.frame),
                                                            imgSeperator3.frame.size.width,
                                                            imgSeperator3.frame.size.height)];

                         

                         
                     } completion:^(BOOL finished){
                         if (addressBtn.isSelected) {
                             [addressDropDownImageView setImage:[UIImage imageNamed:@"Minus.png"]];
                         }else{
                             [addressDropDownImageView setImage:[UIImage imageNamed:@"Plus.png"]];
                         }
    }];
}

- (IBAction)broadcastBtnPressed:(id)sender {
    [addressView setHidden:YES];
    [UIView animateWithDuration:0.4
                     animations:^{
                         if(broadcastBtn.isSelected) {
                             [emptyBroadcastlbl setHidden:YES];
                             [broadcastBtn setFrame:CGRectMake(broadcastBtn.frame.origin.x,
                                                             366,
                                                             broadcastBtn.frame.size.width,
                                                             broadcastBtn.frame.size.height)];
                             broadcastBtn.selected = NO;
                             broadcastTableView.hidden = YES;

                             [scrollView setFrame:CGRectMake(scrollView.frame.origin.x,
                                                             scrollView.frame.origin.y,
                                                             scrollView.frame.size.width,
                                                             416)];
                             
                             
                             //push upwards
                             [carousel setFrame:CGRectMake(carousel.frame.origin.x,
                                                           0,
                                                           carousel.frame.size.width,
                                                           carousel.frame.size.height)];
                             
                         }else{
                             [broadcastBtn setFrame:CGRectMake(broadcastBtn.frame.origin.x,
                                                             0,
                                                             broadcastBtn.frame.size.width,
                                                             broadcastBtn.frame.size.height)];
                             broadcastBtn.selected = YES;
                             broadcastTableView.hidden = NO;
                             [scrollView setFrame:CGRectMake(scrollView.frame.origin.x,
                                                             scrollView.frame.origin.y,
                                                             scrollView.frame.size.width,
                                                             self.view.frame.size.height+100)];
                             //push upwards
                             [carousel setFrame:CGRectMake(carousel.frame.origin.x,
                                                           (descriptionTextView.frame.origin.y-CGRectGetMaxY(scrollView.frame)),
                                                           carousel.frame.size.width,
                                                           carousel.frame.size.height)];
                             
                             
                         }
                         [broadcastDropDownImageView setFrame:CGRectMake(broadcastDropDownImageView.frame.origin.x,
                                                                       broadcastBtn.frame.origin.y+13,
                                                                       broadcastDropDownImageView.frame.size.width,
                                                                       broadcastDropDownImageView.frame.size.height)];
                         
                         [broadcastTableView setFrame:CGRectMake(broadcastTableView.frame.origin.x,
                                                                 CGRectGetMaxY(broadcastBtn.frame),
                                                                 broadcastTableView.frame.size.width,
                                                                 CGRectGetHeight(self.view.frame) - CGRectGetHeight(broadcastBtn.frame))];
                         
                         [imgSeperator1 setFrame:CGRectMake(imgSeperator1.frame.origin.x,
                                                            CGRectGetMaxY(carousel.frame),
                                                            imgSeperator1.frame.size.width,
                                                            imgSeperator1.frame.size.height)];
                         
                         [descriptionTextView setFrame:CGRectMake(descriptionTextView.frame.origin.x,
                                                                  imgSeperator1.frame.origin.y+1,
                                                                  descriptionTextView.frame.size.width,
                                                                  descriptionTextView.frame.size.height)];
                         
                         [imgSeperator2 setFrame:CGRectMake(imgSeperator2.frame.origin.x,
                                                            CGRectGetMaxY(descriptionTextView.frame),
                                                            imgSeperator2.frame.size.width,
                                                            imgSeperator2.frame.size.height)];
                         
                         [actionView setFrame:CGRectMake(actionView.frame.origin.x,
                                                         CGRectGetMaxY(imgSeperator2.frame),
                                                         actionView.frame.size.width,
                                                         actionView.frame.size.height)];
                         
                         [imgSeperator3 setFrame:CGRectMake(imgSeperator3.frame.origin.x,
                                                            CGRectGetMaxY(actionView.frame),
                                                            imgSeperator3.frame.size.width,
                                                            imgSeperator3.frame.size.height)];
                         
                         [addressBtn setFrame:CGRectMake(addressBtn.frame.origin.x,
                                                         CGRectGetMaxY(imgSeperator3.frame),
                                                         addressBtn.frame.size.width,
                                                         addressBtn.frame.size.height)];
                         
                         [addressDropDownImageView setFrame:CGRectMake(addressDropDownImageView.frame.origin.x,
                                                                       addressBtn.frame.origin.y+13,
                                                                       addressDropDownImageView.frame.size.width,
                                                                       addressDropDownImageView.frame.size.height)];

                         
                         [imgSeperator4 setFrame:CGRectMake(imgSeperator4.frame.origin.x,
                                                            CGRectGetMaxY(addressBtn.frame),
                                                            imgSeperator4.frame.size.width,
                                                            imgSeperator4.frame.size.height)];
                         
                     } completion:^(BOOL finished){
                         if (broadcastBtn.isSelected) {
                             [broadcastDropDownImageView setImage:[UIImage imageNamed:@"Minus.png"]];
                             if (broadcastArray.count > 0) {
                                 [emptyBroadcastlbl setHidden:YES];
                             }else{
                                 [emptyBroadcastlbl setHidden:NO];
                             }
                         }else{
                             [broadcastDropDownImageView setImage:[UIImage imageNamed:@"Plus.png"]];
                         }
                     }];
}



- (IBAction)callStoreBtnPressed:(id)sender {
    if (telArray != nil && telArray.count > 0) {
        [ActionSheetStringPicker showPickerWithTitle:@"Select number" rows:self.telArray initialSelection:0 target:self successAction:@selector(selectTel:element:) cancelAction:@selector(actionPickerCancelled:) origin:sender];
    }
}

#pragma mark - Actionsheet Implementation
- (void)selectTel:(NSNumber *)lselectedIndex element:(id)element {
    //may have originated from textField or barButtonItem, use an IBOutlet instead of element
    selectedTelNumberToCall = [[telArray objectAtIndex:[lselectedIndex intValue]] stringByReplacingOccurrencesOfString:@" " withString:@""];
    DebugLog(@"selectedTelNumberToCall %@",selectedTelNumberToCall);
    
    NSString *deviceType = [UIDevice currentDevice].model;
    if([deviceType isEqualToString:@"iPhone"])
    {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: [NSString stringWithFormat: @"Do you want to call %@ store number ?",selectedTelNumberToCall]
                                                          delegate: self
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"NO", @"YES", nil
                              ];
        [alert show];
        NSDictionary *storeDetailParams = [NSDictionary dictionaryWithObjectsAndKeys:[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"Area_Name", self.title,@"StoreName", nil];
        [INEventLogger logEvent:@"StoreDetail_Call" withParams:storeDetailParams];
    }
    else
    {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: @"Calling functionality is not available in this device. "
                                                          delegate: nil
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"OK", nil
                              ];
        [ alert show ];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:@"YES"])
    {
        UIApplication *myApp = [UIApplication sharedApplication];
        DebugLog(@"selectedTelNumberToCall = -%@-",selectedTelNumberToCall);
        [myApp openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",selectedTelNumberToCall]]];
    }
}

- (IBAction)likeStoreBtnPressed:(id)sender {
    if([IN_APP_DELEGATE networkavailable])
    {
        if(![INUserDefaultOperations isCustomerLoggedIn])
        {
            SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Please LOGIN to mark any store as favourite."];
            alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
            alertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
            
            [alertView addButtonWithTitle:@"LOGIN"
                                     type:SIAlertViewButtonTypeDestructive
                                  handler:^(SIAlertView *alert) {
                                      DebugLog(@"LOGIN Clicked");
                                      [self showLoginModal];
                                  }];
            [alertView addButtonWithTitle:@"NOT NOW"
                                     type:SIAlertViewButtonTypeCancel
                                  handler:^(SIAlertView *alert) {
                                      DebugLog(@"NOT NOW Clicked");
                                  }];
            [alertView show];
        } else {
            INStoreDetailObj *lstoreObj = [storeArray objectAtIndex:0];
            DebugLog(@"%@",lstoreObj.user_like);
            if([lstoreObj.user_like isEqualToString:@"1"])
            {
                [self sendStoreUnLikeRequest];
                NSDictionary *storeDetailParams = [NSDictionary dictionaryWithObjectsAndKeys:[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"Area_Name", self.title,@"StoreName", nil];
                [INEventLogger logEvent:@"StoreDetail_UnFavourite" withParams:storeDetailParams];
            } else {
                [self sendStoreLikeRequest];
                NSDictionary *storeDetailParams = [NSDictionary dictionaryWithObjectsAndKeys:[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"Area_Name", self.title,@"StoreName", nil];
                [INEventLogger logEvent:@"StoreDetail_Favourite" withParams:storeDetailParams];
            }
        }
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

- (IBAction)getDirectionBtnPressed:(id)sender {
    if ([INUserDefaultOperations getLatitude] > 0 && [INUserDefaultOperations getLongitude] > 0) {
        NSDictionary *storeDetailParams = [NSDictionary dictionaryWithObjectsAndKeys:[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"Area_Name", self.title,@"StoreName", nil];
        [INEventLogger logEvent:@"StoreDetail_Directions" withParams:storeDetailParams];
        INStoreDetailObj *lstoreObj = [storeArray objectAtIndex:0];
        
//        UIApplication *application = [UIApplication sharedApplication];
//        [application openURL:[NSURL URLWithString:GETDIRECTION_LINK_USING_GOOGLEMAPS_IN_BROWSER([INUserDefaultOperations getLatitude], [INUserDefaultOperations getLongitude], lstoreObj.latitude, lstoreObj.longitude) ]];
//        
        
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"comgooglemaps://"]]) //is Google maps installed?
        {
            DebugLog(@"Can use comgooglemaps://");
            NSString *googleMapsURLString = GETDIRECTION_LINK_USING_GOOGLEMAPS_IN_APP([INUserDefaultOperations getLatitude], [INUserDefaultOperations getLongitude], lstoreObj.latitude, lstoreObj.longitude);
            DebugLog(@"URL string-----> %@",googleMapsURLString);
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapsURLString]];
        } else {
            DebugLog(@"Can't use comgooglemaps://");
            NSString *googleMapsURLString = GETDIRECTION_LINK_USING_GOOGLEMAPS_IN_BROWSER([INUserDefaultOperations getLatitude], [INUserDefaultOperations getLongitude], lstoreObj.latitude, lstoreObj.longitude);
            DebugLog(@"URL string-----> %@",googleMapsURLString);
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapsURLString]];
        }

    }
}


-(void)showLoginModal{
    INCustomerLoginViewController *custLoginController = [[INCustomerLoginViewController alloc] initWithNibName:@"INCustomerLoginViewController" bundle:nil] ;
    custLoginController.delegate = self;
    UINavigationController *loginnavBar=[[UINavigationController alloc]initWithRootViewController:custLoginController];
    UIButton *moreButton1 =  [UIButton buttonWithType:UIButtonTypeCustom];
    [moreButton1 setTitle:@"Cancel" forState:UIControlStateNormal];
    moreButton1.titleLabel.font = [UIFont systemFontOfSize:15.0];
    [moreButton1 setBackgroundImage:[UIImage imageNamed:@"Transparent_Button.png"] forState:UIControlStateNormal];
    [moreButton1 addTarget:self action:@selector(dismissLoginView) forControlEvents:UIControlEventTouchUpInside];
    [moreButton1 setFrame:CGRectMake(250, 5, 60, 32)];
    [loginnavBar.navigationBar addSubview:moreButton1];
    [self.navigationController presentViewController:loginnavBar animated:YES completion:nil];
}

-(void)dismissLoginView
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"Dismiss");
        [self sendStoreDetailRequest];
    }];
}


-(void)setStoreLikeType
{
    INStoreDetailObj *lstoreObj = [storeArray objectAtIndex:0];
    DebugLog(@"like-->%@",lstoreObj.user_like);
    if([lstoreObj.user_like isEqualToString:@"1"])
    {
        [likeStore setImage:[UIImage imageNamed:@"heart_fav.png"] forState:UIControlStateNormal];
        lblAddfav.text = @"Favourited";

    } else {
        [likeStore setImage:[UIImage imageNamed:@"heart_unfav.png"] forState:UIControlStateNormal];
        lblAddfav.text = @"Add to favourite";
    }
}

#pragma MWPhotoBrowser delegate methods
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser
{
    return [_photos count];
}
- (id<MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index
{
    return [_photos objectAtIndex:index];
}

-(NSString *)getStoreDetailsMessageBody{
    
    NSMutableString *message = [[NSMutableString alloc] initWithString:@""];
    INStoreDetailObj *lstoreObj = [storeArray objectAtIndex:0];
    if (lstoreObj.name != nil && ![lstoreObj.name isEqualToString:@""]) {
        [message appendString:[NSString stringWithFormat:@"%@\n",lstoreObj.name]];
    }
    
    NSMutableString *addreessString = [[NSMutableString alloc] initWithString:@""];
    if (![lstoreObj.street isEqualToString:@""] && lstoreObj.street != nil) {
        [addreessString appendString:lstoreObj.street];
    }
    if (![lstoreObj.landmark isEqualToString:@""] && lstoreObj.landmark != nil) {
        if ([addreessString isEqualToString:@""]) {
            [addreessString appendString:lstoreObj.landmark];
        }else{
            [addreessString appendString:[NSString stringWithFormat:@", %@", lstoreObj.landmark]];
        }
    }
    if (![lstoreObj.area isEqualToString:@""] && lstoreObj.area != nil) {
        if ([addreessString isEqualToString:@""]) {
            [addreessString appendString:lstoreObj.area];
        }else{
            [addreessString appendString:[NSString stringWithFormat:@", %@", lstoreObj.area]];
        }
    }
    [message appendString:[NSString stringWithFormat:@"Address: %@\n",addreessString]];
    
    if (lstoreObj.mob_no1 != nil && ![lstoreObj.mob_no1 isEqualToString:@""]) {
        [message appendString:[NSString stringWithFormat:@"Contact: %@\n",lstoreObj.mob_no1]];
    }
    if (lstoreObj.city != nil && ![lstoreObj.city isEqualToString:@""]) {
        [message appendString:[NSString stringWithFormat:@"City: %@\n",lstoreObj.city]];
    }
    if (lstoreObj.open_time != nil && ![lstoreObj.open_time isEqualToString:@""]) {
        [message appendString:[NSString stringWithFormat:@"Timings: %@ To %@\n",lstoreObj.open_time, lstoreObj.close_time]];
    }
    if (lstoreObj.description != nil && ![lstoreObj.description isEqualToString:@""]) {
        [message appendString:[NSString stringWithFormat:@"Description: %@\n",lstoreObj.description]];
    }
    
    if (lstoreObj.latitude != nil && ![lstoreObj.latitude isEqualToString:@""] && lstoreObj.longitude != nil && ![lstoreObj.longitude isEqualToString:@""] && [lstoreObj.latitude doubleValue] != 0 && [lstoreObj.longitude doubleValue] != 0) {
        [message appendString:[NSString stringWithFormat:@"Find us on Google Map : \n %@\n",MAP_URL([lstoreObj.latitude floatValue],[lstoreObj.longitude floatValue])]];
    }
    return message;    
}

#pragma share Callbacks
-(void)smsShareClicked{
    if ([MFMessageComposeViewController canSendText]) {
        NSString *message = [self getStoreDetailsMessageBody];
        MFMessageComposeViewController *messageComposer = [[MFMessageComposeViewController alloc] init];
        messageComposer.messageComposeDelegate = self;
        messageComposer.navigationBar.tintColor  = [UIColor brownColor];
        [messageComposer setBody:message];
        [messageComposer setRecipients:nil];
        [self presentModalViewController:messageComposer animated:YES];
    }else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"You may not have set up sms service on your device.\nPlease check and try again."
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}
#pragma mark MFMessageComposeViewController Delegate Methods
-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
		case MessageComposeResultCancelled:
            DebugLog(@"SMS Cancelled");
			break;
		case MessageComposeResultFailed:
        {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Unknown Error\nSMS failed"
														   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
        }
			break;
		case MessageComposeResultSent:
        {
            DebugLog(@"SMS Sent");
            [self sendStoreShareRequest:@"sms"];
        }
			break;
		default:
			break;
	}
    [self dismissModalViewControllerAnimated:YES];
}

-(void)emailShareClicked{
    if ([MFMailComposeViewController canSendMail]) {
        NSString *message = [self getStoreDetailsMessageBody];
        DebugLog(@"message %@",message);
        MFMailComposeViewController *emailComposer = [[MFMailComposeViewController alloc] init];
        emailComposer.mailComposeDelegate = self;
        emailComposer.navigationBar.tintColor  = [UIColor brownColor];
        [emailComposer setToRecipients:nil];
        [emailComposer setSubject:@""];
        [emailComposer setMessageBody:message isHTML:NO];
        [self presentViewController:emailComposer animated:YES completion:nil];
    }else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"You may not have set up mail service on your device or You may not connected to internet.\nPlease check and try again."
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}
#pragma mark Mail Compose Delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    if (result == MFMailComposeResultFailed) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                        message:NSLocalizedString(@"Email failed to send. Please try again.", nil)
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", nil) otherButtonTitles:nil];
		[alert show];
    }else if (result == MFMailComposeResultSent)
    {
        [self sendStoreShareRequest:@"email"];
    }
    [self dismissViewControllerAnimated:YES completion:nil];

}
-(void)whatsAppShareClicked{
    NSString *message = [self getStoreDetailsMessageBody];
    NSString *urlString = [NSString stringWithFormat:@"whatsapp://send?text=%@",[message stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURL *whatsappURL = [NSURL URLWithString:urlString];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [[UIApplication sharedApplication] openURL: whatsappURL];
        [self sendStoreShareRequest:@"whatsapp"];
    }
}

-(void)twitterShareClicked{
    
    if ([TWTweetComposeViewController canSendTweet]) {
        TWTweetComposeViewController *tweetComposer = [[TWTweetComposeViewController alloc] init];
        NSString *message = [self getStoreDetailsMessageBody];
        DebugLog(@"Length %d",[message length]);
        if ([message length] > 140) {
            [tweetComposer setInitialText:[message substringToIndex:140]];
        }else{
            [tweetComposer setInitialText:message];
        }
        [self presentViewController:tweetComposer animated:YES completion:nil];
        tweetComposer.completionHandler = ^(TWTweetComposeViewControllerResult result){
            if (result == TWTweetComposeViewControllerResultDone) {
                DebugLog(@"User has finished composing the post, and tapped the send button");
                [self sendStoreShareRequest:@"twitter"];
            }
            if (result == TWTweetComposeViewControllerResultCancelled) {
                DebugLog(@"User has cancelled composing the post, and tapped the cancel button");
            }
            [self dismissViewControllerAnimated:YES completion:NULL];
        };
    } else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"You may not have set up twitter service on your device or You may not connected to internet.\nPlease check and try again."
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

-(void)facebookShareClicked{
    FaceBookShareViewController *facebookViewComposer = [[FaceBookShareViewController alloc] initWithNibName:@"FaceBookShareViewController" bundle:nil];
    facebookViewComposer.fbdelegate = self;
    facebookViewComposer.title = @"FACEBOOK SHARE";
    facebookViewComposer.FBtitle = ALERT_TITLE;
    facebookViewComposer.postMessageText = [self getStoreDetailsMessageBody];
    UINavigationController *fbnavBar=[[UINavigationController alloc]initWithRootViewController:facebookViewComposer];
    [self.navigationController presentViewController:fbnavBar animated:YES completion:nil];
}

-(void)faceBookCompletionCallBack:(int)result
{
    DebugLog(@"Customer : faceBookCompletionCallBack %d",result);
    if (result == FACEBOOK_POST_SUCCESS) {
        [self sendStoreShareRequest:@"facebook"];
    }
}
-(void)sendStoreShareRequest:(NSString *)viaString{
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];

    [params setObject:[NSString stringWithFormat:@"%d",self.storeId] forKey:@"place_id"];
    [params setObject:viaString forKey:@"via"];
    
    [httpClient postPath:SHARE_STORE parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
//            NSString* message = [json objectForKey:@"message"];
//            [INUserDefaultOperations showAlert:message];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        [CommonCallback hideProgressHud];
    }];
}

@end
