//
//  INAboutUsViewController.m
//  shoplocal
//
//  Created by Rishi on 30/09/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INAboutUsViewController.h"
#import "constants.h"
#import "INWebViewController.h"
#import "MFSideMenu.h"
#import "INAppDelegate.h"

#define SHOPLOCAL_CONTACT @"+91-22-65160691"
#define FAQ @"http://stage.phonethics.in/shoplocal/info/faqs"

@interface INAboutUsViewController ()

@end

@implementation INAboutUsViewController
@synthesize aboutusTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:FALSE];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:TRUE];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:TRUE];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:FALSE];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                             style:UIBarButtonItemStylePlain
                                                            target:nil
                                                            action:nil];
    [[self navigationItem] setBackBarButtonItem:back];
    //[self setupMenuBarButtonItems];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setAboutusTableView:nil];
    [super viewDidUnload];
}

#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


#pragma set Navigation barbuttons callback
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:MENU_TITLE style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
//    return [[UIBarButtonItem alloc]
//            initWithTitle:MENU_TITLE style:UIBarButtonItemStyleBordered
//            target:self.navigationController.sideMenu
//            action:@selector(toggleRightSideMenu)];
    return nil;
}
- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark Table view methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}


//
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 40;
//}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(0,0,300,30)];
//    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
//    headerLabel.backgroundColor = [UIColor clearColor];
//    headerLabel.frame = CGRectMake(15, 5, 300, 30);
//    headerLabel.font = DEFAULT_BOLD_FONT(16.0f);
//    switch (section) {
//        case 0:
//            headerLabel.text = @"About App";
//            break;
//        case 1:
//            headerLabel.text = @"Info";
//            break;
////        default:
////            headerLabel.text = @"Info";
////            break;
//    }
//    headerLabel.textColor = [UIColor whiteColor];
//    [customView addSubview:headerLabel];
//    return customView;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    NSString *CellIdentifier = [NSString stringWithFormat:@"about%d%d",indexPath.section,indexPath.row] ;
    static  NSString *CellIdentifier = @"aboutCell";
    UIView *topView;
    UILabel *lblTitle;
    UIImageView *thumbImg;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        
//        cell.accessoryType = UITableViewCellAccessoryNone;
//        cell.textLabel.textColor = BROWN_COLOR;
//        cell.textLabel.font = DEFAULT_BOLD_FONT(15);
//        cell.detailTextLabel.textColor = BROWN_COLOR;
//        cell.detailTextLabel.font = DEFAULT_FONT(15);
//        
//        UIView *bgColorView = [[UIView alloc] init];
//        bgColorView.backgroundColor = BROWN_OFFWHITE_COLOR;
//        cell.selectedBackgroundView = bgColorView;
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backgroundColor = [UIColor clearColor];
        cell.layer.shadowOffset = CGSizeMake(-15, 20);
        cell.layer.shadowColor = [UIColor darkGrayColor].CGColor;
        cell.layer.shadowRadius = 5;
        
        cell.layer.shadowPath = [UIBezierPath bezierPathWithRect:cell.bounds].CGPath;
        
        // Create the top view
        topView = [[UIView alloc] initWithFrame:CGRectMake(10, 5, 300, 75)];
        topView.tag = 111;
        topView.backgroundColor = [UIColor whiteColor];
        //topView = [CommonCallback setViewPropertiesWithRoundedCorner:topView];
        topView.clipsToBounds = YES;
        
        
        thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(5, 7, 60, 60)];
        thumbImg.tag = 1;
        thumbImg.contentMode = UIViewContentModeScaleAspectFit;
        //[cell.contentView addSubview:thumbImg];
        [topView addSubview:thumbImg];
        
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(80.0,25.0,210.0,25.0)];
        lblTitle.text = @"";
        lblTitle.tag = 2;
        lblTitle.font = DEFAULT_FONT(17);
        lblTitle.textAlignment = NSTextAlignmentLeft;
        lblTitle.textColor = DEFAULT_COLOR;
        lblTitle.backgroundColor =  [UIColor clearColor];
        lblTitle.minimumScaleFactor = 0.5f;
        lblTitle.adjustsFontSizeToFitWidth = YES;
        //[cell.contentView addSubview:lblTitle];
        [topView addSubview:lblTitle];
        
        //        UILabel *lblline = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 73.0, 300.0, 2.0)];
        //        lblline.text = @"";
        //        lblline.backgroundColor =  LIGHT_ORANGE_COLOR;
        //        [topView addSubview:lblline];
        
        // Add views to contentView
        [cell.contentView addSubview:topView];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = BROWN_OFFWHITE_COLOR;
        cell.selectedBackgroundView = bgColorView;
    }
    
    topView = (UIView *)[cell viewWithTag:111];
    
    switch (indexPath.row) {
        case 0:
        {
//            cell.textLabel.text = @"Version";
//            cell.detailTextLabel.text = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
//            cell.accessoryType = UITableViewCellAccessoryNone;
            
            UILabel *lblTitle = (UILabel *)[topView viewWithTag:2];
            [lblTitle setText:[NSString stringWithFormat:@"%@ (%@)",@"Version",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]]];
            
            UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
            [thumbImgview setImage:[UIImage imageNamed:@"Version.png"]];
        }
            break;
            
        case 1:
        {
            //cell.textLabel.text = @"How Shoplocal Works";
//            cell.textLabel.text = @"www.shoplocal.co.in";
//            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            UILabel *lblTitle = (UILabel *)[topView viewWithTag:2];
            [lblTitle setText:@"www.shoplocal.co.in"];
            
            UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
            [thumbImgview setImage:[UIImage imageNamed:@"Website.png"]];
        }
            break;

        case 2:
        {
//            cell.textLabel.text = @"Privacy Policy";
//            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            UILabel *lblTitle = (UILabel *)[topView viewWithTag:2];
            [lblTitle setText:@"Privacy Policy"];
            
            UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
            [thumbImgview setImage:[UIImage imageNamed:@"Privacy.png"]];
        }
            break;
            
        case 3:
        {
//            cell.textLabel.text = @"Terms and Conditions";
//            cell.detailTextLabel.text = @"";
//            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            UILabel *lblTitle = (UILabel *)[topView viewWithTag:2];
            [lblTitle setText:@"Terms and Conditions"];
            
            UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
            [thumbImgview setImage:[UIImage imageNamed:@"Terms.png"]];
        }
            break;
            
        case 4:
        {
//            cell.textLabel.text = @"FAQ";
//            cell.detailTextLabel.text = @"";
//            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            UILabel *lblTitle = (UILabel *)[topView viewWithTag:2];
            [lblTitle setText:@"FAQ"];
            
            UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
            [thumbImgview setImage:[UIImage imageNamed:@"FAQ.png"]];
        }
            break;
            

        default:
            break;
    }

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 1:
        {
            if([IN_APP_DELEGATE networkavailable])
            {
                //@"How Shoplocal Works";
                //@"Via shoplocal.co.in";
                NSDictionary *socialParams = [NSDictionary dictionaryWithObjectsAndKeys:@"Website",@"Source", nil];
                [INEventLogger logEvent:@"AboutShoplocal" withParams:socialParams];
                INWebViewController *webViewController = [[INWebViewController alloc] initWithNibName:@"INWebViewController" bundle:nil];
                webViewController.title = @"shoplocal.co.in";
                webViewController.urlString = SHOPLOCAL_WEBSITE_URL;
                [self.navigationController pushViewController:webViewController animated:YES];
            }  else   {
                [INUserDefaultOperations showOfflineAlert];
            }
        }
        break;
            
            

        case 2:
        {
            if([IN_APP_DELEGATE networkavailable])
            {
                //@"Privacy Policy";
                NSDictionary *socialParams = [NSDictionary dictionaryWithObjectsAndKeys:@"Policy",@"Source", nil];
                [INEventLogger logEvent:@"AboutShoplocal" withParams:socialParams];
                INWebViewController *webViewController = [[INWebViewController alloc] initWithNibName:@"INWebViewController" bundle:nil];
                webViewController.title = @"Privacy Policy";
                webViewController.urlString = PRIVACY_LINK;
                [self.navigationController pushViewController:webViewController animated:YES];
            }  else   {
                [INUserDefaultOperations showOfflineAlert];
            }
        }
            break;
            
        case 3:
        {
            if([IN_APP_DELEGATE networkavailable])
            {
                //@"Terms and Conditions";
                NSDictionary *socialParams = [NSDictionary dictionaryWithObjectsAndKeys:@"T&C",@"Source", nil];
                [INEventLogger logEvent:@"AboutShoplocal" withParams:socialParams];
                INWebViewController *webViewController = [[INWebViewController alloc] initWithNibName:@"INWebViewController" bundle:nil];
                webViewController.title = @"Terms and Conditions";
                webViewController.urlString = TERMS_AND_CONDITIONS_LINK;
                [self.navigationController pushViewController:webViewController animated:YES];
            }  else   {
                [INUserDefaultOperations showOfflineAlert];
            }
            
        }
            break;
            
        case 4:
        {
            if([IN_APP_DELEGATE networkavailable])
            {
                //@"FAQ";
                NSDictionary *socialParams = [NSDictionary dictionaryWithObjectsAndKeys:@"FAQ",@"Source", nil];
                [INEventLogger logEvent:@"AboutShoplocal" withParams:socialParams];
                INWebViewController *webViewController = [[INWebViewController alloc] initWithNibName:@"INWebViewController" bundle:nil];
                webViewController.title = @"FAQ";
                webViewController.urlString = FAQ;
                [self.navigationController pushViewController:webViewController animated:YES];
            }  else   {
                [INUserDefaultOperations showOfflineAlert];
            }
            
        }
            break;
            
        default:
            break;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark Mail Compose Delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    if (result == MFMailComposeResultFailed) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                        message:NSLocalizedString(@"Email failed to send. Please try again.", nil)
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", nil) otherButtonTitles:nil];
		[alert show];
    }else if (result == MFMailComposeResultSent)
    {
        DebugLog(@"MFMailComposeResultSent");
    }
	[self dismissViewControllerAnimated:YES completion:nil];
}
@end
