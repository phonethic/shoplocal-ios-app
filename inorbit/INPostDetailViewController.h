//
//  INPostDetailViewController.h
//  shoplocal
//
//  Created by Rishi on 20/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import "MWPhotoBrowser.h"
#import "FaceBookShareViewController.h"

@interface INPostDetailViewController : UIViewController <UIActionSheetDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,FacebookProtocolDelegate,MWPhotoBrowserDelegate>{
    MBProgressHUD *hud;
}
@property (nonatomic, readwrite) int postType;
@property (nonatomic, copy) NSString *postlikeType;
@property (nonatomic, copy) NSString *postTitle;
@property (nonatomic, copy) NSString *postDescription;
@property (nonatomic, copy) NSString *postimageUrl;
@property (nonatomic, copy) NSString *postimageTitle;
@property (nonatomic, copy) NSString *postOfferDateTime;
@property (nonatomic, copy) NSString *postIsOffered;
@property (nonatomic, copy) NSString *postDateTime;

@property (nonatomic, readwrite) NSInteger postId;
@property (nonatomic, copy) NSString *storeName;
@property (nonatomic, readwrite) NSInteger placeId;
@property (strong, nonatomic) NSMutableArray *galleryArray;
@property (strong, nonatomic) NSMutableArray *photos;


@property (nonatomic, copy) NSString *likescountString;
@property (nonatomic, copy) NSString *sharecountString;
@property (nonatomic, copy) NSString *viewscountString;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UIView *postView;
@property (strong, nonatomic) IBOutlet UILabel *titlelbl;
@property (strong, nonatomic) IBOutlet UILabel *descriptionlbl;
@property (strong, nonatomic) IBOutlet UIImageView *postimageView;

@property (strong, nonatomic) IBOutlet UIButton *likepostBtn;
@property (strong, nonatomic) IBOutlet UIButton *sharepostBtn;
@property (strong, nonatomic) IBOutlet UIButton *viewpostBtn;
@property (strong, nonatomic) IBOutlet UIButton *sharepostBtn1;

@property (strong, nonatomic) IBOutlet UIImageView *likepostBtnImageView;
@property (strong, nonatomic) IBOutlet UIImageView *sharepostBtnImageView;
@property (strong, nonatomic) IBOutlet UIImageView *viewpostBtnImageView;
@property (strong, nonatomic) IBOutlet UIImageView *sharepostBtn1ImageView;

@property (strong, nonatomic) IBOutlet UIImageView *himgSeperator1;
@property (strong, nonatomic) IBOutlet UIImageView *himgSeperator2;
@property (strong, nonatomic) IBOutlet UIImageView *himgSeperator3;
@property (strong, nonatomic) IBOutlet UIImageView *himgSeperator4;

@property (strong, nonatomic) IBOutlet UIImageView *vimgSeperator1;
@property (strong, nonatomic) IBOutlet UIImageView *vimgSeperator2;

@property (strong, nonatomic) IBOutlet UILabel *lblPostedOn;
@property (strong, nonatomic) IBOutlet UILabel *lblOfferDate;



- (IBAction)likepostBtnPressed:(id)sender;
- (IBAction)sharepostBtnPressed:(id)sender;

@end
