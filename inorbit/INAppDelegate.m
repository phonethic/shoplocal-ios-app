//
//  INAppDelegate.m
//  shoplocal
//
//  Created by Rishi on 22/07/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INAppDelegate.h"
#import "ACTReporter.h"     // To Track iOS Downloads in Google AdWords
#import "NMSplashViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+WebCache.h"
#import "INMerchantViewController.h"
#import "INAreaObject.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import "SideMenuViewController.h"
#import "MerchantSideMenuViewController.h"
#import "INChangeLocationViewController.h"
#import "UAirship.h"
#import "UAConfig.h"
#import "UAPush.h"
#import <MessageUI/MessageUI.h>
#import "INAllStoreViewController.h"
#import "SIAlertView.h"
#import "INMerchantLoginViewController.h"

#import "CommonCallback.h"
#import "FacebookShareAPI.h"

#define AREAS_LINK(ID) [NSString stringWithFormat:@"%@%@%@place_api/areas?iso-code=%@",LIVE_SERVER,URL_PREFIX,API_VERSION,ID]
#define DEVICE_ID_URL [NSString stringWithFormat:@"%@%@device_api/device",URL_PREFIX,API_VERSION]

NSString *const FBSessionStateChangedNotification = @"com.facebook.shoplocal:MSessionStateChangedNotification";

@implementation INAppDelegate
@synthesize networkavailable;
@synthesize splashviewController;
@synthesize databasePath;
@synthesize loggedInUserID = _loggedInUserID;
@synthesize loggedInSession = _loggedInSession;
@synthesize splashJson;
@synthesize areaArray;
@synthesize customerSideMenu;
@synthesize merchantSideMenu;
@synthesize activePlaceId;
@synthesize activeAreaId;
@synthesize custleftSideMenuController;
@synthesize storeIdToOpen;

/*
 * Callback for session changes.
 */
- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error
{
    switch (state) {
        case FBSessionStateOpen:
            if (!error) {
                // We have a valid session
                //DebugLog(@"User session found");
                [FBRequestConnection
                 startForMeWithCompletionHandler:^(FBRequestConnection *connection,
                                                   NSDictionary<FBGraphUser> *user,
                                                   NSError *error) {
                     if (!error) {
                         self.loggedInUserID = [user objectForKey:@"id"];
                         self.loggedInSession = FBSession.activeSession;
                     }
                 }];
            }
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            [FBSession.activeSession closeAndClearTokenInformation];
            break;
        default:
            break;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:FBSessionStateChangedNotification object:session];
    
//    if (error) {
//        UIAlertView *alertView = [[UIAlertView alloc]
//                                  initWithTitle:@"Only Error"
//                                  message:error.localizedDescription
//                                  delegate:nil
//                                  cancelButtonTitle:@"OK"
//                                  otherButtonTitles:nil];
//        [alertView show];
//    }

    if (error) {
        // Handle errors
        [self handleAuthError:error];
    } else {
        // No error
    }
}

/*
 * Opens a Facebook session and optionally shows the login UX.
 */
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI {
    NSArray *permissions = @[@"public_profile", @"email",@"user_birthday"];
    return [FBSession openActiveSessionWithReadPermissions:permissions
                                              allowLoginUI:allowLoginUI
                                         completionHandler:^(FBSession *session,
                                                             FBSessionState state,
                                                             NSError *error) {
                                             [self sessionStateChanged:session
                                                                 state:state
                                                                 error:error];
                                         }];
}

/*
 *
 */
- (void) closeSession {
    [FBSession.activeSession closeAndClearTokenInformation];
}

/*
 * If we have a valid session at the time of openURL call, we handle
 * Facebook transitions by passing the url argument to handleOpenURL
 */
//- (BOOL)application:(UIApplication *)application
//            openURL:(NSURL *)url
//  sourceApplication:(NSString *)sourceApplication
//         annotation:(id)annotation {
//    // attempt to extract a token from the url
//    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
//}
// In order to process the response you get from interacting with the Facebook login process
// and to handle any deep linking calls from Facebook
// you need to override application:openURL:sourceApplication:annotation:

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    storeIdToOpen = @"";
    BOOL wasHandled = [FBAppCall handleOpenURL:url
                             sourceApplication:sourceApplication
                               fallbackHandler:^(FBAppCall *call)
      {
            DebugLog(@"url deep link: %@", url);
            DebugLog(@"call deep link: %@", call);
            if([[call appLinkData] targetURL] != nil) {
                DebugLog(@"[call appLinkData]: %@", [call appLinkData]);
                NSString *targetURL = @"";
                // get the object ID string from the deep link URL
                // we use the substringFromIndex so that we can delete the leading '/' from the targetURL
                targetURL = [[[call appLinkData] targetURL].path substringFromIndex:1];
                DebugLog(@"targetURL %@",targetURL);
                if ([targetURL length] > 0) {
                    [self handleDeepLinking:targetURL];
                }
            } else {
                DebugLog(@"Unhandled deep link(call): %@", [[call appLinkData] targetURL]);
                if(url != nil)
                {
                // Parse the incoming URL to look for a target_url parameter
                NSString *targetURL = @"";
                NSString *urlString = [url relativeString];
                DebugLog(@"relativePath: %@",urlString);
                DebugLog(@"components: %@",[urlString componentsSeparatedByString:@"&"]);
                for (NSString *str in [urlString componentsSeparatedByString:@"&"]) {
                    if ([str hasPrefix:@"target_url="]) {
                        targetURL = [[str componentsSeparatedByString:@"="] objectAtIndex:1];
                        break;
                    }
                }
                DebugLog(@"targetURL %@",targetURL);
                if ([targetURL length] > 0) {
                    [self handleDeepLinking:targetURL];
                }
    //                NSString *query = [url query];
    //                NSDictionary *params = [FacebookShareAPI parseURLParams:query];
    //                
    //                // Check if target URL exists
    //                NSString *appLinkDataString = [params valueForKey:@"al_applink_data"];
    //                if (appLinkDataString)
    //                {
    //                    NSError *error = nil;
    //                    NSDictionary *applinkData =
    //                    [NSJSONSerialization JSONObjectWithData:[appLinkDataString dataUsingEncoding:NSUTF8StringEncoding]
    //                                                    options:0
    //                                                      error:&error];
    //                    if (!error && [applinkData isKindOfClass:[NSDictionary class]] && applinkData[@"target_url"]) {
    //                       // self.refererAppLink = applinkData[@"referer_app_link"];
    //                        NSString *targetURLString = applinkData[@"target_url"];
    //                       
    //                        // Show the incoming link in an alert
    //                        // Your code to direct the user to the
    //                        // appropriate flow within your app goes here
    //                        [[[UIAlertView alloc] initWithTitle:@"Received link:"
    //                                                    message:targetURLString
    //                                                   delegate:nil
    //                                          cancelButtonTitle:@"OK"
    //                                          otherButtonTitles:nil] show];
    //                    }
    //                }
                }else{
                    DebugLog(@"Unhandled deep link (url): %@", url);
                }
            }
      }];
    return wasHandled;
}

- (BOOL)isNumeric:(NSString*)inputString{
    BOOL isValid = NO;
    NSCharacterSet *alphaNumbersSet = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *stringSet       = [NSCharacterSet characterSetWithCharactersInString:inputString];
    isValid = [alphaNumbersSet isSupersetOfSet:stringSet];
    return isValid;
}

-(void)handleDeepLinking:(NSString *)targetURL
{
    NSString *storeId       = [targetURL lastPathComponent];
    DebugLog(@"storeId : %@", storeId);
    if ([self isNumeric:storeId]) {
        DebugLog(@"Found only number");
//        int activeAreaIndex    = [[targetURL pathComponents] indexOfObject:storeId] - 3;
//        NSString *activeAreaSlugFromDeepLink    = [[targetURL pathComponents] objectAtIndex:activeAreaIndex];
//        DebugLog(@"activeAreaSlugFromDeepLink : %@", activeAreaSlugFromDeepLink);
//        NSString *activeAreaSlugFromDatabase   = [self getActiveSlugFromAreaTable];
//        DebugLog(@"activeAreaSlugFromDatabase : %@", activeAreaSlugFromDatabase);
        
//        if ([activeAreaSlugFromDeepLink isEqualToString:activeAreaSlugFromDatabase]) {
            storeIdToOpen = storeId;
            [self manuallyOpenViewController:STOREDETAILS_FBDEEPLINKINGNTYPE];
//        }
    }else{
        DebugLog(@"Found number + other");
    }
}

- (void)handleAuthError:(NSError *)error
{
    NSString *alertText;
    DebugLog(@"================handleAuthError============\n error %@ \n ================================================",error);
    
    if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
        // Error requires people using you app to make an action outside your app to recover
        alertText = [FBErrorUtility userMessageForError:error];
        [self performSelector:@selector(showMessage:) withObject:alertText afterDelay:1.0];
        
    } else {
        DebugLog(@"errorCategoryForError %d",[FBErrorUtility errorCategoryForError:error]);

        // You need to find more information to handle the error within your app
        if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
            //The user refused to log in into your app, either ignore or...
            alertText = @"Login cancelled\nYour Login could not be completed because you didn't grant the necessary permissions.";
            [self performSelector:@selector(showMessage:) withObject:alertText afterDelay:1.0];

        } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession){
            // We need to handle session closures that happen outside of the app
            alertText = @"Session Error\nYour current session is no longer valid. Please log in again.";
            [self performSelector:@selector(showMessage:) withObject:alertText afterDelay:1.0];

        } else {
            
            // Get more error information from the error
            // this is the parsed result of the graph call; some errors can appear here, too, sadly
            NSDictionary *parsedResponse = [error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"];
            NSDictionary *errorInformation = [[parsedResponse objectForKey:@"body"] objectForKey:@"error"];
            DebugLog(@"errorInformation %@",errorInformation);
            
            int errorCode = 0;
            if ([errorInformation objectForKey:@"code"]){
                errorCode = [[errorInformation objectForKey:@"code"] integerValue];
            }
            DebugLog(@"errorCode %d",errorCode);
            
            int errorSubcode = 0;
            if ([errorInformation objectForKey:@"error_subcode"]){
                errorSubcode = [[errorInformation objectForKey:@"error_subcode"] integerValue];
            }
            DebugLog(@"errorSubcode %d",errorSubcode);

            if (errorCode == 190 && (errorSubcode == 459 || errorSubcode == 464)) {
                alertText = @"Error validating access token: You cannot access the app till you log in to www.facebook.com and follow the instructions given.";
            }else{
                // All other errors that can happen need retries
                // Show the user a generic error message
                alertText = @"Something went wrong.Please retry";
            }
            [self performSelector:@selector(showMessage:) withObject:alertText afterDelay:1.0];
        }
    }
    // Clear this token
    [FBSession.activeSession closeAndClearTokenInformation];
}

- (void)showMessage:(NSString *)text
{
    [[[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                message:text
                               delegate:nil
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
}

#pragma iOS 6 orientation Methods
- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    NSUInteger orientations = UIInterfaceOrientationMaskPortrait;
    DebugLog(@"self.window.rootViewController %@",self.window.rootViewController);
    if (self.window.rootViewController) {
        if ([self.window.rootViewController isKindOfClass:[NMSplashViewController class]]) {
            orientations = [self.window.rootViewController supportedInterfaceOrientations];
        }
        else{
            UIViewController* presented = [[(UINavigationController *)self.window.rootViewController viewControllers] lastObject];
            DebugLog(@"presented %@",presented);
            orientations = [presented supportedInterfaceOrientations];
//            DebugLog(@"orientations %lu",(unsigned long)orientations);
//            if (orientations == UIInterfaceOrientationMaskPortrait) {
//                DebugLog(@"UIInterfaceOrientationMaskPortrait");
//            }
//            else  if (orientations == UIInterfaceOrientationMaskLandscapeLeft) {
//                DebugLog(@"UIInterfaceOrientationMaskLandscapeLeft");
//            }
//            else  if (orientations == UIInterfaceOrientationMaskLandscapeRight) {
//                DebugLog(@"UIInterfaceOrientationMaskLandscapeRight");
//            }
//            else  if (orientations == UIInterfaceOrientationMaskPortraitUpsideDown) {
//                DebugLog(@"UIInterfaceOrientationMaskPortraitUpsideDown");
//            }
//            else  if (orientations == UIInterfaceOrientationMaskAll) {
//                DebugLog(@"UIInterfaceOrientationMaskAll");
//            }
//            else  if (orientations == UIInterfaceOrientationMaskLandscape) {
//                DebugLog(@"UIInterfaceOrientationMaskLandscape");
//            }
//            else  if (orientations == UIInterfaceOrientationMaskAllButUpsideDown) {
//                DebugLog(@"UIInterfaceOrientationMaskAllButUpsideDown");
//            }
        }
    }
    return orientations;
}

- (void) application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    //Place your code to handle the notification here.
    DebugLog(@"Notification %@",notification);
    
    [self handleLocalNotification:notification];
}


-(void)handleLocalNotification:(UILocalNotification *)notification
{
    
    //[[UIApplication sharedApplication] cancelAllLocalNotifications];
    [[UIApplication sharedApplication] cancelLocalNotification:notification] ;
    DebugLog(@"Notification %@",[[UIApplication sharedApplication] scheduledLocalNotifications]);
    
    if ([[notification.userInfo objectForKey:@"Type"] length] > 0) {
        
        NSString *notificationType = [notification.userInfo objectForKey:@"Type"];
        NSDictionary *editstoreParams = [NSDictionary dictionaryWithObjectsAndKeys:notificationType,@"Type", nil];
        [INEventLogger logEvent:@"App_LaunchLocalNotification" withParams:editstoreParams];
        
        DebugLog(@"notificationType %@",notificationType);
        
       // [INUserDefaultOperations showAlert:[NSString stringWithFormat:@"Local Notifitaion type %@",notificationType]];

        if ([notificationType isEqualToString:APP_NOT_LAUNCHED_LNTYPE]) {
            return;
        }else {
            [self setUpNavigationBarAppeance];
            if ([notificationType isEqualToString:TALKNOW_LNTYPE] || [notificationType isEqualToString:EDITSTORE_LNTYPE]) {
                if ([INUserDefaultOperations isMerchantLoggedIn]) {
                    if ([notificationType isEqualToString:TALKNOW_LNTYPE]) {
                        DebugLog(@"TALKNOW");
                        [self manuallyOpenViewController:TALKNOW_LNTYPE];
                    }
                    else if ([notificationType isEqualToString:EDITSTORE_LNTYPE]) {
                        DebugLog(@"EDITSTORE");
                        [self manuallyOpenViewController:EDITSTORE_LNTYPE];
                    }
                }else{
                    if(self.merchantviewController != nil)
                    {
                        self.merchantviewController.switchView = 0;
                    }
                }
            }else if ([notificationType isEqualToString:CUSTOMERSPECIALDATE_LNTYPE]) {
                if ([INUserDefaultOperations isCustomerLoggedIn]) {
                    DebugLog(@"CUSTOMERSPECIALDATE");
                    [self manuallyOpenViewController:CUSTOMERSPECIALDATE_LNTYPE];
                    
                }
            }else if ([notificationType isEqualToString:CUSTOMERFAVOURITESTORE_LNTYPE]) {
                DebugLog(@"CUSTOMERFAVOURITESTORE");
                [self manuallyOpenViewController:CUSTOMERFAVOURITESTORE_LNTYPE];
            }
        }
    }
}

-(void)manuallyOpenViewController:(NSString *)notificationType{
    if ([notificationType isEqualToString:TALKNOW_LNTYPE] || [notificationType isEqualToString:EDITSTORE_LNTYPE]) {
        if ([self.window.rootViewController isEqual:customerSideMenu.navigationController]) {
            self.window.rootViewController = merchantnavigationController;
            [INUserDefaultOperations setWelcomeScreenState:@"merchant"];
        }else{
            [merchantnavigationController popToRootViewControllerAnimated:NO];
        }
        [self.window makeKeyAndVisible];

        NSDictionary *dictionary;
        if ([notificationType isEqualToString:TALKNOW_LNTYPE]) {
            dictionary = [NSDictionary dictionaryWithObject:TALKNOW_LNTYPE forKey:IN_MERCHANT_SHOW_LOCAL_NOTIFICATION_KEY];
        }
        else if ([notificationType isEqualToString:EDITSTORE_LNTYPE]) {
            dictionary = [NSDictionary dictionaryWithObject:EDITSTORE_LNTYPE forKey:IN_MERCHANT_SHOW_LOCAL_NOTIFICATION_KEY];
        }
        [self performSelector:@selector(postNotificationWithDictionary:) withObject:dictionary afterDelay:2.0];
        
    }else if ([notificationType isEqualToString:CUSTOMERSPECIALDATE_LNTYPE] || [notificationType isEqualToString:CUSTOMERFAVOURITESTORE_LNTYPE] || [notificationType isEqualToString:STOREDETAILS_FBDEEPLINKINGNTYPE]) {
        
        if ([self.window.rootViewController isEqual:merchantnavigationController]) {
            if(self.merchantviewController != nil && ![INUserDefaultOperations isMerchantLoggedIn])
            {
                self.merchantviewController.switchView = 0;
                if([merchantnavigationController.visibleViewController isKindOfClass:[INMerchantLoginViewController class]]){
                    DebugLog(@"Got INMerchantLoginViewController");
                    [merchantnavigationController dismissViewControllerAnimated:NO completion:nil];
                }
//                DebugLog(@"Got 1 %@",merchantnavigationController.presentedViewController);
//                DebugLog(@"Got 2 %@",merchantnavigationController.presentingViewController);
//                DebugLog(@"Got 3 %@",merchantnavigationController.presentingViewController.presentedViewController);
//
//                DebugLog(@"Got 1 %@",merchantnavigationController.parentViewController.presentedViewController);
//                DebugLog(@"Got 2 %@",merchantnavigationController.parentViewController.presentingViewController);
//                DebugLog(@"Got 3 %@",merchantnavigationController.parentViewController.presentingViewController.presentedViewController);
//
//                DebugLog(@"Got 4 %@",[merchantnavigationController viewControllers]);
//                DebugLog(@"Got 5 %@",merchantnavigationController.visibleViewController);

            }
            self.window.rootViewController = customerSideMenu.navigationController;
            [INUserDefaultOperations setWelcomeScreenState:@"customer"];
        }
        [self.window makeKeyAndVisible];
        
        NSDictionary *dictionary;
        if ([notificationType isEqualToString:CUSTOMERSPECIALDATE_LNTYPE]) {
            dictionary = [NSDictionary dictionaryWithObject:CUSTOMERSPECIALDATE_LNTYPE forKey:IN_CUSTOMER_SHOW_LOCAL_NOTIFICATION_KEY];
        }else if ([notificationType isEqualToString:CUSTOMERFAVOURITESTORE_LNTYPE]) {
            dictionary = [NSDictionary dictionaryWithObject:CUSTOMERFAVOURITESTORE_LNTYPE forKey:IN_CUSTOMER_SHOW_LOCAL_NOTIFICATION_KEY];
        }else if ([notificationType isEqualToString:STOREDETAILS_FBDEEPLINKINGNTYPE] && [storeIdToOpen length] > 0) {
            dictionary = [NSDictionary dictionaryWithObjectsAndKeys:STOREDETAILS_FBDEEPLINKINGNTYPE,IN_CUSTOMER_SHOW_LOCAL_NOTIFICATION_KEY,storeIdToOpen,STORE_ID, nil];
        }
        [self performSelector:@selector(postNotificationWithDictionary:) withObject:dictionary afterDelay:2.0];
    }
}

-(void)postNotificationWithDictionary:(NSDictionary *)dictionary
{
    if ([self.window.rootViewController isEqual:merchantnavigationController]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:IN_MERCHANT_SHOW_LOCAL_NOTIFICATION object:nil userInfo:dictionary];
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_SHOW_LOCAL_NOTIFICATION object:nil userInfo:dictionary];
    }
}

#pragma sidemenu callbacks
- (void) setupNavigationControllerApp {
    ///////////////////////////Customer Side Menu/////////////////////////////////////////////////////////////////
    DebugLog(@"INITIALLY activePlaceid %@ activeAreaId %@",activePlaceId,activeAreaId);
//    if (activePlaceid != nil && ![activePlaceid isEqualToString:@""]) {
//        // initial view is : offers
////        self.viewController = [[INViewController alloc] initWithNibName:@"INViewController" bundle:nil];
////        clientnavigationController = [[UINavigationController alloc] initWithRootViewController:self.viewController];
////        
////        custleftSideMenuController.launchscreenType = LAUNCH_SCREEN_IS_OFFERS;
//        
//        // initial view is : splash
//        splashviewController    = [[NMSplashViewController alloc] initWithNibName:@"NMSplashViewController" bundle:nil];
//        splashviewController.place_id   = activePlaceid;
//        splashviewController.comeFrom = SPLASH_SCREEN;
//        clientnavigationController = [[UINavigationController alloc] initWithRootViewController:self.splashviewController];
//        custleftSideMenuController.launchscreenType = LAUNCH_SCREEN_IS_SPLASHSCREEN;

//        
//    }else{
//        INChangeLocationViewController *changeLocationViewController = [[INChangeLocationViewController alloc] initWithNibName:@"INChangeLocationViewController" bundle:nil];
//        changeLocationViewController.comeFrom = SPLASH_SCREEN;
//        clientnavigationController = [[UINavigationController alloc] initWithRootViewController:changeLocationViewController];
//        
//        custleftSideMenuController.launchscreenType = LAUNCH_SCREEN_IS_LOCATION;
//    }
    DebugLog(@"tour state -- >%d",[INUserDefaultOperations getTourFinishedState]);
    if (activeAreaId != (NSString *)[NSNull null]  && activeAreaId != nil && ![activeAreaId isEqualToString:@""]) {
        activePlaceId = [self getActiveShoplocalPlaceIdFromAreaTable];
//        // initial view is : splash
//        splashviewController    = [[NMSplashViewController alloc] initWithNibName:@"NMSplashViewController" bundle:nil];
//        splashviewController.place_id   = activePlaceId;
//        splashviewController.comeFrom = SPLASH_SCREEN;
//        clientnavigationController = [[UINavigationController alloc] initWithRootViewController:self.splashviewController];
//        custleftSideMenuController.launchscreenType = LAUNCH_SCREEN_IS_SPLASHSCREEN;
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        // initial view is : splash
        INAllStoreViewController *allStoresController    = [[INAllStoreViewController alloc] initWithNibName:@"INAllStoreViewController" bundle:nil];
        //allStoresController.title = ALL_BUSINESS;
        allStoresController.comeFrom = SPLASH_SCREEN;
        clientnavigationController = [[UINavigationController alloc] initWithRootViewController:allStoresController];
        custleftSideMenuController.launchscreenType = LAUNCH_SCREEN_IS_ALL_BUSINESS;

    }else{
      //  if ([INUserDefaultOperations getTourFinishedState]) {
            INChangeLocationViewController *changeLocationViewController = [[INChangeLocationViewController alloc] initWithNibName:@"INChangeLocationViewController" bundle:nil];
            changeLocationViewController.comeFrom = SPLASH_SCREEN;
            clientnavigationController = [[UINavigationController alloc] initWithRootViewController:changeLocationViewController];
            custleftSideMenuController.launchscreenType = LAUNCH_SCREEN_IS_LOCATION;
            [INEventLogger logPageViews:clientnavigationController];
//        }else{
//            INTourViewController *tourViewController = [[INTourViewController alloc] initWithNibName:@"INTourViewController" bundle:nil];
//            clientnavigationController = [[UINavigationController alloc] initWithRootViewController:tourViewController];
//            custleftSideMenuController.launchscreenType = LAUNCH_SCREEN_IS_SPLASHSCREEN;
//        }
        

    }
    
    clientnavigationController.navigationBar.translucent = NO;
    
    UIButton *locationBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
    [locationBtn setTitle:@"" forState:UIControlStateNormal];
    [locationBtn setBackgroundImage:[UIImage imageNamed:@"live_location.png"] forState:UIControlStateNormal];
    [locationBtn addTarget:self action:@selector(changeLocationClicked:) forControlEvents:UIControlEventTouchUpInside];
    [locationBtn setFrame:CGRectMake(90, 0, 200, 44)];
    [locationBtn setTitleEdgeInsets:UIEdgeInsetsMake(0.0, -20.0, 0.0, 0.0)];
    //[locationBtn setImageEdgeInsets:UIEdgeInsetsMake(0.0, 0.0, 0.0, 50.0)];
    locationBtn.titleLabel.numberOfLines = 2;
    locationBtn.tag = 1100;
    locationBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    locationBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [locationBtn.titleLabel setFont:DEFAULT_FONT(14.0)];
    [clientnavigationController.navigationBar addSubview:locationBtn];
    
    NSString *selectedArea = [IN_APP_DELEGATE getActivePlaceNameFromAreaTable];
    DebugLog(@"selectedArea %@",selectedArea);
    
    if (selectedArea != nil && ![selectedArea isEqualToString:@""]) {
        [locationBtn setTitle:selectedArea forState:UIControlStateNormal];
    }
    
    customerSideMenu  = [MFSideMenu menuWithNavigationController:clientnavigationController
                                       leftSideMenuController:custleftSideMenuController
                                      rightSideMenuController:nil];
    custleftSideMenuController.sideMenu = customerSideMenu;

    ///////////////////////////Merchant Side Menu/////////////////////////////////////////////////////////////////
    if(self.merchantviewController == nil)
    {
        self.merchantviewController = [[INMerchantViewController alloc] initWithNibName:@"INMerchantViewController" bundle:nil];
        merchantnavigationController = [[UINavigationController alloc] initWithRootViewController:self.merchantviewController];
        UIImage *image1 = [UIImage imageNamed:@"bar_icon.png"];
        UIImageView *imageView1= [[UIImageView alloc] initWithImage:image1];
        imageView1.frame = CGRectMake(5, 10, 30, 30);
        imageView1.tag = 1000;
        
        UILabel *lbl2=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(imageView1.frame)+20, 0, 220, 44)];
        [lbl2 setText:@"Business Dashboard"];
        lbl2.backgroundColor = [UIColor clearColor];
        lbl2.textAlignment = NSTextAlignmentCenter;
        lbl2.textColor = [UIColor whiteColor];
        lbl2.font = DEFAULT_FONT(18);
        lbl2.tag = 1003;
        
        UIButton *moreButton1 =  [UIButton buttonWithType:UIButtonTypeCustom];
        moreButton1.tag = 1002;
        moreButton1.titleLabel.font = [UIFont systemFontOfSize:15.0];
        [moreButton1 setImage:[UIImage imageNamed:@"switch.png"] forState:UIControlStateNormal];
        [moreButton1 addTarget:self action:@selector(switchViewController) forControlEvents:UIControlEventTouchUpInside];
        [moreButton1 setFrame:CGRectMake(CGRectGetMaxX(lbl2.frame)+10, 5, 32, 32)];
        
        [merchantnavigationController.navigationBar addSubview:imageView1];
        [merchantnavigationController.navigationBar addSubview:lbl2];
        [merchantnavigationController.navigationBar addSubview:moreButton1];
        merchantnavigationController.navigationBar.translucent = NO;
        [INEventLogger logPageViews:merchantnavigationController];
    }
    
//    MerchantSideMenuViewController *merchantleftSideMenuController = [[MerchantSideMenuViewController alloc] init];
//    merchantSideMenu  = [MFSideMenu menuWithNavigationController:merchantnavigationController
//                                          leftSideMenuController:merchantleftSideMenuController
//                                         rightSideMenuController:nil];
//    merchantleftSideMenuController.sideMenu = merchantSideMenu;

    ///////////////////////////Set initial View Controller////////////////////////////////////////////////////////
//    splashviewController = nil;
    if (activeAreaId != nil && ![activeAreaId isEqualToString:@""]) {
       [self initialViewController];
    }else{
        self.window.rootViewController = customerSideMenu.navigationController;
        [INUserDefaultOperations setWelcomeScreenState:@"customer"];
    }
    [self.window makeKeyAndVisible];
}


-(void)changeLocationClicked:(id) sender
{
     DebugLog(@"Location Btn Clicked");
    [self showChangeLocationModal];
}


-(void)showChangeLocationModal {
    [INEventLogger logEvent:@"Tab_Location"];
    INChangeLocationViewController *changeLocationViewController = [[INChangeLocationViewController alloc] initWithNibName:@"INChangeLocationViewController" bundle:nil] ;
    changeLocationViewController.title = @"Change Location";
    changeLocationViewController.comeFrom = POPOVER;
    UINavigationController *loginnavBar=[[UINavigationController alloc]initWithRootViewController:changeLocationViewController];
    loginnavBar.navigationBar.translucent = FALSE;
    UIButton *moreButton1 =  [UIButton buttonWithType:UIButtonTypeCustom];
    [moreButton1 setTitle:@"Cancel" forState:UIControlStateNormal];
    moreButton1.titleLabel.font = [UIFont systemFontOfSize:17.0];
    //[moreButton1 setBackgroundImage:[UIImage imageNamed:@"Transparent_Button.png"] forState:UIControlStateNormal];
    [moreButton1 addTarget:self action:@selector(dismissLoginView) forControlEvents:UIControlEventTouchUpInside];
    [moreButton1 setFrame:CGRectMake(250, 7, 70, 32)];
    [loginnavBar.navigationBar addSubview:moreButton1];
    [clientnavigationController presentViewController:loginnavBar animated:YES completion:nil];
}

-(void)dismissLoginView
{
    [clientnavigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"Dismiss");
    }];
}

-(void)initialViewController{
    NSString *state = [INUserDefaultOperations getWelcomeScreenState];
    DebugLog(@"initialViewController state %@",state);
    [UIView  transitionWithView:self.window duration:0.8  options:UIViewAnimationOptionTransitionFlipFromLeft
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         if (state != nil && [state isEqualToString:@"merchant"]) {
                             self.window.rootViewController = merchantnavigationController;
                             [INUserDefaultOperations setWelcomeScreenState:@"merchant"];
                         }else{
                             self.window.rootViewController = customerSideMenu.navigationController;
                             [INUserDefaultOperations setWelcomeScreenState:@"customer"];
                         }
                         [self.window makeKeyAndVisible];
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:nil];
}

-(void)switchViewController
{
    [UIView  transitionWithView:self.window duration:0.8  options:UIViewAnimationOptionTransitionFlipFromLeft
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         if([self.window.rootViewController isEqual:customerSideMenu.navigationController])
                         {
                             self.window.rootViewController = merchantnavigationController;
                             [INUserDefaultOperations setWelcomeScreenState:@"merchant"];
                             [self.window makeKeyAndVisible];
                         } else {
                             self.window.rootViewController = customerSideMenu.navigationController;
                             [INUserDefaultOperations setWelcomeScreenState:@"customer"];
                             [self.window makeKeyAndVisible];
                         }
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:nil];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    storeIdToOpen = @"";
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[UAPush shared] setBadgeNumber:5];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[UAPush shared] resetBadge];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
     [FBProfilePictureView class];
    //Add Reachability
    [self startCheckNetwork];

    //Add Flurry
    [Flurry startSession:FLURRY_APPID];
    //[Flurry setDebugLogEnabled:YES];
    
    //ADD Crittercism
    [Crittercism enableWithAppID:CRITTERCISM_APPID];
    
    // Google iOS Download tracking snippet
    [ACTConversionReporter reportWithConversionID:@"978544645" label:@"Pg2bCPPSmQcQhdDN0gM" value:@"0" isRepeatable:NO];

    // Create Database
    [self checkAndCreateDatabase];
    

    //[self registerUniqueDeviceId];
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    DebugLog(@"v->%@",version);
    if ([[INUserDefaultOperations getAppVersion] isEqualToString:@"0.0"]) {
        [INEventLogger logEvent:@"App_Installs"];
        [INEventLogger logEvent:@"App_DailyEngagedUsers"];
        [INEventLogger logEvent:@"App_MonthlyEngagedUsers"];
        [INUserDefaultOperations setAppVersion:version];
        [INUserDefaultOperations setDailySyncDate];
        [INUserDefaultOperations setMonthlySyncDate];
        [INUserDefaultOperations setAppRaiterState:TRUE];
//        [[UIApplication sharedApplication] cancelAllLocalNotifications];
    } else {
        if(![[INUserDefaultOperations getAppVersion] isEqualToString:version]) {
            [INEventLogger logEvent:@"App_Upgrades"];
            [INUserDefaultOperations setAppVersion:version];
            [INUserDefaultOperations setAppRaiterState:TRUE];
            
            //Clear all notification states
            [INUserDefaultOperations setAppNotLaunchedNotificationState:FALSE];
            [INUserDefaultOperations setCustomerSignInNotificationState:FALSE];
            [INUserDefaultOperations setCustomerSpecialDatesNotificationState:FALSE];
            [INUserDefaultOperations setCustomerFavouriteNotificationState:FALSE];
            [INUserDefaultOperations setEditStoreNotificationState:FALSE];
            [INUserDefaultOperations setTalkNowNotificationState:FALSE];
        }
    }
    
    int count = [self getRowsCountOfAreaTable];
    DebugLog(@"RowsCountOfAreaTable %d",count);
    if (count <= 0) {
        if(!networkavailable) {
            [INUserDefaultOperations showOfflineAlert];
        }
    }
    
    custleftSideMenuController = [[SideMenuViewController alloc] init];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendAreaRequest) name:NSCurrentLocaleDidChangeNotification  object:nil];
 
//    activePlaceid           = [self getActiveShoplocalPlaceIdFromAreaTable];
//    DebugLog(@"activePlaceid -%@-",activePlaceid);
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:[NSString stringWithFormat:@"%@",activePlaceid] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [alert show];
    
//    splashviewController    = [[NMSplashViewController alloc] initWithNibName:@"NMSplashViewController" bundle:nil];
//    splashviewController.place_id   = activePlaceid;
//    self.window.rootViewController  = splashviewController;
   [self animationStop];
    
    [self setUpNavigationBarAppeance];

    //Urban Airship Initialization
     UAConfig *config = [UAConfig defaultConfig];
    config.automaticSetupEnabled = NO;
    //config.developmentLogLevel = UALogLevelDebug;
    //config.productionLogLevel = UALogLevelDebug;
    [UAirship takeOff:config];
    [UAirship setLogging:FALSE];
    [UAPush shared].notificationTypes = (UIRemoteNotificationTypeBadge |
                                         UIRemoteNotificationTypeSound |
                                         UIRemoteNotificationTypeAlert);
    
    [[UAPush shared] registerForRemoteNotifications];
    
    [self.window makeKeyAndVisible];
    
    DebugLog(@"launchOptions %@",launchOptions);
    if (launchOptions != (NSDictionary*) [NSNull null] && launchOptions.count > 0) {
        if ([launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey] != nil) {
            UILocalNotification *localNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
            if (localNotification) {
                // run after a delay so that the cordova/kendo app has time to get ready
                // [INUserDefaultOperations showAlert:[NSString stringWithFormat:@"%@",localNotification]];
                [self performSelector:@selector(handleLocalNotification:) withObject:localNotification afterDelay:2.0];
            }
        }
    }
    
    // Whenever a person opens the app, check for a cached session
    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
        DebugLog(@"Found a cached session");
        // If there's one, just open the session silently, without showing the user the login UI
        [self openSessionWithAllowLoginUI:NO];
    }
    return YES;
}

-(void)setUpNavigationBarAppeance{
    [[UINavigationBar appearance] setBackgroundImage:[self blueBarBackground] forBarMetrics:UIBarMetricsDefault];
    
    NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIColor whiteColor],UITextAttributeTextColor,
                                               //                                               [UIColor blackColor], UITextAttributeTextShadowColor,
                                               //                                               [NSValue valueWithUIOffset:UIOffsetMake(-1, 0)], UITextAttributeTextShadowOffset,
                                               DEFAULT_FONT(15),UITextAttributeFont,
                                               nil];
    
    [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
    
    // Disable the custom font when the NavigationBar is presented in a MFMailComposeViewController
    [[UINavigationBar appearanceWhenContainedIn:[MFMailComposeViewController class], nil] setTitleTextAttributes:
     @{UITextAttributeTextColor : [UIColor whiteColor],
       UITextAttributeFont : [UIFont systemFontOfSize:18.0f]}];
    
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque];
        [[UINavigationBar appearance] setTintColor:DEFAULT_COLOR];
    }else{
        //self.window.tintColor = DEFAULT_COLOR;
        [[UINavigationBar appearance] setBarStyle:UIBarStyleBlackOpaque];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
        [[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
        
        //[[UINavigationBar appearance] setBarTintColor:DEFAULT_COLOR];
        
        //set back button color
        [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    }
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    // Updates the device token and registers the token with UA. This won't occur until
    // push is enabled if the outlined process is followed. This call is required.

    [[UAPush shared] registerDeviceToken:deviceToken];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    DebugLog(@"Received remote notification: %@", userInfo);
    
    /* {
     Type = Customer;
     "_" = ugMGAIKiEeOs2wAbIc5DvA;
     aps =     {
     alert = "Test Push with payload";
     badge = 5;
     }; */
    
    // Fire the handlers for both regular and rich push
    [[UAPush shared] handleNotification:userInfo applicationState:application.applicationState];    
    //[UAInboxPushHandler handleNotification:userInfo];
//    if([[userInfo objectForKey:@"Type"] isEqualToString:@"Customer"] || [[userInfo objectForKey:@"Type"] isEqualToString:@"Merchant"]) {
//        [self switchViewController];
//    }
}

-(void)animationStop
{
    //[splashviewController.view removeFromSuperview];
    //splashviewController = nil;
    activeAreaId           = [self getActiveAreaIdFromAreaTable];
    DebugLog(@"activeAreaId -%@-",activeAreaId);
    
    [UIView  transitionWithView:self.window duration:1.0  options:UIViewAnimationOptionTransitionCrossDissolve
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         [self setupNavigationControllerApp];
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:nil];
}



-(void)sendDailyEvents
{
    //DebugLog(@"diff - >%d",[INUserDefaultOperations getDateDifferenceInHours:[INUserDefaultOperations getDailySyncDate]]);
    
    if(([INUserDefaultOperations getDateDifferenceInHours:[INUserDefaultOperations getDailySyncDate]]) >= 24)
    {
        [INEventLogger logEvent:@"App_DailyEngagedUsers"];
        [INUserDefaultOperations setDailySyncDate];
    }
}

-(void)sendMonthlyEvents
{
    //DebugLog(@"diff - >%d",[INUserDefaultOperations getDateDifferenceInDays:[INUserDefaultOperations getMonthlySyncDate]]);
    
    if(([INUserDefaultOperations getDateDifferenceInHours:[INUserDefaultOperations getMonthlySyncDate]]) >= 30)
    {
        [INEventLogger logEvent:@"App_MonthlyEngagedUsers"];
        [INUserDefaultOperations setMonthlySyncDate];
        
    }
}

- (UIImage *)blueBarBackground
{
    /* Create a DeviceRGB color space. */
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    /* Create a bitmap context. The context draws into a bitmap which is `width'
    pixels wide and `height' pixels high*/
    CGContextRef composedImageContext = CGBitmapContextCreate(NULL,
                                                              10,
                                                              10,
                                                              8,
                                                              10*4,
                                                              colorSpace,
                                                              kCGImageAlphaPremultipliedFirst);
    
    CGColorSpaceRelease(colorSpace);
    
    
    CGContextSetFillColorWithColor(composedImageContext, [DEFAULT_COLOR CGColor]);
    CGContextFillRect(composedImageContext, CGRectMake(0, 0, 10, 10));
    /* Return an image containing a snapshot of the bitmap context `context'.*/
    CGImageRef cgImage = CGBitmapContextCreateImage(composedImageContext);
    
    return [UIImage imageWithCGImage:cgImage];
}



-(void)sendAreaRequest
{
    if (!networkavailable || isAreaRequestIsRunning) {
        return;
    }
    [CommonCallback showProgressHud:@"Refetching your nearest areas" subtitle:@"Please wait"];
    isAreaRequestIsRunning = YES;
    NSLocale *locale = [NSLocale currentLocale];
    NSString *countryCode = [locale objectForKey: NSLocaleCountryCode];
    DebugLog(@"countryCode:%@", countryCode);
    countryCode = @"IN";   //This is done temporarily for App Store approval
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:AREAS_LINK([countryCode lowercaseString])]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    DebugLog(@"%@",urlRequest.URL);
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        isAreaRequestIsRunning = NO;
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"%@", self.splashJson);
                                                        if(self.splashJson  != nil) {
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                if (areaArray == nil) {
                                                                    areaArray = [[NSMutableArray alloc] init];
                                                                }else{
                                                                    [areaArray removeAllObjects];
                                                                }
                                                                NSMutableDictionary *merchantCountDict = [[NSMutableDictionary alloc] init];
                                                                NSDictionary *maindict = [self.splashJson  objectForKey:@"data"];
                                                                NSArray* jsonArray = [maindict  objectForKey:@"areas"];
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *objdict = [jsonArray  objectAtIndex:index];
                                                                    //DebugLog(@"\n\n%@\n\n",objdict);
                                                                    areaObj = [[INAreaObject alloc] init];
                                                                    
                                                                    areaObj.city = [objdict objectForKey:@"city"];
                                                                    areaObj.country = [objdict objectForKey:@"country"];
                                                                    areaObj.countrycode = [objdict objectForKey:@"country_code"];
                                                                    areaObj.areaid = [objdict objectForKey:@"id"];
                                                                    areaObj.iso_code = [objdict objectForKey:@"iso_code"];
                                                                    areaObj.latitude = [objdict objectForKey:@"latitude"];
                                                                    areaObj.longitude = [objdict objectForKey:@"longitude"];
                                                                    areaObj.locality = [objdict objectForKey:@"locality"];
                                                                    areaObj.pincode = [objdict objectForKey:@"pincode"];
                                                                    areaObj.published_status = [objdict objectForKey:@"published_status"];
                                                                    areaObj.shoplocalplaceid = [objdict objectForKey:@"shoplocal_place_id"];
                                                                    areaObj.state = [objdict objectForKey:@"state"];
                                                                    areaObj.sublocality = [objdict objectForKey:@"sublocality"];
                                                                    areaObj.merchant_count = [objdict objectForKey:@"merchant_count"];
                                                                    areaObj.slug = [objdict objectForKey:@"slug"];
//                                                                    if ([activePlaceid isEqualToString:areaObj.shoplocalplaceid]) {
//                                                                        areaObj.is_active = 1;
//                                                                    }else{
//                                                                        areaObj.is_active = 0;
//                                                                    }
                                                                    [areaArray addObject:areaObj];
                                                                    
                                                                    if ([objdict objectForKey:@"merchant_count"] != (NSString *)[NSNull null]) {
                                                                        [merchantCountDict setObject:[objdict objectForKey:@"merchant_count"] forKey:areaObj.sublocality];
                                                                    }
                                                                    areaObj = nil;
                                                                }
                                                                DebugLog(@"areaArray %d",areaArray.count);
                                                                if (areaArray.count  > 0) {
                                                                    NSString *tempAreaId = [self getActiveAreaIdFromAreaTable];
                                                                    [self deleteAreasTable];
                                                                    [self insertAreaTable:areaArray];
                                                                    [areaArray removeAllObjects];
                                                                    areaArray = nil;
                                                                    
                                                                    [self updateAreasTable:tempAreaId is_active:1];
                                                                    DebugLog(@"MerchantCount %@",merchantCountDict);
                                                                    [INUserDefaultOperations setMerchantCountInArea:merchantCountDict];
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:AREA_UPDATE_NOTIFICATION object:nil userInfo:nil];
                                                                }
                                                                [CommonCallback hideProgressHud];
                                                            } else {
                                                                [CommonCallback hideProgressHud];
                                                                [INUserDefaultOperations performSelector:@selector(showAlert:) withObject:[self.splashJson  objectForKey:@"message"] afterDelay:2.0];
                                                            }
                                                        }   
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        [CommonCallback hideProgressHud];
                                                        isAreaRequestIsRunning = NO;
                                                        [INUserDefaultOperations performSelector:@selector(showAlert:) withObject:[NSString stringWithFormat:@"%@",[error localizedDescription]] afterDelay:2.0];
                                                    }];
    
    [operation start];
}

-(void)registerUniqueDeviceId
{
    //[CommonCallback showProgressHud:@"Processing" subtitle:HUD_SUBTITLE];
    
    
    if (networkavailable) {
    
        NSUUID *deviceID = [[UIDevice currentDevice] identifierForVendor];
        DebugLog(@"id---->%@", [deviceID UUIDString]);
        
        AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
        [httpClient setParameterEncoding:AFJSONParameterEncoding];
        [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
        if ([[deviceID UUIDString] length] > 0) {
            [httpClient setDefaultHeader:@"device_id" value:[deviceID UUIDString]];
        }
        
        NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
        [postParams setObject:[[UIDevice currentDevice] model] forKey:@"os_type"];
        [postParams setObject:[[UIDevice currentDevice] systemVersion] forKey:@"os_version"];
        [postParams setObject:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] forKey:@"app_version"];
        
        DebugLog(@"postParams %@",postParams);
        
        [httpClient postPath:DEVICE_ID_URL parameters:postParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
            //[hud hide:YES];
            //[CommonCallback hideProgressHud];
            NSError* error;
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
            DebugLog(@"User id : json ->%@",json);
            if(json != nil) {
                if([[json  objectForKey:@"success"] isEqualToString:@"true"])
                {
                    
                    NSDictionary* data = [json objectForKey:@"data"];
                    NSString * resultid = [data objectForKey:@"id"];
                    NSString * resultcode = [data objectForKey:@"auth_code"];
                    DebugLog(@"%@ %@",resultid,resultcode);
                    
                }
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
            //[hud hide:YES];
            //[CommonCallback hideProgressHud];
        }];
        
    }
}

#pragma Reachability Methods
-(void) startCheckNetwork
{
    
	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
	
    internetReach = [Reachability reachabilityForInternetConnection];
	[internetReach startNotifer];
    [self updateReachabitlityFlag:internetReach];
    
}

-(void) stopCheckNetwork
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
	internetReach = nil;
}

- (void)reachabilityChanged:(NSNotification *)note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
	[self updateReachabilityStatus: curReach];
}

- (void) updateReachabilityStatus: (Reachability*) curReach
{
	if(curReach == internetReach)
	{
		[self updateReachabitlityFlag: curReach];
    }
}

- (void) updateReachabitlityFlag: (Reachability*) curReach
{
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    BOOL connectionRequired=[curReach connectionRequired];
    
    switch (netStatus)
    {
        case NotReachable:
        {
            networkavailable =  NO;
            break;
        }
            
        case ReachableViaWWAN:
        {
			if(connectionRequired==NO)
			{
                networkavailable = YES;
			}
			else
			{
				//this is for invoking internet library
				NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:@"maps.google.com"] cachePolicy:NO timeoutInterval:15.0] ;
				
				NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
				if (theConnection) {
					connectionRequired	= NO;
				}
			}
			break;
        }
        case ReachableViaWiFi:
        {
            if(connectionRequired==NO)
            {
                networkavailable = YES;
            }
            break;
		}
    }
    if(connectionRequired)
    {
		networkavailable = NO;
    }
    DebugLog(@"network status = %d",networkavailable);
    [self performSelector:@selector(sendNetworkNotification) withObject:nil afterDelay:2.0];
    //[self sendNetworkNotification];
}

#pragma Wifi Notification CallBack
-(void)sendNetworkNotification
{
    if (networkavailable) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NETWORK_NOTIFICATION object:nil];
//        int count = [self getRowsCountOfAreaTable];
//        DebugLog(@"RowsCountOfAreaTable %d",count);
        NSDate *expireDate = [INUserDefaultOperations getAreaListGetRequestExipirationDate];
        DebugLog(@"/////////////////////////////////////////////\n---expireDate %@---/////////////////////////////////////////////\n",expireDate);
        if (expireDate != nil) {
            NSDate *currentDate = [NSDate date];

            NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
                                                                fromDate:expireDate
                                                                  toDate:currentDate
                                                                 options:0];
            DebugLog(@"days = %d",[components day]);
            DebugLog(@"-----expireDate %@ --currentDate %@--",expireDate,currentDate);

            if([components day] != 0) // Compare expiration dates. if greater than one then users session timeout occurs.
            {
                 [self sendAreaRequest]; // expireDate != current date
            }
        }else{
            [self sendAreaRequest];  // expireDate == nil
        }
    }
}

-(NSString*)urlEncode:(NSString*) string
{
    ////DebugLog(@"==BEFORE ENCODE=%@",string);
    NSMutableString *escaped = [[string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] mutableCopy];
    [escaped replaceOccurrencesOfString:@"=" withString:@"%3D" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escaped length])];
    [escaped replaceOccurrencesOfString:@" " withString:@"%20" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escaped length])];
    [escaped replaceOccurrencesOfString:@"\"" withString:@"%22" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escaped length])];
    [escaped replaceOccurrencesOfString:@"\n" withString:@"%0A" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escaped length])];
    ////DebugLog(@"==AFTER ENCODE=%@",escaped);
    return escaped;
}

#pragma File handling callbacks
//Method writes a NSDictionary to a text file
-(void) writeToTextFile:(NSDictionary *)lcontent name:(NSString *)lname
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fileName = [NSString stringWithFormat:@"%@.txt",lname];
    //DebugLog(@"filename = %@" , fileName);
    NSString* fullPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    // DebugLog(@"fullpath = %@" , fullPath);
    
    if ([fileManager fileExistsAtPath:fullPath])
    {
        [self removeFile:fileName];
        [lcontent writeToFile:fullPath atomically:YES];
    } else {
        [lcontent writeToFile:fullPath atomically:YES];
    }
}

-(NSDictionary *) getTextFromFile:(NSString *)lname
{
	NSFileManager *fileManager = [NSFileManager defaultManager];
    //get the documents directory:
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	
	//make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@.txt",lname];
    //DebugLog(@"filename = %@" , fileName);
    NSString* fullPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    DebugLog(@"fullpath = %@" , fullPath);
    if ([fileManager fileExistsAtPath:fullPath])
    {
        NSDictionary *data = [NSDictionary dictionaryWithContentsOfFile:fullPath];
        return data;
    } else {
        return nil;
    }
}

-(BOOL)checkFileExits:(NSString *)lname
{
	NSFileManager *fileManager = [NSFileManager defaultManager];
    //get the documents directory:
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	
	//make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@.txt",lname];
    //DebugLog(@"filename = %@" , fileName);
    NSString* fullPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    DebugLog(@"fullpath = %@" , fullPath);
    if ([fileManager fileExistsAtPath:fullPath])
    {
        return TRUE;
    } else {
        return FALSE;
    }
}

- (void)removeFile:(NSString*)lname {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:lname];
    BOOL success = [fileManager removeItemAtPath: fullPath error:NULL];
    if(success == YES)
        DebugLog(@" %@ removed",lname);
    else
        DebugLog(@" %@ NOT removed",lname);
}


-(void) checkAndCreateDatabase {
    
    NSString *docsDir;
    NSArray *dirPaths;
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"shoplocal.db"]];
    //DebugLog(@" new path = %@",databasePath);
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        
        [INEventLogger logEvent:@"App_Installs"];
        
        const char *dbpath = [databasePath UTF8String];
        
        if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
        {
            DebugLog(@"Database opened successfully");
            char *errMsg;
            const char *sql_stmt_area = "CREATE TABLE IF NOT EXISTS AREA (ID TEXT, SUBLOCALITY TEXT, LOCALITY TEXT, LATITUDE TEXT, LONGITUDE TEXT, PINCODE TEXT, PUBLISHED TEXT, CITY TEXT, STATE TEXT, COUNTRY TEXT, COUNTRY_CODE TEXT,  ISO_CODE TEXT, SHOPLOCAL_PLACE_ID TEXT, IS_ACTIVE INTEGER, MERCHANT_COUNT TEXT, SLUG TEXT)";
            
            if (sqlite3_exec(shoplocalDB, sql_stmt_area, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                DebugLog(@"Failed to create table");
            }
            
            const char *sql_stmt_myplace = "CREATE TABLE IF NOT EXISTS MYPLACES (ID INTEGER PRIMARY KEY AUTOINCREMENT, PLACEID INTEGER not null unique, PLACE_PARENT INTEGER, NAME TEXT, DESCRIPTION TEXT, BUILDING TEXT, STREET TEXT, LANDMARK TEXT, AREA TEXT, CITY TEXT, MOB_NO1 TEXT, MOB_NO2 TEXT, MOB_NO3 TEXT, TEL_NO1 TEXT, TEL_NO2 TEXT, TEL_NO3 TEXT, IMG_URL TEXT, EMAIL TEXT, WEBSITE TEXT, TOTAL_LIKE TEXT, TOTAL_SHARE TEXT, TOTAL_VIEW TEXT, PLACE_STATUS TEXT, PUBLISHED TEXT, COUNTRY TEXT, STATE TEXT, PINCODE TEXT, FB_URL TEXT, TWITTER_URL TEXT, LATITUDE TEXT, LONGITUDE TEXT )";
            
            if (sqlite3_exec(shoplocalDB, sql_stmt_myplace, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                DebugLog(@"Failed to create table");
            }
            
            const char *sql_stmt_feed = "CREATE TABLE IF NOT EXISTS FEEDS (ID INTEGER PRIMARY KEY AUTOINCREMENT, POSTID INTEGER not null unique, PLACEID INTEGER, AREAID TEXT, TITLE TEXT, DESCRIPTION TEXT, DATE TEXT, OFFERDATE TEXT, DISTANCE TEXT, MOB_NO1 TEXT, MOB_NO2 TEXT, MOB_NO3 TEXT, TEL_NO1 TEXT, TEL_NO2 TEXT, TEL_NO3 TEXT, IMG_URL1 TEXT, IMG_URL2 TEXT, IMG_URL3 TEXT, THUMB_URL1 TEXT, THUMB_URL2 TEXT, THUMB_URL3 TEXT, IS_OFFERED TEXT, TOTAL_LIKE TEXT, TOTAL_SHARE TEXT, LATITUDE TEXT, LONGITUDE TEXT, TYPE TEXT , NAME TEXT)";
            
            if (sqlite3_exec(shoplocalDB, sql_stmt_feed, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                DebugLog(@"Failed to create table");
            }
            
            const char *sql_stmt_place = "CREATE TABLE IF NOT EXISTS PLACES (ID INTEGER PRIMARY KEY AUTOINCREMENT, PLACEID INTEGER not null unique, PLACE_PARENT INTEGER, NAME TEXT, DESCRIPTION TEXT, DISTANCE TEXT, BUILDING TEXT, STREET TEXT, LANDMARK TEXT, AREA TEXT, CITY TEXT, MOB_NO1 TEXT, MOB_NO2 TEXT, MOB_NO3 TEXT, TEL_NO1 TEXT, TEL_NO2 TEXT, TEL_NO3 TEXT, IMG_URL TEXT, EMAIL TEXT, WEBSITE TEXT, HAS_OFFER TEXT, TITLE TEXT, TOTAL_LIKE TEXT, AREAID TEXT, VERIFIED TEXT)";
            
            if (sqlite3_exec(shoplocalDB, sql_stmt_place, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                DebugLog(@"Failed to create table");
            }
            
            const char *sql_stmt_fav = "CREATE TABLE IF NOT EXISTS FAVOURITES (ID INTEGER PRIMARY KEY AUTOINCREMENT, PLACEID INTEGER not null unique, PLACE_PARENT INTEGER, NAME TEXT, DESCRIPTION TEXT, BUILDING TEXT, STREET TEXT, LANDMARK TEXT, AREA TEXT, CITY TEXT, MOB_NO1 TEXT, MOB_NO2 TEXT, MOB_NO3 TEXT, TEL_NO1 TEXT, TEL_NO2 TEXT, TEL_NO3 TEXT, IMG_URL TEXT, EMAIL TEXT, WEBSITE TEXT, HAS_OFFER TEXT, TITLE TEXT, TOTAL_LIKE TEXT, AREAID TEXT, VERIFIED TEXT)";
            
            if (sqlite3_exec(shoplocalDB, sql_stmt_fav, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                DebugLog(@"Failed to create table");
            }
            
            const char *sql_stmt_place_record = "CREATE TABLE IF NOT EXISTS PLACE_RECORD (STORE TEXT unique, TOTAL_PAGE INTEGER, TOTAL_RECORD INTEGER, CURRENT_PAGE INTEGER)";
            
            if (sqlite3_exec(shoplocalDB, sql_stmt_place_record, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                DebugLog(@"Failed to create table");
            }

            const char *sql_stmt_fav_record = "CREATE TABLE IF NOT EXISTS FAV_RECORD (STORE TEXT, TOTAL_PAGE INTEGER, TOTAL_RECORD INTEGER, CURRENT_PAGE INTEGER)";
            
            if (sqlite3_exec(shoplocalDB, sql_stmt_fav_record, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                DebugLog(@"Failed to create table");
            }

            NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO FAV_RECORD (STORE) VALUES (\"%@\")", @""];

            const char *insert_stmt = [insertSQL UTF8String];
            if (sqlite3_exec(shoplocalDB, insert_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
            }

            const char *sql_stmt_feeds_record = "CREATE TABLE IF NOT EXISTS FEEDS_RECORD (STORE TEXT unique, TOTAL_PAGE INTEGER, TOTAL_RECORD INTEGER, CURRENT_PAGE INTEGER)";
            
            if (sqlite3_exec(shoplocalDB, sql_stmt_feeds_record, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                DebugLog(@"Failed to create table");
            }

            [INUserDefaultOperations setStoreIds];
            [INUserDefaultOperations setSelectedStore:@"Lokhandwala, Andheri West"];
            [self setDatabaseSchemaVersion:DATABASE_VERSION];
            int oldVersion = [self checkdatabaseversion:databasePath];
            DebugLog(@"old version %d",oldVersion);
        } else {
            DebugLog(@"Failed to open/create database");
        }
        sqlite3_close(shoplocalDB);
    } else {
        DebugLog(@" old path = %@",databasePath);
        int oldVersion = [self checkdatabaseversion:databasePath];
        DebugLog(@"old version %d",oldVersion);
        DebugLog(@"DATABASE_VERSION  %d",DATABASE_VERSION);
        
        if (oldVersion < DATABASE_VERSION) {
            DebugLog(@"old version != DATABASE_VERSION");
//            [filemgr removeItemAtPath:databasePath error:NULL];
//            [self checkAndCreateDatabase];
            BOOL success = [self alterTable];
            if (success) {
                [self setDatabaseSchemaVersion:DATABASE_VERSION];
                [self sendAreaRequest];
            }
        }else{
            DebugLog(@"old version == DATABASE_VERSION");
        }
    }
}

-(int)checkdatabaseversion:(NSString *)checkdbpath
{
    sqlite3 *micraDB;
    static sqlite3_stmt *stmt_version;
    int databaseVersion = 0;
    
    const char *dbpath = [checkdbpath UTF8String];
    
    if (sqlite3_open(dbpath, &micraDB) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(micraDB, "PRAGMA user_version;", -1, &stmt_version, NULL) == SQLITE_OK) {
            while(sqlite3_step(stmt_version) == SQLITE_ROW) {
                databaseVersion = sqlite3_column_int(stmt_version, 0);
                DebugLog(@"%s: version %d", __FUNCTION__, databaseVersion);
            }
            DebugLog(@"%s: the databaseVersion is: %d", __FUNCTION__, databaseVersion);
        } else {
            DebugLog(@"%s: ERROR Preparing: , %s", __FUNCTION__, sqlite3_errmsg(micraDB) );
        }
        sqlite3_finalize(stmt_version);
    }
    sqlite3_close(micraDB);
    
    return databaseVersion;
}

- (void)setDatabaseSchemaVersion:(int)version {
    sqlite3 *micraDB;
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &micraDB) == SQLITE_OK)
    {
        sqlite3_exec(micraDB, [[NSString stringWithFormat:@"PRAGMA user_version = %d", version] UTF8String], NULL, NULL, NULL);
    }
}

-(BOOL)alterTable
{
    BOOL success = YES;
    
    if (sqlite3_open([databasePath UTF8String], &shoplocalDB) == SQLITE_OK) {
        NSString *query = @"ALTER TABLE AREA ADD COLUMN MERCHANT_COUNT TEXT";
        char *errMsg;
        if (sqlite3_exec(shoplocalDB, [query UTF8String], NULL, NULL, &errMsg) == SQLITE_OK)
        {
            DebugLog(@"ADDED COLUMN MERCHANT_COUNT");
        }else{
            DebugLog(@"ADD COLUMN MERCHANT_COUNT %s", sqlite3_errmsg(shoplocalDB));
            success = NO;
        }
        
        query = @"ALTER TABLE AREA ADD COLUMN SLUG TEXT";
        if (sqlite3_exec(shoplocalDB, [query UTF8String], NULL, NULL, &errMsg) == SQLITE_OK)
        {
            DebugLog(@"ADDED COLUMN SLUG");
        }else{
            DebugLog(@"ADD COLUMN SLUG %s", sqlite3_errmsg(shoplocalDB));
            success = NO;
        }
    }
    sqlite3_close(shoplocalDB);
    return success;
}

- (void)insertAreaTable:(NSArray *)lareaArray
{
    sqlite3_stmt    *statement;
    int lastrowid;
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        for (int index=0; index<[lareaArray count] ; index++)
        {
            INAreaObject *lareaObj = [lareaArray objectAtIndex:index];
//            NSString *insertSQL = [NSString stringWithFormat:
//                                   @"INSERT INTO AREA (CITY , COUNTRY , COUNTRY_CODE , ID , ISO_CODE , LATITUDE , LONGITUDE , LOCALITY , PINCODE , PUBLISHED , SHOPLOCAL_PLACE_ID , STATE , SUBLOCALITY,IS_ACTIVE) VALUES (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",%d)",lareaObj.city,lareaObj.country,lareaObj.countrycode,lareaObj.areaid,lareaObj.iso_code,lareaObj.latitude,lareaObj.longitude,lareaObj.locality,lareaObj.pincode,lareaObj.published_status,lareaObj.shoplocalplaceid,lareaObj.state,lareaObj.sublocality,lareaObj.is_active];
            NSString *insertSQL = [NSString stringWithFormat:
                                   @"INSERT INTO AREA (CITY , COUNTRY , COUNTRY_CODE , ID , ISO_CODE , LATITUDE , LONGITUDE , LOCALITY , PINCODE , PUBLISHED , SHOPLOCAL_PLACE_ID , STATE , SUBLOCALITY,IS_ACTIVE,MERCHANT_COUNT,SLUG) VALUES (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",%d,\"%@\",\"%@\")",lareaObj.city,lareaObj.country,lareaObj.countrycode,lareaObj.areaid,lareaObj.iso_code,lareaObj.latitude,lareaObj.longitude,lareaObj.locality,lareaObj.pincode,lareaObj.published_status,lareaObj.shoplocalplaceid,lareaObj.state,lareaObj.sublocality,lareaObj.is_active,lareaObj.merchant_count,lareaObj.slug];
            DebugLog(@"%@",insertSQL);
            const char *insert_stmt = [insertSQL UTF8String];
            sqlite3_prepare_v2(shoplocalDB, insert_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                lastrowid = sqlite3_last_insert_rowid(shoplocalDB);
                // DebugLog(@"inserted id========%d", lastrowid);
            } else {
                DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
            }
            sqlite3_finalize(statement);
            
            NSString *placeinsertSQL = [NSString stringWithFormat:@"INSERT OR IGNORE INTO PLACE_RECORD (STORE) VALUES (\"%@\")", lareaObj.areaid];
            DebugLog(@"%@",placeinsertSQL);
            const char *placeinsert_stmt = [placeinsertSQL UTF8String];
            sqlite3_prepare_v2(shoplocalDB, placeinsert_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                DebugLog(@"placeinsertSQL success");
            } else {
                DebugLog(@"placeinsertSQL %s", sqlite3_errmsg(shoplocalDB));
            }
            sqlite3_finalize(statement);
            
//            NSString *favinsertSQL = [NSString stringWithFormat:@"INSERT INTO FAV_RECORD (STORE) VALUES (\"%@\")", lareaObj.areaid];
//            DebugLog(@"%@",favinsertSQL);
//            const char *favinsert_stmt = [favinsertSQL UTF8String];
//            sqlite3_prepare_v2(shoplocalDB, favinsert_stmt, -1, &statement, NULL);
//            if (sqlite3_step(statement) == SQLITE_DONE)
//            {
//                DebugLog(@"favinsertSQL success");
//            } else {
//                DebugLog(@"favinsertSQL %s", sqlite3_errmsg(shoplocalDB));
//            }
//            sqlite3_finalize(statement);
            
            NSString *feedinsertSQL = [NSString stringWithFormat:@"INSERT OR IGNORE INTO FEEDS_RECORD (STORE) VALUES (\"%@\")", lareaObj.areaid];
            DebugLog(@"%@",feedinsertSQL);
            const char *feedinsert_stmt = [feedinsertSQL UTF8String];
            sqlite3_prepare_v2(shoplocalDB, feedinsert_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                DebugLog(@"feedinsertSQL success");
            } else {
                DebugLog(@"feedinsertSQL %s", sqlite3_errmsg(shoplocalDB));
            }
            sqlite3_finalize(statement);
        }
        
    }
    sqlite3_close(shoplocalDB);
    
    [INUserDefaultOperations setAreaListGetRequestExipirationDate:[NSDate date]];
}

-(void)deleteAreasTable
{
    sqlite3_stmt    *statement;
    if (sqlite3_open([databasePath UTF8String], &shoplocalDB) == SQLITE_OK)
    {
        NSString *deleteSQL = @"DELETE FROM AREA";
        DebugLog(@"delete---->%@",deleteSQL);
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(shoplocalDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"All rows deleted from AREA table");
        } else {
            DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
}

-(int)getRowsCountOfAreaTable{
    sqlite3_stmt *statement;
    int rowcount = 0;
    if (sqlite3_open([databasePath UTF8String], &shoplocalDB) == SQLITE_OK)
    {
        NSString *countSQL = [NSString stringWithFormat:@"SELECT count(*) FROM AREA"];
        DebugLog(@"RowsCount---->%@",countSQL);
        const char *count_stmt = [countSQL UTF8String];
        if (sqlite3_prepare_v2(shoplocalDB, count_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                rowcount =  sqlite3_column_int(statement, 0);
                DebugLog(@"%d", rowcount);
            } else {
                DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
            }
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
    return rowcount;
}

-(void)updateAreasTableFromStateActiveToNonActive{
    sqlite3_stmt    *statement;
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        NSString *updateSQL = [NSString stringWithFormat:@"UPDATE AREA set IS_ACTIVE=0 WHERE IS_ACTIVE = 1"];
        DebugLog(@"%@",updateSQL);
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(shoplocalDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"updated updateAreasTableFromStateActiveToNonActive");
        } else {
            DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
}

-(void)updateAreasTable:(NSString *) area_id is_active:(int)activeValue{
    sqlite3_stmt    *statement;
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        BOOL success = NO;
        NSString *updateSQL = [NSString stringWithFormat:@"UPDATE AREA set IS_ACTIVE=0 WHERE IS_ACTIVE = 1"];
        DebugLog(@"%@",updateSQL);
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(shoplocalDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            success = YES;
            DebugLog(@"updated updateAreasTableFromStateActiveToNonActive");
        } else {
            DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
        }
        sqlite3_finalize(statement);
        
        if (success) {
            NSString *updateSQL1 = [NSString stringWithFormat:@"UPDATE AREA set IS_ACTIVE=\"%d\" WHERE ID=\"%@\"",activeValue,area_id];
            DebugLog(@"%@",updateSQL1);
            const char *update_stmt1 = [updateSQL1 UTF8String];
            sqlite3_prepare_v2(shoplocalDB, update_stmt1, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                DebugLog(@"updated area_id========%@", area_id);
            } else {
                DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
            }
            sqlite3_finalize(statement);
        }
    }
    sqlite3_close(shoplocalDB);
}
-(void)updateAreasTableByName:(NSString *) sublocality is_active:(int)activeValue{
    sqlite3_stmt    *statement;
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        BOOL success = NO;
        NSString *updateSQL = [NSString stringWithFormat:@"UPDATE AREA set IS_ACTIVE=0 WHERE IS_ACTIVE = 1"];
        DebugLog(@"%@",updateSQL);
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(shoplocalDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            success = YES;
            DebugLog(@"1) updated updateAreasTableFromStateActiveToNonActive");
        } else {
            DebugLog(@"1) sqlite3_errmsg %s", sqlite3_errmsg(shoplocalDB));
        }
        sqlite3_finalize(statement);
        
        if (success) {
            NSString *updateSQL1 = [NSString stringWithFormat:@"UPDATE AREA set IS_ACTIVE=\"%d\" WHERE SUBLOCALITY=\"%@\"",activeValue,sublocality];
            DebugLog(@"%@",updateSQL1);
            const char *update_stmt1 = [updateSQL1 UTF8String];
            sqlite3_prepare_v2(shoplocalDB, update_stmt1, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                DebugLog(@"2) updated area_id========%@", sublocality);
            } else {
                DebugLog(@"2) sqlite3_errmsg%s", sqlite3_errmsg(shoplocalDB));
            }
            sqlite3_finalize(statement);
        }
    }
    sqlite3_close(shoplocalDB);
}

-(NSString *)getActiveAreaIdFromAreaTable
{
    NSString *areaId = @"";
	if(sqlite3_open([databasePath UTF8String], &shoplocalDB) == SQLITE_OK)
    {
        // Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select ID from AREA WHERE IS_ACTIVE = 1"];
        const char *sqlStatement = [querySQL UTF8String];
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(shoplocalDB, sqlStatement, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                areaId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                DebugLog(@"areaId -> %@",areaId);
            }
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
    return areaId;
}

-(NSString *)getActiveShoplocalPlaceIdFromAreaTable
{
    NSString *placeId = @"";
	if(sqlite3_open([databasePath UTF8String], &shoplocalDB) == SQLITE_OK)
    {
        // Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select SHOPLOCAL_PLACE_ID from AREA WHERE IS_ACTIVE = 1"];
        const char *sqlStatement = [querySQL UTF8String];
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(shoplocalDB, sqlStatement, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                placeId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                DebugLog(@"placeId -> %@",placeId);
            }
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
    return placeId;
}

-(NSString *)getActivePlaceNameFromAreaTable
{
    NSString *placeName = @"";
	if(sqlite3_open([databasePath UTF8String], &shoplocalDB) == SQLITE_OK)
    {
        // Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select SUBLOCALITY from AREA WHERE IS_ACTIVE = 1"];
        const char *sqlStatement = [querySQL UTF8String];
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(shoplocalDB, sqlStatement, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                placeName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                DebugLog(@"placeName -> %@",placeName);
            }
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
    return placeName;
}

-(NSString *)getActiveSlugFromAreaTable
{
    NSString *slug = @"";
    if (sqlite3_open([databasePath UTF8String], &shoplocalDB) == SQLITE_OK) {
        NSString *query = @"SELECT SLUG FROM AREA WHERE IS_ACTIVE = 1";
        sqlite3_stmt *statement;
        if (sqlite3_prepare_v2(shoplocalDB, [query UTF8String], -1, &statement, NULL) == SQLITE_OK) {
            if (sqlite3_step(statement) == SQLITE_ROW) {
                slug = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
            }
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
    return slug;
}

-(NSString *)getLocalityFromAreaTable
{
    NSString *placeName = @"";
	if(sqlite3_open([databasePath UTF8String], &shoplocalDB) == SQLITE_OK)
    {
        // Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select LOCALITY from AREA WHERE IS_ACTIVE = 1"];
        const char *sqlStatement = [querySQL UTF8String];
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(shoplocalDB, sqlStatement, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                placeName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                DebugLog(@"placeName -> %@",placeName);
            }
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
    return placeName;
}


-(NSString *)getCityFromAreaTable
{
    NSString *placeName = @"";
	if(sqlite3_open([databasePath UTF8String], &shoplocalDB) == SQLITE_OK)
    {
        // Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select CITY from AREA WHERE IS_ACTIVE = 1"];
        const char *sqlStatement = [querySQL UTF8String];
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(shoplocalDB, sqlStatement, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                placeName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                DebugLog(@"placeName -> %@",placeName);
            }
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
    return placeName;
}

-(NSString *)getPincodeFromAreaTable
{
    NSString *placeName = @"";
	if(sqlite3_open([databasePath UTF8String], &shoplocalDB) == SQLITE_OK)
    {
        // Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select PINCODE from AREA WHERE IS_ACTIVE = 1"];
        const char *sqlStatement = [querySQL UTF8String];
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(shoplocalDB, sqlStatement, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                placeName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                DebugLog(@"placeName -> %@",placeName);
            }
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
    return placeName;
}

-(NSString *)getAreaIdFromAreaTable:(NSString *)sublocality
{
    NSString *areaId = @"";
	if(sqlite3_open([databasePath UTF8String], &shoplocalDB) == SQLITE_OK)
    {
        // Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select ID from AREA WHERE SUBLOCALITY = '%@'", sublocality];
        const char *sqlStatement = [querySQL UTF8String];
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(shoplocalDB, sqlStatement, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                areaId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                DebugLog(@"areaId -> %@",areaId);
            }
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
    return areaId;
}

-(NSString *)getCityFromAreaTable:(NSString *)sublocality
{
    NSString *placeName = @"";
	if(sqlite3_open([databasePath UTF8String], &shoplocalDB) == SQLITE_OK)
    {
        // Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select CITY from AREA WHERE SUBLOCALITY = '%@'", sublocality];
        const char *sqlStatement = [querySQL UTF8String];
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(shoplocalDB, sqlStatement, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                placeName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                DebugLog(@"placeName -> %@",placeName);
            }
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
    return placeName;
}

-(NSString *)getPincodeFromAreaTable:(NSString *)sublocality
{
    NSString *placeName = @"";
	if(sqlite3_open([databasePath UTF8String], &shoplocalDB) == SQLITE_OK)
    {
        // Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select PINCODE from AREA WHERE SUBLOCALITY = '%@'", sublocality];
        const char *sqlStatement = [querySQL UTF8String];
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(shoplocalDB, sqlStatement, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                placeName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                DebugLog(@"placeName -> %@",placeName);
            }
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
    return placeName;
}

-(NSString *)getLatitudeFromAreaTable
{
    NSString *placeName = @"";
	if(sqlite3_open([databasePath UTF8String], &shoplocalDB) == SQLITE_OK)
    {
        // Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select LATITUDE from AREA WHERE IS_ACTIVE = 1"];
        const char *sqlStatement = [querySQL UTF8String];
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(shoplocalDB, sqlStatement, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                placeName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                DebugLog(@"placeName -> %@",placeName);
            }
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
    return placeName;
}

-(NSString *)getLongitudeFromAreaTable
{
    NSString *placeName = @"";
	if(sqlite3_open([databasePath UTF8String], &shoplocalDB) == SQLITE_OK)
    {
        // Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select LONGITUDE from AREA WHERE IS_ACTIVE = 1"];
        const char *sqlStatement = [querySQL UTF8String];
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(shoplocalDB, sqlStatement, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                placeName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                DebugLog(@"placeName -> %@",placeName);
            }
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
    return placeName;
}

- (NSString*)base64forData:(NSData*)theData {
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

-(NSString *)getPostDetailsMessageBody:(NSString *)storeName offerTitle:(NSString *)postTitle offerDescription:(NSString *)postDescription isOffered:(NSString *)postIsOffered offerDateTime:(NSString *)postOfferDateTime contact:(NSString *)contactString shareVia:(NSString *)shareVia{
    
    //    Template for Sharing Offer from Offer Stream:
    //    ---------------------------------------------
    //        Subject (only for Mail): Store Name - Offer
    //        Store Name - Offer
    //        <Offer Title>
    //        <Offer Description>
    //        Valid Till: <Offer Valid Till Date Time>
    //        Contact: +<Store Mobile Nos> <Store Landline nos>
    //        (Shared via: http://www.shoplocal.co.in )
    
    
    NSMutableString *message = [[NSMutableString alloc] initWithString:@""];
    
    if (![shareVia isEqualToString:@"facebook"]) {
        if (storeName != nil && ![storeName isEqualToString:@""]) {
            [message appendString:[NSString stringWithFormat:@"%@ - Offer\n",storeName]];
        }
    }
    if (postTitle != nil && ![postTitle isEqualToString:@""]) {
        [message appendString:[NSString stringWithFormat:@"%@\n",postTitle]];
    }
    
    if (postDescription != nil && [[postDescription stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
        [message appendString:[NSString stringWithFormat:@"%@\n",postDescription]];
    }
    if (postIsOffered != nil && ![postIsOffered isEqualToString:@"0"]) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        NSDate *myDate = [[NSDate alloc] init];
        myDate = [dateFormatter dateFromString:postOfferDateTime];
        DebugLog(@"MyDate is: %@", myDate);
        
        NSDateFormatter *dayformatter = [[NSDateFormatter alloc] init];
        [dayformatter setDateFormat:@"dd"];
        NSString *dayFromDate = [dayformatter stringFromDate:myDate];
        DebugLog(@"Myday is: %@", dayFromDate);
        
        NSDateFormatter *monthformatter = [[NSDateFormatter alloc] init];
        [monthformatter setDateFormat:@"MMM"];
        NSString *monthFromDate = [monthformatter stringFromDate:myDate];
        DebugLog(@"MyMonth is: %@", monthFromDate);
        
        NSDateFormatter *timeformatter = [[NSDateFormatter alloc] init];
        [timeformatter setDateFormat:@"hh:mm a"];
        NSString *timeFromDate = [timeformatter stringFromDate:myDate];
        DebugLog(@"timeFromDate is: %@", timeFromDate);
        
        NSMutableString *postOfferDateTimeString = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@ %@   %@",dayFromDate,monthFromDate,timeFromDate]];
        
        [message appendString:[NSString stringWithFormat:@"Valid Till: %@\n",postOfferDateTimeString]];
    }
    if (contactString != nil && ![contactString isEqualToString:@""]) {
        [message  appendString:[NSString stringWithFormat:@"Contact: %@\n",contactString]];
    }
    [message appendString:@"(Shared via: http://www.shoplocal.co.in)\n"];
    return message;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
//    if([INUserDefaultOperations isMerchantLoggedIn]) { //If merchant is loggedin
//        [INUserDefaultOperations setEditStoreNotification];
//        [INUserDefaultOperations setTalkNowNotification];
//        [INUserDefaultOperations setWeekendMerchantOffersNotification];
//    } else { //  Reminders for Customers
//        
//        if([INUserDefaultOperations isCustomerLoggedIn]) {
//            [INUserDefaultOperations setCustomerSpecialDatesNotification];
//            [INUserDefaultOperations setCustomerFavouriteNotification];
//        } else {
//            [INUserDefaultOperations setCustomerSignInNotification];
//        }
//        [INUserDefaultOperations setAppNotLaunchedNotification];
//        [INUserDefaultOperations setWeekendCustomerOffersNotification];
//    }
    DebugLog(@"isEditStoreNotificationAlreadySet %d",[INUserDefaultOperations isEditStoreNotificationAlreadySet]);
    DebugLog(@"isTalkNowNotificationAlreadySet %d",[INUserDefaultOperations isTalkNowNotificationAlreadySet]);
    
    DebugLog(@"isCustomerSpecialDatesNotificationAlreadySet %d",[INUserDefaultOperations isCustomerSpecialDatesNotificationAlreadySet]);
    DebugLog(@"isCustomerFavouriteNotificationAlreadySet %d",[INUserDefaultOperations isCustomerFavouriteNotificationAlreadySet]);
    
    DebugLog(@"isCustomerSignInNotificationAlreadySet %d",[INUserDefaultOperations isCustomerSignInNotificationAlreadySet]);
    
    DebugLog(@"isAppNotLaunchedNotificationAlreadySet %d",[INUserDefaultOperations isAppNotLaunchedNotificationAlreadySet]);

    if([INUserDefaultOperations isMerchantLoggedIn]) { //If merchant is loggedin
        if(![INUserDefaultOperations isEditStoreNotificationAlreadySet])
            [INUserDefaultOperations setEditStoreNotification];     //After 7 Days
        
        if(![INUserDefaultOperations isTalkNowNotificationAlreadySet])
            [INUserDefaultOperations setTalkNowNotification];       // After 3 Days from today
        
    } else { //  Reminders for Customers
        
        if([INUserDefaultOperations isCustomerLoggedIn]) {
            if(![INUserDefaultOperations isCustomerSpecialDatesNotificationAlreadySet])
                [INUserDefaultOperations setCustomerSpecialDatesNotification];  // After 7 days

            if(![INUserDefaultOperations isCustomerFavouriteNotificationAlreadySet])
                [INUserDefaultOperations setCustomerFavouriteNotification];     // After 3 days
            
            [INUserDefaultOperations cancelCustomerSignInNotification];
        } else {
            if(![INUserDefaultOperations isCustomerSignInNotificationAlreadySet])
                [INUserDefaultOperations setCustomerSignInNotification];        // After 7 days
        }
       // if(![INUserDefaultOperations isAppNotLaunchedNotificationAlreadySet])
            [INUserDefaultOperations setAppNotLaunchedNotification];        // After 4 days
    }
    
    [INUserDefaultOperations setAppEndDate];
    NSUInteger sessionTime = [INUserDefaultOperations getDateDifferenceInSeconds:[INUserDefaultOperations getAppStartDate] endDate:[INUserDefaultOperations getAppEndDate]];
    DebugLog(@"sessionTime -- > %ld",(long)sessionTime);
    DebugLog(@"last app time ---> %ld",(long)[INUserDefaultOperations getAppTotalUsageTime]);
    NSUInteger totalTime = [INUserDefaultOperations getAppTotalUsageTime] + sessionTime ;
    DebugLog(@"%ld",(long)totalTime);
    [INUserDefaultOperations setAppTotalUsageTime:totalTime];
    [INEventLogger localyticsSessionWillResignActive];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
//    activeAreaId = [self getActiveAreaIdFromAreaTable];
//     if ((activeAreaId == nil || [activeAreaId isEqualToString:@""]) && (![INUserDefaultOperations isCustomerLoggedIn] && [INUserDefaultOperations getCustomerLoginStateInTour] == 0)) {
//        [INUserDefaultOperations setTourFinishedState:0];
//        [IN_APP_DELEGATE animationStop];
//    }
    [INEventLogger localyticsSessionDidEnterBackground];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    DebugLog(@"applicationWillEnterForeground");
    [UIApplication sharedApplication].applicationIconBadgeNumber = 5;
    [[UAPush shared] setBadgeNumber:5];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[UAPush shared] resetBadge];
    [INEventLogger localyticsSessionWillEnterForeground];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[NSNotificationCenter defaultCenter] postNotificationName:IN_APP_BACKGROUND_CHANGE_NOTIFICATION object:nil];
    [FBAppCall handleDidBecomeActive];
    [INEventLogger localyticsSessionDidBecomeActive:LOCALYTICS_APPID];
    [self sendDailyEvents];
    [self sendMonthlyEvents];
    // Intimating App Install and App Activations to facebook so it appears in Insight session of shoplocal app on FB
    [FBSettings setDefaultAppID:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"FacebookAppID"]];
    [FBAppEvents activateApp];
    //[[UIApplication sharedApplication] cancelAllLocalNotifications];
    [INUserDefaultOperations setAppStartDate];
     DebugLog(@"applicationDidBecomeActive");
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    DebugLog(@"applicationWillTerminate");
    //[INUserDefaultOperations setTourFinishedState:0];
    [FBSession.activeSession close];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSCurrentLocaleDidChangeNotification  object:nil];
    [INEventLogger localyticsSessionWillTerminate];
}

@end
