//
//  INMerchantViewController.m
//  shoplocal
//
//  Created by Rishi on 12/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INMerchantViewController.h"
#import "INMerchantLoginViewController.h"
#import "INAddUpdateStoreViewController.h"
#import "INStoreListViewController.h"
#import "INBroadcastViewController.h"
#import "INMultiSelectStoreListViewController.h"
#import "INFBWebViewController.h"
#import "INAppDelegate.h"
#import "INInitialOfferViewController.h"
#import "CommonCallback.h"
#import "INStoreDetailObj.h"
#import "INAllPostsViewController.h"
#import "INStoreGalleryViewController.h"
#import "INMerchantReportsViewController.h"
#import "UAPush.h"

#define MERCHANT_STORE_LIST [NSString stringWithFormat:@"%@%@%@place_api/places",LIVE_SERVER,URL_PREFIX,API_VERSION]
#define VALIDATE_LOGIN_MERCHANT [NSString stringWithFormat:@"%@%@merchant_api/validate_login",URL_PREFIX,API_VERSION]

@interface INMerchantViewController ()
@property(strong) NSDictionary *splashJson;
@end

@implementation INMerchantViewController
@synthesize merchantLoginBtn;
@synthesize talkNowBtn;
@synthesize storeArray;
@synthesize rowcount;
@synthesize mainTableView;
@synthesize offerBackView;
@synthesize switchView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
} 

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if([INUserDefaultOperations isMerchantLoggedIn]){
        if ([IN_APP_DELEGATE networkavailable]) {
            [self sendMerchantValidateLoginRequest];
        }
        
        DebugLog(@"refresh-->%d",[INUserDefaultOperations getRefreshState]);
        if([INUserDefaultOperations getRefreshState] || [self getCountMyPlacesTable] == 0)
        {
            [self sendMerchantStoreListRequest];
        }
        //[merchantLoginBtn setBackgroundImage:[UIImage imageNamed:@"Grid_MLogout.png"] forState:UIControlStateNormal];
        //[merchantLoginBtn setTitle:@"Logout" forState:UIControlStateNormal] ;
        //merchantLoginBtn.backgroundColor = GRAY_COLOR_FOR_BUTTON;

    }else{
        //[merchantLoginBtn setBackgroundImage:[UIImage imageNamed:@"Grid_MLogin.png"] forState:UIControlStateNormal];
        //[merchantLoginBtn setTitle:@"Login" forState:UIControlStateNormal];
        //merchantLoginBtn.backgroundColor = GREEN_COLOR_FOR_BUTTON;

        if (switchView==0) {
            [self pushMerchantLoginScreen];
        }
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIImageView *imageView = (UIImageView *)[self.navigationController.navigationBar viewWithTag:1000];
    [imageView setHidden:FALSE];
    UILabel *lbl = (UILabel *)[self.navigationController.navigationBar viewWithTag:1001];
    [lbl setHidden:FALSE];
    UIButton *moreButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1002];
    [moreButton setHidden:FALSE];
    UILabel *lbl1 = (UILabel *)[self.navigationController.navigationBar viewWithTag:1003];
    [lbl1 setHidden:FALSE];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    UIImageView *imageView = (UIImageView *)[self.navigationController.navigationBar viewWithTag:1000];
    [imageView setHidden:TRUE];
    UILabel *lbl = (UILabel *)[self.navigationController.navigationBar viewWithTag:1001];
    [lbl setHidden:TRUE];
    UIButton *moreButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1002];
    [moreButton setHidden:TRUE];
    UILabel *lbl1 = (UILabel *)[self.navigationController.navigationBar viewWithTag:1003];
    [lbl1 setHidden:TRUE];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    switchView = 0;
    
    storeArray = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushStoreSelector) name:IN_MERCHANT_LOGIN_NOTIFICATION object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushOfferSelector) name:IN_MERCHANT_SHOW_OFFER_NOTIFICATION object:nil];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushBroadcastSelector) name:IN_MERCHANT_SHOW_BROADCAST_NOTIFICATION object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLocalNotification:) name:IN_MERCHANT_SHOW_LOCAL_NOTIFICATION object:nil];

    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendMerchantStoreListRequest) name:IN_MERCHANT_RELOAD_MYPLACES_NOTIFICATION object:nil];
    
    //carousel.type = iCarouselTypeCoverFlow;
    
    //[carousel scrollToItemAtIndex:1 animated:NO];
    
    [self addHUDView];
    
    //merchantLoginBtn.titleLabel.font           = DEFAULT_FONT(25);
    //merchantLoginBtn.titleLabel.textAlignment  = NSTextAlignmentCenter;
    //[merchantLoginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    talkNowBtn.titleLabel.font           = DEFAULT_BOLD_FONT(25);
    talkNowBtn.titleLabel.textAlignment  = NSTextAlignmentCenter;
    [talkNowBtn setTitle:@"Post an offer" forState:UIControlStateNormal];
    [talkNowBtn setTitleColor:BROWN_COLOR forState:UIControlStateNormal];
    
    offerBackView.backgroundColor = [UIColor colorWithRed:113/255.0 green:91/255.0  blue:87/255.0 alpha:0.8];
    offerBackView.layer.shadowColor      = [UIColor blackColor].CGColor;
    offerBackView.layer.shadowOffset     = CGSizeMake(1, 1);
    offerBackView.layer.shadowOpacity    = 1.0;
}


-(void)addHUDView
{
    hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:hud];
	hud.dimBackground = NO;
    hud.labelText = @"";
	hud.detailsLabelText = @"Refreshing store list,\nplease wait.";
    [hud hide:NO];
}

-(void)removeHUDView
{
    [hud removeFromSuperview];
    hud = nil;
}


-(void)pushStoreSelector
{
    DebugLog(@"PUSH IN_MERCHANT_LOGIN_NOTIFICATION");
    [self newpostBtnPressed:nil];
}

#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void)switchController
{
    switchView = 1;
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        switchView = 0;
        [IN_APP_DELEGATE switchViewController];
    }];
    
}


#pragma UIAlertView Delegates

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"YES"])
    {
        [INUserDefaultOperations clearMerchantDetails];
        //[merchantLoginBtn setBackgroundImage:[UIImage imageNamed:@"Grid_MLogin.png"] forState:UIControlStateNormal];
        //[merchantLoginBtn setTitle:@"Login" forState:UIControlStateNormal];
        //merchantLoginBtn.backgroundColor = GREEN_COLOR_FOR_BUTTON;
        [self pushMerchantLoginScreen];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showAddStoreAlert{
    SIAlertView *sialertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"You have not added any store. Would you like to add one?"];
    sialertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
    sialertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
    [sialertView addButtonWithTitle:@"Add Store"
                               type:SIAlertViewButtonTypeDestructive
                            handler:^(SIAlertView *alert) {
                                DebugLog(@"Add Store");
                                INAddUpdateStoreViewController *addupdateController = [[INAddUpdateStoreViewController alloc] initWithNibName:@"INAddUpdateStoreViewController" bundle:nil];
                                addupdateController.title = @"Add Store";
                                [self.navigationController pushViewController:addupdateController animated:YES];
                            }];
    [sialertView addButtonWithTitle:@"Cancel"
                               type:SIAlertViewButtonTypeCancel
                            handler:^(SIAlertView *alert) {
                                DebugLog(@"Cancel Clicked");
                            }];
    [sialertView show];
}

- (IBAction)newpostBtnPressed:(id)sender {
    if([self getCountMyPlacesTable] == 0)
    {
        if (sender == nil) {
            INAddUpdateStoreViewController *addupdateController = [[INAddUpdateStoreViewController alloc] initWithNibName:@"INAddUpdateStoreViewController" bundle:nil];
            addupdateController.title = @"Add Store";
            [self.navigationController pushViewController:addupdateController animated:YES];
        }else{
            [self showAddStoreAlert];
        }
    } else if([self getCountMyPlacesTable] == 1){
        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
        NSString *placeid = [NSString stringWithFormat:@"%d",[self getMyPlacesStoreId]];
        [tempArray addObject:placeid];
        DebugLog(@"placeid--->%@",placeid);
        [INEventLogger logEvent:@"Tab_TalkNow"];
        INBroadcastViewController *broadcastController = [[INBroadcastViewController alloc] initWithNibName:@"INBroadcastViewController" bundle:nil];
        broadcastController.title = @"Talk to shoppers";
        broadcastController.placeidArray = tempArray;
        [self.navigationController pushViewController:broadcastController animated:YES];
        tempArray = nil;
    } else {
        [INEventLogger logEvent:@"Tab_TalkNow"];
        INMultiSelectStoreListViewController *multistoreListController = [[INMultiSelectStoreListViewController alloc] initWithNibName:@"INMultiSelectStoreListViewController" bundle:nil];
        multistoreListController.title = @"Choose Place";
        multistoreListController.storeListType = 1;  //Add Post
        [self.navigationController pushViewController:multistoreListController animated:YES];
    }
}

- (IBAction)viewpostBtnPressed:(id)sender {
    if([self getCountMyPlacesTable] == 0)
    {
//        INAddUpdateStoreViewController *addupdateController = [[INAddUpdateStoreViewController alloc] initWithNibName:@"INAddUpdateStoreViewController" bundle:nil];
//        addupdateController.title = @"Add Store";
//        [self.navigationController pushViewController:addupdateController animated:YES];
        [self showAddStoreAlert];

    } else if([self getCountMyPlacesTable] == 1){
        [INEventLogger logEvent:@"Tab_ViewPost"];
        INAllPostsViewController *allpostController = [[INAllPostsViewController alloc] initWithNibName:@"INAllPostsViewController" bundle:nil];
        allpostController.title = @"Store Broadcasts";
        allpostController.storeId = [self getMyPlacesStoreId];
        allpostController.storeName = [self getMyPlacesStoreName];
        [self.navigationController pushViewController:allpostController animated:YES];
    } else {
        [INEventLogger logEvent:@"Tab_ViewPost"];
        INStoreListViewController *storeListController = [[INStoreListViewController alloc] initWithNibName:@"INStoreListViewController" bundle:nil];
        storeListController.title = @"Choose Place";
        storeListController.storeListType = 2; //See post details
        [self.navigationController pushViewController:storeListController animated:YES];
    }
}

- (IBAction)addstoreBtnPressed:(id)sender {
    [INEventLogger logEvent:@"Tab_AddStore"];
    INAddUpdateStoreViewController *addupdateController = [[INAddUpdateStoreViewController alloc] initWithNibName:@"INAddUpdateStoreViewController" bundle:nil];
    addupdateController.title = @"Add Store";
    [self.navigationController pushViewController:addupdateController animated:YES];
}

- (IBAction)editstoreBtnPressed:(id)sender {
    if([self getCountMyPlacesTable] == 0)
    {
//        INAddUpdateStoreViewController *addupdateController = [[INAddUpdateStoreViewController alloc] initWithNibName:@"INAddUpdateStoreViewController" bundle:nil];
//        addupdateController.title = @"Add Store";
//        [self.navigationController pushViewController:addupdateController animated:YES];
        [self showAddStoreAlert];
    } else if([self getCountMyPlacesTable] == 1){
        [INEventLogger logEvent:@"Tab_EditStore"];
        INAddUpdateStoreViewController *addupdateController = [[INAddUpdateStoreViewController alloc] initWithNibName:@"INAddUpdateStoreViewController" bundle:nil];
        addupdateController.title = @"Edit Store";
        addupdateController.placeId = [NSString stringWithFormat:@"%d", [self getMyPlacesStoreId]];
        [self.navigationController pushViewController:addupdateController animated:YES];
    } else {
        [INEventLogger logEvent:@"Tab_EditStore"];
        INStoreListViewController *storeListController = [[INStoreListViewController alloc] initWithNibName:@"INStoreListViewController" bundle:nil];
        storeListController.title = @"Choose Place";
        storeListController.storeListType = 4; //Update Store Details
        [self.navigationController pushViewController:storeListController animated:YES];
    }
}

- (IBAction)managegalleryBtnPressed:(id)sender {
    if([self getCountMyPlacesTable] == 0)
    {
//        INAddUpdateStoreViewController *addupdateController = [[INAddUpdateStoreViewController alloc] initWithNibName:@"INAddUpdateStoreViewController" bundle:nil];
//        addupdateController.title = @"Add Store";
//        [self.navigationController pushViewController:addupdateController animated:YES];
        [self showAddStoreAlert];
    } else if([self getCountMyPlacesTable] == 1){
        NSString *placeid = [NSString stringWithFormat:@"%d",[self getMyPlacesStoreId]];
        DebugLog(@"placeid--->%@",placeid);
        [INEventLogger logEvent:@"Tab_ManageGallery"];
        INStoreGalleryViewController *galleryController = [[INStoreGalleryViewController alloc] initWithNibName:@"INStoreGalleryViewController" bundle:nil];
        galleryController.title = @"Manage Gallery";
        galleryController.storeId = [self getMyPlacesStoreId];
        [self.navigationController pushViewController:galleryController animated:YES];
    } else {
        [INEventLogger logEvent:@"Tab_ManageGallery"];
        INStoreListViewController *storeListController = [[INStoreListViewController alloc] initWithNibName:@"INStoreListViewController" bundle:nil];
        storeListController.title = @"Choose Place";
        storeListController.storeListType = 3; //Manage Gallery
        [self.navigationController pushViewController:storeListController animated:YES];
    }
 
}

- (IBAction)reportsBtnPressed:(id)sender {
    if([self getCountMyPlacesTable] == 0)
    {
//        INAddUpdateStoreViewController *addupdateController = [[INAddUpdateStoreViewController alloc] initWithNibName:@"INAddUpdateStoreViewController" bundle:nil];
//        addupdateController.title = @"Add Store";
//        [self.navigationController pushViewController:addupdateController animated:YES];
        [self showAddStoreAlert];
    } else if([self getCountMyPlacesTable] == 1){
        [INEventLogger logEvent:@"Tab_Report"];
        INMerchantReportsViewController *reportsViewController = [[INMerchantReportsViewController alloc] initWithNibName:@"INMerchantReportsViewController" bundle:nil];
        reportsViewController.title = [NSString stringWithFormat:@"%@ Reports",[self getMyPlacesStoreName]];
        reportsViewController.storeName = [self getMyPlacesStoreName];
        reportsViewController.placeId = [self getMyPlacesStoreId];
        [self.navigationController pushViewController:reportsViewController animated:YES];
    } else {
        [INEventLogger logEvent:@"Tab_Report"];
        INStoreListViewController *storeListController = [[INStoreListViewController alloc] initWithNibName:@"INStoreListViewController" bundle:nil];
        storeListController.title = @"Choose Place";
        storeListController.storeListType = 5; //See report details
        [self.navigationController pushViewController:storeListController animated:YES];
    }
    
}

- (IBAction)fbBtnPressed:(id)sender {
    [INEventLogger logEvent:@"Tab_MerchantFacebook"];
    INFBWebViewController *fbViewController = [[INFBWebViewController alloc] initWithNibName:@"INFBWebViewController" bundle:nil] ;
    fbViewController.title = @"Facebook";
    fbViewController.type = 1;
    [self.navigationController pushViewController:fbViewController animated:YES];
}

- (IBAction)twitterBtnPressed:(id)sender {
    [INEventLogger logEvent:@"Tab_MerchantTwitter"];
    INFBWebViewController *twViewController = [[INFBWebViewController alloc] initWithNibName:@"INFBWebViewController" bundle:nil] ;
    twViewController.title = @"Twitter";
    twViewController.type = 2;
    [self.navigationController pushViewController:twViewController animated:YES];
}

- (IBAction)logoutBtnPressed:(id)sender {
    if(![INUserDefaultOperations isMerchantLoggedIn])
    {
        [self pushMerchantLoginScreen];
    } else {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: @"Are you sure you want to log out?"
                                                          delegate: self
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"NO", @"YES", nil
                              ];
        [alert show];
    }
}

-(void)pushMerchantLoginScreen
{
    INMerchantLoginViewController *mercLoginController = [[INMerchantLoginViewController alloc] initWithNibName:@"INMerchantLoginViewController" bundle:nil] ;
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:mercLoginController];
        UIButton *moreButton1 =  [UIButton buttonWithType:UIButtonTypeCustom];
        [moreButton1 setTitle:@"Cancel" forState:UIControlStateNormal];
        moreButton1.titleLabel.font = [UIFont systemFontOfSize:17.0];
        [moreButton1 setImage:[UIImage imageNamed:@"switch.png"] forState:UIControlStateNormal];
        [moreButton1 addTarget:self action:@selector(switchController) forControlEvents:UIControlEventTouchUpInside];
        [moreButton1 setFrame:CGRectMake(280, 7, 32, 32)];
        [navBar.navigationBar addSubview:moreButton1];
    [self.navigationController presentViewController:navBar animated:YES completion:nil];
}

-(void)pushOfferSelector
{
    [self performSelector:@selector(showInitialOfferScreen) withObject:nil afterDelay:0.3];
}

-(void)showInitialOfferScreen {
    INInitialOfferViewController *mercLoginController = [[INInitialOfferViewController alloc] initWithNibName:@"INInitialOfferViewController" bundle:nil] ;
    mercLoginController.title = @"";
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:mercLoginController];
    
    UIImage *image = [UIImage imageNamed:@"bar_icon.png"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.frame = CGRectMake(5, 10, 30, 30);
    
    UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(imageView.frame)+10, 0, 100, 44)];
    [lbl setText:@"Shoplocal"];
    lbl.backgroundColor = DEFAULT_COLOR;
    lbl.textAlignment = NSTextAlignmentLeft;
    lbl.textColor = [UIColor whiteColor];
    lbl.font = DEFAULT_FONT(18);
    
    [navBar.navigationBar addSubview:imageView];
    [navBar.navigationBar addSubview:lbl];
    [navBar.navigationBar setTranslucent:NO];
    [self.navigationController presentViewController:navBar animated:YES completion:nil];
}

-(void)pushBroadcastSelector
{
    [self performSelector:@selector(newpostBtnPressed:) withObject:nil afterDelay:0.3];
}

-(void)handleLocalNotification:(NSNotification *)notification
{
    DebugLog(@"handleLocalNotification");
    NSDictionary *dict            = [notification userInfo];
    NSString *TabNameToOpen       = [dict objectForKey:IN_MERCHANT_SHOW_LOCAL_NOTIFICATION_KEY];
    DebugLog(@"TabName %@",TabNameToOpen);
    if ([TabNameToOpen isEqualToString:TALKNOW_LNTYPE]) {
        [self newpostBtnPressed:nil];
    }else if([TabNameToOpen isEqualToString:EDITSTORE_LNTYPE]){
        [self editstoreBtnPressed:nil];
    }
}

- (IBAction)sureShopBtnPressed:(id)sender {
    INStoreListViewController *storeListController = [[INStoreListViewController alloc] initWithNibName:@"INStoreListViewController" bundle:nil];
    storeListController.title = @"Choose Place";
    storeListController.storeListType = 6; //See sureshop details
    [self.navigationController pushViewController:storeListController animated:YES];
}

- (IBAction)contactUsBtnPressed:(id)sender {
    if ([MFMailComposeViewController canSendMail]) {
        [INEventLogger logEvent:@"Tab_MerchantContactUs"];
        MFMailComposeViewController *emailComposer = [[MFMailComposeViewController alloc] init];
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            emailComposer.navigationBar.tintColor  = [UIColor whiteColor];
        }
        emailComposer.mailComposeDelegate = self;
        [emailComposer setToRecipients:[NSArray arrayWithObjects:@"contact@shoplocal.co.in", nil]];
        [emailComposer setSubject:@""];
        [emailComposer setMessageBody:@"" isHTML:NO];
        [self presentViewController:emailComposer animated:YES completion:nil];
    }else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"You may not have set up mail service on your device or You may not connected to internet.\nPlease check and try again."
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

#pragma mark Mail Compose Delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    if (result == MFMailComposeResultFailed) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                        message:NSLocalizedString(@"Email failed to send. Please try again.", nil)
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", nil) otherButtonTitles:nil];
		[alert show];
    }else if (result == MFMailComposeResultSent)
    {
        DebugLog(@"MFMailComposeResultSent");
    }
	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 9;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UIView *topView;
    UILabel *lblTitle;
    UIImageView *thumbImg;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backgroundColor = [UIColor clearColor];
        cell.layer.shadowOffset = CGSizeMake(-15, 20);
        cell.layer.shadowColor = [UIColor darkGrayColor].CGColor;
        cell.layer.shadowRadius = 5;
        
        cell.layer.shadowPath = [UIBezierPath bezierPathWithRect:cell.bounds].CGPath;
        
        // Create the top view
        topView = [[UIView alloc] initWithFrame:CGRectMake(0, 5, 320, 75)];
        topView.tag = 111;
        topView.backgroundColor = [UIColor whiteColor];
        //topView = [CommonCallback setViewPropertiesWithRoundedCorner:topView];
        topView.clipsToBounds = YES;
        
        
        thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(5, 7, 60, 60)];
        thumbImg.tag = 1;
        thumbImg.contentMode = UIViewContentModeScaleAspectFit;
        //[cell.contentView addSubview:thumbImg];
        [topView addSubview:thumbImg];
        
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(80.0,25.0,180.0,25.0)];
        lblTitle.text = @"";
        lblTitle.tag = 2;
        lblTitle.font = DEFAULT_FONT(22);
        lblTitle.textAlignment = NSTextAlignmentLeft;
        lblTitle.textColor = DEFAULT_COLOR;
        lblTitle.backgroundColor =  [UIColor clearColor];
        //[cell.contentView addSubview:lblTitle];
        [topView addSubview:lblTitle];
        
//        UILabel *lblline = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 73.0, 300.0, 2.0)];
//        lblline.text = @"";
//        lblline.backgroundColor =  LIGHT_ORANGE_COLOR;
//        [topView addSubview:lblline];
        
        // Add views to contentView
        [cell.contentView addSubview:topView];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = BROWN_OFFWHITE_COLOR;
        cell.selectedBackgroundView = bgColorView;
        
    }
    
    topView = (UIView *)[cell viewWithTag:111];
    

        switch (indexPath.row) {
            case 0:
            {
                topView.backgroundColor = [UIColor whiteColor];
                
                UILabel *lblTitle = (UILabel *)[topView viewWithTag:2];
                [lblTitle setText:@"Reports"];
                
                UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
                [thumbImgview setImage:[UIImage imageNamed:@"Reports.png"]];
            }
                break;
                
            case 1:
            {
                topView.backgroundColor = [UIColor whiteColor];
                
                UILabel *lblTitle = (UILabel *)[topView viewWithTag:2];
                [lblTitle setText:@"View Post"];
                
                UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
                [thumbImgview setImage:[UIImage imageNamed:@"View-Post.png"]];
            }
                break;
                

            case 2:
            {
                topView.backgroundColor = [UIColor whiteColor];
                
                UILabel *lblTitle = (UILabel *)[topView viewWithTag:2];
                [lblTitle setText:@"Add Store"];
                
                UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
                [thumbImgview setImage:[UIImage imageNamed:@"AddStore.png"]];
            }
                break;
                
            case 3:
            {
                topView.backgroundColor = [UIColor whiteColor];
                
                UILabel *lblTitle = (UILabel *)[topView viewWithTag:2];
                [lblTitle setText:@"Edit Store"];
                
                UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
                [thumbImgview setImage:[UIImage imageNamed:@"EditStore.png"]];
            }
                break;
                
            case 4:
            {
                topView.backgroundColor = [UIColor whiteColor];
                
                UILabel *lblTitle = (UILabel *)[topView viewWithTag:2];
                [lblTitle setText:@"Manage Gallery"];
                
                UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
                [thumbImgview setImage:[UIImage imageNamed:@"Gallery.png"]];
            }
                break;
                
//            case 5:
//            {
//                topView.backgroundColor = [UIColor whiteColor];
//                
//                UILabel *lblTitle = (UILabel *)[topView viewWithTag:2];
//                [lblTitle setText:@"Facebook"];
//                
//                UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
//                [thumbImgview setImage:[UIImage imageNamed:@"Facebook_circle.png"]];
//            }
//                break;
//                
//            case 6:
//            {
//                topView.backgroundColor = [UIColor whiteColor];
//                
//                UILabel *lblTitle = (UILabel *)[topView viewWithTag:2];
//                [lblTitle setText:@"Twitter"];
//                
//                UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
//                [thumbImgview setImage:[UIImage imageNamed:@"Twitter_circle.png"]];
//            }
//                break;
                
            case 5:
            {
                topView.backgroundColor = [UIColor whiteColor];
                
                UILabel *lblTitle = (UILabel *)[topView viewWithTag:2];
                [lblTitle setText:@"Contact Us"];
                
                UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
                [thumbImgview setImage:[UIImage imageNamed:@"EMail.png"]];
            }
                break;
                
            case 6:
            {
                topView.backgroundColor = [UIColor whiteColor];
                
                UILabel *lblTitle = (UILabel *)[topView viewWithTag:2];
                [lblTitle setText:@"Logout"];
                
                UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
                [thumbImgview setImage:[UIImage imageNamed:@"Logout.png"]];
            }
                break;
                
                
            default:
            {
                UILabel *lblTitle = (UILabel *)[topView viewWithTag:2];
                [lblTitle setText:@""];
                
                UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
                [thumbImgview setImage:nil];
                
                topView.backgroundColor = [UIColor clearColor];
            }
                break;
        }
    
    return cell;
}


#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (![IN_APP_DELEGATE networkavailable]) {
        [INUserDefaultOperations showOfflineAlert];
        return;
    }
    
    switch (indexPath.row) {
        case 0:
        {
             [self reportsBtnPressed:nil];

        }
            break;
            
        case 1:
        {
            [self viewpostBtnPressed:nil];

        }
            break;
            
        case 2:
        {
            [self addstoreBtnPressed:nil];

        }
            break;
            
        case 3:
        {
            [self editstoreBtnPressed:nil];
            
        }
            break;
            
        case 4:
        {
            [self managegalleryBtnPressed:nil];
            
        }
            break;
            
//        case 5:
//        {
//            [self fbBtnPressed:nil];
//            
//        }
//            break;
//            
//        case 6:
//        {
//            [self twitterBtnPressed:nil];
//            
//        }
//            break;
            
        case 5:
        {
            [self contactUsBtnPressed:nil];
            
        }
            break;
            
        case 6:
        {
            [self logoutBtnPressed:nil];
            
        }
            break;
            
        default:
            break;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


-(void)sendMerchantStoreListRequest
{
    [hud show:YES];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:MERCHANT_STORE_LIST]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setValue:[INUserDefaultOperations getMerchantAuthId] forHTTPHeaderField:@"user_id"];
    [urlRequest setValue:[INUserDefaultOperations getMerchantAuthCode] forHTTPHeaderField:@"auth_id"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        [CommonCallback hideProgressHud];
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"%@",self.splashJson);
                                                        if(self.splashJson  != nil) {
                                                            [storeArray removeAllObjects];
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                [INUserDefaultOperations setEditStoreState:FALSE];
                                                                NSArray* jsonArray = [self.splashJson  objectForKey:@"data"];
                                                                DebugLog(@"\n\n%@\n\n",jsonArray);
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *objdict = [jsonArray  objectAtIndex:index];
                                                                    DebugLog(@"\n\n%@, %d\n\n",objdict,[objdict count]);
                                                                    storeObj = [[INStoreDetailObj alloc] init];
                                                                    storeObj.ID = [[objdict objectForKey:@"id"] integerValue];
                                                                    storeObj.name = [objdict objectForKey:@"name"];
                                                                    storeObj.place_parent = [[objdict objectForKey:@"place_parent"] integerValue];
                                                                    storeObj.place_status = [objdict objectForKey:@"place_status"];
                                                                    storeObj.published = [objdict objectForKey:@"published"];
                                                                    storeObj.description = [objdict objectForKey:@"description"];
                                                                    storeObj.building = [objdict objectForKey:@"building"];
                                                                    storeObj.street = [objdict objectForKey:@"street"];
                                                                    storeObj.landmark = [objdict objectForKey:@"landmark"];
                                                                    storeObj.area = [objdict objectForKey:@"area"];
                                                                    storeObj.pincode = [objdict objectForKey:@"pincode"];
                                                                    storeObj.city = [objdict objectForKey:@"city"];
                                                                    storeObj.state = [objdict objectForKey:@"state"];
                                                                    storeObj.country = [objdict objectForKey:@"country"];
                                                                    storeObj.mob_no1 = [objdict objectForKey:@"mob_no1"];
                                                                    storeObj.mob_no2 = [objdict objectForKey:@"mob_no2"];
                                                                    storeObj.mob_no3 = [objdict objectForKey:@"mob_no3"];
                                                                    storeObj.tel_no1 = [objdict objectForKey:@"tel_no1"];
                                                                    storeObj.tel_no2 = [objdict objectForKey:@"tel_no2"];
                                                                    storeObj.tel_no3 = [objdict objectForKey:@"tel_no3"];
                                                                    storeObj.image_url = [objdict objectForKey:@"image_url"];
                                                                    storeObj.email = [objdict objectForKey:@"email"];
                                                                    storeObj.website = [objdict objectForKey:@"website"];
                                                                    storeObj.total_like = [objdict objectForKey:@"total_like"];
                                                                    storeObj.total_share = [objdict objectForKey:@"total_share"];
                                                                    storeObj.total_view = [objdict objectForKey:@"total_view"];
                                                                    storeObj.fb_url = [objdict objectForKey:@"facebook_url"];
                                                                    storeObj.twitter_url = [objdict objectForKey:@"twitter_url"];
                                                                    storeObj.latitude = [objdict objectForKey:@"latitude"];
                                                                    storeObj.longitude = [objdict objectForKey:@"longitude"];
                                                                    
                                                                    if((storeObj.latitude == 0) || ([storeObj.description length] == 0) || ([storeObj.image_url length] == 0))
                                                                    {
                                                                        [INUserDefaultOperations setEditStoreState:TRUE];  //Set Alarm
                                                                    } 
                                                                    
                                                                    [storeArray addObject:storeObj];
                                                                    storeObj = nil;
                                                                }
                                                            }
                                                            [self deleteMyPlacesTable];
                                                            [self insertAllStoreListTable];
                                                            [INUserDefaultOperations setRefreshState:FALSE];
                                                        }
                                                        [hud hide:YES];
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        [hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                        [av show];
                                                        
                                                    }];
    
    
    
    [operation start];
}


#pragma Table Operations
- (void)insertAllStoreListTable
{
    NSMutableArray *idtagsArray = [[NSMutableArray alloc] init];

    DebugLog(@"%d",[storeArray count]);
    sqlite3_stmt    *statement;
    int lastrowid;
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        for (int index=0; index<[storeArray count] ; index++)
        {
            INStoreDetailObj *tempstoreObj = [storeArray objectAtIndex:index];
            NSString *insertSQL = [NSString stringWithFormat:
                                   @"INSERT or REPLACE INTO MYPLACES (PLACEID, PLACE_PARENT, NAME, DESCRIPTION, BUILDING, STREET, LANDMARK, AREA, CITY, MOB_NO1, MOB_NO2, MOB_NO3, TEL_NO1, TEL_NO2, TEL_NO3, IMG_URL, EMAIL, WEBSITE, TOTAL_LIKE , TOTAL_SHARE , TOTAL_VIEW , PLACE_STATUS , PUBLISHED , COUNTRY , STATE , PINCODE , FB_URL , TWITTER_URL , LATITUDE , LONGITUDE) VALUES (\"%d\",\"%d\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",tempstoreObj.ID,tempstoreObj.place_parent,tempstoreObj.name,[tempstoreObj.description stringByReplacingOccurrencesOfString:@"\"" withString:@"'"],tempstoreObj.building,tempstoreObj.street,tempstoreObj.landmark,tempstoreObj.area,tempstoreObj.city,tempstoreObj.mob_no1,tempstoreObj.mob_no2,tempstoreObj.mob_no3, tempstoreObj.tel_no1, tempstoreObj.tel_no2, tempstoreObj.tel_no3, tempstoreObj.image_url, tempstoreObj.email, tempstoreObj.website, tempstoreObj.total_like, tempstoreObj.total_share, tempstoreObj.total_view, tempstoreObj.place_status, tempstoreObj.published, tempstoreObj.country,tempstoreObj.state, tempstoreObj.pincode, tempstoreObj.fb_url, tempstoreObj.twitter_url, tempstoreObj.latitude, tempstoreObj.longitude];
            DebugLog(@"%@",insertSQL);
            const char *insert_stmt = [insertSQL UTF8String];
            sqlite3_prepare_v2(shoplocalDB, insert_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                lastrowid = sqlite3_last_insert_rowid(shoplocalDB);
                DebugLog(@"inserted id========%d", lastrowid);
                [idtagsArray addObject:[NSString stringWithFormat:@"%d",tempstoreObj.ID]];
            } else {
                DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
            }
            sqlite3_finalize(statement);
        }
    }
    sqlite3_close(shoplocalDB);
    if (idtagsArray.count > 0) {
        [CommonCallback addTagsInUrbanAriship:idtagsArray name:PUSH_TAG_MERCHANT_MYPLACES];
    }
}

-(void)deleteMyPlacesTable
{
    sqlite3_stmt    *statement;
    
    DebugLog(@"path---->%@",[IN_APP_DELEGATE databasePath]);
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM MYPLACES"];
        DebugLog(@"delete---->%@",deleteSQL);
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(shoplocalDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"All rows deleted");
        } else {
            DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
}

-(int)getCountMyPlacesTable
{
    sqlite3_stmt    *statement;
    
    DebugLog(@"path---->%@",[IN_APP_DELEGATE databasePath]);
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        NSString *countSQL = [NSString stringWithFormat:@"SELECT count(*) FROM MYPLACES"];
        DebugLog(@"select---->%@",countSQL);
        const char *count_stmt = [countSQL UTF8String];
        if (sqlite3_prepare_v2(shoplocalDB, count_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                rowcount =  sqlite3_column_int(statement, 0);
                DebugLog(@"%d", rowcount);
            } else {
                DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
            }
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
    return rowcount;
}

-(int)getMyPlacesStoreId
{
    sqlite3_stmt    *statement;
    int placeid = 0;
    DebugLog(@"path---->%@",[IN_APP_DELEGATE databasePath]);
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        NSString *placeidSQL = [NSString stringWithFormat:@"SELECT PLACEID FROM MYPLACES"];
        DebugLog(@"placeSQL---->%@",placeidSQL);
        const char *placeid_stmt = [placeidSQL UTF8String];
        if (sqlite3_prepare_v2(shoplocalDB, placeid_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                placeid =  sqlite3_column_int(statement, 0);
                DebugLog(@"%d", placeid);
            } else {
                DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
            }
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
    return placeid;
}

-(NSString *)getMyPlacesStoreName
{
    sqlite3_stmt    *statement;
    NSString * placeName = @"";
    DebugLog(@"path---->%@",[IN_APP_DELEGATE databasePath]);
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        NSString *placenameSQL = [NSString stringWithFormat:@"SELECT NAME FROM MYPLACES"];
        DebugLog(@"placeSQL---->%@",placenameSQL);
        const char *placename_stmt = [placenameSQL UTF8String];
        if (sqlite3_prepare_v2(shoplocalDB, placename_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                placeName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                DebugLog(@"%@", placeName);
            } else {
                DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
            }
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
    return placeName;
}

- (void)viewDidUnload {
    //[self setMerchantLoginBtn:nil];
    [super viewDidUnload];
}


-(void)sendMerchantValidateLoginRequest
{
    NSDate *lastvalidateloginDate = [INUserDefaultOperations getMerchantValidateLoginDate];
    DebugLog(@"/////////////////////////\n---lastvalidateloginDate %@---///////////////////////////////////\n",lastvalidateloginDate);
    if (lastvalidateloginDate != nil) {
        NSDate *currentDate = [NSDate date];
        
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
                                                            fromDate:lastvalidateloginDate
                                                              toDate:currentDate
                                                             options:0];
        DebugLog(@"days = %d",[components day]);
        DebugLog(@"-----lastvalidateloginDate %@ --currentDate %@--",lastvalidateloginDate,currentDate);
        
        if([components day] == 0) // Compare expiration dates. if greater than one then users session timeout occurs.
        {
            return; // lastvalidateloginDate != current date
        }
    }
    
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"X-HTTP-Method-Override" value:@"PUT"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSString *register_from = [NSString stringWithFormat:@"%@ v%@",DEVICE_TYPE,version];

    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[INUserDefaultOperations getMerchantAuthId] forKey:@"user_id"];
    [params setObject:[INUserDefaultOperations getMerchantAuthCode] forKey:@"auth_id"];
    [params setObject:register_from forKey:@"register_from"];
    DebugLog(@"params = %@",params);

    [httpClient postPath:VALIDATE_LOGIN_MERCHANT parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            if([[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
                DebugLog(@"Validate Login success");
                [INUserDefaultOperations setMerchantValidateLoginDate:[NSDate date]];

                [INEventLogger logEvent:@"Merchant_ValidateLogin"];
                
                //When merchant logged in then add him to UAPush
//                [[UAPush shared] addTagToCurrentDevice:@"merchant"];
//                [[UAPush shared] updateRegistration];
                [CommonCallback addTagsInUrbanAriship:[[NSArray alloc] initWithObjects:PUSH_TAG_MERCHANT, nil] name:PUSH_TAG_MERCHANT];

            } else {
                if([json  objectForKey:@"code"] != [NSNull null] && [[json  objectForKey:@"code"] isEqualToString:MERCHANT_INVALID_USER_AUTH_CODE]) {
                    [INUserDefaultOperations showAlert:MERCHANT_INVALID_USER_AUTH_MESSAGE];
                    
                    [self pushMerchantLoginScreen];
                    [INUserDefaultOperations clearMerchantDetails];
                }
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}

@end
