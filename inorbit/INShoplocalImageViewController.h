//
//  INShoplocalImageViewController.h
//  shoplocal
//
//  Created by Rishi on 20/11/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VCGridView.h"

@protocol ShoplocalImageViewControllerDelegate <NSObject>
@required
-(void)saveImagePath:(NSString *)image;
@end

@interface INShoplocalImageViewController : UIViewController <VCGridViewDelegate, VCGridViewDataSource>
{
    VCGridView *_gridView;
}
@property(nonatomic,assign) id<ShoplocalImageViewControllerDelegate> delegate;
@property (nonatomic, retain) VCGridView *gridView;
@property (strong, nonatomic) NSMutableArray *galleryArray;
@end
