//
//  FaceBookShareViewController.h
//  shoplocal
//
//  Created by Kirti Nikam on 06/09/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

#define FACEBOOK_POST_SUCCESS 0
#define FACEBOOK_POST_FAIL 1

@protocol FacebookProtocolDelegate <NSObject>
-(void)faceBookCompletionCallBack:(int)result;
@end

@interface FaceBookShareViewController : UIViewController <UITextViewDelegate>
@property (nonatomic, copy) NSString *FBtitle;
@property (nonatomic, copy) NSString *FBtLink;
@property (nonatomic, copy) NSString *FBtCaption;
@property (nonatomic, copy) NSString *FBtPic;
@property (nonatomic, copy) NSString *postMessageText;
@property (strong, nonatomic) NSMutableDictionary *postParams;


@property(nonatomic,assign) id<FacebookProtocolDelegate> fbdelegate;

@property (strong, nonatomic) IBOutlet UIButton *fbLoginBtn;
@property (strong, nonatomic) IBOutlet UIButton *cancelBtn;

@property (strong, nonatomic) IBOutlet UIView *mainFBView;
@property (strong, nonatomic) IBOutlet UIButton *loginFBBtn;
@property (strong, nonatomic) IBOutlet UIButton *postFBBtn;
@property (strong, nonatomic) IBOutlet UIButton *cancelFBBtn;
@property (strong, nonatomic) IBOutlet UITextView *postMessageTextView;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
@property (strong, nonatomic) IBOutlet FBProfilePictureView *userProfileImage;

- (IBAction)fbLoginBtnPressed:(id)sender;
- (IBAction)cancelBtnPressed:(id)sender;
- (IBAction)postFBBtnPressed:(id)sender;
@end
