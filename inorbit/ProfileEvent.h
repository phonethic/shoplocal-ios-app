//
//  ProfileEvent.h
//  shoplocal
//
//  Created by Kirti Nikam on 03/09/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProfileEvent : NSObject
@property (nonatomic,copy) NSDate *dateVal;
@property (nonatomic,copy) NSString *date;
@property (nonatomic,copy) NSString *dateCategory;
@end
