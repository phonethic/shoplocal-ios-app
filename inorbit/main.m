//
//  main.m
//  shoplocal
//
//  Created by Rishi on 22/07/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "INAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([INAppDelegate class]));
    }
}
