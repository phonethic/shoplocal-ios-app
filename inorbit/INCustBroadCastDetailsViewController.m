//
//  INCustBroadCastDetailsViewController.m
//  shoplocal
//
//  Created by Kirti Nikam on 17/09/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import <Social/Social.h>

#import "INCustBroadCastDetailsViewController.h"
#import "constants.h"
#import "INGalleryObj.h"
#import "CommonCallback.h"
#import "INAppDelegate.h"
#import "CommonCallback.h"
#import "ActionSheetPicker.h"
#import "INDetailViewController.h"
#import "SIAlertView.h"
#import "INStoreDetailViewController.h"

#import "FacebookOpenGraphAPI.h"
#import "FacebookShareAPI.h"

#define LIKE_STORE [NSString stringWithFormat:@"%@%@place_api/like",URL_PREFIX,API_VERSION]

#define LIKE_BROADCAST_OF_STORE [NSString stringWithFormat:@"%@%@broadcast_api/like",URL_PREFIX,API_VERSION]
#define UNLIKE_BROADCAST_OF_STORE(ID) [NSString stringWithFormat:@"%@%@broadcast_api/like?post_id=%d",URL_PREFIX,API_VERSION,ID]

#define SHARE_STORE_POST [NSString stringWithFormat:@"%@%@broadcast_api/share",URL_PREFIX,API_VERSION]

#define THUMBNAIL_LINK(IMG_LINK) [NSString stringWithFormat:@"%@%@%@",LIVE_SERVER,URL_PREFIX,IMG_LINK]


#define BROADCAST_DETAIL(ID) [NSString stringWithFormat:@"%@%@%@broadcast_api/broadcasts?post_id=%d",LIVE_SERVER,URL_PREFIX,API_VERSION,ID]
#define BROADCAST_DETAIL_WITH_USERID(ID,USER_ID) [NSString stringWithFormat:@"%@%@%@broadcast_api/broadcasts?post_id=%d&user_id=%@",LIVE_SERVER,URL_PREFIX,API_VERSION,ID,USER_ID]

#define CALL_STORE [NSString stringWithFormat:@"%@%@broadcast_api/call",URL_PREFIX,API_VERSION]

#define THUMBNAIL_LINK(IMG_LINK) [NSString stringWithFormat:@"%@%@%@",LIVE_SERVER,URL_PREFIX,IMG_LINK]

@interface INCustBroadCastDetailsViewController ()
@property(strong) NSDictionary *splashJson;
@property ACAccount* facebookAccount;
@end

@implementation INCustBroadCastDetailsViewController
@synthesize viewOpenedFrom;
@synthesize titlelbl;
@synthesize descriptionlbl;
@synthesize postimageView;
@synthesize postTitle;
@synthesize postDescription;
@synthesize postimageUrl,postimageTitle;
@synthesize postId;
@synthesize likepostBtn;
@synthesize postType;
@synthesize postlikeType;
@synthesize storeName;
@synthesize galleryArray;
@synthesize photos = _photos;
@synthesize sharepostBtn;
@synthesize postView;
@synthesize splashJson;
@synthesize scrollView;
@synthesize lblFav,lblShare;
@synthesize buyBtn,gotoStoreBtn;
@synthesize likescountString,sharecountString;
@synthesize telArray;
@synthesize selectedTelNumberToCall;
@synthesize placeId;
@synthesize likepostBtnImageView,sharepostBtnImageView;
@synthesize SHOW_BUY_BTN;
@synthesize addStoreToMyShoplocalBtn;
@synthesize buyBtnBackView;
@synthesize postIsOffered,postOfferDateTime;
@synthesize imgSeperator1,imgSeperator2;
@synthesize buttonCollection;
@synthesize lblPostedOn,lblOfferDate;
@synthesize imgSeperator3;
@synthesize postDateTime;
@synthesize callModalViewBackView,callModalView,lblcallModalHeader,lblcallModalHeaderSeperator,callModalTableView,btnCancelcallModal;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma view lifecycle
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:TRUE];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:TRUE];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:FALSE];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:TRUE];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                             style:UIBarButtonItemStylePlain
                                                            target:nil
                                                            action:nil];
    [[self navigationItem] setBackBarButtonItem:back];
    
    [self setUI];

    galleryArray = [[NSMutableArray alloc] init];
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    viewTap.numberOfTapsRequired = 1;
    viewTap.cancelsTouchesInView = NO;
    [postimageView  addGestureRecognizer:viewTap];
    
    //[self addHUDView];
    
    
    if ([storeName length] == 0) {
        storeName = @"";
    }
    
    likepostBtn.hidden = FALSE;
    [likepostBtn setUserInteractionEnabled:TRUE];
    
    if ([postlikeType isEqualToString:@"0"]) {
        [likepostBtn setSelected:FALSE];
    } else {
        [likepostBtn setSelected:TRUE];
    }
    titlelbl.text = self.postTitle;
    if (self.postDescription != nil && ![self.postDescription isEqualToString:@""]) {
        descriptionlbl.text = self.postDescription;
    }else{
      //  descriptionlbl.text = DESCRIPTION_PLACEHOLDER_TEXT;
    }
    DebugLog(@"imageurl %@",postimageUrl);
    if(postimageUrl != nil && ![postimageUrl isEqualToString:@""])
        [postimageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(postimageUrl)] placeholderImage:[UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"]];
    
    [self adjustViewsAccordingToViewComeFrom];
    [callModalTableView reloadData];
    [callModalTableView sizeToFit];
    if ([IN_APP_DELEGATE networkavailable]) {
        [self sendGetBroadcastDetailsRequest];
    }
//    else{
//        UIAlertView *errorView = [[UIAlertView alloc]
//                                  initWithTitle:@"No Network Connection"
//                                  message:@"Please check your internet connection and try again."
//                                  delegate:nil
//                                  cancelButtonTitle:@"OK"
//                                  otherButtonTitles:nil];
//        [errorView show];
//    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)dealloc {
//    [self removeHUDView];
//}

- (void)viewDidUnload {
    [self setTitlelbl:nil];
    [self setPostView:nil];
    [self setDescriptionlbl:nil];
    [self setPostimageView:nil];
    [self setLikepostBtn:nil];
    [self setSharepostBtn:nil];
    [self setScrollView:nil];
    [self setLblFav:nil];
    [self setLblShare:nil];
    [self setBuyBtn:nil];
    [self setGotoStoreBtn:nil];
    [self setSharepostBtnImageView:nil];
    [self setLikepostBtnImageView:nil];
    [self setAddStoreToMyShoplocalBtn:nil];
    [self setBuyBtnBackView:nil];
    [super viewDidUnload];
}

#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma internal methods
- (void)tapDetected:(UIGestureRecognizer *)sender {
    DebugLog(@"-------------------------");
    if(postimageUrl != nil && ![postimageUrl isEqualToString:@""])
    {
        _photos = [[NSMutableArray alloc] init];
        MWPhoto *photo;
        photo = [[MWPhoto alloc] initWithURL:[NSURL URLWithString:THUMBNAIL_LINK(postimageUrl)]];
        if (postimageTitle != nil && ![postimageTitle isEqualToString:@""]) {
            photo.caption = postimageTitle;
        }
        [_photos addObject:photo];
        
//        MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
//        browser.displayActionButton = YES;
//        browser.wantsFullScreenLayout = YES;
//        [browser setInitialPageIndex:0];
//        [self.navigationController pushViewController:browser animated:YES];
        // Create browser
        MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
        // Set options
        browser.displayActionButton = YES; // Show action button to allow sharing, copying, etc (defaults to YES)
        browser.wantsFullScreenLayout = YES; // iOS 5 & 6 only: Decide if you want the photo browser full screen, i.e. whether the status bar is affected (defaults to YES)
        
        browser.displayNavArrows = YES; // Whether to display left and right nav arrows on toolbar (defaults to NO)
        browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
        browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
        browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
        browser.enableGrid = NO; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
        browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
        
        // Optionally set the current visible photo before displaying
        [browser setCurrentPhotoIndex:0];
        
        // Present
        [self.navigationController pushViewController:browser animated:YES];
        [INEventLogger logEvent:@"OfferDetails_ImageViewed"];
    }
}
//-(void)adjustViewsAccordingToViewComeFrom{
//    
//    CGSize titleSize = [postTitle sizeWithFont:titlelbl.font constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
//    
//    CGSize descriptionSize = [postDescription sizeWithFont:descriptionlbl.font constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
//    
//    [titlelbl setFrame:CGRectMake(titlelbl.frame.origin.x,
//                                  titlelbl.frame.origin.y,
//                                  titlelbl.frame.size.width,
//                                  titleSize.height + 30)];
//    
//    [imgSeperator1 setFrame:CGRectMake(imgSeperator1.frame.origin.x,
//                                       CGRectGetMaxY(titlelbl.frame),
//                                       imgSeperator1.frame.size.width,
//                                       imgSeperator1.frame.size.height)];
//    
//    [descriptionlbl setFrame:CGRectMake(descriptionlbl.frame.origin.x,
//                                        CGRectGetMaxY(imgSeperator1.frame),
//                                        descriptionlbl.frame.size.width,
//                                        descriptionSize.height + 40)];
//    
//    [imgSeperator2 setFrame:CGRectMake(imgSeperator2.frame.origin.x,
//                                       CGRectGetMaxY(descriptionlbl.frame),
//                                       imgSeperator2.frame.size.width,
//                                       imgSeperator2.frame.size.height)];
//    
//    [postimageView setFrame:CGRectMake(postimageView.frame.origin.x,
//                                       CGRectGetMaxY(descriptionlbl.frame),
//                                       postimageView.frame.size.width,
//                                       postimageView.frame.size.height)];
//
//    if(postimageUrl == nil || [postimageUrl isEqualToString:@""])
//    {
//        postimageView.hidden = YES;
//        imgSeperator2.hidden = YES;
//        [postView setFrame:CGRectMake(postView.frame.origin.x,
//                                      postView.frame.origin.y,
//                                      postView.frame.size.width,
//                                      CGRectGetMaxY(descriptionlbl.frame)+10)];
//    }else{
//        postimageView.hidden = NO;
//        imgSeperator2.hidden = NO;
//        [postView setFrame:CGRectMake(postView.frame.origin.x,
//                                      postView.frame.origin.y,
//                                      postView.frame.size.width,
//                                      CGRectGetMaxY(postimageView.frame))];
//    }
//
//    if (viewOpenedFrom == FROM_STORE_DETAILS) {
//        [gotoStoreBtn setHidden:YES];
//        [addStoreToMyShoplocalBtn setFrame:CGRectMake(addStoreToMyShoplocalBtn.frame.origin.x,
//                                         CGRectGetMaxY(postView.frame)+10,
//                                         addStoreToMyShoplocalBtn.frame.size.width,
//                                         addStoreToMyShoplocalBtn.frame.size.height)];
//    }else{
//        [gotoStoreBtn setFrame:CGRectMake(gotoStoreBtn.frame.origin.x,
//                                                      CGRectGetMaxY(postView.frame)+10,
//                                                      gotoStoreBtn.frame.size.width,
//                                                      gotoStoreBtn.frame.size.height)];
//        
//        [addStoreToMyShoplocalBtn setFrame:CGRectMake(addStoreToMyShoplocalBtn.frame.origin.x,
//                                                      CGRectGetMaxY(gotoStoreBtn.frame)+10,
//                                                      addStoreToMyShoplocalBtn.frame.size.width,
//                                                      addStoreToMyShoplocalBtn.frame.size.height)];
//    }
//    
//    [likepostBtn setFrame:CGRectMake(likepostBtn.frame.origin.x,
//                                     CGRectGetMaxY(addStoreToMyShoplocalBtn.frame)+10,
//                                     likepostBtn.frame.size.width,
//                                     likepostBtn.frame.size.height)];
//    
//    [sharepostBtn setFrame:CGRectMake(sharepostBtn.frame.origin.x,
//                                      CGRectGetMaxY(addStoreToMyShoplocalBtn.frame)+10,
//                                      sharepostBtn.frame.size.width,
//                                      sharepostBtn.frame.size.height)];
//    
//    [likepostBtnImageView setFrame:CGRectMake(likepostBtnImageView.frame.origin.x,
//                                              likepostBtn.frame.origin.y+8,
//                                              likepostBtnImageView.frame.size.width,
//                                              likepostBtnImageView.frame.size.height)];
//    
//    [sharepostBtnImageView setFrame:CGRectMake(sharepostBtnImageView.frame.origin.x,
//                                               sharepostBtn.frame.origin.y+8,
//                                               sharepostBtnImageView.frame.size.width,
//                                               sharepostBtnImageView.frame.size.height)];
//    
//    [lblFav setFrame:CGRectMake(lblFav.frame.origin.x,
//                                likepostBtn.frame.origin.y+6,
//                                lblFav.frame.size.width,
//                                lblFav.frame.size.height)];
//    
//    [lblShare setFrame:CGRectMake(lblShare.frame.origin.x,
//                                  sharepostBtn.frame.origin.y+6,
//                                  lblShare.frame.size.width,
//                                  lblShare.frame.size.height)];
//    
//    if (SHOW_BUY_BTN) {
//        [buyBtnBackView setHidden:NO];
//        scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, CGRectGetMaxY(sharepostBtn.frame)+CGRectGetHeight(buyBtnBackView.frame)+20);
//    }else{
//        [buyBtnBackView setHidden:YES];
//        scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, CGRectGetMaxY(sharepostBtn.frame)+20);
//    }
//}

-(void)adjustViewsAccordingToViewComeFrom{
   // [UIView animateWithDuration:0.2 animations:^(void){
        CGSize titleSize = [postTitle sizeWithFont:titlelbl.font constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
        
        CGSize descriptionSize = [postDescription sizeWithFont:descriptionlbl.font constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
        
        [titlelbl setFrame:CGRectMake(titlelbl.frame.origin.x,
                                      titlelbl.frame.origin.y,
                                      titlelbl.frame.size.width,
                                      titleSize.height + 30)];
        
        [imgSeperator1 setFrame:CGRectMake(imgSeperator1.frame.origin.x,
                                           CGRectGetMaxY(titlelbl.frame),
                                           imgSeperator1.frame.size.width,
                                           imgSeperator1.frame.size.height)];
        
        double nextYaxis = CGRectGetMaxY(imgSeperator1.frame);
        if(postDescription == nil || [[postDescription stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0)
        {
            descriptionlbl.hidden = YES;
            imgSeperator2.hidden = YES;
        }else{
            descriptionlbl.hidden = NO;
            imgSeperator2.hidden = NO;
            descriptionlbl.text = postDescription;
            [descriptionlbl setFrame:CGRectMake(descriptionlbl.frame.origin.x,
                                                CGRectGetMaxY(imgSeperator1.frame),
                                                descriptionlbl.frame.size.width,
                                                descriptionSize.height + 40)];
            [imgSeperator2 setFrame:CGRectMake(imgSeperator2.frame.origin.x,
                                               CGRectGetMaxY(descriptionlbl.frame),
                                               imgSeperator2.frame.size.width,
                                               imgSeperator2.frame.size.height)];
            nextYaxis = CGRectGetMaxY(descriptionlbl.frame);
        }
       
        if(postimageUrl == nil || [postimageUrl isEqualToString:@""])
        {
            postimageView.hidden = YES;
        }else{
            if (postDescription == nil || [[postDescription stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0) {
                imgSeperator1.hidden = YES;
                nextYaxis = CGRectGetMaxY(titlelbl.frame);
            }
            postimageView.hidden = NO;
            [postimageView setFrame:CGRectMake(postimageView.frame.origin.x,
                                               nextYaxis+postView.frame.origin.y,
                                               postimageView.frame.size.width,
                                               postimageView.frame.size.height)];
            nextYaxis = CGRectGetMaxY(postimageView.frame)  - postView.frame.origin.y;
        }
    
        //Adjust lblOfferDate
        [imgSeperator3 setHidden:YES];
        [lblOfferDate setHidden:YES];
        if (postIsOffered && postOfferDateTime != nil && ![postOfferDateTime isEqualToString:@""]) {
            NSString *postOfferFormatString = [self formatOfferDateTime:postOfferDateTime];
            if ([postOfferFormatString length] > 0)
            {
                [imgSeperator3 setHidden:NO];
                [lblOfferDate setHidden:NO];
                lblOfferDate.text =  [NSString stringWithFormat:@"Offer Valid Till: %@",postOfferFormatString];
                CGSize postOfferDateTimeSize = [postOfferDateTime sizeWithFont:lblOfferDate.font constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
                
                [lblOfferDate setFrame:CGRectMake(lblOfferDate.frame.origin.x,
                                                 nextYaxis,
                                                 lblOfferDate.frame.size.width,
                                                 postOfferDateTimeSize.height + 30)];
                
                [imgSeperator3 setFrame:CGRectMake(imgSeperator3.frame.origin.x,
                                                   CGRectGetMaxY(lblOfferDate.frame),
                                                   imgSeperator3.frame.size.width,
                                                   imgSeperator3.frame.size.height)];
                nextYaxis  = CGRectGetMaxY(imgSeperator3.frame);
            }
        }
    
        //Adjust lblPostedOn
        if (postDateTime != nil && ![postDateTime isEqualToString:@""]) {
            NSString *postOfferFormatString = [self formatOfferDateTime:postDateTime];
            if ([postOfferFormatString length] > 0)
            {
                lblPostedOn.text =  [NSString stringWithFormat:@" %@",postOfferFormatString];
                CGSize postDateSize = [postDateTime sizeWithFont:lblPostedOn.font constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
                [lblPostedOn setFrame:CGRectMake(lblPostedOn.frame.origin.x,
                                                     nextYaxis+3,
                                                     lblPostedOn.frame.size.width,
                                                     postDateSize.height+5)];
                nextYaxis  = CGRectGetMaxY(lblPostedOn.frame);
            }
        }
        [postView setFrame:CGRectMake(postView.frame.origin.x,
                                      postView.frame.origin.y,
                                      postView.frame.size.width,
                                      nextYaxis+10)];
    
        double next = 0;
        if (viewOpenedFrom == FROM_STORE_DETAILS) {
            [gotoStoreBtn setHidden:YES];
            [addStoreToMyShoplocalBtn setFrame:CGRectMake(addStoreToMyShoplocalBtn.frame.origin.x,
                                                          CGRectGetMaxY(postView.frame)+10,
                                                          addStoreToMyShoplocalBtn.frame.size.width,
                                                          addStoreToMyShoplocalBtn.frame.size.height)];
            next = CGRectGetMaxY(postView.frame)+10;
        }else{
            [gotoStoreBtn setFrame:CGRectMake(gotoStoreBtn.frame.origin.x,
                                              CGRectGetMaxY(postView.frame)+10,
                                              gotoStoreBtn.frame.size.width,
                                              gotoStoreBtn.frame.size.height)];
            
            [addStoreToMyShoplocalBtn setFrame:CGRectMake(addStoreToMyShoplocalBtn.frame.origin.x,
                                                          CGRectGetMaxY(gotoStoreBtn.frame)+10,
                                                          addStoreToMyShoplocalBtn.frame.size.width,
                                                          addStoreToMyShoplocalBtn.frame.size.height)];
            next = CGRectGetMaxY(gotoStoreBtn.frame)+10;
        }
        
        [likepostBtn setFrame:CGRectMake(likepostBtn.frame.origin.x,
                                         //CGRectGetMaxY(addStoreToMyShoplocalBtn.frame)+10,
                                         next,
                                         likepostBtn.frame.size.width,
                                         likepostBtn.frame.size.height)];
        
        [sharepostBtn setFrame:CGRectMake(sharepostBtn.frame.origin.x,
                                        //  CGRectGetMaxY(addStoreToMyShoplocalBtn.frame)+10,
                                          next,
                                          sharepostBtn.frame.size.width,
                                          sharepostBtn.frame.size.height)];
        
        [likepostBtnImageView setFrame:CGRectMake(likepostBtnImageView.frame.origin.x,
                                                  likepostBtn.frame.origin.y+3,
                                                  likepostBtnImageView.frame.size.width,
                                                  likepostBtnImageView.frame.size.height)];
        
        [sharepostBtnImageView setFrame:CGRectMake(sharepostBtnImageView.frame.origin.x,
                                                   sharepostBtn.frame.origin.y+3,
                                                   sharepostBtnImageView.frame.size.width,
                                                   sharepostBtnImageView.frame.size.height)];
        
        [lblFav setFrame:CGRectMake(lblFav.frame.origin.x,
                                    likepostBtn.frame.origin.y+6,
                                    lblFav.frame.size.width,
                                    lblFav.frame.size.height)];
        
        [lblShare setFrame:CGRectMake(lblShare.frame.origin.x,
                                      sharepostBtn.frame.origin.y+6,
                                      lblShare.frame.size.width,
                                      lblShare.frame.size.height)];
        
        if (SHOW_BUY_BTN) {
            [buyBtnBackView setHidden:NO];
            scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, CGRectGetMaxY(sharepostBtn.frame)+CGRectGetHeight(buyBtnBackView.frame)+20);
        }else{
            [buyBtnBackView setHidden:YES];
            scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, CGRectGetMaxY(sharepostBtn.frame)+20);
        }
        
  //  }];
}

-(void)setUI
{
    //postView = [CommonCallback setViewPropertiesWithRoundedCorner:postView];
//    postView.layer.shadowColor = [UIColor blackColor].CGColor;
//    postView.layer.shadowOffset = CGSizeMake(0, 0);
//    postView.layer.shadowRadius = 5;
//    postView.layer.shadowOpacity = 0.8;

    postView.backgroundColor =  [UIColor whiteColor];
    postView.clipsToBounds = YES;
    postimageView.backgroundColor   = [UIColor clearColor];

    titlelbl.font                   =   DEFAULT_BOLD_FONT(18.0);
    titlelbl.textColor              =   BROWN_COLOR;
    titlelbl.numberOfLines          =   0;
    titlelbl.textAlignment          =   NSTextAlignmentCenter;
    titlelbl.backgroundColor            =   [UIColor clearColor];
    
    descriptionlbl.backgroundColor      =   [UIColor clearColor];
    descriptionlbl.font                 =   DEFAULT_FONT(16.0);
    descriptionlbl.textColor            =   BROWN_COLOR;
    descriptionlbl.numberOfLines        =   0;
    descriptionlbl.textAlignment        =   NSTextAlignmentLeft;
    
    lblPostedOn.font                   =   DEFAULT_FONT(12.0);
    lblPostedOn.textColor              =   BROWN_COLOR;
    lblPostedOn.numberOfLines          =   0;
    lblPostedOn.textAlignment          =   NSTextAlignmentLeft;
    lblPostedOn.backgroundColor        =   [UIColor clearColor];
    
    lblOfferDate.font                   =   DEFAULT_BOLD_FONT(15.0);
    lblOfferDate.textColor              =   BROWN_COLOR;
    lblOfferDate.numberOfLines          =   0;
    lblOfferDate.textAlignment          =   NSTextAlignmentLeft;
    lblOfferDate.backgroundColor        =   [UIColor clearColor];
    
    for (UIButton *btn in buttonCollection) {
        
        btn.backgroundColor     =     [UIColor whiteColor];
//        btn.layer.borderColor   =     BUTTON_BORDER_COLOR.CGColor;
//        btn.layer.borderWidth   =     2.0;
//        btn.layer.cornerRadius  =     5.0;
        [btn setTitleColor:BUTTON_COLOR forState:UIControlStateNormal];
        [btn setTitleColor:BUTTON_OFFWHITE_COLOR forState:UIControlStateHighlighted];
        
//        btn.layer.shadowColor = [UIColor blackColor].CGColor;
//        btn.layer.shadowOffset = CGSizeMake(0, 0);
//        btn.layer.shadowRadius = 2;
//        btn.layer.shadowOpacity = 0.8;
    }

    
    lblFav.font     = DEFAULT_FONT(16);
    lblShare.font   = DEFAULT_FONT(16);
    
    lblFav.textAlignment     = NSTextAlignmentCenter;
    lblShare.textAlignment   = NSTextAlignmentCenter;
    
    lblFav.backgroundColor      =   [UIColor clearColor];
    lblShare.backgroundColor    =   [UIColor clearColor];
    
    lblFav.textColor    =  BROWN_COLOR;
    lblShare.textColor  =  BROWN_COLOR;
    
    if ([likescountString isEqualToString:@""] || likescountString == nil) {
        lblFav.text     = @"0";
    }else{
        lblFav.text     = likescountString;
    }
    
    if ([sharecountString isEqualToString:@""] || sharecountString == nil) {
        lblShare.text     = @"0";
    }else{
        lblShare.text   = sharecountString;
    }
    
    buyBtnBackView.backgroundColor = [UIColor colorWithRed:113/255.0 green:91/255.0  blue:87/255.0 alpha:0.9];

    buyBtn.titleLabel.font      =   DEFAULT_BOLD_FONT(25.0);
    [buyBtn setTitle:@"Call" forState:UIControlStateNormal];
    [buyBtn setTitle:@"Call" forState:UIControlStateHighlighted];
    
    gotoStoreBtn.titleLabel.font    =   DEFAULT_BOLD_FONT(18.0);
    [gotoStoreBtn setTitle:@"Let's Go There" forState:UIControlStateNormal];
    [gotoStoreBtn setTitle:@"Let's Go There" forState:UIControlStateHighlighted];
    
    addStoreToMyShoplocalBtn.titleLabel.font    =   DEFAULT_BOLD_FONT(18.0);
    [addStoreToMyShoplocalBtn setTitle:@"Add to My Shoplocal" forState:UIControlStateNormal];
    [addStoreToMyShoplocalBtn setTitle:@"Add to My Shoplocal" forState:UIControlStateHighlighted];
    
    likepostBtn.titleLabel.font     =   DEFAULT_BOLD_FONT(18.0);
    likepostBtn.backgroundColor     =   BUTTON_COLOR;
    [likepostBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [likepostBtn setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.5] forState:UIControlStateHighlighted];
    
    sharepostBtn.titleLabel.font     =   DEFAULT_BOLD_FONT(18.0);
    sharepostBtn.backgroundColor     =   BUTTON_COLOR;
    [sharepostBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sharepostBtn setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.5] forState:UIControlStateHighlighted];
    
    //callModalView = [CommonCallback setViewPropertiesWithRoundedCorner:callModalView];
    callModalView.backgroundColor =  [UIColor whiteColor];
    callModalView.layer.shadowColor      = [UIColor blackColor].CGColor;
    callModalView.layer.shadowOffset     = CGSizeMake(1, 1);
    callModalView.layer.shadowOpacity    = 1.0;
    callModalView.layer.shadowRadius     = 10.0;
    [callModalViewBackView setHidden:YES];
    [callModalView setHidden:YES];
    
    lblcallModalHeader.textAlignment     = NSTextAlignmentCenter;
    lblcallModalHeader.backgroundColor   = [UIColor clearColor];
    lblcallModalHeader.font              = DEFAULT_BOLD_FONT(20);
    lblcallModalHeader.textColor         = BROWN_COLOR;
    //    lblcallModalHeader.adjustsFontSizeToFitWidth = YES;
    lblcallModalHeader.minimumScaleFactor = 15;
    lblcallModalHeader.text              = @"Select number";
    lblcallModalHeader.numberOfLines     = 2;
    
    lblcallModalHeaderSeperator.backgroundColor   = DEFAULT_COLOR;
    callModalTableView.backgroundColor = [UIColor whiteColor];
    if ([callModalTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [callModalTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    btnCancelcallModal.backgroundColor = [UIColor clearColor];
}

-(NSString *)formatOfferDateTime:(NSString *)offerdate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSDate *myDate = [[NSDate alloc] init];
    myDate = [dateFormatter dateFromString:offerdate];
    DebugLog(@"MyDate is: %@", myDate);
    
    NSDateFormatter *dayformatter = [[NSDateFormatter alloc] init];
    [dayformatter setDateFormat:@"dd MMM YYYY hh:mm a"];
    NSString *dayFromDate = [dayformatter stringFromDate:myDate];
    DebugLog(@"Myday is: %@", dayFromDate);
    
    return dayFromDate;
}

-(void)addHUDView
{
    hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:hud];
	hud.dimBackground = NO;
    hud.labelText = @"Loading";
	hud.detailsLabelText = @"Please wait ...";
    [hud hide:NO];
}

-(void)removeHUDView
{
    [hud removeFromSuperview];
    hud = nil;
}


-(void)showLoginAlert{
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Please Login to mark this post as favourite."];
    alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
    alertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
    
    [alertView addButtonWithTitle:@"LOGIN"
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alert) {
                              DebugLog(@"LOGIN Clicked");
                              [self showLoginModal];
                          }];
    [alertView addButtonWithTitle:@"NOT NOW"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alert) {
                              DebugLog(@"NOT NOW Clicked");
                          }];
    [alertView show];
}

- (IBAction)likepostBtnPressed:(UIButton *)sender {
    [UIView animateWithDuration:0.3/1.5 animations:^{
        sender.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            sender.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3/2 animations:^{
                sender.transform = CGAffineTransformIdentity;
            } completion:^(BOOL finished){
                if(![INUserDefaultOperations isCustomerLoggedIn])
                {
                    [self showLoginAlert];
                } else {
                    NSString *areaName = [IN_APP_DELEGATE getActivePlaceNameFromAreaTable];
                    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:@"OfferDetails",@"Source",areaName,@"AreaName", nil];
                    [INEventLogger logEvent:@"Offer_Like" withParams:params];
                    if([likepostBtn isSelected])
                    {
                        [self sendPostUnLikeRequest];
                    } else {
                        [self sendPostLikeRequest];
                        [self sendStoreLikeRequest];
//                        if ([[INUserDefaultOperations getCustomerLoginFrom] isEqualToString:FACEBOOKSERVER] && [INUserDefaultOperations isOpenGraphShareEnabled]) {
//                            SIAlertView *sialertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Do you want to share this on your Facebook timeline?"];
//                            sialertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
//                            sialertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
//                            [sialertView addButtonWithTitle:@"YES"
//                                                       type:SIAlertViewButtonTypeDestructive
//                                                    handler:^(SIAlertView *alert) {
//                                                        DebugLog(@"YES");
//                                                        [self sendPostLikeRequest];
//                                                        [self sendStoreLikeRequest];
//                                                        
//                                                        NSString *deepLinkingUrl = [FacebookOpenGraphAPI createDeepLinkWithActiveAreaName:[IN_APP_DELEGATE getActiveSlugFromAreaTable] storeName:storeName storeId:[NSString stringWithFormat:@"%d",placeId]];
//                                                        
//                                                        if (postimageUrl != nil && ![postimageUrl isEqualToString:@""]) {
//                                                            if ([postimageUrl hasPrefix:@"http"]) {
//                                                                [FacebookOpenGraphAPI likeContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:postimageUrl deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
//                                                            }else{
//                                                                [FacebookOpenGraphAPI likeContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:THUMBNAIL_LINK(postimageUrl) deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
//                                                            }
//                                                        }else{
//                                                            [FacebookOpenGraphAPI likeContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:@"" deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
//                                                        }
//                                                    }];
//                            [sialertView addButtonWithTitle:@"NOT NOW"
//                                                       type:SIAlertViewButtonTypeCancel
//                                                    handler:^(SIAlertView *alert) {
//                                                        DebugLog(@"NO");
//                                                        [self sendPostLikeRequest];
//                                                        [self sendStoreLikeRequest];
//                                                    }];
//                            [sialertView show];
//                            
//                        }else{
//                            [self sendPostLikeRequest];
//                            [self sendStoreLikeRequest];
//                        }
                    }
                }
            }];
        }];
    }];
}

- (IBAction)sharepostBtnPressed:(UIButton *)sender {
    [UIView animateWithDuration:0.3/1.5 animations:^{
        sender.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            sender.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3/2 animations:^{
                sender.transform = CGAffineTransformIdentity;
            } completion:^(BOOL finished){
                UIActionSheet *shareActionSheet = [[UIActionSheet alloc] initWithTitle:@"Share via" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"SMS",@"Email",@"WhatsApp",@"Facebook",@"Twitter", nil];
                [shareActionSheet showInView:self.view];
            }];
        }];
    }];
}

- (IBAction)buyBtnPressed:(UIButton *)sender {
    [UIView animateWithDuration:0.3/1.5 animations:^{
        sender.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            sender.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3/2 animations:^{
                sender.transform = CGAffineTransformIdentity;
            } completion:^(BOOL finished){
                
                [callModalTableView reloadData];
                if (callModalTableView.contentSize.height < 240) {
                    [callModalTableView setFrame:CGRectMake(callModalTableView.frame.origin.x, callModalTableView.frame.origin.y, callModalTableView.frame.size.width, callModalTableView.contentSize.height)];
                }else{
                    [callModalTableView setFrame:CGRectMake(callModalTableView.frame.origin.x, callModalTableView.frame.origin.y, callModalTableView.frame.size.width, 240)];
                }
                [callModalView setFrame:CGRectMake(callModalView.frame.origin.x, callModalView.frame.origin.y, callModalView.frame.size.width, callModalTableView.frame.size.height+85)];
                

                if (telArray != nil && telArray.count > 0) {
//                    [ActionSheetStringPicker showPickerWithTitle:@"Select number" rows:self.telArray initialSelection:0 target:self successAction:@selector(selectTel:element:) cancelAction:@selector(actionPickerCancelled:) origin:sender];
//                    NSString *areaName = [IN_APP_DELEGATE getActivePlaceNameFromAreaTable];
//                    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:@"OfferDetails",@"Source",areaName,@"AreaName", nil];
//                    [INEventLogger logEvent:@"StoreCall" withParams:params];
                    
                    if ([storeName length] == 0)
                    {
                        lblcallModalHeader.text =  @"Select number";
                    }
                    else {
                        lblcallModalHeader.text = storeName;
                    }
                    
                    [callModalViewBackView setHidden:NO];
                    [callModalView setHidden:NO];
                    [CommonCallback viewtransitionInCompletion:callModalView completion:^{
                        [CommonCallback viewtransitionOutCompletion:callModalView completion:nil];
                    }];
                    
                    NSDictionary *callParams = [NSDictionary dictionaryWithObjectsAndKeys:@"OfferDetails",@"Source",[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"AreaName",storeName,@"StoreName", nil];
                    [INEventLogger logEvent:@"StoreCall" withParams:callParams];
                    
                    [self sendStoreCallRequest:@"StoreCall"];
                }else{
                    [INUserDefaultOperations showSIAlertView:NO_CONTACT_MESSAGE];
                }
            }];
        }];
    }];
}

- (IBAction)gotoStoreBtnPressed:(UIButton *)sender {
    [UIView animateWithDuration:0.3/1.5 animations:^{
        sender.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            sender.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3/2 animations:^{
                sender.transform = CGAffineTransformIdentity;
            } completion:^(BOOL finished){
                INStoreDetailViewController *detailController = [[INStoreDetailViewController alloc] initWithNibName:@"INStoreDetailViewController" bundle:nil] ;
                detailController.title = storeName;
                detailController.storeId = placeId;
                [self.navigationController pushViewController:detailController animated:YES];
                [INEventLogger logEvent:@"OfferDetails_GotoStore"];
            }];
        }];
    }];
}

- (IBAction)addStoreToMyShoplocalBtnPressed:(UIButton *)sender {
    [UIView animateWithDuration:0.3/1.5 animations:^{
        sender.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            sender.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3/2 animations:^{
                sender.transform = CGAffineTransformIdentity;
            } completion:^(BOOL finished){
                if(![INUserDefaultOperations isCustomerLoggedIn])
                {
                    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Please LOGIN to add this Place to MyShoplocal."];
                    alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
                    alertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
                    
                    [alertView addButtonWithTitle:@"LOGIN"
                                             type:SIAlertViewButtonTypeDestructive
                                          handler:^(SIAlertView *alert) {
                                              DebugLog(@"LOGIN Clicked");
                                              [self showLoginModal];
                                          }];
                    [alertView addButtonWithTitle:@"NOT NOW"
                                             type:SIAlertViewButtonTypeCancel
                                          handler:^(SIAlertView *alert) {
                                              DebugLog(@"NOT NOW Clicked");
                                          }];
                    [alertView show];
                } else {
                    [INEventLogger logEvent:@"OfferDetails_StoreFavourited"];
                    [self sendStoreLikeRequest];
                }
            }];
        }];
    }];
}

- (IBAction)touch_down_BtnEventCallBack:(UIButton *)sender {
    sender.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9,0.9);
}


-(void)showLoginModal{
    INCustomerLoginViewController *custLoginController = [[INCustomerLoginViewController alloc] initWithNibName:@"INCustomerLoginViewController" bundle:nil] ;
    custLoginController.delegate = self;
    UINavigationController *loginnavBar=[[UINavigationController alloc]initWithRootViewController:custLoginController];
    UIButton *moreButton1 =  [UIButton buttonWithType:UIButtonTypeCustom];
    [moreButton1 setTitle:@"Cancel" forState:UIControlStateNormal];
    moreButton1.titleLabel.font = [UIFont systemFontOfSize:17.0];
    //[moreButton1 setBackgroundImage:[UIImage imageNamed:@"Transparent_Button.png"] forState:UIControlStateNormal];
    [moreButton1 addTarget:self action:@selector(dismissLoginView) forControlEvents:UIControlEventTouchUpInside];
    [moreButton1 setFrame:CGRectMake(250, 7, 70, 32)];
    [loginnavBar.navigationBar addSubview:moreButton1];
    [loginnavBar.navigationBar setTranslucent:NO];
    [self.navigationController presentViewController:loginnavBar animated:YES completion:nil];
}

-(void)dismissLoginView
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"Dismiss");
        if ([IN_APP_DELEGATE networkavailable]) {
            [self sendGetBroadcastDetailsRequest];
        }
    }];
}


#pragma mark - Actionsheet Implementation
- (void)selectTel:(NSNumber *)lselectedIndex element:(id)element {
    //may have originated from textField or barButtonItem, use an IBOutlet instead of element
    selectedTelNumberToCall = [[telArray objectAtIndex:[lselectedIndex intValue]] stringByReplacingOccurrencesOfString:@" " withString:@""];
    DebugLog(@"selectedTelNumberToCall %@",selectedTelNumberToCall);
    
    NSString *deviceType = [UIDevice currentDevice].model;
    if([deviceType isEqualToString:@"iPhone"])
    {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: [NSString stringWithFormat: @"Do you want to call %@ store number ?",selectedTelNumberToCall]
                                                          delegate: self
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"NO", @"YES", nil
                              ];
        [alert show];
        NSString *areaName = [IN_APP_DELEGATE getActivePlaceNameFromAreaTable];
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:@"OfferDetails",@"Source",areaName,@"AreaName", nil];
        [INEventLogger logEvent:@"StoreCallDone" withParams:params];
    }
    else
    {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: @"Calling functionality is not available in this device. "
                                                          delegate: nil
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"OK", nil
                              ];
        [ alert show ];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:@"YES"])
    {
        UIApplication *myApp = [UIApplication sharedApplication];
        DebugLog(@"selectedTelNumberToCall = -%@-",selectedTelNumberToCall);
        [myApp openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",selectedTelNumberToCall]]];
    }
}


-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSString *title = [actionSheet buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"SMS"])
    {
        [self smsShareClicked];
    }
    else if([title isEqualToString:@"Email"])
    {
        [self emailShareClicked];
    }else if([title isEqualToString:@"WhatsApp"])
    {
        [self whatsAppShareClicked];
    }
    else if([title isEqualToString:@"Facebook"])
    {
        [self facebookShareClicked];
    } else if([title isEqualToString:@"Twitter"])
    {
        [self twitterShareClicked];
    }
    if(![title isEqualToString:@"Cancel"])
    {
        NSString *areaName = [IN_APP_DELEGATE getActivePlaceNameFromAreaTable];
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:@"OfferDetails",@"Source",areaName,@"AreaName", nil];
        [INEventLogger logEvent:@"Offer_Share" withParams:params];
    }
}
-(void)smsShareClicked{
    if ([MFMessageComposeViewController canSendText]) {
        NSString *message = [self getPostDetailsMessageBody:@"sms"];
        MFMessageComposeViewController *messageComposer = [[MFMessageComposeViewController alloc] init];
        messageComposer.messageComposeDelegate = self;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            messageComposer.navigationBar.tintColor  = [UIColor whiteColor];
        }
        [messageComposer setBody:message];
        [messageComposer setRecipients:nil];
        [self presentViewController:messageComposer animated:YES completion:nil];
    }else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"You may not have set up sms service on your device.\nPlease check and try again."
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}
#pragma mark MFMessageComposeViewController Delegate Methods
-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
		case MessageComposeResultCancelled:
            DebugLog(@"SMS Cancelled");
			break;
		case MessageComposeResultFailed:
        {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Unknown Error\nSMS failed"
														   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
        }
			break;
		case MessageComposeResultSent:
        {
            DebugLog(@"SMS Sent");
        }
			break;
		default:
			break;
	}
    [self dismissViewControllerAnimated:YES completion:^{
        if (result == MessageComposeResultSent) {
            [self sendPostShareRequest:@"sms"];
        }
    }];
}

-(void)emailShareClicked{
    if ([MFMailComposeViewController canSendMail]) {
        NSString *message = [self getPostDetailsMessageBody:@"email"];
        MFMailComposeViewController *emailComposer = [[MFMailComposeViewController alloc] init];
        emailComposer.mailComposeDelegate = self;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            emailComposer.navigationBar.tintColor  = [UIColor whiteColor];
        }
        [emailComposer setToRecipients:nil];
        if (storeName != nil && ![storeName isEqualToString:@""]) {
            [emailComposer setSubject:[NSString stringWithFormat:@"%@",storeName]];
        }else{
            [emailComposer setSubject:@""];
        }
        if (postimageUrl != nil && ![postimageUrl isEqualToString:@""]) {
            NSString *htmlMessage = [message stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
            DebugLog(@"%@--%@",message,htmlMessage);
            [emailComposer setMessageBody:[NSString stringWithFormat:@"%@ <br><br><img src=%@ alt=%@/>",htmlMessage,THUMBNAIL_LINK(postimageUrl),postTitle] isHTML:YES];
        }else{
            [emailComposer setMessageBody:message isHTML:NO];
        }
        [self presentViewController:emailComposer animated:YES completion:nil];
    }else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"You may not have set up mail service on your device or You may not connected to internet.\nPlease check and try again."
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}
#pragma mark Mail Compose Delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    if (result == MFMailComposeResultFailed) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                        message:NSLocalizedString(@"Email failed to send. Please try again.", nil)
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", nil) otherButtonTitles:nil];
		[alert show];
    }
    [self dismissViewControllerAnimated:YES completion:^{
        if (result == MFMailComposeResultSent) {
            [self sendPostShareRequest:@"email"];
        }
    }];
}

-(void)whatsAppShareClicked{
    //    NSString *shareMSGBody = [[NSString stringWithFormat:@"Store Name: %@\nOffer: %@\nDescription: %@",storeName,postTitle,postDescription] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *message = [self getPostDetailsMessageBody:@"whatsapp"];
    NSString *urlString = [NSString stringWithFormat:@"whatsapp://send?text=%@",[message stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURL *whatsappURL = [NSURL URLWithString:urlString];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [[UIApplication sharedApplication] openURL: whatsappURL];
        [self sendPostShareRequest:@"whatsapp"];
    }
}

-(void)twitterShareClicked{
    if ([IN_APP_DELEGATE networkavailable]) {
        NSString *message = [self getPostDetailsMessageBody:@"twitter"];

        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
            
            SLComposeViewController *socialComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
            if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
            {
                if ([message length] > 140) {
                    [socialComposer setInitialText:[message substringToIndex:140]];
                }else{
                    [socialComposer setInitialText:message];
                }
            }
            else{
                [socialComposer setInitialText:message];
            }
            if(postimageUrl != nil && ![postimageUrl isEqualToString:@""])
            {
                UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:THUMBNAIL_LINK(postimageUrl)]]];
                [socialComposer addImage:image];
            }
            [socialComposer setCompletionHandler:^(SLComposeViewControllerResult result){
                switch (result) {
                    case SLComposeViewControllerResultCancelled:
                        DebugLog(@"SLComposeViewControllerResultCancelled");
                        break;
                    case SLComposeViewControllerResultDone:
                        DebugLog(@"SLComposeViewControllerResultDone");
                        [self sendPostShareRequest:@"twitter"];
                        break;
                    default:
                        DebugLog(@"SLComposeViewControllerResultFailed");
                        break;
                }
                [self dismissViewControllerAnimated:YES completion:NULL];
            }];
            [self presentViewController:socialComposer animated:YES completion:Nil];
        }
        else{
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:ALERT_TITLE
                                      message:@"You may not have set up twitter service on your device.Please check and try again."
                                      delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [alertView show];
        }
    }else{
        [INUserDefaultOperations showOfflineAlert];
    }
}

-(void)facebookShareClicked{
    if ([IN_APP_DELEGATE networkavailable]) {
        [self sendPostShareRequest:@"facebook"];

        NSString *message = [self getPostDetailsMessageBody:@"facebook"];
        DebugLog(@"message %@",message);
        NSString *deepLinkingUrl = [FacebookOpenGraphAPI createDeepLinkWithActiveAreaName:[IN_APP_DELEGATE getActiveSlugFromAreaTable] storeName:storeName storeId:[NSString stringWithFormat:@"%d",self.placeId]];
        
        if (postimageUrl != nil && ![postimageUrl isEqualToString:@""]) {
            if ([postimageUrl hasPrefix:@"http"]) {
                [FacebookShareAPI shareContentWithTitle:[NSString stringWithFormat:@"%@ - Offer\n",storeName] description:message image:postimageUrl deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
            }else{
                [FacebookShareAPI shareContentWithTitle:[NSString stringWithFormat:@"%@ - Offer\n",storeName] description:message image:THUMBNAIL_LINK(postimageUrl) deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
            }
        }else{
            [FacebookShareAPI shareContentWithTitle:[NSString stringWithFormat:@"%@ - Offer\n",storeName] description:message image:@"" deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
        }
    }else{
        [INUserDefaultOperations showOfflineAlert];
    }
    
//    if ([IN_APP_DELEGATE networkavailable]) {
//        NSString *message = [self getPostDetailsMessageBody:@"facebook"];
//        
//        if (SYSTEM_VERSION_LESS_THAN(@"6.0"))
//        {
//            FaceBookShareViewController *facebookViewComposer = [[FaceBookShareViewController alloc] initWithNibName:@"FaceBookShareViewController" bundle:nil];
//            facebookViewComposer.fbdelegate = self;
//            facebookViewComposer.title = @"FACEBOOK SHARE";
//            facebookViewComposer.FBtitle = ALERT_TITLE;
//            facebookViewComposer.postMessageText = message;
//            facebookViewComposer.FBtPic = THUMBNAIL_LINK(postimageUrl);
//            UINavigationController *fbnavBar=[[UINavigationController alloc]initWithRootViewController:facebookViewComposer];
//            [self.navigationController presentViewController:fbnavBar animated:YES completion:nil];
//        }else{
//            if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
//                
//                SLComposeViewController *socialComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
//                [socialComposer setInitialText:message];
//                if(postimageUrl != nil && ![postimageUrl isEqualToString:@""])
//                {
//                    //                    [socialComposer addURL:[NSURL URLWithString:THUMBNAIL_LINK(postimageUrl)]];
//                    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:THUMBNAIL_LINK(postimageUrl)]]];
//                    [socialComposer addImage:image];
//                }
//                [socialComposer setCompletionHandler:^(SLComposeViewControllerResult result){
//                    switch (result) {
//                        case SLComposeViewControllerResultCancelled:
//                            DebugLog(@"SLComposeViewControllerResultCancelled");
//                            break;
//                        case SLComposeViewControllerResultDone:
//                            DebugLog(@"SLComposeViewControllerResultDone");
//                            [self sendPostShareRequest:@"facebook"];
//                            break;
//                        default:
//                            DebugLog(@"SLComposeViewControllerResultFailed");
//                            break;
//                    }
//                    [self dismissViewControllerAnimated:YES completion:NULL];
//                }];
//                [self presentViewController:socialComposer animated:YES completion:Nil];
//            }
//            else{
//                UIAlertView *alertView = [[UIAlertView alloc]
//                                          initWithTitle:ALERT_TITLE
//                                          message:@"You may not have set up facebook service on your device.Please check and try again."
//                                          delegate:nil
//                                          cancelButtonTitle:@"OK"
//                                          otherButtonTitles:nil];
//                [alertView show];
//            }
//        }
//    }else{
//        [INUserDefaultOperations showOfflineAlert];
//    }
}

-(void)faceBookCompletionCallBack:(int)result
{
    DebugLog(@"Customer : faceBookCompletionCallBack %d",result);
    if (result == FACEBOOK_POST_SUCCESS) {
        [self sendPostShareRequest:@"facebook"];
    }
}



#pragma AFNetworking delegate methods
-(void)sendPostLikeRequest
{
    DebugLog(@"========================sendPostLikeRequest========================");
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[INUserDefaultOperations getCustomerAuthId] forKey:@"user_id"];
    [params setObject:[INUserDefaultOperations getCustomerAuthCode] forKey:@"auth_id"];
    [params setObject:[NSString stringWithFormat:@"%d",self.postId] forKey:@"post_id"];
    [httpClient postPath:LIKE_BROADCAST_OF_STORE parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            if([json  objectForKey:@"message"] != [NSNull null] && [[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
                likescountString = [NSString stringWithFormat:@"%d",[likescountString intValue] + 1];
                lblFav.text = likescountString;
                [likepostBtn setSelected:TRUE];
//                if([json  objectForKey:@"message"] != [NSNull null] && ![[json objectForKey:@"message"] isEqualToString:@""]){
//                    NSString *message = [json objectForKey:@"message"];
//                    [INUserDefaultOperations showSIAlertView:message];
//                }
            } else {
                NSString* code = [json objectForKey:@"code"];
                if([code isEqualToString:CUSTOMER_ALREADY_MADE_POST_FAV])
                {
                    [likepostBtn setSelected:TRUE];
                    if ([likescountString intValue] == 0) {
                        likescountString = @"1";
                        lblFav.text = likescountString;
                    }
//                    if([json  objectForKey:@"message"] != [NSNull null] && ![[json objectForKey:@"message"] isEqualToString:@""]){
//                        NSString *message = [json objectForKey:@"message"];
//                        [INUserDefaultOperations showSIAlertView:message];
//                    }
                }else if([code isEqualToString:CUSTOMER_INVALID_USER_AUTH_CODE])
                {
                    if([INUserDefaultOperations isCustomerLoggedIn]){
                        [INUserDefaultOperations clearCustomerDetails];
                        NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:IN_CUSTOMER_LOGIN_LOGOUT_KEY];
                        [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_LOGIN_LOGOUT_NOTIFICATION object:nil userInfo:dictionary];
                        [self showLoginAlert];
                    }
                }else{
                    if([json  objectForKey:@"message"] != [NSNull null] && ![[json objectForKey:@"message"] isEqualToString:@""]){
                        NSString *message = [json objectForKey:@"message"];
                        [INUserDefaultOperations showSIAlertView:message];
                    }
                }
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}

-(void)sendPostUnLikeRequest
{
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    [httpClient setDefaultHeader:@"user_id" value:[INUserDefaultOperations getCustomerAuthId]];
    [httpClient setDefaultHeader:@"auth_id" value:[INUserDefaultOperations getCustomerAuthCode]];
    [httpClient deletePath:UNLIKE_BROADCAST_OF_STORE(self.postId) parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            if([json  objectForKey:@"message"] != [NSNull null] && [[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
                [likepostBtn setSelected:FALSE];
                if ([likescountString intValue] > 0) {
                    likescountString = [NSString stringWithFormat:@"%d",[likescountString intValue] - 1];
                    lblFav.text = likescountString;
                }
//                if([json  objectForKey:@"message"] != [NSNull null] && ![[json objectForKey:@"message"] isEqualToString:@""]){
//                    NSString *message = [json objectForKey:@"message"];
//                    [INUserDefaultOperations showSIAlertView:message];
//                }
            } else {
                NSString* code = [json objectForKey:@"code"];
                if([code isEqualToString:CUSTOMER_INVALID_USER_AUTH_CODE])
                {
                    if([INUserDefaultOperations isCustomerLoggedIn]){
                        [INUserDefaultOperations clearCustomerDetails];
                        NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:IN_CUSTOMER_LOGIN_LOGOUT_KEY];
                        [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_LOGIN_LOGOUT_NOTIFICATION object:nil userInfo:dictionary];
                        [self showLoginAlert];
                    }
                }else{
                    if([json  objectForKey:@"message"] != [NSNull null] && ![[json objectForKey:@"message"] isEqualToString:@""]){
                        NSString *message = [json objectForKey:@"message"];
                        [INUserDefaultOperations showSIAlertView:message];
                    }
                }
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}

-(void)sendPostShareRequest:(NSString *)viaString{
    if ([[INUserDefaultOperations getCustomerLoginFrom] isEqualToString:FACEBOOKSERVER] && [INUserDefaultOperations isOpenGraphShareEnabled] && ![viaString isEqualToString:@"facebook"]) {
        SIAlertView *sialertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Do you want to share this on your Facebook timeline?"];
        sialertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
        sialertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
        [sialertView addButtonWithTitle:@"YES"
                                   type:SIAlertViewButtonTypeDestructive
                                handler:^(SIAlertView *alert) {
                                    DebugLog(@"YES");
                                    NSString *deepLinkingUrl = [FacebookOpenGraphAPI createDeepLinkWithActiveAreaName:[IN_APP_DELEGATE getActiveSlugFromAreaTable] storeName:storeName storeId:[NSString stringWithFormat:@"%d",placeId]];
                                    
                                    if (postimageUrl != nil && ![postimageUrl isEqualToString:@""]) {
                                        if ([postimageUrl hasPrefix:@"http"]) {
                                            [FacebookOpenGraphAPI shareContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:postimageUrl deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
                                        }else{
                                            [FacebookOpenGraphAPI shareContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:THUMBNAIL_LINK(postimageUrl) deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
                                        }
                                    }else{
                                        [FacebookOpenGraphAPI shareContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:@"" deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
                                    }
                                }];
        [sialertView addButtonWithTitle:@"NOT NOW"
                                   type:SIAlertViewButtonTypeCancel
                                handler:^(SIAlertView *alert) {
                                    DebugLog(@"NO");
                                }];
        [sialertView show];
    }
    
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
//    if ([INUserDefaultOperations isCustomerLoggedIn]) {
//        [params setObject:[INUserDefaultOperations getCustomerAuthId] forKey:@"user_id"];
//        [params setObject:[INUserDefaultOperations getCustomerAuthCode] forKey:@"auth_id"];
//    }
    [params setObject:[NSString stringWithFormat:@"%d",self.postId] forKey:@"post_id"];
    [params setObject:viaString forKey:@"via"];
    
    [httpClient postPath:SHARE_STORE_POST parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
//            NSString* message = [json objectForKey:@"message"];
//            [INUserDefaultOperations showAlert:message];
            sharecountString = [NSString stringWithFormat:@"%d",[sharecountString intValue] + 1];
            lblShare.text = sharecountString;
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}

-(void)sendGetBroadcastDetailsRequest
{
    DebugLog(@"========================sendGetPlaceDetailsRequest========================");
    DebugLog(@"isCustomerLoggedIn %d",[INUserDefaultOperations isCustomerLoggedIn]);
    NSURL *url = nil;
    if ([INUserDefaultOperations isCustomerLoggedIn]) {
        url = [NSURL URLWithString:BROADCAST_DETAIL_WITH_USERID(self.postId,[INUserDefaultOperations getCustomerId])];
    }else{
        url = [NSURL URLWithString:BROADCAST_DETAIL(self.postId)];
    }
    DebugLog(@"%@",url);
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"Response -%@-", self.splashJson);
                                                        if(self.splashJson  != nil) {
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSDictionary* maindict = [self.splashJson  objectForKey:@"data"];
                                                                
                                                                postTitle = [maindict objectForKey:@"title"];
                                                                postDescription = [maindict objectForKey:@"description"];
                                                                
                                                                postimageUrl =  [maindict objectForKey:@"image_url1"];
                                                                postimageTitle = [maindict objectForKey:@"image_title1"];
                                                                
                                                                likescountString = [maindict objectForKey:@"total_like"];
                                                                sharecountString = [maindict objectForKey:@"total_share"];
                                                                
                                                                postIsOffered       = [maindict objectForKey:@"is_offered"];
                                                                postOfferDateTime   = [maindict objectForKey:@"offer_date_time"];

                                                                postDateTime  = [maindict objectForKey:@"date"];

                                                                postlikeType    = [maindict objectForKey:@"user_like"];
                                                                DebugLog(@"postliketype %@",postlikeType);
                                                                if ([postlikeType isEqualToString:@"0"]) {
                                                                    [likepostBtn setSelected:FALSE];
                                                                } else {
                                                                    [likepostBtn setSelected:TRUE];
                                                                }
                                                                
                                                                DebugLog(@"custdetail: imageurl %@",postimageUrl);
                                                                if(postimageUrl != nil && ![postimageUrl isEqualToString:@""]){
                                                                    [postimageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(postimageUrl)] placeholderImage:[UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"]];
                                                                    DebugLog(@"custdetail: imageurl %@",THUMBNAIL_LINK(postimageUrl));
                                                                }
                                                                
                                                                if ([likescountString isEqualToString:@""] || likescountString == nil) {
                                                                    lblFav.text     = @"0";
                                                                }else{
                                                                    lblFav.text     = likescountString;
                                                                }
                                                                
                                                                if ([sharecountString isEqualToString:@""] || sharecountString == nil) {
                                                                    lblShare.text     = @"0";
                                                                }else{
                                                                    lblShare.text   = sharecountString;
                                                                }
                                                                
                                                                [self adjustViewsAccordingToViewComeFrom];
                                                            }
                                                        }
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                        [av show];
                                                    }];
    [operation start];
}


-(void)sendStoreLikeRequest
{
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[INUserDefaultOperations getCustomerAuthId] forKey:@"user_id"];
    [params setObject:[INUserDefaultOperations getCustomerAuthCode] forKey:@"auth_id"];
    [params setObject:[NSString stringWithFormat:@"%d",self.placeId] forKey:@"place_id"];
    [httpClient postPath:LIKE_STORE parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
//            if([json  objectForKey:@"message"] != [NSNull null] && [[json  objectForKey:@"success"] isEqualToString:@"true"])
//            {
//                NSString* message = [json objectForKey:@"message"];
//                [INUserDefaultOperations showAlert:message];
//            } else {
//                NSString* message = [json objectForKey:@"message"];
//                [INUserDefaultOperations showAlert:message];
//            }
            if([json  objectForKey:@"message"] != [NSNull null] && ![[json objectForKey:@"message"] isEqualToString:@""]){
                NSString *message = [json objectForKey:@"message"];
                [INUserDefaultOperations showSIAlertView:message];
                if ([[json  objectForKey:@"code"] isEqualToString:@"-134"]) {
                    [INUserDefaultOperations setCustomerFavouriteMyShoplocalRefresh:TRUE];
                }
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}


#pragma MWPhotoBrowser delegate methods
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser
{
    return [_photos count];
}
- (id<MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index
{
    return [_photos objectAtIndex:index];
}

-(NSString *)getPostDetailsMessageBody:(NSString *)shareVia{
    NSMutableString *constactString = [[NSMutableString alloc] initWithString:@""];
    if (telArray.count > 0) {
        DebugLog(@"telAray %@",telArray);
        [constactString  appendString:[NSString stringWithFormat:@"%@",[[[telArray reverseObjectEnumerator] allObjects]  componentsJoinedByString:@", "]]];
    }
   
    NSString *message = [IN_APP_DELEGATE getPostDetailsMessageBody:storeName offerTitle:postTitle offerDescription:postDescription isOffered:postIsOffered offerDateTime:postOfferDateTime contact:constactString shareVia:shareVia];
    return message;
}

//- (IBAction)postMessage
//{
//    
//    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
//    
//    ACAccountType *accountTypeFacebook =
//    [accountStore accountTypeWithAccountTypeIdentifier:
//     ACAccountTypeIdentifierFacebook];
//    
//    NSDictionary *options = @{
//                              ACFacebookAppIdKey: @"563824743654209",
//                              ACFacebookPermissionsKey: @[@"publish_stream",
//                                                          @"publish_actions"],
//                              ACFacebookAudienceKey: ACFacebookAudienceFriends
//                              };
//    
//    [accountStore requestAccessToAccountsWithType:accountTypeFacebook
//                                          options:options
//                                       completion:^(BOOL granted, NSError *error) {
//                                           
//                                           if(granted) {
//                                               
//                                               NSArray *accounts = [accountStore
//                                                                    accountsWithAccountType:accountTypeFacebook];
//                                               _facebookAccount = [accounts lastObject];
//                                               DebugLog(@"facebook account %@",_facebookAccount);
//                                               NSDictionary *parameters =
//                                               @{@"access_token":_facebookAccount.credential.oauthToken,
//                                                 @"message": @"My first iOS 7 Facebook posting"};
//                                               
//                                               NSURL *feedURL = [NSURL
//                                                                 URLWithString:@"https://graph.facebook.com/me/feed"];
//                                               
//                                               SLRequest *feedRequest =
//                                               [SLRequest
//                                                requestForServiceType:SLServiceTypeFacebook
//                                                requestMethod:SLRequestMethodPOST
//                                                URL:feedURL
//                                                parameters:parameters];
//                                               
//                                               [feedRequest 
//                                                performRequestWithHandler:^(NSData *responseData,
//                                                                            NSHTTPURLResponse *urlResponse, NSError *error)
//                                                {
//                                                    // Handle response
//                                                    if (error) {
//                                                        DebugLog(@"Request failed, %@",[urlResponse description]);
//                                                        DebugLog(@"errorString %@",[error localizedDescription]);
//                                                    }else{
//                                                        DebugLog(@"Post successful");
//                                                        NSString *dataString = [[NSString alloc] initWithData:responseData encoding:NSStringEncodingConversionAllowLossy];
//                                                        DebugLog(@"Response Data: %@", dataString);
//                                                    }
//                                                }];
//                                           } else {
//                                               DebugLog(@"Access Denied");
//                                               DebugLog(@"[%@]",[error localizedDescription]);
//                                           }
//                                       }];
//}


#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [telArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CallCell";
    UIImageView *thumbImg;
    UILabel *lblNumber;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backgroundColor = [UIColor whiteColor];

        thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(10,15, 50, 50)];
        thumbImg.tag = 1111;
        thumbImg.contentMode = UIViewContentModeScaleToFill;
        thumbImg.image = [UIImage imageNamed:@"call_brown.png"];
        [cell.contentView addSubview:thumbImg];
        
        lblNumber = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame),0, 210, 80)];
        lblNumber.text = @"";
        lblNumber.tag = 2222;
        lblNumber.textColor             = BROWN_COLOR;
        lblNumber.highlightedTextColor  = [UIColor whiteColor];
        lblNumber.font                  = DEFAULT_FONT(23);
        lblNumber.textAlignment         = NSTextAlignmentLeft;
        lblNumber.backgroundColor       = [UIColor clearColor];
        [cell.contentView addSubview:lblNumber];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = BROWN_OFFWHITE_COLOR;
        cell.selectedBackgroundView = bgColorView;
    }
    //cell.textLabel.text = [telArray objectAtIndex:indexPath.row];
    lblNumber        = (UILabel *)[cell.contentView viewWithTag:2222];
    lblNumber.text   = [telArray objectAtIndex:indexPath.row];
    return cell;
}

#pragma mark - Table view delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [callModalView setHidden:YES];
    [callModalViewBackView setHidden:YES];
    selectedTelNumberToCall = [[telArray objectAtIndex:indexPath.row] stringByReplacingOccurrencesOfString:@" " withString:@""];
    DebugLog(@"selectedTelNumberToCall %@",selectedTelNumberToCall);
    NSString *deviceType = [UIDevice currentDevice].model;
    if([deviceType isEqualToString:@"iPhone"])
    {
        UIApplication *myApp = [UIApplication sharedApplication];
        DebugLog(@"selectedTelNumberToCall = -%@-",selectedTelNumberToCall);
        [myApp openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",selectedTelNumberToCall]]];
        
        NSDictionary *callParams = [NSDictionary dictionaryWithObjectsAndKeys:@"OfferDetails",@"Source",[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"AreaName",storeName,@"StoreName",selectedTelNumberToCall,@"Number", nil];
        [INEventLogger logEvent:@"StoreCallDone" withParams:callParams];
        
        
        [self sendStoreCallRequest:@"StoreCallDone"];
    }
    else
    {
        UIAlertView *alert = [[ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: @"Calling functionality is not available in this device. "
                                                          delegate: nil
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"OK", nil];
        [alert show];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (IBAction)btnCancelcallModalPressed:(id)sender {
    [callModalView setHidden:YES];
    [callModalViewBackView setHidden:YES];
}


-(void)sendStoreCallRequest:(NSString *)callType
{
    
    if (![INUserDefaultOperations isCustomerLoggedIn]) {
        return;
    }
    
    NSString *areaID = [IN_APP_DELEGATE getActiveShoplocalPlaceIdFromAreaTable];

    DebugLog(@"========================sendStoreCallRequest========================");
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSString *register_from = [NSString stringWithFormat:@"%@ v%@",DEVICE_TYPE,version];
    
    NSMutableDictionary *postparams = [[NSMutableDictionary alloc] init];
//    {"user_id":"${user_id}","auth_id":"${auth_id}","place_id":"106","via":"welcome ","call_type":"type-a","active_area":"1"}
    [postparams setObject:[INUserDefaultOperations getCustomerAuthId] forKey:@"user_id"];
    [postparams setObject:[INUserDefaultOperations getCustomerAuthCode] forKey:@"auth_id"];
    [postparams setObject:[NSString stringWithFormat:@"%d",postId] forKey:@"post_id"];
    [postparams setObject:callType forKey:@"call_type"];
    [postparams setObject:areaID forKey:@"active_area"];
    [postparams setObject:register_from forKey:@"via"];

    DebugLog(@"sendStoreCallRequest : postParams -%@-",postparams);
    
    [httpClient postPath:CALL_STORE parameters:postparams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"sendStoreCallRequest : json afnetworking ->%@",json);
        if (json!= nil) {
            NSString* code = [json objectForKey:@"code"];
            if([code isEqualToString:CUSTOMER_INVALID_USER_AUTH_CODE])
            {
                if([INUserDefaultOperations isCustomerLoggedIn]){
                    [INUserDefaultOperations clearCustomerDetails];
                }
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"sendStoreCallRequest : [HTTPClient Error]: %@", error.localizedDescription);
    }];
}
@end
