//
//  ImageFilterViewController.m
//  shoplocal
//
//  Created by Kirti Nikam on 30/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "ImageFilterViewController.h"
#import "CommonCallback.h"
#import "UIImage+FiltrrCompositions.h"
#import "UIImage+Scale.h"

@interface ImageFilterViewController ()

@end

@implementation ImageFilterViewController
@synthesize originalImage;
@synthesize imageView;
@synthesize delegate;
@synthesize imageCropper;
@synthesize toolBarView;
@synthesize filterTableView;
@synthesize arrEffects;
@synthesize saveImageWithFilterBtn,cancelBtn1;
@synthesize saveBtn,cancelBtn,clockWiseRotateBtn,antiClockWiseRotateBtn;
@synthesize filterImageView;
@synthesize filterToolView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Edit Image";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:[UIColor darkGrayColor]];
    //thumbImage = [originalImage scaleToSize:CGSizeMake(320, 320)];
    //minithumbImage = [thumbImage scaleToSize:CGSizeMake(50, 50)];
    
    //self.imageCropper.hidden = FALSE;
    //self.imageView.hidden = FALSE;
    //toolBarView.hidden = FALSE;

    filterTableView.hidden = TRUE;
    filterTableView.hidden = TRUE;
    filterToolView.hidden = TRUE;
    filterImageView.hidden = TRUE;
    
    rotationInt = 0;
//    _imageCropper.hidden    = FALSE;
    imageView.hidden        = TRUE;
//
//    _imageCropper = [[NLImageCropperView alloc] initWithFrame:self.imageView.frame];
//    [self.view addSubview:_imageCropper];
//    [_imageCropper setImage:originalImage];
//    
//    if (originalImage.size.width > originalImage.size.height) {
//        [_imageCropper setCropRegionRect:CGRectMake(10, 10, originalImage.size.height - 20, originalImage.size.height - 20)];
//    }else{
//        [_imageCropper setCropRegionRect:CGRectMake(10, 10, originalImage.size.width - 20, originalImage.size.width - 20)];
//    }
    

    self.imageCropper = [[BJImageCropper alloc] initWithImage:originalImage andMaxSize:CGSizeMake(400, 400)];
    [self.view addSubview:self.imageCropper];
    self.imageCropper.center = self.imageView.center;
    self.imageCropper.imageView.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.imageCropper.imageView.layer.shadowRadius = 3.0f;
    self.imageCropper.imageView.layer.shadowOpacity = 0.8f;
    self.imageCropper.imageView.layer.shadowOffset = CGSizeMake(1, 1);
    
    imageView.image = originalImage;
    [self.view bringSubviewToFront:toolBarView];
    
    
//    arrEffects = [[NSMutableArray alloc] initWithObjects:
//                  [NSDictionary dictionaryWithObjectsAndKeys:@"Original",@"title",@"",@"method", nil],
//                  [NSDictionary dictionaryWithObjectsAndKeys:@"E1",@"title",@"e1",@"method", nil],
//                  [NSDictionary dictionaryWithObjectsAndKeys:@"E2",@"title",@"e2",@"method", nil],
//                  [NSDictionary dictionaryWithObjectsAndKeys:@"E3",@"title",@"e3",@"method", nil],
//                  [NSDictionary dictionaryWithObjectsAndKeys:@"E4",@"title",@"e4",@"method", nil],
//                  [NSDictionary dictionaryWithObjectsAndKeys:@"E5",@"title",@"e5",@"method", nil],
//                  [NSDictionary dictionaryWithObjectsAndKeys:@"E6",@"title",@"e6",@"method", nil],
//                  [NSDictionary dictionaryWithObjectsAndKeys:@"E7",@"title",@"e7",@"method", nil],
//                  [NSDictionary dictionaryWithObjectsAndKeys:@"E8",@"title",@"e8",@"method", nil],
////                  [NSDictionary dictionaryWithObjectsAndKeys:@"E9",@"title",@"e9",@"method", nil],
//                  [NSDictionary dictionaryWithObjectsAndKeys:@"E10",@"title",@"e10",@"method", nil],
//                  [NSDictionary dictionaryWithObjectsAndKeys:@"E11",@"title",@"e11",@"method", nil],
//                  nil];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setImageView:nil];
    [self setCancelBtn:nil];
    [self setSaveBtn:nil];
    [self setAntiClockWiseRotateBtn:nil];
    [self setClockWiseRotateBtn:nil];
    [self setCropBtn:nil];
    [self setToolBarView:nil];
    [self setFilterTableView:nil];
    [self setSaveImageWithFilterBtn:nil];
    [self setCancelBtn1:nil];
    [self setFilterImageView:nil];
    [self setFilterToolView:nil];
    [super viewDidUnload];
}

#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)cancelBtnClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)saveBtnClicked:(id)sender {
    /*
    _imageCropper.hidden = TRUE;
    imageView.hidden     = FALSE;
    
    UIImage *croppedImage = [_imageCropper getCroppedImage];
    self.imageView.image = [CommonCallback resizeImage:croppedImage newSize:CGSizeMake(160, 160)];
//    self.imageView.transform = _imageCropper.transform;
    
    DebugLog(@"rotationInt %d",rotationInt);
    [self rotateImageView];
    if([self.delegate respondsToSelector:@selector(saveImageToPhotoAlbum:)])
    {
        UIImage *rotatedImage = [CommonCallback fixOrientation:imageView.image];
         [self.delegate saveImageToPhotoAlbum:rotatedImage];
//        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
//        [library writeVideoAtPathToSavedPhotosAlbum:imageURL completionBlock:^(NSURL *assetURL, NSError *error){
//            if (error) {
//                DebugLog(@"error");
//            } else {
//                DebugLog(@"url %@", assetURL);
//                [self.delegate saveImageToPhotoAlbum:imageURL];
//            }
//        }];
    }
     */
//    UIImage *croppedImage = [self.imageCropper getCroppedImage];
//    self.imageView.image =  [CommonCallback resizeImage:croppedImage newSize:CGSizeMake(160, 160)];
//    
//    DebugLog(@"rotationInt %d",rotationInt);
//    [self rotateImageView];
//    thumbImage = [self.imageView.image scaleToSize:CGSizeMake(320, 320)];
//    minithumbImage = [self.imageView.image scaleToSize:CGSizeMake(50, 50)];
//    
//    self.imageCropper.hidden = TRUE;
//    self.imageView.hidden = TRUE;
//    toolBarView.hidden = TRUE;
//    [self.view bringSubviewToFront:filterToolView];
//
//    filterTableView.hidden = FALSE;
//    filterTableView.hidden = FALSE;
//    filterToolView.hidden = FALSE;
//    filterImageView.image = imageView.image;
//    
//    [filterTableView reloadData];
    
    
    UIImage *croppedImage = [self.imageCropper getCroppedImage];
    self.imageView.image = [CommonCallback resizeImage:croppedImage newSize:CGSizeMake(160, 160)];
    
    DebugLog(@"rotationInt %d",rotationInt);
    [self rotateImageView];
    if([self.delegate respondsToSelector:@selector(saveImageToPhotoAlbum:)])
    {
        UIImage *rotatedImage = [CommonCallback fixOrientation:imageView.image];
        [self.delegate saveImageToPhotoAlbum:rotatedImage];
    }
    [self.navigationController popViewControllerAnimated:YES];
    self.delegate = nil;
}

- (IBAction)antiClockWiseRotateBtnClicked:(id)sender {
    [UIView animateWithDuration:0.15 animations:^{
        imageCropper.transform = CGAffineTransformRotate(imageCropper.transform,-M_PI/2);
        rotationInt--;
        if(rotationInt==0)
            rotationInt=4;
    }];
}

- (IBAction)clockWiseRotateBtnClicked:(id)sender {
    [UIView animateWithDuration:0.15 animations:^{
        imageCropper.transform = CGAffineTransformRotate(imageCropper.transform,M_PI/2);
        rotationInt++;
        if(rotationInt==4)
            rotationInt=0;
    }];
}

- (IBAction)cropBtnClicked:(id)sender {
    imageCropper.hidden = TRUE;
    imageView.hidden     = FALSE;
    self.imageView.image = [imageCropper getCroppedImage];
}

-(void)rotateImageView{
	switch(rotationInt)
	{
		case 0:
			imageView.image=[UIImage imageWithCGImage:(CGImageRef)imageView.image.CGImage scale:imageView.image.scale orientation:UIImageOrientationUp];
			break;
		case 1:
			
			imageView.image=[UIImage imageWithCGImage:(CGImageRef)imageView.image.CGImage scale:imageView.image.scale orientation:UIImageOrientationRight];
			break;
		case 2:
			imageView.image=[UIImage imageWithCGImage:(CGImageRef)imageView.image.CGImage scale:imageView.image.scale orientation:UIImageOrientationDown];
			break;
		case 3:
			imageView.image=[UIImage imageWithCGImage:(CGImageRef)imageView.image.CGImage scale:imageView.image.scale orientation:UIImageOrientationLeft];
			break;
	}
}


-(int) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrEffects.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"EffectCell%d%d",indexPath.section,indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    if(((NSString *)[[arrEffects objectAtIndex:indexPath.row] valueForKey:@"method"]).length > 0) {
        SEL _selector = NSSelectorFromString([[arrEffects objectAtIndex:indexPath.row] valueForKey:@"method"]);
        cell.imageView.image = [minithumbImage performSelector:_selector];
    } else
        cell.imageView.image = minithumbImage;
    
   // cell.textLabel.text = [(NSDictionary *)[arrEffects objectAtIndex:indexPath.row] valueForKey:@"title"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(((NSString *)[[arrEffects objectAtIndex:indexPath.row] valueForKey:@"method"]).length > 0) {
        SEL _selector = NSSelectorFromString([[arrEffects objectAtIndex:indexPath.row] valueForKey:@"method"]);
        [filterImageView setImage:[thumbImage performSelector:_selector]];
                
    } else {
        [filterImageView setImage:thumbImage];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (IBAction)saveImageWithFilterBtnPressed:(id)sender {
    
//    UIImage *croppedImage = [self.imageCropper getCroppedImage];
//    self.imageView.image = [CommonCallback resizeImage:croppedImage newSize:CGSizeMake(160, 160)];
//    
//    DebugLog(@"rotationInt %d",rotationInt);
//    [self rotateImageView];
    
    if([self.delegate respondsToSelector:@selector(saveImageToPhotoAlbum:)])
    {
//        UIImage *rotatedImage = [CommonCallback fixOrientation:imageView.image];
//        [self.delegate saveImageToPhotoAlbum:rotatedImage];
        UIImage *filteredImage = filterImageView.image;

        [self.delegate saveImageToPhotoAlbum:filteredImage];
    }
    [self.navigationController popViewControllerAnimated:YES];
    self.delegate = nil;
}

- (IBAction)cancelBtn1Pressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
