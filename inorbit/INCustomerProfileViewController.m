//
//  INCustomerProfileViewController.m
//  shoplocal
//
//  Created by Kirti Nikam on 02/09/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INCustomerProfileViewController.h"
#import "constants.h"
#import "ProfileEvent.h"
#import "INAppDelegate.h"
#import <FacebookSDK/FacebookSDK.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "CommonCallback.h"
#import <FacebookSDK/FBSessionTokenCachingStrategy.h>
#import "INPrivacyPolicyViewController.h"

#define FBUSER_PIC(USERID) [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=200&height=200",USERID]

#define GET_DATE_CATEGORY_LIST [NSString stringWithFormat:@"%@%@%@user_api/date_categories",LIVE_SERVER,URL_PREFIX,API_VERSION]
#define UPDATE_CUSTOMER_PROFILE_POST_URL [NSString stringWithFormat:@"%@%@user_api/user",URL_PREFIX,API_VERSION]
#define GET_CUSTOMER_PROFILE_URL(USER_ID) [NSString stringWithFormat:@"%@%@%@user_api/user?user_id=%@",LIVE_SERVER,URL_PREFIX,API_VERSION,USER_ID]

#define PROFILE_IMAGE_LINK(IMG_LINK) [NSString stringWithFormat:@"%@%@%@",LIVE_SERVER,URL_PREFIX,IMG_LINK]

#define FBUSER_INTEREST @"https://graph.facebook.com/me/interests"

#define SELECT_DATE @"Select date"
#define SELECT_DATE_CATEGORY @"Select type of date"

@interface INCustomerProfileViewController ()
@property(strong) NSDictionary *splashJson;
@end

@implementation INCustomerProfileViewController
@synthesize fbLoginBtn;
@synthesize lbl1Seperator,lbl2Seperator,lbl3Seperator;
@synthesize addDetailsScrollView,addDetailsView;
@synthesize customerProfileImageView,nameText,emailText,cityText;
@synthesize dobBtn,maleBtn,femaleBtn,addEventsBtn,updateProfileBtn;
@synthesize eventsTableView;
@synthesize eventsArray,dateCategoriesDict;
@synthesize currentIndexPath;
@synthesize dateactionSheetPicker;
@synthesize selectedDOBDate;
@synthesize newMedia;
@synthesize placeholderImage;
@synthesize dateEventsArray;
@synthesize facebook_useridString,facebook_accesstokenString;
@synthesize lbleventsHeader;
@synthesize infoView,lblinfoHeader,lblinfoContent,infoOKBtn,privacyBtn;
@synthesize img1Seperator,img2Seperator,img3Seperator,img4Seperator,img5Seperator,img6Seperator,img7Seperator;
@synthesize lblOR;
@synthesize chooseImageBtn;
@synthesize finaldateCategoriesDict;
@synthesize lblSelectedGender;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:TRUE];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:TRUE];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:FALSE];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:TRUE];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[self navigationController] setNavigationBarHidden:NO animated:NO];

    
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                             style:UIBarButtonItemStylePlain
                                                            target:nil
                                                            action:nil];
    [[self navigationItem] setBackBarButtonItem:back];

    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    //[self setupMenuBarButtonItems];
    
    finaldateCategoriesDict = [[NSMutableDictionary alloc] init];
    //[self addHUDView];
    
//    UITapGestureRecognizer *imageViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(imageViewTapDetected:)];
//    imageViewTap.numberOfTapsRequired = 1;
//    [self.customerProfileImageView addGestureRecognizer:imageViewTap];
    
    UITapGestureRecognizer *scrollViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(scrollViewTapDetected:)];
    scrollViewTap.numberOfTapsRequired = 1;
    scrollViewTap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:scrollViewTap];
    
    [self.fbLoginBtn setUserInteractionEnabled:FALSE];
    [self.updateProfileBtn setUserInteractionEnabled:FALSE];

    ANIMATION_DELAY_DURATION = 0;
    [self setUI];
    [self initialization];
    DebugLog(@"user_id %@ auth_id %@",[INUserDefaultOperations getCustomerAuthId],[INUserDefaultOperations getCustomerAuthCode]);

    if ([IN_APP_DELEGATE networkavailable]) {
        if([INUserDefaultOperations isCustomerLoggedIn])
        {
            //[hud show:YES];
            [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
            [self addAlertView];
            [self sendDateCategoriesListRequest];
            [self sendGetCustomerProfileListRequest];
            [self.updateProfileBtn setUserInteractionEnabled:TRUE];
        }else {
            [INUserDefaultOperations showSIAlertView:@"Please login first."];
        }
    }else{
         [INUserDefaultOperations showOfflineAlert];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    //[self removeHUDView];
}

- (void)viewDidUnload {
    [self setFbLoginBtn:nil];
    [self setLbl1Seperator:nil];
    [self setLbl2Seperator:nil];
    [self setLbl3Seperator:nil];
    [self setAddDetailsScrollView:nil];
    [self setAddDetailsView:nil];
    [self setCustomerProfileImageView:nil];
    [self setNameText:nil];
    [self setDobBtn:nil];
    [self setEmailText:nil];
    [self setCityText:nil];
    [self setMaleBtn:nil];
    [self setFemaleBtn:nil];
    [self setEventsTableView:nil];
    [self setAddEventsBtn:nil];
    [self setUpdateProfileBtn:nil];
    [self setLbleventsHeader:nil];
    [self setInfoBtn:nil];
    [self setInfoView:nil];
    [self setLblinfoHeader:nil];
    [self setLblinfoContent:nil];
    [self setInfoOKBtn:nil];
    [self setPrivacyBtn:nil];
    [self setLblOR:nil];
    [self setImg1Seperator:nil];
    [self setImg2Seperator:nil];
    [self setImg4Seperator:nil];
    [self setImg3Seperator:nil];
    [self setImg5Seperator:nil];
    [self setImg6Seperator:nil];
    [self setImg7Seperator:nil];
    [self setChooseImageBtn:nil];
    [self setLblSelectedGender:nil];
    [super viewDidUnload];
}
#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
#pragma set Navigation barbuttons callback
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            //            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:MENU_TITLE style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:MENU_TITLE style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}
- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma Internal Methods
-(void)setUI{
    addDetailsScrollView.backgroundColor = [UIColor whiteColor];
    addDetailsView.backgroundColor = [UIColor clearColor];
    
    //addDetailsView = [CommonCallback setViewPropertiesWithRoundedCorner:addDetailsView];
    addDetailsView.clipsToBounds = YES;
    
    addDetailsScrollView.clipsToBounds      = YES;
//    addDetailsScrollView.layer.cornerRadius = 8.0;
    
    lbleventsHeader.textColor   = BROWN_COLOR;
    lblOR.font                  = DEFAULT_BOLD_FONT(15);
    nameText.font               = DEFAULT_FONT(15);
    emailText.font              = DEFAULT_FONT(15);
    cityText.font               = DEFAULT_FONT(15);
    lbleventsHeader.font        = DEFAULT_BOLD_FONT(18);
    
    chooseImageBtn.titleLabel.font      = DEFAULT_BOLD_FONT(15);
    fbLoginBtn.titleLabel.font          = DEFAULT_BOLD_FONT(18);
    updateProfileBtn.titleLabel.font    = DEFAULT_BOLD_FONT(18);
    addEventsBtn.titleLabel.font        = DEFAULT_BOLD_FONT(15);

    maleBtn.titleLabel.font     = DEFAULT_BOLD_FONT(15);
    femaleBtn.titleLabel.font   = DEFAULT_BOLD_FONT(15);
    lblSelectedGender.font      = DEFAULT_FONT(15);
    lblSelectedGender.textColor = [UIColor blackColor];

    if ([[INUserDefaultOperations getCustomerLoginFrom] isEqualToString:FACEBOOKSERVER]) {
        [fbLoginBtn setTitle:@"Sync profile from facebook" forState:UIControlStateNormal];
        [fbLoginBtn setTitle:@"Sync profile from facebook" forState:UIControlStateHighlighted];
    }else{
        [fbLoginBtn setTitle:@"Connect using facebook" forState:UIControlStateNormal];
        [fbLoginBtn setTitle:@"Connect using facebook" forState:UIControlStateHighlighted];
    }
    
    [fbLoginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [fbLoginBtn setTitleColor:[UIColor colorWithWhite:0.5 alpha:0.5] forState:UIControlStateHighlighted];
    
    [chooseImageBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [chooseImageBtn setTitleColor:[UIColor colorWithWhite:0.5 alpha:0.5] forState:UIControlStateHighlighted];

    [updateProfileBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [updateProfileBtn setTitleColor:[UIColor colorWithWhite:0.5 alpha:0.5] forState:UIControlStateHighlighted];
    
    [addEventsBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [addEventsBtn setTitleColor:[UIColor colorWithWhite:0.5 alpha:0.5] forState:UIControlStateHighlighted];
    
    [maleBtn setTitleColor:BROWN_COLOR forState:UIControlStateNormal];
    [maleBtn setTitleColor:BROWN_OFFWHITE_COLOR forState:UIControlStateHighlighted];
    
    [femaleBtn setTitleColor:BROWN_COLOR forState:UIControlStateNormal];
    [femaleBtn setTitleColor:BROWN_OFFWHITE_COLOR forState:UIControlStateHighlighted];
    
    infoView = [CommonCallback setViewPropertiesWithRoundedCorner:infoView];
    infoView.layer.shadowColor      = [UIColor blackColor].CGColor;
    infoView.layer.shadowOffset     = CGSizeMake(1, 1);
    infoView.layer.shadowOpacity    = 1.0;
    infoView.layer.shadowRadius     = 10.0;
    
    lblinfoHeader.textAlignment     = NSTextAlignmentCenter;
    lblinfoHeader.backgroundColor   = [UIColor clearColor];
    lblinfoHeader.font              = DEFAULT_FONT(20);
    lblinfoHeader.textColor         = [UIColor blackColor];
    lblinfoHeader.adjustsFontSizeToFitWidth = YES;
    lblinfoHeader.text              = ALERT_TITLE;
    
    lblinfoContent.textAlignment    = NSTextAlignmentCenter;
    lblinfoContent.backgroundColor  = [UIColor clearColor];
    lblinfoContent.font             = DEFAULT_FONT(16);
    lblinfoContent.textColor        = [UIColor darkGrayColor];
    lblinfoContent.numberOfLines    = 15;
    lblinfoContent.text             = @"As a logged in Shoplocal user, You can mark your favourite stores and also receive personalized offers from local merchants in your area. Please see our privacy policy for details.";
    
    privacyBtn.titleLabel.font  = DEFAULT_FONT([UIFont buttonFontSize]);
    infoOKBtn.titleLabel.font   = DEFAULT_FONT([UIFont buttonFontSize]);

    privacyBtn.backgroundColor   = [UIColor clearColor];
    infoOKBtn.backgroundColor   = [UIColor clearColor];
    
    [privacyBtn setTitleColor:[UIColor colorWithWhite:0.3 alpha:1] forState:UIControlStateNormal];
    [privacyBtn setTitleColor:[UIColor colorWithWhite:0.3 alpha:0.8] forState:UIControlStateHighlighted];
    
    [infoOKBtn setTitleColor:[UIColor colorWithWhite:0.3 alpha:1] forState:UIControlStateNormal];
    [infoOKBtn setTitleColor:[UIColor colorWithWhite:0.3 alpha:0.8] forState:UIControlStateHighlighted];

    UIImage *normalImage        = [UIImage imageNamed:@"SIAlertView.bundle/button-cancel"];
    UIImage *highlightedImage   = [UIImage imageNamed:@"SIAlertView.bundle/button-cancel-d"];
    CGFloat hInset              = floorf(normalImage.size.width / 2);
	CGFloat vInset              = floorf(normalImage.size.height / 2);
	UIEdgeInsets insets         = UIEdgeInsetsMake(vInset, hInset, vInset, hInset);
	normalImage                 = [normalImage resizableImageWithCapInsets:insets];
	highlightedImage            = [highlightedImage resizableImageWithCapInsets:insets];
	[infoOKBtn setBackgroundImage:normalImage forState:UIControlStateNormal];
	[infoOKBtn setBackgroundImage:highlightedImage forState:UIControlStateHighlighted];
    
    [infoView setHidden:YES];
    
    [self setEditing:FALSE];
}

-(void)addHUDView
{
    hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:hud];
	hud.dimBackground = NO;
    hud.labelText = @"Loading";
	hud.detailsLabelText = @"Please wait ...";
    [hud hide:NO];
}

-(void)removeHUDView
{
    [hud removeFromSuperview];
    hud = nil;
}
-(void) addAlertView{
    sialertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@""];
    sialertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
    sialertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
    [sialertView addButtonWithTitle:@"OK"
                               type:SIAlertViewButtonTypeCancel
                            handler:^(SIAlertView *alert) {
                            }];
    /*  alertView.willShowHandler = ^(SIAlertView *alertView) {
     DebugLog(@"%@, willShowHandler", alertView);
     };
     alertView.didShowHandler = ^(SIAlertView *alertView) {
     DebugLog(@"%@, didShowHandler", alertView);
     };
     alertView.willDismissHandler = ^(SIAlertView *alertView) {
     DebugLog(@"%@, willDismissHandler", alertView);
     };
     alertView.didDismissHandler = ^(SIAlertView *alertView) {
     DebugLog(@"%@, didDismissHandler", alertView);
     };*/
}
-(void)initialization
{
    lbl1Seperator.backgroundColor = BROWN_COLOR;
    lbl2Seperator.backgroundColor = BROWN_COLOR;
    lbl3Seperator.backgroundColor = BROWN_COLOR;
    lbl1Seperator.text= @"";
    lbl2Seperator.text= @"";
    lbl3Seperator.text= @"";

    customerProfileImageView.layer.borderWidth = 2.0;
    customerProfileImageView.layer.borderColor = [UIColor grayColor].CGColor;
//    customerProfileImageView.image = nil;
    
    contentSizeHeight = addDetailsView.frame.size.height - (6 * 40);
    
    DebugLog(@"[INUserDefaultOperations getCustomerLoginFrom] %@",[INUserDefaultOperations getCustomerLoginFrom]);
    if ([[INUserDefaultOperations getCustomerLoginFrom] isEqualToString:FACEBOOKSERVER]) {
        facebook_useridString = [INUserDefaultOperations getCustomerId];
        facebook_accesstokenString = [INUserDefaultOperations getCustomerAuthCode];
    }else{
        facebook_useridString = @"";
        facebook_accesstokenString = @"";
    }
    nameText.text = @"";
    emailText.text = @"";
    cityText.text = @"";
    [self maleBtnPressed:nil];
    
    placeholderImage = [UIImage imageNamed:@"unknown.jpg"];
    self.selectedDOBDate = [NSDate date];
    eventsArray = [[NSMutableArray alloc]init];
    dateCategoriesDict = [[NSMutableDictionary alloc] init];
    if (eventsArray.count <= 0){
        [self addEventsBtnPressed:nil];
    }
    ANIMATION_DELAY_DURATION = 0.5;
}

-(void)initializeEventsArray{
    NSArray *allSortedkeys = [self.dateCategoriesDict keysSortedByValueUsingComparator:^(id first, id second) {
        return [first compare:second];
    }];
 
    ProfileEvent *eventObj = [[ProfileEvent alloc] init];
    eventObj.date = @"";
    eventObj.dateCategory = [allSortedkeys objectAtIndex:0];
    [eventsArray addObject:eventObj];
    eventObj = nil;

    eventObj = [[ProfileEvent alloc] init];
    eventObj.date = @"";
    eventObj.dateCategory = [allSortedkeys objectAtIndex:1];
    [eventsArray addObject:eventObj];
    eventObj = nil;
    
    eventObj = [[ProfileEvent alloc] init];
    eventObj.date = @"";
    eventObj.dateCategory = [allSortedkeys objectAtIndex:2];
    [eventsArray addObject:eventObj];
    eventObj = nil;

    [self reloadEventsTableView];
}

-(void)reloadEventsTableView
{
    [eventsTableView reloadData];
    [UIView animateWithDuration:ANIMATION_DELAY_DURATION animations:^{
        [eventsTableView setFrame:CGRectMake(eventsTableView.frame.origin.x,
                                             eventsTableView.frame.origin.y,
                                             eventsTableView.frame.size.width,
                                             eventsTableView.contentSize.height)];
        [self adjustBottomLayouts];
    } completion:^(BOOL finished) {
        if (eventsArray.count == 0) {
            [eventsTableView reloadData];
        }
    }];
}

-(void)adjustBottomLayouts
{
    [UIView animateWithDuration:ANIMATION_DELAY_DURATION animations:^{
        
        if (self.editing) {
            double nextYaxis = CGRectGetMaxY(eventsTableView.frame)+10;
            if (eventsArray.count < 7) {
                [addEventsBtn setHidden:NO];
                [addEventsBtn setFrame:CGRectMake(addEventsBtn.frame.origin.x,
                                                  nextYaxis,
                                                  addEventsBtn.frame.size.width,
                                                  addEventsBtn.frame.size.height)];
                
                nextYaxis = CGRectGetMaxY(addEventsBtn.frame)+10;
            }else{
                [addEventsBtn setHidden:YES];
            }
            [img7Seperator setFrame:CGRectMake(img7Seperator.frame.origin.x,
                                              nextYaxis,
                                              img7Seperator.frame.size.width,
                                              img7Seperator.frame.size.height)];
            [updateProfileBtn setFrame:CGRectMake(updateProfileBtn.frame.origin.x,
                                              CGRectGetMaxY(img7Seperator.frame),
                                              updateProfileBtn.frame.size.width,
                                              updateProfileBtn.frame.size.height)];
        }
        double tempcontentSizeHeight = contentSizeHeight;
        
//        if (eventsArray.count > 0) {
//            tempcontentSizeHeight = contentSizeHeight + ((eventsArray.count-1) * 40);
//        }
        if (!self.editing) {
            tempcontentSizeHeight =  CGRectGetMaxY(eventsTableView.frame)+10;
        }else{
            tempcontentSizeHeight =  CGRectGetMaxY(updateProfileBtn.frame)+10;
        }
        DebugLog(@"tempcontentSizeHeight %f",tempcontentSizeHeight);
       [addDetailsScrollView setContentSize:CGSizeMake(self.addDetailsScrollView.frame.size.width,tempcontentSizeHeight)];
    } completion:^(BOOL finished) {
    }];
}

-(NSIndexPath *)getIndexpathOfEvent:(id)event
{
    NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:self.eventsTableView];
	NSIndexPath *indexPath = [self.eventsTableView indexPathForRowAtPoint: currentTouchPosition];
    return indexPath;
}

-(NSString *)getDateStringFromDate:(NSDate *)ldate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *formattedDateString = [dateFormatter stringFromDate:ldate];
    return formattedDateString;
}

-(NSDate *)getDateFromDateString:(NSString *)lstring
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *formattedDate = [[NSDate alloc] init];
    formattedDate = [dateFormatter dateFromString:lstring];
    DebugLog(@"myDate %@",formattedDate);
    return formattedDate;
}

-(void)scrollTobottom
{
    CGPoint bottomOffset = CGPointMake(0, 0);
    [addDetailsScrollView setContentOffset:bottomOffset animated:YES];
}

#pragma Gesture Methods
- (void)scrollViewTapDetected:(UIGestureRecognizer *)sender {
    [self.view endEditing:YES];
    CGPoint tapLocation = [sender locationInView:addDetailsScrollView];
    UIView *view = [addDetailsScrollView hitTest:tapLocation withEvent:nil];
    if (![view isKindOfClass:[UIButton class]]) {
       // [self scrollTobottom];
    }
}

- (void)imageViewTapDetected:(UIGestureRecognizer *)sender {
    SIAlertView *sialertView2 = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Choose your option"];
    sialertView2.transitionStyle = SIAlertViewTransitionStyleDropDown;
    sialertView2.backgroundStyle = SIAlertViewBackgroundStyleGradient;
    [sialertView2 addButtonWithTitle:@"Take Picture"
                                type:SIAlertViewButtonTypeDestructive
                             handler:^(SIAlertView *alert) {
                                 DebugLog(@"Take Picture Clicked");
                                 [self useCamera];
                             }];
    [sialertView2 addButtonWithTitle:@"Choose Existing"
                                type:SIAlertViewButtonTypeDestructive
                             handler:^(SIAlertView *alert) {
                                 DebugLog(@"Choose Existing Clicked");
                                [self useCameraRoll];
                             }];
    [sialertView2 addButtonWithTitle:@"Cancel"
                                type:SIAlertViewButtonTypeCancel
                             handler:^(SIAlertView *alert) {
                                 DebugLog(@"Skip Clicked");
                             }];
    [sialertView2 show];
    
//    UIAlertView *addImageToGalleryAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Choose your option" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Choose Existing", @"Take Picture", nil];
//    [addImageToGalleryAlert show];
}

#pragma mark - Image Picker
- (void)alertView:(UIAlertView *)lalertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [lalertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"Choose Existing"])
    {
        [self useCameraRoll];
    } else if([title isEqualToString:@"Take Picture"])
    {
        [self useCamera];
    }
}

- (void)useCamera
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker =
        [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        //[imagePicker.navigationBar setTintColor:BROWN_COLOR];
        imagePicker.sourceType =
        UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = [NSArray arrayWithObjects:
                                  (NSString *) kUTTypeImage,
                                  nil];
        imagePicker.allowsEditing = YES;
        [self presentViewController:imagePicker animated:YES completion:nil];
        newMedia = YES;
    }
}

- (void)useCameraRoll
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    {
        UIImagePickerController *imagePicker =
        [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        //[imagePicker.navigationBar setTintColor:BROWN_COLOR];
        imagePicker.sourceType =
        UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.mediaTypes = [NSArray arrayWithObjects:
                                  (NSString *) kUTTypeImage,
                                  nil];
        imagePicker.allowsEditing = YES;
        [self presentViewController:imagePicker animated:YES completion:nil];
        newMedia = NO;
    }
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
//    [self dismissViewControllerAnimated:YES completion:nil];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
//        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
        
        CLImageEditor *editor = [[CLImageEditor alloc] initWithImage:image];
        editor.delegate = self;
        /* @"CLFilterTool",
         @"CLAdjustmentTool",
         @"CLEffectTool",
         @"CLBlurTool",
         @"CLClippingTool",
         @"CLRotateTool",
         @"CLToneCurveTool",*/
        
        CLImageToolInfo *Filtertool = [editor.toolInfo subToolInfoWithToolName:@"CLFilterTool" recursive:NO];
        Filtertool.available = NO;
        CLImageToolInfo *Blurtool = [editor.toolInfo subToolInfoWithToolName:@"CLBlurTool" recursive:NO];
        Blurtool.available = NO;
        CLImageToolInfo *Curvetool = [editor.toolInfo subToolInfoWithToolName:@"CLToneCurveTool" recursive:NO];
        Curvetool.available = NO;
        CLImageToolInfo *ClippingTool = [editor.toolInfo subToolInfoWithToolName:@"CLClippingTool" recursive:NO];
        ClippingTool.available = NO;
        
        CLImageToolInfo *AdjustmentTool = [editor.toolInfo subToolInfoWithToolName:@"CLAdjustmentTool" recursive:NO];
        AdjustmentTool.dockedNumber = 2;
        CLImageToolInfo *EffectTool = [editor.toolInfo subToolInfoWithToolName:@"CLEffectTool" recursive:NO];
        EffectTool.dockedNumber = 3;
//        CLImageToolInfo *ClippingTool = [editor.toolInfo subToolInfoWithToolName:@"CLClippingTool" recursive:NO];
//        ClippingTool.dockedNumber = 1;
        CLImageToolInfo *RotateTool = [editor.toolInfo subToolInfoWithToolName:@"CLRotateTool" recursive:NO];
        RotateTool.dockedNumber = 1;
        [picker pushViewController:editor animated:YES];
    }
}

-(void)image:(UIImage *)image finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: ALERT_TITLE
                              message: @"Failed to save image in Photo Album."
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark- CLImageEditor delegate

- (void)imageEditor:(CLImageEditor *)editor didFinishEdittingWithImage:(UIImage *)image
{
    [editor dismissViewControllerAnimated:YES completion:nil];
    DebugLog(@"gallery : saveToalbum %@",image);
    customerProfileImageView.image = image;
    if (newMedia)
        UIImageWriteToSavedPhotosAlbum(image,
                                       self,
                                       @selector(image:finishedSavingWithError:contextInfo:),
                                       nil);
    isThisFBProfileImage = NO;
}

#pragma UITableView DataSource and Delegate Methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [eventsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"eventCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    UIButton *dateBtn;
    UIButton *dateCategoryBtn;
    UIButton *deleteBtn;
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];

        dateBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [dateBtn setFrame:CGRectMake(5, 5, 90, 30)];
        [dateBtn setBackgroundColor:[UIColor grayColor]];
        dateBtn.tag = 1;
        [dateBtn setTitle:SELECT_DATE forState:UIControlStateNormal];
        [dateBtn setTitle:SELECT_DATE forState:UIControlStateHighlighted];
        [dateBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        dateBtn.titleLabel.font = DEFAULT_BOLD_FONT(12);
        dateBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [dateBtn addTarget:self action:@selector(selectDateBtnTapped:event:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:dateBtn];
        
        dateCategoryBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [dateCategoryBtn setFrame:CGRectMake(CGRectGetMaxX(dateBtn.frame)+5, 5, 135, 30)];
        [dateCategoryBtn setBackgroundColor:[UIColor grayColor]];
        dateCategoryBtn.tag = 2;
        [dateCategoryBtn setTitle:SELECT_DATE_CATEGORY forState:UIControlStateNormal];
        [dateCategoryBtn setTitle:SELECT_DATE_CATEGORY forState:UIControlStateHighlighted];
        [dateCategoryBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        dateCategoryBtn.titleLabel.font = DEFAULT_BOLD_FONT(12);
        dateCategoryBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [dateCategoryBtn addTarget:self action:@selector(selectDateCategoryBtnTapped:event:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:dateCategoryBtn];
        
        deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [deleteBtn setFrame:CGRectMake(CGRectGetMaxX(dateCategoryBtn.frame)+5, 0, 33, 38)];
//        [deleteBtn setImage:[UIImage imageNamed:@"cancel.png"] forState:UIControlStateNormal];
//        [deleteBtn setBackgroundColor:[UIColor grayColor]];
        [deleteBtn setTitle:@"X" forState:UIControlStateNormal];
        [deleteBtn setTitle:@"X" forState:UIControlStateHighlighted];
        [deleteBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        deleteBtn.titleLabel.font = DEFAULT_BOLD_FONT(15);
        [deleteBtn setBackgroundImage:[UIImage imageNamed:@"Button1.png"] forState:UIControlStateNormal];
        [deleteBtn setBackgroundColor:[UIColor clearColor]];
        deleteBtn.tag = 3;
        [deleteBtn addTarget:self action:@selector(deleteEventBtnTapped:event:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:deleteBtn];
    }
    
    if ([indexPath row] < [eventsArray count]) {
        dateBtn = (UIButton *)[cell viewWithTag:1];
        dateCategoryBtn = (UIButton *)[cell viewWithTag:2];
        deleteBtn = (UIButton *)[cell viewWithTag:3];

        ProfileEvent *eventObj = (ProfileEvent *)[eventsArray objectAtIndex:indexPath.row];
        if (eventObj.date != nil && ![eventObj.date isEqualToString:@""]) {
            [dateBtn setTitle:eventObj.date forState:UIControlStateNormal];
            [dateBtn setTitle:eventObj.date forState:UIControlStateHighlighted];
        }else{
            [dateBtn setTitle:SELECT_DATE forState:UIControlStateNormal];
            [dateBtn setTitle:SELECT_DATE forState:UIControlStateHighlighted];
        }
        
        if (eventObj.dateCategory != nil && ![eventObj.dateCategory isEqualToString:@""]) {
            [dateCategoryBtn setTitle:eventObj.dateCategory forState:UIControlStateNormal];
            [dateCategoryBtn setTitle:eventObj.dateCategory forState:UIControlStateHighlighted];
        }else{
            [dateCategoryBtn setTitle:SELECT_DATE_CATEGORY forState:UIControlStateNormal];
            [dateCategoryBtn setTitle:SELECT_DATE_CATEGORY forState:UIControlStateHighlighted];
        }
        
        if (self.editing){
            deleteBtn.hidden = FALSE;
            [dateCategoryBtn setFrame:CGRectMake(CGRectGetMaxX(dateBtn.frame)+5, 5, 135, 30)];
        }
        else{
            deleteBtn.hidden = TRUE;
            [dateCategoryBtn setFrame:CGRectMake(CGRectGetMaxX(dateBtn.frame)+5, 5, 175, 30)];
        }
    }
    return cell;
}


#pragma UIButton delegate Methods
- (IBAction)infoOKBtnPressed:(id)sender {
    [infoView setHidden:YES];
}

- (IBAction)privacyBtnPressed:(id)sender {
    [infoView setHidden:YES];
    if ([IN_APP_DELEGATE networkavailable]) {
        INPrivacyPolicyViewController *privacyController = [[INPrivacyPolicyViewController alloc] initWithNibName:@"INPrivacyPolicyViewController" bundle:nil] ;
        privacyController.title = @"Privacy Policy";
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:privacyController];
        navController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        navController.modalPresentationStyle = UIModalPresentationFormSheet;
        //navController.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
        //    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
        //                                                                                   target:self
        //                                                                                   action:@selector(didDismissPresentedViewController)];
        //    loginController.navigationItem.rightBarButtonItem = doneBarButton;
        privacyController.navigationItem.title = @"Privacy Policy";
        //termsController.delegate = self;
        [self.navigationController presentViewController:navController animated:YES completion:NULL];
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

- (IBAction)infoBtnPressed:(id)sender {
    [infoView setHidden:FALSE];
    
    //add animation code here
    [CommonCallback viewtransitionInCompletion:infoView completion:^{
        [CommonCallback viewtransitionOutCompletion:infoView completion:nil];
    }];
}

- (IBAction)fbLoginBtnPressed:(id)sender {
    DebugLog(@"Login: Facebook Login");
    if([IN_APP_DELEGATE networkavailable])
    {
        //[hud show:YES];
        [CommonCallback showProgressHud:@"Processing" subtitle:HUD_SUBTITLE];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sessionStateChanged:) name:FBSessionStateChangedNotification object:nil];
        [IN_APP_DELEGATE openSessionWithAllowLoginUI:YES];
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

- (IBAction)dobBtnPressed:(id)sender {
    dateactionSheetPicker = [[ActionSheetDatePicker alloc] initWithTitle:@"" datePickerMode:UIDatePickerModeDate selectedDate:self.selectedDOBDate target:self action:@selector(dobdateSelected:element:) origin:sender];
    self.dateactionSheetPicker.hideCancel = NO;
    [self.dateactionSheetPicker showActionSheetPicker];
}

- (IBAction)maleBtnPressed:(id)sender {
    [femaleBtn setSelected:NO];
    [maleBtn setSelected:YES];
    lblSelectedGender.text = [@"male" capitalizedString];
    [INEventLogger setGender:@"m"];
}

- (IBAction)femaleBtnPressed:(id)sender {
    [femaleBtn setSelected:YES];
    [maleBtn setSelected:NO];
    lblSelectedGender.text = [@"female" capitalizedString];
    [INEventLogger setGender:@"f"];
}

- (IBAction)addEventsBtnPressed:(id)sender {
    
    if (eventsArray.count < 7) {
        ProfileEvent *eventObj = [[ProfileEvent alloc] init];
        eventObj.date = @"";
        eventObj.dateCategory = @"";
        [eventsArray addObject:eventObj];

        [eventsTableView beginUpdates];
        [eventsTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:eventsArray.count-1 inSection:0]] withRowAnimation:UITableViewRowAnimationLeft];
        [eventsTableView endUpdates];
        [self.eventsTableView reloadData];

        [UIView animateWithDuration:ANIMATION_DELAY_DURATION animations:^{
            [eventsTableView setFrame:CGRectMake(eventsTableView.frame.origin.x,
                                                 eventsTableView.frame.origin.y,
                                                 eventsTableView.frame.size.width,
                                                 eventsTableView.contentSize.height)];
            [self adjustBottomLayouts];
        } completion:^(BOOL finished) {
        }];
    }
}

-(BOOL)isCategorySelectedForDate{
    for (ProfileEvent *eventObj in eventsArray) {
        DebugLog(@"EventObj %@ %@",eventObj.date,eventObj.dateCategory);
        if ((eventObj.date != nil && ![eventObj.date isEqualToString:@""])  && (eventObj.dateCategory == nil || [eventObj.dateCategory isEqualToString:@""])) {
            sialertView.message = @"Please select appropriate category for date in events.";
            [sialertView show];
            return NO;
        }
    }
    return YES;
}

- (IBAction)updateProfileBtnPressed:(id)sender {
    if([IN_APP_DELEGATE networkavailable])
    {
        if([INUserDefaultOperations isCustomerLoggedIn])
        {
            //        if([nameText.text isEqualToString:@""] && [emailText.text isEqualToString:@""] && [cityText.text isEqualToString:@""])
            //        {
            //            alertView.message = @"Please fill all the fields.";
            //            [alertView show];
            //        }else if([nameText.text length] <= 0)
            //        {
            //            alertView.message = @"Please enter your name.";
            //            [alertView show];
            //        }else if([emailText.text length] <= 0)
            //        {
            //            alertView.message = @"Please enter email.";
            //            [alertView show];
            //        }
            //        else if(![CommonCallback validateEmail:emailText.text])
            //        {
            //            alertView.message = @"Invalid email address.Please check your email address and try again.";
            //            [alertView show];
            //        } else if([cityText.text length] <= 0)
            //        {
            //            alertView.message = @"Please enter city.";
            //            [alertView show];
            //
            //        }else
            if(![self isCategorySelectedForDate]){
                return;
            }
            else
            {
                if (eventsArray.count > 0) {
                    BOOL showalert = NO;
                    for (ProfileEvent *eventObj in eventsArray) {
                        if (eventObj.date == nil || [eventObj.date isEqualToString:@""]) {
                            showalert = YES;
                            break;
                        }
                    }
                    if (showalert) {
                        [self showAddEventsAlert];
                    }else{
                        [self sendUpdateProfileRequest];
                    }
                }else{
                    [self showAddEventsAlert];
                }
            }
        }else {
            sialertView.message = @"Please login first to update profile.";
            [sialertView show];
        }
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

- (IBAction)chooseImageBtnPressed:(id)sender {
    [self imageViewTapDetected:nil];
}

-(void)showAddEventsAlert{
    SIAlertView *sialertView1 = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Entering important dates allows merchants to send you special offers around those dates."];
    sialertView1.transitionStyle = SIAlertViewTransitionStyleDropDown;
    sialertView1.backgroundStyle = SIAlertViewBackgroundStyleGradient;
    [sialertView1 addButtonWithTitle:@"Fill dates"
                                type:SIAlertViewButtonTypeDestructive
                             handler:^(SIAlertView *alert) {
                                 DebugLog(@"Fill dates Clicked");
                             }];
    [sialertView1 addButtonWithTitle:@"Skip"
                                type:SIAlertViewButtonTypeCancel
                             handler:^(SIAlertView *alert) {
                                 DebugLog(@"Skip Clicked");
                                 [self sendUpdateProfileRequest];
                             }];
    [sialertView1 show];
}


- (void)selectDateBtnTapped:(id)sender event:(id)event
{
    NSDate *tempDate = [NSDate date];
    UIButton *eventDateBtn = (UIButton *)sender;
    if (eventDateBtn.titleLabel.text != nil && ![eventDateBtn.titleLabel.text isEqualToString:@""] && ![eventDateBtn.titleLabel.text hasPrefix:SELECT_DATE]) {
        tempDate = [self getDateFromDateString:eventDateBtn.titleLabel.text];
    }
    currentIndexPath = [self getIndexpathOfEvent:event];
    dateactionSheetPicker = [[ActionSheetDatePicker alloc] initWithTitle:@"" datePickerMode:UIDatePickerModeDate selectedDate:tempDate maximumDate:[NSDate date] target:self action:@selector(eventDateSelected:element:) origin:sender];
    self.dateactionSheetPicker.hideCancel = NO;
    [self.dateactionSheetPicker showActionSheetPicker];
}

- (void)selectDateCategoryBtnTapped:(id)sender event:(id)event
{
    currentIndexPath = [self getIndexpathOfEvent:event];
    int initialIndex = 0;
    UIButton *eventDateCatgBtn = (UIButton *)sender;
    
    [finaldateCategoriesDict removeAllObjects];
    finaldateCategoriesDict = [self.dateCategoriesDict mutableCopy];
    
    DebugLog(@"Alloc => array %@",finaldateCategoriesDict);
    for (ProfileEvent *eventObj in eventsArray) {
        DebugLog(@"eventObj.dateCategory %@",eventObj.dateCategory);
        if ([[finaldateCategoriesDict allKeys] indexOfObject:eventObj.dateCategory] != NSNotFound) {
            [finaldateCategoriesDict removeObjectForKey:eventObj.dateCategory];
        }
    }
    DebugLog(@"Delete => array %@",finaldateCategoriesDict);

    if (eventDateCatgBtn.titleLabel.text != nil && ![eventDateCatgBtn.titleLabel.text isEqualToString:@""] && ![eventDateCatgBtn.titleLabel.text hasPrefix:SELECT_DATE_CATEGORY]) {
        [finaldateCategoriesDict setObject:[self.dateCategoriesDict objectForKey:eventDateCatgBtn.titleLabel.text] forKey:eventDateCatgBtn.titleLabel.text];
    }
    DebugLog(@"Insert => array %@",finaldateCategoriesDict);

    NSArray *allSortedkeys = [finaldateCategoriesDict keysSortedByValueUsingComparator:^(id first, id second) {
        return [first compare:second];
    }];
    
    if (eventDateCatgBtn.titleLabel.text != nil && ![eventDateCatgBtn.titleLabel.text isEqualToString:@""] && ![eventDateCatgBtn.titleLabel.text hasPrefix:SELECT_DATE_CATEGORY]) {
        initialIndex = [allSortedkeys indexOfObject:eventDateCatgBtn.titleLabel.text];
    }
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select Category" rows:allSortedkeys initialSelection:initialIndex target:self successAction:@selector(selectDateCategory:element:) cancelAction:@selector(actionPickerCancelled:) origin:sender];
}

- (void)deleteEventBtnTapped:(id)sender event:(id)event
{
    UIButton *btn = (UIButton *)sender;
    if (btn.tag == 3) {
        NSIndexPath *indexPath = [self getIndexpathOfEvent:event];
        if (indexPath != nil)
        {
            [eventsArray removeObjectAtIndex:indexPath.row];
            [eventsTableView beginUpdates];
            [eventsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
            [eventsTableView endUpdates];
            [self reloadEventsTableView];
        }
    }
}

#pragma mark - Actionsheet Implementation
- (void)dobdateSelected:(NSDate *)selectedDate element:(id)element {
    self.selectedDOBDate = selectedDate;
    
    NSString *tempdobString = [self getDateStringFromDate:selectedDate];
    DebugLog(@"selectedDOBDate is: %@", tempdobString);
    [dobBtn setTitle:tempdobString forState: UIControlStateNormal];
    [dobBtn setTitle:tempdobString forState: UIControlStateHighlighted];
    
    [emailText becomeFirstResponder];
}

- (void)eventDateSelected:(NSDate *)selectedDate element:(id)element {

    NSString *eventDateString = [self getDateStringFromDate:selectedDate];
    DebugLog(@"eventDateString is: %@", eventDateString);
    
    UIButton *eventDateBtn = (UIButton *)element;
    [eventDateBtn setTitle:eventDateString forState: UIControlStateNormal];
    [eventDateBtn setTitle:eventDateString forState: UIControlStateHighlighted];
    
    if (eventDateBtn != nil && currentIndexPath != nil) {
        ProfileEvent *eventObj = (ProfileEvent *)[eventsArray objectAtIndex:currentIndexPath.row];
        eventObj.dateVal = selectedDate;
        eventObj.date = eventDateString;
        [eventsArray replaceObjectAtIndex:currentIndexPath.row withObject:eventObj];
        currentIndexPath = nil;
    }
}

- (void)selectDateCategory:(NSNumber *)lselectedIndex element:(id)element {
    NSArray *allSortedkeys = [self.finaldateCategoriesDict keysSortedByValueUsingComparator:^(id first, id second) {
        return [first compare:second];
    }];
    
    NSInteger selectedIndex = [lselectedIndex intValue];
    NSString *selectedValue = [allSortedkeys objectAtIndex:selectedIndex];
    
    UIButton *eventDateCatgBtn = (UIButton *)element;
    [eventDateCatgBtn setTitle:selectedValue forState: UIControlStateNormal];
    [eventDateCatgBtn setTitle:selectedValue forState: UIControlStateHighlighted];
    
    if (eventDateCatgBtn != nil && currentIndexPath != nil) {
        ProfileEvent *eventObj = (ProfileEvent *)[eventsArray objectAtIndex:currentIndexPath.row];
        eventObj.dateCategory = selectedValue;
        [eventsArray replaceObjectAtIndex:currentIndexPath.row withObject:eventObj];
        currentIndexPath = nil;
    }
}

#pragma UITextField delegate Methods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField.frame.origin.y > addDetailsScrollView.contentOffset.y)
    {
        [addDetailsScrollView setContentOffset:CGPointMake(0,textField.frame.origin.y-80) animated:YES];
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField.frame.origin.y > addDetailsScrollView.contentOffset.y)
    {
        [addDetailsScrollView setContentOffset:CGPointMake(0,textField.frame.origin.y-80) animated:YES];
    }
    
    if (textField == nameText) {
        [nameText resignFirstResponder];
        [emailText becomeFirstResponder];
//        [self dobBtnPressed:dobBtn];
	}  else if (textField == emailText) {
        [emailText resignFirstResponder];
        [cityText becomeFirstResponder];
	} else if (textField == cityText) {
        [cityText resignFirstResponder];
       // [self scrollTobottom];
	}
   	return YES;
}

#pragma NSNotificationCenter Callbacks
/*
 * Configure the logged in versus logged out UI
 */
- (void)sessionStateChanged:(NSNotification*)notification {
    if (FBSession.activeSession.isOpen) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        DebugLog(@"Login: User Logged In");
        [[FBRequest requestForMe] startWithCompletionHandler:
         ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
             if (error) {
                 [IN_APP_DELEGATE handleAuthError:error];
             }else {
                      /*
                       user {
                       birthday = "08/21/1987";
                       email = "developerios1@gmail.com";
                       "first_name" = Ios;
                       gender = male;
                       id = 100005429323067;
                       "last_name" = Dev;
                       link = "https://www.facebook.com/ios.dev.98";
                       locale = "en_US";
                       name = "Ios Dev";
                       timezone = "5.5";
                       "updated_time" = "2013-07-10T08:49:58+0000";
                       username = "ios.dev.98";
                       verified = 1;
                       }
                          */
                 
                 DebugLog(@"user %@",user);
                 DebugLog(@"user ID %@",[user objectForKey:@"id"]);
                 DebugLog(@"user name %@",[user objectForKey:@"first_name"]);
                 DebugLog(@"user middle name %@",[user objectForKey:@"middle_name"]);
                 DebugLog(@"user last name %@",[user objectForKey:@"last_name"]);
                 DebugLog(@"user link %@",[user objectForKey:@"link"]);
                 DebugLog(@"user name %@",[user objectForKey:@"username"]);
                 DebugLog(@"user email %@",[user objectForKey:@"email"]);
                 DebugLog(@"user gender %@",[user objectForKey:@"gender"]);
                 
                 facebook_useridString =[user objectForKey:@"id"];
                 facebook_accesstokenString = [FBSession.activeSession.accessTokenData accessToken];
                 isThisFBProfileImage = YES;
                 
                 NSString *customerIDString = [user objectForKey:@"id"];
                 NSString *nameString       = [user objectForKey:@"name"];
                 NSString *dobString        = [user objectForKey:@"birthday"];
                 NSString *emailString      = [user objectForKey:@"email"];
                 NSString *cityString       = [[user objectForKey:@"location"] objectForKey:@"name"];
                 DebugLog(@"%@",[[user objectForKey:@"location"] objectForKey:@"name"]);
                 NSString *genderString     = [user objectForKey:@"gender"];

                 if (customerIDString != nil) {
//                     [customerProfileImageView setImageWithURL:[NSURL URLWithString:FBUSER_PIC(customerIDString)] placeholderImage:[UIImage imageNamed:@"unknown.jpg"]];
                     DebugLog(@"%@", FBUSER_PIC(customerIDString));
                     NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:FBUSER_PIC(customerIDString)]];
                     customerProfileImageView.image = [UIImage imageWithData:data];
                 }
                 if (dobString != nil) {
                     NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                     [dateFormatter setDateFormat:@"MM/dd/yyyy"];
//                     NSDate *myDate = [[NSDate alloc] init];
                     selectedDOBDate = [dateFormatter dateFromString:dobString];
                     DebugLog(@"myDate %@",selectedDOBDate);

                     dobString = [self getDateStringFromDate:selectedDOBDate];
                     DebugLog(@"dobString1 %@",dobString);
                     
                     BOOL NOTFOUND = YES;
                     ProfileEvent *eventObj = nil;
                     for (int index = 0; index < eventsArray.count; index++) {
                         eventObj = (ProfileEvent *)[eventsArray objectAtIndex:index];
                         if ([eventObj.dateCategory isEqualToString:@"Birthday"]) {
                             eventObj.date = dobString;
                             eventObj.dateCategory = @"Birthday";
                             [eventsArray replaceObjectAtIndex:index withObject:eventObj];
                             NOTFOUND = NO;
                             break;
                         }
                     }
                     if (NOTFOUND) {
                         eventObj = [[ProfileEvent alloc]init];
                         eventObj.date = dobString;
                         eventObj.dateCategory = @"Birthday";
                         [eventsArray addObject:eventObj];
                     }
                     [self reloadEventsTableView];
                     eventObj = nil;
                 }
//                 dobBtn.titleLabel.text = dobString;
                 nameText.text  = nameString;
                 emailText.text = emailString;
                 cityText.text  = cityString;
                 if ([[genderString lowercaseString] hasPrefix:@"female"]) {
                     [self femaleBtnPressed:nil];
                 }else{
                     [self maleBtnPressed:nil];
                 }
                 if (self.editing) {
                     [self.lblSelectedGender setHidden:YES];
                     [self.maleBtn setHidden:NO];
                     [self.femaleBtn setHidden:NO];
                 }else{
                     [self.lblSelectedGender setHidden:NO];
                     [self.maleBtn setHidden:YES];
                     [self.femaleBtn setHidden:YES];
                 }
                 DebugLog(@"send user_interest request");
                 //[self sendUserInterest];
             }
         }];
    } else {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        DebugLog(@"Login: User Logged Out");
    }
}

#pragma AFNetworking Methods
-(void)sendDateCategoriesListRequest
{
    DebugLog(@"========================sendDateCategoriesListRequest========================");

    DebugLog(@"user_id %@ auth_id %@",[INUserDefaultOperations getCustomerAuthId],[INUserDefaultOperations getCustomerAuthCode]);

    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:GET_DATE_CATEGORY_LIST]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setValue:[INUserDefaultOperations getCustomerAuthId] forHTTPHeaderField:@"user_id"];
    [urlRequest setValue:[INUserDefaultOperations getCustomerAuthCode] forHTTPHeaderField:@"auth_id"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        //[hud hide:YES];
                                                        DebugLog(@"%@",self.splashJson);
                                                        if(self.splashJson  != nil)
                                                        {
                                                            [dateCategoriesDict removeAllObjects];
                                                            DebugLog(@"--->%@",[self.splashJson  objectForKey:@"success"]);
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSDictionary *maindict = [self.splashJson  objectForKey:@"data"];
                                                                NSArray *dateCatgArray = [maindict objectForKey:@"date_categories"];
                                                                for (int index = 0; index < [dateCatgArray count]; index++) {
                                                                    NSDictionary *dict = [dateCatgArray objectAtIndex:index];
                                                                    NSString *dateId = [dict objectForKey:@"id"];
                                                                    NSString *dateCategory = [dict objectForKey:@"name"];
                                                                    [dateCategoriesDict setObject:dateId forKey:dateCategory];
                                                                }
                                                                
                                                                if (dateEventsArray != nil && dateEventsArray.count > 0 && self.dateCategoriesDict.count > 0)  {
                                                                    [eventsArray removeAllObjects];
                                                                    for (NSDictionary *dict in dateEventsArray) {
                                                                        NSString *date = [dict objectForKey:@"date"];
                                                                        NSString *dateCategoryType = [dict objectForKey:@"date_category"];
                                                                        NSString *dateCategoryTypeKey = [[self.dateCategoriesDict allKeysForObject:dateCategoryType] objectAtIndex:0];
                                                                        
                                                                        DebugLog(@"[[self.dateCategoriesDict allKeysForObject:dateCategoryType] objectAtIndex:0] %@",dateCategoryTypeKey);
                                                                        if ([date isEqualToString:@"0000-00-00"] && [dateCategoryType isEqualToString:@"1"]) {
                                                                            DebugLog(@"Got 0000-00-00 date for DOB");
                                                                        }else{
                                                                            ProfileEvent *eventObj = [[ProfileEvent alloc] init];
                                                                            eventObj.date = date;
                                                                            eventObj.dateCategory = dateCategoryTypeKey;
                                                                            [eventsArray addObject:eventObj];
                                                                        }
                                                                    }
                                                                    [dateEventsArray removeAllObjects];
                                                                    dateEventsArray = nil;
                                                                    [self reloadEventsTableView];
                                                                }
                                                                if (eventsArray.count <= 0){
                                                                    [self initializeEventsArray];
                                                                    [INUserDefaultOperations setCustomerSpecialDatesState:TRUE];
//                                                                    [self addEventsBtnPressed:nil];
                                                                } else {
                                                                    [INUserDefaultOperations setCustomerSpecialDatesState:FALSE];
                                                                }
                                                                [CommonCallback hideProgressHud];
                                                            } else {
                                                                [CommonCallback hideProgressHud];
                                                            }
                                                        } else {
                                                            [CommonCallback hideProgressHud];
                                                        }

                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                        [av show];

                                                    }];



    [operation start];
}

-(void)sendGetCustomerProfileListRequest
{
    DebugLog(@"========================sendGetCustomerProfileListRequest========================");

    DebugLog(@"user_id %@ auth_id %@",[INUserDefaultOperations getCustomerAuthId],[INUserDefaultOperations getCustomerAuthCode]);
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:GET_CUSTOMER_PROFILE_URL([INUserDefaultOperations getCustomerAuthId])]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setValue:[INUserDefaultOperations getCustomerAuthId] forHTTPHeaderField:@"user_id"];
    [urlRequest setValue:[INUserDefaultOperations getCustomerAuthCode] forHTTPHeaderField:@"auth_id"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        //[hud hide:YES];
                                                        DebugLog(@"response : %@",self.splashJson);
                                                        if(self.splashJson  != nil)
                                                        {
                                                            DebugLog(@"--->%@",[self.splashJson  objectForKey:@"success"]);
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSDictionary *maindict = [self.splashJson  objectForKey:@"data"];
                                                                nameText.text = [maindict objectForKey:@"name"];
                                                                emailText.text = [maindict objectForKey:@"email"];
                                                                cityText.text = [maindict objectForKey:@"city"];
                                                                
                                                                lblSelectedGender.text = [[maindict objectForKey:@"gender"] lowercaseString];
                                                                if ([[lblSelectedGender.text lowercaseString] hasPrefix:@"female"]) {
                                                                    [self femaleBtnPressed:nil];
                                                                }else{
                                                                    [self maleBtnPressed:nil];
                                                                }
                                                                if (self.editing) {
                                                                    [self.lblSelectedGender setHidden:YES];
                                                                    [self.maleBtn setHidden:NO];
                                                                    [self.femaleBtn setHidden:NO];
                                                                }else{
                                                                    [self.lblSelectedGender setHidden:NO];
                                                                    [self.maleBtn setHidden:YES];
                                                                    [self.femaleBtn setHidden:YES];
                                                                }
                                                                NSString *imageUrl = [maindict objectForKey:@"image_url"];
                                                                if (imageUrl != nil && ![imageUrl isEqualToString:@""]) {
                                                                    if ([imageUrl hasPrefix:@"http"]) {
//                                                                        [customerProfileImageView setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"unknown.jpg"]];
                                                                        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrl]];
                                                                        customerProfileImageView.image = [UIImage imageWithData:data];
                                                                    }else{
                                                                        DebugLog(@"image_url -%@-",PROFILE_IMAGE_LINK(imageUrl));

//                                                                        [customerProfileImageView setImageWithURL:[NSURL URLWithString:PROFILE_IMAGE_LINK(imageUrl)] placeholderImage:[UIImage imageNamed:@"unknown.jpg"]];
                                                                        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:PROFILE_IMAGE_LINK(imageUrl)]];
                                                                        customerProfileImageView.image = [UIImage imageWithData:data];
                                                                    }
                                                                }
                                                                
                                                               NSArray *dateEventsArray1 = [maindict objectForKey:@"dates"];
                                                                dateEventsArray = [[NSMutableArray alloc] initWithArray:dateEventsArray1];
                                                                if (dateEventsArray.count > 0 && self.dateCategoriesDict.count > 0)
                                                                {
                                                                    [eventsArray removeAllObjects];
                                                                    for (NSDictionary *dict in dateEventsArray) {
                                                                        NSString *date = [dict objectForKey:@"date"];
                                                                        NSString *dateCategoryType = [dict objectForKey:@"date_category"];
                                                                        NSString *dateCategoryTypeKey = [[self.dateCategoriesDict allKeysForObject:dateCategoryType] objectAtIndex:0];

                                                                        DebugLog(@"[[self.dateCategoriesDict allKeysForObject:dateCategoryType] objectAtIndex:0] %@",dateCategoryTypeKey);
                                                                        if ([date isEqualToString:@"0000-00-00"] && [dateCategoryType isEqualToString:@"1"]) {
                                                                            DebugLog(@"Got 0000-00-00 date for DOB");
                                                                        }else{
                                                                            ProfileEvent *eventObj = [[ProfileEvent alloc] init];
                                                                            eventObj.date = date;
                                                                            eventObj.dateCategory = dateCategoryTypeKey;
                                                                            [eventsArray addObject:eventObj];
                                                                        }
                                                                    }
                                                                    [dateEventsArray removeAllObjects];
                                                                    dateEventsArray = nil;
                                                                    [self reloadEventsTableView];
                                                                }
                                                                if (eventsArray.count <= 0){
                                                                    [self initializeEventsArray];
                                                                    [INUserDefaultOperations setCustomerSpecialDatesState:TRUE];
                                                                    //                                                                    [self addEventsBtnPressed:nil];
                                                                } else {
                                                                    [INUserDefaultOperations setCustomerSpecialDatesState:FALSE];
                                                                }
                                                                [CommonCallback hideProgressHud];
                                                            }
                                                            else
                                                            {
                                                                if([self.splashJson  objectForKey:@"code"] != [NSNull null] && [[self.splashJson  objectForKey:@"code"] isEqualToString:CUSTOMER_INVALID_USER_AUTH_CODE]) {
                                                                    DebugLog(@"Got Invalid Authentication Code.");
                                                                    [INUserDefaultOperations showSIAlertView:CUSTOMER_INVALID_USER_AUTH_MESSAGE];
                                                                    [INUserDefaultOperations clearCustomerDetails];
                                                                    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:IN_CUSTOMER_LOGIN_LOGOUT_KEY];
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_LOGIN_LOGOUT_NOTIFICATION object:nil userInfo:dictionary];
                                                                }
                                                                else if([self.splashJson  objectForKey:@"message"] != [NSNull null] && ![[self.splashJson objectForKey:@"message"] isEqualToString:@""]){
                                                                    NSString *message = [self.splashJson objectForKey:@"message"];
                                                                    [INUserDefaultOperations showSIAlertView:message];
                                                                }
                                                                [CommonCallback hideProgressHud];
                                                            }
                                                        } else {
                                                             [CommonCallback hideProgressHud];
                                                        }
                                                        [self.fbLoginBtn setUserInteractionEnabled:TRUE];
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                        [av show];
                                                        [self.fbLoginBtn setUserInteractionEnabled:TRUE];
                                                    }];
    [operation start];
}

-(NSMutableArray *)getDateCategoryValues{
    //"dates":[{"date_type":"1","date":"1990-12-05"},{"date_type":"2","date":"2015-11-01"},{"date_type":"3","date":"1969-12-31"}]
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (ProfileEvent *eventObj in eventsArray) {
        if (eventObj.date != nil && ![eventObj.date isEqualToString:@""]  && eventObj.dateCategory != nil && ![eventObj.dateCategory isEqualToString:@""]) {
            NSString *typeId = [self.dateCategoriesDict objectForKey:eventObj.dateCategory];
            NSDictionary *dic1 = [[NSDictionary alloc] initWithObjectsAndKeys:typeId,@"date_type",eventObj.date,@"date",nil];
            [array addObject:dic1];
            if ([eventObj.dateCategory isEqualToString:@"Birthday"] && eventObj.dateVal != nil) {
                [INEventLogger setAge:[INUserDefaultOperations getAgeInYears:eventObj.dateVal]];
            }
        }
    }
//    if (selectedDOBDate != nil) {
//        NSString *dobString = [self getDateStringFromDate:selectedDOBDate];
//        NSString *typeId = [self.dateCategoriesDict objectForKey:@"DOB"];
//        NSDictionary *dic1 = [[NSDictionary alloc] initWithObjectsAndKeys:typeId,@"date_type",dobString,@"date",nil];
//        [array addObject:dic1];
//    }
//    NSDictionary *dic1 = [[NSDictionary alloc] initWithObjectsAndKeys:@"1",@"date_type",@"1989-02-22",@"date",nil];
//    NSDictionary *dic2 = [[NSDictionary alloc] initWithObjectsAndKeys:@"2",@"date_type",@"1985-04-02",@"date",nil];
//    [array addObject:dic1];
//    [array addObject:dic2];
    return array;
}


-(void)sendUpdateProfileRequest
{
    DebugLog(@"========================sendUpdateProfileRequest========================");
    [CommonCallback showProgressHud:@"Updating" subtitle:HUD_SUBTITLE];

    NSString *areaID = [IN_APP_DELEGATE getActiveShoplocalPlaceIdFromAreaTable];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"X-HTTP-Method-Override" value:@"PUT"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];

    NSMutableDictionary *postparams = [[NSMutableDictionary alloc] init];
    
    if (eventsArray.count <= 0) {
        [postparams setObject:@"" forKey:@"dates"];
    }else{
        NSMutableArray  *datecatgArray = [self getDateCategoryValues];
        DebugLog(@"datecatgArray %@",datecatgArray);
        if (datecatgArray.count <= 0) {
            [postparams setObject:@"" forKey:@"dates"];
        }else{
            [postparams setObject:datecatgArray forKey:@"dates"];
        }
    }
    if ([nameText.text length] > 0) {
        [postparams setObject:nameText.text forKey:@"name"];
    }
    if ([emailText.text length] > 0) {
        [postparams setObject:emailText.text forKey:@"email"];
    }
    if ([cityText.text length] > 0) {
        [postparams setObject:cityText.text forKey:@"city"];
    }
    if ([lblSelectedGender.text length] > 0) {
        [postparams setObject:[self.lblSelectedGender.text lowercaseString] forKey:@"gender"];
    }
    DebugLog(@"facebook_useridString %@ facebook_accesstokenString %@",facebook_useridString,facebook_accesstokenString);

    if (facebook_useridString != nil && ![facebook_useridString isEqualToString:@""]) {
        [postparams setObject:facebook_useridString forKey:@"facebook_user_id"];
    }
    if (facebook_accesstokenString != nil && ![facebook_accesstokenString isEqualToString:@""]) {
        [postparams setObject:facebook_accesstokenString forKey:@"facebook_access_token"];
    }
    
    if ([customerProfileImageView.image isEqual:placeholderImage]) {
        DebugLog(@"Got placeholder image :)");
    }else{
//        if (facebook_useridString != nil && ![facebook_useridString isEqualToString:@""]) {
//            [postparams setObject:FBUSER_PIC(facebook_useridString) forKey:@"image_url"];
//        }else{
//            NSData *dataObj = UIImageJPEGRepresentation(customerProfileImageView.image,0.75);
//            NSString *image = [IN_APP_DELEGATE base64forData:dataObj];
//            [postparams setObject:image forKey:@"userfile"];
//            [postparams setObject:@"userprofile.jpg" forKey:@"filename"];
//            [postparams setObject:@"image/jpeg" forKey:@"filetype"];
//
//        }
        if (isThisFBProfileImage && facebook_useridString != nil && ![facebook_useridString isEqualToString:@""]) {
            [postparams setObject:FBUSER_PIC(facebook_useridString) forKey:@"image_url"];
        }else{
            NSData *dataObj = UIImageJPEGRepresentation(customerProfileImageView.image,0.75);
            NSString *image = [IN_APP_DELEGATE base64forData:dataObj];
            [postparams setObject:image forKey:@"userfile"];
            [postparams setObject:@"userprofile.jpg" forKey:@"filename"];
            [postparams setObject:@"image/jpeg" forKey:@"filetype"];

        }
    }
    [postparams setObject:areaID forKey:@"active_area"];

    [postparams setObject:[INUserDefaultOperations getCustomerAuthId] forKey:@"user_id"];
    [postparams setObject:[INUserDefaultOperations getCustomerAuthCode] forKey:@"auth_id"];
    DebugLog(@"postParams -%@-",postparams);
    
    [httpClient postPath:UPDATE_CUSTOMER_PROFILE_POST_URL parameters:postparams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json afnetworking ->%@",json);
        if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"true"])
        {
//            [INUserDefaultOperations showAlert:@"Your profile has been updated successfully."];
            sialertView.message = @"Awesome! You can now Shoplocal";
            [sialertView show];
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            if([json  objectForKey:@"code"] != [NSNull null] && [[json  objectForKey:@"code"] isEqualToString:CUSTOMER_INVALID_USER_AUTH_CODE]) {
                DebugLog(@"Got Invalid Authentication Code.");
                [INUserDefaultOperations showSIAlertView:CUSTOMER_INVALID_USER_AUTH_MESSAGE];
                [INUserDefaultOperations clearCustomerDetails];
                NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:IN_CUSTOMER_LOGIN_LOGOUT_KEY];
                [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_LOGIN_LOGOUT_NOTIFICATION object:nil userInfo:dictionary];
            }
            else if([json objectForKey:@"message"] != [NSNull null]){
                sialertView.message = [json objectForKey:@"message"];
                [sialertView show];
            }
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[INUserDefaultOperations showAlert:error.localizedDescription];
    }];
}



#pragma mark - Private Methods

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
	[super setEditing:editing animated:animated];
//	[self.gridView setEditing:editing animated:animated];
	
    [UIView animateWithDuration:ANIMATION_DELAY_DURATION animations:^{
        if (self.editing) {
            
//            if ([[INUserDefaultOperations getCustomerLoginFrom] isEqualToString:FACEBOOKSERVER]) {
//                [customerProfileImageView setFrame:CGRectMake(customerProfileImageView.frame.origin.x,fbLoginBtn.frame.origin.y,
//                                                              customerProfileImageView.frame.size.width,
//                                                              customerProfileImageView.frame.size.height)];
//            }else{
                [customerProfileImageView setFrame:CGRectMake(customerProfileImageView.frame.origin.x,CGRectGetMaxY(lblOR.frame)+10,
                                                              customerProfileImageView.frame.size.width,
                                                              customerProfileImageView.frame.size.height)];
//            }
            
            [chooseImageBtn setFrame:CGRectMake(chooseImageBtn.frame.origin.x,CGRectGetMaxY(customerProfileImageView.frame)+2,
                                                          chooseImageBtn.frame.size.width,
                                                          chooseImageBtn.frame.size.height)];
            
            [img1Seperator setFrame:CGRectMake(img1Seperator.frame.origin.x,CGRectGetMaxY(chooseImageBtn.frame)+5,
                                               img1Seperator.frame.size.width,
                                               img1Seperator.frame.size.height)];
            
        }else{
            [customerProfileImageView setFrame:CGRectMake(customerProfileImageView.frame.origin.x,15,
                                                          customerProfileImageView.frame.size.width,
                                                          customerProfileImageView.frame.size.height)];
            [img1Seperator setFrame:CGRectMake(img1Seperator.frame.origin.x,CGRectGetMaxY(customerProfileImageView.frame)+5,
                                               img1Seperator.frame.size.width,
                                               img1Seperator.frame.size.height)];
        }

            [nameText setFrame:CGRectMake(nameText.frame.origin.x,CGRectGetMaxY(img1Seperator.frame)+2,
                                               nameText.frame.size.width,
                                               nameText.frame.size.height)];

            [img2Seperator setFrame:CGRectMake(img2Seperator.frame.origin.x,CGRectGetMaxY(nameText.frame)+2,
                                               img2Seperator.frame.size.width,
                                               img2Seperator.frame.size.height)];
        
            [lblSelectedGender setFrame:CGRectMake(lblSelectedGender.frame.origin.x,CGRectGetMaxY(img2Seperator.frame)+8,
                                     lblSelectedGender.frame.size.width,
                                     lblSelectedGender.frame.size.height)];
        
            [maleBtn setFrame:CGRectMake(maleBtn.frame.origin.x,CGRectGetMaxY(img2Seperator.frame)+2,
                                          maleBtn.frame.size.width,
                                          maleBtn.frame.size.height)];
        
            [femaleBtn setFrame:CGRectMake(femaleBtn.frame.origin.x,CGRectGetMaxY(img2Seperator.frame)+2,
                                          femaleBtn.frame.size.width,
                                          femaleBtn.frame.size.height)];
            
            [img3Seperator setFrame:CGRectMake(img3Seperator.frame.origin.x,CGRectGetMaxY(maleBtn.frame)+2,
                                               img3Seperator.frame.size.width,
                                               img3Seperator.frame.size.height)];
            
            [emailText setFrame:CGRectMake(emailText.frame.origin.x,CGRectGetMaxY(img3Seperator.frame)+2,
                                          emailText.frame.size.width,
                                          emailText.frame.size.height)];
            
            [img4Seperator setFrame:CGRectMake(img4Seperator.frame.origin.x,CGRectGetMaxY(emailText.frame)+2,
                                               img4Seperator.frame.size.width,
                                               img4Seperator.frame.size.height)];
            
            [cityText setFrame:CGRectMake(cityText.frame.origin.x,CGRectGetMaxY(img4Seperator.frame)+2,
                                          cityText.frame.size.width,
                                          cityText.frame.size.height)];
            
            [img5Seperator setFrame:CGRectMake(img5Seperator.frame.origin.x,CGRectGetMaxY(cityText.frame)+2,
                                               img5Seperator.frame.size.width,
                                               img5Seperator.frame.size.height)];
            
            [lbleventsHeader setFrame:CGRectMake(lbleventsHeader.frame.origin.x,CGRectGetMaxY(img5Seperator.frame)+15,
                                          lbleventsHeader.frame.size.width,
                                          lbleventsHeader.frame.size.height)];
            
            [img6Seperator setFrame:CGRectMake(img6Seperator.frame.origin.x,CGRectGetMaxY(lbleventsHeader.frame)+2,
                                               img6Seperator.frame.size.width,
                                               img6Seperator.frame.size.height)];
            
            [eventsTableView setFrame:CGRectMake(eventsTableView.frame.origin.x,CGRectGetMaxY(img6Seperator.frame)+5,
                                               eventsTableView.frame.size.width,
                                               eventsTableView.frame.size.height)];
        
            double nextYaxis = CGRectGetMaxY(eventsTableView.frame)+7;
            if (eventsArray.count < 7) {
                [addEventsBtn setHidden:NO];
                [addEventsBtn setFrame:CGRectMake(addEventsBtn.frame.origin.x,
                                                  nextYaxis,
                                                  addEventsBtn.frame.size.width,
                                                  addEventsBtn.frame.size.height)];
                
                nextYaxis = CGRectGetMaxY(addEventsBtn.frame)+10;
            }else{
                [addEventsBtn setHidden:YES];
            }
            [img7Seperator setFrame:CGRectMake(img7Seperator.frame.origin.x,
                                               nextYaxis,
                                               img7Seperator.frame.size.width,
                                               img7Seperator.frame.size.height)];
            [updateProfileBtn setFrame:CGRectMake(updateProfileBtn.frame.origin.x,
                                                  CGRectGetMaxY(img7Seperator.frame),
                                                  updateProfileBtn.frame.size.width,
                                                  updateProfileBtn.frame.size.height)];
        
            double tempcontentSizeHeight = contentSizeHeight;
//        if (eventsArray.count > 0) {
//            tempcontentSizeHeight = contentSizeHeight + ((eventsArray.count-1) * 40);
//        }
            if (!self.editing) {
                tempcontentSizeHeight =  CGRectGetMaxY(eventsTableView.frame)+10;
            }else{
                tempcontentSizeHeight =  CGRectGetMaxY(updateProfileBtn.frame)+10;
            }
            [addDetailsScrollView setContentSize:CGSizeMake(self.addDetailsScrollView.frame.size.width,tempcontentSizeHeight)];
        
            if (editing) {
//                if ([[INUserDefaultOperations getCustomerLoginFrom] isEqualToString:FACEBOOKSERVER]) {
//                    fbLoginBtn.hidden = TRUE;
//                    lblOR.hidden = TRUE;
//                }else{
                    fbLoginBtn.hidden = FALSE;
                    lblOR.hidden = FALSE;
//                }
                chooseImageBtn.hidden = FALSE;
                
                nameText.userInteractionEnabled = TRUE;
                emailText.userInteractionEnabled = TRUE;
                cityText.userInteractionEnabled = TRUE;
                
                maleBtn.userInteractionEnabled = TRUE;
                femaleBtn.userInteractionEnabled = TRUE;
                
                eventsTableView.userInteractionEnabled = TRUE;
                img7Seperator.hidden = FALSE;
                updateProfileBtn.hidden = FALSE;
                
                [self.maleBtn setHidden:NO];
                [self.femaleBtn setHidden:NO];
                [self.lblSelectedGender setHidden:YES];
                
            }else {
                
                fbLoginBtn.hidden = TRUE;
                lblOR.hidden = TRUE;
                chooseImageBtn.hidden = TRUE;
                
                nameText.userInteractionEnabled = FALSE;
                emailText.userInteractionEnabled = FALSE;
                cityText.userInteractionEnabled = FALSE;
                
                maleBtn.userInteractionEnabled = FALSE;
                femaleBtn.userInteractionEnabled = FALSE;
                
                eventsTableView.userInteractionEnabled = FALSE;
                addEventsBtn.hidden = TRUE;
                img7Seperator.hidden = TRUE;
                updateProfileBtn.hidden = TRUE;
                
                [self.lblSelectedGender setHidden:NO];
                [self.maleBtn setHidden:YES];
                [self.femaleBtn setHidden:YES];
            }
            [eventsTableView reloadData];
        } completion:^(BOOL finished) {
        }];
}


-(void)sendUserInterest
{
    DebugLog(@"========================sendUserInterest========================");
    
    NSString *accessToken = [[FBSession.activeSession accessTokenData] accessToken];
     
     AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"https://graph.facebook.com/"]];
//     [httpClient setParameterEncoding:AFJSONParameterEncoding];
//     [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    
     NSMutableDictionary *postparams = [[NSMutableDictionary alloc] init];
     [postparams setObject:accessToken forKey:@"access_token"];

     DebugLog(@"postParams -%@-",postparams);
     
     [httpClient getPath:@"me/interests" parameters:postparams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json afnetworking ->%@",json);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        [INUserDefaultOperations showAlert:error.localizedDescription];
    }];

}

@end
