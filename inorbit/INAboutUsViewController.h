//
//  INAboutUsViewController.h
//  shoplocal
//
//  Created by Rishi on 30/09/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface INAboutUsViewController : UIViewController<MFMailComposeViewControllerDelegate,UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *aboutusTableView;
@end
