//
//  SideMenuViewController.h
//  MFSideMenuDemo
//
//  Created by Michael Frederick on 3/19/12.

#import <UIKit/UIKit.h>
#import "MFSideMenu.h"


#define LAUNCH_SCREEN_IS_OFFERS 1
#define LAUNCH_SCREEN_IS_LOCATION 2
#define LAUNCH_SCREEN_IS_SPLASHSCREEN 3
#define LAUNCH_SCREEN_IS_ALL_BUSINESS 4

/////////////////////////////////////////////////////////////////
//#define SECTION0 @"  SHOPLOCAL"
//#define OFFERS @"Offers"
//#define OFFER_CATEGORIES @"Offer Categories"
//#define ALL_STORES @"All Stores"
//#define STORE_CATEGORIES @"Store Categories"
/////////////////////////////////////////////////////////////////
//#define SECTION1 @"  MY SHOPLOCAL"
//#define LOGIN @"Login"
//#define MY_PROFILE @"My Profile"
//#define MY_SHOPLOCAL_OFFERS @"My Shoplocal Offers"
//#define FAV_STORE @"Favourite Stores"
/////////////////////////////////////////////////////////////////
//#define SECTION2 @"  OTHER STUFF"
//#define SETTINGS @"Settings"
//#define NEWS_EVENTS @"News & Events"
//#define CHANGE_LOCATION @"Change Location"
//#define WELCOME_SCREEN @"Welcome Screen"
//#define LOGIN_TO_BUSINESS @"Login to Business"
/////////////////////////////////////////////////////////////////
//#define SECTION3 @"  ABOUT SHOPLOCAL"
//#define ABOUTUS @"About Shoplocal"
//#define CONTACTUS @"Contact Us"
//#define FACEBOOK @"Facebook"
//#define TWITTER @"Twitter"
/////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////
//#define SECTION0 @"  SHOPLOCAL"
//#define ALL_BUSINESS @"Market Place"
//#define DISCOVER @"Offers Stream"
//#define MY_SHOPLOCAL @"My Shoplocal"
/////////////////////////////////////////////////////////////////
//#define SECTION1 @"  PERSONALISE"
//#define LOGIN @"Login"
//#define MY_PROFILE @"My Profile"
//#define CHANGE_LOCATION @"Change Location"
//#define SETTINGS @"Settings"
/////////////////////////////////////////////////////////////////
//#define SECTION2 @"  BUSINESS"
//#define MYFAV_BUSINESS @"My Favourite Business"
/////////////////////////////////////////////////////////////////
//#define SECTION3 @"  ABOUT SHOPLOCAL"
//#define TOUR @"Tour"
//#define ABOUTUS @"About Shoplocal"
//#define CONTACTUS @"Contact Us"
//#define FACEBOOK @"Facebook"
//#define TWITTER @"Twitter"
//#define SHARE_THIS_APP @"Share This App"
/////////////////////////////////////////////////////////////////
//#define SECTION4 @"  SELL"
//#define SETUP_BUSINESS @"Setup Your Business"
//#define SWITCH_TO_BUSINESS @"Business DashBoard"
/////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////
#define SECTION0 @"  SHOPLOCAL"
#define ALL_BUSINESS @"Market Place"
#define DISCOVER @"Offers Stream"
#define MY_SHOPLOCAL @"My Shoplocal"
#define SHARE_THIS_APP @"Invite Neighbours"
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
#define SECTION1 @"Settings"
///////////////////////////////////////////////////////////////

@interface SideMenuViewController : UIViewController {
//    int currentSectionIndex;
//    int preSectionIndex;
//    int currentRowIndex;
//    int preRowIndex;
    int initialAnimation;
}

@property (nonatomic,copy) NSString *current;
@property (nonatomic,copy) NSString *previous;

@property (nonatomic,readwrite) int launchscreenType;
@property (nonatomic, retain) NSMutableDictionary *sideMenuDict;
@property (nonatomic, assign) MFSideMenu *sideMenu;
@end