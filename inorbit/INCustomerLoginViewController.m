//
//  INCustomerLoginViewController.m
//  shoplocal
//
//  Created by Rishi on 20/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INCustomerLoginViewController.h"
#import "INCustomerSignUpViewController.h"
#import "constants.h"
#import "CommonCallback.h"
#import "INCountryCodeObj.h"
#import "ActionSheetPicker.h"
#import "INAppDelegate.h"
#import "INPrivacyPolicyViewController.h"
#import "SIAlertView.h"
#import "UIView+Genie.h"

#define LOGIN_CUSTOMER [NSString stringWithFormat:@"%@%@user_api/validate_credentials",URL_PREFIX,API_VERSION]
#define MOBILE_NUMBER(CODE,NUMBER)  [NSString stringWithFormat:@"%@%@",[CODE stringByReplacingOccurrencesOfString:@"+" withString:@""],NUMBER]
#define REGISTER_CUSTOMER [NSString stringWithFormat:@"%@%@%@user_api/new_user",LIVE_SERVER,URL_PREFIX,API_VERSION]

#define FBUSER_LOGIN_TO_SERVER [NSString stringWithFormat:@"%@%@user_api/validate_credentials",URL_PREFIX,API_VERSION]


@interface INCustomerLoginViewController ()
@property (nonatomic) BOOL viewIsIn;
@end

@implementation INCustomerLoginViewController
@synthesize usernameText;
@synthesize passwordtext;
@synthesize codeText;
@synthesize loginBtn;
@synthesize signupBtn;
@synthesize forgotBtn;
@synthesize lblHeader,lblfirsttimeuser;
@synthesize signUpView,loginView,loginQuesBtn;
@synthesize countryCodeArray;
@synthesize countryCodePickerArray;
@synthesize infoView,infoPrivacyBtn,infoOKBtn,lblinfoHeader,lblinfoContent;
@synthesize delegate;
@synthesize infoBtn;
@synthesize loginType;
@synthesize loginView1,fbLoginBtn,signUpShoplocalBtn;
@synthesize txtViewinfoContent;
@synthesize customerImageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Customer Login";
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [storeButton setHidden:TRUE];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    UIButton *moreButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [moreButton setHidden:FALSE];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    DebugLog(@"viewDidAppear");
//    DebugLog(@"isOpenedTermsViewWhileSignUp %d",isOpenedTermsViewWhileSignUp);
//    DebugLog(@"isRetryingToAcceptTerms %d",isRetryingToAcceptTerms);
//    if (isOpenedTermsViewWhileSignUp) {
//        isOpenedTermsViewWhileSignUp = FALSE;
//        if([usernameText.text length] ==  10 && [[INUserDefaultOperations getCustomerTermsConditionsValue] isEqualToString:@"1"])
//        {
//            [self signupUser];
//        }else{
//            [self showTermsRetryAlert];
//        }
//    }
//    if (isRetryingToAcceptTerms) {
//        isRetryingToAcceptTerms = FALSE;
//        if([usernameText.text length] ==  10 && [[INUserDefaultOperations getCustomerTermsConditionsValue] isEqualToString:@"1"])
//        {
//            [self signupUser];
//        }else if([usernameText.text length] ==  10 && [[INUserDefaultOperations getCustomerTermsConditionsValue] isEqualToString:@"0"]){
//            [self showTermsRetryAlert];
//        }
//    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[self navigationController] setNavigationBarHidden:NO animated:NO];

    if (delegate == nil) {
        [self setupMenuBarButtonItems];
    }
    isSendingFBLoginRequest = NO;
    if (loginType == LOGIN_TYPE_SHOPLOCAL) {
        [loginView1 setHidden:TRUE];
        [self signUpShoplocalBtnPressed:nil];
    }else if (loginType == LOGIN_TYPE_FB) {
        [loginView1 setHidden:FALSE];
        [self.view bringSubviewToFront:loginView1];
        [self.view bringSubviewToFront:infoView];
        [self fbLoginBtnPressed:nil];
    }
    else{
        [loginView1 setHidden:FALSE];
        [self.view bringSubviewToFront:loginView1];
        [self.view bringSubviewToFront:infoView];
    }

    isOpenedTermsViewWhileSignUp = FALSE;
    isRetryingToAcceptTerms = FALSE;
    countryCodeArray = [[NSMutableArray alloc] init];
    countryCodePickerArray = [[NSMutableArray alloc] init];
    [self setUI];
    
//    infoBtn.layer.transform = CATransform3DMakeScale(0.80, 0.80, 1);
//    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform"];
//	animation.toValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
//	animation.autoreverses = YES;
//	animation.duration = 0.40;
//	animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//	animation.repeatCount = HUGE_VALF;
//	[infoBtn.layer addAnimation:animation forKey:@"pulseAnimation"];
    
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    viewTap.numberOfTapsRequired = 1;
    viewTap.cancelsTouchesInView = NO;
    [self.view  addGestureRecognizer:viewTap];
    //[self addHUDView];
    if([[INUserDefaultOperations getCustomerLoginFrom] isEqualToString:SHOPLOCALSERVER] && ![[INUserDefaultOperations getCustomerId] isEqualToString:@""])
    {
        usernameText.text = [INUserDefaultOperations getCustomerId];
    }
    NSError* error;
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"country_code" ofType:@"txt"];
    if (filePath) {
        NSString *data = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:[data dataUsingEncoding:NSUTF8StringEncoding]  options:kNilOptions error:&error];
        
        for(NSDictionary *obj in json) {
            INCountryCodeObj *codeObj = [[INCountryCodeObj alloc] init];
            codeObj.countryName = [obj objectForKey:@"name"];
            codeObj.countrycode = [obj objectForKey:@"code"];
            codeObj.countrydialcode = [obj objectForKey:@"dial_code"];
            [countryCodeArray addObject:codeObj];
            [countryCodePickerArray addObject:[NSString stringWithFormat:@"%@ (%@)",[obj objectForKey:@"name"],[obj objectForKey:@"dial_code"]]];
            codeObj = nil;
        }
    }
    
    if([[INUserDefaultOperations getCustomerCountryCode] isEqualToString:@""])
    {
//        NSLocale *locale = [NSLocale currentLocale];
//        NSString *countryCode = [locale objectForKey: NSLocaleCountryCode];
//        DebugLog(@"countryCode:%@", countryCode);
//        NSString *country = [locale displayNameForKey: NSLocaleCountryCode value: countryCode];
//        DebugLog(@"country:%@", country);
//        
//        for (int i=0; i < [countryCodeArray count] ; i++) {
//            INCountryCodeObj *codeObj = [countryCodeArray objectAtIndex:i];
//            DebugLog(@"%@",codeObj.countrycode);
//            if([codeObj.countrycode isEqualToString:countryCode])
//            {
//                [codeText setText:codeObj.countrydialcode];
//                break;
//            }
//        }
        if([codeText.text isEqualToString:@""])
        {
            [codeText setText:@"+91"];
        }
    } else {
        codeText.text = [INUserDefaultOperations getCustomerCountryCode];
    }
    
    if([codeText.text isEqualToString:@""])
    {
        [codeText setText:@"+91"];
    }
    DebugLog(@"%@",codeText.text);
    
    self.viewIsIn = TRUE;
    
    loginView.hidden = TRUE;
    
    [self.view insertSubview:loginView belowSubview:customerImageView];
    
}


-(void)animate
{
    [self genieToRect:customerImageView.frame edge:BCRectEdgeTop];
}

- (void) genieToRect: (CGRect)rect edge: (BCRectEdge) edge
{
    
    CGRect endRect = CGRectInset(rect, 20.0, 20.0);
    
    if (self.viewIsIn) {
        loginView.hidden = FALSE;
        [self.loginView genieOutTransitionWithDuration:1.0 startRect:endRect startEdge:edge completion:^{
            self.loginView.userInteractionEnabled = YES;
        }];
    } else {
        self.loginView.userInteractionEnabled = NO;
        [self.loginView genieInTransitionWithDuration:1.0 destinationRect:endRect destinationEdge:edge completion:
         ^{
             loginView.hidden = FALSE;
         }];
    }
    
    self.viewIsIn = ! self.viewIsIn;
}

#pragma set Navigation barbuttons callback
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:MENU_TITLE style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithTitle:MENU_TITLE style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleRightSideMenu)];
    return nil;
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)setUI
{
    
//    NSArray *fonts = [UIFont familyNames];
//
//    for(NSString *string in fonts){
//        DebugLog(@"%@", string);
//    }
    
    loginView = [CommonCallback setViewPropertiesWithRoundedCorner:loginView];
    signUpView = [CommonCallback setViewPropertiesWithRoundedCorner:signUpView];
    loginView.backgroundColor = [UIColor clearColor];
    loginView.clipsToBounds = YES;
    
    lblHeader.font          = DEFAULT_FONT(18.0);
    lblfirsttimeuser.font   = DEFAULT_FONT(13.0);
    
    usernameText.font = DEFAULT_FONT(18);
    passwordtext.font = DEFAULT_FONT(18);
    
    loginQuesBtn.titleLabel.font = DEFAULT_FONT(14.0);
    forgotBtn.titleLabel.font    = DEFAULT_FONT(12.0);
    
    usernameText.backgroundColor = [UIColor whiteColor];
    passwordtext.backgroundColor = [UIColor whiteColor];
    
    codeText.font = DEFAULT_FONT(18);
    codeText.textColor = BROWN_COLOR;
    
     loginBtn.titleLabel.font = DEFAULT_FONT(18);
    
    [loginQuesBtn setTitleColor:BROWN_COLOR forState:UIControlStateNormal];
    [forgotBtn setTitleColor:BROWN_COLOR forState:UIControlStateNormal];
    
    [loginQuesBtn setTitleColor:BROWN_OFFWHITE_COLOR forState:UIControlStateHighlighted];
    [forgotBtn setTitleColor:BROWN_OFFWHITE_COLOR forState:UIControlStateHighlighted];

    
    lblHeader.textColor          = BROWN_COLOR;
    lblfirsttimeuser.textColor   = BROWN_COLOR;
    
    usernameText.textColor = BROWN_COLOR;
    passwordtext.textColor = BROWN_COLOR;
    
    infoView = [CommonCallback setViewPropertiesWithRoundedCorner:infoView];
    infoView.layer.shadowColor      = [UIColor blackColor].CGColor;
    infoView.layer.shadowOffset     = CGSizeMake(1, 1);
    infoView.layer.shadowOpacity    = 1.0;
    infoView.layer.shadowRadius     = 10.0;
    
    lblinfoHeader.textAlignment     = NSTextAlignmentCenter;
    lblinfoHeader.backgroundColor   = [UIColor clearColor];
    lblinfoHeader.font              = DEFAULT_FONT(20);
    lblinfoHeader.textColor         = [UIColor blackColor];
    lblinfoHeader.adjustsFontSizeToFitWidth = YES;
    lblinfoHeader.text              = ALERT_TITLE;
    
    lblinfoContent.textAlignment    = NSTextAlignmentCenter;
    lblinfoContent.backgroundColor  = [UIColor clearColor];
    lblinfoContent.font             = DEFAULT_FONT(13);
    lblinfoContent.textColor        = [UIColor darkGrayColor];
    lblinfoContent.numberOfLines    = 15;
//    lblinfoContent.text             = @"We use your mobile number to create a unique identity for you. This allows us to register you as a unique user and prevents duplication. In case you misplace your password or change your mobile phone, this will let you set a new password or get a New personalized Shoplocal version on your phone. We never share your personal data with anybody.\nAs a logged in Shoplocal user, You can mark your favourite stores and also receive personalized offers from local merchants in your area. \nPlease see our Privacy Policy for details.";
    
    txtViewinfoContent.textAlignment    = NSTextAlignmentCenter;
    txtViewinfoContent.backgroundColor  = [UIColor clearColor];
    txtViewinfoContent.font             = DEFAULT_FONT(15);
    txtViewinfoContent.textColor        = [UIColor darkGrayColor];
    txtViewinfoContent.text             = @"We use your mobile number to create a unique identity for you. This allows us to register you as a unique user and prevents duplication. In case you misplace your password or change your mobile phone, this will let you set a new password or get a New personalized Shoplocal version on your phone. We never share your personal data with anybody.\nAs a logged in Shoplocal user, You can mark your favourite stores and also receive personalized offers from local merchants in your area. \nPlease see our Privacy Policy for details.";
    
    infoPrivacyBtn.titleLabel.font  = DEFAULT_FONT([UIFont buttonFontSize]);
    infoOKBtn.titleLabel.font   = DEFAULT_FONT([UIFont buttonFontSize]);
    
    infoPrivacyBtn.backgroundColor   = [UIColor clearColor];
    infoOKBtn.backgroundColor   = [UIColor clearColor];

    [infoPrivacyBtn setTitleColor:[UIColor colorWithWhite:0.3 alpha:1] forState:UIControlStateNormal];
    [infoPrivacyBtn setTitleColor:[UIColor colorWithWhite:0.3 alpha:0.8] forState:UIControlStateHighlighted];
    
    [infoOKBtn setTitleColor:[UIColor colorWithWhite:0.3 alpha:1] forState:UIControlStateNormal];
    [infoOKBtn setTitleColor:[UIColor colorWithWhite:0.3 alpha:0.8] forState:UIControlStateHighlighted];
    
    UIImage *normalImage        = [UIImage imageNamed:@"SIAlertView.bundle/button-cancel"];
    UIImage *highlightedImage   = [UIImage imageNamed:@"SIAlertView.bundle/button-cancel-d"];
    CGFloat hInset              = floorf(normalImage.size.width / 2);
	CGFloat vInset              = floorf(normalImage.size.height / 2);
	UIEdgeInsets insets         = UIEdgeInsetsMake(vInset, hInset, vInset, hInset);
	normalImage                 = [normalImage resizableImageWithCapInsets:insets];
	highlightedImage            = [highlightedImage resizableImageWithCapInsets:insets];
	[infoOKBtn setBackgroundImage:normalImage forState:UIControlStateNormal];
	[infoOKBtn setBackgroundImage:highlightedImage forState:UIControlStateHighlighted];
    
    [infoView setHidden:YES];
    
    self.loginView1.backgroundColor     = [UIColor clearColor]; //[UIColor colorWithRed:161/255.0 green:195/255.0 blue:101/255.0 alpha:1.0];
    
    //fbLoginBtn.titleLabel.font          = DEFAULT_FONT(20);
    signUpShoplocalBtn.titleLabel.font  = DEFAULT_FONT(20);
    //fbLoginBtn.layer.cornerRadius           = 8.0;
    signUpShoplocalBtn.layer.cornerRadius   = 8.0;
    
   // [fbLoginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
   // [fbLoginBtn setTitleColor:[UIColor colorWithWhite:0.5 alpha:0.5] forState:UIControlStateHighlighted];
    
    [signUpShoplocalBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [signUpShoplocalBtn setTitleColor:[UIColor colorWithWhite:0.5 alpha:0.5] forState:UIControlStateHighlighted];
    
    //fbLoginBtn.backgroundColor          = [UIColor colorWithRed:64/255.0 green:96/255.0 blue:140/255.0 alpha:1.0];
    fbLoginBtn.backgroundColor          = [UIColor clearColor];
    signUpShoplocalBtn.backgroundColor  = [UIColor colorWithRed:122/255.0 green:148/255.0 blue:77/255.0 alpha:1.0];
    
    customerImageView.alpha = 0.0;
}

-(void)addHUDView
{
    hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:hud];
	hud.dimBackground = NO;
    hud.labelText = @"Loading";
	hud.detailsLabelText = @"Please wait ...";
    [hud hide:NO];
}

-(void)removeHUDView
{
    [hud removeFromSuperview];
    hud = nil;
}

- (void)tapDetected:(UIGestureRecognizer *)sender {
    [self.view endEditing:YES];
}

- (IBAction)fbLoginBtnPressed:(id)sender {
    DebugLog(@"fbLoginBtnPressed");
    if([IN_APP_DELEGATE networkavailable])
    {
        [CommonCallback showProgressHud:@"Processing" subtitle:HUD_SUBTITLE];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sessionStateChanged:) name:FBSessionStateChangedNotification object:nil];
        [INEventLogger logEvent:@"CSignUp_FBOption"];
        [IN_APP_DELEGATE openSessionWithAllowLoginUI:YES];
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

- (IBAction)signUpShoplocalBtnPressed:(id)sender {
    [UIView animateWithDuration:0.5 animations:^{
        self.loginView1.alpha = 0.0;
    } completion:^(BOOL finished){
        [self.loginView1 setHidden:YES];
        [UIView animateWithDuration:0.5 animations:^{
            customerImageView.alpha = 1.0;
        } completion:^(BOOL finished){
            [self performSelector:@selector(animate) withObject:nil afterDelay:0.3];
        }];
    }];
    [INEventLogger logEvent:@"CSignUp_ShoplocalLoginOption"];
}

- (IBAction)infoOKBtnPressed:(id)sender {
     [infoView setHidden:YES];
}

- (IBAction)infoPrivacyBtnPressed:(id)sender {
    [infoView setHidden:YES];
    [self privacyBtnPressed:nil];
}

- (IBAction)loginQuesBtnPressed:(id)sender {
    [infoView setHidden:FALSE];
    //lblinfoContent.text             = @"As a logged in Shoplocal user, You can mark your favourite stores and also receive personalized offers from local merchants in your area. Please see our privacy policy for details.";
//    lblinfoContent.font             = DEFAULT_FONT(16);
    //add animation code here
    [CommonCallback viewtransitionInCompletion:infoView completion:^{
        [CommonCallback viewtransitionOutCompletion:infoView completion:nil];
    }];
}

- (IBAction)forgotBtnPressed:(id)sender {
    [self forgotPassword];
}

- (IBAction)loginBtnPressed:(id)sender {
    if([usernameText.text isEqualToString:@""] || [passwordtext.text isEqualToString:@""])
    {
        [INUserDefaultOperations showSIAlertView:@"Please fill all the fields."];
    }
    else if([usernameText.text length] < 10)
    {
        [INUserDefaultOperations showSIAlertView:@"Mobile number must be at least 10 characters in length."];
    }
    else if([passwordtext.text length] < 6)
    {
        [INUserDefaultOperations showSIAlertView:@"Password field must be at least 6 characters in length."];
    } else {
        if ([IN_APP_DELEGATE networkavailable]) {
            [self sendCustomerLoginRequest];
        } else {
             [INUserDefaultOperations showOfflineAlert];
        }
    }
}

- (IBAction)signupBtnPressed:(id)sender {
    DebugLog(@"delegate %@",delegate);
    if([usernameText.text isEqualToString:@""])
    {
        [INUserDefaultOperations showSIAlertView:@"Please enter valid mobile number."];
    }
    else if([usernameText.text length] < 10)
    {
        [INUserDefaultOperations showSIAlertView:@"Mobile number must be at least 10 characters in length."];
    } else if([usernameText.text length] > 10)
    {
        [INUserDefaultOperations showSIAlertView:@"Mobile number must be 10 characters in length."];
    }else if([[INUserDefaultOperations getCustomerTermsConditionsValue] isEqualToString:@"0"])
    {
        [self termsBtnPressed:nil];
        isOpenedTermsViewWhileSignUp = YES;
    }
    else {
        if ([IN_APP_DELEGATE networkavailable]) {
           // [self signupUser];
            [INEventLogger logEvent:@"CSignUp_Proceed"];
            [self sendNumberRegisterRequest];

        } else {
            [INUserDefaultOperations showOfflineAlert];
        }
    }
}

-(void)showTermsRetryAlert{
    SIAlertView *alertView1 = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"You must read and accept the Terms & conditions to register on shoplocal."];
    alertView1.transitionStyle = SIAlertViewTransitionStyleDropDown;
    alertView1.backgroundStyle = SIAlertViewBackgroundStyleGradient;
    
    [alertView1 addButtonWithTitle:@"Try again"
                              type:SIAlertViewButtonTypeDestructive
                           handler:^(SIAlertView *alert) {
                               DebugLog(@"Retry Clicked");
                               isRetryingToAcceptTerms = YES;
                               [self termsBtnPressed:nil];
                           }];
    [alertView1 addButtonWithTitle:@"Do not Register"
                              type:SIAlertViewButtonTypeCancel
                           handler:^(SIAlertView *alert) {
                               DebugLog(@"Do not Register Clicked");
                           }];
    [alertView1 show];
}

-(void)dismissTerms{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"termsBtn dismiss completion ");
        DebugLog(@"viewDidAppear");
        DebugLog(@"isOpenedTermsViewWhileSignUp %d",isOpenedTermsViewWhileSignUp);
        DebugLog(@"isRetryingToAcceptTerms %d",isRetryingToAcceptTerms);
        [INUserDefaultOperations setCustomerTermsConditionsValue:[INUserDefaultOperations getTermsConditionsValue]];
        
        if (isOpenedTermsViewWhileSignUp) {
            isOpenedTermsViewWhileSignUp = FALSE;
            if([usernameText.text length] ==  10 && [[INUserDefaultOperations getCustomerTermsConditionsValue] isEqualToString:@"1"])
            {
//                [self signupUser];
                [self sendNumberRegisterRequest];
            }else{
                [self showTermsRetryAlert];
            }
        }
        if (isRetryingToAcceptTerms) {
            isRetryingToAcceptTerms = FALSE;
            if([usernameText.text length] ==  10 && [[INUserDefaultOperations getCustomerTermsConditionsValue] isEqualToString:@"1"])
            {
//                [self signupUser];
            [self sendNumberRegisterRequest];
            }else if([usernameText.text length] ==  10 && [[INUserDefaultOperations getCustomerTermsConditionsValue] isEqualToString:@"0"]){
                [self showTermsRetryAlert];
            }
        }
    }];
}

- (IBAction)termsBtnPressed:(id)sender {
    if ([IN_APP_DELEGATE networkavailable]) {
        INTermsViewController *termsController = [[INTermsViewController alloc] initWithNibName:@"INTermsViewController" bundle:nil] ;
        termsController.title = @"";
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:termsController];
        navController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        navController.modalPresentationStyle = UIModalPresentationFormSheet;
        navController.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
        //    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
        //                                                                                   target:self
        //                                                                                   action:@selector(didDismissPresentedViewController)];
        //    loginController.navigationItem.rightBarButtonItem = doneBarButton;
        termsController.navigationItem.title = @"Terms & Conditions";
        termsController.delegate = self;
        [self.navigationController presentViewController:navController animated:YES completion:^{
            DebugLog(@"termsBtn completion ");
        }];
        isOpenedTermsViewWhileSignUp = NO;
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

- (IBAction)privacyBtnPressed:(id)sender {
    if ([IN_APP_DELEGATE networkavailable]) {
        INPrivacyPolicyViewController *privacyController = [[INPrivacyPolicyViewController alloc] initWithNibName:@"INPrivacyPolicyViewController" bundle:nil] ;
        privacyController.title = @"Privacy Policy";
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:privacyController];
        navController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        navController.modalPresentationStyle = UIModalPresentationFormSheet;
        navController.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
        //    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
        //                                                                                   target:self
        //                                                                                   action:@selector(didDismissPresentedViewController)];
        //    loginController.navigationItem.rightBarButtonItem = doneBarButton;
        privacyController.navigationItem.title = @"Privacy Policy";
        //termsController.delegate = self;
        [self.navigationController presentViewController:navController animated:YES completion:NULL];
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

- (IBAction)infoBtnPressed:(id)sender {
    [infoBtn.layer removeAllAnimations];
    [infoView setHidden:FALSE];
    lblinfoContent.text             = @"We use your mobile number to create a unique identity for you. This allows us to register you as a unique user and prevents duplication. In case you misplace your password or change your mobile phone, this will let you set a new password or get a New personalized Shoplocal version on your phone. We never share your personal data with anybody. Please see our Privacy Policy for details.";
    lblinfoContent.font             = DEFAULT_FONT(14.8);
    //add animation code here
    [CommonCallback viewtransitionInCompletion:infoView completion:^{
        [CommonCallback viewtransitionOutCompletion:infoView completion:nil];
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == usernameText) {
        [passwordtext becomeFirstResponder];
	} else if (textField == passwordtext) {
        [passwordtext resignFirstResponder];
	} 
   	return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(textField==usernameText) {
        NSString *usernameString = [usernameText.text stringByReplacingCharactersInRange:range withString:string];
        return !([usernameString length] > 10);
    } else {
        return YES;
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField==codeText)
    {
        [self setCountryCodeBtnPressed];
        return NO;
    }
    return YES;
}

-(void)setCountryCodeBtnPressed
{
    [ActionSheetStringPicker showPickerWithTitle:@"Select Country" rows:countryCodePickerArray initialSelection:0 target:self successAction:@selector(selectCode:element:) cancelAction:@selector(actionPickerCancelled:) origin:self.codeText];
}

#pragma mark - Actionsheet Implementation
- (void)selectCode:(NSNumber *)lselectedIndex element:(id)element {
    NSInteger selectedIndex = [lselectedIndex integerValue];
    INCountryCodeObj *codeObj = [countryCodeArray objectAtIndex:selectedIndex];
    DebugLog(@"object-->%@  %@ %@",codeObj.countryName,codeObj.countrycode,codeObj.countrydialcode);
    codeText.text = codeObj.countrydialcode;
    DebugLog(@"value-->%@",[countryCodePickerArray objectAtIndex:selectedIndex]);
}

- (void)loginUserWithIdAndPassword:(NSString *)lnumber pass:(NSString *)lpass
{
    usernameText.text = lnumber;
    passwordtext.text = lpass;
    if(signupControllernavBar != nil)
    {
        signupController.logindelegate = nil;
        forgotpassController.logindelegate = nil;
        signupController = nil;
        forgotpassController = nil;
        DebugLog(@"dismissSignup -%@-",[self.navigationController viewControllers]);
        [signupControllernavBar dismissViewControllerAnimated:YES completion:^{
            DebugLog(@"Dismiss signup");
            [self sendCustomerLoginRequest];
        }];
    }
    
}

-(void)signupUser
{
    [INUserDefaultOperations setCustomerLoginStateInTour:1];
     DebugLog(@"%@",codeText.text);
    signupController = [[INCustomerSignUpViewController alloc] initWithNibName:@"INCustomerSignUpViewController" bundle:nil] ;
    signupController.title = @"Sign Up";
    signupController.screenType = 0;  //Sign Up Screen
    signupController.codeNumber = codeText.text;
    signupController.mobileNumber = usernameText.text;
    signupController.logindelegate = self;
    signupControllernavBar = [[UINavigationController alloc]initWithRootViewController:signupController];
    UIButton *moreButton1 =  [UIButton buttonWithType:UIButtonTypeCustom];
    [moreButton1 setTitle:@"Cancel" forState:UIControlStateNormal];
    moreButton1.titleLabel.font = [UIFont systemFontOfSize:15.0];
    //[moreButton1 setBackgroundImage:[UIImage imageNamed:@"Transparent_Button.png"] forState:UIControlStateNormal];
    [moreButton1 addTarget:self action:@selector(dismissSignup) forControlEvents:UIControlEventTouchUpInside];
    [moreButton1 setFrame:CGRectMake(250, 5, 60, 32)];
    [signupControllernavBar.navigationBar addSubview:moreButton1];
    [signupControllernavBar.navigationBar setTranslucent:NO];
    [self.navigationController presentViewController:signupControllernavBar animated:YES completion:^{
      DebugLog(@"signupUser %@",[self.navigationController viewControllers]);
    }];
}

-(void)forgotPassword
{
    forgotpassController = [[INCustomerSignUpViewController alloc] initWithNibName:@"INCustomerSignUpViewController" bundle:nil] ;
    forgotpassController.title = @"Forgot Password";
    forgotpassController.screenType = 1;  //Forgot password Screen
    forgotpassController.logindelegate = self;
    signupControllernavBar = [[UINavigationController alloc]initWithRootViewController:forgotpassController];
    UIButton *moreButton1 =  [UIButton buttonWithType:UIButtonTypeCustom];
    [moreButton1 setTitle:@"Cancel" forState:UIControlStateNormal];
    moreButton1.titleLabel.font = [UIFont systemFontOfSize:15.0];
    //[moreButton1 setBackgroundImage:[UIImage imageNamed:@"Transparent_Button.png"] forState:UIControlStateNormal];
    [moreButton1 addTarget:self action:@selector(dismissSignup) forControlEvents:UIControlEventTouchUpInside];
    [moreButton1 setFrame:CGRectMake(250, 5, 60, 32)];
    [signupControllernavBar.navigationBar addSubview:moreButton1];
    [signupControllernavBar.navigationBar setTranslucent:NO];
    [self.navigationController presentViewController:signupControllernavBar animated:YES completion:nil];
}

-(void)dismissSignup
{
    DebugLog(@"dismissSignup");
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"Dismiss");
        [INUserDefaultOperations setCustomerLoginStateInTour:0];
        [INUserDefaultOperations setCustomerTermsConditionsValue:@"0"];
    }];
}



-(void)sendCustomerLoginRequest
{
//    NSUUID *deviceID = [[UIDevice currentDevice] identifierForVendor];
//    DebugLog(@"id---->%@", [deviceID UUIDString]);
    
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
//    if ([[deviceID UUIDString] length] > 0) {
//        [httpClient setDefaultHeader:@"device_id" value:[deviceID UUIDString]];
//    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:MOBILE_NUMBER(codeText.text, usernameText.text) forKey:@"mobile"];
    [params setObject:passwordtext.text forKey:@"password"];
    [httpClient postPath:LOGIN_CUSTOMER parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            [INUserDefaultOperations setCustomerLoginStateInTour:0];

            if([[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
                NSDictionary* data = [json objectForKey:@"data"];
                NSString * resultid = [data objectForKey:@"id"];
                NSString * resultcode = [data objectForKey:@"auth_code"];
                DebugLog(@"%@ %@",resultid,resultcode);
                [INUserDefaultOperations setCustomerDetails:SHOPLOCALSERVER userid:usernameText.text pass:passwordtext.text];
                [INUserDefaultOperations setCustomerDetails:usernameText.text pass:passwordtext.text];
                [INUserDefaultOperations setCustomerAuthDetails:resultid pass:resultcode];
                [INUserDefaultOperations setCustomerDistance:-1];
                [INUserDefaultOperations setCustomerCountryCode:codeText.text];
                [INEventLogger setUser:[NSString stringWithFormat:@"%@%@", @"C", resultid]];
//                [self.navigationController dismissViewControllerAnimated:YES completion:^{
//                    DebugLog(@"Dismiss");
//                    [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_LOGIN_LOGOUT_NOTIFICATION object:nil];
//                }];
                NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:IN_CUSTOMER_LOGIN_LOGOUT_KEY];
                [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_LOGIN_LOGOUT_NOTIFICATION object:nil userInfo:dictionary];
                [INEventLogger logEvent:@"CSignUp_ShoplocalLoginSuccess"];
                
                NSArray *customerid_Array = [[NSArray alloc] initWithObjects:resultid, nil];
                [CommonCallback addTagsInUrbanAriship:customerid_Array name:PUSH_TAG_CUSTOMER_ID];
                
            } else {
                NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:IN_CUSTOMER_LOGIN_LOGOUT_KEY];
                [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_LOGIN_LOGOUT_NOTIFICATION object:nil userInfo:dictionary];
                [INEventLogger logEvent:@"CSignUp_ShoplocalLoginFailed"];
            }
            
            NSString *message = [json objectForKey:@"message"];
            DebugLog(@"message %@",message);
            SIAlertView *sialertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:message];
            sialertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
            sialertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
            [sialertView addButtonWithTitle:@"OK"
                                       type:SIAlertViewButtonTypeCancel
                                    handler:^(SIAlertView *alert) {
                                        DebugLog(@"OK Clicked");
                                        DebugLog(@"delegate %@",delegate);
                                        if (self.delegate && [self.delegate respondsToSelector:@selector(dismissLoginView)]) {
                                            [self.delegate dismissLoginView];
                                        }
                                    }];
            [sialertView show];
            
        }  else {
//            [INUserDefaultOperations showAlert:@"No response from server. Please try again later."];
            DebugLog(@"delegate %@",delegate);
            if (self.delegate && [self.delegate respondsToSelector:@selector(dismissLoginView)]) {
                [self.delegate dismissLoginView];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(dismissLoginView)]) {
            [self.delegate dismissLoginView];
        }
    }];
}


-(void)sendNumberRegisterRequest
{
    DebugLog(@"number--->%@",(MOBILE_NUMBER(codeText.text,usernameText.text)));
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:REGISTER_CUSTOMER] cachePolicy:NO timeoutInterval:30.0];
    DebugLog(@"link--->%@",REGISTER_CUSTOMER);
    NSMutableDictionary *postReq = [[NSMutableDictionary alloc] init];
    DebugLog(@"%@",MOBILE_NUMBER(codeText.text,usernameText.text));
    [postReq setObject:MOBILE_NUMBER(codeText.text,usernameText.text) forKey:@"mobile"];
    [postReq setObject:DEVICE_TYPE forKey:@"register_from"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postReq
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (!jsonData) {
        DebugLog(@"Got an error: %@", error);
    }
    [urlRequest setHTTPBody: jsonData];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody: jsonData];
    connectionRegisterNumber = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)activateUser:(NSString *)message
{
    DebugLog(@"activateUser");
    INCustomerVerifyViewController *verifyViewController = [[INCustomerVerifyViewController alloc] initWithNibName:@"INCustomerVerifyViewController" bundle:nil] ;
    verifyViewController.title = @"Shoplocal Registration";
    verifyViewController.codeNumber = codeText.text;
    verifyViewController.mobileNumber = usernameText.text;
    verifyViewController.message = message;
    verifyViewController.logindelegate = self;
    UINavigationController *verifyViewControllernavBar = [[UINavigationController alloc]initWithRootViewController:verifyViewController];
    verifyViewControllernavBar.navigationBar.translucent = NO;
    UIButton *cancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    cancelButton.titleLabel.font = [UIFont systemFontOfSize:17.0];
    //[cancelButton setBackgroundImage:[UIImage imageNamed:@"Transparent_Button.png"] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(dismissActivationView) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setFrame:CGRectMake(250, 7, 70, 32)];
    [verifyViewControllernavBar.navigationBar addSubview:cancelButton];
    [verifyViewControllernavBar.navigationBar setTranslucent:NO];
    [self.navigationController presentViewController:verifyViewControllernavBar animated:YES completion:nil];
}

-(void)dismissActivationView
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"dismissActivationView");
        [INUserDefaultOperations setCustomerLoginStateInTour:0];
        [INUserDefaultOperations setCustomerTermsConditionsValue:@"0"];
    }];
}

- (void)loginUserWithActivationCode:(NSString *)lnumber pass:(NSString *)lpass
{
    usernameText.text = lnumber;
    passwordtext.text = lpass;
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"dismissActivationView");
        [self sendCustomerLoginRequest];
    }];
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    DebugLog(@"Error: %@", [error localizedDescription]);
    [hud hide:YES];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError* error;
    //[hud hide:YES];
    [CommonCallback hideProgressHud];
    if (connection==connectionRegisterNumber)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            DebugLog(@"json: %@", json);
            if(json != nil) {
                if([[json  objectForKey:@"success"] isEqualToString:@"true"]) {
                    if([[json  objectForKey:@"code"] isEqualToString:@"-223"])
                    {
                        [INEventLogger logEvent:@"CSignUp_New"];
                    } else {
                        [INEventLogger logEvent:@"CSignUp_Return"];
                    }
                    [INUserDefaultOperations setCustomerTermsConditionsValue:@"0"];
                    [INUserDefaultOperations setCustomerCountryCode:codeText.text];
                    [INUserDefaultOperations setCustomerDetails:SHOPLOCALSERVER userid:usernameText.text pass:nil];
                    [INUserDefaultOperations clearCustomerDetails];
                    [INUserDefaultOperations setCustomerLoginState:[json  objectForKey:@"message"] visitedPage:2];
                    if ([IN_APP_DELEGATE networkavailable]) {
                        [self activateUser:[json  objectForKey:@"message"]];
                    } else {
                        [INUserDefaultOperations showOfflineAlert];
                    }
                } else if([[json  objectForKey:@"success"] isEqualToString:@"false"]) {
                    if([json  objectForKey:@"message"] != [NSNull null] && ![[json objectForKey:@"message"] isEqualToString:@""]){
                        NSString *message = [json objectForKey:@"message"];
                        [INUserDefaultOperations showSIAlertView:message];
                    }
                }
            } else {
                    NSString *message = @"No response from server. Please try again later.";
                    [INUserDefaultOperations showSIAlertView:message];
            }
        }
        responseAsyncData = nil;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)dealloc {
//    [self removeHUDView];
//}


- (void)viewDidUnload {
    [self setLblHeader:nil];
    [self setLoginView:nil];
    [self setSignUpView:nil];
    [self setLoginQuesBtn:nil];
    [self setLblfirsttimeuser:nil];
    [self setCodeText:nil];
    [self setTermsBtn:nil];
    [self setPrivacyBtn:nil];
    [self setInfoView:nil];
    [self setLblinfoHeader:nil];
    [self setLblinfoContent:nil];
    [self setInfoOKBtn:nil];
    [self setInfoPrivacyBtn:nil];
    [self setInfoBtn:nil];
    [self setLoginView:nil];
    [self setLoginView1:nil];
    [self setFbLoginBtn:nil];
    [self setSignUpShoplocalBtn:nil];
    [self setTxtViewinfoContent:nil];
    [self setCustomerImageView:nil];
    [super viewDidUnload];
}


#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma NSNotificationCenter Callbacks
/*
 * Configure the logged in versus logged out UI
 */
- (void)sessionStateChanged:(NSNotification*)notification {
    if (FBSession.activeSession.isOpen) {
        DebugLog(@"Login: User Logged In");
        [[FBRequest requestForMe] startWithCompletionHandler:
         ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
             if (error) {
                 [CommonCallback hideProgressHud];
                 [INEventLogger logEvent:@"CSignUp_LoginFailedFB"];
                 [IN_APP_DELEGATE handleAuthError:error];
             }else {
                 DebugLog(@"user ID %@",[user objectForKey:@"id"]);
                 DebugLog(@"user name %@",[user objectForKey:@"first_name"]);
                 DebugLog(@"user middle name %@",[user objectForKey:@"middle_name"]);
                 DebugLog(@"user last name %@",[user objectForKey:@"last_name"]);
                 DebugLog(@"user link %@",[user objectForKey:@"link"]);
                 DebugLog(@"user name %@",[user objectForKey:@"username"]);
                 DebugLog(@"user email %@",[user objectForKey:@"email"]);
                 DebugLog(@"user gender %@",[user objectForKey:@"gender"]);
                if (!isSendingFBLoginRequest) {
                     [self registerFBUserToShoplocalServer:[user objectForKey:@"id"] accessToken:[FBSession.activeSession.accessTokenData accessToken]];
                    isSendingFBLoginRequest = YES;
                    if (loginType == LOGIN_TYPE_SHOPLOCAL || loginType == LOGIN_TYPE_FB) {
                        [INEventLogger logEvent:@"Tour_LoginSuccessFB"];
                    } else {
                        [INEventLogger logEvent:@"CSignUp_LoginSuccessFB"];
                    }
                    [FBAppEvents logEvent:FBAppEventNameCompletedRegistration
                               parameters:@{ @"Login Type"    : @"Facebook",
                                             @"User Type"     : @"Customer"} ];
                 }
             }
         }];
    } else {
        [CommonCallback hideProgressHud];
        DebugLog(@"Login: User Logged Out");
    }
}


-(void)registerFBUserToShoplocalServer:(NSString *)userId accessToken:(NSString *)token
{
    DebugLog(@"registerFBUserToShoplocalServer %@ %@",userId,token);
    [CommonCallback showProgressHud:@"Processing" subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
//    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
//    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
    [postParams setObject:userId forKey:@"facebook_user_id"];
    [postParams setObject:token forKey:@"facebook_access_token"];
    
    DebugLog(@"postPath %@",FBUSER_LOGIN_TO_SERVER);
    DebugLog(@"postParams %@",postParams);

    [httpClient postPath:FBUSER_LOGIN_TO_SERVER parameters:postParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"registerFBUserToShoplocalServer : json ->%@",json);
        if(json != nil) {
            if([[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
               
                NSDictionary* data = [json objectForKey:@"data"];
                NSString * resultid = [data objectForKey:@"id"];
                NSString * resultcode = [data objectForKey:@"auth_code"];
                DebugLog(@"%@ %@",resultid,resultcode);
                [INUserDefaultOperations setCustomerDetails:FACEBOOKSERVER userid:userId pass:[FBSession.activeSession.accessTokenData accessToken]];
                [INUserDefaultOperations setCustomerDetails:userId pass:[FBSession.activeSession.accessTokenData accessToken]];
                [INUserDefaultOperations setCustomerAuthDetails:resultid pass:resultcode];
                [INUserDefaultOperations setCustomerDistance:-1];
                [INUserDefaultOperations setCustomerCountryCode:codeText.text];
                [INEventLogger setUser:[NSString stringWithFormat:@"%@%@", @"C", resultid]];
                NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:IN_CUSTOMER_LOGIN_LOGOUT_KEY];
                [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_LOGIN_LOGOUT_NOTIFICATION object:nil userInfo:dictionary];
                
                NSArray *customerid_Array = [[NSArray alloc] initWithObjects:resultid, nil];
                [CommonCallback addTagsInUrbanAriship:customerid_Array name:PUSH_TAG_CUSTOMER_ID];
                
                [INUserDefaultOperations setIsOpenGraphShareEnabled:YES]; // To share user open graph on facebook
            } else {
                NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:IN_CUSTOMER_LOGIN_LOGOUT_KEY];
                [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_LOGIN_LOGOUT_NOTIFICATION object:nil userInfo:dictionary];
            }
            NSString *message = [json objectForKey:@"message"];
            DebugLog(@"message %@",message);
            SIAlertView *sialertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:message];
            sialertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
            sialertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
            [sialertView addButtonWithTitle:@"OK"
                                       type:SIAlertViewButtonTypeCancel
                                    handler:^(SIAlertView *alert) {
                                        DebugLog(@"OK Clicked");
                                        DebugLog(@"delegate %@",delegate);
                                        if (self.delegate && [self.delegate respondsToSelector:@selector(dismissLoginView)]) {
                                            [self.delegate dismissLoginView];
                                        }
                                    }];
            [sialertView show];
        }
        else {
            DebugLog(@"delegate %@",delegate);
            if (self.delegate && [self.delegate respondsToSelector:@selector(dismissLoginView)]) {
                [self.delegate dismissLoginView];
            }
        }
        isSendingFBLoginRequest = NO;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        isSendingFBLoginRequest = NO;
        if (self.delegate && [self.delegate respondsToSelector:@selector(dismissLoginView)]) {
            [self.delegate dismissLoginView];
        }
    }];

}

@end
