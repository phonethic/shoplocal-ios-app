//
//  INSocialViewController.m
//  inorbit
//
//  Created by Rishi on 24/12/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INSocialViewController.h"
#import "INFBWebViewController.h"
#import "CommonCallback.h"
#import "constants.h"
#import "INAppDelegate.h"
#import "INWebViewController.h"

@interface INSocialViewController ()

@end

@implementation INSocialViewController
@synthesize socialTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:TRUE];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:TRUE];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:FALSE];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:TRUE];
}

#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}
-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                             style:UIBarButtonItemStylePlain
                                                            target:nil
                                                            action:nil];
    [[self navigationItem] setBackBarButtonItem:back];
}


#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UIView *topView;
    UILabel *lblTitle;
    UIImageView *thumbImg;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backgroundColor = [UIColor clearColor];
        
        // Create the top view
        topView = [[UIView alloc] initWithFrame:CGRectMake(10, 5, 300, 75)];
        topView.tag = 111;
        topView.backgroundColor = [UIColor whiteColor];
        //topView = [CommonCallback setViewPropertiesWithRoundedCorner:topView];
        topView.clipsToBounds = YES;


        thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(10, 7, 60, 60)];
        thumbImg.tag = 1;
        thumbImg.contentMode = UIViewContentModeScaleAspectFit;
        //[cell.contentView addSubview:thumbImg];
        [topView addSubview:thumbImg];
        
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(80.0,25.0,180.0,25.0)];
        lblTitle.text = @"";
        lblTitle.tag = 2;
        lblTitle.font = DEFAULT_BOLD_FONT(18);
        lblTitle.textAlignment = NSTextAlignmentLeft;
        lblTitle.textColor = DEFAULT_COLOR;
        lblTitle.backgroundColor =  [UIColor clearColor];
        //[cell.contentView addSubview:lblTitle];
        [topView addSubview:lblTitle];
        
//        UILabel *lblline = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 73.0, 300.0, 2.0)];
//        lblline.text = @"";
//        lblline.backgroundColor =  LIGHT_ORANGE_COLOR;
//        [topView addSubview:lblline];
        
        // Add views to contentView
        [cell.contentView addSubview:topView];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = BROWN_OFFWHITE_COLOR;
        cell.selectedBackgroundView = bgColorView;

     }
    
    topView = (UIView *)[cell viewWithTag:111];
    switch (indexPath.row) {
        case 0:
        {
            UILabel *lblTitle = (UILabel *)[topView viewWithTag:2];
            [lblTitle setText:@"Facebook"];
            
            UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
            [thumbImgview setImage:[UIImage imageNamed:@"Facebook_circle.png"]];
        }
            break;
            
        case 1:
        {
            UILabel *lblTitle = (UILabel *)[topView viewWithTag:2];
            [lblTitle setText:@"Twitter"];
            
            UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
            [thumbImgview setImage:[UIImage imageNamed:@"Twitter_circle.png"]];
        }
            break;
            
        case 2:
        {
            UILabel *lblTitle = (UILabel *)[topView viewWithTag:2];
            [lblTitle setText:@"Google+"];
            
            UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
            [thumbImgview setImage:[UIImage imageNamed:@"GooglePlus_circle.png"]];
        }
            break;
            
        case 3:
        {
            UILabel *lblTitle = (UILabel *)[topView viewWithTag:2];
            [lblTitle setText:@"Pinterest"];
            
            UIImageView *thumbImgview = (UIImageView *)[topView viewWithTag:1];
            [thumbImgview setImage:[UIImage imageNamed:@"Pinterest_circle.png"]];
        }
            break;
            
            
            
        default:
            break;
    }

    return cell;
}


#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (![IN_APP_DELEGATE networkavailable]) {
        [INUserDefaultOperations showOfflineAlert];
        return;
    }
    
    switch (indexPath.row) {
        case 0:
        {
            //[INEventLogger logEvent:@"SocialConnect_Facebook" withParams:params];
            NSDictionary *socialParams = [NSDictionary dictionaryWithObjectsAndKeys:@"Facebook",@"Source", nil];
            [INEventLogger logEvent:@"Tab_Social" withParams:socialParams];
            INWebViewController *webViewController = [[INWebViewController alloc] initWithNibName:@"INWebViewController" bundle:nil];
            webViewController.title = @"Facebook";
            webViewController.urlString = SHOPLOCAL_FB_URL;
            [self.navigationController pushViewController:webViewController animated:YES];
        }
            break;
            
        case 1:
        {
            //[INEventLogger logEvent:@"SocialConnect_Twitter"];
            NSDictionary *socialParams = [NSDictionary dictionaryWithObjectsAndKeys:@"Twitter",@"Source", nil];
            [INEventLogger logEvent:@"Tab_Social" withParams:socialParams];
            INWebViewController *webViewController = [[INWebViewController alloc] initWithNibName:@"INWebViewController" bundle:nil];
            webViewController.title = @"Twitter";
            webViewController.urlString = SHOPLOCAL_TW_URL;
            [self.navigationController pushViewController:webViewController animated:YES];
        }
            break;
            
        case 2:
        {
            //[INEventLogger logEvent:@"SocialConnect_Youtube"];
            NSDictionary *socialParams = [NSDictionary dictionaryWithObjectsAndKeys:@"Google+",@"Source", nil];
            [INEventLogger logEvent:@"Tab_Social" withParams:socialParams];
            INWebViewController *webViewController = [[INWebViewController alloc] initWithNibName:@"INWebViewController" bundle:nil];
            webViewController.title = @"Google+";
            webViewController.urlString = SHOPLOCAL_GPLUS_URL;
            [self.navigationController pushViewController:webViewController animated:YES];
        }
            break;
            
        case 3:
        {
            //[INEventLogger logEvent:@"SocialConnect_Pinterest"];
            NSDictionary *socialParams = [NSDictionary dictionaryWithObjectsAndKeys:@"Pinterest",@"Source", nil];
            [INEventLogger logEvent:@"Tab_Social" withParams:socialParams];
            INWebViewController *webViewController = [[INWebViewController alloc] initWithNibName:@"INWebViewController" bundle:nil];
            webViewController.title = @"Pinterest";
            webViewController.urlString = SHOPLOCAL_PIN_URL;
            [self.navigationController pushViewController:webViewController animated:YES];
        }
            break;
            
            
            
        default:
            break;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setSocialTableView:nil];
    [super viewDidUnload];
}
@end
