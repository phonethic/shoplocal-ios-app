//
//  INMerchantSignUPViewController.m
//  shoplocal
//
//  Created by Rishi on 13/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INMerchantSignUPViewController.h"
#import "INMerchantLoginViewController.h"
#import "INTermsViewController.h"
#import "constants.h"
#import "CommonCallback.h"
#import "INAppDelegate.h"
#import "INCountryCodeObj.h"
#import "ActionSheetPicker.h"
#import "INPrivacyPolicyViewController.h"

#define REGISTER_MERCHANT [NSString stringWithFormat:@"%@%@%@merchant_api/merchant",LIVE_SERVER,URL_PREFIX,API_VERSION]
#define ISVERIFIED_MERCHANT(NUMBER) [NSString stringWithFormat:@"%@%@%@merchant_api/is_mobile_verified?mobile=%@",LIVE_SERVER,URL_PREFIX,API_VERSION,NUMBER]
#define SET_PASSWORD_MERCHANT [NSString stringWithFormat:@"%@%@%@merchant_api/set_password",LIVE_SERVER,URL_PREFIX,API_VERSION]
#define VERIFIED_CODE_MERCHANT(NUMBER) [NSString stringWithFormat:@"%@%@%@merchant_api/mobile_verify_code?mobile=%@",LIVE_SERVER,URL_PREFIX,API_VERSION,NUMBER]
#define MOBILE_NUMBER(CODE,NUMBER)  [NSString stringWithFormat:@"%@%@",[CODE stringByReplacingOccurrencesOfString:@"+" withString:@""],NUMBER]

@interface INMerchantSignUPViewController ()

@end

@implementation INMerchantSignUPViewController
@synthesize signupScrollView;
@synthesize pageControl;
@synthesize passwordTextField;
@synthesize numberTextField;
@synthesize retypepasswordTextField;
@synthesize verrificationTextField;
@synthesize screenType;
@synthesize messagelbl;
@synthesize logindelegate;
@synthesize phoneNumberScreenlbl;
@synthesize phoneNumberScreenlbl1;
@synthesize phoneNumberScreenlbl2;
@synthesize phoneNumberScreenlbl3;
@synthesize phoneNumberScreenlbl4;
@synthesize phoneNumberScreenforgotlbl;
@synthesize resendBtn;
@synthesize nextBtn;
@synthesize registerstatuslbl;
@synthesize codeNumber;
@synthesize callNumber;
@synthesize confirmView;
@synthesize numberconfirmlbl;
@synthesize confirmnumberTextField;
@synthesize confirmProceedBtn;
@synthesize confirmCancelBtn;
@synthesize countryCodeArray;
@synthesize codeTextField;
@synthesize countryCodePickerArray;
@synthesize codelbl;
@synthesize backView1,backView2,backView3;
@synthesize scrollView3,scrollView1;
@synthesize proceedBtn,termsBtn,questionBtn;
@synthesize privacyBtn;
@synthesize mobileNumber;
@synthesize resendsmslbl;
@synthesize signupIndicatior;
@synthesize callscrlbl;
@synthesize callBtn;
@synthesize proceedafterCallBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self removeHUDView];
}

-(void)addHUDView
{
    hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:hud];
	hud.dimBackground = NO;
    hud.labelText = @"Loading";
	hud.detailsLabelText = @"Please wait ...";
    [hud hide:NO];
}

-(void)removeHUDView
{
    [hud removeFromSuperview];
    hud = nil;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == numberTextField) {
        [numberTextField resignFirstResponder];
	}
    else if (textField == verrificationTextField) {
        [passwordTextField becomeFirstResponder];
	}
    else if (textField == passwordTextField) {
        [retypepasswordTextField becomeFirstResponder];
	}
    else if (textField == retypepasswordTextField) {
        [retypepasswordTextField resignFirstResponder];
        [self scrollTobottom];
	}
   	return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(textField==numberTextField) {
        NSString *usernameString = [numberTextField.text stringByReplacingCharactersInRange:range withString:string];
        return !([usernameString length] > 10);
    } else if(textField==confirmnumberTextField) {
        NSString *usernameString = [confirmnumberTextField.text stringByReplacingCharactersInRange:range withString:string];
        return !([usernameString length] > 10);
    }  else {
        return YES;
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    if(numberTextField.frame.origin.y > scrollView3.contentOffset.y)
    {
        //if(textField == areaTextField || textField == cityTextField || textField == pincodeTextField)
        {
            [scrollView3 setContentOffset:CGPointMake(0,textField.frame.origin.y-80) animated:YES];
        }
    }
    [scrollView3 setContentSize: CGSizeMake(scrollView3.frame.size.width, scrollView3.frame.size.height + 170)];
    
    if(verrificationTextField.frame.origin.y > scrollView1.contentOffset.y)
    {
        //if(textField == passwordTextField || textField == retypepasswordTextField)
        {
            [scrollView1 setContentOffset:CGPointMake(0,textField.frame.origin.y-80) animated:YES];
        }
    }
    [scrollView1 setContentSize: CGSizeMake(scrollView1.frame.size.width, scrollView1.frame.size.height + 170)];

    if(textField==codeTextField)
    {
        [self.view endEditing:YES];
        [self setCountryCodeBtnPressed];
        return NO;
    }
    return YES;
    
    
}

-(void) addAlertView{
    alertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"We use your mobile number to create a unique identity for you. This allows us to register you as a unique user and prevents duplication. In case you misplace your password or change your mobile phone, this will let you set a new password or get a New personalized Shoplocal version on your phone. We never share your personal data with anybody. Pls see our Privacy Policy for details."];
    alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
    alertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
    
    [alertView addButtonWithTitle:@"OK"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alert) {
                          }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [CommonCallback setViewPropertiesWithRoundedCorner:backView1];
    [CommonCallback setViewPropertiesWithRoundedCorner:backView2];
    [CommonCallback setViewPropertiesWithRoundedCorner:backView3];
    DebugLog(@"%@",self.navigationController.viewControllers );
    
    [proceedafterCallBtn setHidden:TRUE];
    
    countryCodeArray = [[NSMutableArray alloc] init];
    countryCodePickerArray = [[NSMutableArray alloc] init];
/*
    NSError* error;
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"country_code" ofType:@"txt"];
    if (filePath) {
        NSString *data = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:[data dataUsingEncoding:NSUTF8StringEncoding]  options:kNilOptions error:&error];
        
        for(NSDictionary *obj in json) {
            INCountryCodeObj *codeObj = [[INCountryCodeObj alloc] init];
            codeObj.countryName = [obj objectForKey:@"name"];
            codeObj.countrycode = [obj objectForKey:@"code"];
            codeObj.countrydialcode = [obj objectForKey:@"dial_code"];
            [countryCodeArray addObject:codeObj];
            [countryCodePickerArray addObject:[NSString stringWithFormat:@"%@ (%@)",[obj objectForKey:@"name"],[obj objectForKey:@"dial_code"]]];
            codeObj = nil;
        }
    }
    
    NSLocale *locale = [NSLocale currentLocale];
    NSString *countryCode = [locale objectForKey: NSLocaleCountryCode];
    DebugLog(@"countryCode:%@", countryCode);
    NSString *country = [locale displayNameForKey: NSLocaleCountryCode value: countryCode];
    DebugLog(@"country:%@", country);
    
    for (int i=0; i < [countryCodeArray count] ; i++) {
        INCountryCodeObj *codeObj = [countryCodeArray objectAtIndex:i];
        DebugLog(@"%@",codeObj.countrycode);
        if([codeObj.countrycode isEqualToString:countryCode])
        {
            [codeTextField setText:codeObj.countrydialcode];
            break;
        }
    }
*/    

    //[self addHUDView];
    numberTextField.text = mobileNumber;
    codeTextField.text = codeNumber;
    if([codeTextField.text isEqualToString:@""])
    {
        [codeTextField setText:@"+91"];
    }
    [INUserDefaultOperations setMerchantTermsConditionsValue:@"0"];
    [self addAlertView];
    for(UILabel *label in _labelsCollection)
    {
        // Apply your styles
        label.font = DEFAULT_FONT(16);
        label.textColor = BROWN_COLOR;
    }
    
    for(UIButton *button in _buttonsCollection)
    {
        if ([button isEqual:privacyBtn] || [button isEqual:termsBtn]) {
            [button setTitleColor:BROWN_COLOR forState:UIControlStateNormal];
            [button setTitleColor:BROWN_OFFWHITE_COLOR forState:UIControlStateHighlighted];
            button.titleLabel.font = DEFAULT_FONT(12);
        } else {
            // Apply your styles
            button.titleLabel.font = DEFAULT_FONT(18);
        }
    }
    
    [questionBtn.titleLabel setTextAlignment:NSTextAlignmentCenter];
    confirmProceedBtn.titleLabel.font = DEFAULT_FONT(18);
    confirmCancelBtn.titleLabel.font = DEFAULT_FONT(18);
    numberconfirmlbl.textColor = BROWN_COLOR;
    numberconfirmlbl.font = DEFAULT_FONT(16);
    numberTextField.font = DEFAULT_FONT(18);
    numberTextField.textColor = BROWN_COLOR;
    verrificationTextField.textColor = BROWN_COLOR;
    verrificationTextField.font = DEFAULT_FONT(18);
    confirmnumberTextField.font = DEFAULT_FONT(15);
    confirmnumberTextField.textColor = BROWN_COLOR;
    codeTextField.font = DEFAULT_FONT(18);
    codeTextField.textColor = BROWN_COLOR;
    codelbl.textColor = BROWN_COLOR;
    codelbl.font = DEFAULT_FONT(15);
    resendsmslbl.font = DEFAULT_FONT(15);
    resendsmslbl.textColor = BROWN_COLOR;
    
    confirmView.layer.borderColor = [UIColor whiteColor].CGColor;
    confirmView.layer.borderWidth = 2.0;
    confirmView.layer.cornerRadius = 8.0;
    confirmView.layer.shadowColor = [UIColor blackColor].CGColor;
    confirmView.layer.shadowOffset = CGSizeMake(1, 1);
    confirmView.layer.shadowOpacity = 1.0;
    confirmView.layer.shadowRadius = 10.0;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkValidNumber:) name:IN_APP_BACKGROUND_CHANGE_NOTIFICATION object:nil];
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    viewTap.numberOfTapsRequired = 1;
    viewTap.cancelsTouchesInView = NO;
    [self.view  addGestureRecognizer:viewTap];
    UIView *view = nil;
    CGFloat curXLoc = 0;
    DebugLog(@"%@",[signupScrollView subviews]);
    for (view in [signupScrollView subviews])
    {
        if ([view isKindOfClass:[UIView class]])
        {
            view.backgroundColor = [UIColor clearColor];
            CGRect frame = view.frame;
            frame.origin = CGPointMake(curXLoc, 0);
            view.frame = frame;
            
            curXLoc += (signupScrollView.frame.size.width);
        }
    }
    
    //------------set the content size so it can be scrollable
    DebugLog(@"_addRecordScrollView.subviews.count %d",signupScrollView.subviews.count);
    [signupScrollView setContentSize:CGSizeMake((3 * signupScrollView.frame.size.width), signupScrollView.frame.size.height)];
    
    signupScrollView.backgroundColor = [UIColor clearColor];
    signupScrollView.clipsToBounds = YES;
    signupScrollView.scrollEnabled = YES;
    signupScrollView.pagingEnabled = YES;
    signupScrollView.showsHorizontalScrollIndicator = NO;
    signupScrollView.showsVerticalScrollIndicator = NO;
    signupScrollView.scrollsToTop = YES;
    signupScrollView.delegate = self;
    signupScrollView.bounces = NO;
    signupScrollView.directionalLockEnabled = YES;
    
    pageControl.numberOfPages = 3;
    if(screenType == 0)
    {
        [phoneNumberScreenlbl setHidden:FALSE];
        [phoneNumberScreenlbl1 setHidden:FALSE];
        [phoneNumberScreenlbl2 setHidden:FALSE];
        [phoneNumberScreenlbl3 setHidden:FALSE];
        [phoneNumberScreenlbl4 setHidden:FALSE];
        [phoneNumberScreenforgotlbl setHidden:TRUE];
        pageControl.currentPage = 0;
        signupScrollView.scrollEnabled = FALSE;
    } else {
        [phoneNumberScreenlbl setHidden:TRUE];
        [phoneNumberScreenlbl1 setHidden:TRUE];
        [phoneNumberScreenlbl2 setHidden:TRUE];
        [phoneNumberScreenlbl3 setHidden:TRUE];
        [phoneNumberScreenlbl4 setHidden:TRUE];
        [phoneNumberScreenforgotlbl setHidden:FALSE];
        pageControl.currentPage = 0;
        //[self pageChange:nil];
        signupScrollView.scrollEnabled = FALSE;
    }
    DebugLog(@"last page %d mobileNumer %@ save %@",[INUserDefaultOperations getMerchantLoginStateWithVisitedPage],mobileNumber,[INUserDefaultOperations getMerchantId]);
    if ([INUserDefaultOperations getMerchantLoginStateWithVisitedPage] == 2 && [mobileNumber isEqualToString:[INUserDefaultOperations getMerchantId]]) {
        pageControl.currentPage = 2;
        [self pageChange:nil];
        messagelbl.text = [INUserDefaultOperations getMerchantLoginStateWithText];
    }else{
        [INUserDefaultOperations clearMerchantDetails];
        [INUserDefaultOperations clearMerchantLoginState];
        [self performSelector:@selector(proceedBtnPressed:) withObject:nil afterDelay:0.5];
    }
}


#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)tapDetected:(UIGestureRecognizer *)sender {
    [self.view endEditing:YES];
    DebugLog(@"page--->%d",pageControl.currentPage);
    UIView *view = nil;
    
    if (pageControl.currentPage == 0) {    //Enter Mobile Number Screen
        CGPoint tapLocation = [sender locationInView:scrollView3];
        view = [scrollView3 hitTest:tapLocation withEvent:nil];
    }
    
    if (pageControl.currentPage == 2) {    //Enter Password Screen
        CGPoint tapLocation = [sender locationInView:scrollView1];
        view = [scrollView1 hitTest:tapLocation withEvent:nil];
    }
    
    if (![view isKindOfClass:[UIButton class]]) {
        [self scrollTobottom];
    }
    
}

-(void)scrollTobottom
{
    //if scrollview content height is greater than scrollview total height then remove the added height from scrollview
    if (pageControl.currentPage == 2) {
        if(scrollView1.contentSize.height > scrollView1.frame.size.height)
        {
            [scrollView1 setContentSize: CGSizeMake(scrollView1.frame.size.width, scrollView1.frame.size.height - 170)];
        }
    }
    
    if (pageControl.currentPage == 0) {
        if(scrollView3.contentSize.height > scrollView3.frame.size.height)  //if scrollview content height is greater than scrollview total height then remove the added height from scrollview
        {
            [scrollView3 setContentSize: CGSizeMake(scrollView3.frame.size.width, scrollView3.frame.size.height - 170)];
        }
    }
    
    CGPoint bottomOffset = CGPointMake(0, 0);
    [scrollView3 setContentOffset:bottomOffset animated:YES];
    [scrollView1 setContentOffset:bottomOffset animated:YES];
    DebugLog(@"scrollview 1 - > %f %f",scrollView1.contentSize.height,scrollView1.frame.size.height);
    DebugLog(@"scrollview 3 - > %f %f",scrollView3.contentSize.height,scrollView3.frame.size.height);
    

}

-(void)checkValidNumber:(NSNotification *)notification
{
    if(self.isViewLoaded && self.view.window)
    {
        if(pageControl.currentPage == 1)
        {
            signupScrollView.scrollEnabled = FALSE;
            [self performSelector:@selector(sendisValidRequest) withObject:nil afterDelay:3];
        }
    }
}

#pragma pageControl delegate method

- (IBAction)pageChange:(id)sender {
    int page = pageControl.currentPage;
    
    
    // update the scroll view to the appropriate page
    CGRect frame = signupScrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    
    [signupScrollView scrollRectToVisible:frame animated:NO];
    
//    [UIView animateWithDuration:0.5
//                          delay:0
//                        options:UIViewAnimationOptionCurveLinear
//                     animations:^{ [signupScrollView scrollRectToVisible:frame animated:NO]; }
//                     completion:NULL];
//    
    //[gridScrollView scrollRectToVisible:frame animated:YES];
    
    // Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
    pageControlUsed = YES;
}

- (IBAction)nextBntPressed:(id)sender {
    [self scrollTobottom];
    passwordTextField.text = verrificationTextField.text;
    retypepasswordTextField.text = verrificationTextField.text;
    if(([passwordTextField.text isEqualToString:@""] && [retypepasswordTextField.text isEqualToString:@""]) || ([verrificationTextField.text isEqualToString:@""] && !verrificationTextField.isHidden))
    {
        [INUserDefaultOperations showAlert:@"Please fill all the fields."];
    }
    else if(([passwordTextField.text length] < 6) || ([retypepasswordTextField.text length] < 6))
    {
        [INUserDefaultOperations showAlert:@"Password field must be at least 6 characters in length."];
    } else if(![passwordTextField.text isEqualToString:retypepasswordTextField.text])
    {
        [INUserDefaultOperations showAlert:@"Passwords do not match.Please check your passwords and try again"];
    } else {
        if ([IN_APP_DELEGATE networkavailable]) {
//            if(screenType == 0)
//            {
//                [self sendPasswordRequest];
//            }
//            else {
                [self sendPasswordWithCodeRequest];
//            }
        } else {
            [INUserDefaultOperations showOfflineAlert];
        }
    }
}

-(void)setCountryCodeBtnPressed
{
    [ActionSheetStringPicker showPickerWithTitle:@"Select Country" rows:countryCodePickerArray initialSelection:0 target:self successAction:@selector(selectCode:element:) cancelAction:@selector(actionPickerCancelled:) origin:self.codeTextField];
}

#pragma mark - Actionsheet Implementation
- (void)selectCode:(NSNumber *)lselectedIndex element:(id)element {
    NSInteger selectedIndex = [lselectedIndex integerValue];
    [self scrollTobottom];
    INCountryCodeObj *codeObj = [countryCodeArray objectAtIndex:selectedIndex];
    DebugLog(@"object-->%@  %@ %@",codeObj.countryName,codeObj.countrycode,codeObj.countrydialcode);
    codeTextField.text = codeObj.countrydialcode;
    DebugLog(@"value-->%@",[countryCodePickerArray objectAtIndex:selectedIndex]);
}

- (void)actionPickerCancelled:(id)sender {
    DebugLog(@"ActionSheetPicker was cancelled");
    [self scrollTobottom];
}


- (IBAction)callBtnPressed:(id)sender {
    NSString *deviceType = [UIDevice currentDevice].model;
    if([deviceType isEqualToString:@"iPhone"])
    {
        DebugLog(@"call - >%@",[NSString stringWithFormat:@"tel://%@",callNumber]);
        UIWebView *callWebview = [[UIWebView alloc] init];
        NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",callNumber]];
        [callWebview loadRequest:[NSURLRequest requestWithURL:telURL]];
        [self.view addSubview:callWebview];
    }else{
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: [NSString stringWithFormat:@"This device cannot make a call. Please call on '%@' from  '+%@'. We will send the verification code to '+%@' so keep it near you.",callNumber, MOBILE_NUMBER(codeTextField.text, numberTextField.text), MOBILE_NUMBER(codeTextField.text, numberTextField.text)]
                                                          delegate: nil
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"OK", nil
                              ];
        [ alert show ];
        [proceedafterCallBtn setHidden:FALSE];
        [callBtn setHidden:TRUE];
    }
  
    //UIApplication *myApp = [UIApplication sharedApplication];
    //[myApp openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",callNumber]]];
}

- (IBAction)proceedBtnPressed:(id)sender {
    [self scrollTobottom];
    if([numberTextField.text isEqualToString:@""])
    {
        [INUserDefaultOperations showAlert:@"Please enter valid mobile number."];
    }
    else if([numberTextField.text length] < 10)
    {
        [INUserDefaultOperations showAlert:@"Mobile number must be at least 10 characters in length."];
    } else if([numberTextField.text length] > 10)
    {
        [INUserDefaultOperations showAlert:@"Mobile number must be 10 characters in length."];
    } else {
        if ([IN_APP_DELEGATE networkavailable]) {
                registerDate = [NSDate date];
                [self sendNumberRegisterRequest];
            } else {
                [INUserDefaultOperations showOfflineAlert];
            }
    }
//    else if([[INUserDefaultOperations getMerchantTermsConditionsValue] isEqualToString:@"0"] && screenType==0) {
//        if ([IN_APP_DELEGATE networkavailable]) { //Sign Up screen. Show confirm T&C dialog.
//            [self termsBtnPressed:nil];
//        } else {
//            [INUserDefaultOperations showAlert:@"Please check your internet connection and try again."];
//        }
//    }
//    else {
//        if(screenType==1)  //Forgot pass screen. Do not show confirm number dialog.
//        {
//            if ([IN_APP_DELEGATE networkavailable]) {
//                [self sendNumberRegisterRequest];
//            } else {
//                [INUserDefaultOperations showOfflineAlert];
//            }
//        } else {
//            if ([IN_APP_DELEGATE networkavailable]) {
//                codelbl.text = codeTextField.text;
//                confirmnumberTextField.text = numberTextField.text;
//                [confirmView setHidden:FALSE];
//                
//                [proceedBtn setUserInteractionEnabled:FALSE];
//                [termsBtn setUserInteractionEnabled:FALSE];
//                [questionBtn setUserInteractionEnabled:FALSE];
//                [privacyBtn setUserInteractionEnabled:FALSE];
//
//                //add animation code here
//                [CommonCallback viewtransitionInCompletion:confirmView completion:^{
//                    [CommonCallback viewtransitionOutCompletion:confirmView completion:nil];
//                }];
//            } else {
//                [INUserDefaultOperations showOfflineAlert];
//            }
//        }
//    }
}

- (IBAction)confirmNumberBtnPressed:(id)sender {
    if([confirmnumberTextField.text isEqualToString:@""])
    {
        [INUserDefaultOperations showAlert:@"Please enter valid mobile number."];
    }
    else if([confirmnumberTextField.text length] < 10)
    {
        [INUserDefaultOperations showAlert:@"Mobile number must be at least 10 characters in length."];
    } else if([confirmnumberTextField.text length] > 10)
    {
        [INUserDefaultOperations showAlert:@"Mobile number must be 10 characters in length."];
    } else {
        [proceedBtn setUserInteractionEnabled:TRUE];
        [termsBtn setUserInteractionEnabled:TRUE];
        [questionBtn setUserInteractionEnabled:TRUE];
        [privacyBtn setUserInteractionEnabled:TRUE];
        numberTextField.text = confirmnumberTextField.text;
        [self sendNumberRegisterRequest];
    }
}

- (IBAction)confirmCancelBtnPressed:(id)sender {
    [proceedBtn setUserInteractionEnabled:TRUE];
    [termsBtn setUserInteractionEnabled:TRUE];
    [questionBtn setUserInteractionEnabled:TRUE];
    [privacyBtn setUserInteractionEnabled:TRUE];
    [confirmView setHidden:TRUE];
}

- (IBAction)questionBtnPressed:(id)sender {
    [self scrollTobottom];
    [alertView show];
}

- (IBAction)privacyBtnPressed:(id)sender {
    if ([IN_APP_DELEGATE networkavailable]) {
        INPrivacyPolicyViewController *privacyController = [[INPrivacyPolicyViewController alloc] initWithNibName:@"INPrivacyPolicyViewController" bundle:nil] ;
        privacyController.title = @"Privacy Policy";
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:privacyController];
        navController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        navController.modalPresentationStyle = UIModalPresentationFormSheet;
        navController.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
        //    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
        //                                                                                   target:self
        //                                                                                   action:@selector(didDismissPresentedViewController)];
        //    loginController.navigationItem.rightBarButtonItem = doneBarButton;
        privacyController.navigationItem.title = @"Privacy Policy";
        //termsController.delegate = self;
        [self presentViewController:navController animated:YES completion:NULL];
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

- (IBAction)editNumberBtnPressed:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"Dismiss");
        [INUserDefaultOperations setMerchantTermsConditionsValue:@"0"];
    }];
}

- (IBAction)proceedafterCallBtnPressed:(id)sender {
    [self sendisValidRequest];
}

- (IBAction)resendBtnPressed:(id)sender {
    [self scrollTobottom];
    if ([IN_APP_DELEGATE networkavailable]) {
        //[self sendVerificationCodeRequest]; //For calling Server API to get verification code
        [self sendNumberRegisterRequest];
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

- (IBAction)termsBtnPressed:(id)sender {
    [self scrollTobottom];
    if ([IN_APP_DELEGATE networkavailable]) {
        INTermsViewController *termsController = [[INTermsViewController alloc] initWithNibName:@"INTermsViewController" bundle:nil] ;
        termsController.title = @"";
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:termsController];
        navController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        navController.modalPresentationStyle = UIModalPresentationFormSheet;
        navController.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
        //    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
        //                                                                                   target:self
        //                                                                                   action:@selector(didDismissPresentedViewController)];
        //    loginController.navigationItem.rightBarButtonItem = doneBarButton;
        termsController.navigationItem.title = @"Terms & Conditions";
        //termsController.delegate = self;
        [self presentViewController:navController animated:YES completion:NULL];
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }

}

#pragma scrollViewDelegates
- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    if (pageControlUsed)
    {
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }
    CGFloat pageWidth = signupScrollView.frame.size.width;
    int page = floor((signupScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if(pageControl.currentPage != page) {
        pageControl.currentPage = page;
        
    }
}

// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
    if(pageControl.currentPage == 0)
    {
        signupScrollView.scrollEnabled = FALSE;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setSignupScrollView:nil];
    [self setPageControl:nil];
    [self setPasswordTextField:nil];
    [self setNumberTextField:nil];
    [self setVerrificationTextField:nil];
    [self setMessagelbl:nil];
    [self setRetypepasswordTextField:nil];
    [self setPhoneNumberScreenlbl:nil];
    [self setResendBtn:nil];
    [self setNextBtn:nil];
    [self setRegisterstatuslbl:nil];
    [self setPhoneNumberScreenlbl1:nil];
    [self setPhoneNumberScreenlbl2:nil];
    [self setPhoneNumberScreenlbl3:nil];
    [self setPhoneNumberScreenforgotlbl:nil];
    [self setLabelsCollection:nil];
    [self setButtonsCollection:nil];
    [self setConfirmView:nil];
    [self setNumberconfirmlbl:nil];
    [self setConfirmnumberTextField:nil];
    [self setConfirmProceedBtn:nil];
    [self setConfirmCancelBtn:nil];
    [self setCodeTextField:nil];
    [self setCodelbl:nil];
    [self setPhoneNumberScreenlbl4:nil];
    [self setQuestionBtn:nil];
    [self setBackView3:nil];
    [self setBackView2:nil];
    [self setBackView1:nil];
    [self setScrollView3:nil];
    [self setScrollView1:nil];
    [self setTermsBtn:nil];
    [self setProceedBtn:nil];
    [self setPrivacyBtn:nil];
    [self setResendsmslbl:nil];
    [self setSignupIndicatior:nil];
    [self setCallscrlbl:nil];
    [self setCallBtn:nil];
    [self setProceedafterCallBtn:nil];
    [super viewDidUnload];
}


-(void)sendNumberRegisterRequest
{
    //[hud show:YES];
    DebugLog(@"number--->%@",MOBILE_NUMBER(codeTextField.text, numberTextField.text));
    callscrlbl.text = MOBILE_NUMBER(codeTextField.text, numberTextField.text);
    //[CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    [phoneNumberScreenforgotlbl setHidden:FALSE];
    DebugLog(@"%ld",(long)pageControl.currentPage);
    if(pageControl.currentPage ==0)
    {
        [signupIndicatior startAnimating];
    } else if(pageControl.currentPage==2)
    {
        [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    }
    DebugLog(@"link--->%@",REGISTER_MERCHANT);
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:REGISTER_MERCHANT] cachePolicy:NO timeoutInterval:30.0];
    NSMutableDictionary *postReq = [[NSMutableDictionary alloc] init];
    [postReq setObject:MOBILE_NUMBER(codeTextField.text, numberTextField.text) forKey:@"mobile"];
    [postReq setObject:DEVICE_TYPE forKey:@"register_from"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postReq
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (!jsonData) {
        DebugLog(@"Got an error: %@", error);
    }
    [urlRequest setHTTPBody: jsonData];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody: jsonData];
    connectionRegisterNumber = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}


-(void)sendisValidRequest
{
     DebugLog(@"link--->%@",ISVERIFIED_MERCHANT(MOBILE_NUMBER(codeTextField.text, numberTextField.text)));
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:ISVERIFIED_MERCHANT(MOBILE_NUMBER(codeTextField.text, numberTextField.text))] cachePolicy:NO timeoutInterval:30.0];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setHTTPMethod:@"GET"];
    connectionisVerifiedNumber = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)sendPasswordRequest
{
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    DebugLog(@"link--->%@,%@,%@",SET_PASSWORD_MERCHANT,MOBILE_NUMBER(codeTextField.text, numberTextField.text),passwordTextField.text);
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:SET_PASSWORD_MERCHANT] cachePolicy:NO timeoutInterval:30.0];
    NSMutableDictionary *postReq = [[NSMutableDictionary alloc] init];
    [postReq setObject:MOBILE_NUMBER(codeTextField.text, numberTextField.text) forKey:@"mobile"];
    [postReq setObject:passwordTextField.text forKey:@"password"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postReq
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (!jsonData) {
        DebugLog(@"Got an error: %@", error);
    }
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setValue:@"PUT" forHTTPHeaderField:@"X-HTTP-Method-Override"];
    
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody: jsonData];
    connectionsetPassword = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)sendPasswordWithCodeRequest
{
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    DebugLog(@"link--->%@,%@,%@",SET_PASSWORD_MERCHANT,MOBILE_NUMBER(codeTextField.text, numberTextField.text),passwordTextField.text);
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:SET_PASSWORD_MERCHANT] cachePolicy:NO timeoutInterval:30.0];
    NSMutableDictionary *postReq = [[NSMutableDictionary alloc] init];
    [postReq setObject:MOBILE_NUMBER(codeTextField.text, numberTextField.text) forKey:@"mobile"];
    [postReq setObject:passwordTextField.text forKey:@"password"];
    [postReq setObject:verrificationTextField.text forKey:@"verification_code"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postReq
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (!jsonData) {
        DebugLog(@"Got an error: %@", error);
    }
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setValue:@"PUT" forHTTPHeaderField:@"X-HTTP-Method-Override"];
    
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody: jsonData];
    connectionsetPasswordWithCode = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)sendVerificationCodeRequest
{
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    DebugLog(@"link--->%@",VERIFIED_CODE_MERCHANT(MOBILE_NUMBER(codeTextField.text, numberTextField.text)));
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:VERIFIED_CODE_MERCHANT(MOBILE_NUMBER(codeTextField.text, numberTextField.text))] cachePolicy:NO timeoutInterval:30.0];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setHTTPMethod:@"GET"];
    connectiongetVerifyCode = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}


#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    DebugLog(@"Error: %@", [error localizedDescription]);
    //[hud hide:YES];
    [CommonCallback hideProgressHud];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError* error;
//  [hud hide:YES];
    [CommonCallback hideProgressHud];
    if (connection==connectionRegisterNumber)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            DebugLog(@"json: %@", json);
            [signupIndicatior stopAnimating];
            [phoneNumberScreenforgotlbl setHidden:TRUE];
            if(json != nil) {
                
                DebugLog(@"diff -- >%f",[[NSDate date] timeIntervalSinceDate:registerDate]);
                
                if([[NSDate date] timeIntervalSinceDate:registerDate] < 1.0f)
                {
                    DebugLog(@"less than 1");
                    sleep(3);
                } else if([[NSDate date] timeIntervalSinceDate:registerDate] < 2.0f) {
                    DebugLog(@"less than 2");
                    sleep(2);
                } else {
                    DebugLog(@"more than 2");
                }
                if([[json  objectForKey:@"success"] isEqualToString:@"true"]) {
                    [confirmView setHidden:TRUE];
                    pageControl.currentPage = 1;
                    [self pageChange:nil];
                    [INEventLogger logEvent:@"MSignUp_New"];
                    [INUserDefaultOperations setMerchantTermsConditionsValue:@"0"];
                    [INUserDefaultOperations setMerchantCountryCode:[codeTextField.text stringByReplacingOccurrencesOfString:@"+" withString:@""]];
                    signupScrollView.scrollEnabled = FALSE;
                    registerstatuslbl.text = [json  objectForKey:@"message"];
                    NSDictionary* callDict = [json objectForKey:@"data"];
                    callNumber = [[callDict objectForKey:@"action_call"] stringByReplacingOccurrencesOfString:@" " withString:@""];
                    DebugLog(@"%@",callNumber);
                } else if([[json  objectForKey:@"success"] isEqualToString:@"false"]) {
                    [confirmView setHidden:TRUE];
                    //screenType = 1;
                    pageControl.currentPage = 2;
                    messagelbl.text = [json  objectForKey:@"message"];
                    [self pageChange:nil];
                    [INEventLogger logEvent:@"MSignUp_Return"];
                    [INUserDefaultOperations setMerchantTermsConditionsValue:@"0"];
                    [INUserDefaultOperations setMerchantCountryCode:[codeTextField.text stringByReplacingOccurrencesOfString:@"+" withString:@""]];
                    signupScrollView.scrollEnabled = FALSE;
                    [INUserDefaultOperations setMerchantDetails:mobileNumber pass:nil];
                    [INUserDefaultOperations clearMerchantDetails];
                    [INUserDefaultOperations setMerchantLoginState:[json  objectForKey:@"message"] visitedPage:2];

                }
            } else {
                [INUserDefaultOperations showAlert:@"No response from server. Please try again."];
                [signupIndicatior stopAnimating];
                [phoneNumberScreenforgotlbl setHidden:TRUE];
            }
        }
        responseAsyncData = nil;
    } else if (connection==connectionisVerifiedNumber) {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            DebugLog(@"json: %@", json);
            if(json != nil) {
                if([json  objectForKey:@"message"] != [NSNull null] && [[json  objectForKey:@"success"] isEqualToString:@"false"])
                {
                    [INUserDefaultOperations showAlert:[json  objectForKey:@"message"]];
                } else if([[json  objectForKey:@"success"] isEqualToString:@"true"]){
//                    if (screenType == 0) { //Sign Up Screen
//                        verrificationTextField.hidden = TRUE;
//                        resendBtn.hidden = TRUE;
//                        nextBtn.frame = CGRectMake(20, nextBtn.frame.origin.y, 257, nextBtn.frame.size.height);
//                        messagelbl.text = [json  objectForKey:@"message"];
//                    } else { //Forgot password Screen
                        verrificationTextField.hidden = FALSE;
                        resendBtn.hidden = FALSE;
//                        nextBtn.frame = CGRectMake(23, nextBtn.frame.origin.y, 110, nextBtn.frame.size.height);
                        messagelbl.text = [json  objectForKey:@"message"];
//                    }
                    pageControl.currentPage = 2;
                    [self pageChange:nil];
                    signupScrollView.scrollEnabled = FALSE;
                    [INUserDefaultOperations setMerchantDetails:mobileNumber pass:nil];
                    [INUserDefaultOperations clearMerchantDetails];
                    [INUserDefaultOperations setMerchantLoginState:[json  objectForKey:@"message"] visitedPage:2];
                }
            }
        }
        responseAsyncData = nil;
    } else if (connection==connectionsetPassword) {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            DebugLog(@"json: %@", json);
            if(json != nil) {
                if([[json  objectForKey:@"success"] isEqualToString:@"true"]) {
                    NSString* code = [json objectForKey:@"message"];
                    DebugLog(@"%@",code);
                    if([code isEqualToString:MERCHANT_PASSWORD_SET_SUCCESS])
                    {
                        //[INUserDefaultOperations showAlert:message];
                        DebugLog(@"Password is set successfully.");
                        if (self.logindelegate && [self.logindelegate respondsToSelector:@selector(loginUserWithIdAndPassword:pass:showAddStore:)]) {
                            [self.logindelegate loginUserWithIdAndPassword:numberTextField.text pass:passwordTextField.text showAddStore:1];
                        }
                    } else if([code isEqualToString:MERCHANT_PASSWORD_ALREADY_SET]){
                        [INUserDefaultOperations showAlert:[json objectForKey:@"message"]];
                        DebugLog(@"Password has been already set.");
                    }
                } else {
                    NSString* message = [json objectForKey:@"message"];
                    [INUserDefaultOperations showAlert:message];
                }
            } else {
                [INUserDefaultOperations showAlert:@"Some error occured. Please try again."];
            }
        }
        responseAsyncData = nil;
    } else if (connection==connectionsetPasswordWithCode) {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            DebugLog(@"connectionsetPasswordWithCode json: %@", json);
            if(json != nil) {
                if([[json  objectForKey:@"success"] isEqualToString:@"true"]) {
                    NSString* message = [json objectForKey:@"message"];
                    DebugLog(@"%@",message);
                    if (self.logindelegate && [self.logindelegate respondsToSelector:@selector(loginUserWithIdAndPassword:pass:showAddStore:)]) {
                        DebugLog(@"%@ %@",numberTextField.text, passwordTextField.text);
                        [self.logindelegate loginUserWithIdAndPassword:numberTextField.text pass:passwordTextField.text showAddStore:1];
                        [INUserDefaultOperations clearMerchantLoginState];
                        [INEventLogger logEvent:@"MSignUp_VerificationCode"];
                    }
                }
                else if ([[json  objectForKey:@"success"] isEqualToString:@"false"]) {
                    [INUserDefaultOperations showAlert:[json objectForKey:@"message"]];
                } else {
                    [INUserDefaultOperations showAlert:@"No response from server. Please try again."];
                }
            }
        }
        responseAsyncData = nil;
    } else if (connection==connectiongetVerifyCode) {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            DebugLog(@"json: %@", json);
            if(json != nil) {
                if([[json objectForKey:@"success"] isEqualToString:@"true"])
                {
                    NSDictionary* data = [json objectForKey:@"data"];
                    NSString *code = [data objectForKey:@"verification_code"];
                    verrificationTextField.text = code;
                } else {
                    NSString* message = [json objectForKey:@"message"];
                    DebugLog(@"%@",message);
                }
            } else {
                [INUserDefaultOperations showAlert:@"Some error occured. Please try again."];
            }
        }
        responseAsyncData = nil;
    }
}
@end
