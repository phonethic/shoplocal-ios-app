//
//  INStoreDetailViewController.h
//  shoplocal
//
//  Created by Sagar Mody on 12/01/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HMSegmentedControl.h"
#import "FXImageView.h"
#import "MWPhotoBrowser.h"
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import "FaceBookShareViewController.h"
#import "INCustomerLoginViewController.h"
#import "VCGridView.h"

@class INStoreDetailObj;
@class INBroadcastDetailObj;
@interface INStoreDetailViewController : UIViewController <MWPhotoBrowserDelegate,UIActionSheetDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,FacebookProtocolDelegate,INCustomerLoginViewControllerDelegate,VCGridViewDelegate, VCGridViewDataSource>
{
    INStoreDetailObj *storeObj;
    INBroadcastDetailObj *broadcastObj;
    MBProgressHUD *hud;
}
@property (nonatomic, strong) HMSegmentedControl *segmentedControl1;
@property (strong, nonatomic) IBOutlet UIScrollView *detailScrollView;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *viewsCollection;
@property (strong, nonatomic) IBOutlet UIImageView *logoImageView;
@property (strong, nonatomic) IBOutlet UILabel *lblstoretime;
@property (strong, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (strong, nonatomic) IBOutlet UIButton *callStoreBtn;
@property (strong, nonatomic) IBOutlet UIButton *likeStore;
@property (strong, nonatomic) IBOutlet UIButton *shareStoreBtn;
@property (strong, nonatomic) IBOutlet UITextView *addressView;
@property (strong, nonatomic) IBOutlet UILabel *lblemail;
@property (strong, nonatomic) IBOutlet UITableView *broadcastTableView;
@property (strong, nonatomic) IBOutlet UIView *galleryView;
@property (strong, nonatomic) IBOutlet UILabel *numberslbl;
@property (strong, nonatomic) IBOutlet UIImageView *emailImageView;
@property (strong, nonatomic) IBOutlet UILabel *broadcastlbl;
@property (strong, nonatomic) IBOutlet UIImageView *broadcastImageView;
@property (strong, nonatomic) IBOutlet UILabel *gallerylbl;
@property (strong, nonatomic) IBOutlet UIImageView *galleryImageView;

@property (strong, nonatomic) NSMutableArray *storeArray;
@property (strong, nonatomic) NSMutableArray *galleryArray;
@property (strong, nonatomic) NSMutableArray *broadcastArray;
@property (nonatomic, readwrite) NSInteger storeId;
@property (nonatomic, copy) NSString *storeImageUrl;
@property (strong, nonatomic) NSMutableArray *photos;
@property (copy, nonatomic) NSMutableArray *telArray;
@property (nonatomic, copy) NSString *selectedTelNumberToCall;
@property (nonatomic, retain) VCGridView *gridView;
@property (strong, nonatomic) IBOutlet UIButton *emailBtn;

@property (strong, nonatomic) IBOutlet UIButton *fbBtn;
@property (strong, nonatomic) IBOutlet UIButton *webBtn;
@property (strong, nonatomic) IBOutlet UIButton *twBtn;


@property (strong, nonatomic) IBOutlet UIView *callModalViewBackView;
@property (strong, nonatomic) IBOutlet UIView *callModalView;
@property (strong, nonatomic) IBOutlet UILabel *lblcallModalHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblcallModalHeaderSeperator;
@property (strong, nonatomic) IBOutlet UITableView *callModalTableView;
@property (strong, nonatomic) IBOutlet UIButton *btnCancelcallModal;

- (IBAction)btnCancelcallModalPressed:(id)sender;

- (IBAction)fbBtnPressed:(id)sender;
- (IBAction)twBtnPressed:(id)sender;
- (IBAction)webBtnPressed:(id)sender;
- (IBAction)callStoreBtnPressed:(id)sender;
- (IBAction)likeStoreBtnPressed:(id)sender;
- (IBAction)shareStoreBtnPressed:(id)sender;
- (IBAction)getDirectionsBtnPressed:(id)sender;
- (IBAction)emailBtnPressed:(id)sender;
@end
