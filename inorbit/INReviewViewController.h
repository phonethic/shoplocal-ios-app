//
//  INReviewViewController.h
//  inorbit
//
//  Created by Rishi on 20/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface INReviewViewController : UIViewController
{
    
}
@property (strong, nonatomic) IBOutlet UILabel *datelbl;
@property (strong, nonatomic) IBOutlet UILabel *descriptionlbl;
@property (strong, nonatomic) IBOutlet UILabel *titlelbl;

- (IBAction)editBtnPressed:(id)sender;
- (IBAction)postBtnPressed:(id)sender;

@end
