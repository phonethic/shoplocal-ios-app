//
//  INGalleryObj.m
//  shoplocal
//
//  Created by Sagar Mody on 24/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INGalleryObj.h"

@implementation INGalleryObj
@synthesize galleryID;
@synthesize image_date;
@synthesize image_url;
@synthesize place_id;
@synthesize thumb_url;
@synthesize title;
@end
