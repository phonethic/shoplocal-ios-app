//
//  INAllStoreViewController.m
//  shoplocal
//
//  Created by Rishi on 07/03/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import "INAllStoreViewController.h"
#import "INAppDelegate.h"
#import "INAllStoreObj.h"
#import "INDetailViewController.h"
#import "CommonCallback.h"
#import "INSearchViewController.h"
#import "INStoreDetailViewController.h"
#import "UIImageView+WebCache.h"
#import "INCategoryViewController.h"

#define ALL_STORE_LINK(LAT,LONG,DIST,LOCALITY,PAGE) [NSString stringWithFormat:@"%@%@%@place_api/search?latitude=%f&longitude=%f&distance=%d&area_id=%@&page=%@&count=50",LIVE_SERVER,URL_PREFIX,API_VERSION,LAT,LONG,DIST,LOCALITY,PAGE]
#define THUMBNAIL_LINK(IMG_LINK) [NSString stringWithFormat:@"%@%@%@",LIVE_SERVER,URL_PREFIX,IMG_LINK]

#define ALLSTORE_SYNC_TIME_INTERVAL 24

#define SEARCH_CATEGORY [NSString stringWithFormat:@"%@%@%@place_api/place_category",LIVE_SERVER,URL_PREFIX,API_VERSION]

#define CALL_STORE [NSString stringWithFormat:@"%@%@broadcast_api/call",URL_PREFIX,API_VERSION]

@interface INAllStoreViewController ()
@property(strong) NSDictionary *splashJson;
@end

@implementation INAllStoreViewController
@synthesize allstoreTableView;
@synthesize storeArray;
@synthesize tempstoreArray;
@synthesize countlbl;
@synthesize textPull, textRelease, textLoading, refreshHeaderView, refreshLabel, refreshArrow, refreshSpinner;
@synthesize totalPage;
@synthesize totalRecord;
@synthesize currentPage;
@synthesize countHeaderView,lblpullToRefresh;
@synthesize globalSearchBar;
@synthesize comeFrom;
@synthesize searchView,searchTextField;
@synthesize callModalViewBackView,callModalView,lblcallModalHeader,lblcallModalHeaderSeperator,callModalTableView,btnCancelcallModal;
@synthesize telArray,selectedPlaceId,selectedPlaceName;
@synthesize changeLocationInfoImageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self setupStrings];
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:FALSE];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:FALSE];
    
    NSString *selectedArea = [IN_APP_DELEGATE getActivePlaceNameFromAreaTable];
    DebugLog(@"selectedArea %@",selectedArea);
    [locatonButton setTitle:selectedArea forState:UIControlStateNormal];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:TRUE];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:TRUE];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self setupMenuBarButtonItems];
    
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                             style:UIBarButtonItemStylePlain
                                                            target:nil
                                                            action:nil];
    [[self navigationItem] setBackBarButtonItem:back];
    [self setUI];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        globalSearchBar.barTintColor = DEFAULT_COLOR;
        [globalSearchBar setTranslucent:NO];
    }
    globalSearchBar.showsCancelButton = YES;
    globalSearchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    globalSearchBar.delegate =  self;
    globalSearchBar.hidden = TRUE;
    [globalSearchBar setFrame:CGRectMake(0, -globalSearchBar.frame.size.height, globalSearchBar.frame.size.width, globalSearchBar.frame.size.height)];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadData:) name:IN_STORE_DID_CHANGE_NOTIFICATION object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showStoreDetailsNotitification:) name:IN_CUSTOMER_SHOW_STORE_DETAILS_NOTIFICATION object:nil];

    //[self addHUDView];
    searchResults = [[NSMutableArray alloc] init];
    storeArray = [[NSMutableArray alloc] init];
    tempstoreArray = [[NSMutableArray alloc] init];
    [self addPullToRefreshHeader];
    //[self loadData];
    
    DebugLog(@"last app time ---> %ld",(long)[INUserDefaultOperations getInfoScreenCount]);
    if([INUserDefaultOperations getInfoScreenCount] < 3)
    {
        UITapGestureRecognizer *infoimageViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(infoimageViewTapDetected:)];
        infoimageViewTap.numberOfTapsRequired = 1;
        [changeLocationInfoImageView addGestureRecognizer:infoimageViewTap];
        
        changeLocationInfoImageView.hidden = NO;
        changeLocationInfoImageView.alpha = 1.0f;
        
        [self performSelector:@selector(popupInfoImage) withObject:nil afterDelay:3.0];
    } else {
        changeLocationInfoImageView.hidden = YES;
    }
    [self performSelector:@selector(loadData) withObject:nil afterDelay:1.0];
    //    if (comeFrom == SPLASH_SCREEN) {
    //        [self performSelector:@selector(openSideMenu) withObject:nil afterDelay:1.0];
    //    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setAllstoreTableView:nil];
    [self setCountlbl:nil];
    [self setCountHeaderView:nil];
    [self setLblpullToRefresh:nil];
    [self setGlobalSearchBar:nil];
    [super viewDidUnload];
}

- (void)dealloc {
   // [[NSNotificationCenter defaultCenter] removeObserver:self];
    //[self removeHUDView];
}

-(void)openSideMenu
{
    self.navigationController.sideMenu.menuSlideAnimationEnabled = TRUE;
    [self.navigationController.sideMenu setMenuState:MFSideMenuStateLeftMenuOpen];
    // [self.navigationController.sideMenu setPanMode:MFSideMenuPanDirectionLeft
}

#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
#pragma set Navigation barbuttons callback
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
        {
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
//            UIBarButtonItem *filterBarBtn = [[UIBarButtonItem alloc] initWithTitle:@"Search" style:UIBarButtonItemStylePlain target:self action:@selector(filterBarBtnClicked:)];
            self.navigationItem.rightBarButtonItem = nil;//filterBarBtn;
        }
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
        {
//            UIBarButtonItem *filterBarBtn = [[UIBarButtonItem alloc] initWithTitle:@"Search" style:UIBarButtonItemStylePlain target:self action:@selector(filterBarBtnClicked:)];
            self.navigationItem.rightBarButtonItem = nil;//filterBarBtn;
        }
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:MENU_TITLE style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    return [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(searchBarBtnClicked:)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)popupInfoImage
{
    if (!changeLocationInfoImageView.isHidden) {
        [self.view bringSubviewToFront:changeLocationInfoImageView];
        changeLocationInfoImageView.hidden = NO;
        changeLocationInfoImageView.alpha = 1.0f;
        [UIView animateWithDuration:1.0 animations:^{
            changeLocationInfoImageView.alpha = 0.0f;
        } completion:^(BOOL finished) {
            changeLocationInfoImageView.hidden = YES;
            DebugLog(@"last app time ---> %ld",(long)[INUserDefaultOperations getInfoScreenCount]);
            NSUInteger totalTime = [INUserDefaultOperations getInfoScreenCount] + 1 ;
            DebugLog(@"%ld",(long)totalTime);
            [INUserDefaultOperations setInfoScreenCount:totalTime];
        }];
    }
}

- (void)infoimageViewTapDetected:(UIGestureRecognizer *)sender {
    [UIView animateWithDuration:0.4 animations:^{
        changeLocationInfoImageView.alpha = 0.0f;
    } completion:^(BOOL finished) {
        changeLocationInfoImageView.hidden = YES;
        DebugLog(@"last app time ---> %ld",(long)[INUserDefaultOperations getInfoScreenCount]);
        NSUInteger totalTime = [INUserDefaultOperations getInfoScreenCount] + 1 ;
        DebugLog(@"%ld",(long)totalTime);
        [INUserDefaultOperations setInfoScreenCount:totalTime];
    }];
}

- (void)searchBarBtnClicked:(id)sender {
    globalSearchBar.text = @"";
    [globalSearchBar becomeFirstResponder];
    
    self.navigationItem.rightBarButtonItems = nil;
    UIBarButtonItem *filterBarBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"filter.png"]  style:UIBarButtonItemStylePlain target:self action:@selector(filterBarBtnClicked:)];
    self.navigationItem.rightBarButtonItem = filterBarBtn;
    globalSearchBar.hidden = FALSE;
    
    [UIView animateWithDuration:0.3 animations:^{
        [globalSearchBar setFrame:CGRectMake(0, 0, globalSearchBar.frame.size.width, globalSearchBar.frame.size.height)];
        [countHeaderView setFrame:CGRectMake(0, CGRectGetMaxY(globalSearchBar.frame), countHeaderView.frame.size.width, countHeaderView.frame.size.height)];
        [allstoreTableView setFrame:CGRectMake(0, CGRectGetMaxY(countHeaderView.frame)+1, allstoreTableView.frame.size.width, self.view.frame.size.height-(countHeaderView.frame.size.height+globalSearchBar.frame.size.height))];
    }];
}

-(void)searchBarCancelBtnClicked {
    globalSearchBar.text = @"";
    [globalSearchBar resignFirstResponder];
    
    self.navigationItem.rightBarButtonItem = nil;
    //    UIBarButtonItem *filterBarBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"filter.png"]  style:UIBarButtonItemStylePlain target:self action:@selector(filterBarBtnClicked:)];
    //    UIBarButtonItem *searchBtn =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(searchBarBtnClicked:)];
    //    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:searchBtn,filterBarBtn, nil];
    UIBarButtonItem *filterBarBtn = [[UIBarButtonItem alloc] initWithTitle:@"Search" style:UIBarButtonItemStylePlain target:self action:@selector(filterBarBtnClicked:)];
    self.navigationItem.rightBarButtonItem = filterBarBtn;
    
    [UIView animateWithDuration:0.3 animations:^{
        [globalSearchBar setFrame:CGRectMake(0, -globalSearchBar.frame.size.height, globalSearchBar.frame.size.width, globalSearchBar.frame.size.height)];
        [countHeaderView setFrame:CGRectMake(0, 0, countHeaderView.frame.size.width, countHeaderView.frame.size.height)];
        [allstoreTableView setFrame:CGRectMake(0, CGRectGetMaxY(countHeaderView.frame)+1, allstoreTableView.frame.size.width, self.view.frame.size.height-countHeaderView.frame.size.height)];
    } completion:^(BOOL finished){
        globalSearchBar.hidden = TRUE;
    }];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    INSearchViewController *searchController = [[INSearchViewController alloc] initWithNibName:@"INSearchViewController" bundle:nil] ;
    searchController.title = @"Search";
    searchController.textToSearch = searchBar.text;
    searchController.isStoreSearch = YES;
    [self.navigationController pushViewController:searchController animated:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar{
   // [self searchBarCancelBtnClicked];
}

- (void)filterBarBtnClicked:(id)sender {
    DebugLog(@"filterBarBtnClicked");
    INSearchViewController *searchController = [[INSearchViewController alloc] initWithNibName:@"INSearchViewController" bundle:nil] ;
    searchController.title = @"Search";
    searchController.showCancelBarBtn = YES;
    searchController.isStoreSearch = YES;
    [UIView  transitionWithView:self.navigationController.view duration:0.5  options:UIViewAnimationOptionTransitionCrossDissolve
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         [self.navigationController pushViewController:searchController animated:NO];
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:nil];
}

-(void)showStoreDetailsNotitification:(NSNotification *)notification
{
    NSDictionary *dict              = [notification userInfo];
    NSString *storeId               = [dict objectForKey:STORE_ID];
    DebugLog(@"storeId %@",storeId);
    
    INStoreDetailViewController *detailController = [[INStoreDetailViewController alloc] initWithNibName:@"INStoreDetailViewController" bundle:nil] ;
    detailController.storeId    = [storeId integerValue];
    [self.navigationController pushViewController:detailController animated:YES];
}

#pragma internal methods
-(void)setUI
{
    allstoreTableView.backgroundColor   =   [UIColor clearColor];
    allstoreTableView.separatorStyle    =   UITableViewCellSeparatorStyleNone;
    
    countHeaderView.backgroundColor =   [UIColor clearColor];
    countHeaderView.clipsToBounds   = YES;
    
    lblpullToRefresh.font               =   DEFAULT_FONT(15.0);
    lblpullToRefresh.backgroundColor    =   LIGHT_GREEN_COLOR_WITH_ALPHA(0.5);
    lblpullToRefresh.textColor          =   [UIColor whiteColor];
    
    countlbl.font               =   DEFAULT_FONT(15.0);
    countlbl.backgroundColor    =   LIGHT_GREEN_COLOR_WITH_ALPHA(0.5);
    countlbl.textColor          =   [UIColor whiteColor];
    
    searchView.backgroundColor          =       DEFAULT_COLOR;
    searchTextField.font                =       DEFAULT_FONT(20.0);
    searchTextField.backgroundColor     =       [UIColor whiteColor];
    searchTextField.textColor           =       DEFAULT_COLOR;
    searchTextField.layer.cornerRadius  =       8.0;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        searchTextField.tintColor           = DEFAULT_COLOR;
    }
    
    //callModalView = [CommonCallback setViewPropertiesWithRoundedCorner:callModalView];
    callModalView.backgroundColor =  [UIColor whiteColor];
    callModalView.layer.shadowColor      = [UIColor blackColor].CGColor;
    callModalView.layer.shadowOffset     = CGSizeMake(1, 1);
    callModalView.layer.shadowOpacity    = 1.0;
    callModalView.layer.shadowRadius     = 10.0;
    [callModalViewBackView setHidden:YES];
    [callModalView setHidden:YES];
    
    lblcallModalHeader.textAlignment     = NSTextAlignmentCenter;
    lblcallModalHeader.backgroundColor   = [UIColor clearColor];
    lblcallModalHeader.font              = DEFAULT_BOLD_FONT(20);
    lblcallModalHeader.textColor         = BROWN_COLOR;
//    lblcallModalHeader.adjustsFontSizeToFitWidth = YES;
    lblcallModalHeader.minimumScaleFactor = 15;
    lblcallModalHeader.text              = @"Select number";
    lblcallModalHeader.numberOfLines     = 2;
    
    lblcallModalHeaderSeperator.backgroundColor   = DEFAULT_COLOR;
    callModalTableView.backgroundColor = [UIColor whiteColor];
    if ([callModalTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [callModalTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    btnCancelcallModal.backgroundColor = [UIColor clearColor];
}

-(void)addHUDView
{
    hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:hud];
	hud.dimBackground = NO;
    hud.labelText = @"Loading";
	hud.detailsLabelText = @"Please wait ...";
    [hud hide:NO];
}

-(void)removeHUDView
{
    [hud removeFromSuperview];
    hud = nil;
}

-(void)sendAllStoreRequest:(NSString *)urlString
{
    DebugLog(@"url - %@",urlString);
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"--->%@",self.splashJson);
                                                        if(isLoading)
                                                        {
                                                            [self stopLoading];
                                                        }
                                                        if(self.splashJson  != nil) {
                                                            [tempstoreArray removeAllObjects];
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                [INUserDefaultOperations setAllStoreSyncDate];
                                                                NSDictionary *maindict = [self.splashJson  objectForKey:@"data"];
                                                                self.totalPage = [[maindict  objectForKey:@"total_page"] integerValue];
                                                                self.totalRecord = [[maindict  objectForKey:@"total_record"] integerValue];
                                                                if(self.currentPage == 1)
                                                                {
                                                                    [self  deletePlacesTable];
                                                                    [storeArray removeAllObjects];
                                                                }
                                                                [self updateTotalPage:self.totalPage];
                                                                [self updateTotalRecords:self.totalRecord];
                                                                [self updateCurrentPage:self.currentPage];
                                                                NSArray* jsonArray = [maindict  objectForKey:@"record"];
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *objdict = [jsonArray  objectAtIndex:index];
                                                                    DebugLog(@"\n\n%@\n\n",objdict);
                                                                    storeObj = [[INAllStoreObj alloc] init];
                                                                    storeObj.area = [objdict objectForKey:@"area"];
                                                                    storeObj.areaID = [objdict objectForKey:@"area_id"];
                                                                    storeObj.building = [objdict objectForKey:@"building"];
                                                                    storeObj.city = [objdict objectForKey:@"city"];
                                                                    storeObj.description = [objdict objectForKey:@"description"];
                                                                    storeObj.distance = [objdict objectForKey:@"distance"];
                                                                    storeObj.email = [objdict objectForKey:@"email"];
                                                                    storeObj.has_offer = [objdict objectForKey:@"has_offer"];
                                                                    storeObj.ID = [[objdict objectForKey:@"id"] integerValue];
                                                                    storeObj.image_url = [objdict objectForKey:@"image_url"];
                                                                    storeObj.landmark = [objdict objectForKey:@"landmark"];
                                                                    storeObj.mob_no1 = [objdict objectForKey:@"mob_no1"];
                                                                    storeObj.mob_no2 = [objdict objectForKey:@"mob_no2"];
                                                                    storeObj.mob_no3 = [objdict objectForKey:@"mob_no3"];
                                                                    storeObj.tel_no1 = [objdict objectForKey:@"tel_no1"];
                                                                    storeObj.tel_no2 = [objdict objectForKey:@"tel_no2"];
                                                                    storeObj.tel_no3 = [objdict objectForKey:@"tel_no3"];
                                                                    storeObj.name = [objdict objectForKey:@"name"];
                                                                    storeObj.place_parent = [[objdict objectForKey:@"place_parent"] integerValue];
                                                                    storeObj.street = [objdict objectForKey:@"street"];
                                                                    storeObj.title = [objdict objectForKey:@"title"];
                                                                    storeObj.total_like = [objdict objectForKey:@"total_like"];
                                                                    storeObj.website = [objdict objectForKey:@"website"];
                                                                    storeObj.verified = [objdict objectForKey:@"verified"];
                                                                    [tempstoreArray addObject:storeObj];
                                                                    storeObj = nil;
                                                                }
                                                            } else {
                                                                [CommonCallback hideProgressHud];
                                                                if([self.splashJson  objectForKey:@"message"] != [NSNull null] && ![[self.splashJson objectForKey:@"message"] isEqualToString:@""]){
                                                                    [INUserDefaultOperations performSelector:@selector(showAlert:) withObject:[self.splashJson  objectForKey:@"message"] afterDelay:1.0];
                                                                }
                                                            }
                                                            if([tempstoreArray count] > 0)
                                                            {
                                                                DebugLog(@"-----%d--------",[tempstoreArray count]);
                                                                [self insertAllStoreListTable];
                                                                [self readAllStoreFromDatabase];
                                                            }
                                                            [allstoreTableView reloadData];
                                                            [self setCountLabel];
                                                            [CommonCallback hideProgressHud];
                                                        }
                                                        isAdding = NO;
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        [CommonCallback hideProgressHud];
                                                        DebugLog(@"-%@-%@-%@-%d-",response,error,JSON,response.statusCode);
                                                        DebugLog(@"-%d-%d-",[error code],NSURLErrorCancelled);
                                                        if ([error code] == NSURLErrorCancelled) {
                                                            DebugLog(@"yup operation is cancelled %d",[error code]);
                                                        }else{
                                                            UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                         message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                        delegate:nil
                                                                                               cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                            [av show];
                                                            NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:urlString,@"Url",[error localizedDescription],@"errorDescription", nil];
                                                            [INEventLogger logEvent:@"Error" withParams:params];
                                                        }
                                                        isAdding = NO;
                                                        if(isLoading)
                                                        {
                                                            [self stopLoading];
                                                        }
                                                        [self readRecordDatabase];
                                                    }];
    [operation start];
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleShrink];
    [MMProgressHUD showWithTitle:HUD_TITLE
                          status:@""
             confirmationMessage:[NSString stringWithFormat:@"Cancel?"]
                     cancelBlock:^{
                         DebugLog(@"Task was cancelled!");
                         [operation cancel];
                     }];
}

#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:callModalTableView]) {
        return 80;
    }else{
        return 125;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:callModalTableView]) {
        return [telArray count];
    }else{
        return [storeArray count];
    }
}

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
//    
//    if ([tableView isEqual:callModalTableView])
//        return;
//    
////    if (indexPath.row > feedsArray.count || feedsArray.count <= 0) {
////        return;
////    }
////    if (!loadWithAnimation) {
////        return;
////    }
//    dispatch_async(dispatch_get_main_queue(), ^{
//        cell.contentView.alpha = 0.3;
//        [allstoreTableView bringSubviewToFront:cell.contentView];
//        [UIView transitionWithView:cell.contentView
//                          duration:0.7
//                          options:UIViewAnimationOptionTransitionFlipFromBottom
//                          animations:^{
//                              cell.contentView.alpha = 1;
//                          } completion:nil];
//    });
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:callModalTableView]) {
        static NSString *CellIdentifier = @"CallCell";
        UIImageView *thumbImg;
        UILabel *lblNumber;
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.backgroundColor = [UIColor whiteColor];
            
            thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(10,15, 50, 50)];
            thumbImg.tag = 1111;
            thumbImg.contentMode = UIViewContentModeScaleToFill;
            thumbImg.image = [UIImage imageNamed:@"call_brown.png"];
            [cell.contentView addSubview:thumbImg];
            
            lblNumber = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame),0, 210, 80)];
            lblNumber.text = @"";
            lblNumber.tag = 2222;
            lblNumber.textColor             = BROWN_COLOR;
            lblNumber.highlightedTextColor  = [UIColor whiteColor];
            lblNumber.font                  = DEFAULT_FONT(23);
            lblNumber.textAlignment         = NSTextAlignmentLeft;
            lblNumber.backgroundColor       = [UIColor clearColor];
            [cell.contentView addSubview:lblNumber];
            
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = BROWN_OFFWHITE_COLOR;
            cell.selectedBackgroundView = bgColorView;
        }
        lblNumber        = (UILabel *)[cell.contentView viewWithTag:2222];
        DebugLog(@"lblNumber %@",[telArray objectAtIndex:indexPath.row]);
        lblNumber.text   = [telArray objectAtIndex:indexPath.row];
        return cell;
    }else{
        static NSString *CellIdentifier = @"Cell";
        UIView *backView;
        UILabel *lblOffer;
        
        UIButton *btnCall;
        //UIImageView *thumbImg;
        UILabel *lblTitle;
        UILabel *lblDesc;
        
        UIImageView *offerthumbImg;
        
        UIImageView *likethumbImg;
        UILabel *lblLikeCount;
        
        UIImageView *ldistthumbImg;
        UILabel *lblDist;
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.backgroundColor = [UIColor clearColor];
            
            //[cell.imageView setImage:[UIImage imageNamed:@"table_placeholder.png"]];
            //Initialize Image View with tag 1.(Thumbnail Image)
            backView = [[UIView alloc] initWithFrame:CGRectMake(0, 5, 320, 120)];
            backView.tag = 333;
            backView.backgroundColor =  [UIColor whiteColor];
            //backView = [CommonCallback setViewPropertiesWithRoundedCorner:backView];
            backView.clipsToBounds = YES;
            
            lblOffer = [[UILabel alloc] initWithFrame:CGRectMake(0.0,0, 320.0, 30.0)];
            lblOffer.text = @"";
            lblOffer.tag = 111;
            lblOffer.font = DEFAULT_BOLD_FONT(15);
            lblOffer.textAlignment = NSTextAlignmentCenter;
            lblOffer.textColor = [UIColor whiteColor];
            lblOffer.backgroundColor =  LIGHT_GREEN_COLOR;
            lblOffer.hidden = TRUE;
            [backView addSubview:lblOffer];
            
            btnCall = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnCall setFrame:CGRectMake(0, 20, 80, 80)];
            [btnCall setImage:[UIImage imageNamed:@"Call_white.png"] forState:UIControlStateNormal];
            [btnCall setBackgroundColor:[UIColor clearColor]];
            [btnCall setTitle:@"" forState:UIControlStateNormal];
            [btnCall addTarget:self action:@selector(callBtnTapped:event:) forControlEvents:UIControlEventTouchUpInside];
            [backView addSubview:btnCall];
            
//            thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, 120, 120)];
//            thumbImg.tag = 100;
//            thumbImg.contentMode = UIViewContentModeScaleToFill;
//            thumbImg.image = [UIImage imageNamed:@"call_brown.png"];
//            [backView addSubview:thumbImg];
            
            offerthumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,71.0,50.0)];
            offerthumbImg.tag = 107;
            offerthumbImg.image = [UIImage imageNamed:@"Offer_Icon1.png"];
            offerthumbImg.contentMode = UIViewContentModeScaleAspectFill;
            [backView addSubview:offerthumbImg];
            
            lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(btnCall.frame)+5,4,230.0,40.0)];
            lblTitle.text = @"";
            lblTitle.tag = 101;
            lblTitle.font = DEFAULT_BOLD_FONT(15);
            //lblTitle.minimumScaleFactor = 0.5f;
            //lblTitle.adjustsFontSizeToFitWidth = YES;
            lblTitle.textAlignment = NSTextAlignmentLeft;
            lblTitle.textColor = BROWN_COLOR;
            lblTitle.highlightedTextColor = [UIColor whiteColor];
            lblTitle.backgroundColor =  [UIColor clearColor];
            lblTitle.numberOfLines = 2;
            [backView addSubview:lblTitle];
            
            lblDesc = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(btnCall.frame)+5, CGRectGetMaxY(lblTitle.frame) - 5,230.0,60.0)];
            lblDesc.text = @"";
            lblDesc.tag = 102;
            lblDesc.numberOfLines = 3;
            lblDesc.font = DEFAULT_FONT(12);
            lblDesc.textAlignment = NSTextAlignmentLeft;
            lblDesc.textColor = BROWN_COLOR;
            lblDesc.highlightedTextColor = [UIColor whiteColor];
            lblDesc.backgroundColor =  [UIColor clearColor];
            [backView addSubview:lblDesc];
            
            likethumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth(backView.frame) - 110, CGRectGetMaxY(lblDesc.frame)+2,15.0,15.0)];
            likethumbImg.tag = 103;
            likethumbImg.image = [UIImage imageNamed:@"list_fav_icon.png"];
            likethumbImg.contentMode = UIViewContentModeScaleAspectFill;
            [backView addSubview:likethumbImg];
            
            lblLikeCount = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(likethumbImg.frame),CGRectGetMaxY(lblDesc.frame)+2,30.0,15.0)];
            lblLikeCount.text = @"";
            lblLikeCount.tag = 104;
            lblLikeCount.font = DEFAULT_BOLD_FONT(10);
            lblLikeCount.textAlignment = NSTextAlignmentCenter;
            lblLikeCount.textColor = BROWN_COLOR;
            lblLikeCount.highlightedTextColor = [UIColor whiteColor];
            lblLikeCount.backgroundColor =  [UIColor clearColor];
            [backView addSubview:lblLikeCount];
            
            ldistthumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth(backView.frame)-67,CGRectGetMaxY(lblDesc.frame)+2,15.0,15.0)];
            ldistthumbImg.image = [UIImage imageNamed:@"list_distance_icon.png"];
            ldistthumbImg.tag = 105;
            ldistthumbImg.contentMode = UIViewContentModeScaleAspectFill;
            [backView addSubview:ldistthumbImg];
            
            lblDist = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(ldistthumbImg.frame),CGRectGetMaxY(lblDesc.frame)+2,50.0,15.0)];
            lblDist.text = @"";
            lblDist.tag = 106;
            lblDist.font = DEFAULT_BOLD_FONT(10);
            lblDist.textAlignment = NSTextAlignmentCenter;
            lblDist.textColor = BROWN_COLOR;
            lblDist.highlightedTextColor = [UIColor whiteColor];
            lblDist.backgroundColor =  [UIColor clearColor];
            [backView addSubview:lblDist];
            
            [cell.contentView addSubview:backView];
            
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
            bgColorView.tag = 104;
            cell.selectedBackgroundView = bgColorView;

        }
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        if ([indexPath row] < [storeArray count]) {
            INAllStoreObj *placeObj  = [storeArray objectAtIndex:indexPath.row];
            //cell.textLabel.text = placeObj.name;
            //cell.detailTextLabel.text = placeObj.description;
            //cell.detailTextLabel.numberOfLines = 3;
            backView        = (UIView *)[cell viewWithTag:333];
            lblOffer        = (UILabel *)[backView viewWithTag:111];
            //thumbImg        = (UIImageView *)[backView viewWithTag:100];
            lblTitle        = (UILabel *)[backView viewWithTag:101];
            lblDesc         = (UILabel *)[backView viewWithTag:102];
            likethumbImg    = (UIImageView *)[backView viewWithTag:103];
            lblLikeCount    = (UILabel *)[backView viewWithTag:104];
            ldistthumbImg   = (UIImageView *)[backView viewWithTag:105];
            lblDist         = (UILabel *)[backView viewWithTag:106];
            offerthumbImg   = (UIImageView *)[backView viewWithTag:107];
        
            lblOffer.hidden = YES;
            if (placeObj.has_offer != nil && [placeObj.has_offer isEqualToString:@"1"]) {
                //backView.layer.borderColor = ORANGE_COLOR.CGColor;
                //backView.layer.borderWidth = 5.0;
                offerthumbImg.hidden = FALSE;
            }else{
                //backView.layer.borderWidth = 0.0;
                offerthumbImg.hidden = YES;
            }
            
            /*
            DebugLog(@"link-%@ %@",placeObj.image_url, placeObj.name);
            
            if(placeObj.image_url != nil && ![placeObj.image_url isEqualToString:@""])
            {
                __weak UIImageView *thumbImg_ = thumbImg;
                //            [thumbImg setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(placeObj.image_url)]
                //                     placeholderImage:[UIImage imageNamed:@"list_thumbnail.png"]
                //                              success:^(UIImage *image) {
                //                                  DebugLog(@"success");
                //                              }
                //                              failure:^(NSError *error) {
                //                                  DebugLog(@"write error %@", error);
                //                                  thumbImg_.image = [UIImage imageNamed:@"list_thumbnail.png"];
                //                              }];
                [thumbImg setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(placeObj.image_url)] placeholderImage:[UIImage imageNamed:@"list_thumbnail.png"] completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType) {
                    if (error) {
                        thumbImg_.image = [UIImage imageNamed:@"list_thumbnail.png"];
                    }
                }];
            } else {
                thumbImg.image = [UIImage imageNamed:@"list_thumbnail.png"];
            }
            */
            [lblTitle setText:placeObj.name];
            [lblDesc setText: placeObj.description];
            
            [lblDist setHidden:TRUE];
            [ldistthumbImg setHidden:TRUE];
            
            if(placeObj.distance != nil) {
                DebugLog(@"%@",placeObj.distance);
                if ([placeObj.distance floatValue] < 1000) {
                    
                    likethumbImg.frame = CGRectMake(CGRectGetWidth(backView.frame) - 110, CGRectGetMaxY(lblDesc.frame)+2,15.0,15.0);
                    lblLikeCount.frame = CGRectMake(CGRectGetMaxX(likethumbImg.frame),CGRectGetMaxY(lblDesc.frame)+2,30.0,15.0);
                    
                    [lblDist setHidden:FALSE];
                    [ldistthumbImg setHidden:FALSE];
                    
                    if([placeObj.distance floatValue]  > -1 && [placeObj.distance floatValue]  < 1)
                        [lblDist setText: [NSString stringWithFormat:@"%.2fm",([placeObj.distance floatValue] * 100)]];
                    else
                        [lblDist setText: [NSString stringWithFormat:@"%.2fkm",([placeObj.distance floatValue])]];
                } else {
                    likethumbImg.frame = CGRectMake(CGRectGetWidth(backView.frame) - 67, CGRectGetMaxY(lblDesc.frame)+2,15.0,15.0);
                    lblLikeCount.frame = CGRectMake(CGRectGetMaxX(ldistthumbImg.frame),CGRectGetMaxY(lblDesc.frame)+2,30.0,15.0);
                }
            }
            
            //DebugLog(@"like-->%@",placeObj.total_like);
            //        if(placeObj.total_like != nil && ![placeObj.total_like isEqualToString:@"0"]) {
            [lblLikeCount setHidden:FALSE];
            [likethumbImg setHidden:FALSE];
            lblLikeCount.text = placeObj.total_like;
            //        } else {
            //            [lblLikeCount setHidden:TRUE];
            //            [likethumbImg setHidden:TRUE];
            //        }
        }
        return cell;
    }
}


#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:callModalTableView]) {
        [callModalView setHidden:YES];
        [callModalViewBackView setHidden:YES];
        NSString *selectedTelNumberToCall = [[telArray objectAtIndex:indexPath.row] stringByReplacingOccurrencesOfString:@" " withString:@""];
        DebugLog(@"selectedTelNumberToCall %@",selectedTelNumberToCall);
        NSString *deviceType = [UIDevice currentDevice].model;
        if([deviceType isEqualToString:@"iPhone"])
        {
            UIApplication *myApp = [UIApplication sharedApplication];
            DebugLog(@"selectedTelNumberToCall = -%@-",selectedTelNumberToCall);
            [myApp openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",selectedTelNumberToCall]]];
            
            NSDictionary *callParams = [NSDictionary dictionaryWithObjectsAndKeys:@"MarketPlace",@"Source",[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"AreaName",selectedPlaceName,@"StoreName",selectedTelNumberToCall,@"Number", nil];
            [INEventLogger logEvent:@"StoreCallDone" withParams:callParams];
            
            [self sendStoreCallRequest:@"StoreCallDone"];
        }
        else
        {
            UIAlertView *alert = [[ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                              message: @"Calling functionality is not available in this device. "
                                                             delegate: nil
                                                    cancelButtonTitle: nil
                                                    otherButtonTitles: @"OK", nil];
            [alert show];
        }
    }else{
        INAllStoreObj *placeObj  = [storeArray objectAtIndex:indexPath.row];
        INStoreDetailViewController *detailController = [[INStoreDetailViewController alloc] initWithNibName:@"INStoreDetailViewController" bundle:nil] ;
        detailController.title      = placeObj.name;
        detailController.storeId    = placeObj.ID;
        DebugLog(@"placeObj.image_url %@",placeObj.image_url);
        if ([placeObj.image_url hasPrefix:@"http"]) {
            detailController.storeImageUrl = [NSURL URLWithString:placeObj.image_url];
        }else{
            detailController.storeImageUrl = [NSURL URLWithString:THUMBNAIL_LINK(placeObj.image_url)];
        }
        [self.navigationController pushViewController:detailController animated:YES];
        [INEventLogger logEvent:@"MarketPlace_Details"];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(NSIndexPath *)getIndexpathOfEvent:(id)event
{
    NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:allstoreTableView];
	NSIndexPath *indexPath = [allstoreTableView indexPathForRowAtPoint: currentTouchPosition];
    return indexPath;
}

- (void)callBtnTapped:(id)sender event:(id)event
{
    NSIndexPath *indexPath = [self getIndexpathOfEvent:event];
        if (indexPath != nil)
        {
            INAllStoreObj *placeObj  = [storeArray objectAtIndex:indexPath.row];
            DebugLog(@"place %@",placeObj.name);
            if (placeObj.name == nil || [placeObj.name length] == 0){
                lblcallModalHeader.text =  @"Select number";
                selectedPlaceName = @"";
            }
            else{
                lblcallModalHeader.text = placeObj.name;
                selectedPlaceName = placeObj.name;
            }
            
            selectedPlaceId = placeObj.ID;
            if (telArray == nil) {
                telArray = [[NSMutableArray alloc] init];
            }else{
                [telArray removeAllObjects];
            }
            if (placeObj.tel_no1 != nil && ![placeObj.tel_no1 isEqualToString:@""]) {
                [telArray addObject:placeObj.tel_no1];
            }
            if (placeObj.tel_no2 != nil && ![placeObj.tel_no2 isEqualToString:@""]) {
                [telArray addObject:placeObj.tel_no2];
            }
            if (placeObj.tel_no3 != nil && ![placeObj.tel_no3 isEqualToString:@""]) {
                [telArray addObject:placeObj.tel_no3];
            }
            if (placeObj.mob_no1 != nil && ![placeObj.mob_no1 isEqualToString:@""]) {
                [telArray addObject:[NSString stringWithFormat:@"+%@",placeObj.mob_no1]];
            }
            if (placeObj.mob_no2 != nil && ![placeObj.mob_no2 isEqualToString:@""]) {
                [telArray addObject:[NSString stringWithFormat:@"+%@",placeObj.mob_no2]];
            }
            if (placeObj.mob_no3 != nil && ![placeObj.mob_no3 isEqualToString:@""]) {
                [telArray addObject:[NSString stringWithFormat:@"+%@",placeObj.mob_no3]];
            }
            [callModalTableView reloadData];
            if (callModalTableView.contentSize.height < 240) {
                [callModalTableView setFrame:CGRectMake(callModalTableView.frame.origin.x, callModalTableView.frame.origin.y, callModalTableView.frame.size.width, callModalTableView.contentSize.height)];
            }else{
                [callModalTableView setFrame:CGRectMake(callModalTableView.frame.origin.x, callModalTableView.frame.origin.y, callModalTableView.frame.size.width, 240)];
            }
            [callModalView setFrame:CGRectMake(callModalView.frame.origin.x, callModalView.frame.origin.y, callModalView.frame.size.width, callModalTableView.frame.size.height+85)];
            
            DebugLog(@"temparray %@",telArray);
            if (telArray != nil && telArray.count > 0) {
                [callModalViewBackView setHidden:NO];
                [callModalView setHidden:NO];
                [CommonCallback viewtransitionInCompletion:callModalView completion:^{
                    [CommonCallback viewtransitionOutCompletion:callModalView completion:nil];
                }];
                
                NSDictionary *callParams = [NSDictionary dictionaryWithObjectsAndKeys:@"MarketPlace",@"Source",[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"AreaName",selectedPlaceName,@"StoreName", nil];
                [INEventLogger logEvent:@"StoreCall" withParams:callParams];
                
                [self sendStoreCallRequest:@"StoreCall"];
            } else {
                [INUserDefaultOperations showSIAlertView:NO_CONTACT_MESSAGE];
            }
        }
}

- (IBAction)btnCancelcallModalPressed:(id)sender {
    [callModalView setHidden:YES];
    [callModalViewBackView setHidden:YES];
}

- (UIImage *)scale:(UIImage *)image toSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}

-(void)pageRequest:(NSString *)pageNum
{
    if([IN_APP_DELEGATE networkavailable])
    {
        isAdding = YES;
        //[self sendAllStoreRequest:ALL_STORE_LINK([INUserDefaultOperations storeIdForKey:[INUserDefaultOperations getSelectedStore]],pageNum)];
        [self sendAllStoreRequest:ALL_STORE_LINK([INUserDefaultOperations getLatitude],[INUserDefaultOperations getLongitude],[INUserDefaultOperations getCustomerDistance],[IN_APP_DELEGATE getActiveAreaIdFromAreaTable],pageNum)];
    }  else   {
        isAdding = NO;
        if(isLoading)
        {
            [self stopLoading];
        }
        [INUserDefaultOperations showOfflineAlert];
    }
}

-(void)loadData
{
    [self readAllStoreFromDatabase];
    if([storeArray count] > 0)
    {
        [allstoreTableView reloadData];
        [self readRecordDatabase];
        [self setCountLabel];
        
        DebugLog(@"diff - >%d",[INUserDefaultOperations getDateDifferenceInHours:[INUserDefaultOperations getAllStoreSyncDate]]);
        
        if(([INUserDefaultOperations getDateDifferenceInHours:[INUserDefaultOperations getAllStoreSyncDate]]) > ALLSTORE_SYNC_TIME_INTERVAL)
        {
            if([IN_APP_DELEGATE networkavailable])
            {
                [storeArray removeAllObjects];
                self.currentPage = 1;
                [allstoreTableView reloadData];
                [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
            }
        }
    }else{
        //http request
        DebugLog(@"file not exists");
        self.currentPage = 1;
        [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
    }
}

- (void)reloadData:(NSNotification *)notification
{
    //DebugLog(@"Reload data");
    [self loadData];
}


-(void)setCountLabel
{
    //DebugLog(@"-----%d--------%d------",[storeArray count],self.totalRecord);
    self.countlbl.text = [NSString stringWithFormat:@"%d/%d",[storeArray count],self.totalRecord];
    //DebugLog(@"-----%@--------",countlbl.text);
}

#pragma mark PULL TO REFRESH
- (void)setupStrings{
    textPull = @"Pull down to refresh...";
    textRelease = @"Release to refresh...";
    textLoading = @"Loading please wait...";
}

- (void)addPullToRefreshHeader {
    refreshHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0 - REFRESH_HEADER_HEIGHT, 320, REFRESH_HEADER_HEIGHT)];
    refreshHeaderView.backgroundColor = [UIColor clearColor];
    
    refreshLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, REFRESH_HEADER_HEIGHT)];
    refreshLabel.backgroundColor = [UIColor clearColor];
    refreshLabel.font = DEFAULT_BOLD_FONT(15);
    refreshLabel.textAlignment = NSTextAlignmentCenter;
    refreshLabel.textColor = DEFAULT_COLOR;
    refreshArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:PULLTOREFRESH_BARROW]];
    refreshArrow.frame = CGRectMake(floorf((REFRESH_HEADER_HEIGHT - 2) / 2),
                                    (floorf(REFRESH_HEADER_HEIGHT - 44) / 2),
                                    27, 44);
    
    refreshSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    refreshSpinner.frame = CGRectMake(floorf(floorf(REFRESH_HEADER_HEIGHT - 2) / 2), floorf((REFRESH_HEADER_HEIGHT - 20) / 2), 20, 20);
    refreshSpinner.hidesWhenStopped = YES;
    
    [refreshHeaderView addSubview:refreshLabel];
    [refreshHeaderView addSubview:refreshArrow];
    [refreshHeaderView addSubview:refreshSpinner];
    [allstoreTableView addSubview:refreshHeaderView];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if ([scrollView isEqual:callModalTableView]) {
        return;
    }
    if (isLoading) return;
    isDragging = YES;
    
    lastContentOffset = scrollView.contentOffset.y;
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([scrollView isEqual:callModalTableView]) {
        return;
    }
    if (isLoading) {
        // Update the content inset, good for section headers
        if (scrollView.contentOffset.y > 0)
            allstoreTableView.contentInset = UIEdgeInsetsZero;
        else if (scrollView.contentOffset.y >= -REFRESH_HEADER_HEIGHT)
            allstoreTableView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (isDragging && scrollView.contentOffset.y < 0) {
        // Update the arrow direction and label
        [UIView animateWithDuration:0.25 animations:^{
            if (scrollView.contentOffset.y < -REFRESH_HEADER_HEIGHT) {
                // User is scrolling above the header
                refreshLabel.text = self.textRelease;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
            } else {
                // User is scrolling somewhere within the header
                refreshLabel.text = self.textPull;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
            }
        }];
    } else {
        CGFloat height = scrollView.frame.size.height;
        
        CGFloat contentYoffset = scrollView.contentOffset.y;
        
        CGFloat distanceFromBottom = scrollView.contentSize.height - contentYoffset;
        
        if(distanceFromBottom <= height)
        {
            if(isAdding==TRUE) {
                DebugLog(@"<<<<<<<<<<<<<<<");
                return;
            } else {
                DebugLog(@"end of the table page = %d", self.currentPage);
                if(self.currentPage < self.totalPage)
                {
                    self.currentPage = self.currentPage + 1;
                    [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
                }
            }
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if ([scrollView isEqual:callModalTableView]) {
        return;
    }
    if (isLoading) return;
    isDragging = NO;
    if (scrollView.contentOffset.y <= -REFRESH_HEADER_HEIGHT) {
        // Released above the header
        [self startLoading];
    }
    if (lastContentOffset < (int)scrollView.contentOffset.y) {
        DebugLog(@"lastContentOffset <");
        if (searchView.frame.size.height >= 72) {
            [UIView animateWithDuration:0.2
                             animations:^(void){
                                 searchView.frame = CGRectMake(searchView.frame.origin.x, searchView.frame.origin.y, searchView.frame.size.width, 36);
                                 allstoreTableView.frame = CGRectMake(allstoreTableView.frame.origin.x, CGRectGetMaxY(searchView.frame), allstoreTableView.frame.size.width, self.view.frame.size.height-(CGRectGetMaxY(searchView.frame)));
                             } completion:nil];
        }
    }
    else if (lastContentOffset > (int)scrollView.contentOffset.y) {
        DebugLog(@"lastContentOffset >");
        if (searchView.frame.size.height < 72) {
            [UIView animateWithDuration:0.2
                             animations:^(void){
                                 searchView.frame = CGRectMake(searchView.frame.origin.x, searchView.frame.origin.y, searchView.frame.size.width, 72);
                                 allstoreTableView.frame = CGRectMake(allstoreTableView.frame.origin.x, CGRectGetMaxY(searchView.frame), allstoreTableView.frame.size.width, self.view.frame.size.height-(CGRectGetMaxY(searchView.frame)));
                             } completion:nil];
        }
    }
}

- (void)startLoading {
    isLoading = YES;
    // Show the header
    [UIView animateWithDuration:0.3 animations:^{
        allstoreTableView.contentInset = UIEdgeInsetsMake(REFRESH_HEADER_HEIGHT, 0, 0, 0);
        refreshLabel.text = self.textLoading;
        refreshArrow.hidden = YES;
        [refreshSpinner startAnimating];
    }];
    if([IN_APP_DELEGATE networkavailable])
    {
        [storeArray removeAllObjects];
        self.currentPage = 1;
        [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
    }
    else
    {
        [self stopLoading];
    }
    // Refresh action!
    //[self refresh];
}

- (void)stopLoading {
    isLoading = NO;
    // Hide the header
    [UIView animateWithDuration:0.3 animations:^{
        allstoreTableView.contentInset = UIEdgeInsetsZero;
        [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
    }
                     completion:^(BOOL finished) {
                         [self performSelector:@selector(stopLoadingComplete)];
                     }];
}

- (void)stopLoadingComplete {
    // Reset the header
    refreshLabel.text = self.textPull;
    refreshArrow.hidden = NO;
    [refreshSpinner stopAnimating];
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if ([textField isEqual:searchTextField]) {
        INSearchViewController *searchController = [[INSearchViewController alloc] initWithNibName:@"INSearchViewController" bundle:nil] ;
        searchController.title = @"Search";
        searchController.hideBackBarBtn = YES;
        searchController.isStoreSearch = YES;
        [UIView  transitionWithView:self.navigationController.view duration:0.5  options:UIViewAnimationOptionTransitionCrossDissolve
                  animations:^(void) {
                      BOOL oldState = [UIView areAnimationsEnabled];
                      [UIView setAnimationsEnabled:NO];
                      [self.navigationController pushViewController:searchController animated:NO];
                      [UIView setAnimationsEnabled:oldState];
                  }
                  completion:^(BOOL finished){
                      [textField resignFirstResponder];
                  }];
    }
    return YES;
}

#pragma Table Operations
- (void)insertAllStoreListTable
{
    sqlite3_stmt    *statement;
    int lastrowid;
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        for (int index=0; index<[tempstoreArray count] ; index++)
        {
            INAllStoreObj *tempstoreObj = [tempstoreArray objectAtIndex:index];
            NSString *insertSQL = [NSString stringWithFormat:
                                   @"INSERT or REPLACE INTO PLACES (PLACEID, PLACE_PARENT, NAME, DESCRIPTION, DISTANCE, BUILDING, STREET, LANDMARK, AREA, CITY, MOB_NO1, MOB_NO2, MOB_NO3, TEL_NO1, TEL_NO2, TEL_NO3, IMG_URL, EMAIL, WEBSITE, HAS_OFFER, TITLE, TOTAL_LIKE, AREAID, VERIFIED) VALUES (\"%d\",\"%d\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",tempstoreObj.ID,tempstoreObj.place_parent,tempstoreObj.name,[tempstoreObj.description stringByReplacingOccurrencesOfString:@"\"" withString:@"'"],tempstoreObj.distance,tempstoreObj.building,tempstoreObj.street,tempstoreObj.landmark,tempstoreObj.area,tempstoreObj.city,tempstoreObj.mob_no1, tempstoreObj.mob_no2, tempstoreObj.mob_no3, tempstoreObj.tel_no1, tempstoreObj.tel_no2, tempstoreObj.tel_no3, tempstoreObj.image_url, tempstoreObj.email,tempstoreObj.website,tempstoreObj.has_offer,tempstoreObj.title,tempstoreObj.total_like, tempstoreObj.areaID, tempstoreObj.verified];
            DebugLog(@"%@",insertSQL);
            const char *insert_stmt = [insertSQL UTF8String];
            sqlite3_prepare_v2(shoplocalDB, insert_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                lastrowid = sqlite3_last_insert_rowid(shoplocalDB);
                // DebugLog(@"inserted id========%d", lastrowid);
            } else {
                DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
            }
            sqlite3_finalize(statement);
        }
    }
    sqlite3_close(shoplocalDB);
}

-(void) readAllStoreFromDatabase {
    
    NSString *activeId = [IN_APP_DELEGATE getActiveAreaIdFromAreaTable];
    
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    // Init the expense Array
    if(storeArray == nil) {
        DebugLog(@"new  expense array created");
        storeArray = [[NSMutableArray alloc] init];
    } else {
        DebugLog(@"old  expense array used");
        [storeArray removeAllObjects];
    }
	// Open the database from the users filessytem
	if(sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"select * from PLACES where AREAID like \"%@\" COLLATE NOCASE",activeId];
        DebugLog(@"query--->%@",querySQL);
        const char *sqlStatement = [querySQL UTF8String];
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(shoplocalDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				// Read the data from the result row
                //int count = sqlite3_data_count(compiledStatement);
                //DebugLog(@"count=====>>%d",count);
				NSInteger lid = sqlite3_column_int(compiledStatement, 1);
				NSInteger lplace_parent = sqlite3_column_int(compiledStatement, 2);
				NSString *lname = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                NSString *ldescription = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
                NSString *ldistance = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];
                NSString *lbuilding = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 6)];
                NSString *lstreet = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 7)];
                NSString *llandmark = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 8)];
				NSString *larea = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 9)];
				NSString *lcity = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 10)];
                NSString *lmob_no1 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 11)];
                NSString *lmob_no2 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 12)];
                NSString *lmob_no3 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 13)];
                NSString *ltel_no1 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 14)];
                NSString *ltel_no2 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 15)];
                NSString *ltel_no3 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 16)];
                NSString *limage_url = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 17)];
                NSString *lemail = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 18)];
                NSString *lwebsite = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 19)];
				NSString *lhas_offer = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 20)];
				NSString *ltitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 21)];
                NSString *ltotal_like = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 22)];
                NSString *lareaid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 23)];
                NSString *lverified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 24)];
                
				INAllStoreObj *tempstoreObj = [[INAllStoreObj alloc] init];
                tempstoreObj.ID = lid;
                tempstoreObj.place_parent = lplace_parent;
                tempstoreObj.name = lname;
                tempstoreObj.description = ldescription;
                tempstoreObj.distance = ldistance;
                tempstoreObj.building = lbuilding;
                tempstoreObj.street = lstreet;
                tempstoreObj.landmark = llandmark;
                tempstoreObj.area = larea;
                tempstoreObj.city = lcity;
                tempstoreObj.mob_no1 = lmob_no1;
                tempstoreObj.mob_no2 = lmob_no2;
                tempstoreObj.mob_no3 = lmob_no3;
                tempstoreObj.tel_no1 = ltel_no1;
                tempstoreObj.tel_no2 = ltel_no2;
                tempstoreObj.tel_no3 = ltel_no3;
                tempstoreObj.image_url = limage_url;
                tempstoreObj.email = lemail;
                tempstoreObj.website = lwebsite;
                tempstoreObj.has_offer = lhas_offer;
                tempstoreObj.title = ltitle;
                tempstoreObj.total_like = ltotal_like;
                tempstoreObj.areaID = lareaid;
                tempstoreObj.verified = lverified;
				[storeArray addObject:tempstoreObj];
				tempstoreObj = nil;
            }
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
		
	}
	sqlite3_close(shoplocalDB);
}

-(void)deletePlacesTable
{
    NSString *activeId = [IN_APP_DELEGATE getActiveAreaIdFromAreaTable];
    
    sqlite3_stmt    *statement;
    
    DebugLog(@"path---->%@",[IN_APP_DELEGATE databasePath]);
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM PLACES WHERE AREAID like \"%@\" COLLATE NOCASE",activeId];
        DebugLog(@"delete---->%@",deleteSQL);
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(shoplocalDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"All rows deleted");
        } else {
            DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
}

- (void)updateCurrentPage:(NSInteger)page
{
    NSString *activeId = [IN_APP_DELEGATE getActiveAreaIdFromAreaTable];
    
    sqlite3_stmt    *statement;
    int lastrowid;
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        NSString *updateSQL = [NSString stringWithFormat:@"UPDATE PLACE_RECORD set CURRENT_PAGE=\"%d\" WHERE STORE like \"%@\" COLLATE NOCASE",page, activeId];
        DebugLog(@"%@",updateSQL);
        
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(shoplocalDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            lastrowid = sqlite3_last_insert_rowid(shoplocalDB);
            // DebugLog(@"inserted id========%d", lastrowid);
        } else {
            DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
}

- (void)updateTotalPage:(NSInteger)pageCount
{
    NSString *activeId = [IN_APP_DELEGATE getActiveAreaIdFromAreaTable];
    
    sqlite3_stmt    *statement;
    int lastrowid;
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        NSString *updateSQL = [NSString stringWithFormat:@"UPDATE PLACE_RECORD set TOTAL_PAGE=\"%d\" WHERE STORE like \"%@\" COLLATE NOCASE",pageCount, activeId];
        DebugLog(@"%@",updateSQL);
        
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(shoplocalDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            lastrowid = sqlite3_last_insert_rowid(shoplocalDB);
            // DebugLog(@"inserted id========%d", lastrowid);
        } else {
            DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
}

- (void)updateTotalRecords:(NSInteger)recordsCount
{
    NSString *activeId = [IN_APP_DELEGATE getActiveAreaIdFromAreaTable];
    
    sqlite3_stmt    *statement;
    int lastrowid;
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        NSString *updateSQL = [NSString stringWithFormat:@"UPDATE PLACE_RECORD set TOTAL_RECORD=\"%d\" WHERE STORE like \"%@\" COLLATE NOCASE",recordsCount,activeId];
        DebugLog(@"%@",updateSQL);
        
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(shoplocalDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            lastrowid = sqlite3_last_insert_rowid(shoplocalDB);
            // DebugLog(@"inserted id========%d", lastrowid);
        } else {
            DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
}

-(void)readRecordDatabase {
    
    NSString *activeId = [IN_APP_DELEGATE getActiveAreaIdFromAreaTable];
    
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
	// Open the database from the users filessytem
	if(sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"select TOTAL_PAGE,TOTAL_RECORD,CURRENT_PAGE from PLACE_RECORD WHERE STORE like \"%@\" COLLATE NOCASE",activeId];
        const char *sqlStatement = [querySQL UTF8String];
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(shoplocalDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				// Read the data from the result row
				self.totalPage = sqlite3_column_int(compiledStatement, 0);
				self.totalRecord = sqlite3_column_int(compiledStatement, 1);
                self.currentPage = sqlite3_column_int(compiledStatement, 2);
            }
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
		
	}
	sqlite3_close(shoplocalDB);
    DebugLog(@"total page %d- total record %d- current page %d",self.totalPage,self.totalRecord,self.currentPage);
}

-(void)sendStoreCallRequest:(NSString *)callType
{
    
    if (![INUserDefaultOperations isCustomerLoggedIn]) {
        return;
    }
    
    NSString *areaID = [IN_APP_DELEGATE getActiveShoplocalPlaceIdFromAreaTable];
    
    DebugLog(@"========================sendStoreCallRequest========================");
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSString *register_from = [NSString stringWithFormat:@"%@ v%@",DEVICE_TYPE,version];
    
    NSMutableDictionary *postparams = [[NSMutableDictionary alloc] init];
    //    {"user_id":"${user_id}","auth_id":"${auth_id}","place_id":"106","via":"welcome ","call_type":"type-a","active_area":"1"}
    [postparams setObject:[INUserDefaultOperations getCustomerAuthId] forKey:@"user_id"];
    [postparams setObject:[INUserDefaultOperations getCustomerAuthCode] forKey:@"auth_id"];
    [postparams setObject:[NSString stringWithFormat:@"%d",selectedPlaceId] forKey:@"place_id"];
    [postparams setObject:callType forKey:@"call_type"];
    [postparams setObject:areaID forKey:@"active_area"];
    [postparams setObject:register_from forKey:@"via"];
    
    DebugLog(@"sendStoreCallRequest : postParams -%@-",postparams);
    
    [httpClient postPath:CALL_STORE parameters:postparams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"sendStoreCallRequest : json afnetworking ->%@",json);
        if (json!= nil) {
            NSString* code = [json objectForKey:@"code"];
            if([code isEqualToString:CUSTOMER_INVALID_USER_AUTH_CODE])
            {
                if([INUserDefaultOperations isCustomerLoggedIn]){
                    [INUserDefaultOperations clearCustomerDetails];
                }
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"sendStoreCallRequest : [HTTPClient Error]: %@", error.localizedDescription);
    }];
}
//Scroll animation
//
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    CGRect frame = self.navigationController.navigationBar.frame;
//    CGFloat size = frame.size.height - 21;
//    CGFloat framePercentageHidden = ((20 - frame.origin.y) / (frame.size.height - 1));
//    CGFloat scrollOffset = scrollView.contentOffset.y;
//    CGFloat scrollDiff = scrollOffset - previousScrollViewYOffset;
//    CGFloat scrollHeight = scrollView.frame.size.height;
//    CGFloat scrollContentSizeHeight = scrollView.contentSize.height + scrollView.contentInset.bottom;
//    
//    if (scrollOffset <= -scrollView.contentInset.top) {
//        frame.origin.y = 20;
//    } else if ((scrollOffset + scrollHeight) >= scrollContentSizeHeight) {
//        frame.origin.y = -size;
//    } else {
//        frame.origin.y = MIN(20,
//                             MAX(-size, frame.origin.y -
//                                 (frame.size.height * (scrollDiff / scrollHeight))));
//    }
//    
//    [self.navigationController.navigationBar setFrame:frame];
//    [self updateBarButtonItems:(1 - framePercentageHidden)];
//    previousScrollViewYOffset = scrollOffset;
//}
//
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    [self stoppedScrolling];
//}
//
//- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView
//                  willDecelerate:(BOOL)decelerate
//{
//    if (!decelerate) {
//        [self stoppedScrolling];
//    }
//}
//
//- (void)stoppedScrolling
//{
//    CGRect frame = self.navigationController.navigationBar.frame;
//    if (frame.origin.y < 20) {
//        [self animateNavBarTo:-(frame.size.height - 21)];
//    }
//}
//
//- (void)updateBarButtonItems:(CGFloat)alpha
//{
//    [self.navigationItem.leftBarButtonItems enumerateObjectsUsingBlock:^(UIBarButtonItem* item, NSUInteger i, BOOL *stop) {
//        item.customView.alpha = alpha;
//    }];
//    [self.navigationItem.rightBarButtonItems enumerateObjectsUsingBlock:^(UIBarButtonItem* item, NSUInteger i, BOOL *stop) {
//        item.customView.alpha = alpha;
//    }];
//    self.navigationItem.titleView.alpha = alpha;
//    self.navigationController.navigationBar.tintColor = [self.navigationController.navigationBar.tintColor colorWithAlphaComponent:alpha];
//}
//
//- (void)animateNavBarTo:(CGFloat)y
//{
//    [UIView animateWithDuration:0.2 animations:^{
//        CGRect frame = self.navigationController.navigationBar.frame;
//        CGFloat alpha = (frame.origin.y >= y ? 0 : 1);
//        frame.origin.y = y;
//        [self.navigationController.navigationBar setFrame:frame];
//        [self updateBarButtonItems:alpha];
//    }];
//}



@end
