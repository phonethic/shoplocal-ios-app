//
//  INFoodViewController.m
//  shoplocal
//
//  Created by Rishi on 08/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import <time.h>
#import <xlocale.h>

#import "INFoodViewController.h"
#import "INAppDelegate.h"
#import "INAllStoreObj.h"
#import "INDetailViewController.h"
#import "CommonCallback.h"
#import "INBroadcastDetailObj.h"
#import "INCustBroadCastDetailsViewController.h"
#import <Twitter/Twitter.h>
#import "INStoreDetailViewController.h"

#import "CellWithImageView.h"
#import "CellWithoutImageView.h"

//http://stage.phonethics.in/shoplocal/api-2/place_api/search?latitude=19.1321795&longitude=72.8345789&distance=25&locality=lokhandwala&category_id=2&page=1&count=20
//#define ALL_STORE_LINK(LAT,LONG,DIST,LOCALITY,PAGE) [NSString stringWithFormat:@"%@%@%@place_api/search?latitude=%@&longitude=%@&distance=%@&locality=%@&page=%@&count=20",LIVE_SERVER,URL_PREFIX,API_VERSION,LAT,LONG,DIST,LOCALITY,PAGE]
#define FOOD_STORE_LINK(LAT,LONG,DIST,LOCALITY,PAGE) [NSString stringWithFormat:@"%@%@%@place_api/search?latitude=%f&longitude=%f&distance=%d&area_id=%@&category_id=2&page=%@&count=50",LIVE_SERVER,URL_PREFIX,API_VERSION,LAT,LONG,DIST,LOCALITY,PAGE]
#define THUMBNAIL_LINK(IMG_LINK) [NSString stringWithFormat:@"%@%@%@",LIVE_SERVER,URL_PREFIX,IMG_LINK]

#define FOOD_BROADCASTPOST_LINK(LOCALITY,PAGE) [NSString stringWithFormat:@"%@%@%@broadcast_api/search?area_id=%@&category_id=2&page=%@&count=50",LIVE_SERVER,URL_PREFIX,API_VERSION,LOCALITY,PAGE]

@interface INFoodViewController ()
@property(strong) NSDictionary *splashJson;
@end

@implementation INFoodViewController
@synthesize foodTableView;
@synthesize foodArray;
@synthesize countlbl;
@synthesize textPull, textRelease, textLoading, refreshHeaderView, refreshLabel, refreshArrow, refreshSpinner;
@synthesize totalPage;
@synthesize totalRecord;
@synthesize currentPage;
@synthesize countHeaderView,lblpullToRefresh;
@synthesize isStoreSearch;
@synthesize currentSelectedRow;
@synthesize bgImageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self setupStrings];
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:FALSE];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:TRUE];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                             style:UIBarButtonItemStylePlain
                                                            target:nil
                                                            action:nil];
    [[self navigationItem] setBackBarButtonItem:back];
    [self setUI];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadData:) name:IN_STORE_DID_CHANGE_NOTIFICATION object:nil];
    //[self addHUDView];
    
    
    if (!isStoreSearch) {
        [self.foodTableView registerNib:[UINib nibWithNibName:@"CellWithImageView" bundle:nil] forCellReuseIdentifier:kWithImageCellIdentifier];
        [self.foodTableView registerNib:[UINib nibWithNibName:@"CellWithoutImageView" bundle:nil] forCellReuseIdentifier:kWithoutImageCellIdentifier];
    }
    bgImageView.hidden = NO;
    self.foodTableView.backgroundColor = [UIColor clearColor];//[UIColor colorWithWhite:0.9 alpha:1.0];
    self.foodTableView.separatorColor = [UIColor clearColor];
    
    foodArray = [[NSMutableArray alloc] init];
    [self addPullToRefreshHeader];
    [self loadData];
}

#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void)setUI
{
    countHeaderView.backgroundColor =   [UIColor clearColor];
    countHeaderView.clipsToBounds   = YES;
    
    lblpullToRefresh.font               =   DEFAULT_FONT(15.0);
    lblpullToRefresh.backgroundColor    =   LIGHT_GREEN_COLOR_WITH_ALPHA(0.5);
    lblpullToRefresh.textColor          =   [UIColor whiteColor];
    
    countlbl.font               =   DEFAULT_FONT(15.0);
    countlbl.backgroundColor    =   LIGHT_GREEN_COLOR_WITH_ALPHA(0.5);
    countlbl.textColor          =   [UIColor whiteColor];
}

-(void)addHUDView
{
    hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:hud];
	hud.dimBackground = NO;
    hud.labelText = @"Loading";
	hud.detailsLabelText = @"Please wait ...";
    [hud hide:NO];
}

-(void)removeHUDView
{
    [hud removeFromSuperview];
    hud = nil;
}

-(void)sendAllStoreRequest:(NSString *)urlString
{
    DebugLog(@"url - %@",urlString);
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        if(isLoading)
                                                        {
                                                            [self stopLoading];
                                                        }
                                                        if(self.splashJson  != nil) {
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSDictionary *maindict = [self.splashJson  objectForKey:@"data"];
                                                                self.totalPage = [[maindict  objectForKey:@"total_page"] integerValue];
                                                                self.totalRecord = [[maindict  objectForKey:@"total_record"] integerValue];
                                                                NSArray* jsonArray = [maindict  objectForKey:@"record"];
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *objdict = [jsonArray  objectAtIndex:index];
                                                                    //DebugLog(@"\n\n%@\n\n",objdict);
                                                                    storeObj = [[INAllStoreObj alloc] init];
                                                                    storeObj.area = [objdict objectForKey:@"area"];
                                                                    storeObj.building = [objdict objectForKey:@"building"];
                                                                    storeObj.city = [objdict objectForKey:@"city"];
                                                                    storeObj.description = [objdict objectForKey:@"description"];
                                                                    storeObj.distance = [objdict objectForKey:@"distance"];
                                                                    storeObj.email = [objdict objectForKey:@"email"];
                                                                    storeObj.has_offer = [objdict objectForKey:@"has_offer"];
                                                                    storeObj.ID = [[objdict objectForKey:@"id"] integerValue];
                                                                    storeObj.image_url = [objdict objectForKey:@"image_url"];
                                                                    storeObj.landmark = [objdict objectForKey:@"landmark"];
                                                                    storeObj.mob_no1 = [objdict objectForKey:@"mob_no1"];
                                                                    storeObj.name = [objdict objectForKey:@"name"];
                                                                    storeObj.place_parent = [[objdict objectForKey:@"place_parent"] integerValue];
                                                                    storeObj.street = [objdict objectForKey:@"street"];
                                                                    storeObj.title = [objdict objectForKey:@"title"];
                                                                    storeObj.total_like = [objdict objectForKey:@"total_like"];
                                                                    storeObj.website = [objdict objectForKey:@"website"];
                                                                    [foodArray addObject:storeObj];
                                                                    storeObj = nil;
                                                                }
                                                            } else {
                                                                if([self.splashJson  objectForKey:@"message"] != [NSNull null])
                                                                    [INUserDefaultOperations showAlert:[self.splashJson  objectForKey:@"message"]];
                                                            }
//                                                            if([foodArray count] > 0)
//                                                            {
                                                                [foodTableView reloadData];
                                                                [self setCountLabel];
//                                                            }
                                                        }
                                                        isAdding = NO;
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        DebugLog(@"-%@-%@-%@-%d-",response,error,JSON,response.statusCode);
                                                        DebugLog(@"-%d-%d-",[error code],NSURLErrorCancelled);
                                                        if ([error code] == NSURLErrorCancelled) {
                                                            DebugLog(@"yup operation is cancelled %d",[error code]);
                                                        }else{
                                                            UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                         message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                        delegate:nil
                                                                                               cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                            [av show];
                                                        }
                                                        isAdding = NO;
                                                        if(isLoading)
                                                        {
                                                            [self stopLoading];
                                                        }
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                    }];    
    [operation start];
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleShrink];
    [MMProgressHUD showWithTitle:HUD_TITLE
                          status:@""
             confirmationMessage:[NSString stringWithFormat:@"Cancel?"]
                     cancelBlock:^{
                         DebugLog(@"Task was cancelled!");
                         [operation cancel];
                     }];
}

-(void)sendFoodBroadCastRequest:(NSString *)urlString
{
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"sendStoreBroadcastRequest %@", self.splashJson);
                                                        if(self.splashJson  != nil) {
                                                            [foodArray removeAllObjects];
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSDictionary *maindict = [self.splashJson  objectForKey:@"data"];
                                                                self.totalPage = [[maindict  objectForKey:@"total_page"] integerValue];
                                                                self.totalRecord = [[maindict  objectForKey:@"total_record"] integerValue];
                                                                NSArray* jsonArray = [maindict  objectForKey:@"record"];
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *objdict = [jsonArray  objectAtIndex:index];
                                                                    //DebugLog(@"\n\n%@\n\n",objdict);
                                                                    INBroadcastDetailObj *broadcastObj = [[INBroadcastDetailObj alloc] init];
                                                                    broadcastObj.ID = [[objdict objectForKey:@"post_id"] integerValue];
                                                                    broadcastObj.place_ID = [[objdict objectForKey:@"id"] integerValue];
                                                                    broadcastObj.areaID = [objdict objectForKey:@"area_id"];
                                                                    broadcastObj.name = [objdict objectForKey:@"name"];
                                                                    broadcastObj.title = [objdict objectForKey:@"title"];
                                                                    broadcastObj.description = [objdict objectForKey:@"description"];
                                                                    broadcastObj.date = [objdict objectForKey:@"date"];
                                                                    broadcastObj.offerdate = [objdict objectForKey:@"offer_date_time"];
                                                                    broadcastObj.distance = [objdict objectForKey:@"distance"];
                                                                    broadcastObj.latitude = [objdict objectForKey:@"latitude"];
                                                                    broadcastObj.longitude = [objdict objectForKey:@"longitude"];
                                                                    broadcastObj.tel_no1 = [objdict objectForKey:@"tel_no1"];
                                                                    broadcastObj.tel_no2 = [objdict objectForKey:@"tel_no2"];
                                                                    broadcastObj.tel_no3 = [objdict objectForKey:@"tel_no3"];
                                                                    broadcastObj.mob_no1 = [objdict objectForKey:@"mob_no1"];
                                                                    broadcastObj.mob_no2 = [objdict objectForKey:@"mob_no2"];
                                                                    broadcastObj.mob_no3 = [objdict objectForKey:@"mob_no3"];
                                                                    broadcastObj.image_url1 = [objdict objectForKey:@"image_url1"];
                                                                    broadcastObj.image_url2 = [objdict objectForKey:@"image_url2"];
                                                                    broadcastObj.image_url3 = [objdict objectForKey:@"image_url3"];
                                                                    broadcastObj.thumb_url1 = [objdict objectForKey:@"thumb_url1"];
                                                                    broadcastObj.thumb_url2 = [objdict objectForKey:@"thumb_url2"];
                                                                    broadcastObj.thumb_url3 = [objdict objectForKey:@"thumb_url3"];
                                                                    broadcastObj.total_like = [objdict objectForKey:@"total_like"];
                                                                    broadcastObj.total_share = [objdict objectForKey:@"total_share"];
                                                                    broadcastObj.is_offered = [objdict objectForKey:@"is_offered"];
                                                                    broadcastObj.type = [objdict objectForKey:@"type"];
                                                                    broadcastObj.verified = [objdict objectForKey:@"verified"];
                                                                    [foodArray addObject:broadcastObj];
                                                                    broadcastObj = nil;
                                                                }
                                                            }else {
                                                                if([self.splashJson  objectForKey:@"message"] != [NSNull null])
                                                                    [INUserDefaultOperations showAlert:[self.splashJson  objectForKey:@"message"]];
                                                            }
                                                            DebugLog(@"foodArray %@",foodArray);
//                                                            if([foodArray count] > 0)
//                                                            {
                                                                [foodTableView reloadData];
                                                                [self setCountLabel];
//                                                            }
                                                        }
                                                        isAdding = NO;
                                                        if(isLoading)
                                                        {
                                                            [self stopLoading];
                                                        }
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK"
                                                                                           otherButtonTitles:nil];
                                                        [av show];
                                                        isAdding = NO;
                                                        if(isLoading)
                                                        {
                                                            [self stopLoading];
                                                        }
                                                    }];
    [operation start];
}



#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (isStoreSearch) {
//        INAllStoreObj *placeObj  = [foodArray objectAtIndex:indexPath.row];
//        if (placeObj.has_offer != nil && [placeObj.has_offer isEqualToString:@"1"]) {
//            return 142;
//        } else {
//            return 110;
//        }
        return 130;
    }else{
        INBroadcastDetailObj *tempbroadObj  = [foodArray objectAtIndex:indexPath.row];
        if(tempbroadObj.image_url1 != (NSString*)[NSNull null] && ![tempbroadObj.image_url1 isEqualToString:@""]) {
            return kWithImageCellHeight;
        }
        else {
            return kWithoutImageCellHeight;
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [foodArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isStoreSearch) {
        static NSString *CellIdentifier = @"Cell";
        UIView *backView;
        UILabel *lblOffer;
        
        UIImageView *thumbImg;
        UILabel *lblTitle;
        UILabel *lblDesc;
        
        UIImageView *offerthumbImg;
        
        UIImageView *likethumbImg;
        UILabel *lblLikeCount;
        
        UIImageView *ldistthumbImg;
        UILabel *lblDist;

        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryNone;
            backView = [[UIView alloc] initWithFrame:CGRectMake(10, 5, 300, 120)];
            backView.tag = 333;
            backView = [CommonCallback setViewPropertiesWithRoundedCorner:backView];
            backView.clipsToBounds = YES;
            
            lblOffer = [[UILabel alloc] initWithFrame:CGRectMake(0.0,0, 320.0, 30.0)];
            lblOffer.text = @"";
            lblOffer.tag = 111;
            lblOffer.font = DEFAULT_BOLD_FONT(15);
            lblOffer.textAlignment = UITextAlignmentCenter;
            lblOffer.textColor = [UIColor whiteColor];
            lblOffer.backgroundColor =  LIGHT_GREEN_COLOR;
            lblOffer.hidden = TRUE;
            [backView addSubview:lblOffer];
            
            thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, 120, 120)];
            thumbImg.tag = 100;
            thumbImg.contentMode = UIViewContentModeScaleToFill;
            [backView addSubview:thumbImg];
            
            offerthumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(4,4,71.0,50.0)];
            offerthumbImg.tag = 107;
            offerthumbImg.image = [UIImage imageNamed:@"Offer_Icon1.png"];
            offerthumbImg.contentMode = UIViewContentModeScaleAspectFill;
            [backView addSubview:offerthumbImg];
            
            lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame)+5,4,130,25.0)];
            lblTitle.text = @"";
            lblTitle.tag = 101;
            lblTitle.font = DEFAULT_BOLD_FONT(15);
            lblTitle.textAlignment = UITextAlignmentLeft;
            lblTitle.textColor = BROWN_COLOR;
            lblTitle.highlightedTextColor = [UIColor whiteColor];
            lblTitle.backgroundColor =  [UIColor clearColor];
            lblTitle.numberOfLines = 2;
            [backView addSubview:lblTitle];
            
            lblDesc = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame)+5, CGRectGetMaxY(lblTitle.frame),169,70)];
            lblDesc.text = @"";
            lblDesc.tag = 102;
            lblDesc.numberOfLines = 4;
            lblDesc.font = DEFAULT_FONT(12);
            lblDesc.textAlignment = UITextAlignmentLeft;
            lblDesc.textColor = BROWN_COLOR;
            lblDesc.highlightedTextColor = [UIColor whiteColor];
            lblDesc.backgroundColor =  [UIColor clearColor];
            [backView addSubview:lblDesc];
            
            likethumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lblTitle.frame)+2, 10,15.0,15.0)];
            likethumbImg.tag = 103;
            likethumbImg.image = [UIImage imageNamed:@"list_fav_icon.png"];
            likethumbImg.contentMode = UIViewContentModeScaleAspectFill;
            [backView addSubview:likethumbImg];
            
            lblLikeCount = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(likethumbImg.frame),10,20.0,15.0)];
            lblLikeCount.text = @"";
            lblLikeCount.tag = 104;
            lblLikeCount.font = DEFAULT_BOLD_FONT(10);
            lblLikeCount.textAlignment = UITextAlignmentCenter;
            lblLikeCount.textColor = BROWN_COLOR;
            lblLikeCount.highlightedTextColor = [UIColor whiteColor];
            lblLikeCount.backgroundColor =  [UIColor clearColor];
            [backView addSubview:lblLikeCount];
            
            ldistthumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth(backView.frame)-67,CGRectGetMaxY(lblDesc.frame)+2,15.0,15.0)];
            ldistthumbImg.image = [UIImage imageNamed:@"list_distance_icon.png"];
            ldistthumbImg.tag = 105;
            ldistthumbImg.contentMode = UIViewContentModeScaleAspectFill;
            [backView addSubview:ldistthumbImg];
            
            lblDist = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(ldistthumbImg.frame),CGRectGetMaxY(lblDesc.frame)+2,50.0,15.0)];
            lblDist.text = @"";
            lblDist.tag = 106;
            lblDist.font = DEFAULT_BOLD_FONT(10);
            lblDist.textAlignment = UITextAlignmentCenter;
            lblDist.textColor = BROWN_COLOR;
            lblDist.highlightedTextColor = [UIColor whiteColor];
            lblDist.backgroundColor =  [UIColor clearColor];
            [backView addSubview:lblDist];
            
            [cell.contentView addSubview:backView];
            
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
            bgColorView.tag = 104;
            cell.selectedBackgroundView = bgColorView;
        }
        if ([indexPath row] < [foodArray count]) {
            INAllStoreObj *placeObj  = [foodArray objectAtIndex:indexPath.row];
            //cell.textLabel.text = placeObj.name;
            //cell.detailTextLabel.text = placeObj.description;
            //cell.detailTextLabel.numberOfLines = 3;
            backView        = (UIView *)[cell viewWithTag:333];
            lblOffer        = (UILabel *)[backView viewWithTag:111];
            thumbImg        = (UIImageView *)[backView viewWithTag:100];
            lblTitle        = (UILabel *)[backView viewWithTag:101];
            lblDesc         = (UILabel *)[backView viewWithTag:102];
            likethumbImg    = (UIImageView *)[backView viewWithTag:103];
            lblLikeCount    = (UILabel *)[backView viewWithTag:104];
            ldistthumbImg   = (UIImageView *)[backView viewWithTag:105];
            lblDist         = (UILabel *)[backView viewWithTag:106];
            offerthumbImg   = (UIImageView *)[backView viewWithTag:107];

            /*
            if (placeObj.has_offer != nil && [placeObj.has_offer isEqualToString:@"1"]) {
                //thumbImgview.frame =  CGRectMake(10, 30, 100, 100);
                lblOffer.hidden = FALSE;
                [lblOffer setText:[NSString stringWithFormat:@"Special Offer: %@",placeObj.title]];
                
                thumbImg.frame =  CGRectMake(1,CGRectGetMaxY(lblOffer.frame), thumbImg.frame.size.width, thumbImg.frame.size.height);
                lblTitle.frame =  CGRectMake(CGRectGetMaxX(thumbImg.frame)+5,CGRectGetMaxY(lblOffer.frame), lblTitle.frame.size.width, lblTitle.frame.size.height);
                
                likethumbImg.frame =  CGRectMake(CGRectGetMaxX(lblTitle.frame)+2,CGRectGetMaxY(lblOffer.frame)+10, likethumbImg.frame.size.width, likethumbImg.frame.size.height);
                lblLikeCount.frame =  CGRectMake(CGRectGetMaxX(likethumbImg.frame),CGRectGetMaxY(lblOffer.frame)+10, lblLikeCount.frame.size.width, lblLikeCount.frame.size.height);
            } else {
                //thumbImgview.frame =  CGRectMake(10, 10, 100, 100);
                lblOffer.hidden = TRUE;
                [lblOffer setText:@""];
                
                thumbImg.frame =  CGRectMake(1,0, thumbImg.frame.size.width, thumbImg.frame.size.height);
                lblTitle.frame =  CGRectMake(CGRectGetMaxX(thumbImg.frame)+5,0, lblTitle.frame.size.width, lblTitle.frame.size.height);
                
                likethumbImg.frame =  CGRectMake(CGRectGetMaxX(lblTitle.frame)+2,10, likethumbImg.frame.size.width, likethumbImg.frame.size.height);
                lblLikeCount.frame =  CGRectMake(CGRectGetMaxX(likethumbImg.frame),10, lblLikeCount.frame.size.width, lblLikeCount.frame.size.height);
            }
            lblDesc.frame =  CGRectMake(CGRectGetMaxX(thumbImg.frame)+5,CGRectGetMaxY(lblTitle.frame), lblDesc.frame.size.width, lblDesc.frame.size.height);
            
            ldistthumbImg.frame =  CGRectMake(250,CGRectGetMaxY(lblDesc.frame), ldistthumbImg.frame.size.width, ldistthumbImg.frame.size.height);
            lblDist.frame =  CGRectMake(CGRectGetMaxX(ldistthumbImg.frame),CGRectGetMaxY(lblDesc.frame), lblDist.frame.size.width, lblDist.frame.size.height);
            */
            
            lblOffer.hidden = YES;
            if (placeObj.has_offer != nil && [placeObj.has_offer isEqualToString:@"1"]) {
                backView.layer.borderColor = ORANGE_COLOR.CGColor;
                backView.layer.borderWidth = 5.0;
                offerthumbImg.hidden = FALSE;
            }else{
                backView.layer.borderWidth = 0.0;
                offerthumbImg.hidden = YES;
            }

            //DebugLog(@"link-%@",THUMBNAIL_LINK(placeObj.image_url));
            
            __weak UIImageView *thumbImg_ = thumbImg;
            [thumbImg setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(placeObj.image_url)]
                     placeholderImage:[UIImage imageNamed:@"list_thumbnail.png"]
                              success:^(UIImage *image) {
                                  //DebugLog(@"success");
                              }
                              failure:^(NSError *error) {
                                  //DebugLog(@"write error %@", error);
                                  thumbImg_.image = [UIImage imageNamed:@"list_thumbnail.png"];
                              }];
            
            [lblTitle setText:placeObj.name];
            [lblDesc setText: placeObj.description];
            
            if(placeObj.distance != nil) {
                DebugLog(@"%@",placeObj.distance);
                if([placeObj.distance floatValue]  > -1 && [placeObj.distance floatValue]  < 1)
                    [lblDist setText: [NSString stringWithFormat:@"%.2fm",([placeObj.distance floatValue] * 100)]];
                else
                    [lblDist setText: [NSString stringWithFormat:@"%.2fkm",([placeObj.distance floatValue])]];
            } else {
                [lblDist setText: @"0.00m"];
            }
            
            //DebugLog(@"like-->%@",placeObj.total_like);
            //        if(placeObj.total_like != nil && ![placeObj.total_like isEqualToString:@"0"]) {
            [lblLikeCount setHidden:FALSE];
            [likethumbImg setHidden:FALSE];
            lblLikeCount.text = placeObj.total_like;
            //        } else {
            //            [lblLikeCount setHidden:TRUE];
            //            [likethumbImg setHidden:TRUE];
            //        }
        }
        return cell;
    }else{

        DebugLog(@"---%@----",foodArray);
        if ([foodArray count] <= 0)
            return nil;
        
        INBroadcastDetailObj *tempbroadcastObj  = [foodArray objectAtIndex:indexPath.row];
        DebugLog(@"---%@----",tempbroadcastObj.date);
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        NSDate *myDate = [[NSDate alloc] init];
        myDate = [dateFormatter dateFromString:tempbroadcastObj.date];
        DebugLog(@"MyDate is: %@", myDate);
        
        //char	*strptime_l(const char * __restrict, const char * __restrict, struct tm * __restrict, locale_t)
//        const char *str = [tempbroadcastObj.date UTF8String];
//        const char *fmt = [@"%Y-%m-%d %H:%M:%S" UTF8String];
//        struct tm timeinfo;
//        memset(&timeinfo, 0, sizeof(timeinfo));
//        char *ret = strptime_l(str, fmt, &timeinfo, NULL);
//        
//        NSDate *myDate = nil;
//        
//        if (ret) {
//            time_t time = mktime(&timeinfo);
//            
//            myDate = [NSDate dateWithTimeIntervalSince1970:time];
//            DebugLog(@"MyDate is: %@", myDate);
//        }
        
        
        NSDateFormatter *dayformatter = [[NSDateFormatter alloc] init];
        [dayformatter setDateFormat:@"dd"];
        NSString *dayFromDate = [dayformatter stringFromDate:myDate];
        DebugLog(@"Myday is: %@", dayFromDate);
        
        NSDateFormatter *monthformatter = [[NSDateFormatter alloc] init];
        [monthformatter setDateFormat:@"MMM"];
        NSString *monthFromDate = [monthformatter stringFromDate:myDate];
        DebugLog(@"MyMonth is: %@", monthFromDate);
        
        NSDateFormatter *timeformatter = [[NSDateFormatter alloc] init];
        [timeformatter setDateFormat:@"hh:mm a"];
        NSString *timeFromDate = [timeformatter stringFromDate:myDate];
        DebugLog(@"timeFromDate is: %@", timeFromDate);
        
        if(tempbroadcastObj.image_url1 != (NSString*)[NSNull null] && ![tempbroadcastObj.image_url1 isEqualToString:@""]) {
            NSString *cellIdentifier = kWithImageCellIdentifier;
            CellWithImageView *cell = (CellWithImageView *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            cell.lblTitle.text          = tempbroadcastObj.title;
            cell.lblLeftTitle.text      = tempbroadcastObj.name;
            cell.lblDescription.text    = tempbroadcastObj.description;
            cell.lblLikeCount.text      = tempbroadcastObj.total_like;
            cell.lblShareCount.text     = tempbroadcastObj.total_share;
            cell.lblTimeStamp.text      = [NSString stringWithFormat:@"%@ %@    %@",dayFromDate,monthFromDate,timeFromDate];
            if (tempbroadcastObj.is_offered != nil && [tempbroadcastObj.is_offered isEqualToString:@"1"]) {
                [cell.offerImageView setHidden:NO];
            }else{
                [cell.offerImageView setHidden:YES];
            }
            if (tempbroadcastObj.user_like != nil && [tempbroadcastObj.user_like isEqualToString:@"1"]) {
                cell.favImageView.image = [UIImage imageNamed:@"Shoplocal_Post-Title_Fv.png"];
            }else{
                cell.favImageView.image = [UIImage imageNamed:@"favGray.png"];
            }
            
//            [cell.thumbImageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(tempbroadcastObj.image_url1)] placeholderImage:[UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"]];
            
            if(tempbroadcastObj.image_url1 != nil && ![tempbroadcastObj.image_url1 isEqualToString:@""])
            {
                __weak UIImageView *thumbImg_ = cell.thumbImageView;
                [cell.thumbImageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(tempbroadcastObj.image_url1)]
                                    placeholderImage:[UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"]
                                             success:^(UIImage *image) {
                                                 DebugLog(@"success");
                                             }
                                             failure:^(NSError *error) {
                                                 DebugLog(@"write error %@", error);
                                                 thumbImg_.image = [UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"];
                                             }];
            } else {
                cell.thumbImageView.image = [UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"];
            }
            
            if(tempbroadcastObj.image_url1 != nil && ![tempbroadcastObj.image_url1 isEqualToString:@""])
            {
                __weak UIImageView *thumbImg_ = cell.thumbImageView;
                [cell.thumbImageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(tempbroadcastObj.image_url1)]
                                    placeholderImage:[UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"]
                                             success:^(UIImage *image) {
                                                 DebugLog(@"success");
                                             }
                                             failure:^(NSError *error) {
                                                 DebugLog(@"write error %@", error);
                                                 thumbImg_.image = [UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"];
                                             }];
            } else {
                cell.thumbImageView.image = [UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"];
            }
            
            UIButton *likeBtn = (UIButton *)[cell viewWithTag:111];
            [likeBtn addTarget:self action:@selector(likeBtnPressed:event:) forControlEvents:UIControlEventTouchUpInside];
            
            UIButton *shareBtn = (UIButton *)[cell viewWithTag:222];
            [shareBtn addTarget:self action:@selector(shareBtnPressed:event:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
            
        }else{
            NSString *cellIdentifier = kWithoutImageCellIdentifier;
            CellWithoutImageView *cell = (CellWithoutImageView *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            cell.lblTitle.text          = tempbroadcastObj.title;
            cell.lblLeftTitle.text      = tempbroadcastObj.name;
            cell.lblDescription.text    = tempbroadcastObj.description;
            cell.lblLikeCount.text      = tempbroadcastObj.total_like;
            cell.lblShareCount.text     = tempbroadcastObj.total_share;
            cell.lblTimeStamp.text      = [NSString stringWithFormat:@"%@ %@    %@",dayFromDate,monthFromDate,timeFromDate];
            if (tempbroadcastObj.is_offered != nil && [tempbroadcastObj.is_offered isEqualToString:@"1"]) {
                [cell.offerImageView setHidden:NO];
            }else{
                [cell.offerImageView setHidden:YES];
            }
            if (tempbroadcastObj.user_like != nil && [tempbroadcastObj.user_like isEqualToString:@"1"]) {
                cell.favImageView.image = [UIImage imageNamed:@"Shoplocal_Post-Title_Fv.png"];
            }else{
                cell.favImageView.image = [UIImage imageNamed:@"favGray.png"];
            }
            
            UIButton *likeBtn = (UIButton *)[cell viewWithTag:111];
            [likeBtn addTarget:self action:@selector(likeBtnPressed:event:) forControlEvents:UIControlEventTouchUpInside];
            
            UIButton *shareBtn = (UIButton *)[cell viewWithTag:222];
            [shareBtn addTarget:self action:@selector(shareBtnPressed:event:) forControlEvents:UIControlEventTouchUpInside];

            return cell;
        }
    }
}


#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (isStoreSearch) {
        INAllStoreObj *placeObj  = [foodArray objectAtIndex:indexPath.row];
        INStoreDetailViewController *detailController = [[INStoreDetailViewController alloc] initWithNibName:@"INStoreDetailViewController" bundle:nil] ;
        detailController.title = placeObj.name;
        detailController.storeId = placeObj.ID;
        if ([placeObj.image_url hasPrefix:@"http"]) {
            detailController.storeImageUrl = [NSURL URLWithString:placeObj.image_url];
        }else{
            detailController.storeImageUrl = [NSURL URLWithString:THUMBNAIL_LINK(placeObj.image_url)];
        }
        [self.navigationController pushViewController:detailController animated:YES];
    }else{
        INBroadcastDetailObj *broadcastObj  = [foodArray objectAtIndex:indexPath.row];
        INCustBroadCastDetailsViewController *postDetailController = [[INCustBroadCastDetailsViewController alloc] initWithNibName:@"INCustBroadCastDetailsViewController" bundle:nil] ;
        postDetailController.title = broadcastObj.name;//@"Broadcast Detail";
        postDetailController.storeName = broadcastObj.name;
        postDetailController.postType = 1;
        postDetailController.postlikeType = broadcastObj.user_like;
        postDetailController.postId = broadcastObj.ID;
        postDetailController.postTitle = broadcastObj.title;
        postDetailController.postDescription = broadcastObj.description;
        DebugLog(@"lfeedObj.image_url1 %@",broadcastObj.image_url1);
        if ([broadcastObj.image_url1 isEqual:[NSNull null]] || broadcastObj.image_url1 == nil) {
            postDetailController.postimageUrl = @"";
        }else{
            postDetailController.postimageUrl = broadcastObj.image_url1;
        }
        postDetailController.likescountString = broadcastObj.total_like;
        postDetailController.sharecountString = broadcastObj.total_share;
        postDetailController.viewOpenedFrom = FROM_NEWS_FEED;
        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
        if (broadcastObj.tel_no1 != nil && ![broadcastObj.tel_no1 isEqualToString:@""]) {
            [tempArray addObject:broadcastObj.tel_no1];
        }
        if (broadcastObj.tel_no2 != nil && ![broadcastObj.tel_no2 isEqualToString:@""]) {
            [tempArray addObject:broadcastObj.tel_no2];
        }
        if (broadcastObj.tel_no3 != nil && ![broadcastObj.tel_no3 isEqualToString:@""]) {
            [tempArray addObject:broadcastObj.tel_no3];
        }
        if (broadcastObj.mob_no1 != nil && ![broadcastObj.mob_no1 isEqualToString:@""]) {
            [tempArray addObject:broadcastObj.mob_no1];
        }
        if (broadcastObj.mob_no2 != nil && ![broadcastObj.mob_no2 isEqualToString:@""]) {
            [tempArray addObject:broadcastObj.mob_no2];
        }
        if (broadcastObj.mob_no3 != nil && ![broadcastObj.mob_no3 isEqualToString:@""]) {
            [tempArray addObject:broadcastObj.mob_no3];
        }
        DebugLog(@"temparray %@",tempArray);
        postDetailController.telArray = tempArray;
        postDetailController.SHOW_BUY_BTN = YES;
        postDetailController.placeId = broadcastObj.place_ID;
        [self.navigationController pushViewController:postDetailController animated:YES];
    }
     [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setFoodTableView:nil];
    [self setCountlbl:nil];
    [self setCountHeaderView:nil];
    [self setLblpullToRefresh:nil];
    [self setBgImageView:nil];
    [super viewDidUnload];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    //[self removeHUDView];
}

- (UIImage *)scale:(UIImage *)image toSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}

-(void)pageRequest:(NSString *)pageNum
{
    if([IN_APP_DELEGATE networkavailable])
    {
        isAdding = YES;
        DebugLog(@"----------isStoreSearch %d---------",isStoreSearch);
        if (isStoreSearch) {
            [self sendAllStoreRequest:FOOD_STORE_LINK([INUserDefaultOperations getLatitude],[INUserDefaultOperations getLongitude],[INUserDefaultOperations getCustomerDistance],[IN_APP_DELEGATE getActiveAreaIdFromAreaTable],pageNum)];
        }else{
            [self sendFoodBroadCastRequest:FOOD_BROADCASTPOST_LINK([IN_APP_DELEGATE getActiveAreaIdFromAreaTable],pageNum)];
        }
    }  else   {
        isAdding = NO;
        if(isLoading)
        {
            [self stopLoading];
        }
         [INUserDefaultOperations showOfflineAlert];
    }
}

-(void)loadData
{
    self.currentPage = 1;
    [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
}

- (void)reloadData:(NSNotification *)notification
{
    DebugLog(@"Reload data");
    [foodArray removeAllObjects];
    self.currentPage = 1;
    [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
}

-(void)setCountLabel
{
    self.countlbl.text = [NSString stringWithFormat:@"%d/%d",[foodArray count],self.totalRecord];
    DebugLog(@"-----%@--------",countlbl.text);
}

#pragma mark PULL TO REFRESH
- (void)setupStrings{
    textPull = @"Pull down to refresh...";
    textRelease = @"Release to refresh...";
    textLoading = @"Loading please wait...";
}

- (void)addPullToRefreshHeader {
    refreshHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0 - REFRESH_HEADER_HEIGHT, 320, REFRESH_HEADER_HEIGHT)];
    refreshHeaderView.backgroundColor = [UIColor clearColor];
    
    refreshLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, REFRESH_HEADER_HEIGHT)];
    refreshLabel.backgroundColor = [UIColor clearColor];
    refreshLabel.font = DEFAULT_BOLD_FONT(15);
    refreshLabel.textAlignment = UITextAlignmentCenter;
    refreshLabel.textColor = [UIColor whiteColor];
    refreshArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:PULLTOREFRESH_WARROW]];
    refreshArrow.frame = CGRectMake(floorf((REFRESH_HEADER_HEIGHT - 2) / 2),
                                    (floorf(REFRESH_HEADER_HEIGHT - 44) / 2),
                                    27, 44);
    
    refreshSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    refreshSpinner.frame = CGRectMake(floorf(floorf(REFRESH_HEADER_HEIGHT - 2) / 2), floorf((REFRESH_HEADER_HEIGHT - 20) / 2), 20, 20);
    refreshSpinner.hidesWhenStopped = YES;
    
    [refreshHeaderView addSubview:refreshLabel];
    [refreshHeaderView addSubview:refreshArrow];
    [refreshHeaderView addSubview:refreshSpinner];
    [foodTableView addSubview:refreshHeaderView];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (isLoading) return;
    isDragging = YES;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (isLoading) {
        // Update the content inset, good for section headers
        if (scrollView.contentOffset.y > 0)
            foodTableView.contentInset = UIEdgeInsetsZero;
        else if (scrollView.contentOffset.y >= -REFRESH_HEADER_HEIGHT)
            foodTableView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (isDragging && scrollView.contentOffset.y < 0) {
        // Update the arrow direction and label
        [UIView animateWithDuration:0.25 animations:^{
            if (scrollView.contentOffset.y < -REFRESH_HEADER_HEIGHT) {
                // User is scrolling above the header
                refreshLabel.text = self.textRelease;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
            } else {
                // User is scrolling somewhere within the header
                refreshLabel.text = self.textPull;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
            }
        }];
    } else {
        CGFloat height = scrollView.frame.size.height;
        
        CGFloat contentYoffset = scrollView.contentOffset.y;
        
        CGFloat distanceFromBottom = scrollView.contentSize.height - contentYoffset;
        
        if(distanceFromBottom <= height)
        {
            if(isAdding==TRUE) {
                DebugLog(@"<<<<<<<<<<<<<<<");
                return;
            } else {
                DebugLog(@"end of the table page = %d", self.currentPage);
                if(self.currentPage < self.totalPage)
                {
                    self.currentPage = self.currentPage + 1;
                    [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
                }
            }
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (isLoading) return;
    isDragging = NO;
    if (scrollView.contentOffset.y <= -REFRESH_HEADER_HEIGHT) {
        // Released above the header
        [self startLoading];
    }
}

- (void)startLoading {
    isLoading = YES;
    // Show the header
    [UIView animateWithDuration:0.3 animations:^{
        foodTableView.contentInset = UIEdgeInsetsMake(REFRESH_HEADER_HEIGHT, 0, 0, 0);
        refreshLabel.text = self.textLoading;
        refreshArrow.hidden = YES;
        [refreshSpinner startAnimating];
    }];
    if([IN_APP_DELEGATE networkavailable])
    {
        [foodArray removeAllObjects];
        self.currentPage = 1;
        [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
    }
    else
    {
        [self stopLoading];
    }
    // Refresh action!
    //[self refresh];
}

- (void)stopLoading {
    isLoading = NO;
    // Hide the header
    [UIView animateWithDuration:0.3 animations:^{
        foodTableView.contentInset = UIEdgeInsetsZero;
        [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
    }
                     completion:^(BOOL finished) {
                         [self performSelector:@selector(stopLoadingComplete)];
                     }];
}

- (void)stopLoadingComplete {
    // Reset the header
    refreshLabel.text = self.textPull;
    refreshArrow.hidden = NO;
    [refreshSpinner stopAnimating];
}

-(void)showLoginAlert{
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Please Login to mark this post as favourite."];
    alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
    alertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
    
    [alertView addButtonWithTitle:@"LOGIN"
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alert) {
                              DebugLog(@"LOGIN Clicked");
                              [self showLoginModal];
                          }];
    [alertView addButtonWithTitle:@"NOT NOW"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alert) {
                              DebugLog(@"NOT NOW Clicked");
                          }];
    [alertView show];
}
- (void)likeBtnPressed:(id)sender event:(id)event {
    DebugLog(@"likeBtnPressed");
    if([IN_APP_DELEGATE networkavailable])
    {
        NSSet *touches = [event allTouches];
        UITouch *touch = [touches anyObject];
        CGPoint currentTouchPosition = [touch locationInView:self.foodTableView];
        NSIndexPath *indexPath = [self.foodTableView indexPathForRowAtPoint:currentTouchPosition];
        if (indexPath != nil) {
            if(![INUserDefaultOperations isCustomerLoggedIn])
            {
                [self showLoginAlert];
            } else {
                currentSelectedRow = indexPath.row;
                INBroadcastDetailObj *tempBroadCastObj = (INBroadcastDetailObj *)[foodArray objectAtIndex:indexPath.row];
                DebugLog(@"user_like %@ post_id %d",tempBroadCastObj.user_like,tempBroadCastObj.ID);
                if ([tempBroadCastObj.user_like intValue]) {
                    [self sendPostUnLikeRequest];
                }else{
                    [self sendPostLikeRequest];
                }
            }
        }
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

- (void)shareBtnPressed:(id)sender event:(id)event {
    DebugLog(@"shareBtnPressed");
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.foodTableView];
    NSIndexPath *indexPath = [self.foodTableView indexPathForRowAtPoint:currentTouchPosition];
    if (indexPath != nil) {
        currentSelectedRow = indexPath.row;
        UIActionSheet *shareActionSheet = [[UIActionSheet alloc] initWithTitle:@"Share via" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"SMS",@"Email",@"WhatsApp",@"Facebook",@"Twitter", nil];
        [shareActionSheet showInView:self.view];
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSString *title = [actionSheet buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"SMS"])
    {
        [self smsShareClicked];
    }
    else if([title isEqualToString:@"Email"])
    {
        [self emailShareClicked];
    }else if([title isEqualToString:@"WhatsApp"])
    {
        [self whatsAppShareClicked];
    }
    else if([title isEqualToString:@"Facebook"])
    {
        [self facebookShareClicked];
    } else if([title isEqualToString:@"Twitter"])
    {
        [self twitterShareClicked];
    }
}
-(void)smsShareClicked{
    if ([MFMessageComposeViewController canSendText]) {
        MFMessageComposeViewController *messageComposer = [[MFMessageComposeViewController alloc] init];
        messageComposer.messageComposeDelegate = self;
        messageComposer.navigationBar.tintColor  = [UIColor brownColor];
        INBroadcastDetailObj *tempBroadCastObj = (INBroadcastDetailObj *)[foodArray objectAtIndex:currentSelectedRow];
        [messageComposer setBody:SHARE_MSG_BODY(tempBroadCastObj.name,tempBroadCastObj.title,tempBroadCastObj.description)];
        [messageComposer setRecipients:nil];
        [self presentModalViewController:messageComposer animated:YES];
    }else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"You may not have set up sms service on your device.\nPlease check and try again."
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}
#pragma mark MFMessageComposeViewController Delegate Methods
-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
		case MessageComposeResultCancelled:
            DebugLog(@"SMS Cancelled");
			break;
		case MessageComposeResultFailed:
        {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Unknown Error\nSMS failed"
														   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
        }
			break;
		case MessageComposeResultSent:
        {
            DebugLog(@"SMS Sent");
            [self sendPostShareRequest:@"sms"];
        }
			break;
		default:
			break;
	}
    [self dismissModalViewControllerAnimated:YES];
}

-(void)emailShareClicked{
    if ([MFMailComposeViewController canSendMail]) {
        INBroadcastDetailObj *tempBroadCastObj = (INBroadcastDetailObj *)[foodArray objectAtIndex:currentSelectedRow];
        
        MFMailComposeViewController *emailComposer = [[MFMailComposeViewController alloc] init];
        emailComposer.mailComposeDelegate = self;
        emailComposer.navigationBar.tintColor  = [UIColor brownColor];
        [emailComposer setToRecipients:nil];
        [emailComposer setSubject:@""];
        [emailComposer setMessageBody:SHARE_MSG_BODY(tempBroadCastObj.name,tempBroadCastObj.title,tempBroadCastObj.description) isHTML:NO];
        emailComposer.toolbar.tag = 2;
        [self presentModalViewController:emailComposer animated:YES];
    }else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"You may not have set up mail service on your device or You may not connected to internet.\nPlease check and try again."
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}
#pragma mark Mail Compose Delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    if (result == MFMailComposeResultFailed) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                        message:NSLocalizedString(@"Email failed to send. Please try again.", nil)
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", nil) otherButtonTitles:nil];
		[alert show];
    }else if (result == MFMailComposeResultSent)
    {
        if (controller.toolbar.tag == 2) {
            [self sendPostShareRequest:@"email"];
        }
    }
	[self dismissModalViewControllerAnimated:YES];
}

-(void)whatsAppShareClicked{
    //    NSString *shareMSGBody = [[NSString stringWithFormat:@"Store Name: %@\nOffer: %@\nDescription: %@",storeName,postTitle,postDescription] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    INBroadcastDetailObj *tempBroadCastObj = (INBroadcastDetailObj *)[foodArray objectAtIndex:currentSelectedRow];
    
    NSString *urlString = [NSString stringWithFormat:@"whatsapp://send?text=%@",[SHARE_MSG_BODY(tempBroadCastObj.name,tempBroadCastObj.title,tempBroadCastObj.description) stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURL *whatsappURL = [NSURL URLWithString:urlString];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [[UIApplication sharedApplication] openURL: whatsappURL];
        [self sendPostShareRequest:@"whatsapp"];
    }
}

-(void)twitterShareClicked{
    
    if ([TWTweetComposeViewController canSendTweet]) {
        INBroadcastDetailObj *tempBroadCastObj = (INBroadcastDetailObj *)[foodArray objectAtIndex:currentSelectedRow];
        
        TWTweetComposeViewController *tweetComposer = [[TWTweetComposeViewController alloc] init];
        NSString *message = SHARE_MSG_BODY(tempBroadCastObj.name,tempBroadCastObj.title,tempBroadCastObj.description);
        DebugLog(@"Length %d",[message length]);
        if ([message length] > 140) {
            [tweetComposer setInitialText:[message substringToIndex:140]];
        }else{
            [tweetComposer setInitialText:message];
        }
        [self presentViewController:tweetComposer animated:YES completion:nil];
        tweetComposer.completionHandler = ^(TWTweetComposeViewControllerResult result){
            if (result == TWTweetComposeViewControllerResultDone) {
                DebugLog(@"User has finished composing the post, and tapped the send button");
                [self sendPostShareRequest:@"twitter"];
            }
            if (result == TWTweetComposeViewControllerResultCancelled) {
                DebugLog(@"User has cancelled composing the post, and tapped the cancel button");
            }
            [self dismissViewControllerAnimated:YES completion:NULL];
        };
    } else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"You may not have set up twitter service on your device or You may not connected to internet.\nPlease check and try again."
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

-(void)facebookShareClicked{
    INBroadcastDetailObj *tempBroadCastObj = (INBroadcastDetailObj *)[foodArray objectAtIndex:currentSelectedRow];
    
    FaceBookShareViewController *facebookViewComposer = [[FaceBookShareViewController alloc] initWithNibName:@"FaceBookShareViewController" bundle:nil];
    facebookViewComposer.fbdelegate = self;
    facebookViewComposer.title = @"FACEBOOK SHARE";
    facebookViewComposer.FBtitle = ALERT_TITLE;
    facebookViewComposer.postMessageText = SHARE_MSG_BODY(tempBroadCastObj.name,tempBroadCastObj.title,tempBroadCastObj.description);
    facebookViewComposer.FBtPic = THUMBNAIL_LINK(tempBroadCastObj.image_url1);
    UINavigationController *fbnavBar=[[UINavigationController alloc]initWithRootViewController:facebookViewComposer];
    [self.navigationController presentViewController:fbnavBar animated:YES completion:nil];
}

-(void)faceBookCompletionCallBack:(int)result
{
    DebugLog(@"Customer : faceBookCompletionCallBack %d",result);
    if (result == FACEBOOK_POST_SUCCESS) {
        [self sendPostShareRequest:@"facebook"];
    }
}


#pragma AFNetworking delegate methods
-(void)sendPostLikeRequest
{
    DebugLog(@"========================sendPostLikeRequest========================");
    INBroadcastDetailObj *tempBroadCastObj = (INBroadcastDetailObj *)[foodArray objectAtIndex:currentSelectedRow];
    
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[INUserDefaultOperations getCustomerAuthId] forKey:@"user_id"];
    [params setObject:[INUserDefaultOperations getCustomerAuthCode] forKey:@"auth_id"];
    [params setObject:[NSString stringWithFormat:@"%d",tempBroadCastObj.ID] forKey:@"post_id"];
    [httpClient postPath:LIKE_BROADCAST_OF_STORE parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            if([json  objectForKey:@"message"] != [NSNull null] && [[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
                NSString* message = [json objectForKey:@"message"];
                [INUserDefaultOperations showAlert:message];
                tempBroadCastObj.user_like = @"1";
                tempBroadCastObj.total_like = [NSString stringWithFormat:@"%d",[tempBroadCastObj.total_like intValue] + 1];
            } else {
                NSString* code = [json objectForKey:@"code"];
                if([code isEqualToString:CUSTOMER_ALREADY_MADE_POST_FAV])
                {
                    tempBroadCastObj.user_like = @"1";
                    if ([tempBroadCastObj.total_like intValue] == 0) {
                        tempBroadCastObj.total_like = @"1";
                    }
                    [INUserDefaultOperations showAlert:[json objectForKey:@"message"]];
                }
                else if([code isEqualToString:CUSTOMER_INVALID_USER_AUTH_CODE])
                {
                    if([INUserDefaultOperations isCustomerLoggedIn]){
                        [INUserDefaultOperations clearCustomerDetails];
                        NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:IN_CUSTOMER_LOGIN_LOGOUT_KEY];
                        [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_LOGIN_LOGOUT_NOTIFICATION object:nil userInfo:dictionary];
                        [self showLoginAlert];
                    }
                }else{
                    [INUserDefaultOperations showAlert:[json objectForKey:@"message"]];
                }
            }
            [foodArray replaceObjectAtIndex:currentSelectedRow withObject:tempBroadCastObj];
            [foodTableView reloadData];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}

-(void)sendPostUnLikeRequest
{
    INBroadcastDetailObj *tempBroadCastObj = (INBroadcastDetailObj *)[foodArray objectAtIndex:currentSelectedRow];
    
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    [httpClient setDefaultHeader:@"user_id" value:[INUserDefaultOperations getCustomerAuthId]];
    [httpClient setDefaultHeader:@"auth_id" value:[INUserDefaultOperations getCustomerAuthCode]];
    [httpClient deletePath:UNLIKE_BROADCAST_OF_STORE(tempBroadCastObj.ID) parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            if([json  objectForKey:@"message"] != [NSNull null] && [[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
                NSString* message = [json objectForKey:@"message"];
                [INUserDefaultOperations showAlert:message];
                tempBroadCastObj.user_like = @"0";
                if ([tempBroadCastObj.total_like intValue] > 0) {
                    tempBroadCastObj.total_like = [NSString stringWithFormat:@"%d",[tempBroadCastObj.total_like intValue] - 1];
                }
                [foodArray replaceObjectAtIndex:currentSelectedRow withObject:tempBroadCastObj];
                [foodTableView reloadData];
            } else {
                NSString* code = [json objectForKey:@"code"];
                if([code isEqualToString:CUSTOMER_INVALID_USER_AUTH_CODE])
                {
                    if([INUserDefaultOperations isCustomerLoggedIn]){
                        [INUserDefaultOperations clearCustomerDetails];
                        NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:IN_CUSTOMER_LOGIN_LOGOUT_KEY];
                        [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_LOGIN_LOGOUT_NOTIFICATION object:nil userInfo:dictionary];
                        [self showLoginAlert];
                    }
                }else{
                    [INUserDefaultOperations showAlert:[json objectForKey:@"message"]];
                }
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}

-(void)sendPostShareRequest:(NSString *)viaString{
    DebugLog(@"shareMsg %@ ",viaString);
    INBroadcastDetailObj *tempBroadCastObj = (INBroadcastDetailObj *)[foodArray objectAtIndex:currentSelectedRow];
    
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    //    if ([INUserDefaultOperations isCustomerLoggedIn]) {
    //        [params setObject:[INUserDefaultOperations getCustomerAuthId] forKey:@"user_id"];
    //        [params setObject:[INUserDefaultOperations getCustomerAuthCode] forKey:@"auth_id"];
    //    }
    [params setObject:[NSString stringWithFormat:@"%d",tempBroadCastObj.ID] forKey:@"post_id"];
    [params setObject:viaString forKey:@"via"];
    
    [httpClient postPath:SHARE_STORE_POST parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            if([json  objectForKey:@"message"] != [NSNull null] && [[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
                //                NSString* message = [json objectForKey:@"message"];
                //                [INUserDefaultOperations showAlert:message];
                tempBroadCastObj.total_share = [NSString stringWithFormat:@"%d",[tempBroadCastObj.total_share intValue] + 1];
                [foodArray replaceObjectAtIndex:currentSelectedRow withObject:tempBroadCastObj];
                [foodTableView reloadData];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}

-(void)showLoginModal{
    INCustomerLoginViewController *custLoginController = [[INCustomerLoginViewController alloc] initWithNibName:@"INCustomerLoginViewController" bundle:nil] ;
    custLoginController.delegate = self;
    UINavigationController *loginnavBar=[[UINavigationController alloc]initWithRootViewController:custLoginController];
    UIButton *moreButton1 =  [UIButton buttonWithType:UIButtonTypeCustom];
    [moreButton1 setTitle:@"Cancel" forState:UIControlStateNormal];
    moreButton1.titleLabel.font = [UIFont systemFontOfSize:15.0];
    //[moreButton1 setBackgroundImage:[UIImage imageNamed:@"Transparent_Button.png"] forState:UIControlStateNormal];
    [moreButton1 addTarget:self action:@selector(dismissLoginView) forControlEvents:UIControlEventTouchUpInside];
    [moreButton1 setFrame:CGRectMake(250, 5, 60, 32)];
    [loginnavBar.navigationBar addSubview:moreButton1];
    [self.navigationController presentViewController:loginnavBar animated:YES completion:nil];
}

-(void)dismissLoginView
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"Dismiss");
    }];
}
@end
