//
//  NMSplashImageViewController.m
//  NissanMicra
//
//  Created by Rishi on 24/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "NMSplashImageViewController.h"
#import "UIImageView+WebCache.h"

@interface NMSplashImageViewController ()

@end

@implementation NMSplashImageViewController
@synthesize imglink;
@synthesize titlelink;
@synthesize splashimageView;
@synthesize splashWebView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    DebugLog(@"------>>>%@",self.imglink);
    if (imglink!= nil && [imglink hasPrefix:@"http"]) {
        [splashimageView setContentMode:UIViewContentModeScaleAspectFit];
//        [splashimageView setImageWithURL:[NSURL URLWithString:self.imglink]
//                   placeholderImage:nil
//                            success:^(UIImage *image) {
//                                //DebugLog(@"success");
//                            }
//                            failure:^(NSError *error) {
//                                //DebugLog(@"write error %@", error);
//                            }];
        
        [splashimageView setImageWithURL:[NSURL URLWithString:self.imglink] placeholderImage:nil completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType) {

        }];
        splashWebView.backgroundColor = [UIColor clearColor];
        [splashWebView loadHTMLString:titlelink baseURL:nil];
        [splashWebView.scrollView setBounces:NO];
        [splashWebView.scrollView setScrollEnabled:NO];
        [splashWebView setOpaque:NO];
    }else{
        [splashimageView setImage:[UIImage imageNamed:imglink]];
        [splashimageView setContentMode:UIViewContentModeScaleAspectFit];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setSplashimageView:nil];
    [self setSplashWebView:nil];
    [self setSplashWebView:nil];
    [super viewDidUnload];
}

#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
@end
