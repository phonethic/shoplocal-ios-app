//
//  INNetworkHandler.h
//  inorbit
//
//  Created by Rishi on 28/07/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>


// declare our class
@class INNetworkHandler;

// define the protocol for the delegate
@protocol INNetworkDelegate

// define protocol functions that can be used in any class using this delegate
-(void)responseSuccess:(INNetworkHandler*)inNetwork ResponseData:(id)data;
-(void)responseFail:(INNetworkHandler*)inNetwork error:(NSError *)errorHandler;

@end


@interface INNetworkHandler : NSObject {
    
}
// define delegate property
@property(strong)id<INNetworkDelegate>delegate;
@property(copy) NSDictionary *header;
@property(copy) NSString *method;
@property(copy)NSData* body;
@property(assign)int timeOut;
@property(copy) NSDictionary *responseHeader;
@property(nonatomic,strong)NSMutableData *responseData;
// define public functions
/** This is method used to do Service Call with specified URL.
 
 @param url - URL for service call.
 @return nil
 */
-(void)doAsynchronousRequest:(NSString*)url ;
@end
