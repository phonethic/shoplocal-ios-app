//
//  INCustomerLoginViewController.h
//  shoplocal
//
//  Created by Rishi on 20/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "INCustomerSignUpViewController.h"
#import "INTermsViewController.h"
#import "INCustomerVerifyViewController.h"

#define LOGIN_TYPE_SHOPLOCAL 1
#define LOGIN_TYPE_FB 2

@protocol INCustomerLoginViewControllerDelegate <NSObject>
- (void)dismissLoginView;
@end

@interface INCustomerLoginViewController : UIViewController <INCustomerSignUPViewControllerDelegate,termsDelegate,INCustomerVerifyViewControllerDelegate> {
    INCustomerSignUpViewController *signupController;
    INCustomerSignUpViewController *forgotpassController;
    UINavigationController *signupControllernavBar;
    MBProgressHUD *hud;
    
    BOOL isOpenedTermsViewWhileSignUp;
    BOOL isRetryingToAcceptTerms;
    
    BOOL isSendingFBLoginRequest;
    
    NSURLConnection *connectionRegisterNumber;
    NSMutableData *responseAsyncData;
    int status;
}
@property (weak, nonatomic) id<INCustomerLoginViewControllerDelegate>  delegate;

@property (readwrite, nonatomic) NSInteger loginType;

@property (strong, nonatomic) IBOutlet UITextField *usernameText;
@property (strong, nonatomic) IBOutlet UITextField *passwordtext;
@property (strong, nonatomic) IBOutlet UIButton *loginBtn;
@property (strong, nonatomic) IBOutlet UIButton *signupBtn;
@property (strong, nonatomic) IBOutlet UIButton *forgotBtn;

@property (strong, nonatomic) IBOutlet UITextField *codeText;

@property (strong, nonatomic) IBOutlet UILabel *lblHeader;
@property (strong, nonatomic) IBOutlet UIView *loginView;
@property (strong, nonatomic) IBOutlet UIView *signUpView;
@property (strong, nonatomic) IBOutlet UIButton *loginQuesBtn;
@property (strong, nonatomic) IBOutlet UILabel *lblfirsttimeuser;

@property (strong,nonatomic) NSMutableArray *countryCodeArray;
@property (strong,nonatomic) NSMutableArray *countryCodePickerArray;

@property (strong, nonatomic) IBOutlet UIButton *termsBtn;
@property (strong, nonatomic) IBOutlet UIButton *privacyBtn;

@property (strong, nonatomic) IBOutlet UIView *infoView;
@property (strong, nonatomic) IBOutlet UILabel *lblinfoHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblinfoContent;
@property (strong, nonatomic) IBOutlet UIButton *infoOKBtn;
@property (strong, nonatomic) IBOutlet UIButton *infoPrivacyBtn;
@property (strong, nonatomic) IBOutlet UIButton *infoBtn;
@property (strong, nonatomic) IBOutlet UITextView *txtViewinfoContent;

@property (strong, nonatomic) IBOutlet UIView *loginView1;
@property (strong, nonatomic) IBOutlet UIButton *fbLoginBtn;
@property (strong, nonatomic) IBOutlet UIButton *signUpShoplocalBtn;
@property (strong, nonatomic) IBOutlet UIImageView *customerImageView;

- (IBAction)fbLoginBtnPressed:(id)sender;
- (IBAction)signUpShoplocalBtnPressed:(id)sender;


- (IBAction)infoOKBtnPressed:(id)sender;
- (IBAction)infoPrivacyBtnPressed:(id)sender;

- (IBAction)loginQuesBtnPressed:(id)sender;
- (IBAction)forgotBtnPressed:(id)sender;
- (IBAction)loginBtnPressed:(id)sender;
- (IBAction)signupBtnPressed:(id)sender;
- (IBAction)termsBtnPressed:(id)sender;
- (IBAction)privacyBtnPressed:(id)sender;
- (IBAction)infoBtnPressed:(id)sender;
@end
