//
//  INSearchViewController.m
//  shoplocal
//
//  Created by Rishi on 12/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import <time.h>
#import <xlocale.h>
#import <Social/Social.h>

#import "INSearchViewController.h"
#import "INAppDelegate.h"
#import "INAllStoreObj.h"
//#import "INOffersViewController.h"
//#import "INDetailViewController.h"
//#import "INShopViewController.h"
//#import "INServicesViewController.h"
//#import "INEntertainmentViewController.h"
//#import "INFoodViewController.h"
#import "CommonCallback.h"
//#import "INKidViewController.h"
#import "INBroadcastDetailObj.h"
#import "INCustBroadCastDetailsViewController.h"
#import "constants.h"
#import "INCategoryViewController.h"
#import "INStoreDetailViewController.h"
#import "UIImageView+WebCache.h"
#import "CellWithImageView.h"
#import "CellWithoutImageView.h"

#import "FacebookOpenGraphAPI.h"
#import "FacebookShareAPI.h"

#define SEARCH_CATEGORY [NSString stringWithFormat:@"%@%@%@place_api/place_category",LIVE_SERVER,URL_PREFIX,API_VERSION]
#define THUMBNAIL_LINK(IMG_LINK) [NSString stringWithFormat:@"%@%@%@",LIVE_SERVER,URL_PREFIX,IMG_LINK]

#define STORE_SEARCH_WITHOUT_OFFER(TEXT,LAT,LONG,DIST,LOCALITY,PAGE) [NSString stringWithFormat:@"%@%@%@place_api/search?search=%@&latitude=%f&longitude=%f&distance=%d&area_id=%@&page=%d&count=50",LIVE_SERVER,URL_PREFIX,API_VERSION,TEXT,LAT,LONG,DIST,LOCALITY,PAGE]
#define STORE_SEARCH_WITH_OFFER(TEXT,LAT,LONG,DIST,LOCALITY,PAGE) [NSString stringWithFormat:@"%@%@%@place_api/search?search=%@&latitude=%f&longitude=%f&distance=%d&area_id=%@&page=%d&count=50&offer=1",LIVE_SERVER,URL_PREFIX,API_VERSION,TEXT,LAT,LONG,DIST,LOCALITY,PAGE]

//http://stage.phonethics.in/shoplocal/api-3/broadcast_api/search?search=jeans&page=1&count=20
#define BROADCAST_SEARCH_WITHOUT_OFFER(TEXT,LOCALITY,PAGE)  [NSString stringWithFormat:@"%@%@%@broadcast_api/search?search=%@&area_id=%@&page=%d&count=50",LIVE_SERVER,URL_PREFIX,API_VERSION,TEXT,LOCALITY,PAGE]

#define BROADCAST_SEARCH_WITH_OFFER(TEXT,LOCALITY,PAGE)  [NSString stringWithFormat:@"%@%@%@broadcast_api/search?search=%@&area_id=%@&page=%d&count=50&offer=1",LIVE_SERVER,URL_PREFIX,API_VERSION,TEXT,LOCALITY,PAGE]

#define CALL_STORE [NSString stringWithFormat:@"%@%@broadcast_api/call",URL_PREFIX,API_VERSION]


@interface INSearchViewController ()
@property(strong) NSDictionary *splashJson;
@end

@implementation INSearchViewController
@synthesize searchArray;
@synthesize searchdict;
@synthesize searchTableView;
@synthesize countlbl;
@synthesize totalPage;
@synthesize totalRecord;
@synthesize currentPage;
@synthesize searchType;
@synthesize storeSearchBar;
@synthesize textPull, textRelease, textLoading, refreshHeaderView, refreshLabel, refreshArrow, refreshSpinner;
@synthesize countHeaderView,lblpullToRefresh;
@synthesize textToSearch;
@synthesize showCancelBarBtn;
@synthesize isStoreSearch;
@synthesize currentSelectedRow;
@synthesize searchView,searchTextField,searchCancelBtn,searchZoomOutBtn;
@synthesize hideBackBarBtn;
@synthesize callModalViewBackView,callModalView,lblcallModalHeader,lblcallModalHeaderSeperator,callModalTableView,btnCancelcallModal;
@synthesize telArray,selectedPlaceId;
@synthesize selectedPlaceName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
         [self setupStrings];
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:TRUE];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:TRUE];
    if (isSearchOn) {
        [searchTextField becomeFirstResponder];
    }
    [[self navigationController] setNavigationBarHidden:YES animated:NO];

}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:FALSE];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:FALSE];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if ([searchTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [searchTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    [self setupMenuBarButtonItems];

    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        storeSearchBar.barTintColor = DEFAULT_COLOR;
        [storeSearchBar setTranslucent:NO];
    }
    storeSearchBar.autocorrectionType = UITextAutocorrectionTypeNo;
//    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back"
//                                                             style:UIBarButtonItemStylePlain
//                                                            target:nil
//                                                            action:nil];
//    [[self navigationItem] setBackBarButtonItem:back];
    
    if (hideBackBarBtn) {
        self.navigationItem.hidesBackButton = YES;
        isSearchOn = YES;
        [searchTextField becomeFirstResponder];
    }else{
        self.navigationItem.hidesBackButton = NO;
    }
    
//    if (showCancelBarBtn) {
//        self.navigationItem.hidesBackButton = YES;
//    }else{
//        self.navigationItem.hidesBackButton = NO;
//    }
    
    [self setUI];
    [countHeaderView setHidden:TRUE];
    
    keyboardHeight = 216;
    searchTableView.frame = CGRectMake(0, CGRectGetMaxY(searchView.frame), searchTableView.frame.size.width, self.view.frame.size.height-(CGRectGetMaxY(searchView.frame)+keyboardHeight));
    
    //[self addHUDView];
    [self addPullToRefreshHeader];


    if (!isStoreSearch) {
        [searchTableView registerNib:[UINib nibWithNibName:@"CellWithImageView" bundle:nil] forCellReuseIdentifier:kWithImageCellIdentifier];
        [searchTableView registerNib:[UINib nibWithNibName:@"CellWithoutImageView" bundle:nil] forCellReuseIdentifier:kWithoutImageCellIdentifier];
    }
    if ([IN_APP_DELEGATE checkFileExits:@"search"]) {
        if(([INUserDefaultOperations getDateDifferenceInHours:[INUserDefaultOperations getSearchSyncDate]]) >= 24)
        {
            searchdict = [[NSMutableDictionary alloc] init];
            [searchdict setObject:@"Offers" forKey:@"0"];
            if ([IN_APP_DELEGATE networkavailable]) {
                [self sendSearchCategoryRequest];
            }
        } else {
            searchdict = [[NSMutableDictionary alloc] initWithDictionary:[IN_APP_DELEGATE getTextFromFile:@"search"]];
        }
    } else {
        searchdict = [[NSMutableDictionary alloc] init];
        [searchdict setObject:@"Offers" forKey:@"0"];
         if ([IN_APP_DELEGATE networkavailable]) {
             [self sendSearchCategoryRequest];
         }
    }
    if ([IN_APP_DELEGATE networkavailable]) {
        if (textToSearch != nil && ![textToSearch isEqualToString:@""]) {
            storeSearchBar.text = textToSearch;
            if (isStoreSearch) {
                NSDictionary *catgParams = [NSDictionary dictionaryWithObjectsAndKeys:@"All",@"Category_Name", storeSearchBar.text,@"Search_Text", nil];
                DebugLog(@"catgParams-%@-",catgParams);
                [INEventLogger logEvent:@"Search_StoreCategory" withParams:catgParams];
                
                NSString* encodeUrlString = [STORE_SEARCH_WITHOUT_OFFER(textToSearch,[INUserDefaultOperations getLatitude],[INUserDefaultOperations getLongitude],[INUserDefaultOperations getCustomerDistance],[IN_APP_DELEGATE getActiveAreaIdFromAreaTable],self.currentPage) stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                self.searchType = 0; //Set search type(without offer)
                DebugLog(@"%@",encodeUrlString);
                [self sendSearchStoreRequest:encodeUrlString page:self.currentPage];
            }else{
                NSDictionary *catgParams = [NSDictionary dictionaryWithObjectsAndKeys:@"All",@"Category_Name", storeSearchBar.text,@"Search_Text", nil];
                DebugLog(@"catgParams-%@-",catgParams);
                [INEventLogger logEvent:@"Search_OffersCategory" withParams:catgParams];

                
                NSString* encodeUrlString = [BROADCAST_SEARCH_WITHOUT_OFFER(textToSearch,[IN_APP_DELEGATE getActiveAreaIdFromAreaTable],self.currentPage)stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                self.searchType = 0; //Set search type(without offer)
                DebugLog(@"%@",encodeUrlString);
                [self sendSearchBroadCastRequest:encodeUrlString page:self.currentPage];
            }
        }
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)
                                                 name:@"UIKeyboardWillShowNotification"
                                               object:nil];
}

#pragma set Navigation barbuttons callback
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
        { if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                if (!hideBackBarBtn) {
                    self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
                }
            }
            if (showCancelBarBtn) {
                self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            }
        }
            break;
        case MFSideMenuStateLeftMenuOpen:
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
        {
            if (showCancelBarBtn) {
                self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            }
        }
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:MENU_TITLE style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
//    return [[UIBarButtonItem alloc]
//            initWithTitle:MENU_TITLE style:UIBarButtonItemStyleBordered
//            target:self.navigationController.sideMenu
//            action:@selector(toggleRightSideMenu)];
//    return [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelBarBtnClicked:)];
    return nil;
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelBarBtnClicked:(id)sender {
    [UIView  transitionWithView:self.navigationController.view duration:0.5  options:UIViewAnimationOptionTransitionCrossDissolve
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         [self.navigationController popViewControllerAnimated:NO];
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:nil];
}

- (void) keyboardWillShow:(NSNotification *)note {
    if ([self.view window]) //means is visible
    {    //do something
        NSDictionary *userInfo = [note userInfo];
        CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        DebugLog(@"userInfo %@", userInfo);
        
        DebugLog(@"Keyboard Height: %f Width: %f", kbSize.height, kbSize.width);
        
        keyboardHeight = kbSize.height;
        if (isSearchOn) {
            [UIView animateWithDuration:0.3 animations:^{
                searchTableView.frame = CGRectMake(0, CGRectGetMaxY(searchView.frame), searchTableView.frame.size.width, self.view.frame.size.height-(CGRectGetMaxY(searchView.frame)+keyboardHeight));
            }];
        }
    }else{
        //return
    }
}

#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void)setUI
{
    self.searchTableView.backgroundColor = [UIColor clearColor];
    countHeaderView.backgroundColor =   [UIColor clearColor];
    countHeaderView.clipsToBounds   = YES;
    
    lblpullToRefresh.font               =   DEFAULT_FONT(15.0);
    lblpullToRefresh.backgroundColor    =   LIGHT_GREEN_COLOR_WITH_ALPHA(0.5);
    lblpullToRefresh.textColor          =   [UIColor whiteColor];
    
    countlbl.font               =   DEFAULT_FONT(15.0);
    countlbl.backgroundColor    =   LIGHT_GREEN_COLOR_WITH_ALPHA(0.5);
    countlbl.textColor          =   [UIColor whiteColor];
    
    searchView.backgroundColor          =       DEFAULT_COLOR;
    searchTextField.font                =       DEFAULT_FONT(20.0);
    searchTextField.backgroundColor     =       [UIColor whiteColor];
    searchTextField.textColor           =       DEFAULT_COLOR;
    searchTextField.layer.cornerRadius  =       8.0;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        searchTextField.tintColor           = DEFAULT_COLOR;
    }
    searchTextField.autocorrectionType  = UITextAutocorrectionTypeYes;
    searchTextField.spellCheckingType   = UITextSpellCheckingTypeYes;
    searchCancelBtn.backgroundColor     =    [UIColor clearColor];
    searchCancelBtn.titleLabel.font     =    DEFAULT_FONT(15.0);
//    [searchCancelBtn setTitle:@"Cancel" forState:UIControlStateNormal];
//    [searchCancelBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    searchZoomOutBtn.backgroundColor     =    [UIColor clearColor];
    searchZoomOutBtn.titleLabel.font     =    DEFAULT_FONT(17.0);
    [searchZoomOutBtn setTitle:@"Search" forState:UIControlStateNormal];
    [searchZoomOutBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    searchZoomOutBtn.hidden = YES;

    //callModalView = [CommonCallback setViewPropertiesWithRoundedCorner:callModalView];
    callModalView.backgroundColor =  [UIColor whiteColor];
    callModalView.layer.shadowColor      = [UIColor blackColor].CGColor;
    callModalView.layer.shadowOffset     = CGSizeMake(1, 1);
    callModalView.layer.shadowOpacity    = 1.0;
    callModalView.layer.shadowRadius     = 10.0;
    [callModalViewBackView setHidden:YES];
    [callModalView setHidden:YES];
    
    lblcallModalHeader.textAlignment     = NSTextAlignmentCenter;
    lblcallModalHeader.backgroundColor   = [UIColor clearColor];
    lblcallModalHeader.font              = DEFAULT_BOLD_FONT(20);
    lblcallModalHeader.textColor         = BROWN_COLOR;
    //    lblcallModalHeader.adjustsFontSizeToFitWidth = YES;
    lblcallModalHeader.minimumScaleFactor = 15;
    lblcallModalHeader.text              = @"Select number";
    lblcallModalHeader.numberOfLines     = 2;
    
    lblcallModalHeaderSeperator.backgroundColor   = DEFAULT_COLOR;
    callModalTableView.backgroundColor = [UIColor whiteColor];
    if ([callModalTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [callModalTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    btnCancelcallModal.backgroundColor = [UIColor clearColor];
}

-(void)addHUDView
{
    hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:hud];
	hud.dimBackground = NO;
    hud.labelText = @"Loading";
	hud.detailsLabelText = @"Please wait ...";
    [hud hide:NO];
}

-(void)removeHUDView
{
    [hud removeFromSuperview];
    hud = nil;
}

-(void)sendSearchCategoryRequest
{
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    DebugLog(@"%@",SEARCH_CATEGORY);
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:SEARCH_CATEGORY]];
//    [urlRequest setCachePolicy:NSURLRequestReturnCacheDataElseLoad];
//    DebugLog(@"search=%@",urlRequest.URL);
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"%@",self.splashJson);
                                                        if(self.splashJson  != nil) {
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSArray *jsonArray = [self.splashJson  objectForKey:@"data"];
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *objdict = [jsonArray  objectAtIndex:index];
                                                                    [searchdict setObject:[objdict valueForKey:@"label"] forKey:[objdict valueForKey:@"id"]];
                                                                }
                                                                [IN_APP_DELEGATE writeToTextFile:searchdict name:@"search"];
                                                                [INUserDefaultOperations setSearchSyncDate];
                                                            } else {
                                                                if([self.splashJson  objectForKey:@"message"] != [NSNull null] && ![[self.splashJson objectForKey:@"message"] isEqualToString:@""]){
                                                                    NSString *message = [self.splashJson objectForKey:@"message"];
                                                                    [INUserDefaultOperations showSIAlertView:message];
                                                                }
                                                            }
                                                        }
                                                        
                                                        DebugLog(@"searchdict -%@-",searchdict);
                                                        [searchTableView reloadData];

                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        [INUserDefaultOperations showAlert:[NSString stringWithFormat:@"%@",[error localizedDescription]]];
                                                    }];
    
    
    
    [operation start];
}

//Store Search
-(void)sendSearchStoreRequest:(NSString *)urlString  page:(int)pageNumber
{
    //[hud show:YES];
    DebugLog(@"%d",pageNumber);
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        //[hud hide:YES];
//                                                        searchTableView.frame = CGRectMake(0, CGRectGetMaxY(countHeaderView.frame), searchTableView.frame.size.width, self.view.frame.size.height-CGRectGetMaxY(countHeaderView.frame));
//                                                        [countHeaderView setHidden:FALSE];
                                                        [CommonCallback hideProgressHud];
                                                        if(isLoading)
                                                        {
                                                            [self stopLoading];
                                                        }
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"%@",self.splashJson);
                                                        if(self.splashJson  != nil) {
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSDictionary *maindict = [self.splashJson  objectForKey:@"data"];
                                                                self.totalPage = [[maindict  objectForKey:@"total_page"] integerValue];
                                                                self.totalRecord = [[maindict  objectForKey:@"total_record"] integerValue];
                                                                NSArray* jsonArray = [maindict  objectForKey:@"record"];
                                                                if (jsonArray.count > 0) {
                                                                    if (searchArray == nil) {
                                                                        searchArray = [[NSMutableArray alloc] init];
                                                                    }else{
                                                                        if(pageNumber <= 1)
                                                                        {
                                                                            [searchArray removeAllObjects];
                                                                        }
                                                                    }
                                                                }
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *objdict = [jsonArray  objectAtIndex:index];
                                                                    //DebugLog(@"\n\n%@\n\n",objdict);
                                                                    storeObj = [[INAllStoreObj alloc] init];
                                                                    storeObj.area = [objdict objectForKey:@"area"];
                                                                    storeObj.building = [objdict objectForKey:@"building"];
                                                                    storeObj.city = [objdict objectForKey:@"city"];
                                                                    storeObj.description = [objdict objectForKey:@"description"];
                                                                    storeObj.email = [objdict objectForKey:@"email"];
                                                                    storeObj.has_offer = [objdict objectForKey:@"has_offer"];
                                                                    storeObj.ID = [[objdict objectForKey:@"id"] integerValue];
                                                                    storeObj.image_url = [objdict objectForKey:@"image_url"];
                                                                    storeObj.landmark = [objdict objectForKey:@"landmark"];
                                                                    storeObj.name = [objdict objectForKey:@"name"];
                                                                    storeObj.place_parent = [[objdict objectForKey:@"place_parent"] integerValue];
                                                                    storeObj.street = [objdict objectForKey:@"street"];
                                                                    storeObj.title = [objdict objectForKey:@"title"];
                                                                    storeObj.website = [objdict objectForKey:@"website"];
                                                                    storeObj.mob_no1 = [objdict objectForKey:@"mob_no1"];
                                                                    storeObj.mob_no2 = [objdict objectForKey:@"mob_no2"];
                                                                    storeObj.mob_no3 = [objdict objectForKey:@"mob_no3"];
                                                                    storeObj.tel_no1 = [objdict objectForKey:@"tel_no1"];
                                                                    storeObj.tel_no2 = [objdict objectForKey:@"tel_no2"];
                                                                    storeObj.tel_no3 = [objdict objectForKey:@"tel_no3"];
                                                                    [searchArray addObject:storeObj];
                                                                    storeObj = nil;
                                                                }
                                                            } else {
                                                                if([self.splashJson  objectForKey:@"message"] != [NSNull null] && ![[self.splashJson objectForKey:@"message"] isEqualToString:@""]){
                                                                    NSString *message = [self.splashJson objectForKey:@"message"];
                                                                    [INUserDefaultOperations showSIAlertView:message];
                                                                }
                                                                 //[countHeaderView setHidden:TRUE];
                                                                
                                                               // searchTableView.frame = CGRectMake(0, CGRectGetMaxY(storeSearchBar.frame), searchTableView.frame.size.width, self.view.frame.size.height-CGRectGetMaxY(storeSearchBar.frame));
                                                            }
                                                            DebugLog(@"%d",[searchArray count]);
//                                                            if([searchArray count] > 0)
//                                                            {
                                                                [searchTableView reloadData];
                                                                [self setCountLabel];
                                                                [storeSearchBar resignFirstResponder];
                                                            [searchTextField resignFirstResponder];
                                                            searchTableView.frame = CGRectMake(0, CGRectGetMaxY(searchView.frame), searchTableView.frame.size.width, self.view.frame.size.height-CGRectGetMaxY(searchView.frame));


//                                                            }
                                                            isAdding = NO;
                                                            if([searchArray count] > 0)
                                                            {
                                                                [FBAppEvents logEvent:FBAppEventNameSearched
                                                                           parameters:@{ @"ContentType"   : @"Store",
                                                                                         @"SearchString"  : searchTextField.text,
                                                                                         @"Area"          : [IN_APP_DELEGATE getActivePlaceNameFromAreaTable],
                                                                                         @"Success"       : @"TRUE" } ];
                                                            } else {
                                                                [FBAppEvents logEvent:FBAppEventNameSearched
                                                                           parameters:@{ @"ContentType"   : @"Store",
                                                                                         @"SearchString"  : searchTextField.text,
                                                                                         @"Area"          : [IN_APP_DELEGATE getActivePlaceNameFromAreaTable],
                                                                                         @"Success"       : @"FALSE" } ];
                                                            }
                                                        }
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                        [av show];
                                                        isAdding = NO;
                                                        if(isLoading)
                                                        {
                                                            [self stopLoading];
                                                        }
                                                    }];
    
    
    
    [operation start];
}

//BroadCast Search
-(void)sendSearchBroadCastRequest:(NSString *)urlString  page:(int)pageNumber
{
    //[hud show:YES];
    DebugLog(@"%d",pageNumber);
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        //[hud hide:YES];
//                                                        searchTableView.frame = CGRectMake(0, CGRectGetMaxY(countHeaderView.frame), searchTableView.frame.size.width, self.view.frame.size.height-CGRectGetMaxY(countHeaderView.frame));
//                                                        [countHeaderView setHidden:FALSE];
                                                        [CommonCallback hideProgressHud];
                                                        if(isLoading)
                                                        {
                                                            [self stopLoading];
                                                        }
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"%@",self.splashJson);
                                                        if(self.splashJson  != nil) {
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSDictionary *maindict = [self.splashJson  objectForKey:@"data"];
                                                                self.totalPage = [[maindict  objectForKey:@"total_page"] integerValue];
                                                                self.totalRecord = [[maindict  objectForKey:@"total_record"] integerValue];
                                                                NSArray* jsonArray = [maindict  objectForKey:@"record"];
                                                                if (jsonArray.count > 0) {
                                                                    if (searchArray == nil) {
                                                                        searchArray = [[NSMutableArray alloc] init];
                                                                    }else{
                                                                        if(pageNumber <= 1)
                                                                        {
                                                                            [searchArray removeAllObjects];
                                                                        }
                                                                    }
                                                                }
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *objdict = [jsonArray  objectAtIndex:index];
                                                                    //DebugLog(@"\n\n%@\n\n",objdict);
                                                                    INBroadcastDetailObj *broadcastObj = [[INBroadcastDetailObj alloc] init];
                                                                    broadcastObj.ID = [[objdict objectForKey:@"post_id"] integerValue];
                                                                    broadcastObj.place_ID = [[objdict objectForKey:@"id"] integerValue];
                                                                    broadcastObj.areaID = [objdict objectForKey:@"area_id"];
                                                                    broadcastObj.name = [objdict objectForKey:@"name"];
                                                                    broadcastObj.title = [objdict objectForKey:@"title"];
                                                                    broadcastObj.description = [objdict objectForKey:@"description"];
                                                                    broadcastObj.date = [objdict objectForKey:@"date"];
                                                                    broadcastObj.offerdate = [objdict objectForKey:@"offer_date_time"];
                                                                    broadcastObj.distance = [objdict objectForKey:@"distance"];
                                                                    broadcastObj.latitude = [objdict objectForKey:@"latitude"];
                                                                    broadcastObj.longitude = [objdict objectForKey:@"longitude"];
                                                                    broadcastObj.tel_no1 = [objdict objectForKey:@"tel_no1"];
                                                                    broadcastObj.tel_no2 = [objdict objectForKey:@"tel_no2"];
                                                                    broadcastObj.tel_no3 = [objdict objectForKey:@"tel_no3"];
                                                                    broadcastObj.mob_no1 = [objdict objectForKey:@"mob_no1"];
                                                                    broadcastObj.mob_no2 = [objdict objectForKey:@"mob_no2"];
                                                                    broadcastObj.mob_no3 = [objdict objectForKey:@"mob_no3"];
                                                                    broadcastObj.image_url1 = [objdict objectForKey:@"image_url1"];
                                                                    broadcastObj.image_url2 = [objdict objectForKey:@"image_url2"];
                                                                    broadcastObj.image_url3 = [objdict objectForKey:@"image_url3"];
                                                                    broadcastObj.thumb_url1 = [objdict objectForKey:@"thumb_url1"];
                                                                    broadcastObj.thumb_url2 = [objdict objectForKey:@"thumb_url2"];
                                                                    broadcastObj.thumb_url3 = [objdict objectForKey:@"thumb_url3"];
                                                                    broadcastObj.total_like = [objdict objectForKey:@"total_like"];
                                                                    broadcastObj.total_share = [objdict objectForKey:@"total_share"];
                                                                    broadcastObj.is_offered = [objdict objectForKey:@"is_offered"];
                                                                    broadcastObj.type = [objdict objectForKey:@"type"];
                                                                    broadcastObj.verified = [objdict objectForKey:@"verified"];
                                                                    [searchArray addObject:broadcastObj];
                                                                    broadcastObj = nil;
                                                                }
                                                            } else {
                                                                if([self.splashJson  objectForKey:@"message"] != [NSNull null] && ![[self.splashJson objectForKey:@"message"] isEqualToString:@""]){
                                                                    NSString *message = [self.splashJson objectForKey:@"message"];
                                                                    [INUserDefaultOperations showSIAlertView:message];
                                                                }
                                                                
//                                                                [countHeaderView setHidden:TRUE];
//                                                                searchTableView.frame = CGRectMake(0, CGRectGetMaxY(storeSearchBar.frame), searchTableView.frame.size.width, self.view.frame.size.height-CGRectGetMaxY(storeSearchBar.frame));
                                                            }
                                                            DebugLog(@"%d",[searchArray count]);
                                                            [searchTableView reloadData];
                                                            [self setCountLabel];
                                                            [storeSearchBar resignFirstResponder];
                                                            [searchTextField resignFirstResponder];
                                                            searchTableView.frame = CGRectMake(0, CGRectGetMaxY(searchView.frame), searchTableView.frame.size.width, self.view.frame.size.height-CGRectGetMaxY(searchView.frame));

                                                            isAdding = NO;
                                                            
                                                            if([searchArray count] > 0)
                                                            {
                                                                [FBAppEvents logEvent:FBAppEventNameSearched
                                                                           parameters:@{ @"ContentType"   : @"Offers",
                                                                                         @"SearchString"  : searchTextField.text,
                                                                                         @"Area"          : [IN_APP_DELEGATE getActivePlaceNameFromAreaTable],
                                                                                         @"Success"       : @"TRUE" } ];
                                                            } else {
                                                                [FBAppEvents logEvent:FBAppEventNameSearched
                                                                           parameters:@{ @"ContentType"   : @"Offers",
                                                                                         @"SearchString"  : searchTextField.text,
                                                                                         @"Area"          : [IN_APP_DELEGATE getActivePlaceNameFromAreaTable],
                                                                                         @"Success"       : @"FALSE" } ];
                                                            }
                                                        }
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                        [av show];
                                                        isAdding = NO;
                                                        if(isLoading)
                                                        {
                                                            [self stopLoading];
                                                        }
                                                    }];
    
    
    
    [operation start];
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
     DebugLog(@"search=%@",searchBar.text);
    if([searchArray count] > 0)
    {
        [searchArray removeAllObjects];
        searchArray = nil;
        self.searchType = 0;
        self.currentPage = 0;
        self.totalPage = 0;
        self.totalRecord = 0;
        [self setCountLabel];
        [countHeaderView setHidden:TRUE];
        searchTableView.frame = CGRectMake(0, CGRectGetMaxY(storeSearchBar.frame), searchTableView.frame.size.width, self.view.frame.size.height-CGRectGetMaxY(storeSearchBar.frame));
    }
    [searchTableView reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    DebugLog(@"search=%@",searchBar.text);

    [searchArray removeAllObjects];
    searchArray = nil;
    self.currentPage = 1;
    UITableViewCell *cell = [searchTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    
    NSDictionary *catgParams = [NSDictionary dictionaryWithObjectsAndKeys:@"All",@"Category_Name", storeSearchBar.text,@"Search_Text", nil];
    DebugLog(@"catgParams-%@-",catgParams);
    if (isStoreSearch) {
        [INEventLogger logEvent:@"Search_StoreCategory" withParams:catgParams];
    }else{
        [INEventLogger logEvent:@"Search_OffersCategory" withParams:catgParams];
    }
    
    if(cell.accessoryType == UITableViewCellAccessoryCheckmark)
    {
        if (isStoreSearch) {
            NSString* encodeUrlString = [STORE_SEARCH_WITH_OFFER(searchBar.text,[INUserDefaultOperations getLatitude],[INUserDefaultOperations getLongitude],[INUserDefaultOperations getCustomerDistance],[IN_APP_DELEGATE getActiveAreaIdFromAreaTable],self.currentPage) stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            DebugLog(@"%@",encodeUrlString);
            self.searchType = 1;   //Set search type(with offer)
            [self sendSearchStoreRequest:encodeUrlString page:self.currentPage];
        }else{
            NSString* encodeUrlString = [BROADCAST_SEARCH_WITH_OFFER(searchBar.text,[IN_APP_DELEGATE getActiveAreaIdFromAreaTable],self.currentPage)stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            self.searchType = 1; //Set search type(with offer)
            DebugLog(@"%@",encodeUrlString);
            [self sendSearchBroadCastRequest:encodeUrlString page:self.currentPage];
        }
    } else {
        if (isStoreSearch) {
            NSString* encodeUrlString = [STORE_SEARCH_WITHOUT_OFFER(searchBar.text,[INUserDefaultOperations getLatitude],[INUserDefaultOperations getLongitude],[INUserDefaultOperations getCustomerDistance],[IN_APP_DELEGATE getActiveAreaIdFromAreaTable],self.currentPage) stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            self.searchType = 0; //Set search type(without offer)
            DebugLog(@"%@",encodeUrlString);
            [self sendSearchStoreRequest:encodeUrlString page:self.currentPage];
        }else{
            NSString* encodeUrlString = [BROADCAST_SEARCH_WITHOUT_OFFER(searchBar.text,[IN_APP_DELEGATE getActiveAreaIdFromAreaTable],self.currentPage)stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            self.searchType = 0; //Set search type(without offer)
            DebugLog(@"%@",encodeUrlString);
            [self sendSearchBroadCastRequest:encodeUrlString page:self.currentPage];
        }
    }
}


#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:callModalTableView]) {
        return 80;
    }else{
        if ([searchArray count] > 0 && searchArray != nil) {
            if (isStoreSearch) {
                return 100;
            }else{
                INBroadcastDetailObj *tempbroadObj  = [searchArray objectAtIndex:indexPath.row];
                if(tempbroadObj.image_url1 != (NSString*)[NSNull null] && ![tempbroadObj.image_url1 isEqualToString:@""]) {
                    return kWithImageCellHeight;
                }
                else {
                    return kWithoutImageCellHeight;
                }
            }
        }
        else{
            return 40;
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:callModalTableView]) {
        return [telArray count];
    }else{
        if ([searchArray count] > 0 && searchArray != nil) {
            return [searchArray count];
        }
        else{
            return [[searchdict allKeys] count];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:callModalTableView]) {
        static NSString *CellIdentifier = @"CallCell";
        UIImageView *thumbImg;
        UILabel *lblNumber;
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.backgroundColor = [UIColor whiteColor];
            
            thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(10,15, 50, 50)];
            thumbImg.tag = 1111;
            thumbImg.contentMode = UIViewContentModeScaleToFill;
            thumbImg.image = [UIImage imageNamed:@"call_brown.png"];
            [cell.contentView addSubview:thumbImg];
            
            lblNumber = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame),0, 210, 80)];
            lblNumber.text = @"";
            lblNumber.tag = 2222;
            lblNumber.textColor             = BROWN_COLOR;
            lblNumber.highlightedTextColor  = [UIColor whiteColor];
            lblNumber.font                  = DEFAULT_FONT(23);
            lblNumber.textAlignment         = NSTextAlignmentLeft;
            lblNumber.backgroundColor       = [UIColor clearColor];
            [cell.contentView addSubview:lblNumber];
            
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = BROWN_OFFWHITE_COLOR;
            cell.selectedBackgroundView = bgColorView;
        }
        lblNumber        = (UILabel *)[cell.contentView viewWithTag:2222];
        DebugLog(@"lblNumber %@",[telArray objectAtIndex:indexPath.row]);
        lblNumber.text   = [telArray objectAtIndex:indexPath.row];
        return cell;
    }else{
    if (!isStoreSearch && [searchArray count] > 0 && searchArray != nil) {
        searchTableView.backgroundColor = [UIColor clearColor];
        searchTableView.separatorColor = [UIColor clearColor];
        searchTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        //DebugLog(@"---%@----",tempfeedObj.date);
        DebugLog(@"---%@----",searchArray);
        if ([searchArray count] <= 0)
            return nil;
        
        INBroadcastDetailObj *tempbroadcastObj  = [searchArray objectAtIndex:indexPath.row];
         DebugLog(@"---%@----",tempbroadcastObj.date);
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        NSDate *myDate = [[NSDate alloc] init];
        myDate = [dateFormatter dateFromString:tempbroadcastObj.date];
        DebugLog(@"MyDate is: %@", myDate);
        
        
        //char	*strptime_l(const char * __restrict, const char * __restrict, struct tm * __restrict, locale_t)
//        const char *str = [tempbroadcastObj.date UTF8String];
//        const char *fmt = [@"%Y-%m-%d %H:%M:%S" UTF8String];
//        struct tm timeinfo;
//        memset(&timeinfo, 0, sizeof(timeinfo));
//        char *ret = strptime_l(str, fmt, &timeinfo, NULL);
//        
//        NSDate *myDate = nil;
//        
//        if (ret) {
//            time_t time = mktime(&timeinfo);
//            
//            myDate = [NSDate dateWithTimeIntervalSince1970:time];
//            DebugLog(@"MyDate is: %@", myDate);
//        }
        
        
        NSDateFormatter *dayformatter = [[NSDateFormatter alloc] init];
        [dayformatter setDateFormat:@"dd"];
        NSString *dayFromDate = [dayformatter stringFromDate:myDate];
        DebugLog(@"Myday is: %@", dayFromDate);
        
        NSDateFormatter *monthformatter = [[NSDateFormatter alloc] init];
        [monthformatter setDateFormat:@"MMM"];
        NSString *monthFromDate = [monthformatter stringFromDate:myDate];
        DebugLog(@"MyMonth is: %@", monthFromDate);
        
        NSDateFormatter *timeformatter = [[NSDateFormatter alloc] init];
        [timeformatter setDateFormat:@"hh:mm a"];
        NSString *timeFromDate = [timeformatter stringFromDate:myDate];
        DebugLog(@"timeFromDate is: %@", timeFromDate);
        
        if(tempbroadcastObj.image_url1 != (NSString*)[NSNull null] && ![tempbroadcastObj.image_url1 isEqualToString:@""]) {
            NSString *cellIdentifier = kWithImageCellIdentifier;
            CellWithImageView *cell = (CellWithImageView *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            cell.lblTitle.text          = tempbroadcastObj.title;
            cell.lblLeftTitle.text      = tempbroadcastObj.name;
            cell.lblDescription.text    = tempbroadcastObj.description;
            cell.lblLikeCount.text      = tempbroadcastObj.total_like;
            cell.lblShareCount.text     = tempbroadcastObj.total_share;
            cell.lblTimeStamp.text      = [NSString stringWithFormat:@"%@ %@    %@",dayFromDate,monthFromDate,timeFromDate];
            if (tempbroadcastObj.is_offered != nil && [tempbroadcastObj.is_offered isEqualToString:@"1"]) {
                [cell.offerImageView setHidden:NO];
            }else{
                [cell.offerImageView setHidden:YES];
            }
            if (tempbroadcastObj.user_like != nil && [tempbroadcastObj.user_like isEqualToString:@"1"]) {
                cell.favImageView.image = [UIImage imageNamed:@"filled_like.png"];
            }else{
                cell.favImageView.image = [UIImage imageNamed:@"empty_like.png"];
            }
//            [cell.thumbImageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(tempbroadcastObj.image_url1)] placeholderImage:[UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"]];
            
            if(tempbroadcastObj.image_url1 != nil && ![tempbroadcastObj.image_url1 isEqualToString:@""])
            {
                __weak UIImageView *thumbImg_ = cell.thumbImageView;
//                [cell.thumbImageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(tempbroadcastObj.image_url1)]
//                                    placeholderImage:[UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"]
//                                             success:^(UIImage *image) {
//                                                 DebugLog(@"success");
//                                             }
//                                             failure:^(NSError *error) {
//                                                 DebugLog(@"write error %@", error);
//                                                 thumbImg_.image = [UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"];
//                                             }];
                [cell.thumbImageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(tempbroadcastObj.image_url1)] placeholderImage:[UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"] completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType) {
                    if (error) {
                        thumbImg_.image = [UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"];
                    }
                }];
            } else {
                cell.thumbImageView.image = [UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"];
            }
            
            UIButton *likeBtn = (UIButton *)[cell viewWithTag:111];
            [likeBtn addTarget:self action:@selector(likeBtnPressed:event:) forControlEvents:UIControlEventTouchUpInside];
            
            UIButton *shareBtn = (UIButton *)[cell viewWithTag:222];
            [shareBtn addTarget:self action:@selector(shareBtnPressed:event:) forControlEvents:UIControlEventTouchUpInside];
            
            UIButton *callBtn = (UIButton *)[cell viewWithTag:333];
            [callBtn addTarget:self action:@selector(callBtnTapped:event:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
            
        }else{
            NSString *cellIdentifier = kWithoutImageCellIdentifier;
            CellWithoutImageView *cell = (CellWithoutImageView *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            cell.lblTitle.text          = tempbroadcastObj.title;
            cell.lblLeftTitle.text      = tempbroadcastObj.name;
            cell.lblDescription.text    = tempbroadcastObj.description;
            cell.lblLikeCount.text      = tempbroadcastObj.total_like;
            cell.lblShareCount.text     = tempbroadcastObj.total_share;
            cell.lblTimeStamp.text      = [NSString stringWithFormat:@"%@ %@    %@",dayFromDate,monthFromDate,timeFromDate];
            if (tempbroadcastObj.is_offered != nil && [tempbroadcastObj.is_offered isEqualToString:@"1"]) {
                [cell.offerImageView setHidden:NO];
            }else{
                [cell.offerImageView setHidden:YES];
            }
            if (tempbroadcastObj.user_like != nil && [tempbroadcastObj.user_like isEqualToString:@"1"]) {
                cell.favImageView.image = [UIImage imageNamed:@"filled_like.png"];
            }else{
                cell.favImageView.image = [UIImage imageNamed:@"empty_like.png"];
            }
            UIButton *likeBtn = (UIButton *)[cell viewWithTag:111];
            [likeBtn addTarget:self action:@selector(likeBtnPressed:event:) forControlEvents:UIControlEventTouchUpInside];
            
            UIButton *shareBtn = (UIButton *)[cell viewWithTag:222];
            [shareBtn addTarget:self action:@selector(shareBtnPressed:event:) forControlEvents:UIControlEventTouchUpInside];
            
            UIButton *callBtn = (UIButton *)[cell viewWithTag:333];
            [callBtn addTarget:self action:@selector(callBtnTapped:event:) forControlEvents:UIControlEventTouchUpInside];
            return cell;
        }
    }else{
        searchTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        searchTableView.backgroundColor = [UIColor whiteColor];
        searchTableView.separatorColor  = [UIColor lightGrayColor];
        
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        UIButton *btnCall;
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.backgroundColor = [UIColor clearColor];

            cell.textLabel.font = DEFAULT_BOLD_FONT(16);
            cell.textLabel.textColor = BROWN_COLOR;
            cell.detailTextLabel.font = DEFAULT_FONT(14);
            cell.detailTextLabel.textColor = BROWN_COLOR;
            
            btnCall = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnCall setFrame:CGRectMake(0, 0,100,100)];
            btnCall.tag = 4444;
            [btnCall setImage:[UIImage imageNamed:@"Call_white.png"] forState:UIControlStateNormal];
            [btnCall setBackgroundColor:[UIColor clearColor]];
            [btnCall setTitle:@"" forState:UIControlStateNormal];
            [btnCall addTarget:self action:@selector(callBtnTapped:event:) forControlEvents:UIControlEventTouchUpInside];
            [cell addSubview:btnCall];
            
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
            bgColorView.tag = 104;
            cell.selectedBackgroundView = bgColorView;
        }
        btnCall = (UIButton *)[cell viewWithTag:4444];
        if ([searchArray count] > 0 && searchArray != nil) {
            btnCall.hidden = NO;
            INAllStoreObj *placeObj  = [searchArray objectAtIndex:indexPath.row];
            cell.textLabel.textColor = DEFAULT_COLOR;
            cell.detailTextLabel.textColor = DEFAULT_COLOR;
            cell.textLabel.font = DEFAULT_BOLD_FONT(16);

            cell.textLabel.text = placeObj.name;
            cell.textLabel.numberOfLines = 2;
            cell.detailTextLabel.text = placeObj.description;
            cell.detailTextLabel.numberOfLines = 3;
            [cell.imageView setImage:[UIImage imageNamed:@"table_placeholder.png"]];
            cell.accessoryType = UITableViewCellAccessoryNone;

//            [cell.imageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(placeObj.image_url)] placeholderImage:[UIImage imageNamed:@"list_thumbnail.png"]];
//            cell.accessoryType = UITableViewCellAccessoryNone;
//           // cell.imageView.image = [self scale:cell.imageView.image toSize:CGSizeMake(100, 100)];
//            cell.imageView.transform = CGAffineTransformMakeScale(100, 100);
        }
//        else if([searchTextField.text length] > 0 && searchArray == nil)
//        {
//            searchTableView.backgroundColor = [UIColor clearColor];
//            cell.textLabel.textColor = [UIColor whiteColor];
//            cell.textLabel.font = DEFAULT_FONT(18);
//
//            cell.textLabel.text = @"With offers";
//            cell.detailTextLabel.text = @"";
//            cell.imageView.image = nil;
//            cell.accessoryType = UITableViewCellAccessoryNone;
//        }
        else {
            searchTableView.backgroundColor = [UIColor whiteColor];
            cell.textLabel.textColor = DEFAULT_COLOR;
            cell.textLabel.font = DEFAULT_FONT(20);
            btnCall.hidden = YES;
            
            if ([indexPath row] < [[searchdict allKeys] count]) {
                NSString *key = [[searchdict.allKeys sortedArrayUsingComparator:^(id firstObject, id secondObject) {
                    return [((NSString *)firstObject) compare:((NSString *)secondObject) options:NSNumericSearch];
                }] objectAtIndex:indexPath.row];
                cell.textLabel.text = [searchdict objectForKey:key];
                cell.textLabel.numberOfLines = 2;
                cell.detailTextLabel.text = @"";
                cell.imageView.image = nil;
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        }
        return cell;
    }
    }
}


#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:callModalTableView]) {
        [callModalView setHidden:YES];
        [callModalViewBackView setHidden:YES];
        NSString *selectedTelNumberToCall = [[telArray objectAtIndex:indexPath.row] stringByReplacingOccurrencesOfString:@" " withString:@""];
        DebugLog(@"selectedTelNumberToCall %@",selectedTelNumberToCall);
        NSString *deviceType = [UIDevice currentDevice].model;
        if([deviceType isEqualToString:@"iPhone"])
        {
            UIApplication *myApp = [UIApplication sharedApplication];
            DebugLog(@"selectedTelNumberToCall = -%@-",selectedTelNumberToCall);
            [myApp openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",selectedTelNumberToCall]]];
            
            NSDictionary *callParams = [NSDictionary dictionaryWithObjectsAndKeys:@"Search",@"Source",[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"AreaName",selectedPlaceName,@"StoreName",selectedTelNumberToCall,@"Number", nil];
            [INEventLogger logEvent:@"StoreCallDone" withParams:callParams];
            
            [self sendStoreCallRequest:@"StoreCallDone"];
        }
        else
        {
            UIAlertView *alert = [[ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                              message: @"Calling functionality is not available in this device. "
                                                             delegate: nil
                                                    cancelButtonTitle: nil
                                                    otherButtonTitles: @"OK", nil];
            [alert show];
        }
    }else{
        if([searchArray count] == 0 || searchArray == nil)
        {
            NSString *key = [[searchdict.allKeys sortedArrayUsingComparator:^(id firstObject, id secondObject) {
                return [((NSString *)firstObject) compare:((NSString *)secondObject) options:NSNumericSearch];
            }] objectAtIndex:indexPath.row];
            NSDictionary *catgParams = [NSDictionary dictionaryWithObjectsAndKeys:[searchdict objectForKey:key],@"Category_Name", searchTextField.text,@"Search_Text", nil];
            DebugLog(@"catgParams-%@-",catgParams);

            if (isStoreSearch) {
                [INEventLogger logEvent:@"Search_StoreCategory" withParams:catgParams];
            }else{
                [INEventLogger logEvent:@"Search_OffersCategory" withParams:catgParams];
            }
            INCategoryViewController *catgViewController = [[INCategoryViewController alloc] initWithNibName:@"INCategoryViewController" bundle:nil] ;
            catgViewController.title            = [searchdict objectForKey:key];
            catgViewController.isStoreSearch    = isStoreSearch;
            catgViewController.category_id      = [key intValue];
            [self.navigationController pushViewController:catgViewController animated:YES];
        }
    //    else if([searchTextField.text length] > 0 && searchArray == nil){
    //        UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    //        if(cell.accessoryType == UITableViewCellAccessoryCheckmark)
    //        {
    //            cell.accessoryType = UITableViewCellAccessoryNone;
    //        } else {
    //            cell.accessoryType = UITableViewCellAccessoryCheckmark;
    //        }
    //    }
        else {
            if (isStoreSearch) {
                INAllStoreObj *placeObj  = [searchArray objectAtIndex:indexPath.row];
                INStoreDetailViewController *detailController = [[INStoreDetailViewController alloc] initWithNibName:@"INStoreDetailViewController" bundle:nil] ;
                detailController.title = placeObj.name;
                detailController.storeId = placeObj.ID;
                if ([placeObj.image_url hasPrefix:@"http"]) {
                    detailController.storeImageUrl = [NSURL URLWithString:placeObj.image_url];
                }else{
                    detailController.storeImageUrl = [NSURL URLWithString:THUMBNAIL_LINK(placeObj.image_url)];
                }
                [self.navigationController pushViewController:detailController animated:YES];
            }else{
                INBroadcastDetailObj *broadcastObj  = [searchArray objectAtIndex:indexPath.row];
                INCustBroadCastDetailsViewController *postDetailController = [[INCustBroadCastDetailsViewController alloc] initWithNibName:@"INCustBroadCastDetailsViewController" bundle:nil] ;
                postDetailController.title = broadcastObj.name;//@"Broadcast Detail";
                postDetailController.storeName = broadcastObj.name;
                postDetailController.postType = 1;
                postDetailController.postlikeType = broadcastObj.user_like;
                postDetailController.postId = broadcastObj.ID;
                postDetailController.postTitle = broadcastObj.title;
                postDetailController.postDescription = broadcastObj.description;
                DebugLog(@"lfeedObj.image_url1 %@",broadcastObj.image_url1);
                if ([broadcastObj.image_url1 isEqual:[NSNull null]] || broadcastObj.image_url1 == nil) {
                    postDetailController.postimageUrl = @"";
                }else{
                    postDetailController.postimageUrl = broadcastObj.image_url1;
                }
                postDetailController.likescountString = broadcastObj.total_like;
                postDetailController.sharecountString = broadcastObj.total_share;
                postDetailController.viewOpenedFrom = FROM_NEWS_FEED;
                NSMutableArray *tempArray = [[NSMutableArray alloc] init];
                if (broadcastObj.tel_no1 != nil && ![broadcastObj.tel_no1 isEqualToString:@""]) {
                    [tempArray addObject:broadcastObj.tel_no1];
                }
                if (broadcastObj.tel_no2 != nil && ![broadcastObj.tel_no2 isEqualToString:@""]) {
                    [tempArray addObject:broadcastObj.tel_no2];
                }
                if (broadcastObj.tel_no3 != nil && ![broadcastObj.tel_no3 isEqualToString:@""]) {
                    [tempArray addObject:broadcastObj.tel_no3];
                }
                if (broadcastObj.mob_no1 != nil && ![broadcastObj.mob_no1 isEqualToString:@""]) {
                    [tempArray addObject:broadcastObj.mob_no1];
                }
                if (broadcastObj.mob_no2 != nil && ![broadcastObj.mob_no2 isEqualToString:@""]) {
                    [tempArray addObject:broadcastObj.mob_no2];
                }
                if (broadcastObj.mob_no3 != nil && ![broadcastObj.mob_no3 isEqualToString:@""]) {
                    [tempArray addObject:broadcastObj.mob_no3];
                }
                DebugLog(@"temparray %@",tempArray);
                postDetailController.telArray = tempArray;
                postDetailController.SHOW_BUY_BTN = YES;
                postDetailController.placeId = broadcastObj.place_ID;
                postDetailController.postDateTime   = broadcastObj.date;
                postDetailController.postOfferDateTime  = broadcastObj.offerdate;
                postDetailController.postIsOffered      = broadcastObj.is_offered;
                [self.navigationController pushViewController:postDetailController animated:YES];
            }
        }
    }
     [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


-(NSIndexPath *)getIndexpathOfEvent:(id)event
{
    NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:searchTableView];
	NSIndexPath *indexPath = [searchTableView indexPathForRowAtPoint: currentTouchPosition];
    return indexPath;
}

- (void)callBtnTapped:(id)sender event:(id)event
{
    NSIndexPath *indexPath = [self getIndexpathOfEvent:event];
    if (indexPath != nil)
    {
        if (telArray == nil) {
            telArray = [[NSMutableArray alloc] init];
        }else{
            [telArray removeAllObjects];
        }
        if (isStoreSearch) {
            INAllStoreObj *placeObj  = (INAllStoreObj *)[searchArray objectAtIndex:indexPath.row];
            DebugLog(@"place %@",placeObj.name);
            if (placeObj.name == nil || [placeObj.name length] == 0)
            {
                lblcallModalHeader.text =  @"Select number";
                selectedPlaceName = @"";
            }
            else {
                lblcallModalHeader.text = placeObj.name;
                selectedPlaceName = placeObj.name;
            }
            
            selectedPlaceId = placeObj.ID;
            
            if (placeObj.tel_no1 != nil && ![placeObj.tel_no1 isEqualToString:@""]) {
                [telArray addObject:placeObj.tel_no1];
            }
            if (placeObj.tel_no2 != nil && ![placeObj.tel_no2 isEqualToString:@""]) {
                [telArray addObject:placeObj.tel_no2];
            }
            if (placeObj.tel_no3 != nil && ![placeObj.tel_no3 isEqualToString:@""]) {
                [telArray addObject:placeObj.tel_no3];
            }
            if (placeObj.mob_no1 != nil && ![placeObj.mob_no1 isEqualToString:@""]) {
                [telArray addObject:[NSString stringWithFormat:@"+%@",placeObj.mob_no1]];
            }
            if (placeObj.mob_no2 != nil && ![placeObj.mob_no2 isEqualToString:@""]) {
                [telArray addObject:[NSString stringWithFormat:@"+%@",placeObj.mob_no2]];
            }
            if (placeObj.mob_no3 != nil && ![placeObj.mob_no3 isEqualToString:@""]) {
                [telArray addObject:[NSString stringWithFormat:@"+%@",placeObj.mob_no3]];
            }
        }else{
            INBroadcastDetailObj * placeObj  = (INBroadcastDetailObj *)[searchArray objectAtIndex:indexPath.row];
            DebugLog(@"place %@",placeObj.name);
            if (placeObj.name == nil || [placeObj.name length] == 0)
            {
                lblcallModalHeader.text =  @"Select number";
                selectedPlaceName = @"";
            }
            else {
                lblcallModalHeader.text = placeObj.name;
                selectedPlaceName = placeObj.name;
            }
            
            selectedPlaceId = placeObj.ID;
            
            if (placeObj.tel_no1 != nil && ![placeObj.tel_no1 isEqualToString:@""]) {
                [telArray addObject:placeObj.tel_no1];
            }
            if (placeObj.tel_no2 != nil && ![placeObj.tel_no2 isEqualToString:@""]) {
                [telArray addObject:placeObj.tel_no2];
            }
            if (placeObj.tel_no3 != nil && ![placeObj.tel_no3 isEqualToString:@""]) {
                [telArray addObject:placeObj.tel_no3];
            }
            if (placeObj.mob_no1 != nil && ![placeObj.mob_no1 isEqualToString:@""]) {
                [telArray addObject:[NSString stringWithFormat:@"+%@",placeObj.mob_no1]];
            }
            if (placeObj.mob_no2 != nil && ![placeObj.mob_no2 isEqualToString:@""]) {
                [telArray addObject:[NSString stringWithFormat:@"+%@",placeObj.mob_no2]];
            }
            if (placeObj.mob_no3 != nil && ![placeObj.mob_no3 isEqualToString:@""]) {
                [telArray addObject:[NSString stringWithFormat:@"+%@",placeObj.mob_no3]];
            }
        }
        
        [callModalTableView reloadData];
        if (callModalTableView.contentSize.height < 240) {
            [callModalTableView setFrame:CGRectMake(callModalTableView.frame.origin.x, callModalTableView.frame.origin.y, callModalTableView.frame.size.width, callModalTableView.contentSize.height)];
        }else{
            [callModalTableView setFrame:CGRectMake(callModalTableView.frame.origin.x, callModalTableView.frame.origin.y, callModalTableView.frame.size.width, 240)];
        }
        [callModalView setFrame:CGRectMake(callModalView.frame.origin.x, callModalView.frame.origin.y, callModalView.frame.size.width, callModalTableView.frame.size.height+85)];

        DebugLog(@"temparray %@",telArray);
        if (telArray != nil && telArray.count > 0) {
            [callModalViewBackView setHidden:NO];
            [callModalView setHidden:NO];
            [CommonCallback viewtransitionInCompletion:callModalView completion:^{
                [CommonCallback viewtransitionOutCompletion:callModalView completion:nil];
            }];
            
            NSDictionary *callParams = [NSDictionary dictionaryWithObjectsAndKeys:@"Search",@"Source",[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"AreaName",selectedPlaceName,@"StoreName", nil];
            [INEventLogger logEvent:@"StoreCall" withParams:callParams];
            
            [self sendStoreCallRequest:@"StoreCall"];
        } else {
            [INUserDefaultOperations showSIAlertView:NO_CONTACT_MESSAGE];
        }
    }
}

- (IBAction)btnCancelcallModalPressed:(id)sender {
    [callModalView setHidden:YES];
    [callModalViewBackView setHidden:YES];
}



-(void)sendStoreCallRequest:(NSString *)callType
{
    
    if (![INUserDefaultOperations isCustomerLoggedIn]) {
        return;
    }
    
    NSString *areaID = [IN_APP_DELEGATE getActiveShoplocalPlaceIdFromAreaTable];
    
    DebugLog(@"========================sendStoreCallRequest========================");
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSString *register_from = [NSString stringWithFormat:@"%@ v%@",DEVICE_TYPE,version];
    
    NSMutableDictionary *postparams = [[NSMutableDictionary alloc] init];
    //    {"user_id":"${user_id}","auth_id":"${auth_id}","place_id":"106","via":"welcome ","call_type":"type-a","active_area":"1"}
    [postparams setObject:[INUserDefaultOperations getCustomerAuthId] forKey:@"user_id"];
    [postparams setObject:[INUserDefaultOperations getCustomerAuthCode] forKey:@"auth_id"];
    if (isStoreSearch) {
        [postparams setObject:[NSString stringWithFormat:@"%d",selectedPlaceId] forKey:@"place_id"];
    }else{
        [postparams setObject:[NSString stringWithFormat:@"%d",selectedPlaceId] forKey:@"post_id"];
    }
    [postparams setObject:callType forKey:@"call_type"];
    [postparams setObject:areaID forKey:@"active_area"];
    [postparams setObject:register_from forKey:@"via"];
    
    DebugLog(@"sendStoreCallRequest : postParams -%@-",postparams);
    
    [httpClient postPath:CALL_STORE parameters:postparams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"sendStoreCallRequest : json afnetworking ->%@",json);
        if (json!= nil) {
            NSString* code = [json objectForKey:@"code"];
            if([code isEqualToString:CUSTOMER_INVALID_USER_AUTH_CODE])
            {
                if([INUserDefaultOperations isCustomerLoggedIn]){
                    [INUserDefaultOperations clearCustomerDetails];
                }
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"sendStoreCallRequest : [HTTPClient Error]: %@", error.localizedDescription);
    }];
}

- (UIImage *)scale:(UIImage *)image toSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}

-(void)pageRequest:(NSString *)pageNum
{
    if([IN_APP_DELEGATE networkavailable])
    {
        isAdding = YES;
        if (isStoreSearch) {
            if(self.searchType == 1)
            {
                NSString* encodeUrlString = [STORE_SEARCH_WITH_OFFER(searchTextField.text,[INUserDefaultOperations getLatitude],[INUserDefaultOperations getLongitude],[INUserDefaultOperations getCustomerDistance],[IN_APP_DELEGATE getActiveAreaIdFromAreaTable],self.currentPage) stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                DebugLog(@"%@",encodeUrlString);
                [self sendSearchStoreRequest:encodeUrlString page:self.currentPage];
            } else {
                NSString* encodeUrlString = [STORE_SEARCH_WITHOUT_OFFER(searchTextField.text,[INUserDefaultOperations getLatitude],[INUserDefaultOperations getLongitude],[INUserDefaultOperations getCustomerDistance],[IN_APP_DELEGATE getActiveAreaIdFromAreaTable],self.currentPage) stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                DebugLog(@"%@",encodeUrlString);
                [self sendSearchStoreRequest:encodeUrlString page:self.currentPage];
            }
        }else{
            if(self.searchType == 1)
            {
                NSString* encodeUrlString = [BROADCAST_SEARCH_WITH_OFFER(searchTextField.text,[IN_APP_DELEGATE getActiveAreaIdFromAreaTable],self.currentPage)stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                DebugLog(@"%@",encodeUrlString);
                [self sendSearchBroadCastRequest:encodeUrlString page:self.currentPage];
            } else {
                NSString* encodeUrlString = [BROADCAST_SEARCH_WITHOUT_OFFER(searchTextField.text,[IN_APP_DELEGATE getActiveAreaIdFromAreaTable],self.currentPage)stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                DebugLog(@"%@",encodeUrlString);
                [self sendSearchBroadCastRequest:encodeUrlString page:self.currentPage];
            }
        }

    }  else   {
        isAdding = NO;
        if(isLoading)
        {
            [self stopLoading];
        }
        [INUserDefaultOperations showOfflineAlert];
        
    }
}


-(void)setCountLabel
{
    self.countlbl.text = [NSString stringWithFormat:@"%d/%d",[searchArray count],self.totalRecord];
    DebugLog(@"-----%@--------",countlbl.text);
}

#pragma mark PULL TO REFRESH
- (void)setupStrings{
    textPull = @"Pull down to refresh...";
    textRelease = @"Release to refresh...";
    textLoading = @"Loading please wait...";
}

- (void)addPullToRefreshHeader {
    refreshHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0 - REFRESH_HEADER_HEIGHT, 320, REFRESH_HEADER_HEIGHT)];
    refreshHeaderView.backgroundColor = [UIColor clearColor];
    
    refreshLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, REFRESH_HEADER_HEIGHT)];
    refreshLabel.backgroundColor = [UIColor clearColor];
    refreshLabel.font = DEFAULT_BOLD_FONT(15);
    refreshLabel.textAlignment = NSTextAlignmentCenter;
//    if (isStoreSearch) {
        refreshLabel.textColor = DEFAULT_COLOR;
        refreshArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:PULLTOREFRESH_BARROW]];
//    }else{
//        refreshLabel.textColor = [UIColor whiteColor];
//        refreshArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:PULLTOREFRESH_WARROW]];
//    }
    refreshArrow.frame = CGRectMake(floorf((REFRESH_HEADER_HEIGHT - 2) / 2),
                                    (floorf(REFRESH_HEADER_HEIGHT - 44) / 2),
                                    27, 44);
    
    refreshSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    refreshSpinner.frame = CGRectMake(floorf(floorf(REFRESH_HEADER_HEIGHT - 2) / 2), floorf((REFRESH_HEADER_HEIGHT - 20) / 2), 20, 20);
    refreshSpinner.hidesWhenStopped = YES;
    
    [refreshHeaderView addSubview:refreshLabel];
    [refreshHeaderView addSubview:refreshArrow];
    [refreshHeaderView addSubview:refreshSpinner];
    [searchTableView addSubview:refreshHeaderView];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if ([scrollView isEqual:callModalTableView]) {
        return;
    }
    if (isLoading) return;
    isDragging = YES;
    lastContentOffset = scrollView.contentOffset.y;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([scrollView isEqual:callModalTableView]) {
        return;
    }
    if(searchArray.count == 0 || searchArray == nil){
        refreshArrow.hidden = TRUE;
        refreshLabel.hidden = TRUE;
        return;
    }
    refreshArrow.hidden = FALSE;
    refreshLabel.hidden = FALSE;
    if (isLoading) {
//        DebugLog(@"isLoading");
        // Update the content inset, good for section headers
        if (scrollView.contentOffset.y > 0)
            searchTableView.contentInset = UIEdgeInsetsZero;
        else if (scrollView.contentOffset.y >= -REFRESH_HEADER_HEIGHT)
            searchTableView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (isDragging && scrollView.contentOffset.y < 0) {
//        DebugLog(@"isDragging");
        // Update the arrow direction and label
        [UIView animateWithDuration:0.25 animations:^{
            if (scrollView.contentOffset.y < -REFRESH_HEADER_HEIGHT) {
                // User is scrolling above the header
                refreshLabel.text = self.textRelease;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
            } else {
                // User is scrolling somewhere within the header
                refreshLabel.text = self.textPull;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
            }
        }];
    } else {
//        DebugLog(@"else of isLoading && isDragging");
        CGFloat height = scrollView.frame.size.height;
        
        CGFloat contentYoffset = scrollView.contentOffset.y;
        
        CGFloat distanceFromBottom = scrollView.contentSize.height - contentYoffset;
        
        if(distanceFromBottom <= height)
        {
            if(isAdding==TRUE) {
                DebugLog(@"<<<<<<<<<<<<<<<");
                return;
            } else {
                DebugLog(@"end of the table page = %d", self.currentPage);
                 DebugLog(@"%d %d", self.currentPage, self.totalPage);
                if(self.currentPage < self.totalPage)
                {
                    self.currentPage = self.currentPage + 1;
                    [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
                }
            }
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if ([scrollView isEqual:callModalTableView]) {
        return;
    }
    if(searchArray.count == 0 || searchArray == nil){
        return;
    }
    if (isLoading) return;
    isDragging = NO;
    if (scrollView.contentOffset.y <= -REFRESH_HEADER_HEIGHT) {
        // Released above the header
        [self startLoading];
    }
    if (lastContentOffset < (int)scrollView.contentOffset.y) {
        DebugLog(@"lastContentOffset <");
        if (searchView.frame.size.height >= 92) {
            searchZoomOutBtn.hidden = NO;
            [searchZoomOutBtn setTitle:[NSString stringWithFormat:@"%@",searchTextField.text] forState:UIControlStateNormal];
            [UIView animateWithDuration:0.2
                             animations:^(void){
                                 searchView.frame = CGRectMake(searchView.frame.origin.x, searchView.frame.origin.y, searchView.frame.size.width, searchView.frame.size.height/2);
                                 searchTableView.frame = CGRectMake(searchTableView.frame.origin.x, CGRectGetMaxY(searchView.frame), searchTableView.frame.size.width, self.view.frame.size.height-(CGRectGetMaxY(searchView.frame)));
                                 searchTextField.alpha = 0.0;
                                 searchCancelBtn.alpha = 0.0;
                             } completion:^(BOOL finished){
                                 [UIView animateWithDuration:0.3 animations:^{
                                     searchZoomOutBtn.transform = CGAffineTransformMakeScale(0.8, 0.8);
                                 } completion:^(BOOL finished){
                                     searchTextField.hidden = YES;
                                     searchCancelBtn.hidden = YES;
                                     [searchTextField resignFirstResponder];
                                 }];
                             }];
        }
    }
    else if (lastContentOffset > (int)scrollView.contentOffset.y) {
        DebugLog(@"lastContentOffset >");
        if (searchView.frame.size.height < 92) {
            [UIView animateWithDuration:0.3 animations:^{
                searchZoomOutBtn.transform = CGAffineTransformIdentity;
                searchTextField.alpha = 1.0;
                searchCancelBtn.alpha = 1.0;
            } completion:^(BOOL finished){
                searchZoomOutBtn.hidden = YES;
                searchTextField.hidden = NO;
                searchCancelBtn.hidden = NO;
                [UIView animateWithDuration:0.2
                                 animations:^(void){
                                     searchView.frame = CGRectMake(searchView.frame.origin.x, searchView.frame.origin.y, searchView.frame.size.width, searchView.frame.size.height*2);
                                     searchTableView.frame = CGRectMake(searchTableView.frame.origin.x, CGRectGetMaxY(searchView.frame), searchTableView.frame.size.width, self.view.frame.size.height-(CGRectGetMaxY(searchView.frame)));
                                 } completion:^(BOOL finished){
                                 }];
            }];
        }
    }
}

- (void)startLoading {
    isLoading = YES;
    // Show the header
    [UIView animateWithDuration:0.3 animations:^{
        searchTableView.contentInset = UIEdgeInsetsMake(REFRESH_HEADER_HEIGHT, 0, 0, 0);
        refreshLabel.text = self.textLoading;
        refreshArrow.hidden = YES;
        [refreshSpinner startAnimating];
    }];
    if([IN_APP_DELEGATE networkavailable])
    {
        [searchArray removeAllObjects];
        searchArray = nil;
        self.currentPage = 1;
        [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
    }
    else
    {
        [self stopLoading];
    }
    // Refresh action!
    //[self refresh];
}

- (void)stopLoading {
    isLoading = NO;
    // Hide the header
    [UIView animateWithDuration:0.3 animations:^{
        searchTableView.contentInset = UIEdgeInsetsZero;
        [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
    }
                     completion:^(BOOL finished) {
                         [self performSelector:@selector(stopLoadingComplete)];
                     }];
}

- (void)stopLoadingComplete {
    // Reset the header
    refreshLabel.text = self.textPull;
    refreshArrow.hidden = NO;
    [refreshSpinner stopAnimating];
}
-(void)showLoginAlert{
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Please Login to mark this post as favourite."];
    alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
    alertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
    
    [alertView addButtonWithTitle:@"LOGIN"
                             type:SIAlertViewButtonTypeDestructive
                          handler:^(SIAlertView *alert) {
                              DebugLog(@"LOGIN Clicked");
                              [self showLoginModal];
                          }];
    [alertView addButtonWithTitle:@"NOT NOW"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alert) {
                              DebugLog(@"NOT NOW Clicked");
                          }];
    [alertView show];
}

- (void)likeBtnPressed:(id)sender event:(id)event {
    DebugLog(@"likeBtnPressed");
    if([IN_APP_DELEGATE networkavailable])
    {
        NSSet *touches = [event allTouches];
        UITouch *touch = [touches anyObject];
        CGPoint currentTouchPosition = [touch locationInView:self.searchTableView];
        NSIndexPath *indexPath = [self.searchTableView indexPathForRowAtPoint:currentTouchPosition];
        if (indexPath != nil) {
            if(![INUserDefaultOperations isCustomerLoggedIn])
            {
                [self showLoginAlert];
            } else {
                currentSelectedRow = indexPath.row;
                INBroadcastDetailObj *tempBroadCastObj = (INBroadcastDetailObj *)[searchArray objectAtIndex:indexPath.row];
                DebugLog(@"user_like %@ post_id %d",tempBroadCastObj.user_like,tempBroadCastObj.ID);
                if ([tempBroadCastObj.user_like intValue]) {
                    [self sendPostUnLikeRequest];
                }else{
                    [self sendPostLikeRequest];
//                    if ([[INUserDefaultOperations getCustomerLoginFrom] isEqualToString:FACEBOOKSERVER] && [INUserDefaultOperations isOpenGraphShareEnabled]) {
//                        SIAlertView *sialertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Do you want to share this on your Facebook timeline?"];
//                        sialertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
//                        sialertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
//                        [sialertView addButtonWithTitle:@"YES"
//                                                   type:SIAlertViewButtonTypeDestructive
//                                                handler:^(SIAlertView *alert) {
//                                                    DebugLog(@"YES");
//                                                    [self sendPostLikeRequest];
//
//                                                    NSString *deepLinkingUrl = [FacebookOpenGraphAPI createDeepLinkWithActiveAreaName:[IN_APP_DELEGATE getActiveSlugFromAreaTable] storeName:tempBroadCastObj.name storeId:[NSString stringWithFormat:@"%d",tempBroadCastObj.place_ID]];
//                                                    NSString *postTitle         = tempBroadCastObj.title;
//                                                    NSString *postDescription   = tempBroadCastObj.description;
//                                                    NSString *postimageUrl      = tempBroadCastObj.image_url1;
//                                                    if (postimageUrl != nil && ![postimageUrl isEqualToString:@""]) {
//                                                        if ([postimageUrl hasPrefix:@"http"]) {
//                                                            [FacebookOpenGraphAPI likeContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:postimageUrl deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
//                                                        }else{
//                                                            [FacebookOpenGraphAPI likeContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:THUMBNAIL_LINK(postimageUrl) deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
//                                                        }
//                                                    }else{
//                                                        [FacebookOpenGraphAPI likeContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:@"" deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
//                                                    }
//                                                }];
//                        [sialertView addButtonWithTitle:@"NOT NOW"
//                                                   type:SIAlertViewButtonTypeCancel
//                                                handler:^(SIAlertView *alert) {
//                                                    DebugLog(@"NO");
//                                                    [self sendPostLikeRequest];
//
//                                                }];
//                        [sialertView show];
//                    }else{
//                        [self sendPostLikeRequest];
//                    }
                }
            }
        }
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

- (void)shareBtnPressed:(id)sender event:(id)event {
    DebugLog(@"shareBtnPressed");
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.searchTableView];
    NSIndexPath *indexPath = [self.searchTableView indexPathForRowAtPoint:currentTouchPosition];
    if (indexPath != nil) {
        currentSelectedRow = indexPath.row;
        UIActionSheet *shareActionSheet = [[UIActionSheet alloc] initWithTitle:@"Share via" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"SMS",@"Email",@"WhatsApp",@"Facebook",@"Twitter", nil];
        [shareActionSheet showInView:self.view];
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSString *title = [actionSheet buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"SMS"])
    {
        [self smsShareClicked];
    }
    else if([title isEqualToString:@"Email"])
    {
        [self emailShareClicked];
    }else if([title isEqualToString:@"WhatsApp"])
    {
        [self whatsAppShareClicked];
    }
    else if([title isEqualToString:@"Facebook"])
    {
        [self facebookShareClicked];
    } else if([title isEqualToString:@"Twitter"])
    {
        [self twitterShareClicked];
    }
}
-(void)smsShareClicked{
    if ([MFMessageComposeViewController canSendText]) {
        NSString *message = [self getPostDetailsMessageBody:@"sms"];

        MFMessageComposeViewController *messageComposer = [[MFMessageComposeViewController alloc] init];
        messageComposer.messageComposeDelegate = self;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            messageComposer.navigationBar.tintColor  = [UIColor whiteColor];
        }
        [messageComposer setBody:message];
        [messageComposer setRecipients:nil];
        [self presentViewController:messageComposer animated:YES completion:nil];
    }else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"You may not have set up sms service on your device.\nPlease check and try again."
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}
#pragma mark MFMessageComposeViewController Delegate Methods
-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
		case MessageComposeResultCancelled:
            DebugLog(@"SMS Cancelled");
			break;
		case MessageComposeResultFailed:
        {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Unknown Error\nSMS failed"
														   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
        }
			break;
		case MessageComposeResultSent:
        {
            DebugLog(@"SMS Sent");
        }
			break;
		default:
			break;
	}
    [self dismissViewControllerAnimated:YES completion:^{
        if (result == MessageComposeResultSent) {
            [self sendPostShareRequest:@"sms"];
        }
    }];
}

-(void)emailShareClicked{
    if ([MFMailComposeViewController canSendMail]) {
        INBroadcastDetailObj *tempBroadCastObj = (INBroadcastDetailObj *)[searchArray objectAtIndex:currentSelectedRow];
        NSString *message = [self getPostDetailsMessageBody:@"email"];

        MFMailComposeViewController *emailComposer = [[MFMailComposeViewController alloc] init];
        emailComposer.mailComposeDelegate = self;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            emailComposer.navigationBar.tintColor  = [UIColor whiteColor];
        }
        [emailComposer setToRecipients:nil];
        if (tempBroadCastObj.name != nil && ![tempBroadCastObj.name isEqualToString:@""]) {
            [emailComposer setSubject:[NSString stringWithFormat:@"%@",tempBroadCastObj.name]];
        }else{
            [emailComposer setSubject:@""];
        }
        if (tempBroadCastObj.image_url1 != nil && ![tempBroadCastObj.image_url1 isEqualToString:@""]) {
            NSString *htmlMessage = [message stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
            DebugLog(@"%@--%@",message,htmlMessage);
            [emailComposer setMessageBody:[NSString stringWithFormat:@"%@ <br><br><img src=%@ alt=%@/>",htmlMessage,THUMBNAIL_LINK(tempBroadCastObj.image_url1),tempBroadCastObj.title] isHTML:YES];
        }else{
            [emailComposer setMessageBody:message isHTML:NO];
        }
        emailComposer.toolbar.tag = 2;
        [self presentViewController:emailComposer animated:YES completion:nil];
    }else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"You may not have set up mail service on your device or You may not connected to internet.\nPlease check and try again."
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}
#pragma mark Mail Compose Delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    if (result == MFMailComposeResultFailed) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                        message:NSLocalizedString(@"Email failed to send. Please try again.", nil)
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", nil) otherButtonTitles:nil];
		[alert show];
    }
    [self dismissViewControllerAnimated:YES completion:^{
        if (controller.toolbar.tag == 2 && result == MFMailComposeResultSent) {
            [self sendPostShareRequest:@"email"];
        }
    }];
}

-(void)whatsAppShareClicked{
    //    NSString *shareMSGBody = [[NSString stringWithFormat:@"Store Name: %@\nOffer: %@\nDescription: %@",storeName,postTitle,postDescription] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *message = [self getPostDetailsMessageBody:@"whatsapp"];
    NSString *urlString = [NSString stringWithFormat:@"whatsapp://send?text=%@",[message stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURL *whatsappURL = [NSURL URLWithString:urlString];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [[UIApplication sharedApplication] openURL: whatsappURL];
        [self sendPostShareRequest:@"whatsapp"];
    }
}

-(void)twitterShareClicked{
    if ([IN_APP_DELEGATE networkavailable]) {
        NSString *message = [self getPostDetailsMessageBody:@"twitter"];
        INBroadcastDetailObj *tempBroadCastObj = (INBroadcastDetailObj *)[searchArray objectAtIndex:currentSelectedRow];
        NSString *postimageUrl = tempBroadCastObj.image_url1;
        
        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
            
            SLComposeViewController *socialComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
            if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
            {
                if ([message length] > 140) {
                    [socialComposer setInitialText:[message substringToIndex:140]];
                }else{
                    [socialComposer setInitialText:message];
                }
            }
            else{
                [socialComposer setInitialText:message];
            }
            if(postimageUrl != nil && ![postimageUrl isEqualToString:@""])
            {
                UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:THUMBNAIL_LINK(postimageUrl)]]];
                [socialComposer addImage:image];
            }
            [socialComposer setCompletionHandler:^(SLComposeViewControllerResult result){
                switch (result) {
                    case SLComposeViewControllerResultCancelled:
                        DebugLog(@"SLComposeViewControllerResultCancelled");
                        break;
                    case SLComposeViewControllerResultDone:
                        DebugLog(@"SLComposeViewControllerResultDone");
                        [self sendPostShareRequest:@"twitter"];
                        break;
                    default:
                        DebugLog(@"SLComposeViewControllerResultFailed");
                        break;
                }
                [self dismissViewControllerAnimated:YES completion:NULL];
            }];
            [self presentViewController:socialComposer animated:YES completion:Nil];
        }
        else{
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:ALERT_TITLE
                                      message:@"You may not have set up twitter service on your device.Please check and try again."
                                      delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [alertView show];
        }
    }else{
        [INUserDefaultOperations showOfflineAlert];
    }
}

-(void)facebookShareClicked{
    if ([IN_APP_DELEGATE networkavailable]) {
        [self sendPostShareRequest:@"facebook"];

        NSString *message = [self getPostDetailsMessageBody:@"facebook"];
        DebugLog(@"message %@",message);
        INBroadcastDetailObj *tempBroadCastObj = (INBroadcastDetailObj *)[searchArray objectAtIndex:currentSelectedRow];
        NSString *postimageUrl  = tempBroadCastObj.image_url1;
        NSString *storeName     = tempBroadCastObj.name;
        NSInteger placeId       = tempBroadCastObj.place_ID;
        
        NSString *deepLinkingUrl = [FacebookOpenGraphAPI createDeepLinkWithActiveAreaName:[IN_APP_DELEGATE getActiveSlugFromAreaTable] storeName:storeName storeId:[NSString stringWithFormat:@"%d",placeId]];
        
        if (postimageUrl != nil && ![postimageUrl isEqualToString:@""]) {
            if ([postimageUrl hasPrefix:@"http"]) {
                [FacebookShareAPI shareContentWithTitle:[NSString stringWithFormat:@"%@ - Offer\n",storeName] description:message image:postimageUrl deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
            }else{
                [FacebookShareAPI shareContentWithTitle:[NSString stringWithFormat:@"%@ - Offer\n",storeName] description:message image:THUMBNAIL_LINK(postimageUrl) deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
            }
        }else{
            [FacebookShareAPI shareContentWithTitle:[NSString stringWithFormat:@"%@ - Offer\n",storeName] description:message image:@"" deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
        }
    }else{
        [INUserDefaultOperations showOfflineAlert];
    }
    
//    if ([IN_APP_DELEGATE networkavailable]) {
//        NSString *message = [self getPostDetailsMessageBody:@"facebook"];
//        INBroadcastDetailObj *tempBroadCastObj = (INBroadcastDetailObj *)[searchArray objectAtIndex:currentSelectedRow];
//        NSString *postimageUrl = tempBroadCastObj.image_url1;
//
//        if (SYSTEM_VERSION_LESS_THAN(@"6.0"))
//        {
//            FaceBookShareViewController *facebookViewComposer = [[FaceBookShareViewController alloc] initWithNibName:@"FaceBookShareViewController" bundle:nil];
//            facebookViewComposer.fbdelegate = self;
//            facebookViewComposer.title = @"FACEBOOK SHARE";
//            facebookViewComposer.FBtitle = ALERT_TITLE;
//            facebookViewComposer.postMessageText = message;
//            facebookViewComposer.FBtPic = THUMBNAIL_LINK(postimageUrl);
//            UINavigationController *fbnavBar=[[UINavigationController alloc]initWithRootViewController:facebookViewComposer];
//            [self.navigationController presentViewController:fbnavBar animated:YES completion:nil];
//        }else{
//            if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
//                SLComposeViewController *socialComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
//                [socialComposer setInitialText:message];
//                if(postimageUrl != nil && ![postimageUrl isEqualToString:@""])
//                {
//                    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:THUMBNAIL_LINK(postimageUrl)]]];
//                    [socialComposer addImage:image];
//                }
//                [socialComposer setCompletionHandler:^(SLComposeViewControllerResult result){
//                    switch (result) {
//                        case SLComposeViewControllerResultCancelled:
//                            DebugLog(@"SLComposeViewControllerResultCancelled");
//                            break;
//                        case SLComposeViewControllerResultDone:
//                            DebugLog(@"SLComposeViewControllerResultDone");
//                            [self sendPostShareRequest:@"facebook"];
//                            break;
//                        default:
//                            DebugLog(@"SLComposeViewControllerResultFailed");
//                            break;
//                    }
//                    [self dismissViewControllerAnimated:YES completion:NULL];
//                }];
//                [self presentViewController:socialComposer animated:YES completion:Nil];
//            }
//            else{
//                UIAlertView *alertView = [[UIAlertView alloc]
//                                          initWithTitle:ALERT_TITLE
//                                          message:@"You may not have set up facebook service on your device.Please check and try again."
//                                          delegate:nil
//                                          cancelButtonTitle:@"OK"
//                                          otherButtonTitles:nil];
//                [alertView show];
//            }
//        }
//    }else{
//        [INUserDefaultOperations showOfflineAlert];
//    }
}

-(void)faceBookCompletionCallBack:(int)result
{
    DebugLog(@"Customer : faceBookCompletionCallBack %d",result);
    if (result == FACEBOOK_POST_SUCCESS) {
        [self sendPostShareRequest:@"facebook"];
    }
}


#pragma AFNetworking delegate methods
-(void)sendPostLikeRequest
{
    DebugLog(@"========================sendPostLikeRequest========================");
    INBroadcastDetailObj *tempBroadCastObj = (INBroadcastDetailObj *)[searchArray objectAtIndex:currentSelectedRow];
    
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:[INUserDefaultOperations getCustomerAuthId] forKey:@"user_id"];
    [params setObject:[INUserDefaultOperations getCustomerAuthCode] forKey:@"auth_id"];
    [params setObject:[NSString stringWithFormat:@"%d",tempBroadCastObj.ID] forKey:@"post_id"];
    [httpClient postPath:LIKE_BROADCAST_OF_STORE parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            if([json  objectForKey:@"message"] != [NSNull null] && [[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
//                if([json  objectForKey:@"message"] != [NSNull null] && ![[json objectForKey:@"message"] isEqualToString:@""]){
//                    NSString *message = [json objectForKey:@"message"];
//                    [INUserDefaultOperations showSIAlertView:message];
//                }
                tempBroadCastObj.user_like = @"1";
                tempBroadCastObj.total_like = [NSString stringWithFormat:@"%d",[tempBroadCastObj.total_like intValue] + 1];
            } else {
                NSString* code = [json objectForKey:@"code"];
                if([code isEqualToString:CUSTOMER_ALREADY_MADE_POST_FAV])
                {
                    tempBroadCastObj.user_like = @"1";
                    if ([tempBroadCastObj.total_like intValue] == 0) {
                        tempBroadCastObj.total_like = @"1";
                    }
//                    if([json  objectForKey:@"message"] != [NSNull null] && ![[json objectForKey:@"message"] isEqualToString:@""]){
//                        NSString *message = [json objectForKey:@"message"];
//                        [INUserDefaultOperations showSIAlertView:message];
//                    }
                }
                else if([code isEqualToString:CUSTOMER_INVALID_USER_AUTH_CODE])
                {
                    if([INUserDefaultOperations isCustomerLoggedIn]){
                        [INUserDefaultOperations clearCustomerDetails];
                        NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:IN_CUSTOMER_LOGIN_LOGOUT_KEY];
                        [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_LOGIN_LOGOUT_NOTIFICATION object:nil userInfo:dictionary];
                        [self showLoginAlert];
                    }
                }else{
                    if([json  objectForKey:@"message"] != [NSNull null] && ![[json objectForKey:@"message"] isEqualToString:@""]){
                        NSString *message = [json objectForKey:@"message"];
                        [INUserDefaultOperations showSIAlertView:message];
                    }
                }
            }
            [searchArray replaceObjectAtIndex:currentSelectedRow withObject:tempBroadCastObj];
            [searchTableView reloadData];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}

-(void)sendPostUnLikeRequest
{
    INBroadcastDetailObj *tempBroadCastObj = (INBroadcastDetailObj *)[searchArray objectAtIndex:currentSelectedRow];
    
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    [httpClient setDefaultHeader:@"user_id" value:[INUserDefaultOperations getCustomerAuthId]];
    [httpClient setDefaultHeader:@"auth_id" value:[INUserDefaultOperations getCustomerAuthCode]];
    [httpClient deletePath:UNLIKE_BROADCAST_OF_STORE(tempBroadCastObj.ID) parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            if([json  objectForKey:@"message"] != [NSNull null] && [[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
//                NSString* message = [json objectForKey:@"message"];
//                [INUserDefaultOperations showAlert:message];
                tempBroadCastObj.user_like = @"0";
                if ([tempBroadCastObj.total_like intValue] > 0) {
                    tempBroadCastObj.total_like = [NSString stringWithFormat:@"%d",[tempBroadCastObj.total_like intValue] - 1];
                }
                [searchArray replaceObjectAtIndex:currentSelectedRow withObject:tempBroadCastObj];
                [searchTableView reloadData];
            } else {
                NSString* code = [json objectForKey:@"code"];
                if([code isEqualToString:CUSTOMER_INVALID_USER_AUTH_CODE])
                {
                    if([INUserDefaultOperations isCustomerLoggedIn]){
                        [INUserDefaultOperations clearCustomerDetails];
                        NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:IN_CUSTOMER_LOGIN_LOGOUT_KEY];
                        [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_LOGIN_LOGOUT_NOTIFICATION object:nil userInfo:dictionary];
                        [self showLoginAlert];
                    }
                }else{
                    if([json  objectForKey:@"message"] != [NSNull null] && ![[json objectForKey:@"message"] isEqualToString:@""]){
                        NSString *message = [json objectForKey:@"message"];
                        [INUserDefaultOperations showSIAlertView:message];
                    }
                }
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}

-(void)sendPostShareRequest:(NSString *)viaString{
    DebugLog(@"shareMsg %@ ",viaString);
    INBroadcastDetailObj *tempBroadCastObj = (INBroadcastDetailObj *)[searchArray objectAtIndex:currentSelectedRow];
    
    if ([[INUserDefaultOperations getCustomerLoginFrom] isEqualToString:FACEBOOKSERVER] && [INUserDefaultOperations isOpenGraphShareEnabled] && ![viaString isEqualToString:@"facebook"]) {
        SIAlertView *sialertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:@"Do you want to share this on your Facebook timeline?"];
        sialertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
        sialertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
        [sialertView addButtonWithTitle:@"YES"
                                   type:SIAlertViewButtonTypeDestructive
                                handler:^(SIAlertView *alert) {
                                    DebugLog(@"YES");
                                    NSString *deepLinkingUrl = [FacebookOpenGraphAPI createDeepLinkWithActiveAreaName:[IN_APP_DELEGATE getActiveSlugFromAreaTable] storeName:tempBroadCastObj.name storeId:[NSString stringWithFormat:@"%d",tempBroadCastObj.place_ID]];
                                    NSString *postTitle         = tempBroadCastObj.title;
                                    NSString *postDescription   = tempBroadCastObj.description;
                                    NSString *postimageUrl      = tempBroadCastObj.image_url1;
                                    if (postimageUrl != nil && ![postimageUrl isEqualToString:@""]) {
                                        if ([postimageUrl hasPrefix:@"http"]) {
                                            [FacebookOpenGraphAPI shareContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:postimageUrl deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
                                        }else{
                                            [FacebookOpenGraphAPI shareContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:THUMBNAIL_LINK(postimageUrl) deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
                                        }
                                    }else{
                                        [FacebookOpenGraphAPI shareContentUsingOpenGraphAPIWithTitle:postTitle description:postDescription image:@"" deeplinkingURL:deepLinkingUrl objectType:OBJECT_TYPE_POST];
                                    }
                                }];
        [sialertView addButtonWithTitle:@"NOT NOW"
                                   type:SIAlertViewButtonTypeCancel
                                handler:^(SIAlertView *alert) {
                                    DebugLog(@"NO");
                                }];
        [sialertView show];
    }
    
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    //    if ([INUserDefaultOperations isCustomerLoggedIn]) {
    //        [params setObject:[INUserDefaultOperations getCustomerAuthId] forKey:@"user_id"];
    //        [params setObject:[INUserDefaultOperations getCustomerAuthCode] forKey:@"auth_id"];
    //    }
    [params setObject:[NSString stringWithFormat:@"%d",tempBroadCastObj.ID] forKey:@"post_id"];
    [params setObject:viaString forKey:@"via"];
    
    [httpClient postPath:SHARE_STORE_POST parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            if([json  objectForKey:@"message"] != [NSNull null] && [[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
                //                NSString* message = [json objectForKey:@"message"];
                //                [INUserDefaultOperations showAlert:message];
                tempBroadCastObj.total_share = [NSString stringWithFormat:@"%d",[tempBroadCastObj.total_share intValue] + 1];
                [searchArray replaceObjectAtIndex:currentSelectedRow withObject:tempBroadCastObj];
                [searchTableView reloadData];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        //[hud hide:YES];
        [CommonCallback hideProgressHud];
    }];
}

-(void)showLoginModal{
    INCustomerLoginViewController *custLoginController = [[INCustomerLoginViewController alloc] initWithNibName:@"INCustomerLoginViewController" bundle:nil] ;
    custLoginController.delegate = self;
    UINavigationController *loginnavBar=[[UINavigationController alloc]initWithRootViewController:custLoginController];
    UIButton *moreButton1 =  [UIButton buttonWithType:UIButtonTypeCustom];
    [moreButton1 setTitle:@"Cancel" forState:UIControlStateNormal];
    moreButton1.titleLabel.font = [UIFont systemFontOfSize:17.0];
    //[moreButton1 setBackgroundImage:[UIImage imageNamed:@"Transparent_Button.png"] forState:UIControlStateNormal];
    [moreButton1 addTarget:self action:@selector(dismissLoginView) forControlEvents:UIControlEventTouchUpInside];
    [moreButton1 setFrame:CGRectMake(250, 7, 70, 32)];
    [loginnavBar.navigationBar addSubview:moreButton1];
    [loginnavBar.navigationBar setTranslucent:NO];
    [self.navigationController presentViewController:loginnavBar animated:YES completion:nil];
}

-(void)dismissLoginView
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"Dismiss");
    }];
}

-(NSString *)getPostDetailsMessageBody:(NSString *)shareVia{
    INBroadcastDetailObj *tempBroadCastObj = (INBroadcastDetailObj *)[searchArray objectAtIndex:currentSelectedRow];
    
    NSMutableString *contactString = [[NSMutableString alloc] initWithString:@""];
    if (tempBroadCastObj.mob_no1 != nil && ![tempBroadCastObj.mob_no1 isEqualToString:@""]) {
        [contactString appendString:[NSString stringWithFormat:@"%@",tempBroadCastObj.mob_no1]];
    }
    if (tempBroadCastObj.mob_no2 != nil && ![tempBroadCastObj.mob_no2 isEqualToString:@""]) {
        if ([contactString isEqualToString:@""]) {
            [contactString appendString:tempBroadCastObj.mob_no2];
        }else{
            [contactString appendString:[NSString stringWithFormat:@", %@", tempBroadCastObj.mob_no2]];
        }
    }
    if (tempBroadCastObj.mob_no3 != nil && ![tempBroadCastObj.mob_no3 isEqualToString:@""]) {
        if ([contactString isEqualToString:@""]) {
            [contactString appendString:tempBroadCastObj.mob_no3];
        }else{
            [contactString appendString:[NSString stringWithFormat:@", %@", tempBroadCastObj.mob_no3]];
        }
    }
    if (tempBroadCastObj.tel_no1 != nil && ![tempBroadCastObj.tel_no1 isEqualToString:@""]) {
        if ([contactString isEqualToString:@""]) {
            [contactString appendString:tempBroadCastObj.tel_no1];
        }else{
            [contactString appendString:[NSString stringWithFormat:@", %@", tempBroadCastObj.tel_no1]];
        }
    }
    if (tempBroadCastObj.tel_no2 != nil && ![tempBroadCastObj.tel_no2 isEqualToString:@""]) {
        if ([contactString isEqualToString:@""]) {
            [contactString appendString:tempBroadCastObj.tel_no2];
        }else{
            [contactString appendString:[NSString stringWithFormat:@", %@", tempBroadCastObj.tel_no2]];
        }
    }
    if (tempBroadCastObj.tel_no3 != nil && ![tempBroadCastObj.tel_no3 isEqualToString:@""]) {
        if ([contactString isEqualToString:@""]) {
            [contactString appendString:tempBroadCastObj.tel_no3];
        }else{
            [contactString appendString:[NSString stringWithFormat:@", %@", tempBroadCastObj.tel_no3]];
        }
    }
    
    NSString *message = [IN_APP_DELEGATE getPostDetailsMessageBody:tempBroadCastObj.name offerTitle:tempBroadCastObj.title offerDescription:tempBroadCastObj.description isOffered:tempBroadCastObj.is_offered offerDateTime:tempBroadCastObj.offerdate contact:contactString shareVia:shareVia];
    return message;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)viewDidUnload {
    [self setSearchTableView:nil];
    [self setStoreSearchBar:nil];
    [self setCountlbl:nil];
    [self setCountHeaderView:nil];
    [self setLblpullToRefresh:nil];
    [super viewDidUnload];
}

- (IBAction)searchTextChanged:(UITextField *)textField{
    if ([textField.text length] == 0 && [searchArray count] > 0) {
        [searchArray removeAllObjects];
        searchArray = nil;
        [searchTableView reloadData];
    }
}

- (IBAction)searchCancelBtnPressed:(id)sender {
    [UIView  transitionWithView:self.navigationController.view duration:0.5  options:UIViewAnimationOptionTransitionCrossDissolve
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         [self.navigationController popViewControllerAnimated:NO];
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:^(BOOL finished){
                         isSearchOn = NO;
                     }];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if ([textField isEqual:searchTextField]) {
       isSearchOn = YES;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == searchTextField) {
        DebugLog(@"Got searchButton");
        DebugLog(@"search=%@",searchTextField.text);
        
        [searchArray removeAllObjects];
        searchArray = nil;
        self.currentPage = 1;
        UITableViewCell *cell = [searchTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        
        NSDictionary *catgParams = [NSDictionary dictionaryWithObjectsAndKeys:@"All",@"Category_Name", searchTextField.text,@"Search_Text", nil];
        DebugLog(@"catgParams-%@-",catgParams);
        if (isStoreSearch) {
            [INEventLogger logEvent:@"Search_StoreCategory" withParams:catgParams];
        }else{
            [INEventLogger logEvent:@"Search_OffersCategory" withParams:catgParams];
        }
        
        if(cell.accessoryType == UITableViewCellAccessoryCheckmark)
        {
            if (isStoreSearch) {
                NSString* encodeUrlString = [STORE_SEARCH_WITH_OFFER(searchTextField.text,[INUserDefaultOperations getLatitude],[INUserDefaultOperations getLongitude],[INUserDefaultOperations getCustomerDistance],[IN_APP_DELEGATE getActiveAreaIdFromAreaTable],self.currentPage) stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                DebugLog(@"%@",encodeUrlString);
                self.searchType = 1;   //Set search type(with offer)
                [self sendSearchStoreRequest:encodeUrlString page:self.currentPage];
            }else{
                NSString* encodeUrlString = [BROADCAST_SEARCH_WITH_OFFER(searchTextField.text,[IN_APP_DELEGATE getActiveAreaIdFromAreaTable],self.currentPage)stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                self.searchType = 1; //Set search type(with offer)
                DebugLog(@"%@",encodeUrlString);
                [self sendSearchBroadCastRequest:encodeUrlString page:self.currentPage];
            }
        } else {
            if (isStoreSearch) {
                NSString* encodeUrlString = [STORE_SEARCH_WITHOUT_OFFER(searchTextField.text,[INUserDefaultOperations getLatitude],[INUserDefaultOperations getLongitude],[INUserDefaultOperations getCustomerDistance],[IN_APP_DELEGATE getActiveAreaIdFromAreaTable],self.currentPage) stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                self.searchType = 0; //Set search type(without offer)
                DebugLog(@"%@",encodeUrlString);
                [self sendSearchStoreRequest:encodeUrlString page:self.currentPage];
            }else{
                NSString* encodeUrlString = [BROADCAST_SEARCH_WITHOUT_OFFER(searchTextField.text,[IN_APP_DELEGATE getActiveAreaIdFromAreaTable],self.currentPage)stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                self.searchType = 0; //Set search type(without offer)
                DebugLog(@"%@",encodeUrlString);
                [self sendSearchBroadCastRequest:encodeUrlString page:self.currentPage];
            }
        }
	}
   	return YES;
}

- (IBAction)searchZoomOutBtnPressed:(id)sender {
    if (searchView.frame.size.height < 92) {
        [UIView animateWithDuration:0.3 animations:^{
            searchZoomOutBtn.transform = CGAffineTransformIdentity;
            searchTextField.alpha = 1.0;
            searchCancelBtn.alpha = 1.0;
        } completion:^(BOOL finished){
            searchZoomOutBtn.hidden = YES;
            searchTextField.hidden = NO;
            searchCancelBtn.hidden = NO;
            [UIView animateWithDuration:0.2
                             animations:^(void){
                                 searchView.frame = CGRectMake(searchView.frame.origin.x, searchView.frame.origin.y, searchView.frame.size.width, searchView.frame.size.height*2);
                                 searchTableView.frame = CGRectMake(searchTableView.frame.origin.x, CGRectGetMaxY(searchView.frame), searchTableView.frame.size.width, self.view.frame.size.height-(CGRectGetMaxY(searchView.frame)));
                             } completion:^(BOOL finished){
                             }];
        }];
    }
}
@end
