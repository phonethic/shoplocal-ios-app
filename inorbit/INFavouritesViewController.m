//
//  INFavouritesViewController.m
//  shoplocal
//
//  Created by Rishi on 29/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INFavouritesViewController.h"
#import "INAppDelegate.h"
#import "INAllStoreObj.h"
#import "INDetailViewController.h"
#import "CommonCallback.h"
#import "INSearchViewController.h"
#import "INStoreDetailViewController.h"
#import "UIImageView+WebCache.h"
#import "SideMenuViewController.h"

#define FAV_STORE_LINK(PAGE) [NSString stringWithFormat:@"%@%@%@user_api/favourite_places?page=%@&count=50",LIVE_SERVER,URL_PREFIX,API_VERSION,PAGE]
#define THUMBNAIL_LINK(IMG_LINK) [NSString stringWithFormat:@"%@%@%@",LIVE_SERVER,URL_PREFIX,IMG_LINK]

#define CALL_STORE [NSString stringWithFormat:@"%@%@broadcast_api/call",URL_PREFIX,API_VERSION]

#define FAVSTORE_SYNC_TIME_INTERVAL 24

@interface INFavouritesViewController ()
@property(strong) NSDictionary *splashJson;
@end

@implementation INFavouritesViewController
@synthesize favouriteTableView;
@synthesize favArray;
@synthesize tempstoreArray;
@synthesize countlbl;
@synthesize textPull, textRelease, textLoading, refreshHeaderView, refreshLabel, refreshArrow, refreshSpinner;
@synthesize totalPage;
@synthesize totalRecord;
@synthesize currentPage;
@synthesize globalSearchBar;
@synthesize countHeaderView;
@synthesize lblpullToRefresh;
@synthesize loginView,loginBtn,lblLoginText;
@synthesize showOffersBtn,showStoreViewBtn;
@synthesize callModalViewBackView,callModalView,lblcallModalHeader,lblcallModalHeaderSeperator,callModalTableView,btnCancelcallModal;
@synthesize telArray,selectedPlaceId;
 @synthesize selectedPlaceName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
         [self setupStrings];
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:FALSE];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:TRUE];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:FALSE];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if(![INUserDefaultOperations isCustomerLoggedIn])
    {
        [loginView setHidden:FALSE];
    } else {
        [self loadData];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[self navigationController] setNavigationBarHidden:NO animated:NO];

    //[self setupMenuBarButtonItems];

    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                             style:UIBarButtonItemStylePlain
                                                            target:nil
                                                            action:nil];
    [[self navigationItem] setBackBarButtonItem:back];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadData:) name:IN_STORE_DID_CHANGE_NOTIFICATION object:nil];
    
    [self setUI];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        globalSearchBar.barTintColor = DEFAULT_COLOR;
        [globalSearchBar setTranslucent:NO];
    }
    globalSearchBar.showsCancelButton = YES;
    globalSearchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    globalSearchBar.delegate =  self;
    globalSearchBar.hidden = TRUE;
    [globalSearchBar setFrame:CGRectMake(0, -globalSearchBar.frame.size.height, globalSearchBar.frame.size.width, globalSearchBar.frame.size.height)];
    [countHeaderView setFrame:CGRectMake(0, 0, countHeaderView.frame.size.width, countHeaderView.frame.size.height)];
    [favouriteTableView setFrame:CGRectMake(0, CGRectGetMaxY(countHeaderView.frame)+1, favouriteTableView.frame.size.width, self.view.frame.size.height-countHeaderView.frame.size.height)];
    
    favArray = [[NSMutableArray alloc] init];
    tempstoreArray = [[NSMutableArray alloc] init];
    [self addPullToRefreshHeader];
    
    [loginView setHidden:TRUE];
    showStoreViewBtn.hidden = TRUE;
    showOffersBtn.hidden    = TRUE;
    [favouriteTableView setHidden:TRUE];
    [countHeaderView setHidden:TRUE];
    

}

#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
#pragma set Navigation barbuttons callback
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithTitle:MENU_TITLE style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithTitle:MENU_TITLE style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleRightSideMenu)];
//    if([INUserDefaultOperations isCustomerLoggedIn])
//    {
//        return [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(searchBarBtnClicked:)];
//    }else{
        return nil;
//    }
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)searchBarBtnClicked:(id)sender {
    globalSearchBar.text = @"";
    [globalSearchBar becomeFirstResponder];
    self.navigationItem.rightBarButtonItem = nil;
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(searchBarCancelBtnClicked)];
    globalSearchBar.hidden = FALSE;

    [UIView animateWithDuration:0.3 animations:^{
        [globalSearchBar setFrame:CGRectMake(0, 0, globalSearchBar.frame.size.width, globalSearchBar.frame.size.height)];
        [countHeaderView setFrame:CGRectMake(0, CGRectGetMaxY(globalSearchBar.frame), countHeaderView.frame.size.width, countHeaderView.frame.size.height)];
        [favouriteTableView setFrame:CGRectMake(0, CGRectGetMaxY(countHeaderView.frame)+1, favouriteTableView.frame.size.width, self.view.frame.size.height-(countHeaderView.frame.size.height+globalSearchBar.frame.size.height))];
    }];
}

-(void)searchBarCancelBtnClicked {
    globalSearchBar.text = @"";
    [globalSearchBar resignFirstResponder];
    self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
    [UIView animateWithDuration:0.3 animations:^{
        [globalSearchBar setFrame:CGRectMake(0, -globalSearchBar.frame.size.height, globalSearchBar.frame.size.width, globalSearchBar.frame.size.height)];
        [countHeaderView setFrame:CGRectMake(0, 0, countHeaderView.frame.size.width, countHeaderView.frame.size.height)];
        [favouriteTableView setFrame:CGRectMake(0, CGRectGetMaxY(countHeaderView.frame)+1, favouriteTableView.frame.size.width, self.view.frame.size.height-countHeaderView.frame.size.height)];
    } completion:^(BOOL finished){
        globalSearchBar.hidden = TRUE;
    }];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    INSearchViewController *searchController = [[INSearchViewController alloc] initWithNibName:@"INSearchViewController" bundle:nil] ;
    searchController.title = @"Search";
    searchController.textToSearch = searchBar.text;
    searchController.isStoreSearch = YES;
    [self.navigationController pushViewController:searchController animated:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar{
    [self searchBarCancelBtnClicked];
}
#pragma internal methods
-(void)setUI
{
   // loginView = [CommonCallback setViewPropertiesWithRoundedCorner:loginView];
    loginView.backgroundColor       =   [UIColor whiteColor];
    loginView.clipsToBounds         = YES;
    
    favouriteTableView.backgroundColor  =   [UIColor clearColor];
    favouriteTableView.separatorStyle   =   UITableViewCellSeparatorStyleNone;
    
    countHeaderView.backgroundColor =   [UIColor clearColor];
    countHeaderView.clipsToBounds   = YES;
    
    lblpullToRefresh.font               =   DEFAULT_FONT(15.0);
    lblpullToRefresh.backgroundColor    =   LIGHT_GREEN_COLOR_WITH_ALPHA(0.5);
    lblpullToRefresh.textColor          =   [UIColor whiteColor];
    
    countlbl.font               =   DEFAULT_FONT(15.0);
    countlbl.backgroundColor    =   LIGHT_GREEN_COLOR_WITH_ALPHA(0.5);
    countlbl.textColor          =   [UIColor whiteColor];
    
    lblLoginText.textAlignment    = NSTextAlignmentCenter;
    lblLoginText.backgroundColor  = [UIColor clearColor];
    lblLoginText.font             = DEFAULT_FONT(18);
    lblLoginText.textColor        = DEFAULT_COLOR;
    lblLoginText.numberOfLines    = 15;
    lblLoginText.text             = @"You can add stores to Myshoplocal. It brings the latest offers from your favourite stores in the neighbourhood at your fingertips";
    
    loginBtn.titleLabel.font            =   DEFAULT_BOLD_FONT(18);
    loginBtn.titleLabel.textAlignment   =   NSTextAlignmentCenter;
    loginBtn.backgroundColor            =   [UIColor clearColor];
    [loginBtn setTitle:@"Login" forState:UIControlStateNormal];
    [loginBtn setTitle:@"Login" forState:UIControlStateHighlighted];
    [loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    showStoreViewBtn.titleLabel.font            =   DEFAULT_BOLD_FONT(18);
    showStoreViewBtn.titleLabel.textAlignment   =   NSTextAlignmentCenter;
    showStoreViewBtn.titleLabel.numberOfLines   =   3;
    showStoreViewBtn.backgroundColor            =   BUTTON_COLOR;
    [showStoreViewBtn setTitle:@"Start adding business to your Shoplocal Bag" forState:UIControlStateNormal];
    [showStoreViewBtn setTitle:@"Start adding business to your Shoplocal Bag" forState:UIControlStateHighlighted];
    [showStoreViewBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    showOffersBtn.titleLabel.font           =   DEFAULT_BOLD_FONT(18);
    showOffersBtn.titleLabel.textAlignment  =   NSTextAlignmentCenter;
    showOffersBtn.backgroundColor           =   BUTTON_COLOR;
    [showOffersBtn setTitle:@"See offers" forState:UIControlStateNormal];
    [showOffersBtn setTitle:@"See offers" forState:UIControlStateHighlighted];
    [showOffersBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
//    loginBtn.titleLabel.font     =   DEFAULT_BOLD_FONT(16.0);
//    loginBtn.backgroundColor     =   [UIColor whiteColor];
//    loginBtn.layer.borderColor   = BUTTON_BORDER_COLOR.CGColor;
//    loginBtn.layer.borderWidth   = 2.0;
//    loginBtn.layer.cornerRadius  = 5.0;
//    [loginBtn setTitle:@"Login" forState:UIControlStateNormal];
//    [loginBtn setTitle:@"Login" forState:UIControlStateHighlighted];
//    [loginBtn setTitleColor:BUTTON_COLOR forState:UIControlStateNormal];
//    [loginBtn setTitleColor:BUTTON_OFFWHITE_COLOR forState:UIControlStateHighlighted];

    //callModalView = [CommonCallback setViewPropertiesWithRoundedCorner:callModalView];
    callModalView.backgroundColor =  [UIColor whiteColor];
    callModalView.layer.shadowColor      = [UIColor blackColor].CGColor;
    callModalView.layer.shadowOffset     = CGSizeMake(1, 1);
    callModalView.layer.shadowOpacity    = 1.0;
    callModalView.layer.shadowRadius     = 10.0;
    [callModalViewBackView setHidden:YES];
    [callModalView setHidden:YES];
    
    lblcallModalHeader.textAlignment     = NSTextAlignmentCenter;
    lblcallModalHeader.backgroundColor   = [UIColor clearColor];
    lblcallModalHeader.font              = DEFAULT_BOLD_FONT(20);
    lblcallModalHeader.textColor         = BROWN_COLOR;
    //    lblcallModalHeader.adjustsFontSizeToFitWidth = YES;
    lblcallModalHeader.minimumScaleFactor = 15;
    lblcallModalHeader.text              = @"Select number";
    lblcallModalHeader.numberOfLines     = 2;
    
    lblcallModalHeaderSeperator.backgroundColor   = DEFAULT_COLOR;
    callModalTableView.backgroundColor = [UIColor whiteColor];
    if ([callModalTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [callModalTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    btnCancelcallModal.backgroundColor = [UIColor clearColor];
}

-(void)showLoginModal{
    INCustomerLoginViewController *custLoginController = [[INCustomerLoginViewController alloc] initWithNibName:@"INCustomerLoginViewController" bundle:nil] ;
    custLoginController.delegate = self;
    UINavigationController *loginnavBar=[[UINavigationController alloc]initWithRootViewController:custLoginController];
    UIButton *moreButton1 =  [UIButton buttonWithType:UIButtonTypeCustom];
    [moreButton1 setTitle:@"Cancel" forState:UIControlStateNormal];
    moreButton1.titleLabel.font = [UIFont systemFontOfSize:17.0];
    //[moreButton1 setBackgroundImage:[UIImage imageNamed:@"Transparent_Button.png"] forState:UIControlStateNormal];
    [moreButton1 addTarget:self action:@selector(dismissLoginView) forControlEvents:UIControlEventTouchUpInside];
    [moreButton1 setFrame:CGRectMake(250, 7, 70, 32)];
    [loginnavBar.navigationBar addSubview:moreButton1];
    [loginnavBar.navigationBar setTranslucent:NO];
    [self.navigationController presentViewController:loginnavBar animated:YES completion:nil];
}

-(void)dismissLoginView
{
    DebugLog(@"dismissLoginView %@",[self.navigationController viewControllers]);
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"Dismiss");
        if([INUserDefaultOperations isCustomerLoggedIn])
        {
            [INEventLogger logEvent:@"FavouriteStore_LoginSuccess"];
            [loginView setHidden:TRUE];
            [self loadData];
            [self setupMenuBarButtonItems];
        }
    }];
}

- (IBAction)loginBtnPressed:(id)sender {
    [INEventLogger logEvent:@"FavouriteStore_Login"];
    [self showLoginModal];
}

- (IBAction)showStoreViewBtnPressed:(id)sender {
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:ALL_BUSINESS forKey:IN_CUSTOMER_CHANGE_TAB_MANUALLY_KEY];
    [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_CHANGE_TAB_MANUALLY_NOTIFICATION object:nil userInfo:dictionary];
}

- (IBAction)showOffersBtnPressed:(id)sender {
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:DISCOVER forKey:IN_CUSTOMER_CHANGE_TAB_MANUALLY_KEY];
    [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_CHANGE_TAB_MANUALLY_NOTIFICATION object:nil userInfo:dictionary];
}

-(void)sendFavouritesRequest:(NSString *)urlString
{
    DebugLog(@"url - %@",urlString);
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setValue:[INUserDefaultOperations getCustomerAuthId] forHTTPHeaderField:@"user_id"];
    [urlRequest setValue:[INUserDefaultOperations getCustomerAuthCode] forHTTPHeaderField:@"auth_id"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"%@",self.splashJson);
                                                        [CommonCallback hideProgressHud];
                                                        if(isLoading)
                                                        {
                                                            [self stopLoading];
                                                        }
                                                        if(self.splashJson  != nil) {
                                                            [tempstoreArray removeAllObjects];
                                                            DebugLog(@"--->%@",[self.splashJson  objectForKey:@"success"]);
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                [INUserDefaultOperations setFavStoreSyncDate];
                                                                NSDictionary *maindict = [self.splashJson  objectForKey:@"data"];
                                                                self.totalPage = [[maindict  objectForKey:@"total_page"] integerValue];
                                                                self.totalRecord = [[maindict  objectForKey:@"total_record"] integerValue];
                                                                if(self.currentPage == 1)
                                                                {
                                                                    [self  deleteFavouritesTable];
                                                                    [favArray removeAllObjects];
                                                                }
                                                                [self updateTotalPage:self.totalPage];
                                                                [self updateTotalRecords:self.totalRecord];
                                                                [self updateCurrentPage:self.currentPage];
                                                                [INUserDefaultOperations setCustomerFavouriteMyShoplocalRefresh:FALSE];
                                                                NSArray* jsonArray = [maindict  objectForKey:@"record"];
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *objdict = [jsonArray  objectAtIndex:index];
                                                                    DebugLog(@"\n\n%@\n\n",objdict);
                                                                    storeObj = [[INAllStoreObj alloc] init];
                                                                    storeObj.area = [objdict objectForKey:@"area"];
                                                                    storeObj.areaID = [objdict objectForKey:@"area_id"];
                                                                    storeObj.building = [objdict objectForKey:@"building"];
                                                                    storeObj.city = [objdict objectForKey:@"city"];
                                                                    storeObj.description = [[objdict objectForKey:@"description"] stringByReplacingOccurrencesOfString:@"\\" withString:@""];
                                                                    storeObj.email = [objdict objectForKey:@"email"];
                                                                    storeObj.has_offer = [objdict objectForKey:@"has_offer"];
                                                                    storeObj.ID = [[objdict objectForKey:@"id"] integerValue];
                                                                    storeObj.image_url = [objdict objectForKey:@"image_url"];
                                                                    storeObj.landmark = [objdict objectForKey:@"landmark"];
                                                                    storeObj.mob_no1 = [objdict objectForKey:@"mob_no1"];
                                                                    storeObj.mob_no2 = [objdict objectForKey:@"mob_no2"];
                                                                    storeObj.mob_no3 = [objdict objectForKey:@"mob_no3"];
                                                                    storeObj.tel_no1 = [objdict objectForKey:@"tel_no1"];
                                                                    storeObj.tel_no2 = [objdict objectForKey:@"tel_no2"];
                                                                    storeObj.tel_no3 = [objdict objectForKey:@"tel_no3"];
                                                                    storeObj.name = [[objdict objectForKey:@"name"] stringByReplacingOccurrencesOfString:@"\\" withString:@""];
                                                                    storeObj.place_parent = [[objdict objectForKey:@"place_parent"] integerValue];
                                                                    storeObj.street = [objdict objectForKey:@"street"];
                                                                    storeObj.title = [[objdict objectForKey:@"title"] stringByReplacingOccurrencesOfString:@"\\" withString:@""];
                                                                    storeObj.total_like = [objdict objectForKey:@"total_like"];
                                                                    storeObj.website = [objdict objectForKey:@"website"];
                                                                    storeObj.verified = [objdict objectForKey:@"verified"];
                                                                    [tempstoreArray addObject:storeObj];
                                                                    storeObj = nil;
                                                                    
                                                                    [loginView setHidden:TRUE];
                                                                    showStoreViewBtn.hidden = TRUE;
                                                                    showOffersBtn.hidden    = TRUE;
                                                                    [favouriteTableView setHidden:FALSE];
                                                                    [countHeaderView setHidden:FALSE];
                                                                }
                                                            } else {
                                                                if([self.splashJson  objectForKey:@"code"] != [NSNull null] && [[self.splashJson  objectForKey:@"code"] isEqualToString:CUSTOMER_INVALID_USER_AUTH_CODE]) {
                                                                    DebugLog(@"Got Invalid Authentication Code.");
                                                                    [INUserDefaultOperations showSIAlertView:CUSTOMER_INVALID_USER_AUTH_MESSAGE];
                                                                    [loginView setHidden:FALSE];
                                                                    showStoreViewBtn.hidden = TRUE;
                                                                    showOffersBtn.hidden    = TRUE;
                                                                    [favouriteTableView setHidden:TRUE];
                                                                    [countHeaderView setHidden:TRUE];
                                                                    [INUserDefaultOperations clearCustomerDetails];
                                                                    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:IN_CUSTOMER_LOGIN_LOGOUT_KEY];
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_LOGIN_LOGOUT_NOTIFICATION object:nil userInfo:dictionary];
                                                                    [self setupMenuBarButtonItems];

                                                                } else if([self.splashJson  objectForKey:@"code"] != [NSNull null] && ([[self.splashJson  objectForKey:@"code"] isEqualToString:CUSTOMER_NO_RECORDS_FOUND] || [[self.splashJson  objectForKey:@"code"] isEqualToString:CUSTOMER_NO_FAVSTORE_FOUND])) {
                                                                    if(self.currentPage == 1)
                                                                    {
                                                                        [self  deleteFavouritesTable];
                                                                        [favArray removeAllObjects];
                                                                        self.totalRecord = 0;
                                                                        [INUserDefaultOperations setCustomerFavouriteMyShoplocalRefresh:FALSE];
                                                                        
                                                                        [loginView setHidden:TRUE];
                                                                        showStoreViewBtn.hidden = FALSE;
                                                                        showOffersBtn.hidden    = FALSE;
                                                                        [favouriteTableView setHidden:TRUE];
                                                                        [countHeaderView setHidden:TRUE];
                                                                    }
                                                                }
                                                                else if([self.splashJson  objectForKey:@"message"] != [NSNull null]){
                                                                   [INUserDefaultOperations performSelector:@selector(showAlert:) withObject:[self.splashJson  objectForKey:@"message"] afterDelay:2.0];
                                                                }
                                                                
                                                            }
                                                            if([tempstoreArray count] > 0)
                                                            {
                                                                DebugLog(@"-----%d--------",[tempstoreArray count]);
                                                                [self insertAllStoreListTable];
                                                                [self readAllStoreFromDatabase];
                                                            }
                                                            [favouriteTableView reloadData];
                                                            [self setCountLabel];
                                                        }
                                                        isAdding = NO;
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"%@",self.splashJson);
                                                        DebugLog(@"-%@-%@-%@-%d-",response,error,JSON,response.statusCode);
                                                        DebugLog(@"-%d-%d-",[error code],NSURLErrorCancelled);
                                                        if ([error code] == NSURLErrorCancelled) {
                                                            DebugLog(@"yup operation is cancelled %d",[error code]);
                                                        }else{
                                                            UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                         message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                        delegate:nil
                                                                                               cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                            [av show];
                                                        }
                                                        isAdding = NO;
                                                        if(isLoading)
                                                        {
                                                            [self stopLoading];
                                                        }
                                                        [self readRecordDatabase];
                                                    }];
    [operation start];
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleShrink];
    [MMProgressHUD showWithTitle:HUD_TITLE
                          status:@""
             confirmationMessage:[NSString stringWithFormat:@"Cancel?"]
                     cancelBlock:^{
                         DebugLog(@"Task was cancelled!");
                         [operation cancel];
                     }];
}



#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:callModalTableView]) {
        return 80;
    }else{
        return 125;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([favArray count] == 0)
    {
        [INUserDefaultOperations setCustomerFavouriteState:TRUE];
    } else {
        [INUserDefaultOperations setCustomerFavouriteState:FALSE];
    }
    if ([tableView isEqual:callModalTableView]) {
        return [telArray count];
    }else{
        return [favArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:callModalTableView]) {
        static NSString *CellIdentifier = @"CallCell";
        UIImageView *thumbImg;
        UILabel *lblNumber;
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.backgroundColor = [UIColor whiteColor];
            
            thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(10,15, 50, 50)];
            thumbImg.tag = 1111;
            thumbImg.contentMode = UIViewContentModeScaleToFill;
            thumbImg.image = [UIImage imageNamed:@"call_brown.png"];
            [cell.contentView addSubview:thumbImg];
            
            lblNumber = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame),0, 210, 80)];
            lblNumber.text = @"";
            lblNumber.tag = 2222;
            lblNumber.textColor             = BROWN_COLOR;
            lblNumber.highlightedTextColor  = [UIColor whiteColor];
            lblNumber.font                  = DEFAULT_FONT(23);
            lblNumber.textAlignment         = NSTextAlignmentLeft;
            lblNumber.backgroundColor       = [UIColor clearColor];
            [cell.contentView addSubview:lblNumber];
            
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = BROWN_OFFWHITE_COLOR;
            cell.selectedBackgroundView = bgColorView;
        }
        lblNumber        = (UILabel *)[cell.contentView viewWithTag:2222];
        lblNumber.text   = [telArray objectAtIndex:indexPath.row];
        return cell;
    }else{
        static NSString *CellIdentifier = @"Cell";
        UIView *backView;
        UILabel *lblOffer;
        
         UIButton *btnCall;
        //UIImageView *thumbImg;
        UILabel *lblTitle;
        UILabel *lblDesc;
        
        UIImageView *offerthumbImg;
        
        UIImageView *likethumbImg;
        UILabel *lblLikeCount;
        
        UIImageView *ldistthumbImg;
        UILabel *lblDist;
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.backgroundColor = [UIColor clearColor];

            //[cell.imageView setImage:[UIImage imageNamed:@"table_placeholder.png"]];
            //Initialize Image View with tag 1.(Thumbnail Image)
            backView = [[UIView alloc] initWithFrame:CGRectMake(0, 5, 320, 120)];
            backView.tag = 333;
            backView.backgroundColor =  [UIColor whiteColor];
            //backView = [CommonCallback setViewPropertiesWithRoundedCorner:backView];
            backView.clipsToBounds = YES;
            
            lblOffer = [[UILabel alloc] initWithFrame:CGRectMake(0.0,0, 320.0, 30.0)];
            lblOffer.text = @"";
            lblOffer.tag = 111;
            lblOffer.font = DEFAULT_BOLD_FONT(15);
            lblOffer.textAlignment = NSTextAlignmentCenter;
            lblOffer.textColor = [UIColor whiteColor];
            lblOffer.backgroundColor =  LIGHT_GREEN_COLOR;
            lblOffer.hidden = TRUE;
            [backView addSubview:lblOffer];
            
            btnCall = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnCall setFrame:CGRectMake(0, 20, 80, 80)];
            [btnCall setImage:[UIImage imageNamed:@"Call_white.png"] forState:UIControlStateNormal];
            [btnCall setBackgroundColor:[UIColor clearColor]];
            [btnCall setTitle:@"" forState:UIControlStateNormal];
            [btnCall addTarget:self action:@selector(callBtnTapped:event:) forControlEvents:UIControlEventTouchUpInside];
            [backView addSubview:btnCall];
            
//            thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, 120, 120)];
//            thumbImg.tag = 100;
//            thumbImg.contentMode = UIViewContentModeScaleToFill;
//            [backView addSubview:thumbImg];
            
            offerthumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,71.0,50.0)];
            offerthumbImg.tag = 107;
            offerthumbImg.image = [UIImage imageNamed:@"Offer_Icon1.png"];
            offerthumbImg.contentMode = UIViewContentModeScaleAspectFill;
            [backView addSubview:offerthumbImg];
            
            lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(btnCall.frame)+5,4,230,40.0)];
            lblTitle.text = @"";
            lblTitle.tag = 101;
            lblTitle.font = DEFAULT_BOLD_FONT(15);
            lblTitle.textAlignment = NSTextAlignmentLeft;
            lblTitle.textColor = BROWN_COLOR;
            lblTitle.highlightedTextColor = [UIColor whiteColor];
            lblTitle.backgroundColor =  [UIColor clearColor];
            lblTitle.numberOfLines = 2;
            [backView addSubview:lblTitle];
            
            lblDesc = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(btnCall.frame)+5, CGRectGetMaxY(lblTitle.frame) - 5,230,60)];
            lblDesc.text = @"";
            lblDesc.tag = 102;
            lblDesc.numberOfLines = 3;
            lblDesc.font = DEFAULT_FONT(12);
            lblDesc.textAlignment = NSTextAlignmentLeft;
            lblDesc.textColor = BROWN_COLOR;
            lblDesc.highlightedTextColor = [UIColor whiteColor];
            lblDesc.backgroundColor =  [UIColor clearColor];
            [backView addSubview:lblDesc];
            
            likethumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth(backView.frame) - 67, CGRectGetMaxY(lblDesc.frame)+2,15.0,15.0)];
            likethumbImg.tag = 103;
            likethumbImg.image = [UIImage imageNamed:@"list_fav_icon.png"];
            likethumbImg.contentMode = UIViewContentModeScaleAspectFill;
            [backView addSubview:likethumbImg];
            
            lblLikeCount = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(likethumbImg.frame), CGRectGetMaxY(lblDesc.frame)+2,30.0,15.0)];
            lblLikeCount.text = @"";
            lblLikeCount.tag = 104;
            lblLikeCount.font = DEFAULT_BOLD_FONT(10);
            lblLikeCount.textAlignment = NSTextAlignmentCenter;
            lblLikeCount.textColor = BROWN_COLOR;
            lblLikeCount.highlightedTextColor = [UIColor whiteColor];
            lblLikeCount.backgroundColor =  [UIColor clearColor];
            [backView addSubview:lblLikeCount];
            
            ldistthumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth(backView.frame)-67,CGRectGetMaxY(lblDesc.frame)+2,15.0,15.0)];
            ldistthumbImg.image = [UIImage imageNamed:@"list_distance_icon.png"];
            ldistthumbImg.tag = 105;
            ldistthumbImg.contentMode = UIViewContentModeScaleAspectFill;
            [backView addSubview:ldistthumbImg];
            
            lblDist = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(ldistthumbImg.frame),CGRectGetMaxY(lblDesc.frame)+2,50.0,15.0)];
            lblDist.text = @"";
            lblDist.tag = 106;
            lblDist.font = DEFAULT_BOLD_FONT(10);
            lblDist.textAlignment = NSTextAlignmentCenter;
            lblDist.textColor = BROWN_COLOR;
            lblDist.highlightedTextColor = [UIColor whiteColor];
            lblDist.backgroundColor =  [UIColor clearColor];
            [backView addSubview:lblDist];
            
            [cell.contentView addSubview:backView];
            
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
            bgColorView.tag = 104;
            cell.selectedBackgroundView = bgColorView;
        }
        if ([indexPath row] < [favArray count]) {
            INAllStoreObj *placeObj  = [favArray objectAtIndex:indexPath.row];
            //cell.textLabel.text = placeObj.name;
            //cell.detailTextLabel.text = placeObj.description;
            //cell.detailTextLabel.numberOfLines = 3;
            
            backView        = (UIView *)[cell viewWithTag:333];
            lblOffer        = (UILabel *)[backView viewWithTag:111];
           // thumbImg        = (UIImageView *)[backView viewWithTag:100];
            lblTitle        = (UILabel *)[backView viewWithTag:101];
            lblDesc         = (UILabel *)[backView viewWithTag:102];
            likethumbImg    = (UIImageView *)[backView viewWithTag:103];
            lblLikeCount    = (UILabel *)[backView viewWithTag:104];
            ldistthumbImg   = (UIImageView *)[backView viewWithTag:105];
            lblDist         = (UILabel *)[backView viewWithTag:106];
            offerthumbImg   = (UIImageView *)[backView viewWithTag:107];

            
            
           /* if (placeObj.has_offer != nil && [placeObj.has_offer isEqualToString:@"1"]) {
                //thumbImgview.frame =  CGRectMake(10, 30, 100, 100);
                lblOffer.hidden = FALSE;
                [lblOffer setText:[NSString stringWithFormat:@"Special Offer: %@",placeObj.title]];
                
                thumbImg.frame =  CGRectMake(1,CGRectGetMaxY(lblOffer.frame), thumbImg.frame.size.width, thumbImg.frame.size.height);
                lblTitle.frame =  CGRectMake(CGRectGetMaxX(thumbImg.frame)+5,CGRectGetMaxY(lblOffer.frame), lblTitle.frame.size.width, lblTitle.frame.size.height);
                
                likethumbImg.frame =  CGRectMake(CGRectGetMaxX(lblTitle.frame)+2,CGRectGetMaxY(lblOffer.frame)+10, likethumbImg.frame.size.width, likethumbImg.frame.size.height);
                lblLikeCount.frame =  CGRectMake(CGRectGetMaxX(likethumbImg.frame),CGRectGetMaxY(lblOffer.frame)+10, lblLikeCount.frame.size.width, lblLikeCount.frame.size.height);
            } else {
                //thumbImgview.frame =  CGRectMake(10, 10, 100, 100);
                lblOffer.hidden = TRUE;
                [lblOffer setText:@""];
                
                thumbImg.frame =  CGRectMake(1,0, thumbImg.frame.size.width, thumbImg.frame.size.height);
                lblTitle.frame =  CGRectMake(CGRectGetMaxX(thumbImg.frame)+5,0, lblTitle.frame.size.width, lblTitle.frame.size.height);
                
                likethumbImg.frame =  CGRectMake(CGRectGetMaxX(lblTitle.frame)+2,10, likethumbImg.frame.size.width, likethumbImg.frame.size.height);
                lblLikeCount.frame =  CGRectMake(CGRectGetMaxX(likethumbImg.frame),10, lblLikeCount.frame.size.width, lblLikeCount.frame.size.height);
            }
            lblDesc.frame =  CGRectMake(CGRectGetMaxX(thumbImg.frame)+5,CGRectGetMaxY(lblTitle.frame), lblDesc.frame.size.width, lblDesc.frame.size.height);
            
            ldistthumbImg.frame =  CGRectMake(250,CGRectGetMaxY(lblDesc.frame), ldistthumbImg.frame.size.width, ldistthumbImg.frame.size.height);
            lblDist.frame =  CGRectMake(CGRectGetMaxX(ldistthumbImg.frame),CGRectGetMaxY(lblDesc.frame), lblDist.frame.size.width, lblDist.frame.size.height);
            */
            
            lblOffer.hidden = YES;
            if (placeObj.has_offer != nil && [placeObj.has_offer isEqualToString:@"1"]) {
                backView.layer.borderColor = ORANGE_COLOR.CGColor;
                backView.layer.borderWidth = 5.0;
                offerthumbImg.hidden = FALSE;
            }else{
                backView.layer.borderWidth = 0.0;
                offerthumbImg.hidden = YES;
            }
            
            //DebugLog(@"link-%@",THUMBNAIL_LINK(placeObj.image_url));
            
//            __weak UIImageView *thumbImg_ = thumbImg;
//    //        [thumbImg setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(placeObj.image_url)]
//    //                 placeholderImage:[UIImage imageNamed:@"list_thumbnail.png"]
//    //                          success:^(UIImage *image) {
//    //                              //DebugLog(@"success");
//    //                          }
//    //                          failure:^(NSError *error) {
//    //                              //DebugLog(@"write error %@", error);
//    //                              thumbImg_.image = [UIImage imageNamed:@"list_thumbnail.png"];
//    //                          }];
//            [thumbImg setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(placeObj.image_url)] placeholderImage:[UIImage imageNamed:@"list_thumbnail.png"] completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType) {
//                if (error) {
//                    thumbImg_.image = [UIImage imageNamed:@"list_thumbnail.png"];
//                }
//            }];
            
            [lblTitle setText:placeObj.name];
            [lblDesc setText: placeObj.description];
            
            [lblDist setHidden:TRUE];
            [ldistthumbImg setHidden:TRUE];
            
            if(placeObj.distance != nil) {
                DebugLog(@"%@",placeObj.distance);
                if ([placeObj.distance floatValue] < 1000) {
                    [lblDist setHidden:FALSE];
                    [ldistthumbImg setHidden:FALSE];
                    
                    if([placeObj.distance floatValue]  > -1 && [placeObj.distance floatValue]  < 1)
                        [lblDist setText: [NSString stringWithFormat:@"%.2fm",([placeObj.distance floatValue] * 100)]];
                    else
                        [lblDist setText: [NSString stringWithFormat:@"%.2fkm",([placeObj.distance floatValue])]];
                }
            }
            
            [lblLikeCount setHidden:FALSE];
            [likethumbImg setHidden:FALSE];
            lblLikeCount.text = placeObj.total_like;
        }
        return cell;
    }
}


#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:callModalTableView]) {
        [callModalView setHidden:YES];
        [callModalViewBackView setHidden:YES];
        NSString *selectedTelNumberToCall = [[telArray objectAtIndex:indexPath.row] stringByReplacingOccurrencesOfString:@" " withString:@""];
        DebugLog(@"selectedTelNumberToCall %@",selectedTelNumberToCall);
        NSString *deviceType = [UIDevice currentDevice].model;
        if([deviceType isEqualToString:@"iPhone"])
        {
            UIApplication *myApp = [UIApplication sharedApplication];
            DebugLog(@"selectedTelNumberToCall = -%@-",selectedTelNumberToCall);
            [myApp openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",selectedTelNumberToCall]]];
            
            NSDictionary *callParams = [NSDictionary dictionaryWithObjectsAndKeys:@"FavouriteStore",@"Source",[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"AreaName",selectedPlaceName,@"StoreName",selectedTelNumberToCall,@"Number", nil];
            [INEventLogger logEvent:@"StoreCallDone" withParams:callParams];
            
            [self sendStoreCallRequest:@"StoreCallDone"];
        }
        else
        {
            UIAlertView *alert = [[ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                              message: @"Calling functionality is not available in this device. "
                                                             delegate: nil
                                                    cancelButtonTitle: nil
                                                    otherButtonTitles: @"OK", nil];
            [alert show];
        }
    }else{
        INAllStoreObj *placeObj  = [favArray objectAtIndex:indexPath.row];
        INStoreDetailViewController *detailController = [[INStoreDetailViewController alloc] initWithNibName:@"INStoreDetailViewController" bundle:nil] ;
        detailController.title = placeObj.name;
        detailController.storeId = placeObj.ID;
        if ([placeObj.image_url hasPrefix:@"http"]) {
            detailController.storeImageUrl = [NSURL URLWithString:placeObj.image_url];
        }else{
            detailController.storeImageUrl = [NSURL URLWithString:THUMBNAIL_LINK(placeObj.image_url)];
        }
        [self.navigationController pushViewController:detailController animated:YES];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


-(NSIndexPath *)getIndexpathOfEvent:(id)event
{
    NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:favouriteTableView];
	NSIndexPath *indexPath = [favouriteTableView indexPathForRowAtPoint: currentTouchPosition];
    return indexPath;
}

- (void)callBtnTapped:(id)sender event:(id)event
{
    NSIndexPath *indexPath = [self getIndexpathOfEvent:event];
    if (indexPath != nil)
    {
        INAllStoreObj *placeObj  = [favArray objectAtIndex:indexPath.row];
        DebugLog(@"place %@",placeObj.name);
        if (placeObj.name == nil || [placeObj.name length] == 0)
        {
            lblcallModalHeader.text =  @"Select number";
            selectedPlaceName = @"";
        }
        else {
            lblcallModalHeader.text = placeObj.name;
            selectedPlaceName = placeObj.name;
        }
        
        selectedPlaceId = placeObj.ID;
        if (telArray == nil) {
            telArray = [[NSMutableArray alloc] init];
        }else{
            [telArray removeAllObjects];
        }
        if (placeObj.tel_no1 != nil && ![placeObj.tel_no1 isEqualToString:@""]) {
            [telArray addObject:placeObj.tel_no1];
        }
        if (placeObj.tel_no2 != nil && ![placeObj.tel_no2 isEqualToString:@""]) {
            [telArray addObject:placeObj.tel_no2];
        }
        if (placeObj.tel_no3 != nil && ![placeObj.tel_no3 isEqualToString:@""]) {
            [telArray addObject:placeObj.tel_no3];
        }
        if (placeObj.mob_no1 != nil && ![placeObj.mob_no1 isEqualToString:@""]) {
            [telArray addObject:[NSString stringWithFormat:@"+%@",placeObj.mob_no1]];
        }
        if (placeObj.mob_no2 != nil && ![placeObj.mob_no2 isEqualToString:@""]) {
            [telArray addObject:[NSString stringWithFormat:@"+%@",placeObj.mob_no2]];
        }
        if (placeObj.mob_no3 != nil && ![placeObj.mob_no3 isEqualToString:@""]) {
            [telArray addObject:[NSString stringWithFormat:@"+%@",placeObj.mob_no3]];
        }
        [callModalTableView reloadData];
        if (callModalTableView.contentSize.height < 240) {
            [callModalTableView setFrame:CGRectMake(callModalTableView.frame.origin.x, callModalTableView.frame.origin.y, callModalTableView.frame.size.width, callModalTableView.contentSize.height)];
        }else{
            [callModalTableView setFrame:CGRectMake(callModalTableView.frame.origin.x, callModalTableView.frame.origin.y, callModalTableView.frame.size.width, 240)];
        }
        [callModalView setFrame:CGRectMake(callModalView.frame.origin.x, callModalView.frame.origin.y, callModalView.frame.size.width, callModalTableView.frame.size.height+85)];

        DebugLog(@"temparray %@",telArray);
        if (telArray != nil && telArray.count > 0) {
            [callModalTableView reloadData];
            [callModalViewBackView setHidden:NO];
            [callModalView setHidden:NO];
            [CommonCallback viewtransitionInCompletion:callModalView completion:^{
                [CommonCallback viewtransitionOutCompletion:callModalView completion:nil];
            }];
            
            NSDictionary *callParams = [NSDictionary dictionaryWithObjectsAndKeys:@"FavouriteStore",@"Source",[IN_APP_DELEGATE getActivePlaceNameFromAreaTable],@"AreaName",selectedPlaceName,@"StoreName", nil];
            [INEventLogger logEvent:@"StoreCall" withParams:callParams];
            
            [self sendStoreCallRequest:@"StoreCall"];
        } else {
            [INUserDefaultOperations showSIAlertView:NO_CONTACT_MESSAGE];
        }
    }
}

- (IBAction)btnCancelcallModalPressed:(id)sender {
    [callModalView setHidden:YES];
    [callModalViewBackView setHidden:YES];
}

-(void)sendStoreCallRequest:(NSString *)callType
{
    
    if (![INUserDefaultOperations isCustomerLoggedIn]) {
        return;
    }
    
    NSString *areaID = [IN_APP_DELEGATE getActiveShoplocalPlaceIdFromAreaTable];
    
    DebugLog(@"========================sendStoreCallRequest========================");
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:@"Fool"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSString *register_from = [NSString stringWithFormat:@"%@ v%@",DEVICE_TYPE,version];
    
    NSMutableDictionary *postparams = [[NSMutableDictionary alloc] init];
    //    {"user_id":"${user_id}","auth_id":"${auth_id}","place_id":"106","via":"welcome ","call_type":"type-a","active_area":"1"}
    [postparams setObject:[INUserDefaultOperations getCustomerAuthId] forKey:@"user_id"];
    [postparams setObject:[INUserDefaultOperations getCustomerAuthCode] forKey:@"auth_id"];
    [postparams setObject:[NSString stringWithFormat:@"%d",selectedPlaceId] forKey:@"place_id"];
    [postparams setObject:callType forKey:@"call_type"];
    [postparams setObject:areaID forKey:@"active_area"];
    [postparams setObject:register_from forKey:@"via"];
    
    DebugLog(@"sendStoreCallRequest : postParams -%@-",postparams);
    
    [httpClient postPath:CALL_STORE parameters:postparams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"sendStoreCallRequest : json afnetworking ->%@",json);
        if (json!= nil) {
            NSString* code = [json objectForKey:@"code"];
            if([code isEqualToString:CUSTOMER_INVALID_USER_AUTH_CODE])
            {
                if([INUserDefaultOperations isCustomerLoggedIn]){
                    [INUserDefaultOperations clearCustomerDetails];
                }
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"sendStoreCallRequest : [HTTPClient Error]: %@", error.localizedDescription);
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setFavouriteTableView:nil];
    [self setGlobalSearchBar:nil];
    [self setCountHeaderView:nil];
    [self setLblpullToRefresh:nil];
    [self setLoginBtn:nil];
    [self setLblLoginText:nil];
    [self setLoginView:nil];
    [self setShowStoreViewBtn:nil];
    [self setShowOffersBtn:nil];
    [super viewDidUnload];
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (UIImage *)scale:(UIImage *)image toSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}

-(void)pageRequest:(NSString *)pageNum
{
    if([IN_APP_DELEGATE networkavailable])
    {
        isAdding = YES;
        [self sendFavouritesRequest:FAV_STORE_LINK(pageNum)];
    }  else   {
        isAdding = NO;
        if(isLoading)
        {
            [self stopLoading];
        }
        [INUserDefaultOperations showOfflineAlert];
    }
}

-(void)loadData
{
    [self readAllStoreFromDatabase];
    if([favArray count] > 0)
    {
        [favouriteTableView reloadData];
        [self readRecordDatabase];
        [self setCountLabel];
        
        DebugLog(@"diff - >%d",[INUserDefaultOperations getDateDifferenceInHours:[INUserDefaultOperations getFavStoreSyncDate]]);
        
        if((([INUserDefaultOperations getDateDifferenceInHours:[INUserDefaultOperations getFavStoreSyncDate]]) > FAVSTORE_SYNC_TIME_INTERVAL) || ([INUserDefaultOperations getCustomerMyShoplocalRefresh] == TRUE))
        {
            if([IN_APP_DELEGATE networkavailable])
            {
                [favArray removeAllObjects];
                self.currentPage = 1;
                [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
                [INEventLogger logEvent:@"FavouriteStore_Fetch"];
            }
        }else{
            [loginView setHidden:TRUE];
            showStoreViewBtn.hidden = TRUE;
            showOffersBtn.hidden    = TRUE;
            [favouriteTableView setHidden:FALSE];
            [countHeaderView setHidden:FALSE];
        }
    }else{
        //http request
        DebugLog(@"file not exists");
        self.currentPage = 1;
        [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
        [INEventLogger logEvent:@"FavouriteStore_Fetch"];
    }
}

- (void)reloadData:(NSNotification *)notification
{
    DebugLog(@"Reload data");
    [self loadData];
}


-(void)setCountLabel
{
    DebugLog(@"-----%d--------%d------",[favArray count],self.totalRecord);
    self.countlbl.text = [NSString stringWithFormat:@"%d/%d",[favArray count],self.totalRecord];
    DebugLog(@"-----%@--------",countlbl.text);
}

#pragma mark PULL TO REFRESH
- (void)setupStrings{
    textPull = @"Pull down to refresh...";
    textRelease = @"Release to refresh...";
    textLoading = @"Loading please wait...";
}

- (void)addPullToRefreshHeader {
    refreshHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0 - REFRESH_HEADER_HEIGHT, 320, REFRESH_HEADER_HEIGHT)];
    refreshHeaderView.backgroundColor = [UIColor clearColor];
    
    refreshLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, REFRESH_HEADER_HEIGHT)];
    refreshLabel.backgroundColor = [UIColor clearColor];
    refreshLabel.font = DEFAULT_BOLD_FONT(15);
    refreshLabel.textAlignment = NSTextAlignmentCenter;
    refreshLabel.textColor = DEFAULT_COLOR;
    refreshArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:PULLTOREFRESH_BARROW]];
    refreshArrow.frame = CGRectMake(floorf((REFRESH_HEADER_HEIGHT - 2) / 2),
                                    (floorf(REFRESH_HEADER_HEIGHT - 44) / 2),
                                    27, 44);
    
    refreshSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    refreshSpinner.frame = CGRectMake(floorf(floorf(REFRESH_HEADER_HEIGHT - 2) / 2), floorf((REFRESH_HEADER_HEIGHT - 20) / 2), 20, 20);
    refreshSpinner.hidesWhenStopped = YES;
    
    [refreshHeaderView addSubview:refreshLabel];
    [refreshHeaderView addSubview:refreshArrow];
    [refreshHeaderView addSubview:refreshSpinner];
    [favouriteTableView addSubview:refreshHeaderView];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if ([scrollView isEqual:callModalTableView]) {
        return;
    }
    if (isLoading) return;
    isDragging = YES;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([scrollView isEqual:callModalTableView]) {
        return;
    }
    if (isLoading) {
        // Update the content inset, good for section headers
        if (scrollView.contentOffset.y > 0)
            favouriteTableView.contentInset = UIEdgeInsetsZero;
        else if (scrollView.contentOffset.y >= -REFRESH_HEADER_HEIGHT)
            favouriteTableView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (isDragging && scrollView.contentOffset.y < 0) {
        // Update the arrow direction and label
        [UIView animateWithDuration:0.25 animations:^{
            if (scrollView.contentOffset.y < -REFRESH_HEADER_HEIGHT) {
                // User is scrolling above the header
                refreshLabel.text = self.textRelease;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
            } else {
                // User is scrolling somewhere within the header
                refreshLabel.text = self.textPull;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
            }
        }];
    } else {
        CGFloat height = scrollView.frame.size.height;
        
        CGFloat contentYoffset = scrollView.contentOffset.y;
        
        CGFloat distanceFromBottom = scrollView.contentSize.height - contentYoffset;
        
        if(distanceFromBottom <= height)
        {
            if(isAdding==TRUE) {
                DebugLog(@"<<<<<<<<<<<<<<<");
                return;
            } else {
                DebugLog(@"end of the table page = %d", self.currentPage);
                if(self.currentPage < self.totalPage)
                {
                    self.currentPage = self.currentPage + 1;
                    [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
                }
            }
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if ([scrollView isEqual:callModalTableView]) {
        return;
    }
    if (isLoading) return;
    isDragging = NO;
    if (scrollView.contentOffset.y <= -REFRESH_HEADER_HEIGHT) {
        // Released above the header
        [self startLoading];
    }
}

- (void)startLoading {
    isLoading = YES;
    // Show the header
    [UIView animateWithDuration:0.3 animations:^{
        favouriteTableView.contentInset = UIEdgeInsetsMake(REFRESH_HEADER_HEIGHT, 0, 0, 0);
        refreshLabel.text = self.textLoading;
        refreshArrow.hidden = YES;
        [refreshSpinner startAnimating];
    }];
    if([IN_APP_DELEGATE networkavailable])
    {
        [favArray removeAllObjects];
        self.currentPage = 1;
        [self pageRequest:[NSString stringWithFormat:@"%d",self.currentPage]];
        [INEventLogger logEvent:@"FavouriteStore_Fetch"];
    }
    else
    {
        [self stopLoading];
    }
    // Refresh action!
    //[self refresh];
}

- (void)stopLoading {
    isLoading = NO;
    // Hide the header
    [UIView animateWithDuration:0.3 animations:^{
        favouriteTableView.contentInset = UIEdgeInsetsZero;
        [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
    }
                     completion:^(BOOL finished) {
                         [self performSelector:@selector(stopLoadingComplete)];
                     }];
}

- (void)stopLoadingComplete {
    // Reset the header
    refreshLabel.text = self.textPull;
    refreshArrow.hidden = NO;
    [refreshSpinner stopAnimating];
}

#pragma Table Operations
- (void)insertAllStoreListTable
{
    sqlite3_stmt    *statement;
    int lastrowid;
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        for (int index=0; index<[tempstoreArray count] ; index++)
        {
            INAllStoreObj *tempstoreObj = [tempstoreArray objectAtIndex:index];
            NSString *insertSQL = [NSString stringWithFormat:
                                   @"INSERT OR REPLACE INTO FAVOURITES (PLACEID, PLACE_PARENT, NAME, DESCRIPTION, BUILDING, STREET, LANDMARK, AREA, CITY, MOB_NO1, MOB_NO2, MOB_NO3, TEL_NO1, TEL_NO2, TEL_NO3, IMG_URL, EMAIL, WEBSITE, HAS_OFFER, TITLE, TOTAL_LIKE, AREAID, VERIFIED) VALUES (\"%d\",\"%d\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",tempstoreObj.ID,tempstoreObj.place_parent,tempstoreObj.name,[tempstoreObj.description stringByReplacingOccurrencesOfString:@"\"" withString:@"'"],tempstoreObj.building,tempstoreObj.street,tempstoreObj.landmark,tempstoreObj.area,tempstoreObj.city,tempstoreObj.mob_no1, tempstoreObj.mob_no2, tempstoreObj.mob_no3, tempstoreObj.tel_no1, tempstoreObj.tel_no2, tempstoreObj.tel_no3, tempstoreObj.image_url, tempstoreObj.email,tempstoreObj.website,tempstoreObj.has_offer,tempstoreObj.title,tempstoreObj.total_like, tempstoreObj.areaID, tempstoreObj.verified];
            //DebugLog(@"%@",insertSQL);
            const char *insert_stmt = [insertSQL UTF8String];
            sqlite3_prepare_v2(shoplocalDB, insert_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                lastrowid = sqlite3_last_insert_rowid(shoplocalDB);
                // DebugLog(@"inserted id========%d", lastrowid);
            } else {
                DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
            }
            sqlite3_finalize(statement);
        }
    }
    sqlite3_close(shoplocalDB);
}

-(void) readAllStoreFromDatabase {
    
    //NSString *activeId = [IN_APP_DELEGATE getActivePlaceIdFromAreaTable];
    
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    // Init the expense Array
    if(favArray == nil) {
        DebugLog(@"new  expense array created");
        favArray = [[NSMutableArray alloc] init];
    } else {
        DebugLog(@"old  expense array used");
        [favArray removeAllObjects];
    }
	// Open the database from the users filessytem
	if(sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
        //NSString *querySQL = [NSString stringWithFormat:@"select * from FAVOURITES where AREAID like \"%@\" COLLATE NOCASE",activeId];
        NSString *querySQL = [NSString stringWithFormat:@"select * from FAVOURITES"];
        DebugLog(@"query--->%@",querySQL);
        const char *sqlStatement = [querySQL UTF8String];
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(shoplocalDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				// Read the data from the result row
                //int count = sqlite3_data_count(compiledStatement);
                //DebugLog(@"count=====>>%d",count);
				NSInteger lid = sqlite3_column_int(compiledStatement, 1);
				NSInteger lplace_parent = sqlite3_column_int(compiledStatement, 2);
				NSString *lname = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                NSString *ldescription = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
                NSString *lbuilding = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];
                NSString *lstreet = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 6)];
                NSString *llandmark = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 7)];
				NSString *larea = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 8)];
				NSString *lcity = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 9)];
                NSString *lmob_no1 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 10)];
                NSString *lmob_no2 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 11)];
                NSString *lmob_no3 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 12)];
                NSString *ltel_no1 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 13)];
                NSString *ltel_no2 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 14)];
                NSString *ltel_no3 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 15)];
                NSString *limage_url = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 16)];
                NSString *lemail = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 17)];
                NSString *lwebsite = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 18)];
				NSString *lhas_offer = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 19)];
				NSString *ltitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 20)];
                NSString *ltotal_like = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 21)];
                NSString *lareaid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 22)];
                NSString *lverified = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 23)];
                
				INAllStoreObj *tempstoreObj = [[INAllStoreObj alloc] init];
                tempstoreObj.ID = lid;
                tempstoreObj.place_parent = lplace_parent;
                tempstoreObj.name = lname;
                tempstoreObj.description = ldescription;
                tempstoreObj.building = lbuilding;
                tempstoreObj.street = lstreet;
                tempstoreObj.landmark = llandmark;
                tempstoreObj.area = larea;
                tempstoreObj.city = lcity;
                tempstoreObj.mob_no1 = lmob_no1;
                tempstoreObj.mob_no2 = lmob_no2;
                tempstoreObj.mob_no3 = lmob_no3;
                tempstoreObj.tel_no1 = ltel_no1;
                tempstoreObj.tel_no2 = ltel_no2;
                tempstoreObj.tel_no3 = ltel_no3;
                tempstoreObj.image_url = limage_url;
                tempstoreObj.email = lemail;
                tempstoreObj.website = lwebsite;
                tempstoreObj.has_offer = lhas_offer;
                tempstoreObj.title = ltitle;
                tempstoreObj.total_like = ltotal_like;
                tempstoreObj.areaID = lareaid;
                tempstoreObj.verified = lverified;
				[favArray addObject:tempstoreObj];
				tempstoreObj = nil;
            }
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
		
	}
	sqlite3_close(shoplocalDB);
}

-(void)deleteFavouritesTable
{
    //NSString *activeId = [IN_APP_DELEGATE getActivePlaceIdFromAreaTable];
    
    sqlite3_stmt    *statement;
    
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        //NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM FAVOURITES WHERE AREAID like \"%@\" COLLATE NOCASE",activeId];
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM FAVOURITES"];

        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(shoplocalDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"All rows deleted");
        } else {
            DebugLog(@"Deletion failed.");
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
}

- (void)updateCurrentPage:(NSInteger)page
{
    sqlite3_stmt    *statement;
    int lastrowid;
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        NSString *updateSQL = [NSString stringWithFormat:@"UPDATE FAV_RECORD set CURRENT_PAGE=\"%d\" WHERE STORE like \"%@\" COLLATE NOCASE",page,[INUserDefaultOperations getSelectedStore]];
        DebugLog(@"%@",updateSQL);
        
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(shoplocalDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            lastrowid = sqlite3_last_insert_rowid(shoplocalDB);
            // DebugLog(@"inserted id========%d", lastrowid);
        } else {
            DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
}

- (void)updateTotalPage:(NSInteger)pageCount
{
    sqlite3_stmt    *statement;
    int lastrowid;
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        //NSString *updateSQL = [NSString stringWithFormat:@"UPDATE FAV_RECORD set TOTAL_PAGE=\"%d\" WHERE STORE like \"%@\" COLLATE NOCASE",pageCount,[INUserDefaultOperations getSelectedStore]];
        NSString *updateSQL = [NSString stringWithFormat:@"UPDATE FAV_RECORD set TOTAL_PAGE=\"%d\"",pageCount];
        DebugLog(@"%@",updateSQL);
        
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(shoplocalDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            lastrowid = sqlite3_last_insert_rowid(shoplocalDB);
            // DebugLog(@"inserted id========%d", lastrowid);
        } else {
            DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
}

- (void)updateTotalRecords:(NSInteger)recordsCount
{
    sqlite3_stmt    *statement;
    int lastrowid;
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK)
    {
        //NSString *updateSQL = [NSString stringWithFormat:@"UPDATE FAV_RECORD set TOTAL_RECORD=\"%d\" WHERE STORE like \"%@\" COLLATE NOCASE",recordsCount,[INUserDefaultOperations getSelectedStore]];
        NSString *updateSQL = [NSString stringWithFormat:@"UPDATE FAV_RECORD set TOTAL_RECORD=\"%d\"",recordsCount];
        DebugLog(@"%@",updateSQL);
        
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(shoplocalDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            lastrowid = sqlite3_last_insert_rowid(shoplocalDB);
            // DebugLog(@"inserted id========%d", lastrowid);
        } else {
            DebugLog(@"%s", sqlite3_errmsg(shoplocalDB));
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(shoplocalDB);
}

-(void)readRecordDatabase {
    
    const char *dbpath = [[IN_APP_DELEGATE databasePath] UTF8String];
    
	// Open the database from the users filessytem
	if(sqlite3_open(dbpath, &shoplocalDB) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
        //NSString *querySQL = [NSString stringWithFormat:@"select TOTAL_PAGE,TOTAL_RECORD,CURRENT_PAGE from FAV_RECORD WHERE STORE like \"%@\" COLLATE NOCASE",[INUserDefaultOperations getSelectedStore]];
        NSString *querySQL = [NSString stringWithFormat:@"select TOTAL_PAGE,TOTAL_RECORD,CURRENT_PAGE from FAV_RECORD"];
        const char *sqlStatement = [querySQL UTF8String];
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(shoplocalDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				// Read the data from the result row
				self.totalPage = sqlite3_column_int(compiledStatement, 0);
				self.totalRecord = sqlite3_column_int(compiledStatement, 1);
                self.currentPage = sqlite3_column_int(compiledStatement, 2);
            }
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
		
	}
	sqlite3_close(shoplocalDB);
    DebugLog(@"total page %d- total record %d- current page %d",self.totalPage,self.totalRecord,self.currentPage);
}

-(void)hideLoginControlsInLoginView:(int)type
{
    loginBtn.hidden         = type;
    
    showStoreViewBtn.hidden = !type;
    showOffersBtn.hidden    = !type;
}
@end
