//
//  INMerchantVerifyViewController.m
//  shoplocal
//
//  Created by Kirti Nikam on 23/12/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INMerchantVerifyViewController.h"
#import "CommonCallback.h"
#import "INAppDelegate.h"

#define REGISTER_MERCHANT [NSString stringWithFormat:@"%@%@%@merchant_api/new_merchant",LIVE_SERVER,URL_PREFIX,API_VERSION]
#define MOBILE_NUMBER(CODE,NUMBER)  [NSString stringWithFormat:@"%@%@",[CODE stringByReplacingOccurrencesOfString:@"+" withString:@""],NUMBER]
#define SET_PASSWORD_MERCHANT [NSString stringWithFormat:@"%@%@%@merchant_api/new_set_password",LIVE_SERVER,URL_PREFIX,API_VERSION]

@interface INMerchantVerifyViewController ()

@end

@implementation INMerchantVerifyViewController
@synthesize scrollView;
@synthesize backView;
@synthesize lblMessage,txtActivationCode,imgSeparator;
@synthesize btnNext,btnResend,lblQuest,lblUnderLine;
@synthesize logindelegate;
@synthesize btnEdit;
@synthesize mobileNumber;
@synthesize codeNumber;
@synthesize message;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setUI];
    if([codeNumber isEqualToString:@""])
    {
        codeNumber = @"+91";
    }
    [btnEdit setTitle:[NSString stringWithFormat:@"Edit: %@%@",codeNumber,mobileNumber] forState:UIControlStateNormal];
    lblMessage.text = message;
    
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    viewTap.numberOfTapsRequired = 1;
    viewTap.cancelsTouchesInView = NO;
    [self.view  addGestureRecognizer:viewTap];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setScrollView:nil];
    [self setBackView:nil];
    [self setLblMessage:nil];
    [self setTxtActivationCode:nil];
    [self setImgSeparator:nil];
    [self setBtnNext:nil];
    [self setLblQuest:nil];
    [self setBtnResend:nil];
    [self setLblUnderLine:nil];
    [self setBtnEdit:nil];
    [super viewDidUnload];
}

#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma Internal Methods
-(void)setUI{
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.clipsToBounds      = YES;
    //scrollView.layer.cornerRadius = 8.0;
    
    //backView = [CommonCallback setViewPropertiesWithRoundedCorner:backView];
    backView.backgroundColor =  [UIColor whiteColor];
    backView.clipsToBounds          = YES;
    
    lblMessage.textColor            = BROWN_COLOR;
    lblMessage.font                 = DEFAULT_FONT(15);
    lblMessage.numberOfLines        = 0;
    
    txtActivationCode.textColor     = BROWN_COLOR;
    txtActivationCode.font          = DEFAULT_FONT(18);
    
    lblQuest.textColor              = BROWN_COLOR;
    lblQuest.font                   = DEFAULT_FONT(14);
    
    lblUnderLine.backgroundColor    = BROWN_COLOR;
    
    btnNext.titleLabel.font         = DEFAULT_BOLD_FONT(18);
//    btnNext.layer.cornerRadius    = 8.0;
    btnNext.backgroundColor         = GREEN_COLOR_FOR_BUTTON;
    [btnNext setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnNext setTitleColor:[UIColor colorWithWhite:0.5 alpha:0.5] forState:UIControlStateHighlighted];
    
    btnResend.titleLabel.font       = DEFAULT_BOLD_FONT(15);
//    btnResend.layer.cornerRadius    = 8.0;
    btnResend.backgroundColor       = GRAY_COLOR_FOR_BUTTON;
    [btnResend setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnResend setTitleColor:[UIColor colorWithWhite:0.5 alpha:0.5] forState:UIControlStateHighlighted];
    
    btnEdit.titleLabel.font         = DEFAULT_BOLD_FONT(15);
    [btnEdit setTitleColor:BROWN_COLOR forState:UIControlStateNormal];
    [btnEdit setTitleColor:BROWN_OFFWHITE_COLOR forState:UIControlStateHighlighted];
}

- (void)tapDetected:(UIGestureRecognizer *)sender {
    [self.view endEditing:YES];
    CGPoint tapLocation = [sender locationInView:scrollView];
    UIView *view = [scrollView hitTest:tapLocation withEvent:nil];
    
    if (![view isKindOfClass:[UIButton class]]) {
        [self scrollTobottom];
    }
}
-(void)scrollTobottom
{
    if(scrollView.contentSize.height > scrollView.frame.size.height)  //if scrollview content height is greater than scrollview total height then remove the added height from scrollview
    {
        [scrollView setContentSize: CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height - 60)];
    }
    [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (IBAction)btnNextPressed:(id)sender {
    [self scrollTobottom];
    if([txtActivationCode.text isEqualToString:@""])
    {
        [INUserDefaultOperations showAlert:@"Please enter activation code."];
    }
    else if([txtActivationCode.text length] < 6)
    {
        [INUserDefaultOperations showAlert:@"Activation code must be at least 6 characters in length."];
    } else {
        if ([IN_APP_DELEGATE networkavailable]) {
            [INEventLogger logEvent:@"MSignUp_VerifyCode"];
            [self sendActivationCodeRequest];
        } else {
            [INUserDefaultOperations showOfflineAlert];
        }
    }
}

- (IBAction)btnResendPressed:(id)sender {
    [self scrollTobottom];
    if ([IN_APP_DELEGATE networkavailable]) {
        //[self sendVerificationCodeRequest]; //For calling Server API to get verification code
        [self sendNumberRegisterRequest];
    } else {
        [INUserDefaultOperations showOfflineAlert];
    }
}

- (IBAction)btnEditPressed:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"Dismiss");
        [INUserDefaultOperations setMerchantTermsConditionsValue:@"0"];
    }];
}


//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
//    if(textField==usernameText) {
//        NSString *usernameString = [usernameText.text stringByReplacingCharactersInRange:range withString:string];
//        return !([usernameString length] > 10);
//    } else {
//        return YES;
//    }
//}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(txtActivationCode.frame.origin.y > scrollView.contentOffset.y)
    {
        if(textField == txtActivationCode)
        {
            [scrollView setContentOffset:CGPointMake(0, scrollView.contentOffset.y+60) animated:YES];
            [scrollView setContentSize: CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height + 200)];
        }
    }
    return YES;
}

-(void)sendActivationCodeRequest
{
    //[hud show:YES];
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    DebugLog(@"link--->%@,%@,%@",SET_PASSWORD_MERCHANT,MOBILE_NUMBER(codeNumber, mobileNumber),txtActivationCode.text);
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:SET_PASSWORD_MERCHANT] cachePolicy:NO timeoutInterval:30.0];
    NSMutableDictionary *postReq = [[NSMutableDictionary alloc] init];
    [postReq setObject:MOBILE_NUMBER(codeNumber, mobileNumber) forKey:@"mobile"];
    [postReq setObject:txtActivationCode.text forKey:@"password"];
    [postReq setObject:txtActivationCode.text forKey:@"verification_code"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postReq
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (!jsonData) {
        DebugLog(@"Got an error: %@", error);
    }
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setValue:@"PUT" forHTTPHeaderField:@"X-HTTP-Method-Override"];
    
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody: jsonData];
    connectionsetActivationCode = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(void)sendNumberRegisterRequest
{
    DebugLog(@"number--->%@",MOBILE_NUMBER(codeNumber, mobileNumber));
    DebugLog(@"link--->%@",REGISTER_MERCHANT);
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:REGISTER_MERCHANT] cachePolicy:NO timeoutInterval:30.0];
    NSMutableDictionary *postReq = [[NSMutableDictionary alloc] init];
    [postReq setObject:MOBILE_NUMBER(codeNumber,mobileNumber) forKey:@"mobile"];
    [postReq setObject:DEVICE_TYPE forKey:@"register_from"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postReq
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (!jsonData) {
        DebugLog(@"Got an error: %@", error);
    }
    [urlRequest setHTTPBody: jsonData];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setHTTPMethod:@"POST"];
    connectionRegisterNumber = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}


#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    DebugLog(@"Error: %@", [error localizedDescription]);
    //[hud hide:YES];
    [CommonCallback hideProgressHud];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError* error;
    //  [hud hide:YES];
    //[CommonCallback hideProgressHud];
    if (connection==connectionRegisterNumber)
    {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            DebugLog(@"json: %@", json);
            if(json != nil) {
                if([[json  objectForKey:@"success"] isEqualToString:@"true"]) {
                    if([[json  objectForKey:@"code"] isEqualToString:@"-119"])
                    {
                        [INEventLogger logEvent:@"MSignUp_New"];
                    } else {
                        [INEventLogger logEvent:@"MSignUp_Return"];
                    }
                    [INUserDefaultOperations setMerchantTermsConditionsValue:@"0"];
                    [INUserDefaultOperations setMerchantCountryCode:[codeNumber stringByReplacingOccurrencesOfString:@"+" withString:@""]];
                    [INUserDefaultOperations setMerchantDetails:mobileNumber pass:nil];
                    [INUserDefaultOperations clearMerchantDetails];
                    [INUserDefaultOperations setMerchantLoginState:[json  objectForKey:@"message"] visitedPage:2];
                    if ([IN_APP_DELEGATE networkavailable]) {
                         lblMessage.text = [json  objectForKey:@"message"];
                    } else {
                        [INUserDefaultOperations showOfflineAlert];
                    }
                } else {
                    
                    [INUserDefaultOperations showAlert:[json  objectForKey:@"message"]];
                }
            } else {
                [INUserDefaultOperations showAlert:@"No response from server. Please try again."];
                //[signupIndicatior stopAnimating];
                //[phoneNumberScreenforgotlbl setHidden:TRUE];
            }
        }
        [CommonCallback hideProgressHud];
        responseAsyncData = nil;
    }else if (connection == connectionsetActivationCode) {
        if(responseAsyncData != nil)
        {
            NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseAsyncData  options:kNilOptions error:&error];
            DebugLog(@"connectionsetPasswordWithCode json: %@", json);
            if(json != nil) {
                if([[json  objectForKey:@"success"] isEqualToString:@"true"]) {
                    NSString* lmessage = [json objectForKey:@"message"];
                    DebugLog(@"%@",lmessage);
                    if (self.logindelegate && [self.logindelegate respondsToSelector:@selector(loginUserWithActivationCode:pass:showAddStore:)]) {
                        DebugLog(@"%@ %@",mobileNumber,txtActivationCode);
                        [self.logindelegate loginUserWithActivationCode:mobileNumber pass:txtActivationCode.text showAddStore:1];
                        [INUserDefaultOperations clearMerchantLoginState];
                        [INEventLogger logEvent:@"MSignUp_VerifyCodeDone"];
                        [FBAppEvents logEvent:FBAppEventNameCompletedRegistration
                                   parameters:@{ @"Login Type" : @"Mobile",
                                                 @"User Type"  : @"Merchant"} ];
                    }
                }
                else if ([[json  objectForKey:@"success"] isEqualToString:@"false"]) {
                    [CommonCallback hideProgressHud];
                    [INUserDefaultOperations showAlert:[json objectForKey:@"message"]];
                    [INEventLogger logEvent:@"MSignUp_VerifyCodeInvalid"];
                } else {
                    [CommonCallback hideProgressHud];
                    [INUserDefaultOperations showAlert:@"No response from server. Please try again."];
                }
            } else {
               [CommonCallback hideProgressHud]; 
            }
        } else {
            [CommonCallback hideProgressHud];
        }
        responseAsyncData = nil;
    }
}
@end
