//
//  INBroadcastDetailObj.m
//  shoplocal
//
//  Created by Sagar Mody on 12/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INBroadcastDetailObj.h"

@implementation INBroadcastDetailObj
@synthesize ID,place_ID,areaID;
@synthesize title,description,name;
@synthesize date,offerdate;
@synthesize is_offered;
@synthesize type,url,tags,verified;
@synthesize image_title1,image_title2,image_title3;
@synthesize image_url1,image_url2,image_url3;
@synthesize thumb_url1,thumb_url2,thumb_url3;
@synthesize distance,latitude,longitude;
@synthesize mob_no1,mob_no2,mob_no3;
@synthesize tel_no1,tel_no2,tel_no3;
@synthesize user_like;
@synthesize total_like,total_share,total_view;
@end
