//
//  INCustBroadCastDetailsViewController.h
//  shoplocal
//
//  Created by Kirti Nikam on 17/09/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import "MWPhotoBrowser.h"
#import "FaceBookShareViewController.h"
#import "INCustomerLoginViewController.h"


#define FROM_NEWS_FEED 0
#define FROM_STORE_DETAILS 1

@interface INCustBroadCastDetailsViewController : UIViewController<UIActionSheetDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,FacebookProtocolDelegate,MWPhotoBrowserDelegate,INCustomerLoginViewControllerDelegate>{
    MBProgressHUD *hud;
}

@property (nonatomic, readwrite) int viewOpenedFrom;

@property (nonatomic, readwrite) int postType;
@property (nonatomic, copy) NSString *postlikeType;
@property (nonatomic, copy) NSString *postTitle;
@property (nonatomic, copy) NSString *postDescription;
@property (nonatomic, copy) NSString *postimageUrl;
@property (nonatomic, copy) NSString *postimageTitle;
@property (nonatomic, copy) NSString *storeName;
@property (nonatomic, readwrite) NSInteger placeId;
@property (nonatomic, copy) NSString *postOfferDateTime;
@property (nonatomic, copy) NSString *postIsOffered;
@property (nonatomic, copy) NSString *postDateTime;

@property (nonatomic, readwrite) NSInteger postId;
@property (strong, nonatomic) NSMutableArray *galleryArray;
@property (strong, nonatomic) NSMutableArray *photos;

@property (strong, nonatomic) IBOutlet UIView *postView;
@property (strong, nonatomic) IBOutlet UILabel *titlelbl;
@property (strong, nonatomic) IBOutlet UILabel *descriptionlbl;
@property (strong, nonatomic) IBOutlet UIImageView *postimageView;
@property (strong, nonatomic) IBOutlet UIButton *likepostBtn;
@property (strong, nonatomic) IBOutlet UIButton *sharepostBtn;
@property (strong, nonatomic) IBOutlet UIImageView *likepostBtnImageView;
@property (strong, nonatomic) IBOutlet UIImageView *sharepostBtnImageView;



@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UILabel *lblFav;
@property (strong, nonatomic) IBOutlet UILabel *lblShare;
@property (strong, nonatomic) IBOutlet UIButton *buyBtn;
@property (strong, nonatomic) IBOutlet UIButton *gotoStoreBtn;

@property (nonatomic, copy) NSString *likescountString;
@property (nonatomic, copy) NSString *sharecountString;

@property (copy, nonatomic) NSMutableArray *telArray;
@property (nonatomic, copy) NSString *selectedTelNumberToCall;


@property (strong, nonatomic) IBOutlet UIView *buyBtnBackView;
@property (nonatomic, readwrite) NSInteger SHOW_BUY_BTN;
@property (strong, nonatomic) IBOutlet UIButton *addStoreToMyShoplocalBtn;

@property (strong, nonatomic) IBOutlet UIImageView *imgSeperator1;
@property (strong, nonatomic) IBOutlet UIImageView *imgSeperator2;


@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttonCollection;
@property (strong, nonatomic) IBOutlet UILabel *lblPostedOn;
@property (strong, nonatomic) IBOutlet UIImageView *imgSeperator3;
@property (strong, nonatomic) IBOutlet UILabel *lblOfferDate;

@property (strong, nonatomic) IBOutlet UIView *callModalViewBackView;
@property (strong, nonatomic) IBOutlet UIView *callModalView;
@property (strong, nonatomic) IBOutlet UILabel *lblcallModalHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblcallModalHeaderSeperator;

@property (strong, nonatomic) IBOutlet UITableView *callModalTableView;
@property (strong, nonatomic) IBOutlet UIButton *btnCancelcallModal;

- (IBAction)btnCancelcallModalPressed:(id)sender;


- (IBAction)likepostBtnPressed:(UIButton *)sender;
- (IBAction)sharepostBtnPressed:(UIButton *)sender;
- (IBAction)buyBtnPressed:(UIButton *)sender;
- (IBAction)gotoStoreBtnPressed:(UIButton *)sender;
- (IBAction)addStoreToMyShoplocalBtnPressed:(UIButton *)sender;
- (IBAction)touch_down_BtnEventCallBack:(UIButton *)sender;
@end
