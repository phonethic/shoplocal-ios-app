//
//  INWebViewController.m
//  shoplocal
//
//  Created by Kirti Nikam on 03/10/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INWebViewController.h"
#import "CommonCallback.h"
#import "INAppDelegate.h"

@interface INWebViewController ()

@end

@implementation INWebViewController
@synthesize urlString;
@synthesize webView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:TRUE];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:TRUE];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:FALSE];
    UIButton *locatonButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1100];
    [locatonButton setHidden:FALSE];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    webView.delegate = self;
    webView.scalesPageToFit = TRUE;
    
    if ([urlString isEqualToString:PRIVACY_LINK] || [urlString isEqualToString:TERMS_AND_CONDITIONS_LINK]) {
            NSFileManager *fileManager      = [NSFileManager defaultManager];
            NSArray *paths                  = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory    = [paths objectAtIndex:0];
            NSString* filePath;
            if ([urlString isEqualToString:PRIVACY_LINK]) {
                filePath  = [documentsDirectory stringByAppendingPathComponent:@"privacy.html"];
            }else{
                filePath  = [documentsDirectory stringByAppendingPathComponent:@"terms.html"];
            }
            DebugLog(@"filePath = %@" , filePath);
            
            NSURL *url = nil;
            
            if ([fileManager fileExistsAtPath:filePath])
            {
                DebugLog(@"file exists");
                BOOL loadFromFile  = YES;
                if ([urlString isEqualToString:PRIVACY_LINK] && ([INUserDefaultOperations getDateDifferenceInHours:[INUserDefaultOperations getPrivacySyncDate]]) >= 24) {
                    loadFromFile = NO;
                }else if ([urlString isEqualToString:TERMS_AND_CONDITIONS_LINK] && ([INUserDefaultOperations getDateDifferenceInHours:[INUserDefaultOperations getTermsSyncDate]]) >= 24)
                {
                    loadFromFile = NO;
                }
                
                if(loadFromFile)
                {
                    DebugLog(@"load from file");
                    url = [NSURL fileURLWithPath:filePath];
                    NSURLRequest *request = [NSURLRequest requestWithURL:url];
                    [webView loadRequest:request];
                    
                } else {
                    DebugLog(@"file exists but more than 24 hours");
                    [self sendRequest:filePath];
                }
            }else{
                DebugLog(@"file not exists");
                [self sendRequest:filePath];
            }
    }else{
        [self sendRequest:nil];
    }
}

-(void)sendRequest:(NSString *)filePath{
    if ([IN_APP_DELEGATE networkavailable]) {
        
        NSURL *url = [NSURL URLWithString:urlString];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [webView loadRequest:request];
        
        if ([urlString isEqualToString:PRIVACY_LINK] || [urlString isEqualToString:TERMS_AND_CONDITIONS_LINK]) {
            NSData *urlData = [NSData dataWithContentsOfURL:url];
            [urlData writeToFile:filePath atomically:YES];
            
            if ([urlString isEqualToString:PRIVACY_LINK]) {
                [INUserDefaultOperations setPrivacySyncDate];
            }else if([urlString isEqualToString:TERMS_AND_CONDITIONS_LINK]){
                [INUserDefaultOperations setTermsSyncDate];
            }
        }
    }else{
        [INUserDefaultOperations showOfflineAlert];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self.webView setDelegate:nil];
    [self.webView stopLoading];
//    [self setWebView:nil];
    [super viewDidUnload];
}

-(void)dealloc
{
    [self.webView setDelegate:nil];
    [self.webView stopLoading];

}
#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
#pragma mark - Web View Delegate Methods
- (void)webViewDidStartLoad:(UIWebView *)lwebView
{
    DebugLog(@"webViewDidStartLoad");
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleFade];
    [MMProgressHUD showWithTitle:@"Loading"
                          status:@""
             confirmationMessage:@"Cancel?"
                     cancelBlock:^{
                         DebugLog(@"Task was cancelled!");
                         [lwebView stopLoading];
                         [INUserDefaultOperations showSIAlertView:@"You have cancelled loading."];
                     }];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    DebugLog(@"webViewDidFinishLoad");
    [CommonCallback hideProgressHud];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    DebugLog(@"didFailLoadWithError : %@",error);
    [CommonCallback hideProgressHudWithError];
}
@end
