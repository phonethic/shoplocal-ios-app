//
//  INUserDefaultOperations.h
//  shoplocal
//
//  Created by Rishi on 03/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface INUserDefaultOperations : NSObject

+(void)setStoreIds;
+(NSString *)storeIdForKey:(NSString *)key;
+(void)setSelectedStore:(NSString *)value;
+(NSString *)getSelectedStore;
//Merchant
+(BOOL)isMerchantLoggedIn;
+(void)setMerchantDetails:(NSString *)idvalue pass:(NSString *)passvalue;
+(void)setMerchantAuthDetails:(NSString *)idvalue pass:(NSString *)codevalue;
+(NSString *)getMerchantId;
+(NSString *)getMerchantAuthId;
+(NSString *)getMerchantAuthCode;
+(void)clearMerchantDetails;
+(void)setTermsConditionsValue:(NSString *)value;
+(NSString *)getTermsConditionsValue;
+(void)setMerchantCountryCode:(NSString *)value;
+(NSString *)getMerchantCountryCode;
+(void)setMerchantValidateLoginDate:(NSDate *)value;
+(NSDate *)getMerchantValidateLoginDate;
//Customer
+(BOOL)isCustomerLoggedIn;
+(void)setCustomerDetails:(NSString *)loginFrom userid:(NSString *)idvalue pass:(NSString *)passvalue;
+(void)setCustomerDetails:(NSString *)idvalue pass:(NSString *)passvalue;
+(void)setCustomerAuthDetails:(NSString *)idvalue pass:(NSString *)codevalue;
+(NSString *)getCustomerId;
+(NSString *)getCustomerAuthId;
+(NSString *)getCustomerAuthCode;
+(void)clearCustomerDetails;
+(void)setLatitude:(double)value;
+(double)getLatitude;
+(void)setLongitude:(double)value;
+(double)getLongitude;
+(void)setCustomerCountryCode:(NSString *)value;
+(NSString *)getCustomerCountryCode;

+(void)setWelcomeScreenState:(NSString *)value;
+(NSString *)getWelcomeScreenState;

+(void)showAlert:(NSString*) message;
+(void)showOfflineAlert;
+(void)showSIAlertView:(NSString*)message;

+(void)setCustomerDistance:(int)value;
+(int)getCustomerDistance;

+(void)setCustomerDistanceTemp:(int)value;
+(int)getCustomerDistanceTemp;

+(void)setAreaListGetRequestExipirationDate:(NSDate *)value;
+(NSDate *)getAreaListGetRequestExipirationDate;

+(void)setTourFinishedState:(BOOL)value;
+(BOOL)getTourFinishedState;

+(void)setRefreshState:(BOOL)value;
+(BOOL)getRefreshState;

+(NSString *)getCustomerLoginFrom;

+(void)setCustomerLoginState:(NSString *)text visitedPage:(int)page;
+(void)setMerchantLoginState:(NSString *)text visitedPage:(int)page;

+(NSString *)getCustomerLoginStateWithText;
+(int)getCustomerLoginStateWithVisitedPage;

+(NSString *)getMerchantLoginStateWithText;
+(int)getMerchantLoginStateWithVisitedPage;

+(void)clearCustomerLoginState;
+(void)clearMerchantLoginState;

+(void)setCustomerTermsConditionsValue:(NSString *)value;
+(NSString *)getCustomerTermsConditionsValue;

+(void)setMerchantTermsConditionsValue:(NSString *)value;
+(NSString *)getMerchantTermsConditionsValue;


+(void)setCustomerLoginStateInTour:(int)value;
+(int)getCustomerLoginStateInTour;

+(void)setAppVersion:(NSString *)value;
+(NSString *)getAppVersion;

/*****NSDATE CATEGORIES******/
+(void)setOffersSyncDate;
+(NSDate *)getOffersSyncDate;
+(void)setAllStoreSyncDate;
+(NSDate *)getAllStoreSyncDate;
+(void)setFavStoreSyncDate;
+(NSDate *)getFavStoreSyncDate;
+(NSDate *)getSearchSyncDate;
+(void)setSearchSyncDate;
+(void)setDailySyncDate;
+(NSDate *)getDailySyncDate;
+(void)setMonthlySyncDate;
+(NSDate *)getMonthlySyncDate;
+(NSInteger)getDateDifferenceInSeconds:(NSDate *)startDate endDate:(NSDate *)endDate;
+(NSInteger)getDateDifferenceInMinutes:(NSDate *)startDate endDate:(NSDate *)endDate;
+(NSInteger)getDateDifferenceInHours:(NSDate *)expireDate;
+(NSInteger)getDateDifferenceInDays:(NSDate *)expireDate;
+(NSInteger)getAgeInYears:(NSDate *)birthDate;


+(NSDate *)getTermsSyncDate;
+(void)setTermsSyncDate;
+(NSDate *)getPrivacySyncDate;
+(void)setPrivacySyncDate;
/////////////////////////////////
+(void)setAppStartDate;
+(NSDate *)getAppStartDate;
+(void)setAppEndDate;
+(NSDate *)getAppEndDate;
+(void)setAppTotalUsageTime:(NSInteger)totalTime;
+(NSInteger)getAppTotalUsageTime;
+(void)setInfoScreenCount:(NSInteger)totalCount;
+(NSInteger)getInfoScreenCount;
/////////////////////////////////
+(void)setTalkNowDate;
+(NSDate *)getTalkNowDate;
+(void)setEditStoreState:(BOOL)lval;
+(BOOL)getEditStoreState;
+(void)setCustomerSignInDate;
+(NSDate *)getCustomerSignInDate;
+(void)setCustomerFavouriteState:(BOOL)lval;
+(BOOL)getCustomerFavouriteState;
+(void)setCustomerFavouriteMyShoplocalRefresh:(BOOL)lval;
+(BOOL)getCustomerMyShoplocalRefresh;
+(void)setCustomerSpecialDatesState:(BOOL)lval;
+(BOOL)getCustomerSpecialDatesState;
+(void)setAppNotLaunchedDate;
+(NSDate *)getAppNotLaunchedDate;
+(void)setAppRaiterState:(BOOL)lval;
+(BOOL)getAppRaiterState;
//////////////////////////////////
//===Set Is Local notification already set or not===
// Below notification will be added first time only, if notification appears and user clicks on that then delete that notification, deleted notification will be never schedule again.
//    -   AppNotLaunched
//    -   EditStore
//    -   TalkNow
//
//    -   CustomerSignIn
//    -   CustomerSpecialDates
//    -   CustomerFavourite

+(void)setAppNotLaunchedNotificationState:(BOOL)lval;
+(BOOL)isAppNotLaunchedNotificationAlreadySet;
+(void)setTalkNowNotificationState:(BOOL)lval;
+(BOOL)isTalkNowNotificationAlreadySet;
+(void)setEditStoreNotificationState:(BOOL)lval;
+(BOOL)isEditStoreNotificationAlreadySet;

+(void)setCustomerSignInNotificationState:(BOOL)lval;
+(BOOL)isCustomerSignInNotificationAlreadySet;
+(void)setCustomerFavouriteNotificationState:(BOOL)lval;
+(BOOL)isCustomerFavouriteNotificationAlreadySet;
+(void)setCustomerSpecialDatesNotificationState:(BOOL)lval;
+(BOOL)isCustomerSpecialDatesNotificationAlreadySet;
/////////////////////////////////


/*****LOCAL NOTIFICATIONS******/
#define APP_NOT_LAUNCHED_LNTYPE @"App_NotLaunched"
#define TALKNOW_LNTYPE @"Talk_Now"
#define EDITSTORE_LNTYPE @"Edit_Store"
#define CUSTOMERFAVOURITESTORE_LNTYPE @"Customer_Favourite"
#define CUSTOMERSPECIALDATE_LNTYPE @"Customer_SpecialDate"
#define CUSTOMERSIGN_LNTYPE @"Customer_SignIn"


+(void)setTalkNowNotification;
+(void)setEditStoreNotification;
+(void)setCustomerSignInNotification;
+(void)setCustomerFavouriteNotification;
+(void)setCustomerSpecialDatesNotification;
+(void)setAppNotLaunchedNotification;
+(void)setWeekendMerchantOffersNotification;
+(void)setWeekendCustomerOffersNotification;
+(void)cancelAppNotLaunchedNotification;
+(void)cancelCustomerSignInNotification;

//Set mechant Count In area
+(void)setMerchantCountInArea:(NSDictionary *)areaDict;
+(NSString *)getMerchantCountInArea:(NSString *)areaName;

//set oper graph share enabled
+(void)setIsOpenGraphShareEnabled:(BOOL)idvalue;
+(BOOL)isOpenGraphShareEnabled;
@end
