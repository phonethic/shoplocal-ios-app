//
//  INMerchantViewController.h
//  shoplocal
//
//  Created by Rishi on 12/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "FXImageView.h"
#import <MessageUI/MFMailComposeViewController.h>

@class INStoreDetailObj;
@interface INMerchantViewController : UIViewController <UIScrollViewDelegate,MFMailComposeViewControllerDelegate>
{
    BOOL pageControlUsed;
    INStoreDetailObj *storeObj;
    sqlite3 *shoplocalDB;
    MBProgressHUD *hud;
}
@property (readwrite, nonatomic) int switchView;

@property (strong, nonatomic) IBOutlet UIButton *merchantLoginBtn;
@property (strong, nonatomic) IBOutlet UIButton *talkNowBtn;
@property (strong, nonatomic) NSMutableArray *storeArray;
@property (readwrite, nonatomic) int rowcount;

@property (strong, nonatomic) IBOutlet UITableView *mainTableView;

@property (strong, nonatomic) IBOutlet UIView *offerBackView;

- (IBAction)newpostBtnPressed:(id)sender;
- (IBAction)viewpostBtnPressed:(id)sender;
- (IBAction)addstoreBtnPressed:(id)sender;
- (IBAction)editstoreBtnPressed:(id)sender;
- (IBAction)managegalleryBtnPressed:(id)sender;
- (IBAction)logoutBtnPressed:(id)sender;
- (IBAction)reportsBtnPressed:(id)sender;
- (IBAction)fbBtnPressed:(id)sender;
- (IBAction)twitterBtnPressed:(id)sender;
- (IBAction)sureShopBtnPressed:(id)sender;
- (IBAction)contactUsBtnPressed:(id)sender;
@end
