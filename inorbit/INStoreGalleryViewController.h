//
//  INStoreGalleryViewController.h
//  shoplocal
//
//  Created by Rishi on 23/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "VCGridView.h"
#import "MWPhotoBrowser.h"
#import "CLImageEditor.h"

@interface INStoreGalleryViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate,VCGridViewDelegate, VCGridViewDataSource,MWPhotoBrowserDelegate,UITextViewDelegate,CLImageEditorDelegate>
{
    VCGridView *_gridView;
	BOOL _isInputViewVisible;
}
@property (strong, nonatomic) NSMutableArray *galleryArray;
@property (strong, nonatomic) NSMutableArray *photos;
@property (nonatomic, readwrite) NSInteger storeId;
@property (nonatomic) BOOL newMedia;
@property (nonatomic,strong) UIImage *editedImage;

@property (nonatomic, retain) VCGridView *gridView;


@property (strong, nonatomic) IBOutlet UIView *addImageDescriptionView;
@property (strong, nonatomic) IBOutlet UILabel *lbldescriptionViewHeader;
@property (strong, nonatomic) IBOutlet UILabel *lbldescriptionViewContent;

@property (strong, nonatomic) IBOutlet UIButton *descriptionViewSetBtn;
@property (strong, nonatomic) IBOutlet UIButton *descriptionViewSkipBtn;
@property (strong, nonatomic) IBOutlet UITextView *descriptionViewtitleTextView;

- (IBAction)descriptionViewSetBtnPressed:(id)sender;
- (IBAction)descriptionViewSkipBtnPressed:(id)sender;
@end
