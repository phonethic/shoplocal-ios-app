//
//  SideMenuViewController.m
//  MFSideMenuDemo
//
//  Created by Michael Frederick on 3/19/12.

#import "SideMenuViewController.h"
#import "RATreeView.h"
#import "RADataObject.h"

#import "INAppDelegate.h"

#import "INAllStoreViewController.h"
#import "INSearchViewController.h"
#import "INMerchantLoginViewController.h"
#import "INOffersStreamViewController.h"
#import "INCustomerLoginViewController.h"
#import "INFavouritesViewController.h"
#import "INCustomerProfileViewController.h"
#import "INCustomerSettingsViewController.h"
#import "INFBWebViewController.h"
#import "INAboutUsViewController.h"
#import "INChangeLocationViewController.h"
#import "INMyShoplocalOffersViewController.h"
#import "INContactUsViewController.h"
#import "NMSplashViewController.h"
#import "INShareThisAppViewController.h"

#import "SIAlertView.h"


@interface SideMenuViewController()<RATreeViewDelegate, RATreeViewDataSource>
@property (strong, nonatomic) NSMutableArray *data;
@property (strong, nonatomic) id expanded;
@property (weak, nonatomic) RATreeView *treeView;
@property (strong, nonatomic) IBOutlet UIButton *moreButton;
@property (strong, nonatomic) UIView *footerView;
@end

@implementation SideMenuViewController

@synthesize sideMenu;
@synthesize sideMenuDict;
@synthesize launchscreenType;

@synthesize data,expanded,treeView;
@synthesize current,previous;
@synthesize moreButton;
@synthesize footerView;
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    if([[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."][0] intValue] >= 7) {
//        CGRect statusBarViewRect = [[UIApplication sharedApplication] statusBarFrame];
//        float heightPadding = statusBarViewRect.size.height+self.navigationController.navigationBar.frame.size.height;
//        self.treeView.contentInset = UIEdgeInsetsMake(heightPadding, 0.0, 0.0, 0.0);
//        self.treeView.contentOffset = CGPointMake(0.0, -heightPadding);
//    }
//    self.treeView.frame = self.view.bounds;
    
    CGRect statusBarViewRect = [[UIApplication sharedApplication] statusBarFrame];
    CGRect windowFrame = [[UIScreen mainScreen] bounds];
    treeView.frame = CGRectMake(0, 0,  self.view.frame.size.width, windowFrame.size.height - (statusBarViewRect.size.height + CGRectGetHeight(footerView.frame)));
    footerView.frame = CGRectMake(0,CGRectGetMaxY(treeView.frame), self.view.frame.size.width,footerView.frame.size.height);
    
//    [self switchToFromLoginMyProfile:[INUserDefaultOperations isCustomerLoggedIn]];
//    [self switchToFromSetupBusiness:[INUserDefaultOperations isMerchantLoggedIn]];
}

- (void)viewDidAppear:(BOOL)animated
{
//    [super viewDidAppear:animated];
//    if (launchscreenType == LAUNCH_SCREEN_IS_LOCATION) {
//        current = CHANGE_LOCATION;
//    }else if (launchscreenType == LAUNCH_SCREEN_IS_SPLASHSCREEN) {
//        current = TOUR;
//    }else
    if (launchscreenType == LAUNCH_SCREEN_IS_ALL_BUSINESS) {
        current = ALL_BUSINESS;
    }
    launchscreenType = 0;
    DebugLog(@"current %@",current);
    [self setSelectedRow:current];
}

- (void) viewDidLoad {
    [super viewDidLoad];
    
    /*
    RADataObject *sec0row1 = [RADataObject dataObjectWithName:ALL_BUSINESS children:nil];
    RADataObject *sec0row2 = [RADataObject dataObjectWithName:DISCOVER children:nil];
    RADataObject *sec0row3 = [RADataObject dataObjectWithName:MY_SHOPLOCAL children:nil];
    RADataObject *section0 = [RADataObject dataObjectWithName:SECTION0
                                                     children:[NSArray arrayWithObjects:sec0row1, sec0row2,sec0row3, nil]];
    section0.collapase = FALSE;
    section0.expanded = TRUE;
    section0.isHeader = TRUE;
    
    RADataObject *sec1row1 = [RADataObject dataObjectWithName:LOGIN children:nil];
    RADataObject *sec1row2 = [RADataObject dataObjectWithName:CHANGE_LOCATION children:nil];
    RADataObject *sec1row3 = [RADataObject dataObjectWithName:SETTINGS children:nil];
    RADataObject *section1 = [RADataObject dataObjectWithName:SECTION1
                                                     children:[NSArray arrayWithObjects:sec1row1, sec1row2,sec1row3, nil]];
//    section1.collapase = FALSE;
//    section1.expanded = TRUE;
    section1.isHeader = TRUE;

    RADataObject *sec2row0 = [RADataObject dataObjectWithName:MYFAV_BUSINESS children:nil];
    RADataObject *section2 = [RADataObject dataObjectWithName:SECTION2
                                                     children:[NSArray arrayWithObjects:sec2row0,nil]];
    section2.isHeader = TRUE;

//    RADataObject *sec3row1 = [RADataObject dataObjectWithName:TOUR children:nil];
    RADataObject *sec3row2 = [RADataObject dataObjectWithName:ABOUTUS children:nil];
    RADataObject *sec3row3 = [RADataObject dataObjectWithName:SHARE_THIS_APP children:nil];
    RADataObject *sec3row4 = [RADataObject dataObjectWithName:CONTACTUS children:nil];
    RADataObject *sec3row5 = [RADataObject dataObjectWithName:FACEBOOK children:nil];
    RADataObject *sec3row6 = [RADataObject dataObjectWithName:TWITTER children:nil];
    RADataObject *section3 = [RADataObject dataObjectWithName:SECTION3
                                                     children:[NSArray arrayWithObjects: sec3row2, sec3row3, sec3row4, sec3row5,sec3row6,nil]];
    section3.isHeader = TRUE;

    RADataObject *sec4row1 = [RADataObject dataObjectWithName:SETUP_BUSINESS children:nil];
    RADataObject *section4 = [RADataObject dataObjectWithName:SECTION4
                                                     children:[NSArray arrayWithObjects:sec4row1,nil]];
    section4.isHeader = TRUE;

    //    self.expanded = notebook1;
    
    self.data = [[NSMutableArray alloc] initWithObjects:section0,section1,section2,section3,section4, nil];
    */
    
    RADataObject *sec0row1 = [RADataObject dataObjectWithName:ALL_BUSINESS children:nil];
    RADataObject *sec0row2 = [RADataObject dataObjectWithName:DISCOVER children:nil];
    RADataObject *sec0row3 = [RADataObject dataObjectWithName:MY_SHOPLOCAL children:nil];
    RADataObject *sec0row4 = [RADataObject dataObjectWithName:SHARE_THIS_APP children:nil];
    RADataObject *section0 = [RADataObject dataObjectWithName:SECTION0
                                                     children:[NSArray arrayWithObjects:sec0row1, sec0row2,sec0row3,sec0row4, nil]];
    section0.collapase = FALSE;
    section0.expanded = TRUE;
    section0.isHeader = TRUE;
    
    RADataObject *section1 = [RADataObject dataObjectWithName:SECTION1 children:nil];
    section1.isHeader = TRUE;
    
    self.data = [[NSMutableArray alloc] initWithObjects:section0,section1, nil];
    
//    UIImageView *backImageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
//    backImageView.image = [UIImage imageNamed:@"DrawerBg.png"];
//    backImageView.contentMode = UIViewContentModeRedraw;
//    [self.view addSubview:backImageView];
    
    self.view.backgroundColor = [UIColor colorWithRed:182.0/255.0 green:206.0/255.0 blue:127/255.0 alpha:1.0];
    
    RATreeView *ltreeView = [[RATreeView alloc] initWithFrame:self.view.frame];
    ltreeView.delegate = self;
    ltreeView.dataSource = self;
    ltreeView.editing = FALSE;
    ltreeView.separatorStyle = RATreeViewCellSeparatorStyleNone;
    [ltreeView setBackgroundColor:[UIColor clearColor]];
    ltreeView.rowsExpandingAnimation    = RATreeViewRowAnimationLeft;
    ltreeView.rowsCollapsingAnimation   = RATreeViewRowAnimationLeft;
    
//    UINavigationBar *tempNavigationBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
//    [tempNavigationBar setBarStyle:UIBarStyleDefault];
//    tempNavigationBar.tintColor = [UIColor blackColor];
//    
//    UINavigationItem *navItem = [UINavigationItem alloc];
//    navItem.title = @"Shoplocal";
//    [tempNavigationBar pushNavigationItem:navItem animated:false];
//    
//    ltreeView.treeHeaderView = tempNavigationBar;
    
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,  self.view.frame.size.width, 44)];
    headerView.backgroundColor = [UIColor colorWithRed:186/255.0 green:204/255.0  blue:155/255.0 alpha:0.5];//BROWN_OFFWHITE_COLOR;
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 265, 44)];
    lbl.backgroundColor = [UIColor clearColor];
    lbl.tag = 420;
    lbl.text =  @"Shoplocal";
    lbl.font = DEFAULT_BOLD_FONT(18);
    lbl.adjustsFontSizeToFitWidth = YES;
    lbl.numberOfLines = 4;
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.textColor = [UIColor colorWithWhite:0 alpha:0.4];
    lbl.contentScaleFactor = 0.7;
    //lbl.shadowColor = [UIColor blackColor];
    //lbl.shadowOffset = CGSizeMake(-1, 0);
    [headerView addSubview:lbl];
    
    ltreeView.treeHeaderView = headerView;
    
    
    footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,  265, 80)];
    footerView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];//[UIColor clearColor];//BROWN_OFFWHITE_COLOR;
    
    moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    moreButton.frame = CGRectMake(0, 5, 270, 75);
    [moreButton setTitle:@"" forState:UIControlStateNormal];
    [moreButton setTitle:@"" forState:UIControlStateHighlighted];
    //    [moreButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    [moreButton setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.5] forState:UIControlStateHighlighted];
    [moreButton.titleLabel setFont:DEFAULT_BOLD_FONT(20.0)];
    [moreButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    //[moreButton setBackgroundColor:[UIColor colorWithRed:186/255.0 green:204/255.0  blue:155/255.0 alpha:0.4]];
    [moreButton setBackgroundColor:[UIColor clearColor]];
    [moreButton addTarget:self action:@selector(moreSideBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:moreButton];
    
//    UIImageView *sellImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5,5, 101, 115)];
//    sellImageView.image = [UIImage imageNamed:@"Sell.png"];
//    sellImageView.contentMode = UIViewContentModeRedraw;
//    [footerView addSubview:sellImageView];
    
    UILabel *lblSell = [[UILabel alloc] initWithFrame:CGRectMake(5 ,0, footerView.frame.size.width-10 , 50)];
    lblSell.textColor = [UIColor whiteColor];
    lblSell.font = DEFAULT_FONT(40.0);
    lblSell.backgroundColor = [UIColor clearColor];
    lblSell.highlightedTextColor = [UIColor whiteColor];
    lblSell.textAlignment = NSTextAlignmentCenter;
    lblSell.numberOfLines = 4;
    lblSell.text = @"SELL";
    [footerView addSubview:lblSell];
    
    UILabel *lblSellDetail = [[UILabel alloc] initWithFrame:CGRectMake(5 ,CGRectGetMaxY(lblSell.frame) - 5, footerView.frame.size.width-10 , 25)];
    lblSellDetail.textColor = [UIColor whiteColor];
    lblSellDetail.font = DEFAULT_FONT(15);
    lblSellDetail.backgroundColor = [UIColor clearColor];
    lblSellDetail.highlightedTextColor = [UIColor whiteColor];
    lblSellDetail.textAlignment = NSTextAlignmentCenter;
    lblSellDetail.numberOfLines = 4;
    lblSellDetail.text = @"Setup your business for free";
    [footerView addSubview:lblSellDetail];
    
   // ltreeView.treeFooterView = footerView;
    
    CGRect statusBarViewRect = [[UIApplication sharedApplication] statusBarFrame];
    CGRect windowFrame = [[UIScreen mainScreen] bounds];

    ltreeView.frame = CGRectMake(0, 0,  self.view.frame.size.width, windowFrame.size.height - (statusBarViewRect.size.height + CGRectGetHeight(footerView.frame)));
    footerView.frame = CGRectMake(0,CGRectGetMaxY(ltreeView.frame), self.view.frame.size.width,footerView.frame.size.height);
    
    self.treeView = ltreeView;
//    [ltreeView expandRowForItem:section0 withRowAnimation:RATreeViewRowAnimationLeft]; //expands Row
//    [ltreeView setBackgroundColor:UIColorFromRGB(0xF7F7F7)];
    
    [ltreeView reloadData];
    [self.view addSubview:treeView];
    [self.view addSubview:footerView];
    
    NSString *selectedArea = [IN_APP_DELEGATE getActivePlaceNameFromAreaTable];
    if (selectedArea != nil && ![selectedArea isEqualToString:@""]) {
        lbl.text =  selectedArea;
    }
    
    //initialAnimation = 0;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleCustomerLoginLogoutNotification:) name:IN_CUSTOMER_LOGIN_LOGOUT_NOTIFICATION object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMerchantLoginLogoutNotification:) name:IN_MERCHANT_LOGIN_LOGOUT_NOTIFICATION object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleChangeLocationNotification:) name:IN_CUSTOMER_CHANGE_LOCATION_NOTIFICATION object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleChangeTabManuallyNotification:) name:IN_CUSTOMER_CHANGE_TAB_MANUALLY_NOTIFICATION object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLocalNotification:) name:IN_CUSTOMER_SHOW_LOCAL_NOTIFICATION object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)moreSideBtnPressed:(id)sender {
    DebugLog(@"moreSideBtnPressed");
    [INEventLogger logEvent:@"Tab_Sell"];
    [self.sideMenu setMenuState:MFSideMenuStateClosed];
    [IN_APP_DELEGATE switchViewController];
    
//    if (moreButton.isSelected) {
//        [moreButton setTitle:@"MORE" forState:UIControlStateNormal];
//        [moreButton setTitle:@"MORE" forState:UIControlStateHighlighted];
//        moreButton.selected = FALSE;
//    }else{
//        [moreButton setTitle:@"LESS" forState:UIControlStateNormal];
//        [moreButton setTitle:@"LESS" forState:UIControlStateHighlighted];
//        moreButton.selected = TRUE;
//    }
//    [self.treeView reloadData];
}


#pragma mark TreeView Delegate methods
- (CGFloat)treeView:(RATreeView *)treeView heightForRowForItem:(id)item treeNodeInfo:(RATreeNodeInfo *)treeNodeInfo
{
    RADataObject *tempItem = (RADataObject *)item;
    if ([tempItem.name isEqualToString:SECTION0]) {
        return 0;
    }
    else if([tempItem.name isEqualToString:SECTION1])
    {
         return 30;
    } else {
        return 74;
    }
}

- (NSInteger)treeView:(RATreeView *)treeView indentationLevelForRowForItem:(id)item treeNodeInfo:(RATreeNodeInfo *)treeNodeInfo
{
    DebugLog(@"%@-%d",((RADataObject *)item).name,treeNodeInfo.treeDepthLevel);
    return 3 * treeNodeInfo.treeDepthLevel;
}

- (BOOL)treeView:(RATreeView *)treeView shouldExpandItem:(id)item treeNodeInfo:(RATreeNodeInfo *)treeNodeInfo
{
    return YES;
}

- (BOOL)treeView:(RATreeView *)ltreeView shouldItemBeExpandedAfterDataReload:(id)item treeDepthLevel:(NSInteger)treeDepthLevel
{
    if (((RADataObject *)item).expanded) {
        return YES;
    }
    return NO;
}
- (void)treeView:(RATreeView *)ltreeView willExpandRowForItem:(id)item treeNodeInfo:(RATreeNodeInfo *)treeNodeInfo{
    [ltreeView scrollToRowForItem:item atScrollPosition:RATreeViewScrollPositionTop animated:YES];
}

- (void)treeView:(RATreeView *)ltreeView didExpandRowForItem:(id)item treeNodeInfo:(RATreeNodeInfo *)treeNodeInfo{
    if (((RADataObject *)item).collapase) {
        UITableViewCell *cell = [ltreeView cellForItem:item];
        UILabel *lblExpandCollapse =   (UILabel *)[cell viewWithTag:102];
        lblExpandCollapse.text = @"-";
    }
}

- (BOOL)treeView:(RATreeView *)ltreeView shouldCollapaseRowForItem:(id)item treeNodeInfo:(RATreeNodeInfo *)treeNodeInfo{
    if (((RADataObject *)item).collapase) {
        return YES;
    }
    return NO;
}

- (void)treeView:(RATreeView *)ltreeView didCollapseRowForItem:(id)item treeNodeInfo:(RATreeNodeInfo *)treeNodeInfo{
    if (((RADataObject *)item).collapase) {
        UITableViewCell *cell = [ltreeView cellForItem:item];
        UILabel *lblExpandCollapse =   (UILabel *)[cell viewWithTag:102];
        lblExpandCollapse.text = @"+";
    }
}

- (void)treeView:(RATreeView *)treeView willDisplayCell:(UITableViewCell *)cell forItem:(id)item treeNodeInfo:(RATreeNodeInfo *)treeNodeInfo
{
//    if (treeNodeInfo.treeDepthLevel == 0) {
//        cell.backgroundColor = [UIColor colorWithRed:186/255.0 green:204/255.0  blue:155/255.0 alpha:0.2];//UIColorFromRGB(0xF7F7F7);
//    }
}

#pragma mark TreeView Data Source
- (BOOL)treeView:(RATreeView *)treeView canEditRowForItem:(id)item treeNodeInfo:(RATreeNodeInfo *)treeNodeInfo
{
     return NO;
}
- (UITableViewCell *)treeView:(RATreeView *)treeView cellForItem:(id)item treeNodeInfo:(RATreeNodeInfo *)treeNodeInfo
{
    UILabel *lblHeader;
    UIView *bottomlineView;
    UIImageView *thumbImg;
    UILabel *lblExpandCollapse;
//    UIImageView *bottomlineView;

    //    UITableViewCell *cell = [treeView dequeueReusableCellWithIdentifier:@"cell"];
    //    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    //    if (cell == nil) {
    
    RADataObject *tempItem = (RADataObject *)item;
    NSString *cellIdetifier = tempItem.name;
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdetifier];
    cell.backgroundColor = [UIColor clearColor];
    cell.editing = FALSE;
    thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(5,15, 45, 45)];
    thumbImg.tag = 100;
    thumbImg.backgroundColor = [UIColor clearColor];
    thumbImg.contentMode = UIViewContentModeScaleAspectFit;
    [cell.contentView addSubview:thumbImg];
    
    lblHeader = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame)+5 ,0, 190, 44)];
    lblHeader.textColor = [UIColor whiteColor];
    lblHeader.font = DEFAULT_FONT(18.0);
    lblHeader.backgroundColor = [UIColor clearColor];
    lblHeader.highlightedTextColor = [UIColor whiteColor];
    lblHeader.textAlignment = NSTextAlignmentLeft;
    lblHeader.tag = 101;
    [cell.contentView addSubview:lblHeader];
    
    lblExpandCollapse = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lblHeader.frame)+2 ,2,40, 40)];
    lblExpandCollapse.textColor = [UIColor whiteColor];
    lblExpandCollapse.font = DEFAULT_BOLD_FONT(20);
    lblExpandCollapse.backgroundColor = [UIColor clearColor];
    lblExpandCollapse.highlightedTextColor = [UIColor whiteColor];
    lblExpandCollapse.textAlignment = NSTextAlignmentLeft;
    lblExpandCollapse.tag = 102;
    [cell.contentView addSubview:lblExpandCollapse];
    
    bottomlineView = [[UIView alloc] initWithFrame:CGRectMake(0,CGRectGetMaxY(lblHeader.frame),320,1)];
//    bottomlineView = [[UIImageView alloc] initWithFrame:CGRectMake(0,CGRectGetMaxY(lblHeader.frame),320,14)];
//    bottomlineView.image = [UIImage imageNamed:@"MerchantLogin_separator.png"];
    bottomlineView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
    bottomlineView.tag = 103;
    [cell.contentView addSubview:bottomlineView];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
    bgColorView.tag = 104;
    cell.selectedBackgroundView = bgColorView;
    
    //    }
    NSString *text = tempItem.name;
    
    thumbImg                =   (UIImageView *)[cell viewWithTag:100];
    lblHeader               =   (UILabel *)[cell viewWithTag:101];
    lblExpandCollapse       =   (UILabel *)[cell viewWithTag:102];
    bottomlineView          =   (UIView *)[cell viewWithTag:103];
    lblHeader.text          =   text;
    thumbImg.frame          =   CGRectMake(5,15, 45, 45);
    if (tempItem.isHeader) {
        lblExpandCollapse.hidden = TRUE;
        if ([text isEqualToString:SECTION0]) {
            lblHeader.hidden = TRUE;
            bottomlineView.hidden   =   YES;
        }else{
//            lblHeader.frame         =   CGRectMake(5, lblHeader.frame.origin.y, lblHeader.frame.size.width, lblHeader.frame.size.height);
//            lblExpandCollapse.frame     = CGRectMake(CGRectGetMaxX(lblHeader.frame)+50 ,2,40, 40);
//            bottomlineView.hidden       =   NO;
//            if (tempItem.collapase) {
//                lblExpandCollapse.hidden = FALSE;
//                if (treeNodeInfo.isExpanded) {
//                    lblExpandCollapse.text = @"-";
//                }else{
//                    lblExpandCollapse.text = @"+";
//                }
//            }
            if ([text isEqualToString:SECTION1]){
                thumbImg.image = [UIImage imageNamed:@"Settings_White.png"];
                thumbImg.frame  =   CGRectMake(20,3, 20, 20);
                lblHeader.font   =   DEFAULT_FONT(15);
                lblHeader.frame  =   CGRectMake(55, 3, lblHeader.frame.size.width, 20);
            }
        }
//thumbImg.hidden         =   YES;
//        cell.selectionStyle         = UITableViewCellSelectionStyleNone;
//        cell.selectedBackgroundView = nil;
        bottomlineView.hidden   =   YES;
    }else{
        thumbImg.hidden         =   NO;
        bottomlineView.hidden   =   YES;
        lblHeader.font          =   DEFAULT_FONT(22);
        lblHeader.frame         =   CGRectMake(CGRectGetMaxX(thumbImg.frame)+5,15, lblHeader.frame.size.width + 15, lblHeader.frame.size.height);
        lblExpandCollapse.hidden = TRUE;
        lblExpandCollapse.text  = @"";
        cell.selectionStyle     = UITableViewCellSelectionStyleBlue;
        
        //SECTION0
        if ([text isEqualToString:DISCOVER]) {
            thumbImg.image = [UIImage imageNamed:@"Discover_White.png"];
            //            thumbImg.highlightedImage = [UIImage imageNamed:@"Grid_Offers.png"];
//            if(initialAnimation == 0) {
//                cell.layer.transform = CATransform3DMakeScale(0.95, 0.95, 1);
//                CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform"];
//                animation.toValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
//                animation.autoreverses = YES;
//                animation.duration = 0.40;
//                animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//                animation.repeatCount = HUGE_VALF;
//                [cell.layer addAnimation:animation forKey:@"pulseAnimation"];
//            }
        }else if ([text isEqualToString:MY_SHOPLOCAL]) {
            thumbImg.image = [UIImage imageNamed:@"MyShoplocal_White.png"];
            //            thumbImg.highlightedImage = [UIImage imageNamed:@"Grid_MyShoplocal.png"];
        }else if ([text isEqualToString:ALL_BUSINESS]) {
            thumbImg.image = [UIImage imageNamed:@"AllBusiness_White.png"];
        }else if ([text isEqualToString:SHARE_THIS_APP]) {
            thumbImg.image = [UIImage imageNamed:@"Share_White.png"];
        }
        
//        //SECTION0
//        if ([text isEqualToString:DISCOVER]) {
//            thumbImg.image = [UIImage imageNamed:@"Discover_White.png"];
//            if(initialAnimation == 0) {
//                cell.layer.transform = CATransform3DMakeScale(0.95, 0.95, 1);
//                CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform"];
//                animation.toValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
//                animation.autoreverses = YES;
//                animation.duration = 0.40;
//                animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//                animation.repeatCount = HUGE_VALF;
//                [cell.layer addAnimation:animation forKey:@"pulseAnimation"];
//            }
//        }else if ([text isEqualToString:MY_SHOPLOCAL]) {
//            thumbImg.image = [UIImage imageNamed:@"MyShoplocal_White.png"];
//        }
//        
//        //SECTION1
//        else if ([text isEqualToString:LOGIN]) {
//            thumbImg.image = [UIImage imageNamed:@"Login_White.png"];
//        }
//        else if ([text isEqualToString:MY_PROFILE]) {
//            thumbImg.image = [UIImage imageNamed:@"MyProfile_White.png"];
//        }else if ([text isEqualToString:CHANGE_LOCATION]) {
//            thumbImg.image = [UIImage imageNamed:@"ChangeLocation_White.png"];
//        }else if ([text isEqualToString:SETTINGS]) {
//            thumbImg.image = [UIImage imageNamed:@"Settings_White.png"];
//        }
//        
//        //SECTION2
//        else if ([text isEqualToString:ALL_BUSINESS]) {
//            thumbImg.image = [UIImage imageNamed:@"AllBusiness_White.png"];
//        }else if ([text isEqualToString:MYFAV_BUSINESS]) {
//            thumbImg.image = [UIImage imageNamed:@"MyFavBusiness_White.png"];
//        }
//        
//        //SECTION3
//        else if ([text isEqualToString:TOUR]) {
//            thumbImg.image = [UIImage imageNamed:@"Welcome_White.png"];
//        }
//        else if ([text isEqualToString:ABOUTUS]) {
//            thumbImg.image = [UIImage imageNamed:@"About_White.png"];
//        }
//        else if ([text isEqualToString:FACEBOOK]) {
//            thumbImg.image = [UIImage imageNamed:@"Facebook_White.png"];
//        }
//        else if ([text isEqualToString:TWITTER]) {
//            thumbImg.image = [UIImage imageNamed:@"Twitter_White.png"];
//        }else if ([text isEqualToString:CONTACTUS]) {
//            thumbImg.image = [UIImage imageNamed:@"Contact_White.png"];
//        }
//        else if ([text isEqualToString:SHARE_THIS_APP]) {
//            thumbImg.image = [UIImage imageNamed:@"Share_White.png"];
//        }
//        //SECTION4
//        else if ([text isEqualToString:SETUP_BUSINESS]) {
//            thumbImg.image = [UIImage imageNamed:@"BusinessDashboard_White.png"];
//        }
//        else if ([text isEqualToString:SWITCH_TO_BUSINESS]) {
//            thumbImg.image = [UIImage imageNamed:@"BusinessDashboard_White.png"];
//        }
    }
    return cell;
}

- (NSInteger)treeView:(RATreeView *)treeView numberOfChildrenOfItem:(id)item
{
    DebugLog(@"moreButton.isSelected %d",moreButton.isSelected);
//    if (item == nil) {
//        if (moreButton.isSelected)
//            return [self.data count];
//        else
//            return 1;
//    }
    if (item == nil) {
        return [self.data count];
    }
    RADataObject *ldata = item;
    return [ldata.children count];
}

- (id)treeView:(RATreeView *)treeView child:(NSInteger)index ofItem:(id)item
{
    RADataObject *ldata = item;
    if (item == nil) {
        return [self.data objectAtIndex:index];
    }
    
    return [ldata.children objectAtIndex:index];
}

- (void)treeView:(RATreeView *)ltreeView didSelectRowForItem:(id)item treeNodeInfo:(RATreeNodeInfo *)treeNodeInfo{
    RADataObject *section0Item = (RADataObject *)[data objectAtIndex:0];
    UITableViewCell *cell = [ltreeView cellForItem:[section0Item.children objectAtIndex:1]];
    [cell.layer removeAllAnimations];
    //initialAnimation = 1;

    RADataObject *tempItem = (RADataObject *)item;
    NSString *text = tempItem.name;
    DebugLog(@"SelectRow %@  and current %@",text,current);
    if ([current isEqualToString:text]) {
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        return;
    }
    previous = current;
    if ([text isEqualToString:ALL_BUSINESS]) {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[INAllStoreViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        } else {
            obj = nil;
        }
        [INEventLogger logEvent:@"Tab_MarketPlace"];
        INAllStoreViewController *allStoresController = [[INAllStoreViewController alloc] initWithNibName:@"INAllStoreViewController" bundle:nil];
        //allStoresController.title = ALL_BUSINESS;
        allStoresController.comeFrom = HOME;
        NSArray *controllers = [NSArray arrayWithObject:allStoresController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        controllers = nil;
    }else if ([text isEqualToString:DISCOVER]) {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[INOffersStreamViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        } else {
            obj = nil;
        }
        [INEventLogger logEvent:@"Tab_OfferStream"];
        INOffersStreamViewController *offersController = [[INOffersStreamViewController alloc] initWithNibName:@"INOffersStreamViewController" bundle:nil];
        //offersController.title = DISCOVER;
        NSArray *controllers = [NSArray arrayWithObject:offersController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        controllers = nil;
    }else if ([text isEqualToString:MY_SHOPLOCAL]) {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[INMyShoplocalOffersViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        } else {
            obj = nil;
        }
        [INEventLogger logEvent:@"Tab_MyShoplocal"];
        INMyShoplocalOffersViewController *myofferViewController = [[INMyShoplocalOffersViewController alloc] initWithNibName:@"INMyShoplocalOffersViewController" bundle:nil];
        myofferViewController.title = MY_SHOPLOCAL;
        NSArray *controllers = [NSArray arrayWithObject:myofferViewController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        controllers = nil;
    }else if ([text isEqualToString:SHARE_THIS_APP]) {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[INShareThisAppViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        } else {
            obj = nil;
        }
        [INEventLogger logEvent:@"Tab_ShareThisApp"];
        INShareThisAppViewController *shareThisAppViewController = [[INShareThisAppViewController alloc] initWithNibName:@"INShareThisAppViewController" bundle:nil];
        shareThisAppViewController.title = SHARE_THIS_APP;
        NSArray *controllers = [NSArray arrayWithObject:shareThisAppViewController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        controllers = nil;
    }
    else if ([text isEqualToString:SECTION1]) {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[INCustomerSettingsViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        } else {
            obj = nil;
        }
        [INEventLogger logEvent:@"Tab_Settings"];
        INCustomerSettingsViewController *settingsController = [[INCustomerSettingsViewController alloc] initWithNibName:@"INCustomerSettingsViewController" bundle:nil];
        settingsController.title = SECTION1;
        NSArray *controllers = [NSArray arrayWithObject:settingsController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        controllers = nil;
    }
    current = text;
}

//- (void)treeView:(RATreeView *)ltreeView didSelectRowForItem:(id)item treeNodeInfo:(RATreeNodeInfo *)treeNodeInfo{
//
//
//    RADataObject *section0Item = (RADataObject *)[data objectAtIndex:0];
//    UITableViewCell *cell = [ltreeView cellForItem:[section0Item.children objectAtIndex:1]];
//    [cell.layer removeAllAnimations];
//    initialAnimation = 1;
//    
//    RADataObject *tempItem = (RADataObject *)item;
//    NSString *text = tempItem.name;
//    DebugLog(@"SelectRow %@  and current %@",text,current);
//    if ([current isEqualToString:text]) {
//        [self.sideMenu setMenuState:MFSideMenuStateClosed];
//        return;
//    }
//    previous = current;
//    if (!tempItem.isHeader) {
//        //SECTION0
//        if ([text isEqualToString:DISCOVER]) {
//            NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
//            id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
//            if ([obj isKindOfClass:[INOffersStreamViewController class]])
//            {
//                [self.sideMenu setMenuState:MFSideMenuStateClosed];
//                return;
//            } else {
//                obj = nil;
//            }
//            [INEventLogger logEvent:@"Tab_OfferStream"];
//            INOffersStreamViewController *offersController = [[INOffersStreamViewController alloc] initWithNibName:@"INOffersStreamViewController" bundle:nil];
//            //offersController.title = DISCOVER;
//            NSArray *controllers = [NSArray arrayWithObject:offersController];
//            self.sideMenu.navigationController.viewControllers = controllers;
//            [self.sideMenu setMenuState:MFSideMenuStateClosed];
//            controllers = nil;
//        }else if ([text isEqualToString:MY_SHOPLOCAL]) {
//            NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
//            id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
//            if ([obj isKindOfClass:[INMyShoplocalOffersViewController class]])
//            {
//                [self.sideMenu setMenuState:MFSideMenuStateClosed];
//                return;
//            } else {
//                obj = nil;
//            }
//            [INEventLogger logEvent:@"Tab_MyShoplocal"];
//            INMyShoplocalOffersViewController *myofferViewController = [[INMyShoplocalOffersViewController alloc] initWithNibName:@"INMyShoplocalOffersViewController" bundle:nil];
//            myofferViewController.title = MY_SHOPLOCAL;
//            NSArray *controllers = [NSArray arrayWithObject:myofferViewController];
//            self.sideMenu.navigationController.viewControllers = controllers;
//            [self.sideMenu setMenuState:MFSideMenuStateClosed];
//            controllers = nil;
//        }
//        
//        //SECTION1
//        else if ([text isEqualToString:LOGIN]) {
//            NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
//            id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
//            if ([obj isKindOfClass:[INCustomerLoginViewController class]])
//            {
//                [self.sideMenu setMenuState:MFSideMenuStateClosed];
//                return;
//            } else {
//                obj = nil;
//            }
//            [INEventLogger logEvent:@"Tab_CustomerLogin"];
//            INCustomerLoginViewController *custLoginController = [[INCustomerLoginViewController alloc] initWithNibName:@"INCustomerLoginViewController" bundle:nil];
//            NSArray *controllers = [NSArray arrayWithObject:custLoginController];
//            self.sideMenu.navigationController.viewControllers = controllers;
//            [self.sideMenu setMenuState:MFSideMenuStateClosed];
//            controllers = nil;
//        }
//        else if ([text isEqualToString:MY_PROFILE]) {
//            NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
//            id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
//            if ([obj isKindOfClass:[INCustomerProfileViewController class]])
//            {
//                [self.sideMenu setMenuState:MFSideMenuStateClosed];
//                return;
//            } else {
//                obj = nil;
//            }
//            [INEventLogger logEvent:@"Tab_CustomerProfile"];
//            INCustomerProfileViewController *customerProfileViewController = [[INCustomerProfileViewController alloc] initWithNibName:@"INCustomerProfileViewController" bundle:nil] ;
//            customerProfileViewController.title = MY_PROFILE;
//            NSArray *controllers = [NSArray arrayWithObject:customerProfileViewController];
//            self.sideMenu.navigationController.viewControllers = controllers;
//            [self.sideMenu setMenuState:MFSideMenuStateClosed];
//            controllers = nil;
//        }else if ([text isEqualToString:CHANGE_LOCATION]) {
//            NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
//            DebugLog(@"SideMenu: %@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
//            id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
//            if ([obj isKindOfClass:[INChangeLocationViewController class]])
//            {
//                [self.sideMenu setMenuState:MFSideMenuStateClosed];
//                return;
//            } else {
//                obj = nil;
//            }
//            [INEventLogger logEvent:@"Tab_Location"];
//            INChangeLocationViewController *changeLocationViewController = [[INChangeLocationViewController alloc] initWithNibName:@"INChangeLocationViewController" bundle:nil];
//            changeLocationViewController.title = CHANGE_LOCATION;
//            changeLocationViewController.comeFrom = HOME;
//            NSArray *controllers = [NSArray arrayWithObject:changeLocationViewController];
//            self.sideMenu.navigationController.viewControllers = controllers;
//            [self.sideMenu setMenuState:MFSideMenuStateClosed];
//            controllers = nil;
//        }else if ([text isEqualToString:SETTINGS]) {
//            NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
//            DebugLog(@"SideMenu: %@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
//            id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
//            if ([obj isKindOfClass:[INCustomerSettingsViewController class]])
//            {
//                [self.sideMenu setMenuState:MFSideMenuStateClosed];
//                return;
//            } else {
//                obj = nil;
//            }
//            [INEventLogger logEvent:@"Tab_Settings"];
//            INCustomerSettingsViewController *settingsController = [[INCustomerSettingsViewController alloc] initWithNibName:@"INCustomerSettingsViewController" bundle:nil];
//            settingsController.title = SETTINGS;
//            NSArray *controllers = [NSArray arrayWithObject:settingsController];
//            self.sideMenu.navigationController.viewControllers = controllers;
//            [self.sideMenu setMenuState:MFSideMenuStateClosed];
//            controllers = nil;
//        }
//        
//        //SECTION2
//        else if ([text isEqualToString:ALL_BUSINESS]) {
//            NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
//            id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
//            if ([obj isKindOfClass:[INAllStoreViewController class]])
//            {
//                [self.sideMenu setMenuState:MFSideMenuStateClosed];
//                return;
//            } else {
//                obj = nil;
//            }
//            [INEventLogger logEvent:@"Tab_MarketPlace"];
//            INAllStoreViewController *allStoresController = [[INAllStoreViewController alloc] initWithNibName:@"INAllStoreViewController" bundle:nil];
//            //allStoresController.title = ALL_BUSINESS;
//            allStoresController.comeFrom = HOME;
//            NSArray *controllers = [NSArray arrayWithObject:allStoresController];
//            self.sideMenu.navigationController.viewControllers = controllers;
//            [self.sideMenu setMenuState:MFSideMenuStateClosed];
//            controllers = nil;
//        }else if ([text isEqualToString:MYFAV_BUSINESS]) {
//            NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
//            id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
//            if ([obj isKindOfClass:[INFavouritesViewController class]])
//            {
//                [self.sideMenu setMenuState:MFSideMenuStateClosed];
//                return;
//            } else {
//                obj = nil;
//            }
//            [INEventLogger logEvent:@"Tab_FavouriteBusiness"];
//            INFavouritesViewController *favViewController = [[INFavouritesViewController alloc] initWithNibName:@"INFavouritesViewController" bundle:nil];
//            favViewController.title = MYFAV_BUSINESS;
//            NSArray *controllers = [NSArray arrayWithObject:favViewController];
//            self.sideMenu.navigationController.viewControllers = controllers;
//            [self.sideMenu setMenuState:MFSideMenuStateClosed];
//            controllers = nil;
//        }
//        
//        //SECTION3
//        else if ([text isEqualToString:TOUR]) {
//            NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
//            id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
//            if ([obj isKindOfClass:[NMSplashViewController class]])
//            {
//                [self.sideMenu setMenuState:MFSideMenuStateClosed];
//                return;
//            } else {
//                obj = nil;
//            }
//            [INEventLogger logEvent:@"Tab_Tour"];
//            NMSplashViewController *splashviewController = [[NMSplashViewController alloc] initWithNibName:@"NMSplashViewController" bundle:nil];
//            splashviewController.title = TOUR;
//            splashviewController.place_id   = [IN_APP_DELEGATE getActiveShoplocalPlaceIdFromAreaTable];
//            NSArray *controllers = [NSArray arrayWithObject:splashviewController];
//            self.sideMenu.navigationController.viewControllers = controllers;
//            [self.sideMenu setMenuState:MFSideMenuStateClosed];
//            controllers = nil;
//        }
//        else if ([text isEqualToString:ABOUTUS]) {
//            NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
//            id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
//            if ([obj isKindOfClass:[INAboutUsViewController class]])
//            {
//                [self.sideMenu setMenuState:MFSideMenuStateClosed];
//                return;
//            } else {
//                obj = nil;
//            }
//            [INEventLogger logEvent:@"Tab_AboutShoplocal"];
//            INAboutUsViewController *aboutusViewController = [[INAboutUsViewController alloc] initWithNibName:@"INAboutUsViewController" bundle:nil];
//            aboutusViewController.title = ABOUTUS;
//            NSArray *controllers = [NSArray arrayWithObject:aboutusViewController];
//            self.sideMenu.navigationController.viewControllers = controllers;
//            [self.sideMenu setMenuState:MFSideMenuStateClosed];
//            controllers = nil;
//        }
//        else if ([text isEqualToString:FACEBOOK]) {
//            NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
//            id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
//            if ([obj isKindOfClass:[INFBWebViewController class]] && [current isEqualToString:FACEBOOK])
//            {
//                [self.sideMenu setMenuState:MFSideMenuStateClosed];
//                return;
//            } else {
//                obj = nil;
//            }
//             [INEventLogger logEvent:@"Tab_Facebook"];
//            INFBWebViewController *facebookViewController = [[INFBWebViewController alloc] initWithNibName:@"INFBWebViewController" bundle:nil];
//            facebookViewController.title = FACEBOOK;
//            facebookViewController.type = 1;
//            NSArray *controllers = [NSArray arrayWithObject:facebookViewController];
//            self.sideMenu.navigationController.viewControllers = controllers;
//            [self.sideMenu setMenuState:MFSideMenuStateClosed];
//            controllers = nil;
//        }
//        else if ([text isEqualToString:TWITTER]) {
//            NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
//            id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
//            if ([obj isKindOfClass:[INFBWebViewController class]] && [current isEqualToString:TWITTER])
//            {
//                [self.sideMenu setMenuState:MFSideMenuStateClosed];
//                return;
//            } else {
//                obj = nil;
//            }
//            [INEventLogger logEvent:@"Tab_Twitter"];
//            INFBWebViewController *twitterViewController = [[INFBWebViewController alloc] initWithNibName:@"INFBWebViewController" bundle:nil];
//            twitterViewController.title = TWITTER;
//            twitterViewController.type = 2;
//            NSArray *controllers = [NSArray arrayWithObject:twitterViewController];
//            self.sideMenu.navigationController.viewControllers = controllers;
//            [self.sideMenu setMenuState:MFSideMenuStateClosed];
//            controllers = nil;
//        }else if ([text isEqualToString:CONTACTUS]) {
//            NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
//            id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
//            if ([obj isKindOfClass:[INContactUsViewController class]])
//            {
//                [self.sideMenu setMenuState:MFSideMenuStateClosed];
//                return;
//            } else {
//                obj = nil;
//            }
//            [INEventLogger logEvent:@"Tab_ContactUs"];
//            INContactUsViewController *contactusViewController = [[INContactUsViewController alloc] initWithNibName:@"INContactUsViewController" bundle:nil];
//            contactusViewController.title = CONTACTUS;
//            NSArray *controllers = [NSArray arrayWithObject:contactusViewController];
//            self.sideMenu.navigationController.viewControllers = controllers;
//            [self.sideMenu setMenuState:MFSideMenuStateClosed];
//            controllers = nil;
//        }
//        else if ([text isEqualToString:SHARE_THIS_APP]) {
//            NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
//            id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
//            if ([obj isKindOfClass:[INShareThisAppViewController class]])
//            {
//                [self.sideMenu setMenuState:MFSideMenuStateClosed];
//                return;
//            } else {
//                obj = nil;
//            }
//            [INEventLogger logEvent:@"Tab_ShareThisApp"];
//            INShareThisAppViewController *shareThisAppViewController = [[INShareThisAppViewController alloc] initWithNibName:@"INShareThisAppViewController" bundle:nil];
//            shareThisAppViewController.title = SHARE_THIS_APP;
//            NSArray *controllers = [NSArray arrayWithObject:shareThisAppViewController];
//            self.sideMenu.navigationController.viewControllers = controllers;
//            [self.sideMenu setMenuState:MFSideMenuStateClosed];
//            controllers = nil;
//        }
//        
//        //SECTION4
//        else if ([text isEqualToString:SETUP_BUSINESS] || [text isEqualToString:SWITCH_TO_BUSINESS]) {
//            [self.sideMenu setMenuState:MFSideMenuStateClosed];
//            [IN_APP_DELEGATE switchViewController];
//            return;
//        }
//        current = text;
//    }
//}

#pragma mark -
#pragma mark - Seperator
- (UIImage *)separatorImage
{
    UIGraphicsBeginImageContext(CGSizeMake(1, 4));
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIGraphicsPushContext(context);
    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:1].CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, 1, 2));
    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:79/255.0 green:79/255.0 blue:77/255.0 alpha:1].CGColor);
    CGContextFillRect(context, CGRectMake(0, 3, 1, 2));
    UIGraphicsPopContext();
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithCGImage:outputImage.CGImage scale:2.0 orientation:UIImageOrientationUp];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma NSNotificationCenter Callbacks
-(void)handleCustomerLoginLogoutNotification:(NSNotification *)notification
{
    NSDictionary *dict = [notification userInfo];
    BOOL isLoggedIn = [[dict objectForKey:IN_CUSTOMER_LOGIN_LOGOUT_KEY] boolValue];

    DebugLog(@"current %@ -- isLoggedIn %d",current,isLoggedIn);
//    [self switchToFromLoginMyProfile:isLoggedIn];
//    if ([current isEqualToString:LOGIN] || [current isEqualToString:MY_PROFILE]) {
//        INOffersStreamViewController *offersController = [[INOffersStreamViewController alloc] initWithNibName:@"INOffersStreamViewController" bundle:nil];
//        //offersController.title = DISCOVER;
//        NSArray *controllers = [NSArray arrayWithObject:offersController];
//        self.sideMenu.navigationController.viewControllers = controllers;
//        [self.sideMenu setMenuState:MFSideMenuStateClosed];
//        [self setSelectedRow:DISCOVER];
//    }
}
//-(void)switchToFromLoginMyProfile:(BOOL)isLoggedIn{
//    DebugLog(@"Customer : isLoggedIn %d",isLoggedIn);
//    BOOL found = NO;
//    for (int index = 0; index < [self.data count]; index++) {
//        found = NO;
//        RADataObject *itemObj = (RADataObject *)[self.data objectAtIndex:index];
//        if (itemObj.children != nil) {
//            DebugLog(@"Parent==> %@",itemObj.name);
//            if ([itemObj.name isEqualToString:SECTION1]) {
//                DebugLog(@"Got Parent %@ == %@",itemObj.name,SECTION1);
//                NSMutableArray *personliseArray   = [[NSMutableArray alloc] initWithArray:itemObj.children];
//                for (int cindex = 0; cindex < [personliseArray count]; cindex++)
//                {
//                    RADataObject *childObj = (RADataObject *)[personliseArray objectAtIndex:cindex];
//                    DebugLog(@"Child==> %@",childObj.name);
//                    if (isLoggedIn) {
//                        if([childObj.name isEqualToString:LOGIN]) {
//                            DebugLog(@"Customer : login found,changed to myprofile");
//                            childObj.name = MY_PROFILE;
//                            [personliseArray replaceObjectAtIndex:cindex withObject:childObj];
//                            found = YES;
//                        }else if([childObj.name isEqualToString:MY_PROFILE]){
//                            DebugLog(@"Customer : myprofile  found ");
//                            found = YES;
//                        }
//                    }else{
//                        if([childObj.name isEqualToString:MY_PROFILE]) {
//                            DebugLog(@"Customer : myprofile found, changed to login");
//                            childObj.name = LOGIN;
//                            [personliseArray replaceObjectAtIndex:cindex withObject:childObj];
//                            found = YES;
//                        }else if([childObj.name isEqualToString:LOGIN]){
//                            DebugLog(@"Customer : login  found ");
//                            found = YES;
//                        }
//                    }
//                    childObj = nil;
//                    if (found) {
//                        break;
//                    }
//                }
//                DebugLog(@"after isLoggedIn %@",personliseArray);
//                itemObj.children = personliseArray;
//                [self.data replaceObjectAtIndex:index withObject:itemObj];
//                [treeView reloadData];
//            }
//            itemObj = nil;
//        }
//        if (found) {
//            break;
//        }
//    }
//}

-(void)handleMerchantLoginLogoutNotification:(NSNotification *)notification
{
    DebugLog(@"handleMerchantLoginLogoutNotification");
//    NSDictionary *dict = [notification userInfo];
//    BOOL isLoggedIn = [[dict objectForKey:IN_MERCHANT_LOGIN_LOGOUT_KEY] boolValue];
//    [self switchToFromSetupBusiness:isLoggedIn];
}
//-(void)switchToFromSetupBusiness:(BOOL)isLoggedIn{
//    DebugLog(@"Merchant current %@ -- isLoggedIn %d",current,isLoggedIn);
//    DebugLog(@"Merchant : isLoggedIn %d",isLoggedIn);
//    BOOL found = NO;
//    for (int index = 0; index < [self.data count]; index++) {
//        found = NO;
//        RADataObject *itemObj = (RADataObject *)[self.data objectAtIndex:index];
//        if (itemObj.children != nil) {
//            DebugLog(@"Parent==> %@",itemObj.name);
//            if ([itemObj.name isEqualToString:SECTION4]) {
//                DebugLog(@"Got Parent %@ == %@",itemObj.name,SECTION4);
//                NSMutableArray *personliseArray   = [[NSMutableArray alloc] initWithArray:itemObj.children];
//                for (int cindex = 0; cindex < [personliseArray count]; cindex++)
//                {
//                    RADataObject *childObj = (RADataObject *)[personliseArray objectAtIndex:cindex];
//                    DebugLog(@"Child==> %@",childObj.name);
//                    if (isLoggedIn) {
//                        if([childObj.name isEqualToString:SETUP_BUSINESS]) {
//                            DebugLog(@"SETUP_BUSINESS found,changed to SWITCH_TO_BUSINESS");
//                            childObj.name = SWITCH_TO_BUSINESS;
//                            [personliseArray replaceObjectAtIndex:cindex withObject:childObj];
//                            found = YES;
//                        }else if([childObj.name isEqualToString:SWITCH_TO_BUSINESS]){
//                            DebugLog(@"SWITCH_TO_BUSINESS  found ");
//                            found = YES;
//                        }
//                    }else{
//                        if([childObj.name isEqualToString:SWITCH_TO_BUSINESS]) {
//                            DebugLog(@"SWITCH_TO_BUSINESS found, changed to SETUP_BUSINESS");
//                            childObj.name = SETUP_BUSINESS;
//                            [personliseArray replaceObjectAtIndex:cindex withObject:childObj];
//                            found = YES;
//                        }else if([childObj.name isEqualToString:SETUP_BUSINESS]){
//                            DebugLog(@"SETUP_BUSINESS  found ");
//                            found = YES;
//                        }
//                    }
//                    childObj = nil;
//                    if (found) {
//                        break;
//                    }
//                }
//                DebugLog(@"after isLoggedIn %@",personliseArray);
//                itemObj.children = personliseArray;
//                [self.data replaceObjectAtIndex:index withObject:itemObj];
//                [treeView reloadData];
//            }
//            itemObj = nil;
//        }
//        if (found) {
//            break;
//        }
//    }
//}

-(void)handleChangeLocationNotification:(NSNotification *)notification
{
    DebugLog(@"handleChangeLocationNotification");
    NSDictionary *dict = [notification userInfo];
    NSString *locationName = [dict objectForKey:IN_CUSTOMER_CHANGE_LOCATION_KEY];
    DebugLog(@"locationName %@",locationName);

    UIView *view = (UIView *)self.treeView.treeHeaderView;
    UILabel *lbl = (UILabel *)[view viewWithTag:420];
    DebugLog(@"lbl %@",lbl);

    lbl.text =  locationName;
  //  [self setSelectedRow:ALL_BUSINESS];
}

-(void)handleChangeTabManuallyNotification:(NSNotification *)notification
{
    DebugLog(@"handleChangeTabManuallyNotification");
    NSDictionary *dict  = [notification userInfo];
    NSString *TabNameToOpen       = [dict objectForKey:IN_CUSTOMER_CHANGE_TAB_MANUALLY_KEY];
    DebugLog(@"TabName %@",TabNameToOpen);
    
    if ([TabNameToOpen isEqualToString:ALL_BUSINESS]) {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        obj = nil;
     //   [INEventLogger logEvent:@"Tab_MarketPlace"];
        INAllStoreViewController *allStoresController = [[INAllStoreViewController alloc] initWithNibName:@"INAllStoreViewController" bundle:nil];
        //allStoresController.title = ALL_BUSINESS;
        NSArray *controllers = [NSArray arrayWithObject:allStoresController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        controllers = nil;
        [self setSelectedRow:ALL_BUSINESS];
    }else{
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        obj = nil;
     //   [INEventLogger logEvent:@"Tab_OfferStream"];
        INOffersStreamViewController *offersController = [[INOffersStreamViewController alloc] initWithNibName:@"INOffersStreamViewController" bundle:nil];
        //offersController.title = DISCOVER;
        NSArray *controllers = [NSArray arrayWithObject:offersController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        controllers = nil;
        [self setSelectedRow:DISCOVER];
    }
}


-(void)handleLocalNotification:(NSNotification *)notification
{
    DebugLog(@"handleLocalNotification");
    NSDictionary *dict            = [notification userInfo];
    NSString *TabNameToOpen       = [dict objectForKey:IN_CUSTOMER_SHOW_LOCAL_NOTIFICATION_KEY];
    DebugLog(@"TabName %@",TabNameToOpen);
    if ([TabNameToOpen isEqualToString:CUSTOMERFAVOURITESTORE_LNTYPE])
    {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[INAllStoreViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
        }
        else
        {
            INAllStoreViewController *allStoresController = [[INAllStoreViewController alloc] initWithNibName:@"INAllStoreViewController" bundle:nil];
            allStoresController.comeFrom = HOME;
            NSArray *controllers = [NSArray arrayWithObject:allStoresController];
            self.sideMenu.navigationController.viewControllers = controllers;
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            controllers = nil;
        }
        [self setSelectedRow:ALL_BUSINESS];
    }
    else if([TabNameToOpen isEqualToString:CUSTOMERSPECIALDATE_LNTYPE])
    {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[INMyShoplocalOffersViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
        } else {
            obj = nil;
            INMyShoplocalOffersViewController *myofferViewController = [[INMyShoplocalOffersViewController alloc] initWithNibName:@"INMyShoplocalOffersViewController" bundle:nil];
            myofferViewController.title = MY_SHOPLOCAL;
            NSArray *controllers = [NSArray arrayWithObject:myofferViewController];
            self.sideMenu.navigationController.viewControllers = controllers;
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            controllers = nil;
        }
        [self setSelectedRow:MY_SHOPLOCAL];
        [self performSelector:@selector(postShowProfileNotification) withObject:nil afterDelay:2.0];
    }
    else if([TabNameToOpen isEqualToString:STOREDETAILS_FBDEEPLINKINGNTYPE])
    {
            NSDictionary *dict              = [notification userInfo];
            NSString *storeId               = [dict objectForKey:STORE_ID];
            DebugLog(@"storeId %@",storeId);
            if ([storeId length] > 0)
            {
                NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
                id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
                if ([obj isKindOfClass:[INAllStoreViewController class]])
                {
                    [self.sideMenu setMenuState:MFSideMenuStateClosed];
                }
                else
                {
                    obj = nil;
                    INAllStoreViewController *allStoresController = [[INAllStoreViewController alloc] initWithNibName:@"INAllStoreViewController" bundle:nil];
                    allStoresController.comeFrom = HOME;
                    NSArray *controllers = [NSArray arrayWithObject:allStoresController];
                    self.sideMenu.navigationController.viewControllers = controllers;
                    [self.sideMenu setMenuState:MFSideMenuStateClosed];
                    controllers = nil;
                }
                [self performSelector:@selector(postShowStoreDetailsNotification:) withObject:storeId afterDelay:2.0];
                [self setSelectedRow:ALL_BUSINESS];
        }
    }
}

-(void)postShowProfileNotification
{
    [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_SHOW_PROFILE_LOCAL_NOTIFICATION object:nil userInfo:nil];
}

-(void)postShowStoreDetailsNotification:(NSString *)storeId
{
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:storeId forKey:STORE_ID];
    [[NSNotificationCenter defaultCenter] postNotificationName:IN_CUSTOMER_SHOW_STORE_DETAILS_NOTIFICATION object:nil userInfo:dictionary];
}

-(void)setSelectedRow:(NSString *)name{
    
    DebugLog(@"current %@ name %@",current,name);

    BOOL found = NO;
    for (RADataObject *itemObj in self.data)
    {
        found = NO;
        DebugLog(@"Parent==> %@",itemObj.name);
        if (itemObj.children != nil) {
            for (RADataObject *citemObj in itemObj.children)
            {
                DebugLog(@"Child ==> %@",citemObj.name);
                if([citemObj.name isEqualToString:name]) {
                    DebugLog(@"Got ==> itemObj %@ ==  name %@",citemObj.name,name);
                    previous     = current;
                    current      = name;
                    [treeView selectRowForItem:citemObj animated:YES scrollPosition:RATreeViewScrollPositionNone];
                    found = YES;
                }
                if (found) {
                    break;
                }
            }
        }
        if (found) {
            break;
        }
    }
}
@end
