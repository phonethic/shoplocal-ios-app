//
//  INCustomerProfileViewController.h
//  shoplocal
//
//  Created by Kirti Nikam on 02/09/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActionSheetPicker.h"
#import "MWPhotoBrowser.h"
#import "SIAlertView.h"
#import "CLImageEditor.h"

@interface INCustomerProfileViewController : UIViewController <UIImagePickerControllerDelegate,UINavigationControllerDelegate,CLImageEditorDelegate>
{
    double contentSizeHeight;
    MBProgressHUD *hud;
    SIAlertView *sialertView;
    
    double ANIMATION_DELAY_DURATION;
    
    BOOL isThisFBProfileImage;
}
@property (strong,nonatomic) NSMutableArray *dateEventsArray;
@property (strong,nonatomic) NSMutableArray *eventsArray;
@property (strong,nonatomic) NSMutableDictionary *dateCategoriesDict;
@property (strong,nonatomic) NSMutableDictionary *finaldateCategoriesDict;

@property (strong,nonatomic) NSIndexPath *currentIndexPath;
@property (strong,nonatomic) NSDate *selectedDOBDate;
@property (strong,nonatomic) UIImage *placeholderImage;

@property (copy,nonatomic) NSString *facebook_useridString;
@property (copy,nonatomic) NSString *facebook_accesstokenString;

@property (nonatomic) BOOL newMedia;

@property (nonatomic, strong) AbstractActionSheetPicker *dateactionSheetPicker;
@property (strong, nonatomic) IBOutlet UIButton *infoBtn;

@property (strong, nonatomic) IBOutlet UIButton *fbLoginBtn;
@property (strong, nonatomic) IBOutlet UILabel *lblOR;


@property (strong, nonatomic) IBOutlet UILabel *lbl1Seperator;
@property (strong, nonatomic) IBOutlet UILabel *lbl2Seperator;
@property (strong, nonatomic) IBOutlet UILabel *lbl3Seperator;

@property (strong, nonatomic) IBOutlet UIImageView *img1Seperator;
@property (strong, nonatomic) IBOutlet UIImageView *img2Seperator;
@property (strong, nonatomic) IBOutlet UIImageView *img3Seperator;
@property (strong, nonatomic) IBOutlet UIImageView *img4Seperator;
@property (strong, nonatomic) IBOutlet UIImageView *img5Seperator;
@property (strong, nonatomic) IBOutlet UIImageView *img6Seperator;
@property (strong, nonatomic) IBOutlet UIImageView *img7Seperator;



@property (strong, nonatomic) IBOutlet UIScrollView *addDetailsScrollView;
@property (strong, nonatomic) IBOutlet UIView *addDetailsView;
@property (strong, nonatomic) IBOutlet UIImageView *customerProfileImageView;
@property (strong, nonatomic) IBOutlet UIButton *chooseImageBtn;
@property (strong, nonatomic) IBOutlet UITextField *nameText;
@property (strong, nonatomic) IBOutlet UIButton *dobBtn;
@property (strong, nonatomic) IBOutlet UIButton *maleBtn;
@property (strong, nonatomic) IBOutlet UIButton *femaleBtn;
@property (strong, nonatomic) IBOutlet UILabel *lblSelectedGender;

@property (strong, nonatomic) IBOutlet UITextField *emailText;
@property (strong, nonatomic) IBOutlet UITextField *cityText;
@property (strong, nonatomic) IBOutlet UITableView *eventsTableView;
@property (strong, nonatomic) IBOutlet UIButton *addEventsBtn;
@property (strong, nonatomic) IBOutlet UIButton *updateProfileBtn;
@property (strong, nonatomic) IBOutlet UILabel *lbleventsHeader;






@property (strong, nonatomic) IBOutlet UIView *infoView;
@property (strong, nonatomic) IBOutlet UILabel *lblinfoHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblinfoContent;
@property (strong, nonatomic) IBOutlet UIButton *infoOKBtn;
@property (strong, nonatomic) IBOutlet UIButton *privacyBtn;

- (IBAction)infoBtnPressed:(id)sender;
- (IBAction)infoOKBtnPressed:(id)sender;
- (IBAction)privacyBtnPressed:(id)sender;

- (IBAction)fbLoginBtnPressed:(id)sender;
- (IBAction)dobBtnPressed:(id)sender;
- (IBAction)maleBtnPressed:(id)sender;
- (IBAction)femaleBtnPressed:(id)sender;
- (IBAction)addEventsBtnPressed:(id)sender;
- (IBAction)updateProfileBtnPressed:(id)sender;
- (IBAction)chooseImageBtnPressed:(id)sender;

@end
