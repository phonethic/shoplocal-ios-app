//
//  CellWithImageView.m
//  shoplocal
//
//  Created by Kirti Nikam on 19/11/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "CellWithImageView.h"
#import "constants.h"
#import "CommonCallback.h"

@implementation CellWithImageView
@synthesize backView;
@synthesize lblTitle,lblLeftTitle,lblDescription,lblLikeCount,lblShareCount,lblTimeStamp;
@synthesize thumbImageView;
@synthesize offerImageView;
@synthesize likeBtn,shareBtn;
@synthesize shadowView;
@synthesize callBtn;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
//    [super setHighlighted:highlighted animated:animated];
//    DebugLog(@"highlighted %d",highlighted);
//    if (highlighted) {
//        shadowView.alpha = 0.0;
//    }else{
//        shadowView.alpha = 1.0;
//    }
//}

-(void)awakeFromNib{
    
    self.backgroundColor = [UIColor clearColor];

    double fontSizeHeader   = 16;
    double fontSizeContent  = 14;
    
    UIColor *colorHeader  = [UIColor whiteColor];
    UIColor *colorContent = [UIColor whiteColor];
    
    backView.clipsToBounds = YES;
    //backView = [CommonCallback setViewPropertiesWithRoundedCorner:backView];
    
//    shadowView.backgroundColor =  [UIColor clearColor];
//    shadowView.layer.cornerRadius = 8.0;
//    shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
//    shadowView.layer.shadowOffset = CGSizeMake(0, -3);
//    shadowView.layer.shadowRadius = 5;
//    shadowView.layer.shadowOpacity = 0.8;
//    shadowView.layer.masksToBounds = NO;
    // above code clutters the animation. got lag while scrolling tableview.  This is because calculating the drop shadow for view requires Core Animation to do an offscreen rendering pass to determine the exact shape of view in order to figure out how to render its drop shadow.
    // Got below solution from http://markpospesel.wordpress.com/2012/04/03/on-the-importance-of-setting-shadowpath/
//    [shadowView.layer setShadowPath:[[UIBezierPath bezierPathWithRect:shadowView.bounds] CGPath]];
    
    thumbImageView.contentMode = UIViewContentModeScaleAspectFill;
    thumbImageView.clipsToBounds = YES;

    lblTitle.numberOfLines = 1;
    lblTitle.font = DEFAULT_BOLD_FONT(fontSizeHeader);
    lblTitle.minimumScaleFactor = 14;
    lblTitle.adjustsFontSizeToFitWidth = TRUE;
    lblTitle.textAlignment = NSTextAlignmentLeft;
    lblTitle.textColor = colorHeader;
    lblTitle.highlightedTextColor = [UIColor whiteColor];
    lblTitle.backgroundColor =  [UIColor clearColor];
    lblTitle.shadowColor = [UIColor darkGrayColor];
    lblTitle.shadowOffset = CGSizeMake(1.0, 1.0);

    lblLeftTitle.numberOfLines = 1;
    lblLeftTitle.font = DEFAULT_BOLD_FONT(fontSizeContent);
    lblLeftTitle.minimumScaleFactor = 12;
    lblLeftTitle.adjustsFontSizeToFitWidth = TRUE;
    lblLeftTitle.textAlignment = NSTextAlignmentLeft;
    lblLeftTitle.textColor = colorContent;
    lblLeftTitle.highlightedTextColor = [UIColor whiteColor];
    lblLeftTitle.backgroundColor =  [UIColor clearColor];
    lblLeftTitle.shadowColor = [UIColor darkGrayColor];
    lblLeftTitle.shadowOffset = CGSizeMake(1.0, 1.0);
    
    lblDescription.numberOfLines = 4;
    lblDescription.font = DEFAULT_FONT(fontSizeHeader);
    lblDescription.minimumScaleFactor = 12;
    lblDescription.adjustsFontSizeToFitWidth = TRUE;
    lblDescription.textAlignment = NSTextAlignmentLeft;
    lblDescription.textColor = colorHeader;
    lblDescription.highlightedTextColor = [UIColor whiteColor];
    lblDescription.backgroundColor =  [UIColor clearColor];
    lblDescription.shadowColor = [UIColor darkGrayColor];
    lblDescription.shadowOffset = CGSizeMake(1.0, 1.0);
    
    lblTimeStamp.font = DEFAULT_BOLD_FONT(13);
    lblTimeStamp.minimumScaleFactor = 12;
    lblTimeStamp.adjustsFontSizeToFitWidth = TRUE;
    lblTimeStamp.textAlignment = NSTextAlignmentLeft;
    lblTimeStamp.textColor = colorContent;
    lblTimeStamp.highlightedTextColor = [UIColor whiteColor];
    lblTimeStamp.backgroundColor =  [UIColor clearColor];
    lblTimeStamp.shadowColor = [UIColor darkGrayColor];
    lblTimeStamp.shadowOffset = CGSizeMake(1.0, 1.0);
    
    lblLikeCount.font = DEFAULT_BOLD_FONT(20);
    lblLikeCount.minimumScaleFactor = 0.7;
    lblLikeCount.adjustsFontSizeToFitWidth = TRUE;
    lblLikeCount.textAlignment = NSTextAlignmentCenter;
    lblLikeCount.textColor = colorContent;
    lblLikeCount.highlightedTextColor = [UIColor whiteColor];
    lblLikeCount.backgroundColor =  [UIColor clearColor];
    lblLikeCount.shadowColor = [UIColor darkGrayColor];
    lblLikeCount.shadowOffset = CGSizeMake(1.0, 1.0);
    
    lblShareCount.font = DEFAULT_BOLD_FONT(20);
    lblShareCount.minimumScaleFactor = 0.7;
    lblShareCount.adjustsFontSizeToFitWidth = TRUE;
    lblShareCount.textAlignment = NSTextAlignmentCenter;
    lblShareCount.textColor = colorContent;
    lblShareCount.highlightedTextColor = [UIColor whiteColor];
    lblShareCount.backgroundColor =  [UIColor clearColor];
    lblShareCount.shadowColor = [UIColor darkGrayColor];
    lblShareCount.shadowOffset = CGSizeMake(1.0, 1.0);
    
    likeBtn.backgroundColor = [UIColor clearColor];
    likeBtn.titleLabel.text = @"";
    shareBtn.backgroundColor = [UIColor clearColor];
    shareBtn.titleLabel.text = @"";
    callBtn.backgroundColor = [UIColor clearColor];
    callBtn.titleLabel.text = @"";
    
    likeBtn.tag = 111;
    shareBtn.tag = 222;
    callBtn.tag = 333;

    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
    self.selectedBackgroundView = bgColorView;
}
@end
