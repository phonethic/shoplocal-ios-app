//
//  NMSplashViewController.m
//  NissanMicra
//
//  Created by Rishi on 24/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "NMSplashViewController.h"
#import "TTUIScrollViewSlidingPages.h"
#import "TTSlidingPage.h"
#import "TTSlidingPageTitle.h"
#import "INAppDelegate.h"
#import "NMSplashImageViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+WebCache.h"

static const CGFloat gradientWidth = 0.2;
static const CGFloat gradientDimAlpha = 0.5;
static const int animationFramesPerSec = 8;

#define SPLASH_LINK(ID) [NSString stringWithFormat:@"%@%@%@place_api/place_gallery?place_id=%@",LIVE_SERVER,URL_PREFIX,API_VERSION,ID]
#define THUMBNAIL_LINK(IMG_LINK) [NSString stringWithFormat:@"%@%@%@",LIVE_SERVER,URL_PREFIX,IMG_LINK]

@interface NMSplashViewController ()
@property(strong) NSDictionary *splashJson;
@end

@implementation NMSplashViewController
@synthesize goBtn;
@synthesize imagelinkArray;
@synthesize imageView1,imageView2;
@synthesize autoAnimationTimer;
@synthesize place_id;
@synthesize staticImageArray;
@synthesize comeFrom;
@synthesize menuBtn;
@synthesize menuHelpImgView;
@synthesize titlelinkArray;
@synthesize webview1,webview2;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    if (stopAnimation == NO) {
        stopAnimation = YES;
    }
    if (conn != nil) {
        [conn cancel];
        conn = nil;
    }
    [[self navigationController] setNavigationBarHidden:NO animated:NO];

}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    if (comeFrom == SPLASH_SCREEN) {
//        [[self navigationController] setNavigationBarHidden:YES animated:NO];
//    }
//    else{
//        [[self navigationController] setNavigationBarHidden:NO animated:NO];
//    }
    DebugLog(@"in splash place_id %@",place_id);

    [self makeViewShine:menuBtn];

    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    
    imagelinkArray = [[NSMutableArray alloc] init];
    staticImageArray = [[NSMutableArray alloc] init];
    titlelinkArray = [[NSMutableArray alloc] init];
    [staticImageArray addObject:@"1.jpg"];
    [staticImageArray addObject:@"2.jpg"];
    [staticImageArray addObject:@"3.jpg"];
    [staticImageArray addObject:@"4.jpg"];
    [staticImageArray addObject:@"5.jpg"];
    
    if (place_id != nil && ![place_id isEqualToString:@""]) {
        if([IN_APP_DELEGATE networkavailable])
        {
             [self sendHttpRequest:SPLASH_LINK(place_id)];
        }
        else  {
             [self parseFromFile];
        }
        [goBtn setTitle:@"Enter" forState:UIControlStateNormal];
        [goBtn setTitle:@"Enter" forState:UIControlStateHighlighted];
        
//        if ([self navigationController].navigationBarHidden) {
//            [UIView animateWithDuration:0.3 animations:^{
//                [[self navigationController] setNavigationBarHidden:NO animated:NO];
//            }];
//        }
    }else{
        [goBtn setTitle:@"Location" forState:UIControlStateNormal];
        [goBtn setTitle:@"Location" forState:UIControlStateHighlighted];
        
    }

    goBtn.titleLabel.font   =   DEFAULT_BOLD_FONT(16.0);
    goBtn.backgroundColor   =   [UIColor clearColor];
    [goBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [goBtn setTitleColor:[UIColor colorWithWhite:0.5 alpha:0.5] forState:UIControlStateHighlighted];
    
    menuBtn.titleLabel.font   =   DEFAULT_BOLD_FONT(16.0);
    menuBtn.backgroundColor   =   [UIColor clearColor];
    [menuBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [menuBtn setTitleColor:[UIColor colorWithWhite:0.5 alpha:0.5] forState:UIControlStateHighlighted];
    [menuBtn addTarget:self.navigationController.sideMenu action:@selector(toggleLeftSideMenu) forControlEvents:UIControlEventTouchUpInside];
    
    
    slider = [[TTScrollSlidingPagesController alloc] init];
    slider.zoomOutAnimationDisabled = YES;
    slider.disableUIPageControl = YES;
    slider.titleScrollerHidden = YES;
    slider.initialPageNumber = 0;
    slider.minimumZoom = 0.0;
    slider.dataSource = self; /*the current view controller (self) conforms to the TTSlidingPagesDataSource protocol)*/
    slider.view.frame = self.view.frame; //I'm setting up the view to be fullscreen in the current view
    [self.view addSubview:slider.view];
    [self addChildViewController:slider];
    [self.view bringSubviewToFront:goBtn];
    [self.view bringSubviewToFront:menuBtn];
    [self.view bringSubviewToFront:menuHelpImgView];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NetworkNotifyCallBack) name:NETWORK_NOTIFICATION object:nil];
//    if([imagelinkArray count] > 0)
//    {
        [slider reloadPages];
        [self performSelector:@selector(startAutoAnimationTimer) withObject:nil afterDelay:5.0];
    [self performSelector:@selector(startAutoAnimationTimer) withObject:nil afterDelay:2.0];
//    }
}

-(void)startAutoSideMenuOpen
{
    [menuBtn sendActionsForControlEvents:UIControlEventTouchUpInside];
    [self performSelector:@selector(closeAutoSideMenuOpen) withObject:nil afterDelay:1.0];
}

-(void)closeAutoSideMenuOpen
{
    [menuBtn sendActionsForControlEvents:UIControlEventTouchUpInside];
}

-(void)makeViewShine:(UIView*) view
{
//    view.layer.shadowColor = [UIColor whiteColor].CGColor;
//    view.layer.shadowRadius = 10.0f;
//    view.layer.shadowOpacity = 1.0f;
//    view.layer.shadowOffset = CGSizeZero;
//    
//    
//    [UIView animateWithDuration:0.7f delay:0 options:UIViewAnimationOptionAutoreverse | UIViewAnimationCurveEaseInOut | UIViewAnimationOptionRepeat | UIViewAnimationOptionAllowUserInteraction  animations:^{
//        
//        [UIView setAnimationRepeatCount:100000];
//        
//        view.transform = CGAffineTransformMakeScale(1.2f, 1.2f);
//        
//        
//    } completion:^(BOOL finished) {
//        
//        view.layer.shadowRadius = 0.0f;
//        view.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
//    }];
    
//    [UIView animateWithDuration:10 delay:0 options:UIViewAnimationOptionLayoutSubviews animations:^{
//        menuBtn.alpha = 0.00;
//    } completion:^(BOOL finished) {
//        menuBtn.alpha = 1.00;
//    }];
    
    // Shrink down to 90% of its original value
	menuBtn.layer.transform = CATransform3DMakeScale(0.80, 0.80, 1);
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform"];
	animation.toValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
	animation.autoreverses = YES;
	animation.duration = 2.0;
	animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
	animation.repeatCount = HUGE_VALF;
	[menuBtn.layer addAnimation:animation forKey:@"pulseAnimation"];
    
//    //Create an animation with pulsating effect
//    CABasicAnimation *theAnimation;
//    
//    //within the animation we will adjust the "opacity"
//    //value of the layer
//    theAnimation=[CABasicAnimation animationWithKeyPath:@"opacity"];
//    //animation lasts 0.4 seconds
//    theAnimation.duration=0.4;
//    //and it repeats forever
//    theAnimation.repeatCount= HUGE_VALF;
//    //we want a reverse animation
//    theAnimation.autoreverses=YES;
//    //justify the opacity as you like (1=fully visible, 0=unvisible)
//    theAnimation.fromValue=[NSNumber numberWithFloat:1.0];
//    theAnimation.toValue=[NSNumber numberWithFloat:0.5];
//    
//    //Assign the animation to your UIImage layer and the
//    //animation will start immediately
//    [menuBtn.layer addAnimation:theAnimation
//                           forKey:@"animateOpacity"];
}



-(void)NetworkNotifyCallBack
{
    DebugLog(@"Splash : Got Network Notification");
     if (place_id != nil && ![place_id isEqualToString:@""]) {
         [self performSelector:@selector(sendHttpRequest:) withObject:SPLASH_LINK(self.place_id) afterDelay:2.0];
     }
}

-(void) parseFromFile
{
    NSDictionary *json = [IN_APP_DELEGATE getTextFromFile:@"splash"];
    //DebugLog(@"\n data:%@\n\n", data);
    if(json != nil) {
        [imagelinkArray removeAllObjects];
        NSArray* jsonArray = [json objectForKey:@"message"];
        for (NSDictionary* songattributeDict in jsonArray) {
            NSString * link = [NSString stringWithFormat:@"%@%@%@",LIVE_SERVER,URL_PREFIX,[songattributeDict objectForKey:@"image_url"]];
            DebugLog(@"link: %@", link);
            [imagelinkArray addObject:link];
        }
//        if([imagelinkArray count] > 0)
//        {
            [slider reloadPages];
            [self performSelector:@selector(startAutoAnimationTimer) withObject:nil afterDelay:5.0];
//        }
    }
}

-(void)sendHttpRequest:(NSString *)urlString
{
    DebugLog(@"---->%@",urlString);
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [urlRequest setValue:@"Fool" forHTTPHeaderField:@"X-API-KEY"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        DebugLog(@"---->%@",self.splashJson);
                                                        [IN_APP_DELEGATE writeToTextFile:JSON name:@"splash"];
                                                        if(self.splashJson  != nil) {
                                                            [imagelinkArray removeAllObjects];
                                                            [titlelinkArray removeAllObjects];
                                                            NSArray* jsonArray = [self.splashJson  objectForKey:@"data"];
                                                            for (NSDictionary* songattributeDict in jsonArray) {
                                                                NSString * link = [NSString stringWithFormat:@"%@%@%@",LIVE_SERVER,URL_PREFIX,[songattributeDict objectForKey:@"image_url"]];
                                                                //DebugLog(@"link: %@", link);
                                                                NSString * title = [songattributeDict objectForKey:@"title"];
                                                                //NSLog(@"title->%@",title);
                                                                [imagelinkArray addObject:link];
                                                                [titlelinkArray addObject:[self addHtmlTag:title]];
                                                            }
//                                                            if([imagelinkArray count] > 0)
//                                                            {
                                                                [slider reloadPages];
                                                                [self performSelector:@selector(startAutoAnimationTimer) withObject:nil afterDelay:5.0];
//                                                            }
                                                        }
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        [INUserDefaultOperations performSelector:@selector(showAlert:) withObject:[NSString stringWithFormat:@"%@",[error localizedDescription]] afterDelay:2.0];
                                                    }];
    
    
    
    [operation start];
}

-(NSString *)addHtmlTag:(NSString *)ltitle
{
    NSString *embedHTML =[NSString stringWithFormat:@"<html><head><meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1'><style type='text/css'>body{color:#FFF;}h2,h3,h4,h5{margin:0px;padding:0px;}h2{font-size:20px;}h3{font-size:15px;}h4{font-size:15px;}h5{font-size:12px;color: #516233;}</style></head><body>%@</body></html>",ltitle];
    return embedHTML;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.view bringSubviewToFront:goBtn];
    [self.view bringSubviewToFront:menuBtn];
    [self.view bringSubviewToFront:menuHelpImgView];
    [self.view setNeedsDisplay];
}

- (IBAction)goBtnPressed:(id)sender {
    DebugLog(@"Enter the app");
    [IN_APP_DELEGATE animationStop];
}

- (IBAction)menuBtnPressed:(id)sender {
//    if ([self navigationController].navigationBarHidden) {
//        [UIView animateWithDuration:0.3 animations:^{
//            [[self navigationController] setNavigationBarHidden:NO animated:NO];
//        }];
//    }
}


#pragma mark TTSlidingPagesDataSource methods
-(int)numberOfPagesForSlidingPagesViewController:(TTScrollSlidingPagesController *)source{
    if([imagelinkArray count] > 0)
    {
        return [imagelinkArray count];
    }
    else
    {
        return [staticImageArray count];
    }
}

-(TTSlidingPage *)pageForSlidingPagesViewController:(TTScrollSlidingPagesController*)source atIndex:(int)index{
    UIViewController *viewController = [[NMSplashImageViewController alloc] init];
    if([imagelinkArray count] > 0)
    {
        DebugLog(@"%@",[imagelinkArray objectAtIndex:index]);
        ((NMSplashImageViewController*)viewController).imglink = [imagelinkArray objectAtIndex:index];
        ((NMSplashImageViewController*)viewController).titlelink = [titlelinkArray objectAtIndex:index];
    }
    else
    {
        DebugLog(@"%@",[staticImageArray objectAtIndex:index]);
        ((NMSplashImageViewController*)viewController).imglink = [staticImageArray objectAtIndex:index];
    }
    return [[TTSlidingPage alloc] initWithContentViewController:viewController];
}

-(TTSlidingPageTitle *)titleForSlidingPagesViewController:(TTScrollSlidingPagesController *)source atIndex:(int)index{
    TTSlidingPageTitle *title = [[TTSlidingPageTitle alloc] initWithHeaderText:@""];
    return title;
}


#pragma FadeIn-Out auto Animation
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    DebugLog(@"autoAnimationTimer %@",autoAnimationTimer);
    if (autoAnimationTimer) {
        [autoAnimationTimer invalidate], autoAnimationTimer=nil;
        TAP_DETECTED = YES;
        [slider reloadPages];
        [slider scrollToPage:currentPageIndex animated:NO];
        [self.view bringSubviewToFront:slider.view];
        [self.view bringSubviewToFront:goBtn];
        [self.view bringSubviewToFront:menuBtn];
        [self.view bringSubviewToFront:menuHelpImgView];
        slider.view.alpha = 1.0;
        goBtn.hidden = FALSE;
        menuBtn.hidden = FALSE;
        [self.view sendSubviewToBack:imageView2];
        [self.view sendSubviewToBack:imageView1];

        imageView2.hidden = TRUE;
        imageView1.hidden = TRUE;
        
        [self.view sendSubviewToBack:webview2];
        [self.view sendSubviewToBack:webview1];
        
        webview2.hidden = TRUE;
        webview1.hidden = TRUE;
    }
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    DebugLog(@"touchesEnded");
    [self performSelector:@selector(startAutoAnimationTimer) withObject:nil afterDelay:5.0];
}
-(void)startAutoAnimationTimer
{
//    if (imagelinkArray.count <= 0) {
//        return;
//    }
    if (!autoAnimationTimer)  {
        autoAnimationTimer = [NSTimer scheduledTimerWithTimeInterval:0.8 target:self
                                                    selector: @selector(performAutoAnimation) userInfo:nil repeats:NO];
    }
    DebugLog(@"startAutoAnimationTimer [autoAnimationTimer isValid] %d",[autoAnimationTimer isValid]);

}
-(void)performAutoAnimation
{
    if (autoAnimationTimer) {
        [self.view sendSubviewToBack:slider.view];
        slider.view.alpha = 0.0;
        
        [self.view bringSubviewToFront:imageView2];
        [self.view bringSubviewToFront:imageView1];
        [self.view bringSubviewToFront:webview2];
        [self.view bringSubviewToFront:webview1];
        [self.view bringSubviewToFront:goBtn];
        [self.view bringSubviewToFront:menuBtn];
        [self.view bringSubviewToFront:menuHelpImgView];
        goBtn.hidden = FALSE;
        menuBtn.hidden = FALSE;

        currentPageIndex = [slider getCurrentDisplayedPage];
        if (imageView1.alpha == 0.0) {
            if([imagelinkArray count] > 0)
            {
//                [imageView2 setImageWithURL:[imagelinkArray objectAtIndex:currentPageIndex]
//                       placeholderImage:[UIImage imageNamed:@"splash.jpg"]
//                                success:^(UIImage *image) {
//                                    //DebugLog(@"success");
//                                }
//                                failure:^(NSError *error) {
//                                    //DebugLog(@"write error %@", error);
//                                }];
                
                [imageView2 setImageWithURL:[NSURL URLWithString:[imagelinkArray objectAtIndex:currentPageIndex]] placeholderImage:[UIImage imageNamed:@"splash.jpg"] completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType) {

                }];
                webview2.backgroundColor = [UIColor clearColor];
                [webview2 loadHTMLString:[titlelinkArray objectAtIndex:currentPageIndex] baseURL:nil];
                [webview2.scrollView setBounces:NO];
                [webview2.scrollView setScrollEnabled:NO];
                [webview2 setOpaque:NO];
            }
            else
            {
                [imageView2 setImage:[UIImage imageNamed:[staticImageArray objectAtIndex:currentPageIndex]]];
            }

        }else{
            if([imagelinkArray count] > 0)
            {
//                [imageView1 setImageWithURL:[imagelinkArray objectAtIndex:currentPageIndex]
//                           placeholderImage:[UIImage imageNamed:@"splash.jpg"]
//                                    success:^(UIImage *image) {
//                                        //DebugLog(@"success");
//                                    }
//                                    failure:^(NSError *error) {
//                                        //DebugLog(@"write error %@", error);
//                                    }];
                [imageView1 setImageWithURL:[NSURL URLWithString:[imagelinkArray objectAtIndex:currentPageIndex]] placeholderImage:[UIImage imageNamed:@"splash.jpg"] completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType) {
                    
                }];
                webview1.backgroundColor = [UIColor clearColor];
                [webview1 loadHTMLString:[titlelinkArray objectAtIndex:currentPageIndex] baseURL:nil];
                [webview1.scrollView setBounces:NO];
                [webview1.scrollView setScrollEnabled:NO];
                [webview1 setOpaque:NO];
            }
            else
            {
                [imageView1 setImage:[UIImage imageNamed:[staticImageArray objectAtIndex:currentPageIndex]]];
            }
        }
        imageView2.hidden = FALSE;
        imageView1.hidden = FALSE;
        webview2.hidden = FALSE;
        webview1.hidden = FALSE;
        if([imagelinkArray count] > 0)
        {
            if (currentPageIndex == imagelinkArray.count-1) {
                currentPageIndex = 0;
            }else{
                currentPageIndex ++;
            }
        }
        else
        {
            if (currentPageIndex == staticImageArray.count-1) {
                currentPageIndex = 0;
            }else{
                currentPageIndex ++;
            }
        }
        TAP_DETECTED = NO;
        [self performSelector:@selector(startAutoAnimation:) withObject:[NSNumber numberWithInt:currentPageIndex] afterDelay:0.0];
    }
}

-(void)startAutoAnimation:(NSNumber*)pageIndex
{
    if (!self.isViewLoaded || !self.view.window){
        return;
    }
    DebugLog(@"index %d",[pageIndex intValue]);
    currentPageIndex = [pageIndex intValue];
    if (TAP_DETECTED || stopAnimation) {
        return;
    }
    UIImageView *preImageView;
    UIImageView *nextImageView;
    UIWebView *prewebview;
    UIWebView *nextwebview;
    if (imageView1.alpha == 0.0) {
        preImageView = imageView2;
        nextImageView = imageView1;
        prewebview = webview2;
        nextwebview = webview1;
    }else{
        preImageView = imageView1;
        nextImageView = imageView2;
        prewebview = webview1;
        nextwebview = webview2;
    }
    [UIView  transitionWithView:preImageView duration:2.0  options:UIViewAnimationOptionTransitionNone |UIViewAnimationOptionAllowUserInteraction
                     animations:^(void) {
                         preImageView.alpha = 0.0;
                         prewebview.alpha = 0.0;
                         [UIView  transitionWithView:nextImageView duration:2.0  options:UIViewAnimationOptionTransitionNone |UIViewAnimationOptionAllowUserInteraction
                                          animations:^(void) {
                                              nextImageView.alpha = 1.0;
                                              nextwebview.alpha = 1.0;
                                              if([imagelinkArray count] > 0)
                                              {
//                                                  [nextImageView setImageWithURL:[imagelinkArray objectAtIndex:[pageIndex intValue]]
//                                                                placeholderImage:[UIImage imageNamed:@"Default.png"]
//                                                                         success:^(UIImage *image) {
//                                                                             //DebugLog(@"success");
//                                                                         }
//                                                                         failure:^(NSError *error) {
//                                                                             //DebugLog(@"write error %@", error);
//                                                                         }];
                                                  [nextImageView setImageWithURL:[imagelinkArray objectAtIndex:[pageIndex intValue]] placeholderImage:[UIImage imageNamed:@"Default.png"] completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType) {
                                                  }];
                                                  
                                                  nextwebview.backgroundColor = [UIColor clearColor];
                                                  [nextwebview loadHTMLString:[titlelinkArray objectAtIndex:[pageIndex intValue]] baseURL:nil];
                                                  [nextwebview.scrollView setBounces:NO];
                                                  [nextwebview.scrollView setScrollEnabled:NO];
                                                  [nextwebview setOpaque:NO];
                                              }
                                              else
                                              {
                                                  [nextImageView setImage:[UIImage imageNamed:[staticImageArray objectAtIndex:[pageIndex intValue]]]];
                                              }
                                          }
                                          completion:^(BOOL finished)
                                          {
                                              if (imagelinkArray.count > 0) {
                                                  if ([pageIndex intValue]== imagelinkArray.count-1) {
                                                      [self performSelector:@selector(startAutoAnimation:) withObject:[NSNumber numberWithInt:0] afterDelay:1.0];
                                                  }else{
                                                   [self performSelector:@selector(startAutoAnimation:) withObject:[NSNumber numberWithInt:[pageIndex intValue]+1] afterDelay:1.0];
                                                  }
                                              }else{
                                                  if ([pageIndex intValue]== staticImageArray.count-1) {
                                                      [self performSelector:@selector(startAutoAnimation:) withObject:[NSNumber numberWithInt:0] afterDelay:1.0];
                                                  }else{
                                                      [self performSelector:@selector(startAutoAnimation:) withObject:[NSNumber numberWithInt:[pageIndex intValue]+1] afterDelay:1.0];
                                                  }
                                              }
                                          }];
                     }
                     completion:^(BOOL finished)
                     {}];
    preImageView = nil;
    nextImageView = nil;
    prewebview = nil;
    nextwebview = nil;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setGoBtn:nil];
    [self setImageView1:nil];
    [self setImageView2:nil];
    [self setMenuBtn:nil];
    [self setMenuHelpImgView:nil];
    [self setWebview1:nil];
    [self setWebview2:nil];
    [super viewDidUnload];
}

#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
- (void)dealloc {
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
