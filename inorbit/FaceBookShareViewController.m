//
//  FaceBookShareViewController.m
//  shoplocal
//
//  Created by Kirti Nikam on 06/09/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "FaceBookShareViewController.h"
#import "INAppDelegate.h"
#import <FacebookSDK/FBSessionTokenCachingStrategy.h>
#import "CommonCallback.h"

#define LOGO_PICTURE [NSString stringWithFormat:@"%@%@assets/images/icons/shoplocal-logo.png",LIVE_SERVER,URL_PREFIX]

NSString *const kPlaceholderPostMessage = @"Say something about this...";

// We will use retryCount as part of the error handling logic for errors that need retries
static int retryCount = 0;

@interface FaceBookShareViewController ()

@end

@implementation FaceBookShareViewController
@synthesize fbLoginBtn,cancelBtn;
@synthesize FBtitle,FBtCaption,FBtLink,FBtPic;
@synthesize mainFBView,userProfileImage,userNameLabel,postMessageTextView,postFBBtn,loginFBBtn,cancelFBBtn;
@synthesize postParams;
@synthesize postMessageText;
@synthesize fbdelegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    DebugLog(@"logo url -%@-",LOGO_PICTURE);
    [self setUI];
    self.postParams =
    [[NSMutableDictionary alloc] initWithObjectsAndKeys:
     @"http://www.shoplocal.co.in", @"link",
                                                        LOGO_PICTURE, @"picture",
                                                        @"Title of Post", @"name",
                                                        @"Shoplocal", @"caption",
                                                        @"Local Noticeboard for Merchants, Service Providers & Residents of a specific geographical area.", @"description",
                                                        nil];
    
    postMessageTextView.text = postMessageText;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sessionStateChanged:) name:FBSessionStateChangedNotification object:nil];
    [IN_APP_DELEGATE openSessionWithAllowLoginUI:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setFbLoginBtn:nil];
    [self setCancelBtn:nil];
    [self setLoginFBBtn:nil];
    [self setPostFBBtn:nil];
    [self setCancelFBBtn:nil];
    [self setMainFBView:nil];
    [self setPostMessageTextView:nil];
    [self setUserNameLabel:nil];
    [self setUserProfileImage:nil];
    [super viewDidUnload];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma internal Callbacks
-(void)setUI{
    fbLoginBtn.titleLabel.font  = DEFAULT_FONT(16);
    cancelBtn.titleLabel.font   = DEFAULT_FONT(18);
    
    self.mainFBView.layer.cornerRadius = 10;
    self.mainFBView.layer.masksToBounds = YES;
    
    postMessageTextView.delegate =  self;
    
    postFBBtn.hidden = TRUE;
    loginFBBtn.hidden = FALSE;
    mainFBView.hidden = TRUE;
    
    UIImage *cancelButtonImage;
    cancelButtonImage = [[UIImage imageNamed:@"DEFacebookSendButtonPortrait"] stretchableImageWithLeftCapWidth:4 topCapHeight:0];
    [self.cancelFBBtn setBackgroundImage:cancelButtonImage forState:UIControlStateNormal];
    [self.postFBBtn setBackgroundImage:cancelButtonImage forState:UIControlStateNormal];
    [self.loginFBBtn setBackgroundImage:cancelButtonImage forState:UIControlStateNormal];
}
/*
 * This sets up the placeholder text.
 */
- (void)resetPostMessage
{
    self.postMessageTextView.text = kPlaceholderPostMessage;
    self.postMessageTextView.textColor = [UIColor lightGrayColor];
}

#pragma mark - UITextViewDelegate methods
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    // Clear the message text when the user starts editing
    if ([textView.text isEqualToString:kPlaceholderPostMessage]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    // Reset to placeholder text if the user is done
    // editing and no message has been entered.
    if ([textView.text isEqualToString:@""]) {
        [self resetPostMessage];
    }
}


#pragma UIButton Callbacks
- (IBAction)fbLoginBtnPressed:(id)sender {
    if([IN_APP_DELEGATE networkavailable])
    {
        [IN_APP_DELEGATE openSessionWithAllowLoginUI:YES];
    } else {
         [INUserDefaultOperations showOfflineAlert];
    }
}

- (IBAction)cancelBtnPressed:(id)sender {
    if (![[self modalViewController] isBeingDismissed]){
        [self dismissViewControllerAnimated:YES completion:^{
            self.fbdelegate = nil;
        }];
    }
}

- (IBAction)postFBBtnPressed:(id)sender {
    if (![self.postMessageTextView.text isEqualToString:kPlaceholderPostMessage] &&
        ![self.postMessageTextView.text isEqualToString:@""]) {
        [self.postParams setObject:self.postMessageTextView.text forKey:@"message"];
    }
    DebugLog(@"-%@-",FBtPic);
    [self.postParams setObject:self.FBtitle forKey:@"name"];
//    [self.postParams setObject:self.FBtCaption forKey:@"caption"];
    if(FBtPic != nil && ![FBtPic isEqualToString:@""])
    {
        [self.postParams setObject:self.FBtPic forKey:@"picture"];
    }
    if (FBtLink != nil && ![self.FBtLink isEqualToString:@""]) {
        [self.postParams setObject:FBtLink forKey:@"link"];
    }
    
    /*self.postParams =
    [[NSMutableDictionary alloc] initWithObjectsAndKeys:
     @"https://developers.facebook.com/ios", @"link",
     @"https://developers.facebook.com/attachment/iossdk_logo.png", @"picture",
     @"Facebook SDK for iOS", @"name",
     @"build apps.", @"caption",
     @"testing for my app.", @"description",
     nil];*/
    
    DebugLog(@"postParams %@",postParams);

    // Ask for publish_actions permissions in context
    [self requestPermission];
}

-(void)requestPermission{
    // Ask for publish_actions permissions in context
    if ([FBSession.activeSession.permissions
         indexOfObject:@"publish_actions"] == NSNotFound) {
        // No permissions found in session, ask for it
        [FBSession.activeSession
         requestNewPublishPermissions:@[@"publish_actions"]
         defaultAudience:FBSessionDefaultAudienceFriends
         completionHandler:^(FBSession *session, NSError *error) {
             if (error) {
                 // Handle new permissions request errors
                 [self handleRequestPermissionError:error];
             } else {
                 // Make API call
                 // If permissions granted, publish the story
                 [self makeGraphAPICall];
             }
         }];
    } else {
        // If permissions present, publish the story
        [self makeGraphAPICall];
    }
}

// Helper method to handle errors during permissions request
- (void)handleRequestPermissionError:(NSError *)error
{
    DebugLog(@"================handleRequestPermissionError============\n error %@ \n ================================================",error);

    NSString *alertText;
    if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
        // Error requires people using an app to make an out-of-band action to recover
        alertText = [FBErrorUtility userMessageForError:error];
        [self performSelector:@selector(showFBAlert:) withObject:alertText afterDelay:1.0];
    } else {
        // We need to handle the error
        if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
            // Ignore it or...
            alertText = @"Permission not granted\nYour post could not be completed because you didn't grant the necessary permissions.";
            [self performSelector:@selector(showFBAlert:) withObject:alertText afterDelay:1.0];
            
        } else{
            // All other errors that can happen need retries
            // Show the user a generic error message
            alertText = @"Something went wrong.Please retry";
            [self performSelector:@selector(showFBAlert:) withObject:alertText afterDelay:1.0];
        }
    }
}


/*
 * Publish the story
 */
// Example Graph API call
- (void)makeGraphAPICall
{
    [CommonCallback showProgressHud:@"Facebook Share" subtitle:@"Processing"];
    [FBRequestConnection
     startWithGraphPath:@"me/feed"
     parameters:self.postParams
     HTTPMethod:@"POST"
     completionHandler:^(FBRequestConnection *connection,
                         id result,
                         NSError *error) {
         [CommonCallback hideProgressHud];
         // Completion handler block
         int fbPostResult;
         if (error) {
             [self handleAPICallError:error];
             fbPostResult = FACEBOOK_POST_FAIL;
         } else {
             retryCount = 0;
             NSString *alertText = @"Your message has been successfully posted on your facebook wall.";
             fbPostResult = FACEBOOK_POST_SUCCESS;
             [self performSelector:@selector(showFBAlert:) withObject:alertText afterDelay:1.0];
         }
         if ([[INUserDefaultOperations getCustomerLoginFrom] isEqualToString:SHOPLOCALSERVER]) {
             DebugLog(@"User Logged by SHOPLOCALSERVER server");
             [FBSession.activeSession closeAndClearTokenInformation];
         }else if ([[INUserDefaultOperations getCustomerLoginFrom] isEqualToString:FACEBOOKSERVER]){
             DebugLog(@"User Logged by Facebook server");
         }else{
             DebugLog(@"User Logged by Other server -%@-",[INUserDefaultOperations getCustomerLoginFrom]);
         }
         [self.fbdelegate faceBookCompletionCallBack:fbPostResult];
         [self dismissViewControllerAnimated:YES completion:^{
             self.fbdelegate = nil;
         }];
     }];
}

-(void)showFBAlert:(NSString*) message
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}



// Helper method to handle errors during API calls
- (void)handleAPICallError:(NSError *)error
{
    DebugLog(@"================handleAPICallError============\n error %@ \n ================================================",error);

    // If the user has removed a permission that was previously granted
    if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryPermissions) {
        DebugLog(@"Re-requesting permissions");
        // Ask for required permissions.
        [self requestPermission];
        return;
    }
    
    // Some Graph API errors need retries, we will have a simple retry policy of one additional attempt
    // We also retry on a throttling error message, a more sophisticated app should consider a back-off period
    retryCount++;
    if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryRetry ||
        [FBErrorUtility errorCategoryForError:error] == FBErrorCategoryThrottling) {
        if (retryCount < 2) {
            DebugLog(@"Retrying open graph post");
            // Recovery tactic: Call API again.
            [self makeGraphAPICall];
            return;
        } else {
            DebugLog(@"Retry count exceeded.");
            return;
        }
    }
    
    // For all other errors...
    NSString *alertText;
    
    if ([FBErrorUtility shouldNotifyUserForError:error]) {
        // If the user should be notified, we show them the corresponding message
        alertText = [FBErrorUtility userMessageForError:error];
        [self performSelector:@selector(showFBAlert:) withObject:alertText afterDelay:1.0];
    } else {
        // We need to handle the error
        if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
            // Ignore it or...
            alertText = @"Permission not granted\nYour post could not be completed because you didn't grant the necessary permissions.";
            [self performSelector:@selector(showFBAlert:) withObject:alertText afterDelay:1.0];
            
        } else{
            
            // Get more error information from the error
            NSDictionary *parsedResponse = [error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"];
            NSDictionary *errorInformation = [[parsedResponse objectForKey:@"body"] objectForKey:@"error"];
            DebugLog(@"errorInformation %@",errorInformation);
            
            int errorCode = 0;
            if ([errorInformation objectForKey:@"code"]){
                errorCode = [[errorInformation objectForKey:@"code"] integerValue];
            }
            DebugLog(@"errorCode %d",errorCode);
            
            int errorSubcode = 0;
            if ([errorInformation objectForKey:@"error_subcode"]){
                errorSubcode = [[errorInformation objectForKey:@"error_subcode"] integerValue];
            }
            DebugLog(@"errorSubcode %d",errorSubcode);
            
            if (errorCode == 190 && errorSubcode == 458) {
                // Tell the user the action failed because duplicate action-object  are not allowed
                alertText = @"Permission not granted\nYour post could not be completed because you didn't grant the necessary basic read permissions like to access your profile at login time.";
                [self performSelector:@selector(showFBAlert:) withObject:alertText afterDelay:1.0];
            }
//            else{
//                // show a generic error message
//                DebugLog(@"Unexpected error posting to open graph: %@", error);
//                alertTitle = @"Something went wrong";
//                alertText = @"Please try again later.";
//            }
        }
    }
}

- (void)showMessage:(NSString *)text withTitle:(NSString *)title
{
    [[[UIAlertView alloc] initWithTitle:title
                                message:text
                               delegate:nil
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
}




#pragma NSNotificationCenter Callbacks
/*
 * Configure the logged in versus logged out UI
 */
- (void)sessionStateChanged:(NSNotification*)notification {
    if (FBSession.activeSession.isOpen) {
        DebugLog(@"User Logged In");
        [[FBRequest requestForMe] startWithCompletionHandler:
         ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
             if (error) {
                 DebugLog(@"sessionStateChanged error %@",error);

                 postFBBtn.enabled =  FALSE;
                 postFBBtn.hidden = TRUE;
                 loginFBBtn.hidden = FALSE;
                 self.fbLoginBtn.hidden = FALSE;
                 self.cancelBtn.hidden = FALSE;
                 self.mainFBView.hidden = TRUE;
                 [self handleAuthError:error];
             }else{
                 DebugLog(@"user ID %@",[user objectForKey:@"id"]);
                 DebugLog(@"%@",[user objectForKey:@"name"]);
                 self.fbLoginBtn.hidden = TRUE;
                 self.cancelBtn.hidden = TRUE;
                 self.userNameLabel.text = user.name;
                 self.userProfileImage.profileID = [user objectForKey:@"id"];
                 postFBBtn.enabled =  TRUE;
                 postFBBtn.hidden = FALSE;
                 self.mainFBView.hidden = FALSE;
                 loginFBBtn.hidden = TRUE;
             }
         }];
    }
}

- (void)handleAuthError:(NSError *)error
{
    NSString *alertText;
    DebugLog(@"================FB handleAuthError============\n error %@ \n ================================================",error);
    
    if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
        // Error requires people using you app to make an action outside your app to recover
        alertText = [FBErrorUtility userMessageForError:error];
        [self performSelector:@selector(showFBAlert:) withObject:alertText afterDelay:1.0];
        
    } else {
        DebugLog(@"errorCategoryForError %d",[FBErrorUtility errorCategoryForError:error]);
        
        // You need to find more information to handle the error within your app
        if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
            //The user refused to log in into your app, either ignore or...
            alertText = @"Login cancelled.\nYour Login could not be completed because you didn't grant the necessary permissions.";
            [self performSelector:@selector(showFBAlert:) withObject:alertText afterDelay:1.0];
            
        } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession){
            // We need to handle session closures that happen outside of the app
            alertText = @"Session Error.\nYour current session is no longer valid. Please log in again.";
            [self performSelector:@selector(showFBAlert:) withObject:alertText afterDelay:1.0];
            
        } else {
            
            // Get more error information from the error
            //            int errorCode = error.code;
            //            NSDictionary *errorInformation = [[[[error userInfo] objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"]
            //                                               objectForKey:@"body"]
            //                                              objectForKey:@"error"];
            
            // this is the parsed result of the graph call; some errors can appear here, too, sadly
            NSDictionary *parsedResponse = [error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"];
            NSDictionary *errorInformation = [[parsedResponse objectForKey:@"body"] objectForKey:@"error"];
            DebugLog(@"errorInformation %@",errorInformation);
            
            int errorCode = 0;
            if ([errorInformation objectForKey:@"code"]){
                errorCode = [[errorInformation objectForKey:@"code"] integerValue];
            }
            DebugLog(@"errorCode %d",errorCode);
            
            int errorSubcode = 0;
            if ([errorInformation objectForKey:@"error_subcode"]){
                errorSubcode = [[errorInformation objectForKey:@"error_subcode"] integerValue];
            }
            DebugLog(@"errorSubcode %d",errorSubcode);
            
            if (errorCode == 190 && (errorSubcode == 459 || errorSubcode == 464)) {
                alertText = @"Error validating access token: You cannot access the app till you log in to www.facebook.com and follow the instructions given.";
                [self performSelector:@selector(showFBAlert:) withObject:alertText afterDelay:1.0];
            }else{
                // All other errors that can happen need retries
                // Show the user a generic error message
                alertText = @"Something went wrong.\nPlease retry";
                [self performSelector:@selector(showFBAlert:) withObject:alertText afterDelay:1.0];
            }
        }
    }
}
@end
