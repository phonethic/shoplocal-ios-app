//
//  INFBWebViewController.h
//  shoplocal
//
//  Created by Rishi on 25/09/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface INFBWebViewController : UIViewController <UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIWebView *fbWebView;
@property (nonatomic, copy) NSString *openURL;
@property (nonatomic,readwrite)int type;
@end
