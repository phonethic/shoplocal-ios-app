//
//  INMerchantLoginViewController.h
//  shoplocal
//
//  Created by Rishi on 12/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "INMerchantSignUPViewController.h"
#import "INTermsViewController.h"
#import "INMerchantVerifyViewController.h"

@class INStoreDetailObj;
@interface INMerchantLoginViewController : UIViewController <INMerchantSignUPViewControllerDelegate,termsDelegate,INMerchantVerifyViewControllerDelegate>
{
    INMerchantSignUPViewController *signupController;
    INMerchantSignUPViewController *forgotpassController;
    UINavigationController *signupControllernavBar;
    MBProgressHUD *hud;
    INStoreDetailObj *storeObj;
    sqlite3 *shoplocalDB;
    
    BOOL isOpenedTermsViewWhileSignUp;
    BOOL isRetryingToAcceptTerms;
    
    NSURLConnection *connectionRegisterNumber;
    NSMutableData *responseAsyncData;
    int status;

}
@property (strong, nonatomic) IBOutlet UIImageView *merchantImageView;

@property (strong, nonatomic) IBOutlet UIView *loginView;

@property (strong, nonatomic) IBOutlet UIButton *loginQuesBtn;

@property (strong, nonatomic) IBOutlet UILabel *lblHeader;
@property (strong, nonatomic) IBOutlet UIView *signUpView;
@property (strong, nonatomic) IBOutlet UILabel *lblfirsttimeuser;

@property (strong, nonatomic) IBOutlet UITextField *merchantnameText;

@property (strong, nonatomic) IBOutlet UITextField *usernameText;
@property (strong, nonatomic) IBOutlet UITextField *passwordtext;
@property (strong, nonatomic) IBOutlet UIButton *loginBtn;
@property (strong, nonatomic) IBOutlet UIButton *signupBtn;
@property (strong, nonatomic) IBOutlet UIButton *forgotBtn;
@property (strong, nonatomic) IBOutlet UITextField *codeText;

@property (strong,nonatomic) NSMutableArray *countryCodeArray;
@property (strong,nonatomic) NSMutableArray *countryCodePickerArray;
@property (strong, nonatomic) NSMutableArray *storeArray;

@property (readwrite, nonatomic) int rowcount;

@property (strong, nonatomic) IBOutlet UIButton *termsBtn;
@property (strong, nonatomic) IBOutlet UIButton *privacyBtn;
@property (strong, nonatomic) IBOutlet UIButton *infoBtn;

@property (strong, nonatomic) IBOutlet UIView *infoView;
@property (strong, nonatomic) IBOutlet UILabel *lblinfoHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblinfoContent;
@property (strong, nonatomic) IBOutlet UIButton *infoOKBtn;
@property (strong, nonatomic) IBOutlet UIButton *infoPrivacyBtn;
@property (strong, nonatomic) IBOutlet UITextView *txtViewinfoContent;

- (IBAction)infoOKBtnPressed:(id)sender;
- (IBAction)infoPrivacyBtnPressed:(id)sender;

- (IBAction)forgotBtnPressed:(id)sender;
- (IBAction)loginBtnPressed:(id)sender;
- (IBAction)signupBtnPressed:(id)sender;
- (IBAction)termsBtnPressed:(id)sender;
- (IBAction)privacyBtnPressed:(id)sender;
- (IBAction)infoBtnPressed:(id)sender;
- (IBAction)loginQuesBtnPressed:(id)sender;
@end
