//
//  INCategoryViewController.h
//  shoplocal
//
//  Created by Kirti Nikam on 09/01/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import "FaceBookShareViewController.h"
#import "INCustomerLoginViewController.h"

@class INAllStoreObj;
@interface INCategoryViewController : UIViewController<UIActionSheetDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,FacebookProtocolDelegate,INCustomerLoginViewControllerDelegate>
{
    INAllStoreObj *storeObj;
    BOOL isDragging;
    BOOL isLoading;
    BOOL isAdding;
    sqlite3 *shoplocalDB;
    MBProgressHUD *hud;
}

@property (nonatomic, readwrite) int isStoreSearch;
@property (nonatomic, readwrite) int category_id;;

@property (strong, nonatomic) IBOutlet UIImageView *bgImageView;
@property (strong, nonatomic) IBOutlet UITableView *categoryTableView;

@property (strong, nonatomic) IBOutlet UIView *countHeaderView;
@property (strong, nonatomic) IBOutlet UILabel *lblpullToRefresh;
@property (strong, nonatomic) IBOutlet UILabel *countlbl;


@property (strong, nonatomic) NSMutableArray *catgArray;
@property (nonatomic, retain) UIView *refreshHeaderView;
@property (nonatomic, retain) UILabel *refreshLabel;
@property (nonatomic, retain) UIImageView *refreshArrow;
@property (nonatomic, retain) UIActivityIndicatorView *refreshSpinner;
@property (readwrite, nonatomic) NSInteger totalPage;
@property (readwrite, nonatomic) NSInteger totalRecord;
@property (readwrite, nonatomic) NSInteger currentPage;

//*****Pull TO Refresh
@property (nonatomic, copy) NSString *textPull;
@property (nonatomic, copy) NSString *textRelease;
@property (nonatomic, copy) NSString *textLoading;

@property (nonatomic, readwrite) NSInteger currentSelectedRow;

@property (strong, nonatomic) NSMutableArray *telArray;
@property (readwrite, nonatomic) NSInteger selectedPlaceId;
@property (copy, nonatomic) NSString* selectedPlaceName;


@property (strong, nonatomic) IBOutlet UIView *callModalViewBackView;
@property (strong, nonatomic) IBOutlet UIView *callModalView;
@property (strong, nonatomic) IBOutlet UILabel *lblcallModalHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblcallModalHeaderSeperator;
@property (strong, nonatomic) IBOutlet UITableView *callModalTableView;
@property (strong, nonatomic) IBOutlet UIButton *btnCancelcallModal;
- (IBAction)btnCancelcallModalPressed:(id)sender;

@end
