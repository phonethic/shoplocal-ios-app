//
//  INPopTableController.m
//  shoplocal
//
//  Created by Rishi on 03/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INPopTableController.h"
#import "constants.h"

@interface INPopTableController ()

@end

@implementation INPopTableController
@synthesize delegate=_delegate;


- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.title = ALERT_TITLE;
    self.tableView.allowsMultipleSelection = FALSE;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    switch (indexPath.row) {
        case 0:
        {
            cell.textLabel.text = @"Lokhandwala, Andheri West";
            cell.textLabel.numberOfLines = 2;
            cell.textLabel.font = DEFAULT_FONT(14.0);
            cell.textLabel.textColor = BROWN_COLOR;
        }
            break;
            
        case 1:
        {
            cell.textLabel.text = @"";

        }
            break;
            
        case 2:
        {
            cell.textLabel.text = @"";

        }
            break;
            
        case 3:
        {
            cell.textLabel.text = @"";
            
        }
            break;
            
        default:
            break;
    }
    if([cell.textLabel.text isEqualToString:[INUserDefaultOperations getSelectedStore]])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}


#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if([self.delegate respondsToSelector:@selector(selectedTableRow:)])
    {
        if(![cell.textLabel.text isEqualToString:@""])
            [self.delegate selectedTableRow:cell.textLabel.text];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
