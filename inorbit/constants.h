
#ifndef _ALERT_MESSAGES_H_
#define _ALERT_MESSAGES_H_
#import "Flurry.h"
#import "Reachability.h"

#define FACEBOOK_APPID @"563824743654209"
#define TEST_FACEBOOK_APPID @"283631075147170"

#define SYSTEM_VERSION_LESS_THAN(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define HOME 0
#define SPLASH_SCREEN 1
#define POPOVER 2

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define ALERT_TITLE @"Shoplocal"
#define ALERT_MESSAGE_NO_NETWORK @"No internet. Please connect to the internet and try again."

#define NETWORK_NOTIFICATION @"NetWorkchangeNotification"

#define SHOPLOCAL_APPSTORE_URL @"https://itunes.apple.com/us/app/shoplocal-india/id768745585?mt=8"
#define SHOPLOCAL_WEBSITE_URL @"http://shoplocal.co.in/"

#define SHOPLOCAL_FB_URL @"https://www.facebook.com/shoplocal.co.in"
#define SHOPLOCAL_TW_URL @"https://twitter.com/myshoplocal"
#define SHOPLOCAL_GPLUS_URL @"https://plus.google.com/108704719894873121741/posts"
#define SHOPLOCAL_PIN_URL @"http://www.pinterest.com/shoplocalindia/"

#define DEVICE_TYPE [NSString stringWithFormat:@"%@ %@", [[UIDevice currentDevice] model], [[UIDevice currentDevice] systemVersion]]

#define SHOPLOCAL_CONTACT @"+91-22-65160691"

#define CRITTERCISM_APPID @"5269206be432f53d32000001"

#define PARSE_APPID @"UD0PNYHMlbsxliXGBVn6IUJe9KTQ8mz93385tb8k"
#define PARSE_CLIENTKEY @"849ZNsheq3xTNHSw120qO1xDeWjWDz8CcdslMwn5"

#define DATABASE_VERSION 2

//#ifdef DEBUG
//    #define FLURRY_APPID @"JDM7MR3TXVRW8TCJRHR8" //TEST APP Flurry ID
//    #define LOCALYTICS_APPID @"fb919b6fb4f4b93d56ea935-7b7f7fd2-783f-11e3-94f2-005cf8cbabd8"  //Rishi APP LOCALYTICS ID
//#else
    #define FLURRY_APPID @"P42K47DMTJZGCR8SMVCP"
    #define LOCALYTICS_APPID @"1823a68bcf36a76b6a01c2c-42cfa378-743f-11e3-1898-004a77f8b47f"  // Live APP LOCALYTICS ID
//#endif

//#define CONNECT_STAGE
#ifdef CONNECT_STAGE
    #define LIVE_SERVER @"http://stage.phonethics.in/"
    #define URL_PREFIX @"shoplocal/"
    #define API_VERSION @"api-3/"
#else
    #define LIVE_SERVER @"http://shoplocal.co.in/"
    #define URL_PREFIX @""
    #define API_VERSION @"api-3/"
#endif

#define LOKHANDWALA @"Lokhandwala"
#define MENU_TITLE @"Menu"


#define HUD_TITLE @"Loading"
#define HUD_SUBTITLE @"Please wait ..."

#define DEFAULT_RED 30/255.0
#define DEFAULT_GREEN 93/255.0
#define DEFAULT_BLUE 125/255.0
//#define DEFAULT_COLOR [UIColor colorWithRed:0.443 green:0.356  blue:0.341 alpha:1]
#define DEFAULT_COLOR [UIColor colorWithRed:179/255.0 green:142/255.0  blue:136/255.0 alpha:1]

//#define DEFAULT_FONT(SIZE) [UIFont systemFontOfSize:SIZE]
//#define DEFAULT_BOLD_FONT(SIZE) [UIFont boldSystemFontOfSize:SIZE]

#define DEFAULT_FONT(SIZE) [UIFont fontWithName:@"CenturyGothic" size:SIZE]
#define DEFAULT_BOLD_FONT(SIZE) [UIFont fontWithName:@"CenturyGothic-Bold" size:SIZE]


#define TEXT_COLOR [UIColor colorWithRed:0.0 green:0.6  blue:0.8 alpha:1] 
#define DARK_BLUE_COLOR [UIColor colorWithRed:0.098 green:0.329  blue:0.572 alpha:1] 
#define LIGHT_BLUE [UIColor colorWithRed:0.435 green:0.639  blue:0.972 alpha:1] 
#define DARK_YELLOW_COLOR [UIColor colorWithRed:1.0 green:0.709  blue:0.376 alpha:1] 
#define LIGHT_YELLOW_COLOR [UIColor colorWithRed:0.996 green:0.749  blue:0.305 alpha:1] 
#define LIGHT_ORANGE_COLOR [UIColor colorWithRed:0.901 green:0.666  blue:0.274 alpha:1]
#define LIGHT_LINE_COLOR [UIColor colorWithRed:0.890 green:0.890  blue:0.890 alpha:1]

#define LIGHT_GREEN_COLOR [UIColor colorWithRed:186/255.0 green:204/255.0  blue:155/255.0 alpha:1]
#define DARK_GREEN_COLOR [UIColor colorWithRed:148/255.0 green:163/255.0  blue:125/255.0 alpha:1]

#define LIGHT_GREEN_COLOR_WITH_ALPHA(ALPHA) [UIColor colorWithRed:186/255.0 green:204/255.0  blue:155/255.0 alpha:ALPHA]

#define BUTTON_COLOR [UIColor colorWithRed:214/255.0 green:144/255.0  blue:137/255.0 alpha:1]
#define BUTTON_OFFWHITE_COLOR [UIColor colorWithRed:214/255.0 green:144/255.0  blue:137/255.0 alpha:0.5]

#define BUTTON_BORDER_COLOR [UIColor colorWithRed:204/255.0 green:129/255.0  blue:122/255.0 alpha:1]


#define BROWN_COLOR [UIColor colorWithRed:113/255.0 green:91/255.0  blue:87/255.0 alpha:1]
#define BROWN_OFFWHITE_COLOR [UIColor colorWithRed:113/255.0 green:91/255.0  blue:87/255.0 alpha:0.5]

#define ORANGE_COLOR  [UIColor colorWithRed:248/255.0 green:152/255.0 blue:56/255.0 alpha:1.0]

#define GREEN_COLOR_FOR_BUTTON [UIColor colorWithRed:45/255.0 green:150/255.0 blue:0 alpha:1.0]
#define GRAY_COLOR_FOR_BUTTON [UIColor colorWithRed:127/255.0 green:127/255.0 blue:127/255.0 alpha:1.0]

#define REFRESH_HEADER_HEIGHT 52.0f
#define REFRESH_LABEL_FONT [UIFont fontWithName:@"Cochin-Bold" size:16.0]

#define IN_STORE_DID_CHANGE_NOTIFICATION @"IN_STORE_DID_CHANGE_NOTIFICATION"

#define IN_APP_BACKGROUND_CHANGE_NOTIFICATION @"IN_APP_BACKGROUND_CHANGE_NOTIFICATION"

#define IN_CUSTOMER_LOGIN_LOGOUT_NOTIFICATION @"IN_CUSTOMER_LOGIN_LOGOUT_NOTIFICATION"

#define IN_MERCHANT_LOGIN_NOTIFICATION @"IN_MERCHANT_LOGIN_NOTIFICATION"

#define IN_MERCHANT_SHOW_OFFER_NOTIFICATION @"IN_MERCHANT_SHOW_OFFER_NOTIFICATION"

#define IN_MERCHANT_SHOW_BROADCAST_NOTIFICATION @"IN_MERCHANT_SHOW_BROADCAST_NOTIFICATION"

#define IN_MERCHANT_RELOAD_MYPLACES_NOTIFICATION @"IN_MERCHANT_RELOAD_MYPLACES_NOTIFICATION"


#define IN_CUSTOMER_LOGIN_LOGOUT_KEY @"IN_CUSTOMER_LOGIN_LOGOUT"
#define IN_MERCHANT_LOGIN_LOGOUT_KEY @"IN_MERCHANT_LOGIN_LOGOUT"

#define IN_MERCHANT_LOGIN_LOGOUT_NOTIFICATION @"IN_MERCHANT_LOGIN_LOGOUT_NOTIFICATION"

#define IN_CUSTOMER_CHANGE_LOCATION_NOTIFICATION @"IN_CUSTOMER_CHANGE_LOCATION_NOTIFICATION"
#define IN_CUSTOMER_CHANGE_LOCATION_KEY @"IN_CUSTOMER_CHANGE_LOCATION"

#define IN_CUSTOMER_CHANGE_TAB_MANUALLY_NOTIFICATION @"IN_CUSTOMER_CHANGE_TAB_MANUALLY_NOTIFICATION"
#define IN_CUSTOMER_CHANGE_TAB_MANUALLY_KEY @"IN_CUSTOMER_CHANGE_TAB_MANUALLY_KEY"

//======= Local Notifications ============================================================================
#define IN_MERCHANT_SHOW_LOCAL_NOTIFICATION @"IN_MERCHANT_SHOW_LOCAL_NOTIFICATION"
#define IN_MERCHANT_SHOW_LOCAL_NOTIFICATION_KEY @"IN_MERCHANT_SHOW_LOCAL_NOTIFICATION_KEY"

#define IN_CUSTOMER_SHOW_LOCAL_NOTIFICATION @"IN_CUSTOMER_SHOW_LOCAL_NOTIFICATION"
#define IN_CUSTOMER_SHOW_LOCAL_NOTIFICATION_KEY @"IN_CUSTOMER_SHOW_LOCAL_NOTIFICATION_KEY"

#define IN_CUSTOMER_SHOW_PROFILE_LOCAL_NOTIFICATION @"IN_CUSTOMER_SHOW_PROFILE_LOCAL_NOTIFICATION"
//======= End of Local Notifications ============================================================================

//======= Facebook Deeplinking Notifications ============================================================================
#define STOREDETAILS_FBDEEPLINKINGNTYPE @"STOREDETAILS_FBDEEPLINKINGNTYPE"
#define STORE_ID @"STORE_ID"
#define IN_CUSTOMER_SHOW_STORE_DETAILS_NOTIFICATION @"IN_CUSTOMER_SHOW_STORE_DETAILS_NOTIFICATION"
//======= Facebook Deeplinking Notifications ============================================================================

#define AREALIST_GETREQUEST_EXPIREDDATE @"arealist_expireddate"

#define SHARE_MSG_BODY(STORENAME,OFFER,DESCRIPTION) [NSString stringWithFormat:@"Store Name: %@\nOffer: %@\nDescription: %@",STORENAME,OFFER,DESCRIPTION]

#define LIKE_BROADCAST_OF_STORE [NSString stringWithFormat:@"%@%@broadcast_api/like",URL_PREFIX,API_VERSION]
#define UNLIKE_BROADCAST_OF_STORE(ID) [NSString stringWithFormat:@"%@%@broadcast_api/like?post_id=%d",URL_PREFIX,API_VERSION,ID]

#define SHARE_STORE_POST [NSString stringWithFormat:@"%@%@broadcast_api/share",URL_PREFIX,API_VERSION]

#define PRIVACY_LINK [NSString stringWithFormat:@"%@%@/info/privacy-policy",LIVE_SERVER,URL_PREFIX]
#define TERMS_AND_CONDITIONS_LINK [NSString stringWithFormat:@"%@%@/info/terms-conditions",LIVE_SERVER,URL_PREFIX]


#define LOGGED_BY @"loggedBy"
#define SHOPLOCALSERVER @"Shoplocal Server"
#define FACEBOOKSERVER @"Facebook"
#define LOGIN_USERID @"userId"
#define LOGIN_TOKEN @"accessToken"

#define DESCRIPTION_PLACEHOLDER_TEXT @"No description available."
#define PULLTOREFRESH_BARROW @"arrowbrown.png"
#define PULLTOREFRESH_WARROW @"arrowwhite.png"

#define NO_CONTACT_MESSAGE @"We don't have a contact number for this store."

//ERROR CODES
//====================== CUSTOMER =========================
#define CUSTOMER_INVALID_USER_AUTH_MESSAGE @"Invalid credential. Kindly Re-login"
#define MERCHANT_INVALID_USER_AUTH_MESSAGE @"Invalid credential. Kindly Re-login"

#define CUSTOMER_INVALID_USER_AUTH_CODE @"-221"
#define CUSTOMER_ALREADY_MADE_POST_FAV @"-411"

#define CUSTOMER_PASSWORD_SET_SUCCESS @"-207"
#define CUSTOMER_PASSWORD_ALREADY_SET @"-208"

#define CUSTOMER_NO_FAVSTORE_FOUND_FORPOST @"-240"
#define CUSTOMER_NO_FAVSTORE_FOUND @"-241"

#define CUSTOMER_NO_RECORDS_FOUND @"0"

//====================== MERCHANT =========================
#define MERCHANT_INVALID_USER_AUTH_CODE @"-116"

#define MERCHANT_PASSWORD_SET_SUCCESS @"-106"
#define MERCHANT_PASSWORD_ALREADY_SET @"-107"

#define MERCHANT_NO_RECORDS_FOUND @"0"


//====================== Urban Airship tags : KEYS =========================
#define PUSH_TAG_MERCHANT @"merchant"
#define PUSH_TAG_MERCHANT_MYPLACES @"merchant_myplaces"
#define PUSH_TAG_CUSTOMER_ACTIVE_AREA @"customer_active_area"
#define PUSH_TAG_CUSTOMER_ID @"customer_id"

#define PUSH_TAG_MERCHANT_MYPLACES_PREFIX @"own_"
#define PUSH_TAG_CUSTOMER_ACTIVE_AREA_PREFIX @"area_"
#define PUSH_TAG_CUSTOMER_ID_PREFIX @"customer_id_"
#endif
