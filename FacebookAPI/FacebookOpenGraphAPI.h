//
//  FacebookOpenGraphAPI.h
//  shoplocal
//
//  Created by Kirti Nikam on 07/05/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

#define OBJECT_TYPE_STORE @"store"
#define OBJECT_TYPE_POST @"offer"
#define OBJECT_TYPE_NONE @"none"

@interface FacebookOpenGraphAPI : NSObject
+(NSString *)createDeepLinkWithActiveAreaName:(NSString *)activeAreaName storeName:(NSString *)lstoreName storeId:(NSString *)lstoreId;

+(void)shareContentUsingOpenGraphAPIWithTitle:(NSString *)title description:(NSString *)ldescription image:(NSString *)image deeplinkingURL:(NSString *)url objectType:(NSString *)objectType;
+(void)likeContentUsingOpenGraphAPIWithTitle:(NSString *)title description:(NSString *)ldescription image:(NSString *)image deeplinkingURL:(NSString *)url objectType:(NSString *)objectType;
@end
