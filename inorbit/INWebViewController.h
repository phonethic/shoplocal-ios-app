//
//  INWebViewController.h
//  shoplocal
//
//  Created by Kirti Nikam on 03/10/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface INWebViewController : UIViewController <UIWebViewDelegate>
@property (copy,nonatomic) NSString *urlString;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@end
