//
//  INSearchViewController.h
//  shoplocal
//
//  Created by Rishi on 12/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import "FaceBookShareViewController.h"
#import "INCustomerLoginViewController.h"

@class INAllStoreObj;
@interface INSearchViewController : UIViewController <UIActionSheetDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,FacebookProtocolDelegate,INCustomerLoginViewControllerDelegate>
{
    INAllStoreObj *storeObj;
    MBProgressHUD *hud;
    BOOL isDragging;
    BOOL isLoading;
    BOOL isAdding;
    
    
    double keyboardHeight;
    BOOL isSearchOn;
    
     double lastContentOffset;
}
@property (readwrite, nonatomic) NSInteger hideBackBarBtn;

@property (readwrite, nonatomic) NSInteger showCancelBarBtn;
@property (nonatomic, readwrite) int isStoreSearch;

@property (nonatomic, copy) NSString *textToSearch;

@property (strong, nonatomic) IBOutlet UISearchBar *storeSearchBar;
@property (strong, nonatomic) IBOutlet UITableView *searchTableView;
@property (strong, nonatomic) NSMutableArray *searchArray;
@property (strong, nonatomic) NSMutableDictionary *searchdict;
@property (strong, nonatomic) IBOutlet UILabel *countlbl;

@property (readwrite, nonatomic) NSInteger totalPage;
@property (readwrite, nonatomic) NSInteger totalRecord;
@property (readwrite, nonatomic) NSInteger currentPage;
@property (readwrite, nonatomic) NSInteger searchType;

@property (nonatomic, retain) UIView *refreshHeaderView;
@property (nonatomic, retain) UILabel *refreshLabel;
@property (nonatomic, retain) UIImageView *refreshArrow;
@property (nonatomic, retain) UIActivityIndicatorView *refreshSpinner;


@property (strong, nonatomic) IBOutlet UIView *countHeaderView;
@property (strong, nonatomic) IBOutlet UILabel *lblpullToRefresh;


//*****Pull TO Refresh
@property (nonatomic, copy) NSString *textPull;
@property (nonatomic, copy) NSString *textRelease;
@property (nonatomic, copy) NSString *textLoading;

@property (nonatomic, readwrite) NSInteger currentSelectedRow;

@property (strong, nonatomic) IBOutlet UIView *searchView;
@property (strong, nonatomic) IBOutlet UITextField *searchTextField;
@property (strong, nonatomic) IBOutlet UIButton *searchCancelBtn;
- (IBAction)searchTextChanged:(id)sender;

- (IBAction)searchCancelBtnPressed:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *searchZoomOutBtn;
- (IBAction)searchZoomOutBtnPressed:(id)sender;


@property (strong, nonatomic) NSMutableArray *telArray;
@property (readwrite, nonatomic) NSInteger selectedPlaceId;
 @property (copy, nonatomic) NSString* selectedPlaceName;

@property (strong, nonatomic) IBOutlet UIView *callModalViewBackView;
@property (strong, nonatomic) IBOutlet UIView *callModalView;

@property (strong, nonatomic) IBOutlet UILabel *lblcallModalHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblcallModalHeaderSeperator;
@property (strong, nonatomic) IBOutlet UITableView *callModalTableView;
@property (strong, nonatomic) IBOutlet UIButton *btnCancelcallModal;
- (IBAction)btnCancelcallModalPressed:(id)sender;

@end
