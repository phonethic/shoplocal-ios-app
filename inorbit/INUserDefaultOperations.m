//
//  INUserDefaultOperations.m
//  shoplocal
//
//  Created by Rishi on 03/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INUserDefaultOperations.h"
#import "constants.h"
#import "SIAlertView.h"
#import "CommonCallback.h"

@implementation INUserDefaultOperations


+(void)setMerchantCountInArea:(NSDictionary *)areaDict{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:areaDict forKey:@"Merchant_Count_Dictionary"];
    [defaults synchronize];
}

+(NSString *)getMerchantCountInArea:(NSString *)areaName{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *areaDict  = [defaults objectForKey:@"Merchant_Count_Dictionary"];
    NSString *merchantCount = [areaDict objectForKey:areaName];
    if (merchantCount == nil) {
        return @"";
    }
    return merchantCount;
}


+(void)setStoreIds
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"Lokhandwala, Andheri West" forKey:LOKHANDWALA];
    [defaults synchronize];
}

+(NSString *)storeIdForKey:(NSString *)key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *value = [defaults objectForKey:key];
    return value;
}

+(void)setSelectedStore:(NSString *)value
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:value forKey:@"store"];
    [defaults synchronize];
}

+(NSString *)getSelectedStore
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *value = [defaults objectForKey:@"store"];
    return value;
}

////////////////////////////////////////////Merchant/////////////////////////////////////////////////////////////////
+(void)setMerchantDetails:(NSString *)idvalue pass:(NSString *)passvalue
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:idvalue forKey:@"Merchant_id"];
    [defaults setObject:passvalue forKey:@"Merchant_pass"];
    [defaults synchronize];
}


+(BOOL)isMerchantLoggedIn
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *mpass = [defaults objectForKey:@"Merchant_pass"];
    NSString *mauthid = [defaults objectForKey:@"Merchant_authid"];
    NSString *mauthcode = [defaults objectForKey:@"Merchant_auth_code"];
    
    DebugLog(@"%@    %@    %@",mpass,mauthid,mauthcode);
    if(mauthid == nil || mpass == nil || mauthcode ==nil || [mauthid isEqualToString:@""] || [mpass isEqualToString:@""] || [mauthcode isEqualToString:@""])
    {
        return FALSE;
    } else {
        return TRUE;
    }
}

+(void)setMerchantAuthDetails:(NSString *)idvalue pass:(NSString *)codevalue
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:idvalue forKey:@"Merchant_authid"];
    [defaults setObject:codevalue forKey:@"Merchant_auth_code"];
    [defaults synchronize];
}

+(NSString *)getMerchantId
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"Merchant_id"] == nil)
        return @"";
    return [defaults objectForKey:@"Merchant_id"];
}

+(NSString *)getMerchantAuthId
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"Merchant_authid"] == nil)
        return @"";
    return [defaults objectForKey:@"Merchant_authid"];
}

+(NSString *)getMerchantAuthCode
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"Merchant_auth_code"] == nil)
        return @"";
    return [defaults objectForKey:@"Merchant_auth_code"];
}

+(void)clearMerchantDetails
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"" forKey:@"Merchant_pass"];
    [defaults setObject:@"" forKey:@"Merchant_authid"];
    [defaults setObject:@"" forKey:@"Merchant_auth_code"];
    
    [defaults setObject:nil forKey:@"Merchant_Validate_Login_Date"];

    // clear local notification state
    [defaults setBool:FALSE forKey:@"EditStoreNotificationState"];
    [defaults setBool:FALSE forKey:@"TalkNowNotificationState"];
    [defaults synchronize];
    
    //When merchant logouts in then remove him from UAPush
    [CommonCallback removeTagsFromUrbanAriship:PUSH_TAG_MERCHANT];
    [CommonCallback removeTagsFromUrbanAriship:PUSH_TAG_MERCHANT_MYPLACES];
}

+(void)setMerchantCountryCode:(NSString *)value
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:value forKey:@"Merchant_country_code"];
    [defaults synchronize];
}

+(NSString *)getMerchantCountryCode
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"Merchant_country_code"] == nil)
        return @"";
    return [defaults objectForKey:@"Merchant_country_code"];
}
+(void)setMerchantValidateLoginDate:(NSDate *)value{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSDate date] forKey:@"Merchant_Validate_Login_Date"];
    [defaults synchronize];
}
+(NSDate *)getMerchantValidateLoginDate{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:@"Merchant_Validate_Login_Date"];
}
    
////////////////////////////////////////////Customer/////////////////////////////////////////////////////////////////
+(void)setCustomerDetails:(NSString *)loginFrom userid:(NSString *)idvalue pass:(NSString *)passvalue
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:loginFrom forKey:@"Customer_LoginFrom"];
    [defaults setObject:idvalue forKey:@"Customer_id"];
    [defaults setObject:passvalue forKey:@"Customer_pass"];
    [defaults synchronize];
}


+(void)setCustomerDetails:(NSString *)idvalue pass:(NSString *)passvalue
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:idvalue forKey:@"Customer_id"];
    [defaults setObject:passvalue forKey:@"Customer_pass"];
    [defaults synchronize];
}


+(BOOL)isCustomerLoggedIn
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *cpass = [defaults objectForKey:@"Customer_pass"];
    NSString *cauthid = [defaults objectForKey:@"Customer_authid"];
    NSString *cauthcode = [defaults objectForKey:@"Customer_auth_code"];
    DebugLog(@"%@    %@   %@",cpass,cauthid,cauthcode);
    if(cauthid == nil || cpass == nil || cauthcode ==nil || [cauthid isEqualToString:@""] || [cpass isEqualToString:@""] ||[cauthcode isEqualToString:@""])
    {
        return FALSE;
    } else {
        return TRUE;
    }
}

+(void)setCustomerAuthDetails:(NSString *)idvalue pass:(NSString *)codevalue
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:idvalue forKey:@"Customer_authid"];
    [defaults setObject:codevalue forKey:@"Customer_auth_code"];
    [defaults synchronize];
}

+(NSString *)getCustomerId
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"Customer_id"] == nil)
        return @"";
    return [defaults objectForKey:@"Customer_id"];
}

+(NSString *)getCustomerAuthId
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"Customer_authid"] == nil)
        return @"";
    return [defaults objectForKey:@"Customer_authid"];
}

+(NSString *)getCustomerAuthCode
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"Customer_auth_code"] == nil)
        return @"";
    return [defaults objectForKey:@"Customer_auth_code"];
}

+(void)clearCustomerDetails
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"" forKey:@"Customer_pass"];
    [defaults setObject:@"" forKey:@"Customer_authid"];
    [defaults setObject:@"" forKey:@"Customer_auth_code"];
    [defaults setDouble:0 forKey:@"Latitude"];
    [defaults setDouble:0 forKey:@"Longitude"];
    [defaults setDouble:0 forKey:@"Distance"];
    // clear local notification state
    [defaults setBool:FALSE forKey:@"CustomerSignInNotificationState"];
    [defaults setBool:FALSE forKey:@"CustomerSpecialDatesNotificationState"];
    [defaults setBool:FALSE forKey:@"CustomerFavouriteNotificationState"];
    [defaults synchronize];
    
    [CommonCallback removeTagsFromUrbanAriship:PUSH_TAG_CUSTOMER_ID];
}

+(void)setLatitude:(double)value{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setDouble:value forKey:@"Latitude"];
    [defaults synchronize];
}

+(double)getLatitude{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults doubleForKey:@"Latitude"];
}

+(void)setLongitude:(double)value{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setDouble:value forKey:@"Longitude"];
    [defaults synchronize];
}

+(double)getLongitude{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults doubleForKey:@"Longitude"];
}

+(void)setCustomerCountryCode:(NSString *)value
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:value forKey:@"Customer_country_code"];
    [defaults synchronize];
}

+(NSString *)getCustomerCountryCode
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"Customer_country_code"] == nil)
        return @"";
    return [defaults objectForKey:@"Customer_country_code"];
}




+(void)setWelcomeScreenState:(NSString *)value{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:value forKey:@"WelcomeScreen"];
    [defaults synchronize];
}

+(NSString *)getWelcomeScreenState{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"WelcomeScreen"] == nil)
        return @"";
    return [defaults objectForKey:@"WelcomeScreen"];
}

+(void)setTermsConditionsValue:(NSString *)value
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:value forKey:@"Terms"];
    [defaults synchronize];
}

+(NSString *)getTermsConditionsValue{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"Terms"] == nil)
        return @"0";
    return [defaults objectForKey:@"Terms"];
}

+(void)showAlert:(NSString*) message
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}

+(void)showOfflineAlert
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                        message:ALERT_MESSAGE_NO_NETWORK
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}

+(void)showSIAlertView:(NSString*)message{
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:ALERT_TITLE andMessage:message];
    alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
    alertView.backgroundStyle = SIAlertViewBackgroundStyleGradient;
    
    [alertView addButtonWithTitle:@"OK"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alert) {
                              DebugLog(@"OK Clicked");
                          }];
    [alertView show];
}

+(void)setCustomerDistance:(int)value{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:value forKey:@"Distance"];
    [defaults synchronize];
}

+(int)getCustomerDistance{
   // NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
   // if ([defaults integerForKey:@"Distance"] == 0) {
        return -1;
    //}
    //return [defaults integerForKey:@"Distance"];
}

+(void)setCustomerDistanceTemp:(int)value{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:value forKey:@"DistanceTemp"];
    [defaults synchronize];
}

+(int)getCustomerDistanceTemp{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults integerForKey:@"DistanceTemp"] == 0) {
        return -1;
    }
    return [defaults integerForKey:@"DistanceTemp"];
}

+(void)setAreaListGetRequestExipirationDate:(NSDate *)value{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:value forKey:AREALIST_GETREQUEST_EXPIREDDATE];
    [defaults synchronize];
}

+(NSDate *)getAreaListGetRequestExipirationDate{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:AREALIST_GETREQUEST_EXPIREDDATE];
}

+(void)setTourFinishedState:(BOOL)value{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:value forKey:@"TourFinishedState"];
    [defaults synchronize];
}

+(BOOL)getTourFinishedState{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"TourFinishedState"] == nil)
        return 0;
    return [defaults integerForKey:@"TourFinishedState"];
}

+(void)setRefreshState:(BOOL)value
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:value forKey:@"MyPlaceRefresh"];
    [defaults synchronize];
}


+(BOOL)getRefreshState
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"MyPlaceRefresh"] == nil)
        return 0;
    return [defaults integerForKey:@"MyPlaceRefresh"];
}


//==============================***=============***=============**=============***============================
+(NSString *)getCustomerLoginFrom{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"Customer_LoginFrom"] == nil)
        return @"";
    return [defaults objectForKey:@"Customer_LoginFrom"];
}

+(void)clearCustomerLoginState
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"" forKey:@"Customer_Registration_Text"];
    [defaults setInteger:0 forKey:@"Customer_Login_Visited_PageNumber"];
    [defaults synchronize];
}

+(void)clearMerchantLoginState
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"" forKey:@"Merchant_Registration_Text"];
    [defaults setInteger:0 forKey:@"Merchant_Login_Visited_PageNumber"];
    [defaults synchronize];
}

+(void)setCustomerLoginState:(NSString *)text visitedPage:(int)page{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:text forKey:@"Customer_Registration_Text"];
    [defaults setInteger:page forKey:@"Customer_Login_Visited_PageNumber"];
    [defaults synchronize];
}

+(void)setMerchantLoginState:(NSString *)text visitedPage:(int)page{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:text forKey:@"Merchant_Registration_Text"];
    [defaults setInteger:page forKey:@"Merchant_Login_Visited_PageNumber"];
    [defaults synchronize];
}

+(NSString *)getCustomerLoginStateWithText{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"Customer_Registration_Text"] == nil)
        return @"";
    return [defaults objectForKey:@"Customer_Registration_Text"];
}

+(int)getCustomerLoginStateWithVisitedPage{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults integerForKey:@"Customer_Login_Visited_PageNumber"];
}


+(NSString *)getMerchantLoginStateWithText{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"Merchant_Registration_Text"] == nil)
        return @"";
    return [defaults objectForKey:@"Merchant_Registration_Text"];
}

+(int)getMerchantLoginStateWithVisitedPage{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults integerForKey:@"Merchant_Login_Visited_PageNumber"];
}

+(void)setCustomerTermsConditionsValue:(NSString *)value{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:value forKey:@"Customer_Terms"];
    [defaults synchronize];

}
+(NSString *)getCustomerTermsConditionsValue{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"Customer_Terms"] == nil)
        return @"0";
    return [defaults objectForKey:@"Customer_Terms"];
}

+(void)setMerchantTermsConditionsValue:(NSString *)value{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:value forKey:@"Merchant_Terms"];
    [defaults synchronize];

}
+(NSString *)getMerchantTermsConditionsValue{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"Merchant_Terms"] == nil)
        return @"0";
    return [defaults objectForKey:@"Merchant_Terms"];
}

+(void)setCustomerLoginStateInTour:(int)value{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:value forKey:@"Customer_LoginState_In_Tour"];
    [defaults synchronize];

}

+(int)getCustomerLoginStateInTour{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults integerForKey:@"Customer_LoginState_In_Tour"];
}

+(void)setAppVersion:(NSString *)value
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:value forKey:@"Version"];
    [defaults synchronize];
}

+(NSString *)getAppVersion
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"Version"] == nil)
        return @"0.0";
    return [defaults objectForKey:@"Version"];
}

/*****NSDATE CATEGORIES******/
/////////////////////////////////////////////////////////////////////////////////////////////////
+(void)setOffersSyncDate {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSDate date] forKey:@"offersDate"];
    [defaults synchronize];
}


+(NSDate *)getOffersSyncDate
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"offersDate"] == nil)
        return [NSDate date];
    return [defaults objectForKey:@"offersDate"];
}

+(void)setAllStoreSyncDate {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSDate date] forKey:@"allStoreDate"];
    [defaults synchronize];
}

+(NSDate *)getAllStoreSyncDate
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"allStoreDate"] == nil)
        return [NSDate date];
    return [defaults objectForKey:@"allStoreDate"];
}


+(void)setFavStoreSyncDate
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSDate date] forKey:@"favStoreDate"];
    [defaults synchronize];
}

+(NSDate *)getFavStoreSyncDate
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"favStoreDate"] == nil)
        return [NSDate date];
    return [defaults objectForKey:@"favStoreDate"];
}


+(NSDate *)getSearchSyncDate
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"searchSyncDate"] == nil)
        return [NSDate date];
    return [defaults objectForKey:@"searchSyncDate"];
}

+(void)setSearchSyncDate
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSDate date] forKey:@"searchSyncDate"];
    [defaults synchronize];
}

+(NSDate *)getTermsSyncDate
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"termsSyncDate"] == nil)
        return [NSDate date];
    return [defaults objectForKey:@"termsSyncDate"];
}

+(void)setTermsSyncDate
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSDate date] forKey:@"termsSyncDate"];
    [defaults synchronize];
}

+(NSDate *)getPrivacySyncDate
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"privacySyncDate"] == nil)
        return [NSDate date];
    return [defaults objectForKey:@"privacySyncDate"];
}

+(void)setPrivacySyncDate
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSDate date] forKey:@"privacySyncDate"];
    [defaults synchronize];
}

+(void)setDailySyncDate
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSDate date] forKey:@"dailyDate"];
    [defaults synchronize];
}

+(NSDate *)getDailySyncDate
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"dailyDate"] == nil)
        return [NSDate date];
    return [defaults objectForKey:@"dailyDate"];
}

+(void)setMonthlySyncDate
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSDate date] forKey:@"monthlyDate"];
    [defaults synchronize];
}

+(NSDate *)getMonthlySyncDate
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"monthlyDate"] == nil)
        return [NSDate date];
    return [defaults objectForKey:@"monthlyDate"];
}

//----------------------------------------------------------------------------------
+(void)setAppStartDate
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSDate date] forKey:@"aapStartDate"];
    [defaults synchronize];
}

+(NSDate *)getAppStartDate
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"aapStartDate"] == nil)
        return [NSDate date];
    return [defaults objectForKey:@"aapStartDate"];
}

+(void)setAppEndDate
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSDate date] forKey:@"aapEndDate"];
    [defaults synchronize];
}

+(NSDate *)getAppEndDate
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"aapEndDate"] == nil)
        return [NSDate date];
    return [defaults objectForKey:@"aapEndDate"];
}


+(void)setAppTotalUsageTime:(NSInteger)totalTime
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:totalTime forKey:@"totalAppUpTime"];
    [defaults synchronize];
}

+(NSInteger)getAppTotalUsageTime
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    DebugLog(@"%ld",(long)[defaults integerForKey:@"totalAppUpTime"]);
    return [defaults integerForKey:@"totalAppUpTime"];
}

+(void)setInfoScreenCount:(NSInteger)totalCount
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:totalCount forKey:@"InfoScreenCount"];
    [defaults synchronize];
}

+(NSInteger)getInfoScreenCount
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    DebugLog(@"%ld",(long)[defaults integerForKey:@"InfoScreenCount"]);
    return [defaults integerForKey:@"InfoScreenCount"];
}

//----------------------------------------------------------------------------------

+(NSInteger)getDateDifferenceInSeconds:(NSDate *)startDate endDate:(NSDate *)endDate
{
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:kCFCalendarUnitSecond
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    DebugLog(@"seconds = %ld",(long)[components second]);
    return [components second];
}

+(NSInteger)getDateDifferenceInMinutes:(NSDate *)startDate endDate:(NSDate *)endDate
{
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:kCFCalendarUnitMinute
                                                        fromDate:endDate
                                                          toDate:startDate
                                                         options:0];
    DebugLog(@"minutes = %d",[components minute]);
    return [components minute];
}

+(NSInteger)getDateDifferenceInHours:(NSDate *)expireDate
{
    NSDate *currentDate = [NSDate date];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:kCFCalendarUnitHour
                                                        fromDate:expireDate
                                                          toDate:currentDate
                                                         options:0];
    //DebugLog(@"hours = %d",[components hour]);
    return [components hour];
}

+(NSInteger)getDateDifferenceInDays:(NSDate *)expireDate
{
    NSDate *currentDate = [NSDate date];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:kCFCalendarUnitDay
                                                        fromDate:expireDate
                                                          toDate:currentDate
                                                         options:0];
    //DebugLog(@"days = %d",[components day]);
    return [components day];
}

+(NSInteger)getAgeInYears:(NSDate *)birthDate
{
    NSDate *currentDate = [NSDate date];
    
    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                       components:NSYearCalendarUnit
                                       fromDate:birthDate
                                       toDate:currentDate
                                       options:0];
    DebugLog(@"years = %d",[ageComponents year]);
    return [ageComponents year];
}
  /*****************************************************************************/

+(void)setTalkNowDate
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSDate date] forKey:@"TalkNow"];
    [defaults synchronize];
}

+(NSDate *)getTalkNowDate
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"TalkNow"] == nil)
        return [NSDate date];
    return [defaults objectForKey:@"TalkNow"];
}

+(void)setEditStoreState:(BOOL)lval
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:lval forKey:@"EditStore"];
    [defaults synchronize];
}

+(BOOL)getEditStoreState
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    DebugLog(@"%d",[defaults boolForKey:@"EditStore"]);
    return [defaults boolForKey:@"EditStore"];
}

+(void)setCustomerSignInDate
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSDate date] forKey:@"CustomerSignIn"];
    [defaults synchronize];
}

+(NSDate *)getCustomerSignInDate
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"CustomerSignIn"] == nil)
        return [NSDate date];
    return [defaults objectForKey:@"CustomerSignIn"];
}

+(void)setCustomerFavouriteState:(BOOL)lval
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:lval forKey:@"CustomerFavourite"];
    [defaults synchronize];
}

+(BOOL)getCustomerFavouriteState
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    DebugLog(@"%d",[defaults boolForKey:@"CustomerFavourite"]);
    return [defaults boolForKey:@"CustomerFavourite"];
}

+(void)setCustomerFavouriteMyShoplocalRefresh:(BOOL)lval
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:lval forKey:@"CustomerMyShoplocalRefresh"];
    [defaults synchronize];
}

+(BOOL)getCustomerMyShoplocalRefresh
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults boolForKey:@"CustomerMyShoplocalRefresh"];
}


+(void)setCustomerSpecialDatesState:(BOOL)lval
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:lval forKey:@"CustomerSpecialDates"];
    [defaults synchronize];
}

+(BOOL)getCustomerSpecialDatesState
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults boolForKey:@"CustomerSpecialDates"];
}

+(void)setAppNotLaunchedDate
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSDate date] forKey:@"AppNotLaunched"];
    [defaults synchronize];
}

+(NSDate *)getAppNotLaunchedDate
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"AppNotLaunched"] == nil)
        return [NSDate date];
    return [defaults objectForKey:@"AppNotLaunched"];
}

+(void)setAppRaiterState:(BOOL)lval
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:lval forKey:@"AppRate"];
    [defaults synchronize];
}

+(BOOL)getAppRaiterState
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    DebugLog(@"%d",[defaults boolForKey:@"AppRate"]);
    return [defaults boolForKey:@"AppRate"];
}

/////////////////////////////////////////////////////////////////////////////////////////////////
+(void)setAppNotLaunchedNotificationState:(BOOL)lval{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:lval forKey:@"AppNotLaunchedNotificationState"];
    [defaults synchronize];
}
+(BOOL)isAppNotLaunchedNotificationAlreadySet{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    DebugLog(@"%d",[defaults boolForKey:@"AppNotLaunchedNotificationState"]);
    return [defaults boolForKey:@"AppNotLaunchedNotificationState"];
}

+(void)setTalkNowNotificationState:(BOOL)lval{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:lval forKey:@"TalkNowNotificationState"];
    [defaults synchronize];
}

+(BOOL)isTalkNowNotificationAlreadySet{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    DebugLog(@"%d",[defaults boolForKey:@"TalkNowNotificationState"]);
    return [defaults boolForKey:@"TalkNowNotificationState"];
}

+(void)setEditStoreNotificationState:(BOOL)lval{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:lval forKey:@"EditStoreNotificationState"];
    [defaults synchronize];
}

+(BOOL)isEditStoreNotificationAlreadySet{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    DebugLog(@"%d",[defaults boolForKey:@"EditStoreNotificationState"]);
    return [defaults boolForKey:@"EditStoreNotificationState"];
}

+(void)setCustomerSignInNotificationState:(BOOL)lval{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:lval forKey:@"CustomerSignInNotificationState"];
    [defaults synchronize];
}

+(BOOL)isCustomerSignInNotificationAlreadySet{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    DebugLog(@"%d",[defaults boolForKey:@"CustomerSignInNotificationState"]);
    return [defaults boolForKey:@"CustomerSignInNotificationState"];
}

+(void)setCustomerSpecialDatesNotificationState:(BOOL)lval{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:lval forKey:@"CustomerSpecialDatesNotificationState"];
    [defaults synchronize];
}

+(BOOL)isCustomerSpecialDatesNotificationAlreadySet{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    DebugLog(@"%d",[defaults boolForKey:@"CustomerSpecialDatesNotificationState"]);
    return [defaults boolForKey:@"CustomerSpecialDatesNotificationState"];
}

+(void)setCustomerFavouriteNotificationState:(BOOL)lval{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:lval forKey:@"CustomerFavouriteNotificationState"];
    [defaults synchronize];
}
+(BOOL)isCustomerFavouriteNotificationAlreadySet{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    DebugLog(@"%d",[defaults boolForKey:@"CustomerFavouriteNotificationState"]);
    return [defaults boolForKey:@"CustomerFavouriteNotificationState"];
}


/////////////////////////////////////////////////////////////////////////////////////////////////
+(void)setTalkNowNotification
{
    DebugLog(@"====setTalkNowNotification====");
    NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
    for (int i = 0 ; i < [notificationArray count]; i++) {
        UILocalNotification *localNotification = [notificationArray objectAtIndex:i];
        DebugLog(@"Recieved Notification %@",localNotification);
        if([[localNotification.userInfo objectForKey:@"Type"] isEqualToString:TALKNOW_LNTYPE]) {
            DebugLog(@"the notification which is canceld is %@", [localNotification.userInfo objectForKey:@"Type"]);
            [[UIApplication sharedApplication] cancelLocalNotification:localNotification] ; 
        }
    }
    // get last offer posted date
    // diff with current date
    // if diff > 7, fire next alram on now()+3days
    // if diff < 7, fire last posted date + 7days
    
//    if([INUserDefaultOperations getDateDifferenceInDays:[INUserDefaultOperations getTalkNowDate]] > 7)
//    {
        NSDateComponents *components = [[NSDateComponents alloc] init];
        [components setDay:3];
        NSDate *alertTime = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:[NSDate date] options:0];
        UIApplication* app = [UIApplication sharedApplication];
        UILocalNotification* notifyAlarm = [[UILocalNotification alloc] init];
        if (notifyAlarm)
        {
            notifyAlarm.fireDate = alertTime;
            notifyAlarm.timeZone = [NSTimeZone defaultTimeZone];
            notifyAlarm.repeatInterval = 0;//kCFCalendarUnitWeek;
            notifyAlarm.hasAction = YES;
            notifyAlarm.alertAction = @"Shop Now";
            notifyAlarm.soundName = UILocalNotificationDefaultSoundName; 
            notifyAlarm.alertBody = @"Local customers are looking for your business. Make an Offer or send a message to them on Shoplocal";
            notifyAlarm.userInfo = [NSDictionary dictionaryWithObject:@"Talk_Now" forKey:@"Type"];
            [app scheduleLocalNotification:notifyAlarm];
        }
//    } else {
//        NSDateComponents *components = [[NSDateComponents alloc] init];
//        [components setDay:7];
//        NSDate *alertTime = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:[INUserDefaultOperations getTalkNowDate] options:0];
//        UIApplication* app = [UIApplication sharedApplication];
//        UILocalNotification* notifyAlarm = [[UILocalNotification alloc] init];
//        if (notifyAlarm)
//        {
//            notifyAlarm.fireDate = alertTime;
//            notifyAlarm.timeZone = [NSTimeZone defaultTimeZone];
//            notifyAlarm.repeatInterval = kCFCalendarUnitWeek;
//            notifyAlarm.hasAction = YES;
//            notifyAlarm.alertAction = @"Shop Now";
//            notifyAlarm.soundName = UILocalNotificationDefaultSoundName;
//            notifyAlarm.alertBody = @"Local customers are looking for your business. Make an Offer or send a message to them on Shoplocal";
//            notifyAlarm.userInfo = [NSDictionary dictionaryWithObject:@"Talk_Now" forKey:@"Type"];
//            [app scheduleLocalNotification:notifyAlarm];
//        }
//    }
    NSArray *notificationArray1 = [[UIApplication sharedApplication] scheduledLocalNotifications];
    for (int i = 0 ; i < [notificationArray1 count]; i++) {
        UILocalNotification *localNotification = [notificationArray1 objectAtIndex:i];
        if([[localNotification.userInfo objectForKey:@"Type"] isEqualToString:TALKNOW_LNTYPE]) {
            DebugLog(@"Notification %@",localNotification);
            [INUserDefaultOperations setTalkNowNotificationState:TRUE];
        }
    }
}


+(void)setEditStoreNotification
{
    DebugLog(@"====setEditStoreNotification====");
    if([INUserDefaultOperations getEditStoreState] == TRUE) {
        NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
        for (int i = 0 ; i < [notificationArray count]; i++) {
            UILocalNotification *localNotification = [notificationArray objectAtIndex:i];
            DebugLog(@"Recieved Notification %@",localNotification);
            if([[localNotification.userInfo objectForKey:@"Type"] isEqualToString:EDITSTORE_LNTYPE]) {
                DebugLog(@"the notification which is canceld is %@", [localNotification.userInfo objectForKey:@"Type"]);
                [[UIApplication sharedApplication] cancelLocalNotification:localNotification] ;
            }
        }
        
        NSDateComponents *components = [[NSDateComponents alloc] init];
//        [components setDay:2];
        [components setDay:7];
        NSDate *alertTime = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:[NSDate date] options:0];
        UIApplication* app = [UIApplication sharedApplication];
        UILocalNotification* notifyAlarm = [[UILocalNotification alloc] init];
        if (notifyAlarm)
        {
            notifyAlarm.fireDate = alertTime;
            notifyAlarm.timeZone = [NSTimeZone defaultTimeZone];
            notifyAlarm.repeatInterval = 0;//kCFCalendarUnitWeek;
            notifyAlarm.hasAction = YES;
            notifyAlarm.alertAction = @"Shop Now";
            notifyAlarm.soundName = UILocalNotificationDefaultSoundName;
            notifyAlarm.alertBody = @"Fill up more info about your business on Shoplocal. Local customers are looking for your business.";
            notifyAlarm.userInfo = [NSDictionary dictionaryWithObject:EDITSTORE_LNTYPE forKey:@"Type"];
            [app scheduleLocalNotification:notifyAlarm];
        }
        NSArray *notificationArray1 = [[UIApplication sharedApplication] scheduledLocalNotifications];
        for (int i = 0 ; i < [notificationArray1 count]; i++) {
            UILocalNotification *localNotification = [notificationArray1 objectAtIndex:i];
            if([[localNotification.userInfo objectForKey:@"Type"] isEqualToString:EDITSTORE_LNTYPE]) {
                DebugLog(@"Notification %@",localNotification);
                [INUserDefaultOperations setEditStoreNotificationState:TRUE];
            }
        }
    }
}

+(void)setCustomerSignInNotification
{
    DebugLog(@"====setCustomerSignInNotification====");

    NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
    for (int i = 0 ; i < [notificationArray count]; i++) {
        UILocalNotification *localNotification = [notificationArray objectAtIndex:i];
        DebugLog(@"Recieved Notification %@",localNotification);
        if([[localNotification.userInfo objectForKey:@"Type"] isEqualToString:CUSTOMERSIGN_LNTYPE]) {
            DebugLog(@"the notification which is canceld is %@", [localNotification.userInfo objectForKey:@"Type"]);
            [[UIApplication sharedApplication] cancelLocalNotification:localNotification] ;
        }
    }
    NSDateComponents *components = [[NSDateComponents alloc] init];
//    [components setDay:3];
    [components setDay:7];
    NSDate *alertTime = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:[NSDate date] options:0];
    UIApplication* app = [UIApplication sharedApplication];
    UILocalNotification* notifyAlarm = [[UILocalNotification alloc] init];
    if (notifyAlarm)
    {
        notifyAlarm.fireDate = alertTime;
        notifyAlarm.timeZone = [NSTimeZone defaultTimeZone];
        notifyAlarm.repeatInterval = 0;//kCFCalendarUnitWeek;
        notifyAlarm.hasAction = YES;
        notifyAlarm.alertAction = @"Shop Now";
        notifyAlarm.soundName = UILocalNotificationDefaultSoundName;
        notifyAlarm.alertBody = @"Sign in to Shoplocal and receive alerts when your favourite stores have a special offer for you.";
        notifyAlarm.userInfo = [NSDictionary dictionaryWithObject:CUSTOMERSIGN_LNTYPE forKey:@"Type"];
        [app scheduleLocalNotification:notifyAlarm];
    }
    NSArray *notificationArray1 = [[UIApplication sharedApplication] scheduledLocalNotifications];
    for (int i = 0 ; i < [notificationArray1 count]; i++) {
        UILocalNotification *localNotification = [notificationArray1 objectAtIndex:i];
        if([[localNotification.userInfo objectForKey:@"Type"] isEqualToString:CUSTOMERSIGN_LNTYPE]) {
            DebugLog(@"Notification %@",localNotification);
            [INUserDefaultOperations setCustomerSignInNotificationState:TRUE];
        }
    }
}

+(void)setCustomerFavouriteNotification
{
    DebugLog(@"====setCustomerFavouriteNotification====");

    if([INUserDefaultOperations getCustomerFavouriteState] == FALSE) //Has not marked any store favourite
    {
        NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
        for (int i = 0 ; i < [notificationArray count]; i++) {
            UILocalNotification *localNotification = [notificationArray objectAtIndex:i];
            DebugLog(@"Recieved Notification %@",localNotification);
            if([[localNotification.userInfo objectForKey:@"Type"] isEqualToString:CUSTOMERFAVOURITESTORE_LNTYPE]) {
                DebugLog(@"the notification which is canceld is %@", [localNotification.userInfo objectForKey:@"Type"]);
                [[UIApplication sharedApplication] cancelLocalNotification:localNotification] ;
            }
        }
        NSDateComponents *components = [[NSDateComponents alloc] init];
        [components setDay:3];
        NSDate *alertTime = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:[NSDate date] options:0];
        UIApplication* app = [UIApplication sharedApplication];
        UILocalNotification* notifyAlarm = [[UILocalNotification alloc] init];
        if (notifyAlarm)
        {
            notifyAlarm.fireDate = alertTime;
            notifyAlarm.timeZone = [NSTimeZone defaultTimeZone];
            notifyAlarm.repeatInterval = 0;// kCFCalendarUnitWeek;
            notifyAlarm.hasAction = YES;
            notifyAlarm.alertAction = @"Shop Now";
            notifyAlarm.soundName = UILocalNotificationDefaultSoundName;
            notifyAlarm.alertBody = @"Add local stores to your favourites on Shoplocal and receive alerts when they have a special offer for you.";
            notifyAlarm.userInfo = [NSDictionary dictionaryWithObject:CUSTOMERFAVOURITESTORE_LNTYPE forKey:@"Type"];
            [app scheduleLocalNotification:notifyAlarm];
        }
        
        NSArray *notificationArray1 = [[UIApplication sharedApplication] scheduledLocalNotifications];
        for (int i = 0 ; i < [notificationArray1 count]; i++) {
            UILocalNotification *localNotification = [notificationArray1 objectAtIndex:i];
            if([[localNotification.userInfo objectForKey:@"Type"] isEqualToString:CUSTOMERFAVOURITESTORE_LNTYPE]) {
                DebugLog(@"Notification %@",localNotification);
                [INUserDefaultOperations setCustomerFavouriteNotificationState:TRUE];
            }
        }
    }
}

+(void)setCustomerSpecialDatesNotification
{
    DebugLog(@"====setCustomerSpecialDatesNotification====");
    // Check if customer profile is not filled based on NSUserDefaultls which is set to False on login and set to true when user updates his profile
    if([INUserDefaultOperations getCustomerSpecialDatesState] == TRUE) //He has Not set any special date
    {
        NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
        for (int i = 0 ; i < [notificationArray count]; i++) {
            UILocalNotification *localNotification = [notificationArray objectAtIndex:i];
            DebugLog(@"Recieved Notification %@",localNotification);
            if([[localNotification.userInfo objectForKey:@"Type"] isEqualToString:CUSTOMERSPECIALDATE_LNTYPE]) {
                DebugLog(@"the notification which is canceld is %@", [localNotification.userInfo objectForKey:@"Type"]);
                [[UIApplication sharedApplication] cancelLocalNotification:localNotification] ;
            }
        }
        NSDateComponents *components = [[NSDateComponents alloc] init];
//        [components setDay:3];
        [components setDay:7];
        NSDate *alertTime = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:[NSDate date] options:0];
        UIApplication* app = [UIApplication sharedApplication];
        UILocalNotification* notifyAlarm = [[UILocalNotification alloc] init];
        if (notifyAlarm)
        {
            notifyAlarm.fireDate = alertTime;
            notifyAlarm.timeZone = [NSTimeZone defaultTimeZone];
            notifyAlarm.repeatInterval = 0;//kCFCalendarUnitWeek;
            notifyAlarm.hasAction = YES;
            notifyAlarm.alertAction = @"Shop Now";
            notifyAlarm.soundName = UILocalNotificationDefaultSoundName;
            notifyAlarm.alertBody = @"Complete your profile on Shoplocal and get special offers on Birthdays & Anniversaries.";
            notifyAlarm.userInfo = [NSDictionary dictionaryWithObject:CUSTOMERSPECIALDATE_LNTYPE forKey:@"Type"];
            [app scheduleLocalNotification:notifyAlarm];
        }
        
        NSArray *notificationArray1 = [[UIApplication sharedApplication] scheduledLocalNotifications];
        for (int i = 0 ; i < [notificationArray1 count]; i++) {
            UILocalNotification *localNotification = [notificationArray1 objectAtIndex:i];
            if([[localNotification.userInfo objectForKey:@"Type"] isEqualToString:CUSTOMERSPECIALDATE_LNTYPE]) {
                DebugLog(@"Notification %@",localNotification);
                [INUserDefaultOperations setCustomerSpecialDatesNotificationState:TRUE];
            }
        }
    }
}

+(void)setAppNotLaunchedNotification
{
    DebugLog(@"====setAppNotLaunchedNotification====");
    NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
    for (int i = 0 ; i < [notificationArray count]; i++) {
        UILocalNotification *localNotification = [notificationArray objectAtIndex:i];
        DebugLog(@"Recieved Notification %@",localNotification);
        if([[localNotification.userInfo objectForKey:@"Type"] isEqualToString:APP_NOT_LAUNCHED_LNTYPE]) {
            
            DebugLog(@"the notification which is canceld is %@", [localNotification.userInfo objectForKey:@"Type"]);
            [[UIApplication sharedApplication] cancelLocalNotification:localNotification] ;
            
            //Reschedule
            if ([INUserDefaultOperations isAppNotLaunchedNotificationAlreadySet]) {
                NSDateComponents *components = [[NSDateComponents alloc] init];
                [components setDay:4];
                NSDate *alertTime = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:[NSDate date] options:0];
                UIApplication* app = [UIApplication sharedApplication];
                UILocalNotification* notifyAlarm = [[UILocalNotification alloc] init];
                if (notifyAlarm)
                {
                    notifyAlarm.fireDate = alertTime;
                    notifyAlarm.timeZone = [NSTimeZone defaultTimeZone];
                    notifyAlarm.repeatInterval = 0;
                    notifyAlarm.hasAction = YES;
                    notifyAlarm.alertAction = @"Shop Now";
                    notifyAlarm.soundName = UILocalNotificationDefaultSoundName;
                    notifyAlarm.alertBody = @"Shoplocal has lots of offers from local merchants around you, come get them.";
                    notifyAlarm.userInfo = [NSDictionary dictionaryWithObject:APP_NOT_LAUNCHED_LNTYPE forKey:@"Type"];
                    [app scheduleLocalNotification:notifyAlarm];
                    
                    return;
                }
            }
        }
    }

    NSDateComponents *components = [[NSDateComponents alloc] init];
//    [components setDay:3];
    [components setDay:4];
    NSDate *alertTime = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:[NSDate date] options:0];
    UIApplication* app = [UIApplication sharedApplication];
    UILocalNotification* notifyAlarm = [[UILocalNotification alloc] init];
    if (notifyAlarm)
    {
        notifyAlarm.fireDate = alertTime;
        notifyAlarm.timeZone = [NSTimeZone defaultTimeZone];
        notifyAlarm.repeatInterval = 0;
        notifyAlarm.hasAction = YES;
        notifyAlarm.alertAction = @"Shop Now";
        notifyAlarm.soundName = UILocalNotificationDefaultSoundName;
        notifyAlarm.alertBody = @"Shoplocal has lots of offers from local merchants around you, come get them.";
        notifyAlarm.userInfo = [NSDictionary dictionaryWithObject:@"App_NotLaunched" forKey:@"Type"];
        [app scheduleLocalNotification:notifyAlarm];
    }
    
    NSArray *notificationArray1 = [[UIApplication sharedApplication] scheduledLocalNotifications];
    for (int i = 0 ; i < [notificationArray1 count]; i++) {
        UILocalNotification *localNotification = [notificationArray1 objectAtIndex:i];
        if([[localNotification.userInfo objectForKey:@"Type"] isEqualToString:APP_NOT_LAUNCHED_LNTYPE]) {
            DebugLog(@"Notification %@",localNotification);
            [INUserDefaultOperations setAppNotLaunchedNotificationState:TRUE];
        }
    }
}


+(void)setWeekendMerchantOffersNotification
{
    NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
    for (int i = 0 ; i < [notificationArray count]; i++) {
        UILocalNotification *localNotification = [notificationArray objectAtIndex:i];
        DebugLog(@"Recieved Notification %@",localNotification);
        if([[localNotification.userInfo objectForKey:@"Type"] isEqualToString:@"Merchant_Offers_Friday"]) {
            DebugLog(@"the notification which is canceld is %@", [localNotification.userInfo objectForKey:@"Type"]);
            [[UIApplication sharedApplication] cancelLocalNotification:localNotification] ;
        }
    }
    
    NSDate *today = [[NSDate alloc] init];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *weekdayComponents = [gregorian components:NSWeekdayCalendarUnit fromDate:today];
    NSDateComponents *componentsToAdd = [[NSDateComponents alloc] init];
    [componentsToAdd setDay: ABS(6 - ([weekdayComponents weekday] % 7))];
    DebugLog(@"%d",ABS(6 - ([weekdayComponents weekday] % 7)));
    
    NSDate *friday = [gregorian dateByAddingComponents:componentsToAdd toDate:today options:0];
    NSDateComponents *components =
    [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit)
                 fromDate: friday];
    [components setHour:16];  /// At 4 in evening
    friday = [gregorian dateFromComponents:components];
    
    UIApplication* app = [UIApplication sharedApplication];
    UILocalNotification* notifyAlarm = [[UILocalNotification alloc] init];
    if (notifyAlarm)
    {
        notifyAlarm.fireDate = friday;
        notifyAlarm.timeZone = [NSTimeZone defaultTimeZone];
        notifyAlarm.repeatInterval = kCFCalendarUnitWeek;
        notifyAlarm.hasAction = YES;
        notifyAlarm.alertAction = @"Shop Now";
        notifyAlarm.soundName = UILocalNotificationDefaultSoundName;
        notifyAlarm.alertBody = @"Make an Offer to get Weekend shoppers.";
        notifyAlarm.userInfo = [NSDictionary dictionaryWithObject:@"Merchant_Offers_Friday" forKey:@"Type"];
        [app scheduleLocalNotification:notifyAlarm];
    }
    
    NSArray *notificationArray1 = [[UIApplication sharedApplication] scheduledLocalNotifications];
    for (int i = 0 ; i < [notificationArray1 count]; i++) {
        UILocalNotification *localNotification = [notificationArray1 objectAtIndex:i];
        if([[localNotification.userInfo objectForKey:@"Type"] isEqualToString:@"Merchant_Offers_Friday"]) {
            DebugLog(@"Notification %@",localNotification);
        }
    }
}

+(void)setWeekendCustomerOffersNotification
{
    NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
    for (int i = 0 ; i < [notificationArray count]; i++) {
        UILocalNotification *localNotification = [notificationArray objectAtIndex:i];
        DebugLog(@"Recieved Notification %@",localNotification);
        if([[localNotification.userInfo objectForKey:@"Type"] isEqualToString:@"Customer_Offers_Friday"]) {
            DebugLog(@"the notification which is canceld is %@", [localNotification.userInfo objectForKey:@"Type"]);
            [[UIApplication sharedApplication] cancelLocalNotification:localNotification] ;
        }
    }
    
    NSDate *today = [[NSDate alloc] init];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *weekdayComponents = [gregorian components:NSWeekdayCalendarUnit fromDate:today];
    NSDateComponents *componentsToAdd = [[NSDateComponents alloc] init];
    [componentsToAdd setDay: ABS(6 - ([weekdayComponents weekday] % 7))];
    DebugLog(@"%d",ABS(6 - ([weekdayComponents weekday] % 7)));
    
    NSDate *friday = [gregorian dateByAddingComponents:componentsToAdd toDate:today options:0];
    NSDateComponents *components =
    [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit)
                 fromDate: friday];
    [components setHour:16];  /// At 6 in evening
    friday = [gregorian dateFromComponents:components];
    
    UIApplication* app = [UIApplication sharedApplication];
    UILocalNotification* notifyAlarm = [[UILocalNotification alloc] init];
    if (notifyAlarm)
    {
        notifyAlarm.fireDate = friday;
        notifyAlarm.timeZone = [NSTimeZone defaultTimeZone];
        notifyAlarm.repeatInterval = kCFCalendarUnitWeek;
        notifyAlarm.hasAction = YES;
        notifyAlarm.alertAction = @"Shop Now";
        notifyAlarm.soundName = UILocalNotificationDefaultSoundName;
        notifyAlarm.alertBody = @"Have you seen the latest local offers on shoplocal ?";
        notifyAlarm.userInfo = [NSDictionary dictionaryWithObject:@"Customer_Offers_Friday" forKey:@"Type"];
        [app scheduleLocalNotification:notifyAlarm];
    }
}

+(void)cancelAppNotLaunchedNotification
{
    NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
    for (int i = 0 ; i < [notificationArray count]; i++) {
        UILocalNotification *localNotification = [notificationArray objectAtIndex:i];
        DebugLog(@"Recieved Notification %@",localNotification);
        if([[localNotification.userInfo objectForKey:@"Type"] isEqualToString:APP_NOT_LAUNCHED_LNTYPE]) {
            DebugLog(@"the notification which is canceld is %@", [localNotification.userInfo objectForKey:@"Type"]);
            [[UIApplication sharedApplication] cancelLocalNotification:localNotification] ;
        }
    }
}

+(void)cancelCustomerSignInNotification
{
    NSArray *notificationArray = [[UIApplication sharedApplication] scheduledLocalNotifications];
    for (int i = 0 ; i < [notificationArray count]; i++) {
        UILocalNotification *localNotification = [notificationArray objectAtIndex:i];
        DebugLog(@"Recieved Notification %@",localNotification);
        if([[localNotification.userInfo objectForKey:@"Type"] isEqualToString:CUSTOMERSIGN_LNTYPE]) {
            DebugLog(@"the notification which is canceld is %@", [localNotification.userInfo objectForKey:@"Type"]);
            [[UIApplication sharedApplication] cancelLocalNotification:localNotification] ;
        }
    }
}

//set oper graph share enabled
+(void)setIsOpenGraphShareEnabled:(BOOL)idvalue{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:idvalue forKey:@"Is_OpenGraphShare_Enabled"];
    [defaults synchronize];
}

+(BOOL)isOpenGraphShareEnabled{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults boolForKey:@"Is_OpenGraphShare_Enabled"];
}
@end
