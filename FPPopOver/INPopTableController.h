//
//  INPopTableController.h
//  shoplocal
//
//  Created by Rishi on 03/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol INPopTableControllerDelegate <NSObject>
@required
- (void)selectedTableRow:(NSString *)value;
@end

@interface INPopTableController : UITableViewController
@property(nonatomic,assign) id<INPopTableControllerDelegate> delegate;
@end
